from datetime import date, datetime, time, timedelta
from decimal import Decimal
from unittest import mock

from dateutil import relativedelta

from django.core import mail
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from django.utils.timezone import get_current_timezone, now

from api.core import ApiError
from benevole.models import Benevole, NoteFrais, TypeActivite
from client.models import Client, Referent
from common.choices import Services
from common.export import openxml_contenttype
from common.test_utils import mocked_httpx, read_xlsx
from transport.models import Facture, Frais, TrajetCommun, Transport
from transport.tests.tests import DataMixin
from transport.utils import generer_notes_frais
from transport.views import LotAFacturer
from ..transport_policies import FacturationPolicy
from ..views import get_api


@override_settings(CID_API_URL="https://example.cid-erp.app/api/")
class NETransportTests(DataMixin, TestCase):
    def test_cout_annulation(self):
        """Les frais d'annulation dépendent de la date du transport."""
        fpolicy = FacturationPolicy()
        transport = Transport(date=date(2020, 4, 3))
        self.assertEqual(fpolicy.cout_annulation(transport), Decimal('10'))
        transport = Transport(date=date(2024, 1, 3))
        self.assertEqual(fpolicy.cout_annulation(transport), Decimal('25'))

    @tag("factures")
    def test_facturation_minimale(self):
        """Un minimum de 4 kilomètres sont facturés"""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE,
        )
        tr.trajets_tries[0].dist_calc = 1200
        tr.trajets_tries[0].save()
        tr.trajets_tries[1].dist_calc = 1200
        tr.trajets_tries[1].save()
        donnees_fact = tr.donnees_facturation()
        self.assertEqual(donnees_fact['km'], Decimal('4.0'))
        self.assertEqual(donnees_fact['cout_km'], Decimal('3.60'))

    @tag("factures")
    def test_facturer_transport_avec_attente(self):
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        test_data = [
            (70, 0),
            (100, 0),
            (125, Decimal("5.00")),
            (180, Decimal("15.00")),  # 3 * 5
        ]
        for minutes, cost in test_data:
            tr.temps_attente = timedelta(minutes=minutes)
            if cost == 0:
                self.assertNotIn('cout_attente', tr.donnees_facturation())
            else:
                self.assertEqual(tr.donnees_facturation()['cout_attente'], cost)
        tr.save()
        self.assertEqual(tr.donnees_facturation()['total'], Decimal('50.28'))
        self.client.force_login(self.user)
        self.client.post(
            reverse('facturer-transports', args=[tr.date.year, tr.date.month]),
            data={'date_facture': date.today()}
        )
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        self.assertEqual(self.trclient.factures_transports.first().nb_transp, 1)

    @tag("factures")
    def test_envoi_facture_mensuelle(self):
        # Premier transport aller-retour, avec temps d'attente et frais
        transport_ar = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=True, km=30,
            duree_rdv=timedelta(minutes=30), temps_attente=timedelta(minutes=195),
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        Frais.objects.create(transport=transport_ar, typ='repas', cout='17.5')
        # Transport aller simple
        transport_a = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date(2023, 7, 15), time(10, 00), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=15,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        # Transport annule
        transport_ann = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date(2023, 7, 15), time(14, 00), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=15, chauffeur=None,
            statut=Transport.StatutChoices.ANNULE,
            statut_fact=Transport.FacturationChoices.FACTURER_ANNUL,
        )
        LotAFacturer(date(2023, 7, 1), client=self.trclient).facturer(date(2023, 8, 5))
        facture = self.trclient.factures_transports.first()
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            msg = 'Impossible d’envoyer une facture si le débiteur n’est pas lié à un partenaire CID'
            with self.assertRaisesMessage(ValueError, msg):
                result = api.create_invoice('transport', [facture])
            self.trclient.persona.id_externe = 42
            self.trclient.persona.save()
            self.client.force_login(self.user)
            response = self.client.post(
                reverse("factures-transmission", args=["transport"]),
                data={"fact_id": facture.pk},
                follow=True
            )
            self.assertContains(response, "1 facture a été transmise avec succès")
            sent_data = mock_response.call_args[1]
        self.maxDiff = None
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 42, 'beneficiaryid': 42,
                'invoicedate': '2023-08-05', 'period': '07/2023',
                'journal': 'F510',
                'lines': [{
                    'code_transport': transport_ar.pk,
                    'date_transport': '2023-07-12 11:30:00',
                    'start_transport': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                    'end_transport': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                    'sens_transport': 'Aller-retour',
                    'type_transport': 'Médico-thérapeutique',
                    'productref': '51.100',
                    'description': 'Transport du 12.07.2023 11:30',
                    'quantity': 29.2,
                }, {
                    'code_transport': transport_ar.pk,
                    'productref': '51.130',
                    'description': 'Prise en charge aller-retour',
                    'quantity': 1.0,
                }, {
                    'code_transport': transport_ar.pk,
                    'description': 'Temps d’attente',
                    'productref': '51.140',
                    'quantity': 3.0
                }, {
                    'code_transport': transport_ar.pk,
                    'productref': '51.310',
                    'description': 'Facturation des frais de repas',
                    'quantity': 1.0,
                    'unitamount': 17.5
                },
                # Transport aller simple
                {
                    'code_transport': transport_a.pk,
                    'date_transport': '2023-07-15 10:00:00',
                    'start_transport': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                    'end_transport': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                    'sens_transport': 'Aller simple',
                    'type_transport': 'Médico-thérapeutique',
                    'productref': '51.100',
                    'description': 'Transport du 15.07.2023 10:00',
                    'quantity': 29.2,
                }, {
                    'code_transport': transport_a.pk,
                    'productref': '51.120',
                    'description': 'Prise en charge aller simple',
                    'quantity': 1.0,
                },
                # Transport annulé
                {
                    'code_transport': transport_ann.pk,
                    'date_transport': '2023-07-15 14:00:00',
                    'start_transport': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                    'end_transport': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                    'sens_transport': 'Aller simple',
                    'type_transport': 'Médico-thérapeutique',
                    'productref': '51.200',
                    'description': 'Annulation de dernière minute',
                    'quantity': 1.0,
                }],
            }
        )
        facture.refresh_from_db()
        self.assertEqual(facture.no, "F510/2025/0249")

    @tag("factures")
    @override_settings(COMPTA_EMAIL='compta@example.org')
    def test_annuler_facture(self):
        facture = Facture.objects.create(
            client=self.trclient, mois_facture=date(2023, 7, 1), no='111', date_facture=date(2023, 8, 5),
            nb_transp=4, montant_total=Decimal('27.35'), exporte=now(), id_externe='555',
        )
        with mock.patch('httpx.put', side_effect=mocked_httpx) as mock_response:
            facture.annuler("Mauvaise adresse")
            sent_data = mock_response.call_args[1]
        self.assertEqual(
            sent_data['json'], {'type': 'refund'}
        )
        self.assertTrue(facture.annulee)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients()[0], "compta@example.org")
        self.assertEqual(mail.outbox[0].subject, "[Croix-Rouge NE] Note de crédit / Donzé Léa")
        self.assertEqual(
            mail.outbox[0].body,
            "Une correction de facture a été effectuée pour Donzé Léa (111).\n"
            "Motif: Mauvaise adresse\n\n"
            "Message automatique de la plateforme MAD, ne pas répondre."
        )

    @tag("factures")
    @override_settings(COMPTA_EMAIL='compta@example.org')
    def test_annuler_facture_deja_payee(self):
        facture = Facture.objects.create(
            client=self.trclient, mois_facture=date(2023, 7, 1), no='111', date_facture=date(2023, 8, 5),
            nb_transp=4, montant_total=Decimal('27.35'), exporte=now(), id_externe='515151',
        )
        with mock.patch('httpx.put', side_effect=mocked_httpx) as mock_response:
            msg = "La facture est déjà dans l’état «Payée» dans CID. Impossible de l’annuler."
            with self.assertRaisesMessage(ApiError, msg):
                facture.annuler("Mauvaise adresse")

    @tag("factures")
    def test_autre_adresse_facturation_no_sinistre(self):
        self.trclient.persona.id_externe = 42
        self.trclient.persona.save()
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date(2023, 7, 15), time(10, 00), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=15,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        referent = Referent.objects.create(
            client=self.trclient, id_externe="123", salutation='', nom='Suva', prenom='',
            npa='2000', localite='Neuchâtel', rue='Service Center',
            no_sinistre="21-3456.4", facturation_pour=['transp'], pers_contact="M. Henri Gole",
        )
        self.assertEqual(self.trclient.adresse_facturation(transport), referent)
        facture = Facture.objects.create(
            client=self.trclient, mois_facture=date(2023, 7, 1), no='111', date_facture=date(2023, 8, 5),
            nb_transp=4, montant_total=Decimal('27.35'), autre_debiteur=referent,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            api.create_invoice('transport', [facture])
            sent_data = mock_response.call_args[1]
        self.assertEqual(sent_data['json']['debitorid'], "123")
        self.assertEqual(sent_data['json']['beneficiaryid'], 42)
        self.assertEqual(sent_data['json']['claimnumber'], "21-3456.4")
        self.assertEqual(sent_data['json']['contact_name'], "M. Henri Gole")

    def test_defrayer_chauffeurs(self):
        chauffeur1 = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        chauffeur2 = self._create_benevole(
            nom="Plot", prenom="John", activites=['transport'], empl_geo=[6.73, 46.94]
        )
        NoteFrais.objects.create(
            benevole=chauffeur1,
            service='transport',
            mois=date(2022, 1, 1),
            kms=6800,
        )
        # Pas contrôlé => non pris en compte
        tr0 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=datetime(2022, 3, 12, 14, 30, tzinfo=get_current_timezone()),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.RAPPORTE
        )
        a16h = datetime(2022, 3, 12, 16, 0, tzinfo=get_current_timezone())
        tr1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=a16h,
            km=Decimal('23.4'), temps_attente=timedelta(minutes=45),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        # Transports 2eme chauffeur
        client2 = Client.objects.create(
            nom='Toto', rue='Rue Neuve 1', npa='2800', localite='Delémont', empl_geo=[6.704, 47.03],
            valide_des='2022-01-01',
        )
        client3 = Client.objects.create(
            nom='Toto', rue='Rue Belle 1', npa='2800', localite='Delémont', empl_geo=[6.712, 46.91],
            valide_des='2022-01-01',
        )
        tr_ch2_0 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('19.2'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        tr_ch2_1 = self.create_transport(
            client=client2, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('59'), temps_attente=timedelta(minutes=190),
            destination=self.hne_ne, retour=True, duree_rdv=timedelta(minutes=180),
            statut=Transport.StatutChoices.CONTROLE
        )
        tr_ch2_2 = self.create_transport(
            client=client3, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('1.5'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.ANNULE,
            defrayer_chauffeur=True,
        )
        # Au retour (même jour), le transport supplémentaire ne devra pas être compté.
        tr_ch2_3 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('19.2'),
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        tr_ch2_4 = self.create_transport(
            client=client2, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('19.2'), temps_attente=timedelta(minutes=125),
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        # 3 trajets de chauffeur2 ont été transportés ensemble
        commun = TrajetCommun.objects.create(chauffeur=chauffeur2, date=a16h.date())
        tr_ch2_0.trajets_tries[0].commun = commun
        tr_ch2_0.trajets_tries[0].save()
        tr_ch2_1.trajets_tries[0].commun = commun
        tr_ch2_1.trajets_tries[0].save()
        tr_ch2_2.trajets_tries[0].commun = commun
        tr_ch2_2.trajets_tries[0].save()
        commun = TrajetCommun.objects.create(chauffeur=chauffeur2, date=a16h.date())
        tr_ch2_3.trajets_tries[0].commun = commun
        tr_ch2_3.trajets_tries[0].save()
        tr_ch2_4.trajets_tries[0].commun = commun
        tr_ch2_4.trajets_tries[0].save()
        Frais.objects.bulk_create([
            Frais(transport=tr0, typ=Frais.TypeFrais.PARKING, cout=Decimal('7.35')),  # Non contrôlé
            Frais(transport=tr1, typ=Frais.TypeFrais.REPAS, cout=Decimal('16.55')),
            Frais(transport=tr1, typ=Frais.TypeFrais.PARKING, cout=Decimal('2.4')),
            Frais(transport=tr_ch2_0, typ=Frais.TypeFrais.REPAS, cout=Decimal('15.50')),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('defraiements') + '?year=2022&month=3')
        self.maxDiff = None
        self.assertEqual(
            response.context['object_list'], [{
                'km_6000': 0, 'km_6001': Decimal('23.4'),
                'nb_attente': 0, 'nb_plusieurs': 0,
                'frais_repas': Decimal('16.55'), 'frais_divers': Decimal('2.4'),
                'benevole': chauffeur1,
                 'date_export': None,
            }, {
                'km_6000': Decimal('120.6'), 'km_6001': Decimal('0'),
                'nb_attente': Decimal('4'), 'nb_plusieurs': Decimal('2'),
                'frais_repas': Decimal('15.5'), 'frais_divers': 0,
                'benevole': chauffeur2,
                'date_export': None,
            }]
        )
        expected_john_line = (
            f'<tr><td><a href="/benevoles/transport/{chauffeur2.pk}/edition/">Plot John</a></td>'
            '<td class="text-end">15.50</td>'  # Frais de repas
            '<td class="text-end">0</td>'  # Frais divers
            '<td class="text-end">4</td>'  # Nb unité d'attente
            '<td class="text-end">2</td>'  # Nb transp. à plusieurs
            '<td class="text-end">120,6</td>'  # Km < 6000
            '<td class="text-end">0</td>'  # Km > 6000
            '<td><img src="/static/admin/img/icon-no.svg"></td></tr>'
        )
        self.assertContains(response, expected_john_line, html=True)
        # Exportation OpenXML
        response = self.client.post(reverse('defrayer-chauffeurs', args=[2022, 3]), data={})
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        content = read_xlsx(response.content)
        un_du_mois = datetime(2022, 3, 1, 0, 0)
        self.assertEqual(content[0][0], 'Chauffeurs')
        self.assertEqual(
            content[1], [
                'Transports DUPLAIN Irma', un_du_mois, 0, 23.4, 0, 0, 16.55, 2.4,
            ]
        )
        self.assertEqual(
            content[2], [
                'Transports PLOT John', un_du_mois, 120.6, 0, 4, 2, 15.5, 0,
            ]
        )
        note_frais = chauffeur1.notefrais_set.get(mois=date(2022, 3, 1))
        self.assertFalse(note_frais.lignes.filter(libelle__no='NF-B10').exists())
        # Kms > 6000km
        self.assertEqual(note_frais.lignes.get(libelle__no='NF-B11').quantite, Decimal('23.40'))
        # Après exportation, les notes de frais statiques ont été générées, les résultats devraient être les mêmes
        response = self.client.get(reverse('defraiements') + '?year=2022&month=3')
        self.assertContains(response, expected_john_line, html=True)
        # Fiche récap. des remboursements par chauffeur
        response = self.client.get(reverse('benevole-transports-rembpdf', args=[chauffeur2.pk, 2022, 3]))
        self.assertEqual(response.headers['Content-Type'], 'application/pdf')

    def _test_envoi_note_frais(self, pregenerer=False):
        benev = self._create_benevole(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[TypeActivite.TRANSPORT],
        )
        # Créer ancienne note pour tester la limite des 6000km
        NoteFrais.objects.create(
            benevole=benev,
            service='transport',
            mois=date(2023, 4, 1),
            kms=5999,
        )
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', service=Services.TRANSPORT,
            npa='2000', localite='Neuchâtel', valide_des='2023-01-01', empl_geo=[7, 47],
        )
        a_1130 = datetime.combine(
            date(2023, 5, 8), time(11, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=client,
            heure_rdv=a_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=benev,
        )
        Frais.objects.create(transport=transport, typ='repas', cout='17.5')
        self.client.force_login(self.user)
        if pregenerer:
            generer_notes_frais(date(2023, 5, 1))
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            response = self.client.post(
                reverse('notesfrais-transmission', args=[2023, 5]), data={'service': 'transport'},
                headers={"accept": "application/json"},
            )
            self.assertEqual(response.json(), {'result': 'OK', 'success': 1, 'errors': []})
            sent_data = mock_response.call_args[1]
        note = benev.notefrais_set.get(mois=date(2023, 5, 1))
        self.assertEqual(note.kms, 4)
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'NF transport - mai 2023',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 17.5, 'servicetype': 'transport'},
                {'productref': 'NF-B10', 'quantity': 1.0, 'servicetype': 'transport'},  # tarif <6000km
                {'productref': 'NF-B11', 'quantity': 3.0, 'servicetype': 'transport'},  # tarif >=6000km
            ],
        })

    def test_envoi_note_frais_pre_calc(self):
        self._test_envoi_note_frais(pregenerer=True)

    def test_envoi_note_frais_no_pre_calc(self):
        self._test_envoi_note_frais(pregenerer=False)

    def test_envoi_note_frais_dynamique(self):
        """Envoi note de frais individuelle dynamiquement à partir de transports."""
        benev = self._create_benevole(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[TypeActivite.TRANSPORT]
        )
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', service=Services.TRANSPORT,
            npa='2000', localite='Neuchâtel', valide_des='2023-01-01', empl_geo=[7, 47],
        )
        a_1130 = datetime.combine(
            date(2023, 5, 8), time(11, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=client,
            heure_rdv=a_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=benev,
        )
        Frais.objects.create(transport=transport, typ='repas', cout='17.5')
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            response = self.client.post(
                reverse('notesfrais-transmission', args=[0, 0]), data={
                    'service': 'transport',
                    'note_benevole_id': benev.pk,
                    'note_year': "2023",
                    'note_month': "5",
                },
                headers={"accept": "application/json"},
            )
            self.assertEqual(response.json(), {'result': 'OK', 'success': 1, 'errors': []})
            sent_data = mock_response.call_args[1]
        note = benev.notefrais_set.get(mois=date(2023, 5, 1))
        self.assertEqual(note.kms, 4)
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'NF transport - mai 2023',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 17.5, 'servicetype': 'transport'},
                {'productref': 'NF-B10', 'quantity': 4.0, 'servicetype': 'transport'},  # tarif <6000km
            ],
        })
