from datetime import date, datetime, timedelta
from decimal import Decimal
from unittest import mock

from freezegun import freeze_time

from django.conf import settings
from django.test import TestCase, override_settings, tag
from django.urls import reverse

from alarme.models import (
    ArticleFacture, Facture, Frais, Installation, Materiel, MaterielClient,
    Mission, RabaisAuto, TypeAbo
)
from alarme.tests.test_general import TestUtils
from ape.models import Appart, Facture as FactureAPE
from benevole.models import TypeActivite
from client.models import Client, Referent
from common.choices import Services
from common.models import Utilisateur
from common.test_utils import mocked_httpx, read_response_pdf_text, read_xlsx

from ..alarme_policies import CalculateurFrais, FacturationPolicy

from ..views import get_api

today = date.today()


class NETests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article = ArticleFacture.objects.create(
            code='40.010', designation="Abonnement mensuel Casa", prix=Decimal('25.5'),
        )

    def test_referent_fact_recoit_courrier(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna', service=Services.ALARME)
        referent = Referent.objects.create(
            client=cl, nom='Schmid', prenom='Esther', salutation='F',
            facturation_pour=['al-tout'],
        )
        self.assertTrue(referent.recoit_courrier)

    def test_note_frais_avec_forfait_tel(self):
        benev = self._create_benevole(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[TypeActivite.INSTALLATION],
            utilisateur=Utilisateur.objects.create_user(
                'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain'
            )
        )
        cl = Client.objects.create(nom='Schmid', prenom='Léna', service=Services.ALARME)
        interv = Mission.objects.create(
            client=cl,
            intervenant=benev.utilisateur,
            delai=date.today(),
            type_mission=self.typem,
            effectuee=date.today().replace(day=1) - timedelta(days=4),
        )
        calculateur = CalculateurFrais(interv.effectuee.replace(day=1))
        calculateur.calculer_frais(enregistrer=True)
        ligne = benev.notefrais_set.first().lignes.first()
        self.assertEqual(str(ligne.libelle), "Type de frais «Forfait téléphone» (NF-B51)")
        self.assertEqual(ligne.quantite, 1)
        self.assertIsNone(ligne.montant_unit)

    @tag("factures")
    def test_install_des_20_non_facturee(self):
        """Seuls les frais d'install sont facturés après le 20 du mois."""
        self.create_client_with_installations(1, debut_install=date.today().replace(day=20))[0]
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 1)
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 0)

    @tag("factures")
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures_avec_samas(self):
        ArticleFacture.objects.create(
            code=settings.CODE_ARTICLE_ABO_SAMA,
            designation="Répondant samaritain - abonnement mensuel", prix=5
        )
        install1, install2 = self.create_client_with_installations(2)
        install1.client.samaritains = 1
        install1.client.samaritains_des = (date.today() - timedelta(days=30), None)
        install1.client.partenaire = Client.objects.create(
            nom="Haddock", prenom="Georges", date_naissance="1950-02-12", service='alarme'
        )
        install1.client.save()
        # Ne pas facturer cet abo samas car seulement activé ce mois.
        install2.client.samaritains = 1
        install2.client.samaritains_des = (date.today(), None)
        install2.client.save()
        article = ArticleFacture.objects.create(
            code='EMO1', designation='Émetteur', prix=Decimal('11.30')
        )
        abo_emetteur = TypeAbo.objects.create(nom='Émetteur supp.', article=article)
        MaterielClient.objects.create(
            client=install1.client,
            materiel=Materiel.objects.create(type_mat=self.type_emetteur),
            abonnement=abo_emetteur,
            date_debut=date(2020, 1, 1),
        )
        Facture.objects.all().delete()
        mois_passe = date.today() - timedelta(days=10)
        num_factures = Installation.generer_factures(mois_passe)
        num_factures += MaterielClient.generer_factures(mois_passe)
        self.assertEqual(num_factures, 5)
        factures = Facture.objects.filter(client=install1.client)
        self.assertEqual(
            sorted(factures.values_list('article__designation', flat=True)),
            ['Abonnement mensuel CASA',
             'Répondant samaritain - abonnement mensuel', 'Répondant samaritain - abonnement mensuel',
             'Émetteur']
        )
        self.assertQuerySetEqual(
            install2.client.factures.values_list('article__designation', flat=True),
            ['Abonnement mensuel CASA']
        )
        # Idempotence...
        num_factures = Installation.generer_factures(mois_passe)
        self.assertEqual(num_factures, 0)

    @tag("factures")
    @freeze_time("2024-03-07")
    def test_generer_factures_avec_rabais_auto(self):
        art_rabais = ArticleFacture.objects.create(code='52.950', designation="Fonds CRS")
        install = self.create_client_with_installations(1)[0]
        RabaisAuto.objects.create(
            client=install.client, article=art_rabais,
            duree=(date.today() - timedelta(days=40), date.today() + timedelta(days=40))
        )
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 2)
        factures = install.client.factures.all()
        self.assertEqual(
            list(factures.values_list('date_facture', 'mois_facture', 'libelle')),
            [(date(2024, 3, 7), date(2024, 3, 1), 'Abonnement mensuel CASA - mars 2024'),
             (date(2024, 3, 7), date(2024, 3, 1), 'Fonds CRS'),
            ]
        )
        self.assertEqual(factures[1].article, art_rabais)

    @tag("factures")
    @freeze_time("2023-11-04")
    def test_facturation_3mois_minimum(self):
        install1 = self.create_client_with_installations(1)[0]
        install1.date_fin_abo = date(2023, 11, 26)
        install1.save()
        num_factures = Installation.generer_factures(date(2023, 11, 1))
        self.assertEqual(num_factures, 3)
        self.assertEqual(
            list(install1.client.factures.values_list('date_facture', 'mois_facture', 'libelle')),
            [(date(2023, 11, 4), date(2023, 11, 1), 'Abonnement mensuel CASA - novembre 2023'),
             (date(2023, 11, 4), date(2023, 11, 1), 'Abonnement mensuel CASA - décembre 2023'),
             (date(2023, 11, 4), date(2023, 11, 1), 'Abonnement mensuel CASA - janvier 2024'),
            ]
        )

    @tag("factures")
    @freeze_time("2023-11-04")
    def test_facturation_3mois_minimum_apres_20(self):
        """Install et désinstallation après le 20 du mois."""
        install1 = self.create_client_with_installations(1, debut_install=date(2023, 11, 22))[0]
        install1.date_fin_abo = date(2023, 11, 26)
        install1.save()
        num_factures = Installation.generer_factures(date(2023, 11, 1))
        self.assertEqual(num_factures, 4)
        facts_abo = install1.client.factures.exclude(libelle__contains='installation')
        self.assertEqual(
            list(facts_abo.values_list('date_facture', 'libelle')),
            [(date(2023, 11, 4), 'Abonnement mensuel CASA - décembre 2023'),
             (date(2023, 11, 4), 'Abonnement mensuel CASA - janvier 2024'),
             (date(2023, 11, 4), 'Abonnement mensuel CASA - février 2024'),
            ]
        )

    @override_settings(SIGNATURES={})
    def test_impression_courrier_client_ape(self):
        """Le courrier de bienvenue des clients «APE» est légèrement adapté."""
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), service=Services.ALARME,
            type_logement='ape',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-courrier', args=[client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')
        content = read_response_pdf_text(response)
        self.assertIn("l’option des répondants mandatés par", content)

    def test_impression_questionnaire_client_ape(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), service=Services.ALARME,
            type_logement='ape',
        )
        self.client.force_login(self.user)
        url = reverse('client-questionnaire', args=[client.pk])
        response = self.client.get(url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        content = read_response_pdf_text(response)
        self.assertIn("Locataire ApE Alarme Croix-Rouge", content)

    def test_impression_courrier_debiteur(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), service=Services.ALARME,
        )
        referent = Referent.objects.create(
            client=client, nom='Schmid', prenom='Esther', salutation='F',
            facturation_pour=['al-tout'],
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('client-referent-courrier', args=[client.pk, referent.pk, 'courrier_debiteur'])
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_exportation_frais_benevoles(self):
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont",
            service=Services.ALARME,
        )
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        benev = self._create_benevole(
            nom="Duplain", prenom="Irma",
            activites=[TypeActivite.INSTALLATION],
            utilisateur=user_benev
        )
        m1 = Mission.objects.create(
            type_mission=self.typem, client=client, intervenant=benev.utilisateur,
            delai=date.today(), effectuee=date(2024, 5, 12), km=26
        )
        Frais.objects.create(mission=m1, typ='repas', cout='18.35', descriptif='Repas')
        un_du_mois = date(2024, 5, 1)
        calculateur = CalculateurFrais(un_du_mois)
        fpolicy = FacturationPolicy()
        frais_par_benevole = calculateur.frais_par_benevole()
        export = fpolicy.export_frais_benevoles(frais_par_benevole, un_du_mois)

        content = read_xlsx(export.get_http_response("foo").content)
        expected = [
            'Duplain Irma', datetime(2024, 5, 1, 0, 0), 18.35, 26, 10,
            '=C2+(ROUND(D2*0.70*20, 0) / 20)+E2'
        ]
        self.assertEqual(content[1], expected)


@tag("factures")
class EnvoiFacturesTests(TestUtils):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article = ArticleFacture.objects.create(
            code='52.100', designation="Abonnement mensuel Casa",
        )

    def test_envoi_facture_mensuelle(self):
        install = self.create_client_with_installations(1)[0]
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1),
            libelle="Abonnement mensuel Casa - avril 2022",
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_envoi_facture_mensuelle(sent_data, result)

    def test_envoi_facture_manuelle(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.create(
            code='40.300', designation="Forfait déplacement", prix=50
        )
        facture = Facture.objects.create(
            client=install.client, article=article,
            date_facture=date(2022, 4, 3), libelle="Forfait déplacement", montant=50,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_envoi_facture_manuelle(sent_data, result)

    def test_envoi_factures_groupees(self):
        install = self.create_client_with_installations(1, debut_install=date(2022, 2, 1))[0]
        num_fact = install.generer_factures(date(2022, 3, 1), date_factures=date(2022, 3, 13))
        self.assertEqual(num_fact, 1)
        Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 3, 13), mois_facture=date(2022, 3, 1), libelle="Abonnement Casa - mars 2022",
        )
        art2 = ArticleFacture.objects.create(
            code='40.320', designation="Bracelet émetteur", prix=60
        )
        Facture.objects.create(
            client=install.client, install=None, article=art2,
            date_facture=date(2022, 3, 13), mois_facture=date(2022, 3, 13),
            libelle="Bracelet émetteur", montant=60,
        )
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('factures-transmission', args=['alarme']), data={'client': install.client_id}, follow=True
            )
        self.assert_envoi_factures_groupees(mock_response)

    def test_envoi_toutes_factures(self):
        install = self.create_client_with_installations(1, debut_install=date(2022, 2, 1))[0]
        Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 3, 13), mois_facture=date(2022, 3, 1), libelle="Abonnement Casa - mars 2022",
        )
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('factures-transmission', args=['alarme']), data={}, follow=True
            )
        mock_response.assert_called_once()

    def test_autre_adresse_facturation(self):
        install = self.create_client_with_installations(1)[0]
        Referent.objects.create(
            salutation='M', nom='Meier', prenom='Aristide', npa='2000', localite='Neuchâtel', rue='Fleurs 5',
            courriel='ad@example.com', facturation_pour=['al-tout'], client=install.client, id_externe='4321',
        )
        Referent.objects.create(
            salutation='M', nom='Mayer', prenom='John', npa='2000', localite='Neuchâtel', rue='Fleurs 15',
            courriel='ad2@example.com', facturation_pour=['al-abo'], client=install.client, id_externe='1234',
            pers_contact="M. Henri Gole",
        )
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture", montant=12.5,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_autre_adresse_facturation(sent_data, result)

    def test_envoi_facture_rabais(self):
        """3 types de rabais selon article facturé (#421)."""
        install = self.create_client_with_installations(1)[0]
        fact1 = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture abo", rabais=5,
        )
        art_install = ArticleFacture.objects.create(
            code='52.300', designation="Installation système Casa", prix=Decimal('55'),
        )
        fact2 = Facture.objects.create(
            client=install.client, install=install, article=art_install,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture install", rabais=25,
        )
        art_mat = ArticleFacture.objects.create(
            code='52.200', designation="Bracelet émetteur", prix=Decimal('15'),
        )
        fact3 = Facture.objects.create(
            client=install.client, install=install, article=art_mat,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture mat",
            montant=12.5, rabais=12.5
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            api.create_invoice('alarme', [fact1, fact2, fact3])
            mock_response.assert_called_once()
            sent_data = mock_response.call_args[1]
        self.assertEqual(sent_data['json'], {
            'debitorid': 7777, 'beneficiaryid': 7777, 'invoicedate': '2022-04-03',
            'period': '03/2022', 'journal': 'F520',
            'lines': [
                {'productref': '52.100', 'description': 'Facture abo', 'quantity': 1.0},
                {'productref': '52.920', 'description': 'Escomptes, rabais', 'quantity': 1.0, 'unitamount': -5.0},
                {'productref': '52.300', 'description': 'Facture install', 'quantity': 1.0},
                {'productref': '52.940', 'description': 'Escomptes, rabais', 'quantity': 1.0, 'unitamount': -25.0},
                {'productref': '52.200', 'description': 'Facture mat', 'quantity': 1.0, 'unitamount': 12.5},
                {'productref': '52.910', 'description': 'Escomptes, rabais', 'quantity': 1.0, 'unitamount': -12.5}
            ]
        })

    def test_envoi_facture_ape(self):
        gerant = Referent.objects.create(
            salutation='M', nom='Meier', prenom='Aristide', npa='2000', localite='Neuchâtel', rue='Fleurs 5',
            courriel='ad@example.com', facturation_pour=['al-tout'], id_externe=4321,
        )
        appart = Appart.objects.create(
            gerant=gerant, no_app='N°3', rue='Place de la Gare 4',
            npa='2300', localite='La Chaux-de-Fonds',
            abo=self.abo
        )
        fact1 = FactureAPE.objects.create(
            appart=appart, abo=self.abo,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1)
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            api.create_invoice('ape', [fact1])
            mock_response.assert_called_once()
            sent_data = mock_response.call_args[1]
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'debitorid': 4321, 'beneficiaryid': 4321,
            'invoicedate': '2022-04-03',
            'period': '03/2022',
            'journal': 'F520',
            'lines': [{
                'productref': 'ABO1',
                'description': 'standard - mars 2022 - Appartement N°3',
                'quantity': 1.0,
            }]
        })

    def test_envoi_facture_erreur(self):
        install = self.create_client_with_installations(1)[0]
        install.client.persona.nom = 'Errorum CID'
        install.client.persona.id_externe = 98765
        install.client.persona.save()
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture erreur", montant=12.5,
        )
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx):
            response = self.client.post(
                reverse('factures-transmission', args=['alarme']),
                data={'fact_id': facture.pk},
                follow=True
            )
        self.assertContains(response, 'Erreur API CID 400:')
        facture.refresh_from_db()
        self.assertTrue(facture.export_err.startswith('Erreur API CID 400:'))


@override_settings(CID_API_URL="https://example.cid-erp.app/api/")
class CIDTests(EnvoiFacturesTests, TestCase):
    def create_client_with_installations(self, *args, **kwargs):
        installs = super().create_client_with_installations(*args, **kwargs)
        installs[0].client.persona.id_externe = 7777
        installs[0].client.persona.save(update_fields=['id_externe'])
        return installs

    def assert_envoi_facture_mensuelle(self, sent_data, result):
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-04-03', 'period': '03/2022',
                'journal': 'F520',
                'lines': [{
                    'productref': '52.100',
                    'description': 'Abonnement mensuel Casa - avril 2022',
                    'quantity': 1.0,
                }],
            }
        )
        self.assertEqual(result, {"id": 1234, "number": "F510/2025/0249"})

    def assert_envoi_facture_manuelle(self, sent_data, result):
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-04-03', 'period': '',
                'journal': 'F520',
                'lines': [{
                    'productref': '40.300', 'description': 'Forfait déplacement',
                    'quantity': 1.0, 'unitamount': 50.0,
                }],
            }
        )
        self.assertEqual(result, {"id": 1234, "number": "F510/2025/0249"})

    def assert_envoi_factures_groupees(self, mock_response):
        posted_data = mock_response.call_args_list[0].kwargs['json']
        self.maxDiff = None
        self.assertEqual(
            posted_data, {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-03-13', 'period': '03/2022',
                'journal': 'F520',
                'lines': [{
                    'productref': 'ABO1', 'description': 'Abonnement mensuel CASA - mars 2022',
                    'quantity': 1.0, 'unitamount': 37.45,
                }, {
                    'productref': '52.100', 'description': 'Abonnement Casa - mars 2022',
                    'quantity': 1.0,
                }, {
                    'productref': '40.320', 'description': 'Bracelet émetteur',
                    'quantity': 1.0, 'unitamount': 60.0,
                }],
            }
        )
        self.assertTrue(Facture.objects.filter(id_externe=1234).exists())

    def assert_autre_adresse_facturation(self, sent_data, result):
        self.assertEqual(sent_data['json']['beneficiaryid'], 7777)
        self.assertEqual(sent_data['json']['debitorid'], 1234)
        self.assertEqual(sent_data['json']['contact_name'], "M. Henri Gole")


@override_settings(CID_API_URL="https://example.cid-erp.app/api/")
class EnvoiNotesFraisTests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.benev = cls._create_benevole(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[TypeActivite.INSTALLATION],
            utilisateur=Utilisateur.objects.create_user(
                'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain'
            )
        )
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=Services.ALARME)
        m1 = Mission.objects.create(
            type_mission=cls.typem, client=client, intervenant=cls.benev.utilisateur,
            delai=date.today(), effectuee=date(2023, 5, 12), km=26
        )
        Frais.objects.create(mission=m1, typ='repas', cout='18.35', descriptif='Repas')
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')

    def test_envoi_toutes_notes_frais(self):
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('notesfrais-transmission', args=[2023, 5]), data={'service': 'alarme'}
            )
            mock_response.assert_called_once()
            sent_data = mock_response.call_args[1]
        note = self.benev.notefrais_set.first()
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'NF alarme - mai 2023',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 18.35, 'servicetype': 'alarme'},
                {'productref': 'NF-B06', 'quantity': 1.0, 'unitamount': 4.5, 'servicetype': 'alarme'},
                {'productref': 'NF-B01', 'quantity': 26.0, 'servicetype': 'alarme'},
                {'productref': 'NF-B51', 'quantity': 1.0, 'servicetype': 'alarme'},
            ],
        })

    def test_envoi_note_frais_individuelle(self):
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            response = self.client.post(
                reverse('notesfrais-transmission', args=[2023, 5]), data={
                    'service': 'alarme',
                    'note_benevole_id': self.benev.pk,
                    'note_year': "2023",
                    'note_month': "5",
                }
            )
            self.assertRedirects(response, reverse('home'))
            mock_response.assert_called_once()
            sent_data = mock_response.call_args[1]
        note = self.benev.notefrais_set.first()
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'NF alarme - mai 2023',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 18.35, 'servicetype': 'alarme'},
                {'productref': 'NF-B06', 'quantity': 1.0, 'unitamount': 4.5, 'servicetype': 'alarme'},
                {'productref': 'NF-B01', 'quantity': 26.0, 'servicetype': 'alarme'},
                {'productref': 'NF-B51', 'quantity': 1.0, 'servicetype': 'alarme'},
            ],
        })
