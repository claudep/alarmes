from datetime import date
from pathlib import Path
from unittest import skipUnless
from unittest.mock import patch

from django.apps import apps
from django.contrib.auth.models import Permission
from django.core.files import File
from django.test import TestCase
from django.urls import reverse

from client.models import Client, Services
from common.models import Utilisateur
from common.test_utils import mocked_httpx
from ..utils import rue_changed


class NETests(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(
                content_type__model='client',
                codename__in=['view_client', 'add_client', 'change_client'])
            )
        )

    def test_rue_changed(self):
        self.assertTrue(rue_changed("Rue des Fleurs 4", "Rue des Fleurs 5"))
        self.assertFalse(rue_changed("Rue des Fleurs 4b", "Rue des  fleurs 4 B"))
        self.assertFalse(rue_changed("Faubourg de l'Hôpital", "Fbg de l'Hôpital"))
        self.assertTrue(rue_changed("Faubourg de l'Hôpital 3", "Fbg du Lac 3"))

    @skipUnless(apps.is_installed("besoins"), "App besoins is not installed")
    def test_import_osad(self):
        # Sera archivé
        ancien_client = Client.objects.create(
            service=Services.OSAD, nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
        )
        # Avec ancienne adresse qui sera mise à jour
        client_existant = Client.objects.create(
            service=[Services.OSAD, Services.TRANSPORT], id_externe2=260497, genre='F',
            nom="HELL BLAU", prenom="Madeleine", date_naissance=date(1938, 4, 20),
            rue="rue du Stand 12", npa="2300", localite="La Chaux-de-Fonds",
            tel_1="079 222 91 44", tel_2="+41 77 444 44 00",
        )
        self.client.force_login(self.user)
        with (Path(__file__).parent / 'clients_osad.csv').open(mode='rb') as fh:
            response = self.client.post(reverse('import_osad'), data={
                'fichier': File(fh, name='clients_osad.csv'),
                'dry_run': "on",
            })
            self.assertContains(response, "Le nouveau client Jules-Maison Jean-Marie a été créé (ligne 3)")
        with (
            (Path(__file__).parent / 'clients_osad.csv').open(mode='rb') as fh,
            self.captureOnCommitCallbacks(execute=True) as callbacks,
            patch('httpx.get', side_effect=mocked_httpx) as mock,
        ):
            response = self.client.post(reverse('import_osad'), data={
                'fichier': File(fh, name='clients_osad.csv'),
            })
        self.assertEqual(len(callbacks), 4)
        mock.assert_called()
        self.assertRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        self.assertEqual(Client.objects.count(), 5)
        # Deuxième fois même fichier, format xlsx
        with (Path(__file__).parent / 'clients_osad.xlsx').open(mode='rb') as fh:
            response = self.client.post(reverse('import_osad'), data={
                'fichier': File(fh, name='clients_osad.xlsx'),
            })
        self.assertRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        self.assertEqual(Client.objects.count(), 5)
        ancien_client.refresh_from_db()
        self.assertEqual(ancien_client.prestations_actuelles(), [])
        client_existant.refresh_from_db()
        self.assertEqual(client_existant.adresse().rue, "Rue de Tête-de-Chien 2")
