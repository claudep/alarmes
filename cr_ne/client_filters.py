from datetime import date

from client.forms import ClientFilter


class ClientsSamaFilter(ClientFilter):
    label = "Clients avec service Samaritains"

    def filter(self, clients):
        return clients.filter(samaritains_des__contains=date.today())
