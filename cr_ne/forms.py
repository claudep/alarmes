from django import forms


class ClientImportForm(forms.Form):
    fichier = forms.FileField(label="Fichier clients à importer")
    dry_run = forms.BooleanField(
        label="Voir les différences sans importer réellement", required=False, initial=True
    )

    def clean_fichier(self):
        fichier = self.cleaned_data['fichier']
        if not fichier.name.endswith(('.csv', '.xlsx')):
            raise forms.ValidationError("Ce fichier ne semble pas être un fichier Excel ou CSV")
        return fichier
