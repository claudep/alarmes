from datetime import timedelta
from decimal import Decimal

from openpyxl.styles import Alignment

from django.utils.dateformat import format as django_format

from alarme.models import CalculateurFraisAlarme, Facture, FacturationPolicyBase
from benevole.models import LigneFrais, TypeFrais
from common.export import ExpLine, OpenXMLExport
from common.utils import same_month


class DefraiementExport(OpenXMLExport):
    default_font_size = 12


class CalculateurFrais(CalculateurFraisAlarme):
    TARIF_KM_ALARME = Decimal('0.70')
     # Participation forfaitaire mensuelle aux frais téléphoniques (CHF)
    PART_FRAIS_TEL = Decimal('10.0')

    def lignes_depuis_data(self, *args, **kwargs):
        lignes = super().lignes_depuis_data(*args, **kwargs)
        # Participation frais tél.
        lignes.append(LigneFrais(
            libelle=TypeFrais.objects.get(no='NF-B51'),
            quantite=Decimal('1'),
        ))
        return lignes


class FacturationPolicy(FacturationPolicyBase):
    def creer_facture_mensuelle(self, mois, installs, date_factures):
        factures = super().creer_facture_mensuelle(mois, installs, date_factures)
        # Facturation minimale de 3 mois (#401)
        install1 = installs[0]
        if (
            len(installs) == 1 and install1.date_fin_abo and
            same_month(install1.date_fin_abo, mois)
        ):
            num_factures = install1.client.factures.filter(article=install1.abonnement.article).count()
            if num_factures < 3:
                libelle = f'{install1.abonnement.article.designation} - %s'
                for i in range(3 - num_factures):
                    mois_futur = (mois.replace(day=1) + timedelta(days=31 * (i + 1))).replace(day=1)
                    nom_mois = django_format(mois_futur, "F Y")
                    factures.append(
                        Facture.objects.create(
                            client=install1.client,
                            mois_facture=mois,
                            date_facture=date_factures,
                            article=install1.abonnement.article,
                            install=install1,
                            materiel=None,
                            libelle=libelle % nom_mois,
                            montant=install1.abonnement.article.prix,
                        )
                    )
        return factures

    def calculateur_frais(self, mois):
        return CalculateurFrais(mois)

    def export_frais_benevoles(self, frais_par_benevole, un_du_mois):
        headers = [
            'Bénévole',
            'Mois de la prestation',
            '606 - Indemnités effectives - Frais divers (CHF)',
            '621 Frais de déplacements – Nb km',
            '633 Participation aux frais téléphone bénévole',
            'Montant à défrayer (y.c. frais + km)',
        ]
        calculateur = self.calculateur_frais(un_du_mois)
        tarif_km = calculateur.TARIF_KM_ALARME
        frais_tel = calculateur.PART_FRAIS_TEL
        frais_par_benevole = calculateur.frais_par_benevole()

        export = DefraiementExport(col_widths=[43, 12, 8, 8, 8, 15])
        export.write_line(ExpLine(
            headers, bold=True, alignment=Alignment(text_rotation=90), height=270, borders='all'
        ))
        export.ws.freeze_panes = export.ws['B2']
        export.ws.print_title_rows = '1:1'

        for idx, (benevole, line) in enumerate(frais_par_benevole.items(), start=2):
            total_formula = f'=C{idx}+(ROUND(D{idx}*{tarif_km}*20, 0) / 20)+E{idx}'
            export.write_line([
                f"{line['benevole__nom']} {line['benevole__prenom']}", un_du_mois,
                line['frais_repas'] + line['frais_divers'], line['kms'], frais_tel, total_formula
            ])
        num_lines = len(frais_par_benevole) + 1
        numb_formats = {3: '0.00', 5: '0.00', 6: "0.00 [$CHF]"}
        export.write_line(ExpLine(
            ['Totaux', ''] + [
                f'=SUM(C2:C{num_lines})', f'=SUM(D2:D{num_lines})', f'=SUM(E2:E{num_lines})',
                f'=SUM(F2:F{num_lines})'
            ],
            bold=True,
            number_formats=numb_formats,
            height=17,
            borders='all'
        ))
        return export
