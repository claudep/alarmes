from collections import defaultdict
from datetime import date, timedelta
from decimal import Decimal

from openpyxl.styles import Alignment
from openpyxl.utils import get_column_letter

from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count, Sum
from django.db.models.functions import TruncMonth

from benevole.models import LigneFrais, TypeFrais
from common.export import ExpLine, OpenXMLExport
from common.utils import canton_app
from transport.models import (
    CalculateurFraisTransport, FacturationPolicyBase, TrajetCommun, Transport
)

TARIF_AVS = Decimal('0.90')
TARIF_NON_AVS = Decimal('1.10')
COUT_ATTENTE_UNITE = Decimal('5.00')


class DefraiementExport(OpenXMLExport):
    default_font_size = 16


class CalculateurFrais(CalculateurFraisTransport):
    def _finaliser(self, chauffeurs):
        super()._finaliser(chauffeurs)
        # Transports à plusieurs clients
        communs = TrajetCommun.objects.annotate(
            month=TruncMonth('date'),
            num_cl=Count('trajet'),
        ).filter(month=self.mois, num_cl__gt=1).annotate(
            client_ids=ArrayAgg('trajet__transport__client_id')
        )
        # Compter les clients suppl. transportés, mais 1x par jour et client
        seen_chauffeur_day_client = defaultdict(set)
        for commun in communs:
            if commun.chauffeur not in chauffeurs:
                continue
            num_clients = len(set(commun.client_ids))
            new_clients = set(commun.client_ids) - seen_chauffeur_day_client[(commun.chauffeur, commun.date)]
            if len(new_clients) < num_clients:
                # Des clients ont déjà été enlevés, pas besoin de soustraire 1
                num_clients_supp = len(new_clients)
            else:
                num_clients_supp = num_clients - 1
            chauffeurs[commun.chauffeur]['nb_plusieurs'] += num_clients_supp
            seen_chauffeur_day_client[(commun.chauffeur, commun.date)] |= set(commun.client_ids)

    def lignes_depuis_data(self, benevole, data):
        lignes = super().lignes_depuis_data(benevole, data)
        type_map = canton_app.TYPE_FRAIS_MAP
        if data.get('nb_plusieurs', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['transp_plusieurs']),
                quantite=data['nb_plusieurs'],
            ))
        # Dès total sur année (1.1-30.12) > 6000kms, tarif différencié
        kms_below_6001, kms_over_6001 = split_kms_transport(benevole, self.mois, data['kms'])
        if kms_below_6001:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['kms_lt_6000']),
                quantite=kms_below_6001,
            ))
        if kms_over_6001:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['kms_gt_6000']),
                quantite=kms_over_6001,
            ))
        return lignes

    def get_note_frais_columns(self):
        return [
            ("Frais de repas", 'frais_repas'),
            ("Frais de parking", 'frais_divers'),
            ("Unités d’attente", 'nb_attente'),
            ("Unités transp. à plus.", 'nb_plusieurs'),
            ("Km (<=6000)", 'km_6000'),
            ("Km (>6000)", 'km_6001'),
        ]


    def get_note_frais_values(self, lignes):
        return {
            'frais_repas': self.sum_by_codes(lignes, [self.type_map['repas']]),
            'frais_divers': self.sum_by_codes(lignes, [self.type_map['divers']]),
            'nb_attente': self.get_by_code(lignes, self.type_map['transp_attente']),
            'nb_plusieurs': self.get_by_code(lignes, self.type_map['transp_plusieurs']),
            'km_6000': self.get_by_code(lignes, self.type_map['kms_lt_6000']),
            'km_6001': self.get_by_code(lignes, self.type_map['kms_gt_6000'], default=Decimal(0)),
        }


class FacturationPolicy(FacturationPolicyBase):
    FORFAIT_ALLER = Decimal('4.50')
    FORFAIT_ALLER_RETOUR = Decimal('9.00')
    FORFAIT_ANNULATION = {
        date(2000, 1, 1): Decimal('10.00'),
        date(2024, 1, 1): Decimal('25.00'),
    }
    # Cf. #355
    TRANSP_FACTURATION_STATUTS = {
        Transport.FacturationChoices.FACTURER,
        Transport.FacturationChoices.FACTURER_ANNUL,
        Transport.FacturationChoices.PAS_FACTURER,
    }
    TRANSP_ANNULATION_STATUTS = {
        Transport.FacturationChoices.FACTURER_ANNUL,
        Transport.FacturationChoices.PAS_FACTURER,
    }
    ATTENTE_HELP_TEXT = (
        "Attente depuis l’heure de rendez-vous client. À partir de 1h30, "
        "CHF 5/demi-heure pleine supplémentaire. Ex: si attente 2h, notez 2h. "
        "Ainsi, 30 min = CHF 5.- vous seront défrayés."
    )

    def tarif_km(self, transport):
        return TARIF_AVS if transport.client.is_avs(transport.date) else TARIF_NON_AVS

    def cout_attente(self, transport):
        """
        Facturation du temps d’attente pour chaque demi-heure dépassant 90 minutes.
        Renvoie (quantité, montant total).
        """
        if transport.temps_attente and transport.temps_attente.total_seconds() / 60 >= 90:
            num_periodes = int(((transport.temps_attente.total_seconds() / 60 - 90) // 30))
            return num_periodes, num_periodes * COUT_ATTENTE_UNITE
        return 0, Decimal('0')

    def cout_forfait(self, transport):
        if transport.est_annule():
            return 0
        return self.FORFAIT_ALLER_RETOUR if transport.retour else self.FORFAIT_ALLER

    def cout_annulation(self, transport):
        _cout_annul = 0
        for depuis, cout in self.FORFAIT_ANNULATION.items():
            if transport.date > depuis:
                _cout_annul = cout
        return _cout_annul

    def calculateur_frais(self, mois):
        return CalculateurFrais(mois)

    def export_frais_chauffeurs(self, liste_notes, un_du_mois):
        headers = [
            'Chauffeurs', 'Mois de la prestation',
            '602 - Nb km',
            '603 - Nb km >6000',
            "604 - Unités de Temps d'attente",
            '605 - Transport de plusieurs personnes (nb)',
            '606 - Indemnités effectives - Frais de repas (CHF)',
            '606 - Indemnités effectives - Frais divers (CHF)',
        ]

        export = DefraiementExport(col_widths=[43, 12, 10, 10, 8, 8, 8, 8, 8, 15])
        export.write_line(ExpLine(headers, bold=True, alignment=Alignment(text_rotation=90), height=325, borders='all'))
        export.ws.freeze_panes = export.ws['B2']
        export.ws.print_title_rows = '1:1'
        export.ws.page_setup.fitToPage = True
        export.ws.page_setup.fitToWidth = 1
        export.ws.page_setup.fitToHeight = 2
        export.ws.set_printer_settings(paper_size=export.ws.PAPERSIZE_A4, orientation='portrait')

        num_formats = {
            3: '0.00', 4: '0.00', 5: '0.00', 6: '0.00', 7: '0.00', 8: '0.00',
            9: '0.00',
        }
        for idx, note in enumerate(liste_notes, start=2):
            nb_attente = note.quantite_pour('NF-B12')
            frais_repas = note.somme_pour('NF-B07')
            frais_divers = note.somme_pour('NF-B06')
            nb_plusieurs = note.quantite_pour('NF-B13')
            kms_below_6000 = note.quantite_pour('NF-B10')
            kms_over_6000 = note.quantite_pour('NF-B11')
            export.write_line(ExpLine(
                [f'Transports {note.benevole.nom.upper()} {note.benevole.prenom}', un_du_mois,
                 kms_below_6000, kms_over_6000, nb_attente, nb_plusieurs,
                 frais_repas, frais_divers,
                ],
                bold=[1],
                number_formats=num_formats,
                height=27,
                borders='all'
            ))
        num_lines = len(liste_notes)
        export.write_line(ExpLine(
            ['Totaux', ''] + [
                f'=SUM({get_column_letter(col)}2:{get_column_letter(col)}{num_lines + 1})' for col in range(3, 9)
            ],
            bold=True,
            number_formats=num_formats,
            height=27,
            borders='all'
        ))
        return export


def split_kms_transport(chauffeur, mois, kms):
    """
    Renvoie les kilomètres effectués et défrayés depuis le 1.12 de l'année
    précédente de `mois` jusqu'à `mois` (non compris),  sous forme de tuple:
    (<kms_jusqu'a 6000>, <kms_au_dessus_de_6000>)
    """
    km_since = date(mois.year - 1, 12, 1) if mois.month < 12 else mois
    kms_avant_ce_mois = chauffeur.notefrais_set.filter(
        service='transport', mois__range=(km_since, mois - timedelta(days=4))
    ).aggregate(total_kms=Sum('kms'))['total_kms'] or Decimal('0')
    kms_over_6001 = kms_avant_ce_mois + kms - max(6000, kms_avant_ce_mois)
    kms_over_6001 = kms_over_6001 if kms_over_6001 > 0 else Decimal('0')
    kms_below_6001 = kms - kms_over_6001
    return kms_below_6001, kms_over_6001
