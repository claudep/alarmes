import codecs
import csv
import logging
from datetime import date
from functools import partial
from io import StringIO

from openpyxl import load_workbook

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.db import transaction
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import SafeString
from django.views.generic import FormView, View

from benevole.models import Benevole, NoteFrais
from client.models import (
    AdresseClient, Alerte, Client, Journal, Persona, Prestation, Telephone, client_saved
)
from common.choices import Services
from common.fields import PhoneCharField
from common.utils import canton_app, current_app, read_date
from .forms import ClientImportForm
from .utils import rue_changed

logger = logging.getLogger('api')

phone_field = PhoneCharField()


def get_api(output=None):
    if settings.CID_API_URL:
        from api.core import CIDApi
        return CIDApi(output=output)
    return None


class FacturesTransmissionView(View):
    """Envoi des factures à l'ERP. Il peut s'agir:
        - d'une facture individuelle si `fact_id` est présent.
        - de toutes les factures d'un client si `client` est présent.
        - sinon, envoi de toutes les factures pas encore transmises.
    """
    def post(self, request, *args, **kwargs):
        if kwargs['app'] == 'alarme':
            from alarme.models import Facture
        elif kwargs['app'] == 'transport':
            from transport.models import Facture
        elif kwargs['app'] == 'ape':
            from ape.models import Facture

        fact_pk = request.POST.get('fact_id')
        client = request.POST.get('client')
        factures = []
        if fact_pk:
            factures = [get_object_or_404(Facture, pk=fact_pk)]
        else:
            if client:
                query = Facture.non_transmises().filter(client__pk=client).order_by('mois_facture')
            else:
                query = Facture.non_transmises()
                if not query.ordered:
                    query = query.order_by(
                        'client__persona__nom', 'client__persona__prenom', 'client', 'mois_facture'
                    )
            factures = list(query)

        if len(factures) == 0:
            messages.error(request, "Aucune facture à envoyer n’a été trouvée.")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

        success, errors = self.envoi_factures(factures)
        result = {
            'result': 'error' if errors else 'OK',
            'success': success,
            'errors': [str(err) for err in errors],
        }
        if request.accepts('text/html'):
            if fact_pk and len(errors) == 1:
                messages.error(request, f"Erreur d’envoi de facture : {errors[0]}")
            elif errors:
                messages.error(request, "%d facture(s) ont produit des erreurs" % len(errors))
            if success > 0:
                if success == 1:
                    msg = "1 facture a été transmise avec succès"
                else:
                    msg = "%d factures ont été transmises avec succès" % success
                messages.success(request, msg)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return JsonResponse(result)

    def envoi_factures(self, fact_list):
        app = self.kwargs['app']
        factures = []
        if app == 'alarme':
            # Group Factures with similar client/date_facture/debiteur
            for fact in fact_list:
                fact._adresse_facturation = fact.client.adresse_facturation(fact)
                if (
                    factures and factures[-1][0].client == fact.client and groupable(fact, factures[-1][0])
                ):
                    factures[-1].append(fact)
                else:
                    factures.append([fact])
        else:
            factures = [[fact] for fact in fact_list]
        api = get_api()
        errors = []
        success = 0
        for facture in factures:
            try:
                result = api.create_invoice(app, facture)
            except Exception as exc:
                #traceback.print_exc()
                facture[0].export_err = str(exc)
                facture[0].save(update_fields=['export_err'])
                logger.error(facture[0].export_err)
                errors.append(exc)
                continue
            if 'error' in result:
                facture[0].export_err = f"Erreur API CID {result['error']['code']}: {result['error']['message']}"
                facture[0].save(update_fields=['export_err'])
                logger.error(facture[0].export_err)
                errors.append(facture[0].export_err)
            else:
                ext_id = result.get("id")
                no_fact = result.get("number")
                montant_total = result.get("totalamount")
                for fact in facture:
                    fact.exporte = timezone.now()
                    if fact.export_err:
                        fact.export_err = ''
                    if ext_id:
                        fact.id_externe = ext_id
                    if no_fact:
                        fact.no = no_fact
                    if ext_id:
                        fact.id_externe = ext_id
                    if len(facture) == 1 and hasattr(fact, 'montant_total') and montant_total:
                        fact.montant_total = montant_total
                    fact.save()
                    success += 1
        return success, errors


def groupable(fact1, fact2):
    return (
        fact1.date_facture == fact2.date_facture and
        fact1._adresse_facturation == fact2._adresse_facturation
    )


class NoteFraisTransmissionView(View):
    def post(self, request, *args, **kwargs):
        self.api = get_api()
        self.errors = []
        self.success = 0
        if request.POST.get('note_id'):
            # Envoi note individuelle
            note = get_object_or_404(NoteFrais, pk=request.POST['note_id'])
            self._envoi_note(note)
        elif request.POST.get('note_benevole_id'):
            # Générer, puis envoyer note individuelle
            benevole = get_object_or_404(Benevole, pk=request.POST.get('note_benevole_id'))
            note_mois = date(int(request.POST.get('note_year')), int(request.POST.get('note_month')), 1)
            calculateur = canton_app.fact_policy.calculateur_frais(note_mois)
            calculateur.calculer_frais(benevole=benevole, enregistrer=True, refaire=False)
            note = calculateur.note_frais_pour(benevole)
            self._envoi_note(note)
        else:
            # Envoyer toutes les notes du mois
            un_du_mois = date(kwargs['year'], kwargs['month'], 1)
            calculateur = canton_app.fact_policy.calculateur_frais(un_du_mois)
            calculateur.calculer_frais(enregistrer=True, refaire=False)
            for note in calculateur.benevoles.values():
                if note.date_export:
                    continue
                self._envoi_note(note)

        result = {
            'result': 'error' if self.errors else 'OK',
            'success': self.success,
            'errors': [str(err) for err in self.errors],
        }
        if request.accepts('text/html'):
            for err in result['errors']:
                messages.error(request, f"Erreur: {err}")
            if self.success > 0:
                if self.success > 1:
                    msg = f"{self.success} notes de frais ont été transmises avec succès"
                else:
                    msg = "Une note de frais a été transmise avec succès"
                messages.success(request, msg)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return JsonResponse(result)

    def _envoi_note(self, note):
        try:
            result = self.api.send_expense(note)
        except Exception as exc:
            self.errors.append(exc)
            return
        if 'error' in result:
            msg = f"Erreur API CID {result['error']['code']}: {result['error']['message']}"
            self.errors.append(msg)
            return

        # WARNING: once a note is sent, an exception should NOT revert the current transaction
        # Envoi note
        note.date_export = timezone.now()
        note.id_externe = result['id']
        # result also contains 'totalamount', not saved currently.
        note.save(update_fields=['date_export', 'id_externe'])
        self.success += 1

    def envoi_frais_alarme(self, mois):
        calculateur = canton_app.fact_policy.calculateur_frais(mois)
        calculateur.calculer_frais(enregistrer=True, refaire=False)
        for note in calculateur.benevoles.values():
            if note.date_export:
                continue
            self._envoi_note(note)

    def envoi_frais_transport(self, mois):
        from transport.utils import generer_notes_frais

        generer_notes_frais(mois)

        liste_notes = NoteFrais.objects.filter(
            service='transport', mois=mois, date_export=None
        ).prefetch_related('lignes')
        for note in liste_notes:
            self._envoi_note(note)


class ImportOSADClients(PermissionRequiredMixin, FormView):
    permission_required = "client.add_client"
    form_class = ClientImportForm
    template_name = 'besoins/client_import.html'
    lang_map = {
        '': [],
        'Allemande': ['de'],
        'Italienne': ['it'],
        'Turque': ['tr'],
    }
    expected_headers = {
        'NOM', 'PRENOM', 'Sexe', 'NP / Ville', 'Rue', 'Téléphone', 'Portable', 'N° Client', 'NE(E) LE'
    }

    def get_success_url(self):
        if self.dry_run:
            return reverse('import_osad')
        else:
            return reverse('besoins:home')

    def iterate_file(self, client_file):
        if client_file.name.endswith('.csv'):
            file_content = client_file.read()
            encoding = 'utf-8-sig' if file_content.startswith(codecs.BOM_UTF8) else 'latin-1'
            buf = StringIO(file_content.decode(encoding))
            dialect = csv.Sniffer().sniff(buf.read(2048))
            buf.seek(0)
            reader = csv.DictReader(buf, dialect=dialect)
            for idx, line in enumerate(reader):
                yield idx, line
        if client_file.name.endswith('.xlsx'):
            wb = load_workbook(client_file)
            ws = wb.active
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                yield idx, values

    def form_valid(self, form):
        self.dry_run = form.cleaned_data['dry_run']
        osad_pks = list(
            Client.objects.par_service(Services.OSAD).values_list('pk', flat=True)
        )
        self.errors = []
        self.rapport = {'new': [], 'archived': [], 'unarchived': [], 'modified': [], 'set_osad': []}
        try:
            with transaction.atomic():
                for idx, (no_line, row) in enumerate(self.iterate_file(form.cleaned_data['fichier'])):
                    if idx == 0:
                        if missing := self.expected_headers - set(row.keys()):
                            messages.error(
                                self.request,
                                f"Impossible d’importer, les en-têtes {missing} semblent absentes."
                            )
                            return HttpResponseRedirect(self.success_url)
                    if (
                        row.get('Actif', 'Oui') != 'Oui' or
                        row['NOM'] in ['ENTRETIEN ADV', 'Z-COLLOQUE', 'Z-FORMATION'] or
                        row['NOM'].lower().startswith(("test", "entretien", "évaluation", "zz"))
                    ):
                        continue
                    client = self.import_client(row, no_line + 1)
                    if client and client.pk in osad_pks:
                        osad_pks.remove(client.pk)
                for pk in osad_pks:
                    # Clients OSAD qui ne sont plus dans le fichier d'importation
                    client = Client.objects.get(pk=pk)
                    if not self.dry_run:
                        client.archiver(
                            self.request.user, for_app='osad', check=False,
                            message="script importation OSAD"
                        )
                    self.rapport['archived'].append(client)
                if self.dry_run:
                    raise RuntimeError("Cancel the transaction")
        except RuntimeError:
            pass

        if self.errors:
            messages.error(self.request, SafeString(
                "Les clients suivants n’ont pas pu être importés:<br>" + "<br>".join(self.errors)
            ))
        if self.dry_run:
            return render(self.request, 'besoins/client_import.html', {
                'form': form,
                'lignes_rapport': self.print_rapport(),
                'errors': self.errors,
            })
        else:
            messages.success(self.request, SafeString(
                "L’importation du fichier OSAD est terminée. Voici les modifications apportées:<br>" +
                "<br>".join(self.print_rapport())
            ))
            mail_admins("Importation OSAD terminée", "\n".join(self.print_rapport()))
            return HttpResponseRedirect(self.get_success_url())

    def print_rapport(self):
        lines = []
        if self.rapport['new']:
            lines.append("Nouveaux clients:")
            lines.extend([
                f" • Le nouveau client {cl} a été créé (ligne {lig})" for cl, lig in self.rapport['new']
            ] + [''])
        if self.rapport['modified']:
            lines.append("Clients modifiés:")
            lines.extend([
                f" • Le client {cl} a été modifié (ligne {lig}): {', '.join(changes)}" for cl, lig, changes in self.rapport['modified']
            ] + [''])
        if self.rapport['set_osad']:
            lines.append("Ajout service OSAD pour clients:")
            lines.extend([
                f" • Le client {cl} a été marqué comme client OSAD (ligne {lig})" for cl, lig in self.rapport['set_osad']
            ] + [''])
        if self.rapport['archived']:
            lines.append("Clients archivés:")
            lines.extend([
                f" • Le client {cl} a été archivé" for cl in self.rapport['archived']
            ] + [''])
        if self.rapport['unarchived']:
            lines.append("Clients désarchivés:")
            lines.extend([f" • Le client {cl} a été désarchivé." for cl, lig in self.rapport['unarchived']])
        return lines

    def import_client(self, values, no_line):
        code_postal, localite = values['NP / Ville'].split(maxsplit=1)
        no_medlink = clean(values['N° Client'])
        date_naissance = read_date(values['NE(E) LE'])
        try:
            client = Client.objects.avec_adresse(date.today()).get(persona__id_externe2=no_medlink)
        except Client.DoesNotExist:
            try:
                client = Client.objects.avec_adresse(date.today()).get(
                    persona__nom__unaccent__iexact=values['NOM'],
                    persona__prenom__unaccent__iexact=values['PRENOM'],
                    adresse_active__npa=code_postal,
                    persona__date_naissance=date_naissance,
                )
            except Client.DoesNotExist:
                client = None
            else:
                client.persona.id_externe2 = no_medlink
                client.persona.save(update_fields=['id_externe2'])

        rue = clean(values['Rue'])
        if not rue:
            self.errors.append(f"Le client {values['NOM']} {values['PRENOM']} n’a pas de rue.")
            return
        rue = rue[0].upper() + rue[1:]
        try:
            tel1 = clean_tel(values['Téléphone'])
            tel2 = clean_tel(values['Portable'])
        except ValidationError as err:
            self.errors.append(f"Le client {values['NOM']} {values['PRENOM']} n’a pas pu être créé: {err}")
            return

        if client is not None:
            # Mise à jour client existant
            if client.archive_le:
                if not self.dry_run:
                    client.desarchiver(
                        self.request.user, Services.OSAD, message="script importation OSAD"
                    )
                self.rapport['unarchived'].append((client, no_line))

            other_types = set(client.prestations_actuelles(as_services=True)) - {Services.OSAD}
            # Mise à jour rue/npa/localité
            changed = {}
            adresse = client.adresse()
            persona = client.persona
            if (
                rue_changed(adresse.rue, rue) or adresse.npa.lower() != code_postal.lower()
                or adresse.localite.lower() != localite.lower()
            ):
                if not self.dry_run:
                    persona.adresses.nouvelle(rue=rue, npa=code_postal, localite=localite)
                changed['adresse'] = f"nouvelle adresse: «{rue}, {code_postal} {localite}»"
            new_tels = [tel for tel in [tel1, tel2] if tel]
            old_tels = persona.telephones()[:2]
            if (len(new_tels) < len(old_tels)) and len(other_types) > 0:
                # Ne pas mettre à jour téls si moins de numéros (#498)
                pass
            elif set(new_tels) != set([t.tel for t in old_tels]):
                if old_tels[0].tel != tel1:
                    changed["tel_1"] = f"tel_1: de «{old_tels[0].tel}» vers «{tel1}»"
                    if tel1:
                        old_tels[0].tel = tel1
                        if not self.dry_run:
                            old_tels[0].save()
                    else:
                        if not self.dry_run:
                            old_tels[0].delete()
                changed["tel_2"] = f"tel_2: de «{old_tels[1].tel if len(old_tels) > 1 else ''}» vers «{tel2}»"
                if tel2 and len(old_tels) > 1:
                    old_tels[1].tel = tel2
                    if not self.dry_run:
                        old_tels[1].save()
                elif tel2:
                    new_tel = Telephone(persona=persona, tel=tel2, priorite=2)
                    if not self.dry_run:
                        new_tel.save()

            if changed:
                self.rapport['modified'].append((client, no_line, changed.values()))
                if not self.dry_run:
                    persona.save(update_fields=[k for k in changed.keys() if k not in ["adresse", "tel_1", "tel_2"]])
                    journal = Journal.objects.create(
                        persona=persona, quand=timezone.now(), qui=self.request.user,
                        description=f"Mise à jour par importation MedLink: " + ", ".join(changed.values()),
                    )
                    transaction.on_commit(
                        partial(client_saved.send, sender=Client, instance=client, created=False)
                    )
                    for app in other_types:
                        Alerte.objects.create(
                            persona=client.persona, cible=app, recu_le=timezone.now(), alerte=journal.description
                        )

            # Si nécessaire, marquer le client comme client OSAD
            if not client.prestations_actuelles(Services.OSAD):
                if not self.dry_run:
                    Prestation.objects.create(client=client, service=Services.OSAD, duree=(date.today(), None))
                    Journal.objects.create(
                        persona=persona,
                        description="Ajout comme client OSAD par script d’importation",
                        qui=self.request.user,
                        quand=timezone.now()
                    )
                self.rapport['set_osad'].append((client, no_line))
            return client

        # Création nouveau client
        langues = self.lang_map.get(values.get('Langue parlée', ''), ['fr'])
        persona = Persona(
            cree_le=timezone.now(),
            id_externe2=no_medlink,
            nom=clean(values['NOM']).title(), prenom=values['PRENOM'], langues=langues,
            genre=values['Sexe'] == "Féminin" and 'F' or 'M',
            date_naissance=date_naissance,
            courriel=clean(values.get('Email', '')),
        )
        try:
            persona.full_clean()
        except ValidationError as err:
            self.errors.append(f"Le client {persona} n’a pas pu être créé: {err}")
            return
        if not self.dry_run:
            persona.save()
            persona.adresses.nouvelle(rue=rue, npa=code_postal, localite=localite)
            if tel1 or tel2:
                Telephone.objects.create(persona=persona, tel=tel1 or tel2, priorite=1)
            if tel1 and tel2:
                Telephone.objects.create(persona=persona, tel=tel2, priorite=2)
            client = Client.objects.create(persona=persona)
            Prestation.objects.create(client=client, service=Services.OSAD, duree=(date.today(), None))
            Journal.objects.create(
                persona=persona,
                description="Création client par script d’importation (OSAD)",
                qui=self.request.user,
                quand=timezone.now()
            )
            transaction.on_commit(
                partial(client_saved.send, sender=Client, instance=client, created=True)
            )
        self.rapport['new'].append((persona, no_line))
        return client


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val


def clean_tel(val):
    if val is None:
        return ''
    num = "".join([c for c in val if c.isdigit() or c in [' ', '+']]).strip()
    return phone_field.clean(num) if num else num
