rue_ignore_mapping = {
    # rue officielle (début): rue selon Google
    "rue docteur lerch": "rue du dr-lerch",
    "neuve": "rue neuve",
    "rue du pont de vaux": "rue de pont-de-vaux",
    "faubourg": "fbg",
    "grand'rue": "grand-rue",
    "treytel": "loge de treytel",
    "rue du 1er mars": "rue du premier-mars",
    "cité-suchard": "passage de cité-suchard",
    "chemin des petits-chênes": "rue des petits-chênes",
    "verger-en-joran": "ch. du verger-en-joran",
    "rue du docteur-de-quervain": "rue du dr. de quervain",
}

def rue_changed(rue1, rue2):
    """
    Détecter si les rues/no sont différents ou si les modifications sont non significatives.
    rue1 est la rue actuellement en BD.
    """
    if rue1.lower().replace(' ', '') == rue2.lower().replace(' ', ''):
        return False
    for current, new in rue_ignore_mapping.items():
        if rue1.lower().startswith(current) and rue2.lower().startswith(new):
            return rue_changed(
                rue1.lower().removeprefix(current), rue2.lower().removeprefix(new)
            )
    return True
