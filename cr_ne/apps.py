from importlib import import_module
from unittest.mock import Mock

from django.apps import AppConfig, apps
from django.contrib import admin, messages
from django.http import JsonResponse


@admin.action(description="Afficher le contenu JSON de cette facture")
def show_facture_json(modeladmin, request, queryset):
    """Affiche le code JSON envoyé à l'API pour cette facture."""
    from .views import get_api

    if queryset.count() != 1:
        modeladmin.message_user(request, "Veuillez sélectionner une et une seule facture.", messages.ERROR)
        return
    fact = queryset.first()
    json = None

    def out_collector(*args, **kwargs):
        nonlocal json
        json = kwargs.get("json")
        return Mock()

    api = get_api(output=out_collector)
    api.create_invoice("transport", [fact])
    return JsonResponse(json, safe=False)


class CRNEAppConfig(AppConfig):
    name = 'cr_ne'
    verbose_name = "Croix-Rouge Neuchâtel"
    abrev = "NE"
    client_profil_form_class = None
    EXTRA_LOGEMENT_CHOICES = (('ape', "ApE contrat CRN"),)
    TYPE_FRAIS_MAP = {
        'repas': 'NF-B07',
        'transports_pub': 'NF-B05',
        'divers': 'NF-B06',
        'forfait_tel': 'NF-B51',
        'kms': 'NF-B01',
        'transp_attente': 'NF-B12',
        'transp_plusieurs':  'NF-B13',
        'kms_lt_6000': 'NF-B10',
        'kms_gt_6000': 'NF-B11',
    }
    # Les clients ayant ce type de logement sont exclus de l'analyse des besoins
    BESOINS_TYPE_LOGEMENT_EXCLUDE = ['home']
    # Tri des clients besoins à questionner
    BESOINS_CLIENT_ORDERING = ["debut_prest", "persona__nom"]
    pdf_doc_registry = {
        'courrier_repondant': {
            'title': "Courrier pour le répondant",
            'class': "alarme.pdf.CourrierReferentPDF",
            'input': "contact-est_repondant",
        },
        'courrier_debiteur': {
            'title': "Courrier pour adresse de facturation",
            'class': "cr_ne.pdf.CourrierDebiteurPDF",
            'input': "contact-facturation_pour",
        },
    }

    def ready(self):
        if apps.is_installed('alarme'):
            from alarme.forms import ClientFilterForm
            from . import alarme_policies
            from .client_filters import ClientsSamaFilter

            ClientFilterForm.add_filter('samas', ClientsSamaFilter())
            self.fact_policy = alarme_policies.FacturationPolicy()

        if apps.is_installed('transport'):
            from transport.admin import FactureAdmin
            from transport.forms import SaisieTransportForm
            from . import transport_policies

            self.fact_policy = transport_policies.FacturationPolicy()
            FactureAdmin.actions += (show_facture_json,)
            # Exclure choix LAA des types de transport
            SaisieTransportForm.declared_fields['typ'].choices = [
                c for c in SaisieTransportForm.declared_fields['typ'].choices
                if c[0] != "laa"
            ]


        if apps.is_installed('visite'):
            from visite.models import FacturationPolicy

            self.fact_policy = FacturationPolicy()

    def pdf_headfoot_class(self):
        try:
            return self.module.pdf.PDFHeaderFooter
        except AttributeError:
            import_module(f'{self.name}.pdf')
            return self.module.pdf.PDFHeaderFooter
