
class ERPSender {
    constructor(form) {
        this.clientWithErrors = [];
        this.form = form;
        this._continue = true;
    }

    async send() {
        // Take first client in list
        let trs = document.querySelectorAll("table.facture-list tbody tr");
        let clientTr = Array.from(trs).find(tr => (tr.dataset.clientid && !this.clientWithErrors.includes(tr.dataset.clientid)));
        if (typeof clientTr === 'undefined' || !this._continue) return;
        const result = await this.sendNextToERP(clientTr.dataset.clientid);
        if (result == 'error') this.clientWithErrors.push(clientTr.dataset.clientid);
        if (result == 'crash') {
            alert("Une erreur de serveur a interrompu l’envoi des factures. Réessayez plus tard.");
            return;
        }
        await this.send();
    }

    async sendNextToERP(clientId) {
        let formData = new FormData();
        formData.append('csrfmiddlewaretoken', document.querySelector('[name=csrfmiddlewaretoken]').value);
        formData.append('client', clientId);
        let url = this.form.action;
        console.log(`POSTing url ${url}`);
        try {
            const resp = await fetch(url, {
                method: 'POST',
                headers: {'Accept': 'application/json'},
                body: formData
            });
            const data = await resp.json();
            console.log(data.result);
            if (data.result == 'OK') {
                // Ajax-refresh list of factures
                const resp2 = await fetch(location.href);
                const html = await resp2.text();
                var parser = new DOMParser();
                var doc = parser.parseFromString(html, "text/html");
                document.querySelector('.facture-list tbody').replaceWith(doc.querySelector('.facture-list tbody'));
                document.querySelector('.list-count').replaceWith(doc.querySelector('.list-count'));
                document.querySelector('#pagination').replaceWith(doc.querySelector('#pagination'));
            }
            return data.result
        } catch(err) {
            return 'crash';
        }
    }
}


window.addEventListener('DOMContentLoaded', () => {
    const envoiFrm = document.querySelector('#envoi-factures');
    if (envoiFrm) {
        let sender = null;
        const stopBtn = envoiFrm.querySelector('.stop-op');
        envoiFrm.addEventListener('submit', async (ev) => {
            ev.preventDefault();
            sender = new ERPSender(ev.target);
            if (stopBtn) stopBtn.removeAttribute('hidden');
            await sender.send();
            envoiFrm.querySelector('#loader-1').remove();
            envoiFrm.querySelector('button[type="submit"]').disabled = false;
            if (stopBtn) stopBtn.setAttribute('hidden', true);
        });
        if (stopBtn) stopBtn.addEventListener('click', async (ev) => {
            sender._continue = false;
        });
    }
});
