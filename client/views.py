from datetime import date, timedelta

from django.apps import apps
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Case, Count, Max, Q, Value, When
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.views.generic import (
    DeleteView, DetailView, FormView, TemplateView, UpdateView, View
)

from common.distance import distance_real, distance_vo
from common.export import ExpLine
from common.models import TypeFichier
from common.utils import canton_app, current_app
from common.views import (
    CreateUpdateView, ExportableMixin, FichierDeleteView, FichierEditView,
    FichierListeView, FilterFormMixin, GeoAddressMixin, JournalMixin, ListView,
)
from . import forms
from .models import (
    AdresseClient, AdressePresence, Alerte, Client, Journal, Persona, Prestation,
    Professionnel, ProfClient, Referent, referent_saved
)


class ClientAccessCheckMixin:
    profile_model = Client

    def dispatch(self, request, *args, **kwargs):
        if 'pk' not in self.kwargs:
            self.client = None
            self.read_only = False
        else:
            self.client = get_object_or_404(self.profile_model, pk=self.kwargs['pk'])
            if self.client.can_edit(request.user):
                self.read_only = False
            elif self.client.can_read(request.user):
                self.read_only = True
            else:
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour voir ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'readonly': self.read_only}


class ClientDetailContextMixin:
    app_tabs_methods = {}  # registry for customized per-app tabs

    def get_tabs(self):
        client = self.client
        tabs = [
            {'id': 'general', 'title': "Général", 'url': client.get_absolute_url()},
            {'id': 'journal', 'title': "Journal", 'url': reverse('client-journal', args=[client.pk])},
        ]
        if (
            apps.is_installed('besoins') and self.request.user.has_perm('besoins.view_questionnaire')
            and client.is_avs()
        ):
            quest_a_faire = (
                client.questionnaire_set.model.clients_a_questionner().filter(pk=client.pk).exists()
            )
            tabs.append({
                'id': 'besoins', 'title': "Besoins", 'quest_a_faire': quest_a_faire,
                'url': reverse('besoins:resume', args=[client.pk])
            })
        # Call function from current app
        if current_app() in self.app_tabs_methods:
            tabs = self.app_tabs_methods[current_app()](self, client, tabs)
        return tabs

    def get_context_data(self, **kwargs):
        if not self.client or not self.client.pk:
            return super().get_context_data(**kwargs)
        return {
            **super().get_context_data(**kwargs),
            "tabs": self.get_tabs(),
            "dans_service_actuel": current_app() in self.client.prestations_actuelles(as_services=True),
        }


class PersonaJournalMixin(JournalMixin):
    def _create_instance(self, **kwargs):
        if self.object is not None:
            assert isinstance(self.object, Persona)
            Journal.objects.create(persona=self.object, **kwargs)


class ClientJournalMixin(JournalMixin):
    def _create_instance(self, **kwargs):
        client = self.get_client()
        if client is not None:
            Journal.objects.create(persona=client.persona, **kwargs)


class ClientListViewBase(PermissionRequiredMixin, ExportableMixin, FilterFormMixin, ListView):
    model = Client
    template_name = 'client/client_list_base.html'
    permission_required = 'client.view_client'
    base_template = None
    is_archive = False
    paginate_by = 25
    client_libelles = ("client", "clients")
    filter_formclass = forms.ClientFilterFormBase
    col_widths = [15, 30, 5, 12, 26, 10, 6, 22, 15, 15, 20, 12, 12]

    def get_queryset(self):
        if self.is_archive:
            # Archivés: au moins une prest du service, mais plus de prestation active
            query = super().get_queryset().annotate(
                num_prest_service=Count('prestations', filter=Q(prestations__service__in=self.client_types)),
                num_prest_service_active=Count('prestations', filter=(
                    Q(prestations__service__in=self.client_types) &
                    Q(prestations__duree__contains=date.today())
                )),
            ).avec_adresse(date.today()).filter(
                num_prest_service__gt=0, num_prest_service_active=0
            )
        else:
            query = super().get_queryset().par_service(self.client_types).avec_adresse(date.today())
        return query.prefetch_related("persona__telephone_set").order_by("persona__nom", "persona__prenom")

    def get_title(self):
        return f"Liste des {self.client_libelles[1]}"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': self.get_title(),
            'libelles': self.client_libelles,
            'exportable': True,
            'base_template': context.get('base_template', self.base_template),
            'active': 'archives' if self.is_archive else 'actifs',
            'count': context['paginator'].count,
            'num_alertes': Alerte.objects.filter(
                cible=current_app(), traite_le__isnull=True
            ).count(),
        })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.filter_form and self.filter_form.active_filter and self.filter_form.active_filter.export_only:
            self.export_flag = True
        return super().render_to_response(context, **response_kwargs)

    def export_lines(self, context):
        queryset = self.get_queryset().avec_adresse(date.today())
        if not "services" in queryset.query.annotations:
            try:
                queryset = queryset.avec_services()
            except AttributeError:
                # Ex. donateurs
                pass
        client_filter = self.filter_form.active_filter if self.filter_form else None
        headers = [
            'Secteurs', 'Nom, Prénom', 'Genre', 'Date de naissance', 'Rue', 'Case postale',
            'NPA', 'Localité', 'Tél. principal', 'Tél. secondaire', 'Courriel', 'Archivé le', 'Décès le',
            'No externe',
        ]
        if client_filter:
            headers.extend(client_filter.extra_headers())
        yield ExpLine(headers, bold=True)
        for idx, client in enumerate(queryset):
            adresse = client.adresse() or AdresseClient(rue="", npa="", localite="")
            persona = client.persona
            tels = persona.telephones()
            line = [
                "" if self.is_archive else ", ".join(getattr(client, "services", [])),
                client.nom_prenom, persona.genre,
                persona.date_naissance, adresse.rue, persona.case_postale,
                adresse.npa, adresse.localite,
                tels[0].tel if tels else "", tels[1].tel if len(tels) > 1 else "",
                persona.courriel,
                client.archive_le, persona.date_deces,
                persona.id_externe or getattr(client, "no_debiteur", ""),
            ]
            if client_filter:
                line.extend(client_filter.extra_values(client))
            yield line


class ClientAlertesView(ListView):
    model = Alerte
    template_name = 'client/client_alertes.html'
    paginate_by = 25

    def get_queryset(self):
        return super().get_queryset().filter(
            cible=current_app(), traite_le__isnull=True
        ).order_by('-recu_le')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['num_alertes'] = context['paginator'].count
        return context


class ClientFromPersonaView(TemplateView):
    template_name= "client/client_from_persona.html"

    def post(self, request, *args, **kwargs):
        persona = get_object_or_404(Persona, pk=self.kwargs["pers_pk"])
        with transaction.atomic():
            client = Client.objects.create(persona_id=self.kwargs["pers_pk"])
            Prestation.objects.create(client=client, service=current_app(), duree=(date.today(), None))
            Journal.objects.create(
                persona=client.persona, description=f"Création du client «{current_app()}»",
                quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(client.get_absolute_url())

    def get_context_data(self, **kwargs):
        persona = get_object_or_404(Persona, pk=self.kwargs["pers_pk"])
        return {**super().get_context_data(**kwargs), "persona": persona, "app": current_app()}


class ClientEditViewBase(ClientAccessCheckMixin, ClientJournalMixin, CreateUpdateView):
    model = Client
    form_class = None
    template_name = "client/client_edit_base.html"
    base_template = None
    client_type = None  # Services.ALARME/TRANSPORT/etc.
    client_libelles = ("client", "clients")
    tab_id = 'general'
    journal_add_message = "Création du client"
    journal_edit_message = "Modification du client: {fields}"

    def get_initial(self):
        if self.is_create:
            return {'langues': ['fr']}
        return super().get_initial()

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'readonly': self.read_only or (self.object and self.object.archive_le is not None),
            'user_is_benev': self.request.user.is_benevole,
        }

    def get_client(self):
        return self.object

    def get_success_url(self):
        return reverse('client-edit', args=[self.object.pk])

    def get_success_message(self, obj):
        if self.is_create:
            return "La création d’un nouveau client a réussi."
        else:
            return f"«{obj.nom_prenom}» a bien été modifié·e"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'base_template' not in context:
            context['base_template'] = self.base_template
        context['libelles'] = self.client_libelles
        if not self.is_create:
            if not hasattr(self.object, "services"):
                self.object.services = self.object.prestations_actuelles(as_services=True)
            context.update({
                'user_is_benev': self.request.user.is_benevole,
                'adresse': self.object.adresse(date.today()),
                'adresse_future': self.object.persona.adresseclient_set.filter(principale__startswith__gt=date.today()).first(),
                'adresses': self.object.persona.adresses.autres(),
                'can_be_archived': self.object.can_be_archived(self.request.user, self.client_type),
                'professionnels': self.object.profclient_set.exclude(
                    professionnel__type_pro='Sama').select_related('professionnel'),
                'samas_count': self.object.profclient_set.filter(professionnel__type_pro='Sama').count(),
                'fichiers': self.object.persona.fichiers_app(current_app()).order_by('-quand'),
                'services': self.object.services,
                'dans_service_actuel': current_app() in self.object.services,
            })
            context.update(self.object.contacts_as_context())
        return context

    def form_valid(self, form):
        client = form.instance
        prestations = client.prestations_actuelles(as_services=True)
        with transaction.atomic():
            response = super().form_valid(form)
            if self.is_create:
                Prestation.objects.create(client=self.object, service=self.client_type, duree=(date.today(), None))
            forms.notify_other_teams(form, client, from_app=self.client_type)
        return response


class ClientAddServiceView(ClientAccessCheckMixin, DetailView):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs["pk"])
        service = current_app()
        if service in client.prestations_actuelles(as_services=True):
            messages.error(request, f"Cette personne est déjà définie comme cliente «{service}»")
        else:
            Prestation.objects.create(client=client, service=service, duree=(date.today(), None))
            Journal.objects.create(
                persona=client.persona, description=f"Ajout type de client «{service}»",
                quand=timezone.now(), qui=self.request.user
            )
            messages.success(self.request, f"Cette personne a bien été ajoutée comme cliente «{service}»")
        return HttpResponseRedirect(request.headers.get('Referer', reverse('clients')))


class ClientAutocompleteView(View):
    """Endpoint for autocomplete search for Client."""

    def get_queryset(self, term_nom, term_prenom=None):
        filters = Q(persona__date_deces__isnull=True) & Q(persona__nom__unaccent__icontains=term_nom)
        if term_prenom:
            filters &= Q(persona__prenom__unaccent__istartswith=term_prenom)
        client_query = Client.objects.filter(filters).annotate(
            archive=Case(When(archive_le__isnull=True, then=Value(False)), default=Value(True)),
        )
        if self.kwargs["types"] != "tous":
            client_query = client_query.par_service(self.kwargs["types"])
        else:
            client_query = client_query.avec_services()
        return client_query.avec_adresse(date.today()).order_by(
            "archive", "persona__nom", "persona__prenom"
        )[:20]

    def get(self, request, *args, **kwargs):
        term = request.GET.get("q", "").strip()
        if not term:
            raise Http404("Le terme de recherche est obligatoire")

        clients = self.get_queryset(term)
        if " " in term and not clients:
            # On suppose que la recherche après l'espace concerne le prénom
            term_nom, term_prenom = term.split(maxsplit=1)
            clients = self.get_queryset(term_nom, term_prenom)

        results = [{
            'label': str(client), 'value': client.pk,
            'adresse': (
                f"{client.adresse_active['rue']}, {client.adresse_active['npa']} "
                f"{client.adresse_active['localite']}"
            ) if client.adresse_active else '',
            'types': client.services,
            'url': client.get_absolute_url(),
            'archive': client.archive_le is not None,
        } for client in clients]
        return JsonResponse(results, safe=False)


class ClientJournalView(ClientAccessCheckMixin, ClientDetailContextMixin, FilterFormMixin, ListView):
    model = Journal
    filter_formclass = forms.JournalFilterForm
    paginate_by = 20
    template_name = 'client/client_journal.html'

    def get_filterform_kwargs(self):
        return {'client': self.client}

    def get_queryset(self):
        qs = super().get_queryset(base_qs=self.client.journaux.order_by('-quand'))
        if self.read_only and not self.request.user.has_perm("client.add_journal"):
            qs = qs.filter(quand__date__gte=date.today() - timedelta(days=180))
        return qs

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientJournalEditView(ClientAccessCheckMixin, CreateUpdateView):
    model = Journal
    form_class = forms.JournalForm
    pk_url_kwarg = 'pk_jr'
    json_response = True

    def form_valid(self, form):
        form.instance.persona = self.client.persona
        form.instance.qui = self.request.user
        form.instance.auto = False
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientJournalDetailsView(ClientAccessCheckMixin, DetailView):
    model = Journal
    pk_url_kwarg = 'pk_jr'
    template_name = 'client/journal_details.html'


class ClientJournalDeleteView(DeleteView):
    model = Journal
    pk_url_kwarg = 'pk_jr'

    def form_valid(self, form):
        if self.object.qui != self.request.user:
            raise PermissionDenied("Vous ne pouvez supprimer que vos propres notes.")
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class AdressePrincipaleEditView(UpdateView):
    model = AdresseClient
    pk_url_kwarg = "pk_adr"
    form_class = forms.PersonaAdresseForm
    template_name = 'general_edit.html'

    def dispatch(self, *args, **kwargs):
        adresse = self.get_object()
        if not adresse.persona.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier cette personne.")
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'client': self.object.persona}

    def form_valid(self, form):
        with transaction.atomic():
            if form.has_changed():
                self.object, message = form.save()
                persona = self.object.persona
                Journal.objects.create(
                    persona=persona, description=message, quand=timezone.now(), qui=self.request.user
                )
                if client:= persona.get_client():
                    client.notifier_autres_equipes(current_app(), message)
        return JsonResponse({'result': 'OK', 'reload': '#clientAdresseBloc'})


class AdresseCheckGeolocation(View):
    def post(self, request, *args, **kwargs):
        adresse = get_object_or_404(AdresseClient, pk=kwargs["pk_adr"])
        result = adresse.geolocalize()
        if result:
            resp = {"result": "OK", "message": "La géolocalisation de l’adresse a réussi"}
        else:
            resp = {"result": "Error", "message": "La géolocalisation de l’adresse a échoué"}
        return JsonResponse(resp)


class ClientAdresseEditView(ClientJournalMixin, CreateUpdateView):
    model = AdresseClient
    form_class = forms.AdresseForm
    template_name = 'client/adresse_edit.html'
    pk_url_kwarg = 'pk_adr'
    journal_add_message = "Ajout de l’adresse «{obj}»"
    journal_edit_message = "Modification de l’adresse «{obj}»: {fields}"
    json_response = True

    def get_client(self):
        client = self.object.persona.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def journalize(self, form, **kwargs):
        if form.cleaned_data['hospitalisation']:
            message = None
            for pres_form in form.formset.forms:
                if not pres_form.has_changed():
                    continue
                message = pres_form.get_changed_string()
                self._create_instance(
                    description=message, quand=timezone.now(), qui=self.request.user
                )
                num = self.get_client().notifier_autres_equipes(current_app(), message)
                if num > 0:
                    messages.success(self.request, "Le ou les autres services concernés ont été alertés.")
            if message is not None:
                return
        super().journalize(form, **kwargs)

    def form_valid(self, form):
        client = self.get_client()
        form.instance.persona = client.persona
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': '#client-addresses'})


class PersonaAdresseDeleteView(DeleteView):
    model = AdresseClient
    pk_url_kwarg = 'pk_adr'

    def form_valid(self, form):
        if not self.object.persona.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier cette personne.")
        with transaction.atomic():
            if self.object.principale is not None:
                if self.object.principale.lower < date.today():
                    raise PermissionDenied("Il n’est pas permis de supprimer une adresse active.")
                # Remettre à None la limite supérieure de l'adresse précédente
                prev_adresse = self.object.persona.adresseclient_set.exclude(
                    Q(principale=None) | Q(pk=self.object.pk)
                ).order_by('-principale__startswith').first()
                prev_adresse.principale = (prev_adresse.principale.lower, None)
                prev_adresse.save()
            self.object.delete()
            Journal.objects.create(
                persona=self.object.persona, description=f"Suppression de l’adresse «{self.object}»",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': '#client-addresses'})


class PersonaAdresseListeView(TemplateView):
    template_name = 'client/adresse_list.html'

    def get_context_data(self, **kwargs):
        persona = get_object_or_404(Persona, pk=self.kwargs['pk'])
        if not persona.can_read(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour voir cette personne.")
        return {
            **super().get_context_data(**kwargs),
            'adresses': persona.adresses.autres(),
            'show_messages': True,
        }


class PersonaAutocompleteView(View):
    """Endpoint for autocomplete search for Persona."""
    def get_queryset(self, term_nom, term_prenom=None, date_naiss=None, npa_loc=None):
        filters = Q(date_deces__isnull=True)
        if term_nom:
            filters &= Q(nom__unaccent__icontains=term_nom)
        if term_prenom:
            filters &= Q(prenom__unaccent__istartswith=term_prenom)
        if date_naiss:
            filters &= Q(date_naissance=date_naiss)
        if npa_loc:
            npa = npa_loc.split()[0]
            filters &= Q(adresse_active__npa=npa)
        return Persona.objects.avec_adresse(date.today()).filter(filters).order_by(
            "nom", "prenom"
        )[:20]

    def get(self, request, *args, **kwargs):
        term_nom, term_prenom, date_naiss, npa_loc = [
            request.GET.get(key) for key in ["q", "prenom", "dateNaiss", "npaLoc"]
        ]
        if not term_nom and "prenom" not in request.GET:
            raise Http404("Le terme de recherche est obligatoire")
        pers_query = self.get_queryset(term_nom, term_prenom, date_naiss, npa_loc)
        if term_nom and " " in term_nom and not pers_query:
            # On suppose que la recherche après l'espace concerne le prénom
            term_nom, term_prenom = term_nom.split(maxsplit=1)
            pers_query = self.get_queryset(term_nom, term_prenom)
        results = []
        for persona in pers_query:
            if client := persona.get_client():
                url = client.get_absolute_url()
            else:
                url = reverse(
                    "donateur-from-persona" if current_app() == "crm" else "client-from-persona",
                    args=[persona.pk]
                )
            results.append({
                'label': str(persona), 'value': persona.pk,
                'adresse': (
                    f"{persona.adresse_active['rue']}, {persona.adresse_active['npa']} "
                    f"{persona.adresse_active['localite']}"
                ) if persona.adresse_active else '',
                'url': url,
            })
        return JsonResponse(results, safe=False)


class ReferentEditView(ClientJournalMixin, CreateUpdateView):
    model = Referent
    form_class = forms.ReferentForm
    pk_url_kwarg = 'pk_ref'
    template_name = 'client/client_referent.html'
    journal_add_message = "Ajout du contact «{obj}»"
    journal_edit_message = "Modification du contact «{obj}» ({fields})"
    json_response = True

    def get_client(self):
        client = self.object.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        client = self.get_client()
        if kwargs['instance'] is None and self.request.GET.get('copy_from'):
            fields_to_copy = [
                'id_externe', 'nom', 'prenom', 'salutation', 'complement', 'rue', 'npa', 'localite',
                'pays', 'courriel', 'remarque',
            ]
            # Données initiales copiées d'un autre contact existant
            copy_from = get_object_or_404(self.model, pk=self.request.GET.get('copy_from'))
            kwargs['initial']['formsets'] = [
                {'tel': tel.tel, 'priorite': tel.priorite, 'remarque': tel.remarque}
                for tel in copy_from.telephones()
            ]
            kwargs['instance'] = Referent()
            for fname in fields_to_copy:
                setattr(kwargs['instance'], fname, getattr(copy_from, fname))
        return {
            **kwargs,
            'client': client,
            'readonly': not client.can_edit(self.request.user),
            'user_is_benev': self.request.user.is_benevole,
            # Éviter conflits de noms de champs avec le formulaire client (en fond de page)
            'prefix': 'contact',
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'user_is_benev': self.request.user.is_benevole,
            'courriers_referents': canton_app.pdf_doc_registry,
        }

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': '#client-contacts'})


class ReferentDeleteView(DeleteView):
    model = Referent
    pk_url_kwarg = 'pk_ref'
    form_class = forms.DateArchiveForm

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        with transaction.atomic():
            self.object.date_archive = form.cleaned_data['date_archive']
            self.object.save()
            referent_saved.send(sender=Referent, instance=self.object, created=False)
            Journal.objects.create(
                persona=self.object.client.persona, description=f"Archivage du contact «{self.object}»",
                quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(self.object.client.get_absolute_url())


class ClientReferentListeView(ClientAccessCheckMixin, TemplateView):
    template_name = 'client/contact_list.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        context = {
            **super().get_context_data(**kwargs),
            'client': client,
            'samas_count': client.profclient_set.filter(professionnel__type_pro='Sama').count(),
        }
        context.update(client.contacts_as_context())
        return context


class ClientReferentReorderView(View):
    typ = None

    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        ref_pks = [('samas' if pk == 'samas' else int(pk)) for pk in request.POST.get('pks').split(',')]
        qset = {
            'référent': client.referents.filter(referent__isnull=False).order_by('referent'),
            'répondant': client.referents.filter(repondant__isnull=False).order_by('repondant')
        }[self.typ]
        old_order = [contact.get_full_name() for contact in qset]
        if self.typ == 'répondant' and client.samaritains:
            old_order.insert(client.samaritains - 1, "Samaritains")
        old_order = ', '.join(f"{idx + 1}. {el}" for idx, el in enumerate(old_order))
        for contact in qset:
            setattr(contact, self.typ.replace('é', 'e'), ref_pks.index(contact.pk) + 1)
            contact.save()
        if self.typ == 'répondant' and 'samas' in ref_pks:
            client.samaritains = ref_pks.index('samas') + 1
            client.save()
        new_order = [contact.get_full_name() for contact in qset.all()]
        if self.typ == 'répondant' and client.samaritains:
            new_order.insert(client.samaritains - 1, "Samaritains")
        new_order = ', '.join(f"{idx + 1}. {el}" for idx, el in enumerate(new_order))
        Journal.objects.create(
            persona=client.persona,
            description=(
                f"Changement de priorité des {self.typ}s.<br>"
                f"<i>Avant:</i> {old_order}.<br>"
                f"<i>Après:</i> {new_order}"
            ),
            quand=timezone.now(), qui=request.user
        )
        return JsonResponse({'result': 'OK'})


class ClientProfessionnelAddView(FormView):
    template_name = 'client/client_professionnel.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """One form for Professionnel selection and one for Professionnel creation."""
        # We do not call super as get_form would crash
        return {
            'client': self.client,
            'form_chose': (
                kwargs['form'] if isinstance(kwargs.get('form'), forms.ProfessionnelSelectForm)
                else forms.ProfessionnelSelectForm()
            ),
            'form_create': (
                kwargs['form'] if isinstance(kwargs.get('form'), forms.ProfessionnelClientForm)
                else forms.ProfessionnelClientForm()
            ),
        }

    def post(self, request, *args, **kwargs):
        if 'professionnel' in request.POST:
            form = forms.ProfessionnelSelectForm(data=request.POST)
        else:
            form = forms.ProfessionnelClientForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        if isinstance(form, forms.ProfessionnelSelectForm):
            prof = Professionnel.objects.get(pk=form.cleaned_data['professionnel_pk'])
        else:
            prof = form.save()
        with transaction.atomic():
            remarque = form.cleaned_data.get('remarque_client', '')
            ProfClient.objects.create(
                professionnel=prof, client=self.client, remarque=remarque
            )
            Journal.objects.create(
                persona=self.client.persona,
                description=f"Ajout du professionnel «{prof}»" + (f" ({remarque})" if remarque else ""),
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientProfessionnelEditView(UpdateView):
    model = ProfClient
    pk_url_kwarg = 'pk_ref'
    form_class = forms.ProfessionnelClientEditForm
    template_name = 'client/client_professionnel_edit.html'

    def form_valid(self, form):
        if not form.instance.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        form.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientProfessionnelDeleteView(DeleteView):
    model = ProfClient
    pk_url_kwarg = 'pk_ref'

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.object.delete()
        Journal.objects.create(
            persona=self.object.client.persona,
            description=f"Suppression du professionnel «{self.object}»",
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ProfessionnelListView(FilterFormMixin, ListView):
    model = Professionnel
    filter_formclass = forms.ProfFilterForm
    paginate_by = 25

    def get_queryset(self):
        return super().get_queryset().filter(date_archive__isnull=True).annotate(
            clients=Count('profclient', filter=Q(profclient__client__archive_le__isnull=True))
        ).order_by('nom', 'prenom')


class ProfessionnelSearchView(View):
    """Endpoint for autocomplete search for ClientProfessionnelAddView."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        results = [
            {'label': str(prof), 'value': prof.pk, 'details': {'remarque': prof.remarque}}
            for prof in Professionnel.objects.filter(
                Q(date_archive__isnull=True) & (
                    Q(nom__unaccent__icontains=term) | Q(remarque__unaccent__icontains=term)
                )
            )[:10]
        ]
        return JsonResponse(results, safe=False)


class ProfessionnelEditView(PermissionRequiredMixin, GeoAddressMixin, CreateUpdateView):
    model = Professionnel
    form_class = forms.ProfessionnelForm
    template_name = 'client/professionnel_edit.html'
    permission_required = 'client.change_professionnel'
    json_response = True

    def geo_enabled(self, instance):
        return super().geo_enabled(instance) and instance.type_pro == 'Sama'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'clients': self.object.profclient_set.filter(
                client__archive_le__isnull=True
            ).order_by('client__persona__nom') if self.object else None,
        }

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ProfessionnelArchiveView(PermissionRequiredMixin, View):
    model = Professionnel
    success_message = "Le professionnel a bien été archivé."
    permission_required = 'client.change_professionnel'

    def post(self, request, *args, **kwargs):
        prof = get_object_or_404(self.model, pk=kwargs['pk'])
        type_pro_str = "samaritain" if prof.type_pro == "Sama" else "professionnel"
        profclients = prof.profclient_set.filter(client__archive_le__isnull=True)
        with transaction.atomic():
            msg = f"Suppression du {type_pro_str} «{prof}» qui a été archivé."
            for lien in profclients:
                Journal.objects.create(
                    persona=lien.client.persona, description=msg,
                    quand=timezone.now(), qui=self.request.user
                )
                lien.delete()
            prof.date_archive = timezone.now()
            prof.save()
        messages.success(request, self.success_message)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientSetSamaritainsView(UpdateView):
    model = Client
    form_class = forms.ClientSamasForm
    template_name = "client/client_set_unset_samas.html"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "install_en_cours": self.object.alarme_actuelle() is not None,
        }

    def form_valid(self, form):
        client = self.object
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        if not client.samaritains:
            priorite = client.referents.filter(repondant__isnull=False).aggregate(Max('repondant'))['repondant__max']
            if priorite is None:
                priorite = 1
            else:
                priorite = priorite + 1
            form.instance.samaritains = priorite
            msg = "Ajout des samaritains comme répondants"
            if client.samaritains_des:
                msg = f"{msg} dès le {client.samaritains_des.lower.strftime('%d.%m.%Y')}"
        else:
            form.instance.samaritains = None
            if form.instance.samaritains_des.lower == form.instance.samaritains_des.upper:
                form.instance.samaritains_des = None
            client.profclient_set.filter(professionnel__type_pro='Sama').delete()
            msg = (
                "Suppression des samaritains comme répondants pour le "
                f"{client.samaritains_des.upper.strftime('%d.%m.%Y')}"
            )
        with transaction.atomic():
            form.save()
            Journal.objects.create(
                persona=client.persona, description=msg,
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': '#client-contacts'})


class ClientSamaritainsView(TemplateView):
    template_name = 'client/client_samas.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        current = client.profclient_set.filter(
            professionnel__type_pro='Sama'
        ).select_related('professionnel').order_by('priorite')
        client_geo = client.adresse().empl_geo
        for sama in current:
            sama.dist_vo = distance_vo(sama.professionnel.empl_geo, client_geo)
            sama.dist_real = distance_real(sama.professionnel.empl_geo, client_geo, only_cache=True)
        others = Professionnel.objects.filter(type_pro='Sama').exclude(
            Q(pk__in=current.values_list('professionnel__pk')) | Q(date_archive__isnull=False)
        )
        for sama in others:
            sama.dist_vo = distance_vo(sama.empl_geo, client_geo)
            sama.dist_real = distance_real(sama.empl_geo, client_geo, only_cache=True)
        others = sorted(others, key=lambda s: (s.dist_vo is None, s.dist_vo))
        return {
            **super().get_context_data(**kwargs),
            'client': client,
            'adresse': client.adresse(),
            'current_samas': current,
            'other_samas': others,
        }


class ClientSamaritainAddView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        sama = get_object_or_404(Professionnel, pk=self.kwargs['sama_pk'])
        priorite = (
            client.profclient_set.filter(
                professionnel__type_pro='Sama'
            ).aggregate(Max('priorite'))['priorite__max'] or 0
        ) + 1
        with transaction.atomic():
            ProfClient.objects.create(client=client, professionnel=sama, priorite=priorite)
            Journal.objects.create(
                persona=client.persona,
                description=f"Ajout samaritain {sama.nom_prenom} en priorité {priorite}",
                quand=timezone.now(), qui=request.user
            )
        return HttpResponseRedirect(reverse('client-samaritains', args=[client.pk]))


class ClientSamaritainRemoveView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        rel = get_object_or_404(ProfClient, pk=self.kwargs['sama_pk'])
        with transaction.atomic():
            msg = f"Suppression du samaritain {rel.professionnel.nom_prenom}"
            rel.delete()
            Journal.objects.create(
                persona=client.persona, description=msg, quand=timezone.now(), qui=request.user
            )
        return HttpResponseRedirect(reverse('client-samaritains', args=[client.pk]))


class ClientSamaritainReorderView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        sama_pks = [int(pk) for pk in request.POST.get('pks').split(',')]
        qset = client.profclient_set.filter(
            professionnel__type_pro='Sama'
        ).select_related('professionnel').order_by('priorite')
        old_order = ', '.join(f"{idx + 1}. {pro.professionnel.nom_prenom}" for idx, pro in enumerate(qset))
        with transaction.atomic():
            for pro in qset:
                pro.priorite = sama_pks.index(pro.pk) + 1
                pro.save()
            new_order = ', '.join(f"{idx + 1}. {pro.professionnel.nom_prenom}" for idx, pro in enumerate(qset.all()))
            Journal.objects.create(
                persona=client.persona,
                description=(
                    f"Changement de priorité des samaritains.<br>"
                    f"<i>Avant:</i> {old_order}.<br>"
                    f"<i>Après:</i> {new_order}"
                ),
                quand=timezone.now(), qui=request.user
            )
        return JsonResponse({'result': 'OK'})


def client_sama_distance(request, pk, sama_pk):
    client = get_object_or_404(Client, pk=pk)
    sama = get_object_or_404(Professionnel, pk=sama_pk)
    return JsonResponse(distance_real(sama.empl_geo, client.empl_geo))


class ClientAbsenceSuiviView(UpdateView):
    model = AdressePresence
    pk_url_kwarg = 'pk_abs'
    template_name = 'client/absence_edit.html'
    form_class = forms.AdressePresenceSuiviForm

    def form_valid(self, form):
        def format_si_date(val):
            if isinstance(val, date):
                return django_format(val, 'd.m.Y')
            return val

        if not form.instance.adresse.persona.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")

        with transaction.atomic():
            form.save()
            changes = ", ".join([
                "{label}: {value}".format(
                    label=form.fields[fname].label.lower(),
                    value=format_si_date(form.cleaned_data[fname])
                )
                for fname in form.changed_data if form.fields[fname].label
            ])
            msg = f"Suivi d’absence ({changes})"
            Journal.objects.create(
                persona=self.object.adresse.persona, description=msg,
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page#absences'})


class ClientFichierEditView(ClientJournalMixin, FichierEditView):
    parent_model = Persona

    def get_client(self):
        return self.get_related_object().client

    def get_related_object(self):
        obj = get_object_or_404(self.parent_model, client__pk=self.kwargs['pk'])
        if not obj.client.can_edit(self.request.user):
            raise PermissionDenied(
                "Vous n’avez pas les permissions nécessaires pour modifier cet objet."
            )
        return obj


class ClientFichierListeView(ClientAccessCheckMixin, FichierListeView):
    parent_model = Client
    editurl_name = 'client-fichier-edit'
    deleteurl_name = 'client-fichier-delete'


class ClientFichierDeleteView(FichierDeleteView):
    parent_model = Client

    def form_valid(self, form):
        response = super().form_valid(form)
        Journal.objects.create(
            persona=self.object.content_object, description=f"Suppression du fichier «{self.object.titre}»",
            quand=timezone.now(), qui=self.request.user
        )
        return response


class ClientArchiveView(PermissionRequiredMixin, View):
    """Vue agissant comme un toggle entre Archiver/Désarchiver."""
    permission_required = 'client.change_client'

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        app = current_app()
        if self.client.archive_le or not self.client.prestations_actuelles(limit_to=app):
            self.client.desarchiver(request.user, app)
            messages.success(request, f"Le client {self.client} a bien été réactivé.")
            return HttpResponseRedirect(self.client.get_absolute_url())

        try:
            msg = self.client.archiver(request.user, app)
        except PermissionDenied as exc:
            messages.error(request, str(exc))
            redir_to = request.headers.get('Referer', reverse('clients'))
        else:
            messages.success(request, msg)
            redir_to = reverse('clients')
        return HttpResponseRedirect(redir_to)


class ClientAlerteEditView(PermissionRequiredMixin, UpdateView):
    model = Alerte
    form_class = forms.AlerteForm
    template_name = 'client/alerte_edit.html'
    permission_required = 'client.view_client'

    def form_valid(self, form):
        if form.cleaned_data['traite_le']:
            form.instance.par = self.request.user
        with transaction.atomic():
            form.save()
            if form.cleaned_data['traite_le'] and form.cleaned_data['remarque']:
                Journal.objects.create(
                    persona=self.object.persona,
                    description=f"Alerte traitée: {form.cleaned_data['remarque']}",
                    quand=timezone.now(), qui=self.request.user
                )
        return JsonResponse({'result': 'OK', 'reload': 'page'})
