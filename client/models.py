from datetime import date, timedelta
from operator import attrgetter

from django.apps import apps
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.postgres.fields import ArrayField, DateRangeField
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import models, transaction
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import F, Min, Prefetch, OuterRef, Q, Subquery
from django.db.models.functions import JSONObject
from django.dispatch import Signal
from django.urls import NoReverseMatch, reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import format_html

from common.choices import Genres, Handicaps, Languages, Pays, Services
from common.fields import ChoiceArrayField, PhoneNumberField
from common.functions import IsAVS, Unaccent
from common.models import GeolocMixin, Fichier, TypeFichier, Utilisateur
from common.utils import RegionFinder, canton_app, current_app, same_month

# Un signal similaire à post_save, mais envoyé par les formulaires d'édition,
# après que de potentielles instances liées soient modifiées/créées.
client_saved = Signal()
referent_saved = Signal()


class PersonneMixin:
    @property
    def nom_prenom(self):
        return f"{self.nom} {self.prenom}".strip()

    def get_full_name(self):
        return f"{self.prenom} {self.nom}".strip()

    def rue_localite(self):
        pays = f'{self.pays}-' if (getattr(self, 'pays', None) and self.pays != 'CH') else ''
        return ", ".join(item for item in [
            self.rue, f'{pays}{self.npa} {self.localite}'
        ] if item)


class PersonaBaseQuerySet(models.QuerySet):
    adr_outer_field = "pk"

    def create_persona(self, **kwargs):
        # Intercepter les paramètres persona et adresse pour créer une instance
        # Persona et renvoyer les paramètres non utilisés.
        if "persona" not in kwargs:
            pers_field_names = [f.name for f in Persona._meta.get_fields()]
            adr_field_names = ['c_o', 'rue', 'npa', 'localite', 'empl_geo']
            pers_fields = {
                fname: val for fname, val in kwargs.items()
                if fname in pers_field_names or fname in adr_field_names
            }
            valide_des = kwargs.pop('valide_des', date.today() - timedelta(days=1))
            persona = Persona.objects.create(valide_des=valide_des, **pers_fields)
            kwargs["persona"] = persona
        return persona, {k: v for k, v in kwargs.items() if k not in pers_fields}

    @classmethod
    def adresse_active_subquery(cls, quand, outer_field_name=None, unaccent=False):
        return Subquery(
            AdresseClient.objects.filter(
                persona=OuterRef(outer_field_name or cls.adr_outer_field),
                principale__contains=quand
            ).values(
                data=JSONObject(
                    id="id",
                    c_o=Unaccent("c_o") if unaccent else "c_o",
                    rue=Unaccent("rue") if unaccent else "rue",
                    npa="npa",
                    localite=Unaccent("localite") if unaccent else "localite",
                    empl_geo="empl_geo",
                    valide_de="principale__startswith", valide_a="principale__endswith",
                )
            )[:1]
        )

    def avec_adresse(self, quand, unaccent=False):
        if "adresse_active" not in self.query.annotations:
            return self.annotate(
                adresse_active=self.adresse_active_subquery(quand, unaccent=unaccent)
            )
        return self


class PersonaQuerySet(PersonaBaseQuerySet):
    def create(self, valide_des=None, **kwargs):
        if "cree_le" not in kwargs:
            kwargs["cree_le"] = timezone.now()
        adr_field_names = ['c_o', 'rue', 'npa', 'localite', 'pays', 'empl_geo']
        adr_fields = {
            fname: kwargs.pop(fname, None if fname == 'empl_geo' else '')
            for fname in adr_field_names
        }
        pers = super().create(**kwargs)
        if any(adr_fields.values()):
            AdresseClient.objects.create(
                **{**adr_fields, 'persona': pers, 'principale': (valide_des, None)}
            )
        return pers


class Persona(PersonneMixin, models.Model):
    """
    Modèle de base représentant une entité Personne ou Entreprise (client,
    référent, utilisateur, bénévole, donateur, etc.)
    """
    id_externe = models.BigIntegerField(null=True, blank=True)  # NE: CID
    id_externe2 = models.BigIntegerField(null=True, blank=True)  # NE: Medlink, JU: n° client transports
    nom = models.CharField("Nom", max_length=60)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    genre = models.CharField("Genre", max_length=1, choices=Genres.choices, blank=True)
    date_naissance = models.DateField("Date de naissance", blank=True, null=True)
    case_postale = models.CharField(
        "Case postale", max_length=20, blank=True, help_text="Inclure «C.P.» ou «Case postale» dans le champ"
    )
    courriel = models.EmailField("Courriel", blank=True)
    langues = ArrayField(
        models.CharField(max_length=3, choices=Languages.choices(), blank=True),
        blank=True, null=True
    )
    cree_le = models.DateTimeField("Créé le")
    date_deces = models.DateField("Décès le", blank=True, null=True)
    fichiers = GenericRelation(Fichier)

    objects = PersonaQuerySet.as_manager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_externe'], name="persona_id_externe_unique"),
            models.UniqueConstraint(fields=['id_externe2'], name="persona_id_externe2_unique"),
        ]

    def __str__(self):
        return self.nom_prenom

    def _can_access(self, user, mode):
        for attr_name in ("client", "benevole", "donateur"):
            try:
                related_obj = getattr(self, attr_name)
            except (AttributeError, ObjectDoesNotExist):
                continue
            else:
                if getattr(related_obj, mode)(user):
                    return True
        return False

    def can_read(self, user):
        return self._can_access(user, "can_read")

    def can_edit(self, user):
        return self._can_access(user, "can_edit")

    @property
    def titre(self):
        return {"F": "Madame", "M": "Monsieur"}.get(self.genre, "")

    @property
    def langues_verbose(self):
        return [dict(Languages.choices())[v] for v in (self.langues or [])]

    def get_langues_display(self):
        return ', '.join(self.langues_verbose)

    def telephones(self):
        cached_tels = getattr(self, '_prefetched_objects_cache', {}).get('telephone_set')
        if cached_tels is not None:
            return sorted(cached_tels, key=attrgetter('priorite'))
        return self.telephone_set.order_by('priorite')

    @cached_property
    def adresses(self):
        return Adresses(self)

    def adresse(self, quand=None):
        return self.adresses.principale(quand)

    def geolocalize(self, save=True):
        return self.adresse().geolocalize(save=save)

    def get_client(self):
        try:
            return self.client
        except (AttributeError, ObjectDoesNotExist):
            return None

    def fichiers_app(self, app):
        types = {
            'alarme': [TypeFichier.DOC_GENERAL, TypeFichier.QUEST_ALARME, TypeFichier.CONTRAT_ALARME],
        }.get(app, [TypeFichier.DOC_GENERAL])
        return self.fichiers.filter(typ__in=types)

    @property
    def region(self):
        npa = self.adresse().npa
        return RegionFinder.get_region(npa) if npa else None


class Telephone(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tel = PhoneNumberField("Numéro")
    priorite = models.PositiveSmallIntegerField("Priorité")
    remarque = models.CharField("Remarque", max_length=80, blank=True)

    class Meta:
        verbose_name = "Téléphone"
        verbose_name_plural = "Téléphones"

    def __str__(self):
        return f"N° {self.tel}"

    @property
    def est_mobile(self):
        return self.tel.startswith("07")


class Journal(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, related_name='journaux')
    description = models.TextField()
    details = models.TextField(blank=True)
    quand = models.DateTimeField(default=timezone.now)
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)
    auto = models.BooleanField(default=True)
    fichier = models.FileField(upload_to='journal', blank=True)

    class Meta:
        db_table = 'alarme_journal'
        get_latest_by = "quand"

    def __str__(self):
        return f"{self.quand}: {self.description}"

    def can_edit(self, user):
        return self.auto is False and self.qui == user


class ClientQuerySet(PersonaBaseQuerySet):
    adr_outer_field = "persona_id"

    def create(self, **kwargs):
        valide_des = kwargs.get('valide_des', date.today() - timedelta(days=1))
        with transaction.atomic():
            if "persona" not in kwargs:
                persona, kwargs = self.create_persona(**kwargs)
                kwargs["persona"] = persona
            service = kwargs.pop('service', [])
            services = service if isinstance(service, list) else [service]
            tel1 = kwargs.pop("tel_1", None)
            tel2 = kwargs.pop("tel_2", None)
            instance = super().create(**kwargs)
            if tel1:
                Telephone.objects.create(persona=persona, tel=tel1, priorite=1)
            if tel2:
                Telephone.objects.create(persona=persona, tel=tel2, priorite=2 if tel1 else 1)
            for service in services:
                end = kwargs['archive_le'] if kwargs.get('archive_le') else None
                start = end - timedelta(days=1) if end else valide_des
                Prestation.objects.create(
                    client=instance, service=service, duree=(start, end),
                )
        return instance

    def par_service(self, services, date_actif=None, strict=False):
        """
        Renvoie une liste des clients ayant un des `services` actif.
        Si strict=True, renvoyer uniquement les clients qui ont ce seul service
        Annote les clients avec un attribut .services (liste de noms de services).
        """
        if strict:
            assert isinstance(services, str) or len(services) == 1
        date_actif = date_actif or date.today()
        if isinstance(date_actif, date):
            date_filter = Q(prestations__duree__contains=date_actif)
        elif isinstance(date_actif, DateRange):
            date_filter = Q(prestations__duree__overlap=date_actif)
        if strict:
            srv_filter = Q(services=[services] if isinstance(services, str) else services)
        else:
            srv_filter = Q(services__overlap=services if isinstance(services, list) else [services])
        return self.annotate(
            services=ArrayAgg('prestations__service', filter=date_filter, distinct=True)
        ).filter(srv_filter)

    def avec_services(self, date_actif=None, date_debut=False):
        if isinstance(date_actif, tuple):
            flt = Q(prestations__duree__overlap=DateRange(*date_actif))
        else:
            date_actif = date_actif or date.today()
            flt = Q(prestations__duree__contains=date_actif)
        annots = {'services': ArrayAgg('prestations__service', filter=flt, default=[])}
        if date_debut:
            annots['date_debut'] = Min('prestations__duree__startswith')
        return self.annotate(**annots)

    def hospitalises(self):
        return self.filter(pk__in=AdressePresence.objects.filter(
            Q(adresse__hospitalisation=True) & (
                Q(jusqua__isnull=True) | (Q(jusqua__gt=date.today()) & Q(depuis__lt=date.today()))
            )
        ).values_list('adresse__persona__client__pk'))


class PersonaProxy:
    def __init__(self, attr):
        self.attr = attr

    def __get__(self, instance, owner):
        return getattr(instance.persona, self.attr)

    def __set__(self, instance, value):
        raise AttributeError("The value should be set on the related persona instance.")


class PersonaProxyFields:
    """Allow direct read access to some fields of the .persona OneToOne field."""
    id_externe = PersonaProxy("id_externe")
    nom = PersonaProxy("nom")
    prenom = PersonaProxy("prenom")
    nom_prenom = PersonaProxy("nom_prenom")
    genre = PersonaProxy("genre")
    date_naissance = PersonaProxy("date_naissance")
    region = PersonaProxy("region")
    courriel = PersonaProxy("courriel")
    telephones = PersonaProxy("telephones")
    date_deces = PersonaProxy("date_deces")
    langues = PersonaProxy("langues")
    adresses = PersonaProxy("adresses")
    fichiers = PersonaProxy("fichiers")
    alertes = PersonaProxy("alertes")
    journaux = PersonaProxy("journaux")

    def adresse(self, quand=None):
        # the queryset may have attached .adresse_active to the parent instance,
        # let propagate it to the related persona.
        if adr_active := getattr(self, "adresse_active", None):
            self.persona.adresse_active = adr_active
        return self.adresses.principale(quand)

    def geolocalize(self, save=True):
        return self.adresse().geolocalize(save=save)


class Client(PersonaProxyFields, models.Model):
    BOITIER_CHOICES = (
        ('prive', 'Privé'),
        ('c-r', 'Croix-Rouge'),
    )
    TYPE_LOGEMENT_CHOICES = (
        ('villa', "Villa"),
        ('appartement', "Appartement"),
        ('resid', "App. avec encadrement"),
        ('home', "Home/institution"),
    ) + canton_app.EXTRA_LOGEMENT_CHOICES
    DONNEES_MEDIC_MAP = {
        'troubles_eloc': 'Troubles de l’élocution',
        'troubles_aud': 'Troubles auditifs',
        'allergies': 'Allergies',
        'empl_medic': 'Emplacement des principaux médicaments (p. ex. antiallergiques)',
        'diabete': 'Diabète',
        'troubles_cardio': "Troubles cardiovasculaires",
        'epilepsie': "Epilepsie",
        'oxygene': "Oxygène",
        'anticoag': "Anticoagulant",
        'directives': "Directives anticipées, lieu de dépôt",
        'autres': "Autres données importantes",
    }
    SITUATION_VIE_SEUL = "Seul-e"
    SITUATION_VIE_COUPLE = "En couple"

    persona = models.OneToOneField(Persona, on_delete=models.CASCADE)
    partenaire = models.OneToOneField("self", on_delete=models.SET_NULL, blank=True, null=True, related_name="partenaire_de")
    type_logement = models.CharField("Type de logement", choices=TYPE_LOGEMENT_CHOICES, max_length=12, blank=True)
    type_logement_info = models.CharField("Type de logement", max_length=60, blank=True)
    etage = models.CharField("Étage/niveau", max_length=20, blank=True)
    ascenseur = models.BooleanField("Ascenseur", default=False)
    code_entree = models.CharField("Code entrée immeuble", max_length=20, blank=True)
    nb_pieces = models.CharField("Nbre de pièces", max_length=50, blank=True)
    boitier_cle = models.CharField("Boîtier à clé", max_length=10, choices=BOITIER_CHOICES, blank=True)
    cles = models.CharField("Infos clés", max_length=250, blank=True)
    situation_vie = models.CharField("Situation de vie", max_length=80, blank=True)
    donnees_medic = models.JSONField("Données médicales", blank=True, null=True)
    prest_compl = models.BooleanField("Prestations complémentaires", default=False)
    no_debiteur = models.CharField("N° de débiteur", max_length=30, blank=True)
    no_centrale = models.CharField("N° client de la centrale", max_length=10, blank=True)
    animal_comp = models.CharField("Animal de compagnie", max_length=50, blank=True)
    samaritains = models.PositiveSmallIntegerField("Samaritains comme répondants", blank=True, null=True)
    samaritains_des = DateRangeField("Prestation Samaritains", blank=True, null=True)
    professionnels = models.ManyToManyField('Professionnel', through='ProfClient', blank=True)
    visiteur = models.ForeignKey(
        Utilisateur, blank=True, null=True, on_delete=models.SET_NULL,
        verbose_name="Visiteur·euse", related_name="visites"
    )
    spec_alarme = models.TextField("Spécificités du système d'alarme", blank=True)
    remarques_int_alarme = models.TextField("Remarques internes (alarme)", blank=True)
    remarques_int_transport = models.TextField("Remarques internes (transports)", blank=True)
    remarques_int_visite = models.TextField("Remarques internes (visites)", blank=True)
    remarques_ext_alarme = models.TextField("Remarques (visibles par bénévoles alarme)", blank=True)
    remarques_ext_transport = models.TextField("Remarques (visibles par bénévoles transports)", blank=True)
    remarques_ext_visite = models.TextField("Remarques (visibles par bénévoles visites)", blank=True)
    courrier_envoye = models.DateField(blank=True, null=True)
    archive_le = models.DateField("Archivé le", blank=True, null=True)
    # Pour transports:
    handicaps = ChoiceArrayField(
        models.CharField(max_length=10, choices=Handicaps.choices, blank=True),
        blank=True, null=True, verbose_name='Handicaps'
    )
    tarif_avs_des = models.DateField("Tarif AVS dès", blank=True, null=True)
    fonds_transport = models.BooleanField(settings.LIBELLE_MAUVAIS_PAYEUR, default=False)

    objects = ClientQuerySet.as_manager()

    class Meta:
        db_table = 'alarme_client'

    def __str__(self):
        return self.persona.nom_prenom

    def get_absolute_url(self):
        try:
            return (
                reverse('transport-client-detail', args=[self.pk]) if apps.is_installed('transport')
                else reverse('client-edit', args=[self.pk])
            )
        except NoReverseMatch:
            return None

    def refresh_from_db(self):
        super().refresh_from_db()
        try:
            del self.adresses
        except AttributeError:
            pass

    def get_partenaire_de(self):
        try:
            return self.partenaire_de
        except ObjectDoesNotExist:
            return None

    @property
    def remarques_ext(self):
        try:
            return getattr(self, f'remarques_ext_{current_app()}')
        except AttributeError:
            return ''

    def can_be_archived(self, user, for_app):
        if self.pk is None or not user.has_perm('client.change_client'):
            return False
        if for_app == 'alarme':
            alarme = self.derniere_alarme()
            if alarme and alarme.retour_mat and alarme.retour_mat < date.today() - timedelta(days=31):
                return True
            elif not alarme and not self.mission_set.filter(effectuee__isnull=True).exists():
                return True
            return False
        elif for_app == 'visite':
            if self.attributions.filter(fin__isnull=True).exists():
                return False
        return True

    def _archiver_fort(self, user=None, message=''):
        """Archivage fort, i.e. remplir archive_le + info services externes."""
        self.archive_le = date.today()
        with transaction.atomic():
            self.save(update_fields=['archive_le'])
            # Pas de update(), sinon le signal de sync referent_saved n'est pas lancé
            for referent in self.referent_set.filter(date_archive__isnull=True):
                referent.date_archive = date.today()
                referent.save(update_fields=['date_archive'])
                referent_saved.send(sender=Referent, instance=referent, created=False)
            journal_msg = "Le client a été archivé"
            if message:
                journal_msg += f" ({message})"
            Journal.objects.create(
                persona=self.persona, description=journal_msg, quand=timezone.now(), qui=user
            )
            client_saved.send(sender=Client, instance=self, created=False, force=True)

    def archiver(self, user, for_app, check=True, message=''):
        """Archivage léger, archivage du service 'for_app' uniquement."""
        if check and not self.can_be_archived(user, for_app):
            raise PermissionDenied("Il n’est pas possible d’archiver ce client")

        with transaction.atomic():
            prestations = self.prestations_actuelles()
            prest_app = next((p for p in prestations if p.service == for_app), None)
            if prest_app:
                if prest_app.duree.lower == date.today():
                    prest_app.delete()
                else:
                    prest_app.duree = (prest_app.duree.lower, date.today())
                    prest_app.save()

            journal_msg = f"Le client a été archivé pour l’application «{for_app}»"
            if message:
                journal_msg += f" ({message})"
            Journal.objects.create(
                persona=self.persona, description=journal_msg, quand=timezone.now(), qui=user
            )
            client_saved.send(sender=Client, instance=self, created=False, force=True)
        return journal_msg

    def desarchiver(self, user, for_app, message=''):
        # Désarchiver
        with transaction.atomic():
            if self.archive_le is not None:
                # Désarchiver les référents archivés le même jour que le client
                for referent in self.referent_set.filter(date_archive=self.archive_le):
                    referent.date_archive = None
                    if for_app == 'alarme':
                        referent.courrier_envoye = None
                    referent.save(update_fields=['date_archive', 'courrier_envoye'])
                    referent_saved.send(sender=Referent, instance=referent, created=False)
                self.archive_le = None
                self.save(update_fields=['archive_le'])
                journal_msg = "Le client a été réactivé et retiré des archives"
            else:
                if for_app == 'alarme':
                    self.referent_set.update(courrier_envoye=None)
                journal_msg = f"Le client a été réactivé pour le secteur «{for_app}»"
            if for_app == 'alarme':
                self.courrier_envoye = None
                self.save(update_fields=['courrier_envoye'])
            Prestation.objects.create(client=self, service=for_app, duree=(date.today(), None))
            client_saved.send(sender=Client, instance=self, created=False)
            if message:
                journal_msg += f" ({message})"
            Journal.objects.create(
                persona=self.persona, description=journal_msg, quand=timezone.now(), qui=user
            )

    def prestations_actuelles(self, limit_to=None, as_services=False):
        if not self.pk:
            return []
        return [
            (p.service if as_services else p) for p in self.prestations.filter(duree__contains=date.today())
            if not limit_to or limit_to == p.service
        ]

    @property
    def titre(self):
        return self.persona.titre

    @property
    def noms(self):
        noms = [self.persona.nom]
        if self.partenaire_id and self.partenaire.persona.nom != noms[0]:
            noms.append(self.partenaire.persona.nom)
        return " et ".join(noms)

    @property
    def prenoms(self):
        return " et ".join(
            p for p in [self.persona.prenom, self.partenaire.persona.prenom if self.partenaire_id else None] if p
        )

    @property
    def handicaps_verbose(self):
        return [dict(Handicaps.choices)[v] for v in (self.handicaps or [])]

    def get_handicaps_display(self, exclude=None):
        handis = self.handicaps_verbose
        if exclude and exclude.label in handis:
            handis.remove(exclude.label)
        return ", ".join(handis)

    def get_donnees_medic_display(self):
        if not self.donnees_medic:
            return ""

        def serialize(val):
            return {'': "-", False: "Non", True: "Oui", None: '?'}.get(val, val)

        return "\n".join([
            f'{label}: {serialize(self.donnees_medic[key])}'
            for key, label in self.DONNEES_MEDIC_MAP.items() if key in self.donnees_medic
        ])

    def age(self, quand=None):
        if not self.persona.date_naissance:
            return None
        age = ((quand or date.today()) - self.persona.date_naissance).days / 365.25
        return int(age * 10) / 10  # 1 décimale arrondi vers le bas

    def is_avs(self, quand=None):
        """
        Indique si client en âge AVS selon normes OFAS
        """
        ref_date = quand or date.today()
        if self.tarif_avs_des and self.tarif_avs_des <= ref_date:
            return True
        # Déléguer à la fonction en base de données pour le calcul exact
        return Client.objects.annotate(
            is_avs_db=IsAVS(ref_date, F('tarif_avs_des'), F('persona__date_naissance'), F('persona__genre'))
        ).get(pk=self.pk).is_avs_db

    def is_laa(self):
        ref_fact = self.referents.filter(facturation_pour__contains=["transp"]).first()
        return ref_fact and ref_fact.no_sinistre

    def can_read(self, user):
        if user.has_perm('client.view_client'):
            return True
        return user.has_intervention_on(self) or (
            self.visiteur and self.visiteur == user
        )

    def can_edit(self, user):
        return user.has_perm('client.change_client') or (
            user.has_intervention_on(self, types=['NEW'])
        )

    def alarme_actuelle(self):
        return self.installation_set.filter(
            Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
        ).select_related('alarme').first()

    def derniere_alarme(self):
        if 'installation_set' in getattr(self, '_prefetched_objects_cache', {}):
            # Avoid access to DB
            return next(iter(sorted(
                self._prefetched_objects_cache['installation_set'],
                key=attrgetter('date_debut'), reverse=True
            )), None)
        try:
            return self.installation_set.select_related(
                'alarme', 'alarme__modele', 'abonnement'
            ).latest('date_debut')
        except ObjectDoesNotExist:
            return None

    def date_resiliation(self):
        alarme = self.derniere_alarme()
        return alarme.date_fin_abo if alarme else None

    def materiel_actuel(self):
        return self.materielclient_set.filter(
            Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
        )

    def has_facture_for_month(self, date_, article):
        # Using factures.all() to take advantage of Prefetch.
        return any([
            same_month(fact.mois_facture, date_) and article == fact.article
            for fact in self.factures.all()
        ])

    @property
    def referents(self):
        if self.archive_le:
            # Afficher contacts archivés si le client est lui-même archivé
            qs = self.referent_set.all()
        else:
            qs = self.referent_set.filter(
                Q(date_archive=None) | Q(date_archive__gt=date.today())
            )
        return qs.prefetch_related(
            Prefetch('referenttel_set', queryset=ReferentTel.objects.order_by('priorite'))
        )

    def contractant(self):
        """Return self or any referent with contractant boolean set."""
        return next(iter([ref for ref in self.referents if ref.contractant]), self)

    def adresse_facturation(self, facture):
        potentials = sorted(
            [tpl for tpl in [(facture.match(ref), ref) for ref in self.referents] if tpl[0] > 0],
            reverse=True
        )
        return potentials[0][1] if potentials else self

    def repondants(self):
        repondants = [ref for ref in self.referents if ref.repondant is not None]
        if self.samaritains:
            repondants.append(RepondantSama(self.samaritains))
        return sorted(repondants, key=attrgetter('repondant'))

    def contacts_as_context(self):
        return {
            'repondants': self.repondants(),
            'referents': sorted(
                [ref for ref in self.referents if ref.referent is not None],
                key=attrgetter('referent')
            ),
            'autres_contacts': [ref for ref in self.referents if not any(
                [ref.repondant is not None, ref.referent is not None]
            )],
        }

    def notifier_autres_equipes(self, from_app, message):
        """Avertir autres équipes d'un changement par une Alerte."""
        other_types = set([p.service for p in self.prestations_actuelles()]) - {from_app}
        msg = f"Modifications depuis l’application «{from_app}»: {message}"
        for app in other_types:
            Alerte.objects.create(
                persona=self.persona, cible=app, alerte=msg, recu_le=timezone.now()
            )
        return len(other_types)


class AdresseClient(GeolocMixin, models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    principale = DateRangeField(blank=True, null=True)
    hospitalisation = models.BooleanField(default=False)
    depose = models.BooleanField(
        "Adresse de dépose", default=False, help_text="Adresse où se rendre pour chercher ou déposer la personne"
    )
    c_o = models.CharField("C/o", max_length=80, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    pays = models.CharField("Pays", max_length=2, choices=Pays, default='CH')
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    remarque = models.TextField(blank=True)

    est_adresse_client = True

    class Meta:
        db_table = 'alarme_adresseclient'

    def __str__(self):
        return ", ".join([item for item in [
            "Hospitalisation" if self.hospitalisation else None, self.adresse()
        ] if item])

    def __html__(self, **kwargs):
        return format_html(
            '<span class="adresse">{}, {} {}</span>', self.rue or "-", self.npa, self.localite
        )

    def adresse(self):
        if self.rue:
            return f"{self.rue}, {self.npa} {self.localite}"
        elif self.localite:
            return f"{self.npa} {self.localite}"
        return ""

    @property
    def actuelle(self):
        """Renvoie l’objet AdressePresence actuel"""
        return self.actuelle_ou_future(0)

    def actuelle_ou_future(self, nb_jours):
        # Leverage prefetch_related
        acts = [
            pres for pres in self.presences.all()
            if pres.jusqua is None or (
                pres.jusqua > date.today() and pres.depuis < (date.today() + timedelta(days=nb_jours))
            )
        ]
        return acts[0] if acts else None


class AdressePresence(models.Model):
    adresse = models.ForeignKey(AdresseClient, on_delete=models.CASCADE, related_name='presences')
    depuis = models.DateField("Dès le")
    jusqua = models.DateField("Jusqu’au", null=True, blank=True)
    dernier_contact = models.DateField("Dernier contact", null=True, blank=True)
    remarque = models.TextField(blank=True)

    class Meta:
        db_table = 'alarme_adressepresence'

    def __str__(self):
        return f"Présence à «{self.adresse}» du {self.depuis} au {self.jusqua}"


class Adresses:
    """Abstraction des différentes adresses d'un client, accessible par Persona.adresses."""

    def __init__(self, pers):
        self.persona = pers
        self._adresses = None

    @property
    def adresses(self):
        if self._adresses is None:
            self._adresses = list(
                self.persona.adresseclient_set.all().prefetch_related('presences')
            ) if self.persona.pk else []
        return self._adresses

    def __len__(self):
        return len(self.adresses)

    def __iter__(self):
        return iter(self.adresses)

    def principale(self, quand=None):
        """Renvoie l'adresse principale de la personne à la date `quand`."""
        quand = quand or date.today()
        if getattr(self.persona, 'adresse_active', None):
            principale = DateRange(
                to_date(self.persona.adresse_active.get('valide_de')), to_date(self.persona.adresse_active.get('valide_a'))
            )
            if quand in principale or quand == principale.upper:
                return AdresseClient(
                    persona=self.persona, principale=principale,
                    **{k: v for k, v in self.persona.adresse_active.items() if k not in {'valide_de', 'valide_a'}}
                )
        adr = next((
            adr for adr in self.adresses
            if adr.principale and (quand in adr.principale or adr.principale.upper == quand)
        ), None)
        if adr is None:
            adr = next((adr for adr in self.adresses if adr.principale), None)
        return adr

    def depose(self, quand=None):
        quand = quand or date.today()
        if presence := self.presence_actuelle():
            if not presence.adresse.hospitalisation:
                return presence.adresse
        adr = next((adr for adr in self.adresses if adr.depose and adr.actuelle), None)
        if not adr:
            adr = self.principale(quand=quand)
        return adr

    def autres(self):
        """Renvoie toutes les adresses clients autres que principales."""
        return [adr for adr in self.adresses if adr.principale is None]

    def presence_actuelle(self, anticip=0):
        """
        Renvoie instance AdressePresence actuelle ou None si pas d'autre adresse active.
        `anticip`: nbre de jours à anticiper si une présence débute dans le futur.
        """
        return next(
            (adr.actuelle for adr in self.autres() if adr.actuelle_ou_future(anticip) and not adr.depose),
            None
        )

    def nouvelle(self, **adr_fields):
        """Changement d'adresse."""
        ancienne = self.principale()
        if ancienne and ancienne.principale.lower == date.today():
            # Modif. dans le même jour que la création, ne pas considérer comme nouvelle adresse
            ancienne.__dict__.update(adr_fields)
            ancienne.save()
        else:
            if ancienne:
                ancienne.principale = (ancienne.principale.lower, date.today() - timedelta(days=1))
                ancienne.save()
            adr = AdresseClient.objects.create(
                persona=self.persona, principale=(date.today(), None), **adr_fields
            )
            adr.check_geolocalized()
            self.adresses  # fill the cache
            self._adresses.append(adr)


class Referent(PersonneMixin, models.Model):
    SALUTATION_CHOICES = (
        ('M', 'Monsieur'),
        ('F', 'Madame'),
        ('P', 'Madame et Monsieur'),
        ('E', 'Entreprise'),
        ('A', 'Maître'),
    )
    FACTURATION_CHOICES = (
        ('al-abo', 'Alarme (install + abo)'),
        ('al-tout', 'Alarme (tout)'),
        ('transp', 'Transports'),
    )
    sama = False
    # Peut être null si création depuis ERP
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True)
    id_externe = models.BigIntegerField(null=True, blank=True)
    cree_le = models.DateTimeField("Créé le", auto_now_add=True)
    no_centrale = models.PositiveIntegerField("N° personne à la centrale", null=True, blank=True)
    nom = models.CharField("Nom", max_length=100)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    salutation = models.CharField("Salutation", max_length=1, choices=SALUTATION_CHOICES, blank=True)
    complement = models.CharField("Nom (ligne 2)", max_length=80, blank=True)
    pers_contact = models.CharField("Personne de contact", max_length=80, blank=True)
    case_postale = models.CharField(
        "Case postale", max_length=20, blank=True, help_text="Inclure «C.P.» ou «Case postale» dans le champ"
    )
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    pays = models.CharField("Pays", max_length=2, choices=Pays, blank=True)
    courriel = models.EmailField("Courriel", blank=True)
    relation = models.CharField("Relation/lien de parenté", max_length=100, blank=True)
    # Role(s) of Referent
    repondant = models.PositiveSmallIntegerField("Priorité répondant", blank=True, null=True)
    referent = models.PositiveSmallIntegerField("Priorité référent", blank=True, null=True)
    admin = models.BooleanField("Contact administratif et technique", default=False)
    facturation_pour = ChoiceArrayField(
        models.CharField(max_length=10, choices=FACTURATION_CHOICES, blank=True),
        blank=True, default=list, verbose_name="Adresse de facturation pour"
    )
    no_sinistre = models.CharField("N° de sinistre", max_length=20, blank=True)
    contractant = models.BooleanField("Personne contractante", default=False)
    contact_transport = models.BooleanField("Contact pour les transports", default=False)
    contact_visites = models.BooleanField("Contact pour les visites", default=False)
    remarque = models.TextField(blank=True)
    courrier_envoye = models.DateField(blank=True, null=True)
    date_archive = models.DateField("Archivé le", null=True, blank=True)

    class Meta:
        db_table = 'alarme_referent'

    def __str__(self):
        if self.client_id:
            return f"{self.get_full_name()}, {self.roles()} pour {self.client}"
        return self.get_full_name()

    def roles(self):
        role_list = [
            f'référent ({self.referent})' if self.referent is not None else None,
            f'répondant ({self.repondant})' if self.repondant is not None else None,
            'contact admin./techn.' if self.admin else None,
            'adresse de fact.' if self.facturation_pour else None,
            'contractant' if self.contractant else None,
        ]
        return ", ".join(role for role in role_list if role)

    def telephones(self):
        return self.referenttel_set.order_by('priorite')

    def adresse(self):
        return self

    def autres_fonctions(self):
        """Nom des fonctions autres que repondant et referent."""
        fonctions = []
        if self.admin:
            fonctions.append("Contact administratif et technique")
        if self.facturation_pour:
            fonctions.append("Adresse de facturation (%s)" % ", ".join([
                dict(Referent.FACTURATION_CHOICES)[v] for v in self.facturation_pour
            ]))
        if self.contractant:
            fonctions.append("Personne contractante")
        return ", ".join(fonctions)

    def recoit_courrier(self):
        roles = self.roles()
        return True if (
            'répondant' in roles or
            'contact admin' in roles or
            (
                'courrier_debiteur' in canton_app.pdf_doc_registry and
                set(self.facturation_pour) & set(['al-abo', 'al-tout'])
            )
        ) else False


class RepondantSama:
    """Mock class to simulate a Referent of type repondant."""
    sama = True

    def __init__(self, priorite):
        self.repondant = priorite


class ReferentTel(models.Model):
    referent = models.ForeignKey(Referent, on_delete=models.CASCADE)
    tel = PhoneNumberField("Numéro")
    priorite = models.PositiveSmallIntegerField("Priorité")
    remarque = models.CharField("Remarque", max_length=80, blank=True)

    class Meta:
        db_table = 'alarme_referenttel'
        verbose_name = "Téléphone de référent"
        verbose_name_plural = "Téléphones de référent"

    def __str__(self):
        return f"N° {self.tel}"

    @property
    def est_mobile(self):
        return self.tel.startswith("07")


class Professionnel(PersonneMixin, GeolocMixin, models.Model):
    PROF_CHOICES = (
        ('Médecin', 'Médecin'),
        ('Infirmière', 'Infirmières indépendantes'),
        ('SAD', 'Soins à domicile'),
        ('Sama', 'Samaritain'),
    )
    no_centrale = models.PositiveIntegerField("N° à la centrale", null=True, blank=True)
    type_pro = models.CharField("Type de professionnel", max_length=20, choices=PROF_CHOICES, blank=True)
    nom = models.CharField("Nom", max_length=100)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    courriel = models.EmailField("Courriel", blank=True)
    tel_1 = PhoneNumberField("Téléphone (priorité 1)")
    tel_2 = PhoneNumberField("Téléphone (priorité 2)", blank=True)
    remarque = models.TextField(blank=True)
    inactif = models.BooleanField("Temporairement inactif", default=False)
    date_archive = models.DateField("Archivé le", null=True, blank=True)

    class Meta:
        db_table = 'alarme_professionnel'

    def __str__(self):
        details = ", ".join([txt for txt in [self.localite, self.get_type_pro_display()] if txt])
        return f'{self.nom_prenom} ({details})'


class ProfClient(models.Model):
    cree_le = models.DateTimeField("Créé le", auto_now_add=True)
    professionnel = models.ForeignKey(Professionnel, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    priorite = models.PositiveSmallIntegerField("Priorité", blank=True, null=True)
    remarque = models.TextField(blank=True)

    class Meta:
        db_table = 'alarme_profclient'

    def __str__(self):
        return f'Lien entre {self.professionnel} et {self.client}'


class Prestation(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='prestations')
    service = models.CharField(max_length=12, choices=Services)
    duree = DateRangeField("Durée")

    def __str__(self):
        return f'Prestation «{self.service}» pour {self.client}'


class Alerte(models.Model):
    """Messages d’alerte pour divers événements à quittancer (centrale d'alarme, etc.)."""
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, related_name="alertes")
    fichier = models.FileField("Fichier", upload_to='alertes', blank=True)
    alerte = models.TextField(blank=True)
    cible = models.CharField(max_length=10, choices=Services)
    recu_le = models.DateTimeField("Reçu le")
    traite_le = models.DateTimeField("Traité le", null=True, blank=True)
    par = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)
    remarque = models.TextField(blank=True)
    rel_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    rel_object_id = models.PositiveIntegerField(blank=True, null=True)
    modele_lie = GenericForeignKey("rel_content_type", "rel_object_id")

    class Meta:
        get_latest_by = ['recu_le']
        indexes = [
            models.Index(fields=['cible'], name='index_cible'),
        ]

    def __str__(self):
        return f"Alerte reçue le {self.recu_le} pour {self.persona}"


def to_date(str_val):
    return date.fromisoformat(str_val) if str_val else str_val
