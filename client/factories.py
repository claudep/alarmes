from datetime import date, timedelta
from random import randint

from django.utils import timezone
import factory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice, FuzzyInteger  # NOQA
from faker import Faker

from common.choices import Services
from common.distance import wgs84_to_mn95
from common.factories import UtilisateurFactory
from . import models

faker = Faker(locale='fr_CH')


class PhoneFaker(factory.Faker):
    def evaluate(self, instance, step, extra):
        result = super().evaluate(instance, step, extra)
        if self.provider == 'phone_number' and '+41' in result:
            result = result.replace('+41 (0)', '0').replace('+41', '0')
        return result


class ReferentFactory(DjangoModelFactory):
    class Meta:
        model = models.Referent

    nom = factory.Faker('last_name')
    prenom = factory.Faker('first_name')
    npa = factory.Faker('postcode')
    localite = factory.Faker('city')
    repondant = factory.Iterator([1, 2, 3, 4])


class PersonaFactory(DjangoModelFactory):
    class Meta:
        model = models.Persona

    nom = factory.Faker('last_name')
    prenom = factory.Faker('first_name')
    date_naissance = factory.Faker('date_between', start_date=date(1930, 1, 1), end_date=date(1960, 12, 31))
    npa = factory.Faker('postcode')
    localite = factory.Faker('city')


class ClientFactory(DjangoModelFactory):
    class Meta:
        model = models.Client

    persona = factory.SubFactory(PersonaFactory)
    referents = factory.RelatedFactoryList(
        ReferentFactory, factory_related_name='client', size=lambda: randint(1, 4)
    )


class PrestationFactory(DjangoModelFactory):
    class Meta:
        model = models.Prestation

    duree = (date.today() - timedelta(days=30), None)


class ClientAlarmeFactory(ClientFactory):
    prestations = factory.RelatedFactory(
        PrestationFactory, factory_related_name='client', service=Services.ALARME,
    )


class ClientTransportFactory(ClientFactory):
    prestations = factory.RelatedFactory(
        PrestationFactory, factory_related_name='client', service=Services.TRANSPORT,
    )
    empl_geo = factory.LazyAttribute(
        # FIXME: looks like local_latlng only iterates between 2 values.
        lambda o: list(wgs84_to_mn95(*reversed(faker.local_latlng('CH', coords_only=True))).coords)
    )


class JournalFactory(DjangoModelFactory):
    class Meta:
        model = models.Journal

    persona = factory.SubFactory(PersonaFactory)
    description = factory.fuzzy.FuzzyText()
    quand = factory.LazyAttribute(lambda _: timezone.now())
    qui = factory.SubFactory(UtilisateurFactory)
