from datetime import date

from django import forms
from django.apps import apps
from django.conf import settings
from django.contrib import admin, messages
from django.db.models import BooleanField, Count, Max, Q
from django.db.models.functions import Cast

from common.admin import NullableAsBooleanListFilter, ExportAction
from common.choices import Services
from common.forms import DateRangeField
from . import models


class TypeClientListFilter(admin.SimpleListFilter):
    title = "Type client"
    parameter_name = "services"

    def lookups(self, request, model_admin):
        return Services.choices

    def queryset(self, request, queryset):
        val = self.value()
        if val:
            return queryset.par_service(self.value())
        return queryset


class NonGeolocalizedListFilter(admin.SimpleListFilter):
    title = "Géolocalisation"
    parameter_name = "geoloc"

    def lookups(self, request, model_admin):
        return [("avec", "Avec"), ("sans", "Sans")]

    def queryset(self, request, queryset):
        val = self.value()
        if val:
            queryset = queryset.annotate(
                num_adr_geo=Count("adresseclient", filter=(
                    Q(adresseclient__principale__contains=date.today()) &
                    Q(adresseclient__empl_geo__isnull=False)
                ))
            )
            if val == "sans":
                return queryset.filter(num_adr_geo=0)
            else:
                return queryset.filter(num_adr_geo__gt=0)
        return queryset


class AdresseClientForm(forms.ModelForm):
    class Meta:
        model = models.AdresseClient
        fields = '__all__'
        field_classes = {
            'principale': DateRangeField,
        }


class PrestationInline(admin.TabularInline):
    model = models.Prestation
    extra = 0


class AdressesInline(admin.StackedInline):
    model = models.AdresseClient
    form = AdresseClientForm
    extra = 0


class ClientExportAction(ExportAction):
    def __init__(self):
        self.has_transports = apps.is_installed('transport')
        extra_fields = [('Services', 'services'), ('Région', 'region')]
        if self.has_transports:
            extra_fields.append(('Dernier transport', 'last_transport'))
        super().__init__("Liste clients", extra_fields=extra_fields)

    def get_queryset(self, queryset):
        if self.has_transports:
            queryset = queryset.annotate(last_transport=Max('transports__date'))
        return queryset


class PersonaExportAction(ExportAction):
    def __init__(self):
        extra_fields = [("Rue", "rue"), ("NPA", "npa"), ("Localité", "localite")]
        super().__init__("Liste des personnes", extra_fields=extra_fields)

    def get_queryset(self, queryset):
        return queryset.avec_adresse(date.today())

    def serialize(self, obj, field_name):
        if field_name in ["rue", "npa", "localite"]:
            return obj.adresse_active[field_name] if obj.adresse_active else ""
        return super().serialize(obj, field_name)


@admin.register(models.Persona)
class PersonaAdmin(admin.ModelAdmin):
    ordering = ['nom', 'prenom']
    list_display = ['nom_prenom', 'rue', 'npa', 'localite', 'est_client', 'est_benev']
    list_filter = [NonGeolocalizedListFilter]
    search_fields = ['nom']
    inlines = [AdressesInline]
    actions = [PersonaExportAction()]

    def get_queryset(self, request):
        return super().get_queryset(request).avec_adresse(date.today()).annotate(
            est_client=Cast("client", output_field=BooleanField()),
            est_benev=Cast("benevole", output_field=BooleanField()),
        )

    def rue(self, obj):
        return (obj.adresse_active or {}).get('rue', '')

    def npa(self, obj):
        return (obj.adresse_active or {}).get('npa', '')

    def localite(self, obj):
        return (obj.adresse_active or {}).get('localite', '')

    @admin.display(boolean=True)
    def est_client(self, obj):
        return obj.est_client is not None

    @admin.display(boolean=True)
    def est_benev(self, obj):
        return obj.est_benev is not None

    def save_model(self, request, obj, *args, **kwargs):
        self.is_new = obj.pk is None
        super().save_model(request, obj, *args, **kwargs)

    def save_related(self, request, form,*args, **kwargs):
        super().save_related(request, form, *args, **kwargs)
        # Signal de synchro lancé *après* que tous les modèles liés sont enregistrés.
        try:
            client = form.instance.client
        except models.Client.DoesNotExist:
            pass
        else:
            models.client_saved.send(sender=models.Client, instance=client, created=self.is_new)


@admin.register(models.Telephone)
class TelephoneAdmin(admin.ModelAdmin):
    list_display = ["persona", "tel", "priorite", "remarque"]
    search_fields = ["persona__nom", "tel"]


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    ordering = ['persona__nom', 'persona__prenom']
    list_display = ['persona', 'rue', 'npa', 'localite', 'services', 'actif']
    list_filter = [
        ('tarif_avs_des', NullableAsBooleanListFilter),
        TypeClientListFilter,
        ('archive_le', NullableAsBooleanListFilter)
    ]
    raw_id_fields = ["persona", "partenaire"]
    search_fields = ["persona__nom"]
    inlines = [PrestationInline]
    actions = [ClientExportAction(), 'sync_client']

    def get_queryset(self, request):
        return super().get_queryset(request).avec_services().avec_adresse(date.today())

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.archive_le is None

    def rue(self, obj):
        return (obj.adresse_active or {}).get('rue', '')

    def npa(self, obj):
        return (obj.adresse_active or {}).get('npa', '')

    def localite(self, obj):
        return (obj.adresse_active or {}).get('localite', '')

    def services(self, obj):
        return obj.services

    @admin.action(description="Synchroniser le client avec l’ERP")
    def sync_client(modeladmin, request, queryset):
        from api.signals import client_sync_receiver
        for client in queryset.all():
            client_sync_receiver(sender=models.Client, instance=client, created=False, force=True)

    def get_actions(self, request):
        actions = super().get_actions(request)
        try:
            can_sync = 'clients' in settings.CID_SYNC_ENABLE
        except AttributeError:
            can_sync = False
        if not can_sync or not request.user.is_superuser:
            actions.pop('sync_client', None)
        return actions

    def get_fieldsets(self, request, obj=None):
        fsets = super().get_fieldsets(request, obj)
        rem_fields = [fname for fname in fsets[0][1]['fields'] if fname.startswith('remarques_')]
        for rem_field in rem_fields:
            fsets[0][1]['fields'].remove(rem_field)
        fsets.append(('Remarques', {'fields': rem_fields, 'classes': ['collapse']}))
        return fsets

    def save_model(self, request, obj, *args, **kwargs):
        self.is_new = obj.pk is None
        super().save_model(request, obj, *args, **kwargs)

    def save_related(self, request, form, formsets, *args, **kwargs):
        fset_changed = formsets[0].has_changed()
        super().save_related(request, form, formsets, *args, **kwargs)
        if fset_changed:
            # Signal de synchro lancé *après* que tous les modèles liés sont enregistrés,
            # uniquement si des changements ont été faits dans les prestations.
            models.client_saved.send(sender=models.Client, instance=form.instance, created=self.is_new)


@admin.register(models.AdresseClient)
class AdresseClientAdmin(admin.ModelAdmin):
    list_display = ['persona', 'rue', 'npa', 'localite', 'est_principale', 'a_empl_geo', 'hospitalisation']
    list_filter = [('empl_geo', NullableAsBooleanListFilter), 'hospitalisation']
    search_fields = ['persona__nom', 'rue', 'npa', 'localite']
    raw_id_fields = ["persona"]
    form = AdresseClientForm
    actions = ["force_geoloc"]

    @admin.display(boolean=True)
    def a_empl_geo(self, obj):
        return obj.empl_geo is not None

    @admin.display(boolean=True)
    def est_principale(self, obj):
        return obj.principale is not None

    @admin.action(description="Tenter la géolocalisation")
    def force_geoloc(self, request, queryset):
        for adr in queryset.all():
            result = adr.geolocalize()
            if result:
                self.message_user(request, f"La géolocalisation de {adr} a réussi", messages.SUCCESS)
            else:
                self.message_user(request, f"La géolocalisation de {adr} a échoué", messages.ERROR)


@admin.register(models.AdressePresence)
class AdressePresenceAdmin(admin.ModelAdmin):
    list_display = ['persona', 'depuis', 'jusqua', 'remarque']

    def persona(self, obj):
        return obj.adresse.persona


class ReferentTelInline(admin.TabularInline):
    model = models.ReferentTel


@admin.register(models.Referent)
class ReferentAdmin(admin.ModelAdmin):
    list_display = [
        'nom', 'prenom', 'complement', 'rue', 'npa', 'localite', 'client',
        'relation', 'roles', 'id_externe', 'actif',
    ]
    ordering = ['nom', 'prenom']
    raw_id_fields = ["client"]
    search_fields = ['nom', 'complement', 'client__persona__nom']
    list_filter = [('date_archive', NullableAsBooleanListFilter)]
    actions = [
        ExportAction("Liste contacts et répondants"),
        'sync_referent',
    ]
    inlines = [ReferentTelInline]

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.date_archive is None

    @admin.action(description="Synchroniser le référent/contact avec l’ERP")
    def sync_referent(modeladmin, request, queryset):
        from api.signals import referent_sync_receiver
        for ref in queryset.all():
            referent_sync_receiver(sender=models.Client, instance=ref, created=False, force=True)

    def get_actions(self, request):
        actions = super().get_actions(request)
        try:
            can_sync = 'referents' in settings.CID_SYNC_ENABLE
        except AttributeError:
            can_sync = False
        if not can_sync or not request.user.is_superuser:
            actions.pop('sync_referent', None)
        return actions

    def save_model(self, request, obj, *args, **kwargs):
        self.is_new = obj.pk is None
        super().save_model(request, obj, *args, **kwargs)

    def save_related(self, request, form,*args, **kwargs):
        super().save_related(request, form, *args, **kwargs)
        models.referent_saved.send(sender=models.Referent, instance=form.instance, created=self.is_new)


@admin.register(models.Professionnel)
class ProfessionnelAdmin(admin.ModelAdmin):
    list_display = ['nom', 'type_pro', 'npa', 'localite', 'tel_1', 'courriel', 'num_clients', 'actif']
    list_filter = ['type_pro']
    ordering = ['nom']
    search_fields = ['nom']
    actions = [ExportAction("Liste professionnels")]

    @admin.display(description='Nbre de clients')
    def num_clients(self, obj):
        return obj.num_clients

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.date_archive is None

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(num_clients=Count('client'))


@admin.register(models.ProfClient)
class ProfClientAdmin(admin.ModelAdmin):
    list_display = ['client', 'professionnel', 'priorite']
    raw_id_fields = ['client', 'professionnel']


@admin.register(models.Journal)
class JournalAdmin(admin.ModelAdmin):
    list_display = ['quand', 'qui', 'persona', 'description']
    date_hierarchy = 'quand'
    list_filter = [
        ('fichier', admin.EmptyFieldListFilter),
        'qui'
    ]
    search_fields = ['persona__nom', 'description']
    raw_id_fields = ['persona']


@admin.register(models.Alerte)
class AlerteAdmin(admin.ModelAdmin):
    list_display = ["recu_le", "persona", "cible", "alerte", "traite_le"]
    list_filter = ["traite_le"]
    raw_id_fields = ["persona"]
