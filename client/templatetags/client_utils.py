from datetime import date

from django.apps import apps
from django.template import Library
from django.templatetags.static import static
from django.urls import reverse
from django.utils.html import escape, format_html, format_html_join
from django.utils.safestring import SafeString

from common.choices import Services
from common.utils import client_url_name

register = Library()

BANDEAU_ANTICIP_JOURS = 30


@register.simple_tag
def bandeau_absence(client):
    """
    Affichage d'un bandeau d'alerte sur diverses pages si le client est actuellement
    ou dans les BANDEAU_ANTICIP_JOURS prochains jours à une autre adresse (ou hôpital).
    """
    presence = client.adresses.presence_actuelle(anticip=BANDEAU_ANTICIP_JOURS) if client else None
    if not presence:
        return ''
    # presence is AdressePresence instance.
    adr = presence.adresse
    if presence.depuis <= date.today():
        msg = "Actuellement"
    else:
        msg = f"<b>Dès le {presence.depuis.strftime('%d.%m.%Y')}:</b>"
    if adr.hospitalisation:
        msg += " à l’hôpital"
        if adr.localite:
            msg += f" ({escape(adr)})"
    else:
        msg += f" à {escape(adr)}"
    if presence.jusqua:
        msg += f", jusqu’au {presence.jusqua.strftime('%d.%m.%Y')}"
    if adr.remarque:
        msg += f" ({escape(adr.remarque)})"
    return SafeString(f'<div class="alert alert-danger">{msg}</div>')


@register.inclusion_tag("client/partenaire.html")
def info_partenaire(client, help_content=""):
    partenaire = client.partenaire if client.partenaire_id else client.get_partenaire_de()
    edit_url = reverse(client_url_name(), args=[partenaire.pk]) if (partenaire and client_url_name()) else ""
    return {
        "partenaire": partenaire,
        "edit_url": edit_url,
        "help_content": help_content,
    }


@register.simple_tag
def services_as_icons(client, exclude='', classes=''):
    icons = ""
    if hasattr(client, 'services'):
        services = client.services
    else:
        services = client.services = client.prestations_actuelles(as_services=True)
    if Services.ALARME in services and exclude != 'alarme':
        icons += (
            f'<img class="icon blacksvg ms-2 {classes}" src="{static("logos/logo-alarme.svg")}" '
            'title="Cette personne est cliente de l’Alarme" data-bs-toggle="tooltip">'
        )
    if Services.TRANSPORT in services and exclude != 'transport':
        icons += (
            f'<img class="icon blacksvg ms-2 {classes}" src="{static("logos/transport.svg")}" '
            'title="Cette personne est cliente des Transports" data-bs-toggle="tooltip">'
        )
    if Services.VISITE in services and exclude != 'visite':
        icons += (
            f'<img class="icon blacksvg ms-2 {classes}" src="{static("logos/visite.svg")}" '
            'title="Cette personne est cliente des Visites" data-bs-toggle="tooltip">'
        )
    if Services.OSAD in services and exclude != 'osad':
        icons += (
            f'<img class="icon blacksvg ms-2 {classes}" src="{static("logos/osad.svg")}" '
            'title="Cette personne est cliente OSAD" data-bs-toggle="tooltip">'
        )
    return SafeString(icons)


@register.filter
def tels_as_divs(client_or_persona):
    return format_html_join(
        "\n", '<div><a class="tel" href="tel:{}">{}</a></div>',
        ((tel.tel, tel.tel) for tel in client_or_persona.telephones())
    )


@register.filter
def archived_for(client, current_app):
    if not client:
        return False
    if client.archive_le:
        return True
    prests_app = [p for p in client.prestations.all() if p.service == current_app]
    return prests_app and all(prest.duree.upper is not None for prest in prests_app)


@register.simple_tag
def client_details_url(client):
    """
    Selon utilisateur connecté et type de client, renvoie l'URL pour accéder
    au dossier client.
    """
    if hasattr(client, 'services'):
        services = client.services
    else:
        services = client.services = client.prestations_actuelles(as_services=True)
    if (
        (apps.is_installed('alarme') and Services.ALARME in services) or
        (apps.is_installed('transport') and Services.TRANSPORT in services) or
        (apps.is_installed('visite') and Services.VISITE in services)
    ):
        return client.get_absolute_url()
    return ""
