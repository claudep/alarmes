from django.db import migrations


def migrate_clients(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    AdresseClient = apps.get_model('client', 'AdresseClient')
    Journal = apps.get_model('client', 'Journal')
    Persona = apps.get_model('client', 'Persona')
    Fichier = apps.get_model('common', 'Fichier')
    ContentType = apps.get_model("contenttypes", "ContentType")
    fields = [
        "id_externe", "id_externe2", "nom", "prenom", "genre", "date_naissance",
        "case_postale", "tel_1", "tel_2", "courriel", "langues", "cree_le",
        "archive_le", "date_deces"
    ]
    client_ct = ContentType.objects.get_for_model(Client)
    persona_ct = ContentType.objects.get_for_model(Persona)
    for client in Client.objects.all():
        client.persona = Persona.objects.create(**{f: getattr(client, f) for f in fields})
        client.save(update_fields=["persona"])
        Fichier.objects.filter(content_type=client_ct, object_id=client.pk).update(
            content_type=persona_ct, object_id=client.persona_id
        )
    for adresse in AdresseClient.objects.all().select_related("client"):
        adresse.persona = adresse.client.persona
        adresse.save(update_fields=["persona"])
    for entry in Journal.objects.all().select_related("client"):
        entry.persona = entry.client.persona
        entry.save(update_fields=["persona"])


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_fichier'),
        ('contenttypes', '__latest__'),
        ('client', '0055_persona_client_persona'),
    ]

    operations = [
        migrations.RunPython(migrate_clients, migrations.RunPython.noop),
    ]
