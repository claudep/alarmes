from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0039_referent_nom_80_car'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referent',
            name='nom',
            field=models.CharField(max_length=100, verbose_name='Nom'),
        ),
    ]
