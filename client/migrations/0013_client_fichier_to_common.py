from django.db import migrations


def migrate_client_fichiers(apps, schema_editor):
    FichierClient = apps.get_model('client', 'Fichier')
    FichierCommon = apps.get_model('common', 'Fichier')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    if not FichierClient.objects.exists():
        return
    client_type = ContentType.objects.get(model='client')
    for fichier in FichierClient.objects.all():
        fichier.fichier.name = fichier.fichier.name.replace('clients', 'fichiers/client')
        fc = FichierCommon.objects.create(
            content_type=client_type,
            object_id=fichier.client_id,
            titre=fichier.titre,
            fichier=fichier.fichier,
            typ=fichier.typ,
            qui=fichier.qui,
            quand=fichier.quand,
        )


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '__first__'),
        ('client', '0012_client_fonds_transport'),
        ('common', '0005_fichier'),
    ]

    operations = [
        migrations.RunPython(migrate_client_fichiers)
    ]
