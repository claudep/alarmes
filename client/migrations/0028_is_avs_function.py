from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0027_remove_client_remarques_ext"),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_avs(
    event_date date, tarif_avs_des date, date_naissance date, genre varchar(1)
)
RETURNS BOOLEAN AS $$
DECLARE is_avs boolean;
        age_int integer := EXTRACT(YEAR FROM age(event_date, date_naissance));
BEGIN
    SELECT (tarif_avs_des IS NOT NULL AND tarif_avs_des <= event_date) OR age_int >= 65 OR (genre = 'F' AND age_int >= 64) INTO is_avs;
    RETURN is_avs;
END
$$
LANGUAGE plpgsql
IMMUTABLE;""")
    ]
