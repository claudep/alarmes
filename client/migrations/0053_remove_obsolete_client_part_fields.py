from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0052_adresseclient_pays'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='date_naissance_part',
        ),
        migrations.RemoveField(
            model_name='client',
            name='genre_part',
        ),
        migrations.RemoveField(
            model_name='client',
            name='nom_part',
        ),
        migrations.RemoveField(
            model_name='client',
            name='prenom_part',
        ),
        migrations.RemoveField(
            model_name='client',
            name='tel_part',
        ),
    ]
