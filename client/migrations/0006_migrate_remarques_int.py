from django.db import migrations
from django.db.models import F


def migrate_remarques_int(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    Client.objects.exclude(remarques_int='').filter(type_client__contains=['alarme']).update(
        remarques_int_alarme=F('remarques_int')
    )
    Client.objects.exclude(remarques_int='').filter(type_client__contains=['transport']).update(
        remarques_int_transport=F('remarques_int')
    )


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0005_client_remarques_int_alarme_and_more'),
    ]

    operations = [
        migrations.RunPython(migrate_remarques_int),
    ]
