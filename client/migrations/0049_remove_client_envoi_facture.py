from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0048_migrate_partenaire'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='envoi_facture',
        ),
    ]
