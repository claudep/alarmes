from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0037_migrate_adresse_to_adresse_client'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='c_o',
        ),
        migrations.RemoveField(
            model_name='client',
            name='rue',
        ),
        migrations.RemoveField(
            model_name='client',
            name='npa',
        ),
        migrations.RemoveField(
            model_name='client',
            name='localite',
        ),
        migrations.RemoveField(
            model_name='client',
            name='empl_geo',
        ),
    ]
