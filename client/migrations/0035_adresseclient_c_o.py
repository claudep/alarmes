from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0034_alter_client_langues'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresseclient',
            name='c_o',
            field=models.CharField(blank=True, max_length=80, verbose_name='C/o'),
        ),
    ]
