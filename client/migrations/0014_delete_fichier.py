from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0013_client_fichier_to_common'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Fichier',
        ),
    ]
