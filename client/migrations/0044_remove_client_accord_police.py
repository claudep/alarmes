from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0043_referent_case_postale'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='accord_police',
        ),
    ]
