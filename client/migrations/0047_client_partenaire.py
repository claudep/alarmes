from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0046_populate_samaritains_des'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='partenaire',
            field=models.OneToOneField(
                blank=True, null=True, on_delete=models.deletion.SET_NULL,
                related_name='partenaire_de', to='client.client'
            ),
        ),
    ]
