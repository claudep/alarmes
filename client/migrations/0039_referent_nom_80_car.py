from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0038_remove_client_adresse_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referent',
            name='nom',
            field=models.CharField(max_length=80, verbose_name='Nom'),
        ),
    ]
