import common.fields
from django.db import migrations, models


def savd_to_osad(apps, schema_editor):
    Alerte = apps.get_model('client', 'Alerte')
    Client = apps.get_model('client', 'Client')
    Prestation = apps.get_model('client', 'Prestation')

    Alerte.objects.filter(cible='savd').update(cible='osad')
    Prestation.objects.filter(service='savd').update(service='osad')
    Client.objects.filter(type_client=['savd']).update(type_client=['osad'])
    for client in Client.objects.filter(type_client__contains=['savd']):
        client.type_client = [('osad' if tp == 'savd' else tp) for tp in client.type_client]
        client.save(update_fields=['type_client'])


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0020_client_id_externe2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alerte',
            name='cible',
            field=models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD'), ('donateur', 'Donateur')], max_length=10),
        ),
        migrations.AlterField(
            model_name='client',
            name='type_client',
            field=common.fields.ChoiceArrayField(base_field=models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD')], max_length=10), blank=True, size=None),
        ),
        migrations.AlterField(
            model_name='prestation',
            name='service',
            field=models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD'), ('donateur', 'Donateur')], max_length=12),
        ),
        migrations.RunPython(savd_to_osad),
    ]
