from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0028_is_avs_function"),
    ]

    # Nouveau calcul selon révision AVS 21
    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_avs(
    event_date date, tarif_avs_des date, date_naissance date, genre varchar(1)
)
RETURNS BOOLEAN AS $$
DECLARE is_avs boolean;
        age_int integer := EXTRACT(YEAR FROM age(event_date, date_naissance));
        age_trunc_m interval := date_trunc('month', age(event_date, date_naissance));
        year_naiss integer := EXTRACT(YEAR FROM date_naissance);
BEGIN
    SELECT (tarif_avs_des IS NOT NULL AND tarif_avs_des <= event_date) OR age_int >= 65 OR (
        genre = 'F' AND year_naiss <= 1960 AND age_int >= 64) OR (
        genre = 'F' AND year_naiss <= 1961 AND age_trunc_m >= interval '64 years 3 mons') OR (
        genre = 'F' AND year_naiss <= 1962 AND age_trunc_m >= interval '64 years 6 mons') OR (
        genre = 'F' AND year_naiss <= 1963 AND age_trunc_m >= interval '64 years 9 mons')
    INTO is_avs;
    RETURN is_avs;
END
$$
LANGUAGE plpgsql
IMMUTABLE;""")
    ]
