from django.db import migrations, models
from common.fields import PhoneNumberField


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0062_referent_pers_contact'),
    ]

    operations = [
        migrations.CreateModel(
            name='Telephone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tel', PhoneNumberField(max_length=18, verbose_name='Numéro')),
                ('priorite', models.PositiveSmallIntegerField(verbose_name='Priorité')),
                ('remarque', models.CharField(blank=True, max_length=80, verbose_name='Remarque')),
                ('persona', models.ForeignKey(on_delete=models.deletion.CASCADE, to='client.persona')),
            ],
            options={
                'verbose_name': 'Téléphone',
                'verbose_name_plural': 'Téléphones',
            },
        ),
    ]
