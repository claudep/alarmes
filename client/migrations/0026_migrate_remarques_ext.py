from django.db import migrations


def migrate_remarques_ext(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    for client in Client.objects.exclude(remarques_ext=''):
        tous_services = set(p.service for p in client.prestations.all())
        if not tous_services:
            raise Exception('Client avec remarques mais sans prestations??')
        for serv in tous_services:
            setattr(client, f'remarques_ext_{serv}', client.remarques_ext)
        client.save()


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0025_client_remarques_ext_apps'),
    ]

    operations = [
        migrations.RunPython(migrate_remarques_ext, migrations.RunPython.noop),
    ]
