import common.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0008_referent_cree_le'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referent',
            name='facturation_pour',
            field=common.fields.ChoiceArrayField(
                base_field=models.CharField(blank=True, choices=[
                    ('al-abo', 'Alarme (install + abo)'), ('al-tout', 'Alarme (tout)'), ('transp', 'Transports')
                ], max_length=10), blank=True, default=list, size=None, verbose_name='Adresse de facturation pour'
            ),
        ),
    ]
