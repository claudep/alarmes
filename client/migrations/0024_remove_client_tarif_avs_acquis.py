from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0023_client_tarif_avs_des'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='tarif_avs_acquis',
        ),
    ]
