from django.db import migrations, models

from common.choices import Pays


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0051_remove_client_en_home'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresseclient',
            name='pays',
            field=models.CharField(choices=Pays, default='CH', max_length=2, verbose_name='Pays'),
        ),
    ]
