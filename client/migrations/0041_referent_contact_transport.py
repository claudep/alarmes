from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0040_referent_nom_100_car'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='contact_transport',
            field=models.BooleanField(default=False, verbose_name='Contact pour les transports'),
        ),
    ]
