from django.db import migrations


def populate_archive_le(apps, schema_editor):
    return
    Client = apps.get_model('client', 'Client')
    Persona = apps.get_model('client', 'Persona')
    for pers in Persona.objects.filter(archive_le__isnull=False):
        Client.objects.filter(persona__id=pers.pk).update(archive_le=pers.archive_le)


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0058_readd_client_archive_le'),
    ]

    operations = [
        migrations.RunPython(populate_archive_le, migrations.RunPython.noop),
    ]
