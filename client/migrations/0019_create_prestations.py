from django.db import migrations

from common.choices import Services


def create_prestations(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    Prestation = apps.get_model('client', 'Prestation')

    # Les migrations pour les autres services se trouvent dans leur application respective.
    # osad
    for client in Client.objects.filter(type_client__contains=['savd']):
        ajout = client.journaux.filter(description__contains="Ajout comme client OSAD").first()
        if ajout:
            debut = ajout.quand.date()
        else:
            debut = client.journaux.order_by('quand').first().quand.date()  # création client
        fin = client.archive_le
        if fin and fin < debut:
            fin = debut
        Prestation.objects.create(client=client, service=Services.OSAD, duree=(debut, fin))


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0018_prestation'),
    ]

    operations = [
        migrations.RunPython(create_prestations),
    ]
