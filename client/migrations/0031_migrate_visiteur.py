from django.db import migrations


def migrate_visiteur(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    visiteurs_sans_utils =  Client.objects.filter(
        visiteur_old__isnull=False
    ).filter(visiteur_old__utilisateur=None)
    if visiteurs_sans_utils.count() > 0:
        raise Exception(
            "Les visiteurs suivants n’ont pas d’utilisateur: %s" %  ", ".join([
                f"{vis.nom} {vis.prenom}" for vis in visiteurs_sans_utils
            ])
        )
    for client in Client.objects.filter(visiteur_old__isnull=False):
        client.visiteur_id=client.visiteur_old.utilisateur_id
        client.save(update_fields=["visiteur_id"])


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '__first__'),
        ('client', '0030_client_visiteur_to_utilisateur'),
    ]

    operations = [
        migrations.RunPython(migrate_visiteur),
    ]
