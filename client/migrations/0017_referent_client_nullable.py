from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0016_alter_journal_quand'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referent',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, to='client.client'),
        ),
    ]
