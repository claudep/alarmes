import django.contrib.postgres.fields.ranges
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0044_remove_client_accord_police'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='samaritains_des',
            field=django.contrib.postgres.fields.ranges.DateRangeField(blank=True, null=True, verbose_name='Prestation Samaritains'),
        ),
        migrations.AddField(
            model_name='profclient',
            name='cree_le',
            field=models.DateTimeField(auto_now_add=True, default='2020-01-01', verbose_name='Créé le'),
            preserve_default=False,
        ),
    ]
