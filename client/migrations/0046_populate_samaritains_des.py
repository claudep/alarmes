from django.db import migrations


def populate_samas_des(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    for client in Client.objects.exclude(samaritains=None):
        journ = client.journaux.filter(description="Ajout des samaritains comme répondants").first()
        if not journ:
            print(f"Impossible de trouver la date d’ajout des Samas pour {client}")
            client.samaritains_des = ("2022-11-15", None)
        else:
            client.samaritains_des = (journ.quand.date(), None)
        client.save(update_fields=["samaritains_des"])


class Migration(migrations.Migration):
    dependencies = [
        ('client', '0045_client_samaritains_des_profclient_cree_le'),
    ]

    operations = [
        migrations.RunPython(populate_samas_des, migrations.RunPython.noop),
    ]
