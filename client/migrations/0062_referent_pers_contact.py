from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0061_remove_referent_no_debiteur'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='pers_contact',
            field=models.CharField(blank=True, max_length=80, verbose_name='Personne de contact'),
        ),
    ]
