from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0019_create_prestations'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='id_externe2',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
