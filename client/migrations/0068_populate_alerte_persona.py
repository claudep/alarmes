from django.db import migrations


def populate_alerte_persona(apps, schema_editor):
    Alerte = apps.get_model('client', 'Alerte')
    for alerte in Alerte.objects.all().select_related('client'):
        alerte.persona_id = alerte.client.persona_id
        alerte.save()


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0067_alerte_add_persona'),
    ]

    operations = [
        migrations.RunPython(populate_alerte_persona),
    ]
