from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0006_migrate_remarques_int'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='remarques_int',
        ),
    ]
