from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0053_remove_obsolete_client_part_fields'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='client',
            constraint=models.UniqueConstraint(fields=('id_externe2',), name='client_id_externe2_unique'),
        ),
    ]
