from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0056_migrate_client_persona'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='client',
            name='client_id_externe2_unique',
        ),
        migrations.RemoveConstraint(
            model_name='client',
            name='client_id_externe_unique',
        ),
        migrations.RemoveField(
            model_name='client',
            name='case_postale',
        ),
        migrations.RemoveField(
            model_name='client',
            name='courriel',
        ),
        migrations.RemoveField(
            model_name='client',
            name='cree_le',
        ),
        migrations.RemoveField(
            model_name='client',
            name='date_deces',
        ),
        migrations.RemoveField(
            model_name='client',
            name='date_naissance',
        ),
        migrations.RemoveField(
            model_name='client',
            name='genre',
        ),
        migrations.RemoveField(
            model_name='client',
            name='id_externe',
        ),
        migrations.RemoveField(
            model_name='client',
            name='id_externe2',
        ),
        migrations.RemoveField(
            model_name='client',
            name='langues',
        ),
        migrations.RemoveField(
            model_name='client',
            name='nom',
        ),
        migrations.RemoveField(
            model_name='client',
            name='prenom',
        ),
        migrations.RemoveField(
            model_name='client',
            name='tel_1',
        ),
        migrations.RemoveField(
            model_name='client',
            name='tel_2',
        ),
        migrations.RemoveField(
            model_name='adresseclient',
            name='client',
        ),
        migrations.RemoveField(
            model_name='journal',
            name='client',
        ),
        migrations.AlterField(
            model_name='adresseclient',
            name='persona',
            field=models.ForeignKey(on_delete=models.deletion.CASCADE, to='client.persona'),
        ),
        migrations.AlterField(
            model_name='journal',
            name='persona',
            field=models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='journaux', to='client.persona'),
        ),
    ]
