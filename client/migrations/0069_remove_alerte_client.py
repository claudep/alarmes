from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0068_populate_alerte_persona'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alerte',
            name='client',
        ),
        migrations.AlterField(
            model_name='alerte',
            name='persona',
            field=models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='alertes', to='client.persona'),
        ),
    ]
