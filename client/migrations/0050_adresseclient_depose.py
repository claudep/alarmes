from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0049_remove_client_envoi_facture'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresseclient',
            name='depose',
            field=models.BooleanField(default=False, help_text='Adresse où se rendre pour chercher ou déposer la personne', verbose_name='Adresse de dépose'),
        ),
    ]
