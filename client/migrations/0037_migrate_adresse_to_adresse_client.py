from datetime import date
from django.db import migrations


def migrate_adresses(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    Journal = apps.get_model('client', 'Journal')
    AdresseClient = apps.get_model('client', 'AdresseClient')
    for client in Client.objects.all():
        try:
            creation = client.journaux.earliest('quand').quand.date()
        except Journal.DoesNotExist:
            creation = date(2020, 1, 1)
        AdresseClient.objects.create(
            client=client,
            principale=(creation, None),
            c_o=client.c_o,
            rue=client.rue,
            npa=client.npa,
            localite=client.localite,
            empl_geo=client.empl_geo,
        )


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0036_adresseclient_principale'),
    ]

    operations = [
        migrations.RunPython(migrate_adresses, migrations.RunPython.noop),
    ]
