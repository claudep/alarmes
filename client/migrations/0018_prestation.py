import django.contrib.postgres.fields.ranges
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0017_referent_client_nullable'),
    ]

    operations = [
        migrations.CreateModel(
            name='Prestation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service', models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('savd', 'Soins à domicile')], max_length=12)),
                ('duree', django.contrib.postgres.fields.ranges.DateRangeField(verbose_name='Durée')),
                ('client', models.ForeignKey(
                    on_delete=models.deletion.CASCADE, to='client.client', related_name='prestations'
                )),
            ],
        ),
    ]
