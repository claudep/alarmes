from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0041_referent_contact_transport'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='no_sinistre',
            field=models.CharField(blank=True, max_length=20, verbose_name='N° de sinistre'),
        ),
    ]
