from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0029_is_avs_function_revision21'),
    ]

    operations = [
        migrations.RenameField(
            model_name='client',
            old_name='visiteur',
            new_name='visiteur_old',
        ),
        migrations.RunSQL('DROP INDEX "alarme_client_visiteur_id_65fe1b77"'),
        migrations.AddField(
            model_name='client',
            name='visiteur',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL, verbose_name='Visiteur·euse', related_name="visites"
            ),
        ),
    ]
