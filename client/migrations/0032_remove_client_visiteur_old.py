from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0031_migrate_visiteur'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='visiteur_old',
        ),
    ]
