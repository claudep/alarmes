from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0060_remove_persona_archive_le'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='referent',
            name='no_debiteur',
        ),
    ]
