from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0066_remove_client_persona_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='alerte',
            name='persona',
            field=models.ForeignKey(null=True, on_delete=models.deletion.CASCADE, to='client.persona'),
        ),
        migrations.AlterField(
            model_name='alerte',
            name='cible',
            field=models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD')], max_length=10),
        ),
        migrations.AlterField(
            model_name='prestation',
            name='service',
            field=models.CharField(choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD')], max_length=12),
        ),
        migrations.AlterModelTable(
            name='alerte',
            table=None,
        ),
    ]
