from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0064_migrate_persona_telephones'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persona',
            name='tel_1',
        ),
        migrations.RemoveField(
            model_name='persona',
            name='tel_2',
        ),
    ]
