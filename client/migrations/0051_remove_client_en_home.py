from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0050_adresseclient_depose'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='en_home',
        ),
    ]
