from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0021_service_savd_to_osad'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='type_client',
        ),
    ]
