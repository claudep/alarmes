from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0022_remove_client_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='tarif_avs_des',
            field=models.DateField(blank=True, null=True, verbose_name='Tarif AVS dès'),
        ),
    ]
