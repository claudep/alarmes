from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0059_repopulate_archive_le'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persona',
            name='archive_le',
        ),
    ]
