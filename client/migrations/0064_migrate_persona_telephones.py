from django.db import migrations


def migrate_telephones(apps, schema_editor):
    Persona = apps.get_model('client', 'Persona')
    Telephone = apps.get_model('client', 'Telephone')

    for persona in Persona.objects.exclude(tel_1="", tel_2=""):
        if persona.tel_1:
            Telephone.objects.create(persona=persona, tel=persona.tel_1, priorite=1)
        if persona.tel_2:
            Telephone.objects.create(persona=persona, tel=persona.tel_2, priorite=2 if persona.tel_1 else 1)


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0063_persona_telephones'),
    ]

    operations = [
        migrations.RunPython(migrate_telephones, migrations.RunPython.noop),
    ]
