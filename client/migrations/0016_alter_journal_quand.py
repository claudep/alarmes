from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0015_referent_id_externe'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='quand',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
