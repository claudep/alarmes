from django.db import migrations
from django.utils import timezone


def migrate_partenaire(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    Prestation = apps.get_model('client', 'Prestation')
    Journal = apps.get_model('client', 'Journal')
    for client in Client.objects.exclude(nom_part=''):
        # Already existing?
        existing = Client.objects.filter(
            nom=client.nom_part, prenom=client.prenom_part, date_naissance=client.date_naissance_part
        ).first()
        if existing:
            client.partenaire = existing
        else:
            client.partenaire = Client.objects.create(
                nom=client.nom_part, prenom=client.prenom_part, genre=client.genre_part,
                date_naissance=client.date_naissance_part, tel_1=client.tel_part,
                archive_le=client.archive_le,
            )
            client_prest = client.prestations.filter(service='alarme').latest('duree')
            Prestation.objects.create(
                client=client.partenaire, service='alarme', duree=client_prest.duree,
            )
            # Copier l'adresse
            client_adr = client.adresseclient_set.exclude(principale__isnull=True).latest('principale')
            client_adr.pk = None
            client_adr._state.adding = True
            client_adr.client = client.partenaire
            client_adr.save()
            Journal.objects.create(
                client=client.partenaire,
                description=f"Création de la personne, partenaire de {client}",
                quand=timezone.now(), qui=None
            )
        client.save(update_fields=["partenaire"])
        # Déplacer émetteur vers partenaire
        if mat := client.materielclient_set.first():
            mat.client = client.partenaire
            mat.save() 


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0047_client_partenaire'),
    ]

    operations = [
        migrations.RunPython(migrate_partenaire),
    ]
