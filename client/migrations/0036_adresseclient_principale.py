from django.contrib.postgres.fields.ranges import DateRangeField
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0035_adresseclient_c_o'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresseclient',
            name='principale',
            field=DateRangeField(blank=True, null=True),
        ),
    ]
