from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0024_remove_client_tarif_avs_acquis'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='remarques_ext_alarme',
            field=models.TextField(blank=True, verbose_name='Remarques (visibles par bénévoles alarme)'),
        ),
        migrations.AddField(
            model_name='client',
            name='remarques_ext_transport',
            field=models.TextField(blank=True, verbose_name='Remarques (visibles par bénévoles transports)'),
        ),
        migrations.AddField(
            model_name='client',
            name='remarques_ext_visite',
            field=models.TextField(blank=True, verbose_name='Remarques (visibles par bénévoles visites)'),
        ),
    ]
