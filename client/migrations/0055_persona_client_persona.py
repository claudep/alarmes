import django.contrib.postgres.fields
import django.db.models.deletion
from django.db import migrations, models

from client.models import PersonneMixin
from common.choices import Languages
from common.fields import PhoneNumberField


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0054_client_id_externe2_unique'),
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_externe', models.BigIntegerField(blank=True, null=True)),
                ('id_externe2', models.BigIntegerField(blank=True, null=True)),
                ('nom', models.CharField(max_length=60, verbose_name='Nom')),
                ('prenom', models.CharField(blank=True, max_length=50, verbose_name='Prénom')),
                ('genre', models.CharField(blank=True, choices=[('F', 'Femme'), ('M', 'Homme')], max_length=1, verbose_name='Genre')),
                ('date_naissance', models.DateField(blank=True, null=True, verbose_name='Date de naissance')),
                ('case_postale', models.CharField(
                    blank=True, max_length=20, verbose_name='Case postale',
                    help_text="Inclure «C.P.» ou «Case postale» dans le champ"
                )),
                ('tel_1', PhoneNumberField(blank=True, max_length=18, verbose_name='Tél. principal')),
                ('tel_2', PhoneNumberField(blank=True, max_length=18, verbose_name='Tél. secondaire')),
                ('courriel', models.EmailField(blank=True, max_length=254, verbose_name='Courriel')),
                ('langues', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, choices=Languages.choices(), max_length=3), blank=True, null=True, size=None)),
                ('cree_le', models.DateTimeField(verbose_name='Créé le')),
                ('archive_le', models.DateField(blank=True, null=True, verbose_name='Archivé le')),
                ('date_deces', models.DateField(blank=True, null=True, verbose_name='Décès le')),
            ],
            bases=(PersonneMixin, models.Model),
        ),
        migrations.AddField(
            model_name='client',
            name='persona',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='client.persona'),
        ),
        migrations.AddConstraint(
            model_name='persona',
            constraint=models.UniqueConstraint(fields=('id_externe',), name='persona_id_externe_unique'),
        ),
        migrations.AddConstraint(
            model_name='persona',
            constraint=models.UniqueConstraint(fields=('id_externe2',), name='persona_id_externe2_unique'),
        ),
        migrations.AddField(
            model_name='adresseclient',
            name='persona',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='client.persona'),
        ),
        migrations.AddField(
            model_name='journal',
            name='persona',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='client.persona'),
        ),
    ]
