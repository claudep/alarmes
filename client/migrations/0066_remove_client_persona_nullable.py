from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0065_remove_persona_tel_1_and_2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='persona',
            field=models.OneToOneField(on_delete=models.deletion.CASCADE, to='client.persona'),
        ),
    ]
