from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="professionnel",
            name="inactif",
            field=models.BooleanField(
                default=False, verbose_name="Temporairement inactif"
            ),
        ),
    ]
