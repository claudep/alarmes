from django.contrib.postgres.fields import ArrayField
from django.db import migrations, models

from common.choices import Languages


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0033_client_en_home'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='langues',
            field=ArrayField(
                base_field=models.CharField(blank=True, choices=Languages.choices(), max_length=3),
                blank=True, null=True, size=None
            ),
        ),
    ]
