from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0042_referent_no_sinistre'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='case_postale',
            field=models.CharField(
                blank=True, max_length=20, verbose_name='Case postale',
                help_text="Inclure «C.P.» ou «Case postale» dans le champ"
            ),
        ),
    ]
