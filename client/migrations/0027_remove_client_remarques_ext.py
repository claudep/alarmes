from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0026_migrate_remarques_ext'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='remarques_ext',
        ),
    ]
