from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0032_remove_client_visiteur_old'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='en_home',
            field=models.BooleanField(default=False, verbose_name='En home/EMS'),
        ),
    ]
