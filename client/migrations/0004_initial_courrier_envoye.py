from datetime import timedelta
from operator import attrgetter

from django.db import migrations


def migrate_courrier_envoye(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    for client in Client.objects.filter(
        archive_le__isnull=True, type_client__contains=['alarme']
    ).prefetch_related('installation_set'):
        installs = list(client.installation_set.all())
        if not installs:
            continue
        premiere = sorted(installs, key=attrgetter('date_debut'))[0]
        client.courrier_envoye = premiere.date_debut + timedelta(days=7)
        client.save(update_fields=['courrier_envoye'])


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0003_client_courrier_envoye"),
    ]

    operations = [
        migrations.RunPython(migrate_courrier_envoye)
    ]
