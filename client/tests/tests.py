from datetime import date, timedelta
from io import StringIO
from pathlib import Path
from unittest import skipIf
from unittest.mock import patch

from freezegun import freeze_time

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.files import File
from django.core.management import call_command
from django.db.models import F
from django.template import Context, Template
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone

from api import signals
from common.choices import Services
from common.export import openxml_contenttype
from common.functions import IsAVS
from common.models import Fichier, Utilisateur
from common.test_utils import TempMediaRootMixin, mocked_httpx
from common.utils import canton_abrev, client_url_name, current_app

from ..forms import PersonaForm, ReferentForm
from ..models import (
    AdresseClient, AdressePresence, Client, Persona, Prestation, Professionnel,
    ProfClient, Referent
)
from ..templatetags.client_utils import bandeau_absence

NOT_SET = object()


class DataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_client', 'change_client', 'change_professionnel'
            ]))
        )

    def assertJournalMessage(self, obj, message, user=NOT_SET):
        journal = obj.journaux.latest()
        self.assertEqual(journal.description, message)
        self.assertEqual(journal.qui, self.user if user is NOT_SET else user)


class ClientTests(TempMediaRootMixin, DataMixin, TestCase):

    def test_titre(self):
        client = Client(
            persona=Persona(nom='Dupond', prenom='Jules', genre='M'),
        )
        self.assertEqual(client.titre, "Monsieur")
        client = Client(
            persona=Persona(nom='Dupond', prenom='Julie', genre='F'),
        )
        self.assertEqual(client.titre, "Madame")

    def test_noms_prenoms(self):
        client = Client(
            persona=Persona(nom='Dupond', prenom='Jules', genre='M'),
            partenaire=Client.objects.create(nom='Dupond', prenom='Julie', genre='F'),
        )
        self.assertEqual(client.noms, 'Dupond')
        self.assertEqual(client.prenoms, 'Jules et Julie')
        client = Client(
            persona=Persona(nom='Dupond', prenom='Jules', genre='M'),
            partenaire=Client.objects.create(nom='Meier', prenom='Julie', genre='F'),
        )
        self.assertEqual(client.noms, 'Dupond et Meier')
        self.assertEqual(client.prenoms, 'Jules et Julie')

    def test_adresse(self):
        def _test_with_client(client):
            adresse = client.adresse()
            self.assertIsInstance(adresse, AdresseClient)
            self.assertEqual(adresse.c_o, 'Maison Bleue')
            self.assertEqual(adresse.rue, 'Av. Charles 12')
            self.assertEqual(adresse.npa, '2000')
            self.assertEqual(adresse.localite, 'Neuchâtel')

        client_obj = Client.objects.create(
            nom='Dupond', prenom='Jules',
            c_o="Maison Bleue", rue='Av. Charles 12', npa='2000', localite='Neuchâtel'
        )
        _test_with_client(client_obj)
        with self.assertNumQueries(1):
            client_from_qs = Client.objects.filter(persona__nom='Dupond').avec_adresse(date.today()
                ).select_related("persona").first()
            _test_with_client(client_from_qs)

    def test_adresse_depose_si_hospit(self):
        client_obj = Client.objects.create(
            nom='Dupond', prenom='Jules',
            c_o="Maison Bleue", rue='Av. Charles 12', npa='2000', localite='Neuchâtel'
        )
        AdressePresence.objects.create(
            adresse=AdresseClient.objects.create(persona=client_obj.persona, hospitalisation=True),
            depuis=date.today() - timedelta(days=5)
        )
        # L'hospitalisation n'est pas considérée comme une adresse de dépose.
        self.assertEqual(
            client_obj.persona.adresses.depose(date.today()).rue, "Av. Charles 12"
        )

    def test_region(self):
        client_obj = Client.objects.create(
            nom='Dupond', prenom='Jules',
            c_o="Maison Bleue", rue='Av. Charles 12', npa='2000', localite='Neuchâtel'
        )
        self.assertEqual(client_obj.persona.region, "Neuchâtel" if canton_abrev() == "NE" else "Autre")

    def test_langues(self):
        persona = Persona(nom='Dupond', prenom='Jules', genre='M', langues=None)
        self.assertEqual(persona.get_langues_display(), "")
        persona.langues = []
        self.assertEqual(persona.get_langues_display(), "")
        persona.langues = ['fr']
        self.assertEqual(persona.get_langues_display(), "français")
        persona.langues = ['fr', 'de', 'es']
        self.assertEqual(persona.get_langues_display(), "français, allemand, espagnol")

    @skipIf(client_url_name() == "", "Pas de possibilité d'édition client dans l'app actuelle")
    def test_info_partenaire(self):
        client = Client.objects.create(nom='Dupond', prenom='Jules', genre='M')
        templ = Template('{% load client_utils %}{% info_partenaire client %}')
        output = templ.render(Context({"client": client}))
        self.assertEqual(output, "\n")
        client.partenaire = Client.objects.create(nom='Dupond', prenom='Julie', genre='F')
        client.save()
        output = templ.render(Context({"client": client}))
        edit_url = reverse(client_url_name(), args=[client.partenaire_id])
        self.assertHTMLEqual(
            output,
            '<div class="alert alert-info">Cette personne vit dans le même logement '
            f'que <a href="{edit_url}">Dupond Julie</a></div>'
        )
        output = templ.render(Context({"client": client.partenaire}))
        edit_url = reverse(client_url_name(), args=[client.pk])
        self.assertHTMLEqual(
            output,
            '<div class="alert alert-info">Cette personne vit dans le même logement '
            f'que <a href="{edit_url}">Dupond Jules</a></div>'
        )

    def test_par_et_avec_service(self):
        cl1 = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        cl2 = Client.objects.create(
            nom="Lafleur", prenom="Justine", service=[Services.TRANSPORT]
        )
        cl3 = Client.objects.create(nom="Nom", prenom="Sans")
        self.assertQuerySetEqual(
            Client.objects.par_service(Services.TRANSPORT).order_by('persona__nom'),
            [cl1, cl2]
        )
        self.assertQuerySetEqual(
            Client.objects.par_service(Services.TRANSPORT, strict=True),
            [cl2]
        )
        self.assertEqual(
            [set(cl.services) for cl in Client.objects.avec_services().order_by('persona__nom')],
            [{'alarme', 'transport'}, {'transport'}, set()]
        )

    def test_client_avs_femmes(self):
        """
        Voir https://www.bsv.admin.ch/bsv/fr/home/informations-aux/faq.html?
             faq-url=/fr/reforme-avs-21/comment-lage-de-la-retraite-des-femmes-est-il-releve
        """
        now = timezone.now()
        personas = Persona.objects.bulk_create([
            Persona(nom='F01', genre='F', date_naissance=date(1960, 12, 30), cree_le=now),
            Persona(nom='F02', genre='F', date_naissance=date(1961, 1, 15), cree_le=now),
            Persona(nom='F03', genre='F', date_naissance=date(1962, 1, 15), cree_le=now),
            Persona(nom='F04', genre='F', date_naissance=date(1963, 1, 15), cree_le=now),
            Persona(nom='F05', genre='F', date_naissance=date(1964, 1, 15), cree_le=now),
        ])
        cl1960, cl1961, cl1962, cl1963, cl1964 = Client.objects.bulk_create(
            [Client(persona=p) for p in personas]
        )

        def get_client_today(client):
            return Client.objects.annotate(
                is_avs_db=IsAVS(date.today(), None, F('persona__date_naissance'), F('persona__genre'))
            ).get(pk=client.pk)

        # Aucun changement en 2024
        with freeze_time("2024-12-29"):
            self.assertIs(get_client_today(cl1960).is_avs_db, False)
        with freeze_time("2024-12-31"):
            self.assertIs(get_client_today(cl1960).is_avs_db, True)

        # 64 + 3 mois en 2025
        with freeze_time("2025-04-14"):
            self.assertIs(get_client_today(cl1961).is_avs_db, False)
            self.assertIs(cl1961.is_avs(date.today()), False)
        with freeze_time("2025-04-16"):
            self.assertIs(get_client_today(cl1961).is_avs_db, True)
            self.assertIs(cl1961.is_avs(date.today()), True)

        # 64 + 6 mois en 2026
        with freeze_time("2026-07-14"):
            self.assertIs(get_client_today(cl1962).is_avs_db, False)
        with freeze_time("2026-07-16"):
            self.assertIs(get_client_today(cl1962).is_avs_db, True)

        # 64 + 9 mois en 2027
        with freeze_time("2027-10-14"):
            self.assertIs(get_client_today(cl1963).is_avs_db, False)
        with freeze_time("2027-10-16"):
            self.assertIs(get_client_today(cl1963).is_avs_db, True)

        # 65 dès 2028
        with freeze_time("2028-12-31"):
            self.assertIs(get_client_today(cl1964).is_avs_db, False)
        with freeze_time("2029-01-16"):
            self.assertIs(get_client_today(cl1964).is_avs_db, True)

    def test_client_from_persona(self):
        persona = Persona.objects.create(nom="Muller", prenom="Juju")
        self.client.force_login(self.user)
        url = reverse("client-from-persona", args=[persona.pk])
        response = self.client.get(url)
        self.assertContains(response, "Muller")
        response = self.client.post(url, data={})
        client = persona.get_client()
        self.assertRedirects(response, client.get_absolute_url())
        self.assertEqual(len(client.prestations_actuelles()), 1)

    def test_client_ajout_fichier(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        self.client.force_login(self.user)
        url = reverse('client-fichier-add', args=[client.pk])
        response = self.client.get(url)
        self.assertEqual(
            [ch[0] for ch in response.context['form'].fields['typ'].choices],
            ['doc', 'quest', 'contrat'] if current_app() == 'alarme' else ['doc']
        )
        with Path(__file__).open(mode='rb') as fh:
            response = self.client.post(url, data={
                'titre': 'Titre',
                'typ': 'doc',
                'fichier': File(fh),
            })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#fichiers-container'})
        self.assertEqual(client.fichiers.count(), 1)
        self.assertJournalMessage(client, "Ajout d’un fichier: Titre")
        response = self.client.get(reverse("client-fichier-list", args=[client.pk]))
        self.assertContains(response, '<a href="/media/fichiers/persona/tests.py">Titre</a>')

    def test_client_edit_fichier(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        with Path(__file__).open(mode='rb') as fh:
            fichier = Fichier.objects.create(
                content_object=client.persona, titre="Document", typ='doc',
                fichier=File(fh, name="test.py"), qui=self.user, quand=timezone.now()
            )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('client-fichier-edit', args=[client.pk, fichier.pk]), data={
                'titre': 'Autre document',
                'typ': 'doc',
            },
        )
        fichier.refresh_from_db()
        self.assertEqual(fichier.titre, "Autre document")

    def test_client_delete_fichier(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        with Path(__file__).open(mode='rb') as fh:
            fichier = Fichier.objects.create(
                content_object=client.persona, titre="Document", typ='doc',
                fichier=File(fh, name="test.py"), qui=self.user, quand=timezone.now()
            )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('client-fichier-delete', args=[fichier.pk]), data={
                'titre': 'Autre document',
                'typ': 'doc',
            },
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#fichiers-container'})
        client.refresh_from_db()
        client = Client.objects.get(pk=client.pk)
        self.assertEqual(client.fichiers.count(), 0)

    def test_client_autocomplete(self):
        now = timezone.now()
        personas = Persona.objects.bulk_create([
            Persona(nom="Smirnov", prenom="Luce", cree_le=now),
            Persona(nom="Smitter", prenom="Ladislas", cree_le=now),
            Persona(nom="Smith", prenom="John", cree_le=now),
            Persona(nom="Rosmid", prenom="Esther", cree_le=now, date_naissance=date(1960, 3, 12)),
            Persona(nom="Smitter", prenom="Lisy", cree_le=now, date_deces=date(2021, 10, 4)),
        ])
        clients = Client.objects.bulk_create([Client(persona=p) for p in personas])
        Prestation.objects.bulk_create([
            Prestation(client=clients[1], service=Services.ALARME, duree=(date.today(), None)),
            Prestation(client=clients[2], service=Services.TRANSPORT, duree=(date.today(), None)),
            Prestation(client=clients[3], service=Services.ALARME, duree=(date.today(), None)),
            Prestation(client=clients[3], service=Services.TRANSPORT, duree=(date.today(), None)),
            Prestation(client=clients[4], service=Services.TRANSPORT, duree=(date.today(), None)),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-autocomplete', args=['tous']))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse('client-autocomplete', args=['tous']) + '?q=smi')
        self.assertEqual(len(response.json()), 4)
        response = self.client.get(reverse('client-autocomplete', args=['transport']) + '?q=smi')
        self.assertEqual(len(response.json()), 2)
        response = self.client.get(reverse('client-autocomplete', args=['transport']) + '?q=smi%20jo')
        self.assertEqual(response.json()[0]["label"], "Smith John")
        response = self.client.get(reverse('client-autocomplete', args=['tous']) + '?q=smi%2B')  # %2B='+'
        self.assertEqual(len(response.json()), 0)

    def test_persona_autocomplete(self):
        now = timezone.now()
        personas = Persona.objects.bulk_create([
            Persona(nom="Smirnov", prenom="Luce", cree_le=now),
            Persona(nom="Smitter", prenom="Ladislas", cree_le=now),
            Persona(nom="Smith", prenom="John", cree_le=now),
            Persona(nom="Rosmid", prenom="Esther", cree_le=now, date_naissance=date(1960, 3, 12)),
            Persona(nom="Smitter", prenom="Lisy", cree_le=now, date_deces=date(2021, 10, 4)),
        ])
        AdresseClient.objects.create(
            persona=personas[3], npa="2354", localite="Goumois",
            principale=(date.today(), None)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("persona-autocomplete"))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse("persona-autocomplete") + '?q=smi')
        self.assertEqual(len(response.json()), 4)
        response = self.client.get(reverse("persona-autocomplete") + '?q=smi%20jo')
        self.assertEqual(response.json()[0]["label"], "Smith John")
        response = self.client.get(
            reverse("persona-autocomplete") + (
                "?prenom=Esth&dateNaiss=1960-03-12&npaLoc=2354%20Goumois"
            )
        )
        self.assertEqual(response.json()[0]["label"], "Rosmid Esther")

    def test_persona_form(self):
        form = PersonaForm()
        self.assertIn('class="form-control autocomplete"', form["langues_select"].as_widget())

    def test_adresse_principale_edit_validite(self):
        """Si seule la date de validité de l'adresse change, ne pas créer de nouvelle adresse."""
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des='2024-04-03',
            service=[Services.ALARME, Services.TRANSPORT],
        )
        adresse = client.adresse()
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('persona-adresse-princ-edit', args=[client.adresse().pk]),
            data={
                'des_le': '2024-04-23',
                'rue': adresse.rue,
                'npa': adresse.npa, 'localite': adresse.localite,
                'npalocalite': f"{adresse.npa} {adresse.localite}",
            }
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#clientAdresseBloc'})
        client.refresh_from_db()
        self.assertEqual(len(client.adresses), 1)
        self.assertJournalMessage(
            client, "Validité d’adresse modifiée du 03.04.2024 au 23.04.2024"
        )

    @skipIf(client_url_name() == "", "Pas de possibilité d'édition client dans l'app actuelle")
    @override_settings(QUERY_GEOADMIN_FOR_ADDRESS=True)
    def test_adresse_principale_edit(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des='2024-04-03',
            service=[Services.ALARME, Services.TRANSPORT],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertContains(response, "Cette adresse n’a pas pu être géolocalisée")
        edit_url = reverse('persona-adresse-princ-edit', args=[client.adresse().pk])
        response = self.client.get(edit_url)
        self.assertContains(response, "1234 Quelquepart")

        # Ajout nouvelle adresse
        form_data = {
            "des_le": date(2024, 1, 1),  # Trop tôt!
            "rue": "Rue du Soleil 12",
            "npalocalite": "4321 Ailleurs",
        }
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(edit_url, data=form_data)
            self.assertContains(response, "Cette date ne peut pas être plus ancienne")
            des_le = form_data["des_le"] = date.today() + timedelta(days=4)
            response = self.client.post(edit_url, data=form_data)
            mock.assert_called()
            client.refresh_from_db()
            self.assertEqual(client.adresse(des_le).localite, "Ailleurs")
            self.assertEqual(client.adresse(des_le).empl_geo, [1.0, 2.0])
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#clientAdresseBloc'})
        self.assertJournalMessage(
            client,
            "Ancienne adresse: Rue de la Paix 36, 1234 Quelquepart<br>"
            f"Nouvelle adresse dès le {des_le.strftime('%d.%m.%Y')}: Rue du Soleil 12, 4321 Ailleurs"
        )
        self.assertEqual(
            client.alertes.latest().alerte,
            f"Modifications depuis l’application «{current_app()}»: "
            "Ancienne adresse: Rue de la Paix 36, 1234 Quelquepart<br>"
            f"Nouvelle adresse dès le {des_le.strftime('%d.%m.%Y')}: Rue du Soleil 12, 4321 Ailleurs"
        )
        # Ancienne et nouvelle adresses sont accessibles
        new, old = client.adresse(des_le), client.adresse()
        self.assertNotEqual(old, new)
        self.assertEqual(new.principale.lower - old.principale.upper, timedelta(days=1))

    @override_settings(QUERY_GEOADMIN_FOR_ADDRESS=True)
    def test_adresse_principale_edit_non_geoloc(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des='2024-04-03',
            service=[Services.ALARME, Services.TRANSPORT],
        )
        self.client.force_login(self.user)
        edit_url = reverse('persona-adresse-princ-edit', args=[client.adresse().pk])
        des_le = date.today() + timedelta(days=4)
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(edit_url, data={
                'des_le': des_le,
                'rue': "Rue du Soleil 12",
                'npalocalite': "4321 Introuvable",
            })
            mock.assert_called_once()
            # La gélocalisation est obligatoire pour les clients transports
            self.assertEqual(
                response.context['form'].errors,
                {'__all__': ['Impossible de géolocaliser cette adresse.']}
            )
        client.prestations.filter(service=Services.TRANSPORT).update(duree=('2022-01-01', '2023-12-31'))
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(edit_url, data={
                'des_le': des_le,
                'rue': "Rue du Soleil 12",
                'npalocalite': "4321 Introuvable",
            })
            mock.assert_called_once()
            self.assertEqual(response.json()['result'], "OK")
        client.refresh_from_db()
        self.assertIsNone(client.adresse(des_le).empl_geo)
        self.assertJournalMessage(client, (
            "Ancienne adresse: Rue de la Paix 36, 1234 Quelquepart<br>"
            f"Nouvelle adresse dès le {des_le.strftime('%d.%m.%Y')}: Rue du Soleil 12, 4321 Introuvable<br>"
            "Attention: la nouvelle adresse n’a pas pu être géolocalisée"
        ))

    def test_nouvelle_adresse_meme_jour(self):
        """
        Une mise à jour d'adresse le même jour que l'adresse en cours ne conserve
        pas l'ancienne adresse mais la remplace.
        """
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des=date.today(),
            service=[Services.ALARME],
        )
        client.adresses.nouvelle(rue="Fbg de l'Hôpital 1", npa="2000", localite="Neuchâtel")
        self.assertEqual(len(client.adresses), 1)

    def test_adresse_principale_delete(self):
        """Il est possible de supprimer simplement une adresse future."""
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des=date.today() - timedelta(days=120), service=[Services.ALARME],
        )
        adr_actuelle = client.adresse()
        adr_actuelle.principale = date.today() - timedelta(days=120), date.today() + timedelta(days=9)
        adr_actuelle.save()
        adr_future = AdresseClient.objects.create(
            persona=client.persona, rue="Grand-Rue 2", npa='1235', localite='Ailleurs',
            principale=(date.today() + timedelta(days=10), None)
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('persona-adresse-delete', args=[adr_future.pk]), data={}
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        client.refresh_from_db()
        self.assertEqual(len([adr for adr in client.adresses if adr.principale is not None]), 1)
        self.assertIsNone(client.adresse().principale.upper)

    def test_adresse_new(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", service=Services.ALARME)
        AdresseClient.objects.create(persona=client.persona, npa='1234', localite='Quelquepart')
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'remarque': 'Résidence de vacances en été',
            'presences-TOTAL_FORMS': '0',
            'presences-INITIAL_FORMS': '0',
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        client.refresh_from_db()
        self.assertEqual(len(client.adresses.autres()), 2)
        self.assertJournalMessage(client, "Ajout de l’adresse «2345 Petaouchnok»")

    def test_adresse_new_with_presence(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", service=Services.ALARME)
        self.client.force_login(self.user)
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
                'rue': 'Av. Belair 3',
                'npalocalite': '2345 Petaouchnok',
                'presences-TOTAL_FORMS': '1',
                'presences-INITIAL_FORMS': '0',
                'presences-0-id': '',
                'presences-0-adresse': '',
                'presences-0-depuis': (date.today() - timedelta(days=5)).strftime('%Y-%m-%d'),
                'presences-0-jusqua': (date.today() + timedelta(days=5)).strftime('%Y-%m-%d'),
            })
        if response['Content-Type'] != 'application/json':
            self.fail(str(response.context['form'].errors) + str(response.context['form'].formset.errors))
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        self.assertEqual(len(client.adresses.autres()), 1)
        self.assertEqual(client.adresses.autres()[0].presences.count(), 1)

    def test_adresse_new_hospitalisation(self):
        # Idem, mais pour hospitalisation
        client = Client.objects.create(
            nom="Dupont", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT],
        )
        self.client.force_login(self.user)
        five_days_ago = date.today() - timedelta(days=5)
        response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
            'hospitalisation': 'on',
            'npalocalite': '',
            'presences-TOTAL_FORMS': '1',
            'presences-INITIAL_FORMS': '0',
            'presences-0-depuis': five_days_ago.strftime('%Y-%m-%d'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(str(response.context['form'].errors) + str(response.context['form'].formset.errors))
        self.assertEqual(response.json()['result'], 'OK')
        message = f"Ajout d’une hospitalisation (dès le {five_days_ago.strftime('%d.%m.%Y')})"
        self.assertJournalMessage(client, message)
        self.assertEqual(
            client.alertes.first().alerte,
            f'Modifications depuis l’application «{current_app()}»: {message}'
        )

    def test_bandeau_absence(self):
        client = Client.objects.create(
            nom="Dupont", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT],
        )
        adr_pres = AdressePresence.objects.create(
            adresse=AdresseClient.objects.create(persona=client.persona, npa='1234', localite='Quelquepart'),
            depuis=date.today() - timedelta(days=5),
        )
        self.assertEqual(
            bandeau_absence(client),
            '<div class="alert alert-danger">Actuellement à 1234 Quelquepart</div>'
        )
        adr_pres.depuis = date.today() + timedelta(days=5)
        adr_pres.save()
        client.refresh_from_db()
        self.assertEqual(
            bandeau_absence(client),
            f'<div class="alert alert-danger"><b>Dès le {adr_pres.depuis.strftime("%d.%m.%Y")}:</b> à 1234 Quelquepart</div>'
        )

    def test_adresse_edit(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        adr = AdresseClient.objects.create(persona=client.persona, hospitalisation=True)
        five_days_ago = date.today() - timedelta(days=5)
        adr_pres1 = AdressePresence.objects.create(
            adresse=adr, depuis=date.today() - timedelta(days=50), jusqua=date.today() - timedelta(days=40)
        )
        adr_pres2 = AdressePresence.objects.create(adresse=adr, depuis=five_days_ago)
        self.client.force_login(self.user)
        form_data = {
            'hospitalisation': 'on',
            'npalocalite': '',
            'presences-TOTAL_FORMS': '2',
            'presences-INITIAL_FORMS': '2',
            'presences-0-id': adr_pres1.pk,
            'presences-0-depuis': adr_pres1.depuis.strftime('%Y-%m-%d'),
            'presences-0-jusqua': adr_pres1.jusqua.strftime('%Y-%m-%d'),
            'presences-1-id': adr_pres2.pk,
            'presences-1-depuis': adr_pres2.depuis.strftime('%Y-%m-%d'),
            'presences-1-jusqua': date.today().strftime('%Y-%m-%d'),
        }
        edit_url = reverse('client-adresse-edit', args=[adr.pk])
        response = self.client.post(edit_url, data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        message = f"Retour d’hospitalisation le {date.today().strftime('%d.%m.%Y')}"
        self.assertJournalMessage(client, message)
        self.assertEqual(
            client.alertes.latest().alerte,
            f'Modifications depuis l’application «{current_app()}»: {message}'
        )
        # Ajout nouvelle hospitalisation
        form_data.update({
            'presences-TOTAL_FORMS': '3',
            'presences-2-depuis': date.today().strftime('%Y-%m-%d'),
        })
        response = self.client.post(edit_url, data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        message = f'Ajout d’une hospitalisation (dès le {date.today().strftime("%d.%m.%Y")})'
        self.assertJournalMessage(client, message)
        self.assertEqual(
            client.alertes.latest().alerte,
            f'Modifications depuis l’application «{current_app()}»: {message}'
        )
        # Modification remarque
        form_data.update({
            'presences-INITIAL_FORMS': '3',
            'presences-2-id': AdressePresence.objects.get(jusqua__isnull=True).pk,
            'presences-2-remarque': "Encore 2 semaines",
        })
        response = self.client.post(edit_url, data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        message = "ajout de remarque («Encore 2 semaines»)"
        self.assertJournalMessage(client, message)

    def test_adresse_delete(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=Services.ALARME,
            npa='1234', localite='Quelquepart',
        )
        adr = AdresseClient.objects.create(
            persona=client.persona, npa='4321', localite='Ailleurs', principale=None
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('persona-adresse-delete', args=[adr.pk]), data={})
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        client.refresh_from_db()
        self.assertEqual(len(client.adresses.autres()), 0)
        self.assertJournalMessage(client, "Suppression de l’adresse «4321 Ailleurs»")

    def test_referent_new(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        Referent.objects.create(client=client, nom='Schmid', referent=1)
        Referent.objects.create(client=client, nom='Martin', repondant=0)
        self.client.force_login(self.user)
        prefix = 'contact-'
        post_data = {
            f'{prefix}nom': 'Tintin',
            f'{prefix}prenom': 'Roger',
            f'{prefix}npalocalite': '2345 Petaouchnok',
            f'{prefix}relation': 'Fils',
            f'{prefix}referent': '',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 3,
            'referenttel_set-0-tel': '032 111 11 11',
            'referenttel_set-1-tel': '074 444 11 11',
        }
        if current_app() == 'alarme':
            post_data.update({
                f'{prefix}est_referent': 'on',
                f'{prefix}repondant': '',
                f'{prefix}est_repondant': 'on',
                f'{prefix}courrier_envoye': 'on',
            })
        response = self.client.post(reverse('client-referent-add', args=[client.pk]), data=post_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-contacts'})
        self.assertEqual(client.referents.count(), 3)
        referent = client.referents.get(nom='Tintin')
        if current_app() == 'alarme':
            self.assertEqual(referent.repondant, 1)
            self.assertEqual(referent.referent, 2)
            self.assertEqual(referent.courrier_envoye, date.today())
            self.assertEqual(len(client.repondants()), 2)
        self.assertEqual(referent.telephones().count(), 2)
        self.assertEqual(sorted(referent.referenttel_set.values_list('priorite', flat=True)), [1, 2])

    def test_referent_new_copy_from(self):
        """Ajout d'un référent en copiant des infos d'un autre référent existant."""
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(
            nom='Schmid', id_externe='12345', facturation_pour=['al-tout']
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('client-referent-add', args=[client.pk]) + f'?copy_from={referent.pk}'
        )
        instance = response.context['form'].instance
        self.assertIsNone(instance.client)
        self.assertEqual(instance.nom, 'Schmid')
        self.assertEqual(instance.id_externe, 12345)
        self.assertEqual(instance.facturation_pour, [])

    def test_referent_autres_fonctions(self):
        referent = Referent(client=None, nom='Schmid', admin=True, facturation_pour=['al-tout'])
        self.assertEqual(
            referent.autres_fonctions(),
            "Contact administratif et technique, Adresse de facturation (Alarme (tout))"
        )

    def test_referent_edit(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=None)
        self.client.force_login(self.user)
        prefix = 'contact-'
        form_data = {
            f'{prefix}nom': 'Schmid',
            f'{prefix}prenom': 'Roger',
            f'{prefix}npalocalite': '2345 Petaouchnok',
            f'{prefix}relation': '',
            f'{prefix}referent': '',
            f'{prefix}repondant': '',
            f'{prefix}est_repondant': 'on',
            f'{prefix}courrier_envoye': '',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 2,
            'referenttel_set-0-tel': '032 111 11 11',
            'referenttel_set-1-tel': '078 777 77 77',
        }
        response = self.client.post(reverse('client-referent-edit', args=[client.pk, referent.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        referent.refresh_from_db()
        self.assertEqual(referent.referenttel_set.count(), 2)
        if current_app() == 'alarme':
            self.assertIsNone(referent.referent)
            self.assertEqual(referent.repondant, 1)
            msg = (
                "Modification du contact «Roger Schmid, répondant (1) pour Dupond Ladislas» (ajout de prénom («Roger»), "
                "répondant (de «faux» à «vrai»), ajout de npa («2345»), ajout de localité («Petaouchnok»), "
                "téléphone de référent: ajout de numéro («032 111 11 11»), "
                "téléphone de référent: ajout de numéro («078 777 77 77»))"
            )
        else:
            msg = (
                "Modification du contact «Roger Schmid,  pour Dupond Ladislas» (ajout de prénom («Roger»), "
                "ajout de npa («2345»), ajout de localité («Petaouchnok»), "
                "téléphone de référent: ajout de numéro («032 111 11 11»), "
                "téléphone de référent: ajout de numéro («078 777 77 77»))"
            )
            referent.repondant = 1
            referent.save()
        self.maxDiff = None
        self.assertJournalMessage(client, msg)
        form_data.update({
            f'{prefix}npa': '2345',
            f'{prefix}localite': 'Petaouchnok',
            f'{prefix}repondant': '1',
            'referenttel_set-INITIAL_FORMS': 2,
            'referenttel_set-0-id': referent.referenttel_set.all()[0].pk,
            'referenttel_set-0-tel': '032 111 11 12',
            'referenttel_set-0-ORDER': '1',
            'referenttel_set-1-id': referent.referenttel_set.all()[1].pk,
            'referenttel_set-1-DELETE': 'on',
        })
        response = self.client.post(reverse('client-referent-edit', args=[client.pk, referent.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.maxDiff = None
        self.assertJournalMessage(
            client,
            "Modification du contact «Roger Schmid, répondant (1) pour Dupond Ladislas» "
            "(téléphone de référent: numéro (de «032 111 11 11» à «032 111 11 12»), "
            "suppression de téléphone de référent (N° 078 777 77 77))"
        )

    def test_referent_facturation_pour(self):
        """Une adresse complète est obligatoire pour les adresses de facturation."""
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=None)
        form_data = {
            'nom': 'Schmid',
            'prenom': 'Roger',
            'rue': '',
            'npalocalite': '',
            'relation': '',
            'referent': '',
            'repondant': '',
            'est_repondant': 'on',
            'courrier_envoye': '',
            'facturation_pour': ['transp'],
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(client=client, instance=referent, data=form_data, initial={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            '__all__': ['Une rue ou une case postale est obligatoire pour les contacts de facturation'],
            'npalocalite': ['La localité est obligatoire pour les contacts de facturation'],
        })

    def test_contact_facturation_unique_par_type(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', facturation_pour=['al-tout'])
        form_data = {
            'client': str(client.pk), 'nom': 'Hirz',
            'rue': 'Rue des Fleurs 4', 'npalocalite': '2000 Neuchâtel',
            'facturation_pour': ['al-tout'],
            'referenttel_set-INITIAL_FORMS': 0, 'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(client=client, data=form_data, initial={})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'facturation_pour': ['Il existe déjà un contact de facturation pour ce client et cet item']}
        )
        form = ReferentForm(client=client, instance=referent, data=form_data, initial={})
        self.assertTrue(form.is_valid())
        # OK si un contact de facturation est archivé
        referent.date_archive = date.today()
        referent.save()
        form = ReferentForm(client=client, data=form_data, initial={})
        self.assertTrue(form.is_valid())

    def test_referent_other_country(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        form_data = {
            'nom': 'Schmid',
            'prenom': 'Roger',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(
            client=client,
            data={**form_data, 'npalocalite': '80027 Amiens', 'pays': 'FR'},
            initial={'langues': ['fr']}
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        form.save()
        self.assertEqual(form.instance.get_pays_display(), "France")
        self.assertEqual(form.instance.localite, "Amiens")
        self.assertEqual(form.instance.npa, "80027")
        self.assertEqual(form.instance.rue_localite(), "FR-80027 Amiens")

        form = ReferentForm(
            client=client,
            data={**form_data, 'npalocalite': 'Nothingham BFG 43R', 'pays': 'GB'},
            initial={'langues': ['fr']}
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        form.save()
        self.assertEqual(form.instance.get_pays_display(), "Grande-Bretagne")
        self.assertEqual(form.instance.localite, "Nothingham BFG 43R")
        self.assertEqual(form.instance.npa, "")
        self.assertEqual(form.instance.rue_localite(), "GB- Nothingham BFG 43R")

    def test_nouvelle_adresse_client_saved(self):
        """Le jour d'un changement d'adresse, le signal client_saved est envoyé."""
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", genre="M", id_externe=1234,
            rue="Rue de la Paix 36", npa='1234', localite='Quelquepart',
            valide_des=date.today() - timedelta(days=120), service=[Services.ALARME],
        )
        adr_actuelle = client.adresse()
        adr_actuelle.principale = date.today() - timedelta(days=120), date.today() - timedelta(days=1)
        adr_actuelle.save()
        adr_future = AdresseClient.objects.create(
            persona=client.persona, rue="Grand-Rue 2", npa='1235', localite='Ailleurs',
            principale=(date.today(), None)
        )
        out = StringIO()
        with (
            signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True),
            patch('httpx.put', side_effect=mocked_httpx) as mock
        ):
            call_command('archive_clients_benevs', verbosity=2, stdout=out, no_color=True)
            mock.assert_called_once()
        self.assertEqual(
            out.getvalue(),
            "\n".join(["Envoi notif. adresse modifiée pour 1 client(s)", "0 client(s) archivé(s)", ""])
        )

    def test_client_archiver_fort(self):
        """Archivage par script cron n jours. après fin dernière activité"""
        today = date.today()
        Client.objects.create(nom='Sans', prenom='Prestation')
        Client.objects.create(
            nom='Donzé', prenom='Ancien',
            service=[Services.ALARME, Services.TRANSPORT],
            archive_le=date(2022, 5, 20)
        )
        client = Client.objects.create(
            nom='Donzé', prenom='Partiel', service=[Services.ALARME, Services.TRANSPORT],
        )
        Prestation.objects.create(
            client=client, service=Services.ALARME, duree=(today - timedelta(days=60), None)
        )
        duree_passee = today - timedelta(days=360), today - timedelta(days=settings.ARCHIVER_CLIENTS_APRES + 2)
        Prestation.objects.create(
            client=client, service=Services.TRANSPORT, duree=duree_passee
        )
        client = Client.objects.create(
            nom='Donzé', prenom='AArchiver', service=[Services.ALARME, Services.TRANSPORT],
        )
        ref = Referent.objects.create(client=client, nom='Schmid', prenom='Claire', referent=1)
        client.prestations.all().update(duree=duree_passee)

        out = StringIO()
        call_command('archive_clients_benevs', verbosity=2, stdout=out, no_color=True)
        self.assertEqual(out.getvalue(), "1 client(s) archivé(s)\n")
        client.refresh_from_db()
        self.assertEqual(client.archive_le, today)
        ref.refresh_from_db()
        self.assertEqual(ref.date_archive, today)
        # Formulaire d'édition en lecture seule, avec bouton "Réactiver"
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertTrue(response.context["form"].readonly)
        self.assertContains(
            response,
            '<input type="text" name="nom" value="Donzé" maxlength="60" class="form-control" '
            'required disabled id="id_nom">',
            html=True
        )
        self.assertContains(response, '<button type="submit" class="btn btn-danger">Réactiver</button>', html=True)

    @skipIf(client_url_name() == "", "Pas de possibilité d'édition client dans l'app actuelle")
    def test_client_desarchiver(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT],
            archive_le=date(2022, 5, 20)
        )
        referent = Referent.objects.create(
            client=client, nom='Schmid', repondant=1,
            courrier_envoye='2022-01-04', date_archive='2022-05-20'
        )
        # client-archive is a toggle, will be reactivated if already archived
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, client.get_absolute_url())
        client.refresh_from_db()
        referent.refresh_from_db()
        self.assertIsNone(client.archive_le)
        if current_app() == "alarme":
            self.assertIsNone(referent.courrier_envoye)
        else:
            self.assertEqual(referent.courrier_envoye, date(2022, 1, 4))
        self.assertIsNone(referent.date_archive)
        self.assertEqual(client.prestations_actuelles(as_services=True), [current_app()])
        # Si on ré-archive le même jour, le service du jour n'est pas conservé
        # (sinon aboutit à une empty Range)
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        client.refresh_from_db()
        self.assertEqual(client.prestations.count(), 2)

    def test_referent_archive(self):
        today = date.today()
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        if client.get_absolute_url() is None:
            self.skipTest("Client not editable")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-referent-delete', args=[client.pk, referent.pk]), data={})
        self.assertContains(response, "Souhaitez vous vraiment archiver Schmid, répondant (1) pour Dupond Ladislas")
        response = self.client.post(reverse('client-referent-delete', args=[client.pk, referent.pk]), data={
            'date_archive': today.strftime('%Y-%m-%d'),
        })
        self.assertRedirects(response, client.get_absolute_url())
        referent.refresh_from_db()
        self.assertEqual(referent.date_archive, today)
        self.assertEqual(len(client.referents), 0)
        # Si la date archive est dans le futur, le référent est encore actif
        referent = Referent.objects.create(
            client=client, nom='Schmid', repondant=1, date_archive=today + timedelta(days=3)
        )
        self.assertEqual(len(client.referents), 1)


class ProfessionnelTests(DataMixin, TestCase):
    def test_professionnel_list(self):
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnels'))
        self.assertQuerySetEqual(response.context['object_list'], [pro])

    def test_professionnel_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnel-new'))
        response = self.client.post(reverse('professionnel-new'), data={
            'type_pro': 'Médecin',
            'nom': 'Dr Maboule',
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'tel_1': '076 111 11 22',
        })
        self.assertEqual(response.json()['result'], 'OK')
        pro = Professionnel.objects.get(nom__contains='Maboule')
        self.assertEqual(pro.localite, 'Petaouchnok')

    def test_professionnel_edit(self):
        pro = Professionnel.objects.create(
            type_pro="Médecin", nom="Dr Jekill", npa="2345", localite="Petaouchnok", tel_1="022 222 00 00"
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("professionnel-edit", args=[pro.pk]))
        self.assertContains(response, "Dr Jekill")
        response = self.client.post(reverse("professionnel-edit", args=[pro.pk]), data={
            "type_pro": "Médecin",
            "nom": "Dr Jekill",
            "prenom": "",
            "rue": "Rue des Fleurs",
            "npalocalite": "2345 Ailleurs",
            "tel_1": "077 777 77 77",
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})

    def test_professionnel_archive(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT]
        )
        client_arch = Client.objects.create(
            nom="Dupond", prenom="Ladislas", service=[Services.ALARME, Services.TRANSPORT],
            archive_le=date(2022, 1, 1),
        )
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        ProfClient.objects.create(professionnel=pro, client=client, priorite=1)
        ProfClient.objects.create(professionnel=pro, client=client_arch, priorite=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnels'))
        self.assertContains(
            response,
            'data-deleteconfirm="Voulez-vous vraiment archiver cette personne ? ATTENTION: elle '
            'sera retirée de 1 clients avec lesquels elle est actuellement liée."'
        )
        response = self.client.post(reverse('professionnel-archive', args=[pro.pk]), data={})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            client,
            "Suppression du professionnel «Dr Jekill (Petaouchnok, Médecin)» qui a été archivé.",
            user=self.user
        )
        # Le client archivé est resté en lien
        self.assertEqual(pro.profclient_set.count(), 1)


class AdminTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_superuser(
            'me@example.org', 'mepassword', first_name='Super', last_name='Admin',
        )

    def test_client_persona_admin(self):
        cl = Client.objects.create(
            nom='Dupond', prenom='Jules',
            c_o="Maison Bleue", rue='Av. Charles 12', npa='2000', localite='Neuchâtel'
        )
        ref = Referent.objects.create(client=cl, nom='Schmid', referent=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse("admin:client_client_changelist"))
        self.assertContains(response, "Av. Charles 12")
        response = self.client.get(reverse("admin:client_persona_changelist"))
        self.assertContains(response, "Av. Charles 12")
        response = self.client.get(reverse("admin:client_referent_changelist") + "?q=sch")
        self.assertEqual(len(response.context["results"]), 1)

    def test_client_export(self):
        Client.objects.create(
            nom="Dupond", prenom="Jules",
            c_o="Maison Bleue", rue="Av. Charles 12", npa="2000", localite="Neuchâtel"
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse("admin:client_client_changelist"), data={
            "action": "export",
            "select_across": "1",
            "_selected_action": list(Client.objects.all().values_list("pk", flat=True)),
        })
        self.assertEqual(response['Content-Type'], openxml_contenttype)

    def test_persona_export(self):
        Client.objects.create(
            nom="Dupond", prenom="Jules",
            c_o="Maison Bleue", rue="Av. Charles 12", npa="2000", localite="Neuchâtel"
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse("admin:client_persona_changelist"), data={
            "action": "export",
            "select_across": "1",
            "_selected_action": list(Persona.objects.all().values_list("pk", flat=True)),
        })
        self.assertEqual(response['Content-Type'], openxml_contenttype)
