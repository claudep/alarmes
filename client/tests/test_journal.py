from datetime import timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time

from client.factories import ClientFactory, JournalFactory
from client.models import Journal
from common.factories import SuperUserFactory, UtilisateurFactory


class JournalTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UtilisateurFactory()
        cls.user.user_permissions.add(
            Permission.objects.get(codename='view_client'),
        )

    def test_creation_journal(self):
        self.client.force_login(self.user)
        client = ClientFactory()
        response = self.client.post(
            reverse("client-journal-add", kwargs={"pk": client.pk}),
            data={"description": "Test manuel"},
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.journaux.count(), 1)

    def test_liste_journal(self):
        client = ClientFactory()
        JournalFactory(persona=client.persona, qui=self.user)
        JournalFactory(persona=client.persona, qui=self.user, description="Supercomment")
        JournalFactory(persona=client.persona)
        self.client.force_login(self.user)
        journal_url = reverse('client-journal', args=[client.pk])
        response = self.client.get(journal_url)
        self.assertEqual(len(response.context['object_list']), 3)
        response = self.client.get(journal_url + f'?qui={self.user.pk}')
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(journal_url + f'?qui={self.user.pk}&description=super')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_update_journal_record_manually_dont_change_quand(self):
        final_description = "Hello World!"
        create_time = timezone.now()
        edit_time = create_time + timedelta(days=1)
        with freeze_time(create_time):
            client = ClientFactory()
            journal = JournalFactory(persona=client.persona, auto=False)
        with freeze_time(edit_time):
            self.client.force_login(SuperUserFactory())
            self.client.post(
                reverse(
                    "client-journal-edit",
                    kwargs={"pk": client.pk, "pk_jr": journal.pk},
                ),
                data={"description": final_description},
            )
        journal.refresh_from_db()
        self.assertEqual(journal.description, final_description)
        self.assertEqual(journal.quand, create_time)

    def test_delete_journal_record(self):
        journal = JournalFactory()
        self.client.force_login(self.user)
        delete_url = reverse("client-journal-delete", args=[journal.persona_id, journal.pk])
        response = self.client.post(delete_url, data={})
        self.assertEqual(response.status_code, 403)
        self.client.force_login(journal.qui)
        response = self.client.post(delete_url, data={})
        self.assertEqual(response.json(), {'reload': 'page', 'result': 'OK'})
        self.assertFalse(Journal.objects.filter(pk=journal.pk).exists())
