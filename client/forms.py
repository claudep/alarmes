from datetime import date, timedelta
from functools import partial

from django import forms
from django.db import transaction
from django.db.models import Count, Max, Prefetch, Q, Value
from django.db.models.signals import pre_save
from django.forms.models import inlineformset_factory
from django.urls import reverse_lazy
from django.utils.timezone import now

from common.choices import Services
from common.forms import (
    BootstrapMixin, BSRadioSelect, DateInput, DateRangeField, FormsetMixin,
    GeoAddressFormMixin, LanguageMultipleChoiceField, ModelForm, NPALocaliteMixin,
    ReadOnlyableMixin,
)
from common.models import Utilisateur
from common.utils import current_app
from .models import (
    AdresseClient, AdressePresence, Alerte, Client, Journal, Persona, Professionnel,
    ProfClient, Referent, ReferentTel, Telephone, client_saved, referent_saved,
)


class DateArchiveForm(forms.Form):
    date_archive = forms.DateField(label="Date de l’archivage", widget=DateInput, required=True)


class ClientFilter:
    """Classe de filtre pour liste de clients."""
    label = ""
    export_only = False

    def filter(self, clients):
        """Custom client filtering"""
        raise NotImplementedError

    def extra_headers(self):
        """Return a list of extra headers for that filter."""
        return []

    def extra_values(self, client):
        """Return a list of extra values for this client and this filter."""
        return []


class ClientAutresDebiteursFilter(ClientFilter):
    label = "Clients avec autre débiteur"

    def __init__(self, fact_items):
        super().__init__()
        self.fact_items = fact_items

    def filter(self, clients):
        return clients.annotate(
            num_fact_autre=Count('referent', filter=Q(
                referent__date_archive=None, referent__facturation_pour__overlap=self.fact_items
            ))
        ).filter(num_fact_autre__gt=0).prefetch_related(
            Prefetch('referent_set', to_attr="debiteurs", queryset=Referent.objects.filter(
                date_archive=None, facturation_pour__overlap=self.fact_items
            ))
        )

    def extra_headers(self):
        return ['Débiteur (1)', 'Débiteur(2)']

    def extra_values(self, client):
        def format_deb(deb):
            if deb is None:
                return ''
            return f"{deb.nom_prenom}, {deb.rue_localite()} ({deb.cree_le.strftime('%d.%m.%Y')})"

        if len(client.debiteurs) == 0:
            return ['', '']
        if len(client.debiteurs) == 1:
            return [format_deb(client.debiteurs[0]), '']
        return [format_deb(client.debiteurs[0]), format_deb(client.debiteurs[1])]


class ClientFilterFormBase(BootstrapMixin, forms.Form):
    nom_prenom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom ou prénom', 'autocomplete': 'off', 'autofocus': True}
        ),
        required=False
    )
    rue = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off'}), required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )
    listes = forms.ChoiceField(
        choices=(),
        widget=forms.Select(attrs={'class': 'w-auto ms-auto'}), required=False
    )
    recherche_part = False  # Recherche aussi dans le nom du partenaire (alarme)
    recherche_deb = False  # Recherche aussi dans le nom du débiteur (contact fact.)
    fact_items = []
    filters = {}  # Dictionnaire d’instances ClientFilter

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.recherche_deb:
            self.fields['nom_prenom'].widget.attrs['placeholder'] = "Nom ou prénom (ou nom déb.)"
        if self.filters:
            self.fields['listes'].choices = [('', '-------')] + [
                (key, flt.label) for key, flt in self.filters.items()
            ]
        else:
            del self.fields['listes']
        self.active_filter = None

    @classmethod
    def add_filter(cls, key, filter_class_instance):
        """Ajout dynamique de filtre de clients, par ex. depuis app canton."""
        if key in cls.filters:
            raise KeyError(f"Key {key} is already in the form filters.")
        cls.filters[key] = filter_class_instance

    def filter(self, clients):
        if self.cleaned_data['nom_prenom']:
            nom_filters = (
                Q(persona__nom__unaccent__icontains=self.cleaned_data['nom_prenom']) |
                Q(persona__prenom__unaccent__icontains=self.cleaned_data['nom_prenom'])
            )
            if self.recherche_part:
                nom_filters |= Q(partenaire__persona__nom__unaccent__icontains=self.cleaned_data['nom_prenom'])
                nom_filters |= Q(partenaire__persona__prenom__unaccent__icontains=self.cleaned_data['nom_prenom'])
            if self.recherche_deb:
                nom_filters |= (
                    Q(referent__nom__unaccent__icontains=self.cleaned_data['nom_prenom']) &
                    ~Q(referent__facturation_pour=[])
                )
            clients = clients.filter(nom_filters).distinct()
        if self.cleaned_data['rue']:
            clients = clients.filter(
                persona__adresseclient__rue__unaccent__icontains=self.cleaned_data['rue']
            )
        if self.cleaned_data['npa_localite']:
            clients = clients.filter(
                Q(persona__adresseclient__npa__icontains=self.cleaned_data['npa_localite']) |
                Q(persona__adresseclient__localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            clients = clients.filter(
                Q(persona__telephone__tel__nospaces__icontains=self.cleaned_data['tel'].replace(' ', ''))
            ).distinct()
        if listes_value := self.cleaned_data.get('listes'):
            self.active_filter = self.filters[listes_value]
            clients = self.active_filter.filter(clients)
        return clients


class PersonaTelForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Telephone
        exclude = ['priorite']


def get_telephone_formset(extra=0):
    return inlineformset_factory(
        Persona, Telephone, form=PersonaTelForm, formset=HiddenDeleteInlineFormSet, extra=extra, can_order=True
    )


class PersonaForm(BootstrapMixin, ReadOnlyableMixin, FormsetMixin, ModelForm):
    langues = LanguageMultipleChoiceField(label="Langues", required=False)
    langues_select = forms.CharField(
        label="Langues",
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete',
            'autocomplete': 'off',
            'placeholder': 'Ajouter une langue',
            'data-searchurl': reverse_lazy('langue-autocomplete'),
        }),
        required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = Persona
        fields = []
        widgets = {
            'genre': BSRadioSelect,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        TelFormset = get_telephone_formset(extra=0 if self.instance.pk else 2)
        self.formset = TelFormset(
            data=kwargs.get("data"), instance=self.instance,
            queryset=Telephone.objects.order_by('priorite'),
            #?initial=initial.get('formsets', []),
        )

    def clean_nom(self):
        data = self.cleaned_data['nom']
        if self.instance.pk:
            # Ne pas accepter modifs noms tout en majuscules
            initial = self.instance.nom
            if initial and data and data.isupper() and initial.lower() == data.lower():
                return initial
        return data

    def save(self, **kwargs):
        if not self.instance.cree_le:
            self.instance.cree_le = now()
        for idx, frm in enumerate(self.formset.ordered_forms):
            frm.instance.priorite = idx + 1
        return super().save(**kwargs)


class AdresseFormMixin:
    adresse_fields = ["c_o", "rue", "npa", "localite"]
    always_add_address_fields = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.is_new = self.instance.pk is None
        if self.instance.pk:
            self.fields.pop("npalocalite", None)
        if self.is_new or self.always_add_address_fields:
            # Ajout dynamique des champs adresse en mode création
            self._add_address_fields()

    def _add_address_fields(self, field_classes=None):
        adr_form = forms.modelform_factory(
            AdresseClient, form=AdresseForm, field_classes=field_classes
        )(instance=None)
        adr_form.fields["npa"].required = True
        adr_form.fields["localite"].required = True
        for fname in self.adresse_fields:
            self.fields[fname] = adr_form.fields[fname]
        if self.instance.pk:
            adr = self.instance.adresse()
            self.initial.update({fname: getattr(adr, fname) for fname in self.adresse_fields})

    def save(self, **kwargs):
        obj = super().save(**kwargs)
        if self.is_new:
            pers = obj if isinstance(obj, Persona) else obj.persona
            adr = AdresseClient.objects.create(
                persona=pers, c_o=self.cleaned_data.get('c_o', ''), rue=self.cleaned_data['rue'],
                npa=self.cleaned_data['npa'], localite=self.cleaned_data['localite'],
                principale=(date.today(), None)
            )
            adr.geolocalize(save=True)
        return obj


class PersonaFormMixin:
    persona_fields = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.is_new = self.instance.pk is None
        self._add_persona_fields(**kwargs)

    def _add_persona_fields(self, **kwargs):
        if not self.persona_fields:
            raise RuntimeError("The form is missing the person_fields list.")
        form_class = forms.modelform_factory(
            Persona, form=PersonaForm, fields=self.persona_fields,
        )
        self.pers_form = form_class(
            instance=self.instance.persona if self.instance.persona_id else None,
            data=kwargs.get("data"),
            readonly=getattr(self, "readonly", False),
        )
        self.initial |= self.pers_form.initial
        for fname in self.persona_fields:
            self.fields[fname] = self.pers_form.fields[fname]
        self.formset = self.pers_form.formset

    def _get_changes(self, details=True):
        return self.pers_form._get_changes() + super()._get_changes(exclude=self.persona_fields)

    def clean(self):
        return super().clean() | self.pers_form.clean()

    def is_valid(self):
        return all([self.pers_form.is_valid(), super().is_valid()])

    def save(self, **kwargs):
        pers = self.pers_form.save()
        if self.is_new:
            self.instance.persona = pers
        return super().save(**kwargs)


class ClientEditFormBase(BootstrapMixin, ReadOnlyableMixin, AdresseFormMixin, PersonaFormMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Client
        fields = []  # overriden by child classes
        widgets = {
            'type_logement': BSRadioSelect,
        }
        labels = {
            'type_logement_info': "Autres infos sur le type de logement",
        }
    benev_hidden_fields = set()  # Champs non disponibles pour les bénévoles
    api_sync = True

    def __init__(self, user_is_benev=False, **kwargs):
        super().__init__(**kwargs)
        self.fields["prenom"].required = True
        if user_is_benev:
            for field_name in self.benev_hidden_fields:
                del self.fields[field_name]
        if self.instance.pk:
            # Champs d'adresse modifiés séparément
            [self.fields.pop(fname, None) for fname in PersonaAdresseForm._meta.fields]
        if self.instance.pk and (partenaire := self.instance.get_partenaire_de()):
            # Champs à éditer dans le formulaire du partenaire
            for field_name in [
                'type_logement', 'type_logement_info', 'etage', 'nb_pieces', 'ascenseur',
                'code_entree', 'boitier_cle', 'cles', 'situation_vie', 'animal_comp', 'visiteur',
            ]:
                self.fields.pop(field_name, None)
        if 'partenaire' in self.fields:
            if not self.instance.pk or self.instance.get_partenaire_de():
                del self.fields['partenaire']
            else:
                potentiels = []
                adr = self.instance.adresse()
                if adr:
                    potentiels = Client.objects.exclude(pk=self.instance.pk).avec_adresse(date.today()).filter(
                        adresse_active__rue=adr.rue, adresse_active__npa=adr.npa
                    )
                if not potentiels:
                    del self.fields['partenaire']
                else:
                    self.fields['partenaire'].queryset = potentiels

    def remarques_fields(self):
        app = current_app()
        for fname in [f'remarques_ext_{app}', f'remarques_int_{app}']:
            try:
                yield self[fname]
            except KeyError:
                continue

    def has_changed_data(self):
        return any([super().has_changed_data(), self.pers_form.has_changed_data()])

    def save(self, **kwargs):
        # Launch signal manually to ensure old client version is saved before the pers_form saving.
        pre_save.send(sender=Client, instance=self.instance)
        is_new = self.instance.pk is None
        client = super().save(**kwargs)
        if self.api_sync:
            transaction.on_commit(
                partial(client_saved.send, sender=Client, instance=client, created=is_new)
            )
        return client


class ClientSamasForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Client
        fields = ["samaritains_des"]
        field_classes = {
            "samaritains_des": DateRangeField,
        }


# Fusionner avec AdresseForm?
class PersonaAdresseForm(BootstrapMixin, NPALocaliteMixin, GeoAddressFormMixin, ModelForm):
    des_le = forms.DateField(label="Valable dès le", widget=DateInput(attrs={'class': 'd-inline'}), required=True)

    class Meta(BootstrapMixin.Meta):
        model = AdresseClient
        fields = ['des_le', 'c_o', 'rue', 'npa', 'localite', 'npalocalite']

    def __init__(self, **kwargs):
        kwargs['initial']['des_le'] = kwargs['instance'].principale.lower
        super().__init__(**kwargs)
        self.fields['rue'].required = True
        self.fields['npalocalite'].required = True
        self.client = self.instance.persona.get_client()

    def clean(self):
        client = self.instance.persona.get_client()
        self.geo_required = client and Services.TRANSPORT in client.prestations_actuelles(as_services=True)
        prev_empl_geo = self.instance.empl_geo.copy() if self.instance.empl_geo else None
        cleaned_data = super().clean()
        if prev_empl_geo != self.instance.empl_geo:
            # On peut alors considérer que l’adresse a changé
            try:
                old_adr = AdresseClient.objects.get(pk=self.instance.pk)
            except AdresseClient.DoesNotExist:
                pass
            else:
                if self.cleaned_data["des_le"] <= old_adr.principale.lower:
                    self.add_error(
                        "des_le",
                        "Cette date ne peut pas être plus ancienne que la date de début de l’adresse précédente"
                    )
        return cleaned_data

    def save(self, **kwargs):
        old_adr = None
        if self.changed_data == ['des_le']:
            message = (
                f"Validité d’adresse modifiée du {self.instance.principale.lower.strftime('%d.%m.%Y')} "
                f"au {self.cleaned_data['des_le'].strftime('%d.%m.%Y')}"
            )
            self.instance.principale = (self.cleaned_data['des_le'], None)
        else:
            if 'des_le' in self.changed_data:
                # Nouvelle adresse (=déménagement)
                old_adr = AdresseClient.objects.get(pk=self.instance.pk)
                old_adr.principale = (old_adr.principale.lower, self.cleaned_data['des_le'] - timedelta(days=1))
                self.instance.pk = None
                self.instance.principale = (self.cleaned_data['des_le'], None)
                message = (
                    f"Ancienne adresse: {old_adr.rue}, {old_adr.npa} {old_adr.localite}<br>"
                    f"Nouvelle adresse dès le {self.cleaned_data['des_le'].strftime('%d.%m.%Y')}: "
                    f"{self.instance.rue}, {self.instance.npa} {self.instance.localite}"
                )
            else:
                # Mise à jour mineure d'adresse (=correction)
                message = super().get_changed_string()
            if self.instance.empl_geo is None:
                message += "<br>Attention: la nouvelle adresse n’a pas pu être géolocalisée"
        client = self.instance.persona.get_client()
        if client:
            pre_save.send(sender=Client, instance=self.instance.persona.client)
        if old_adr:
            old_adr.save()
        adresse = super().save(**kwargs)
        if client:
            transaction.on_commit(
                partial(client_saved.send, sender=Client, instance=adresse.persona.client, created=False)
            )
        return adresse, message


class AdressePresenceForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = AdressePresence
        fields = '__all__'

    def get_changed_string(self, **kwargs):
        is_new = self.cleaned_data['id'] is None
        if is_new and 'depuis' in self.changed_data:
            depuis = self.cleaned_data['depuis']
            message = f"Ajout d’une hospitalisation (dès le {depuis.strftime('%d.%m.%Y')})"
        elif 'jusqua' in self.changed_data:
            retour = self.cleaned_data['jusqua']
            message = f"Retour d’hospitalisation le {retour.strftime('%d.%m.%Y')}"
        else:
            return super().get_changed_string(**kwargs)
        return message


class AdressePresenceSuiviForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = AdressePresence
        fields = ['jusqua', 'dernier_contact', 'remarque']


class AdresseForm(BootstrapMixin, NPALocaliteMixin, GeoAddressFormMixin, ModelForm):
    geo_required = False

    class Meta(BootstrapMixin.Meta):
        model = AdresseClient
        fields = ['hospitalisation', 'depose', 'c_o', 'rue', 'npa', 'localite', 'npalocalite', 'remarque']

    def __init__(self, *args, data=None, **kwargs):
        PresenceFormSet = forms.inlineformset_factory(
            AdresseClient, AdressePresence, form=AdressePresenceForm, extra=1
        )
        self.formset = PresenceFormSet(data=data, instance=kwargs['instance'])
        super().__init__(*args, data=data, **kwargs)

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def clean(self):
        data = super().clean()
        if (
            not data.get('hospitalisation') and not data.get('npa') and
            not data.get('localite') and not data.get('remarque')
        ):
            raise forms.ValidationError(
                "Vous devez indiquer un lieu, une hospitalisation ou une remarque"
            )
        if self.instance.pk is None and data.get('hospitalisation') and not self.formset.has_changed():
            raise forms.ValidationError("Vous devez indiquer un début d’hospitalisation")
        return data

    def save(self, **kwargs):
        adresse = super().save(**kwargs)
        if not self.formset.instance.pk:
            self.formset.instance = adresse
        self.formset.save(**kwargs)
        return adresse


class JournalForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Journal
        fields = ['description']


class JournalFilterForm(BootstrapMixin, forms.Form):
    qui = forms.ModelChoiceField(queryset=Utilisateur.objects.none(), required=False)
    description = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}),
        required=False
    )

    def __init__(self, *args, client=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['qui'].queryset = Utilisateur.objects.filter(
            pk__in=client.journaux.values('qui').distinct()
        ).order_by('last_name', 'first_name')

    def filter(self, journaux):
        if self.cleaned_data['qui']:
            journaux = journaux.filter(qui=self.cleaned_data['qui'])
        if self.cleaned_data['description']:
            journaux = journaux.filter(description__unaccent__icontains=self.cleaned_data['description'])
        return journaux


class HiddenDeleteInlineFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput
    ordering_widget = forms.HiddenInput


class ReferentTelForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = ReferentTel
        exclude = ['priorite']


def get_referent_formset(extra=0):
    return inlineformset_factory(
        Referent, ReferentTel, form=ReferentTelForm, formset=HiddenDeleteInlineFormSet, extra=extra, can_order=True
    )


class ReferentForm(BootstrapMixin, NPALocaliteMixin, ReadOnlyableMixin, FormsetMixin, ModelForm):
    contact_existant = forms.CharField(
        label="Copier contact de facturation existant",
        widget=forms.TextInput(attrs={
            'class': 'autocomplete',
            'autocomplete': 'off',
            'placeholder': "Recherche de contact…",
            'data-searchurl': reverse_lazy('referent-fact-search'),
        }),
        required=False
    )
    est_repondant = forms.BooleanField(label='Répondant', required=False)
    est_referent = forms.BooleanField(
        label='Personne de référence à prévenir en cas d’urgence', required=False
    )
    courrier_envoye = forms.BooleanField(label="Le courrier initial a été envoyé", required=False)

    app_specific_fields = {
        'alarme': ['courrier_envoye', 'contractant', 'est_repondant', 'est_referent'],
        'transport': ['no_sinistre', 'contact_transport'],
        'visite': ['contact_visites'],
    }

    class Meta(BootstrapMixin.Meta):
        model = Referent
        fields = [
            'nom', 'prenom', 'salutation', 'complement', 'case_postale', 'rue',
            'npa', 'localite', 'npalocalite', 'pays', 'courriel', 'relation',
            'repondant', 'referent', 'admin', 'facturation_pour',
            'contractant', 'id_externe', 'no_sinistre', 'pers_contact', 'remarque',
            'courrier_envoye', 'contact_transport', 'contact_visites',
        ]
        labels = {
            'referent': 'Personne de référence à prévenir en cas d’urgence, priorité',
            'id_externe': "N° débiteur",
        }
        widgets = {
            'repondant': forms.HiddenInput,
            'referent': forms.HiddenInput,
        }

    def __init__(self, *args, client=None, data=None, user_is_benev=False, initial=None, **kwargs):
        initial['est_repondant'] = kwargs.get('instance') and kwargs['instance'].repondant is not None
        initial['est_referent'] = kwargs.get('instance') and kwargs['instance'].referent is not None
        initial['courrier_envoye'] = bool(kwargs.get('instance') and kwargs['instance'].courrier_envoye)
        super().__init__(*args, data=data, initial=initial, **kwargs)
        for app, flist in self.app_specific_fields.items():
            # Selon l'application active, désactiver certains champs (avec valeurs)
            # ou les supprimer du formulaire
            if app != current_app():
                for field_name in flist:
                    if (
                        self.instance.pk is None or
                        not getattr(self.instance, field_name, initial.get(field_name, False))
                    ):
                        del self.fields[field_name]
                    else:
                        self.fields[field_name].disabled = True
        if user_is_benev:
            self.fields.pop('courrier_envoye', None)
            self.fields.pop('id_externe', None)
        if self.instance.pk:
            del self.fields['contact_existant']
        TelFormset = get_referent_formset(extra=0 if self.instance.pk else 2)
        self.formset = TelFormset(
            data=data, instance=kwargs.get('instance'),
            queryset=ReferentTel.objects.order_by('priorite'),
            initial=initial.get('formsets', []),
        )
        self.client = client

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['salutation'] == 'E' and cleaned_data['prenom']:
            self.add_error('prenom', "Les contacts Entreprise ne peuvent pas posséder de prénom")
        if cleaned_data.get('facturation_pour'):
            if not cleaned_data.get('rue') and not cleaned_data.get('case_postale'):
                self.add_error(None, "Une rue ou une case postale est obligatoire pour les contacts de facturation")
            if not cleaned_data.get('npalocalite'):
                self.add_error('npalocalite', "La localité est obligatoire pour les contacts de facturation")
        if 'est_repondant' in self.changed_data:
            if cleaned_data['est_repondant']:
                cleaned_data['repondant'] = (
                    self.client.referents.aggregate(Max('repondant'))['repondant__max'] or 0
                ) + 1
            else:
                cleaned_data['repondant'] = None
        if 'est_referent' in self.changed_data:
            if cleaned_data['est_referent']:
                cleaned_data['referent'] = (
                    self.client.referents.aggregate(Max('referent'))['referent__max'] or 0
                ) + 1
            else:
                cleaned_data['referent'] = None
        if 'courrier_envoye' in self.changed_data:
            cleaned_data['courrier_envoye'] = date.today() if cleaned_data['courrier_envoye'] else None
        else:
            cleaned_data['courrier_envoye'] = self.instance.courrier_envoye
        return cleaned_data

    def clean_facturation_pour(self):
        fact = self.cleaned_data['facturation_pour']
        if fact:
            for item in fact:
                if self.client.referents.exclude(pk=self.instance.pk).filter(
                    facturation_pour__contains=[item]
                ).exists():
                    raise forms.ValidationError(
                        "Il existe déjà un contact de facturation pour ce client et cet item"
                    )
        return fact

    def clean_contractant(self):
        contr = self.cleaned_data['contractant']
        if contr and self.client.referents.exclude(pk=self.instance.pk).filter(contractant=True).exists():
            raise forms.ValidationError("Il existe déjà un contact contractant pour ce client")
        return contr

    def _changed_value(self, fname, detail=False):
        if fname == 'ORDER' and 'tel' in self.cleaned_data:
            return f"Priorité {self.cleaned_data['ORDER']} pour n° {self.cleaned_data['tel']}"
        return super()._changed_value(fname, detail=detail)

    def save(self, **kwargs):
        is_new = self.instance.pk is None
        self.instance.client = self.client
        for idx, frm in enumerate(self.formset.ordered_forms):
            frm.instance.priorite = idx + 1
        referent = super().save(**kwargs)
        transaction.on_commit(
            partial(referent_saved.send, sender=Referent, instance=referent, created=is_new)
        )
        return referent


class ProfessionnelSelectForm(BootstrapMixin, forms.Form):
    professionnel_pk = forms.IntegerField(widget=forms.HiddenInput)
    professionnel = forms.CharField(label="Choisir un professionnel de la santé", widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'placeholder': "Nom du professionnel…",
        'data-searchurl': reverse_lazy('professionel-search'),
        'data-pkfield': 'professionnel_pk',
    }))
    remarque_client = forms.CharField(label="Remarque (pour ce client)", widget=forms.Textarea, required=False)


class ProfessionnelForm(BootstrapMixin, NPALocaliteMixin, ModelForm):
    field_order = ['type_pro', 'nom', 'prenom', 'rue', 'npalocalite']

    class Meta(BootstrapMixin.Meta):
        model = Professionnel
        exclude = ['no_centrale', 'empl_geo', 'date_archive']


class ProfessionnelClientForm(ProfessionnelForm):
    remarque_client = forms.CharField(label="Remarque (pour ce client)", widget=forms.Textarea, required=False)


class ProfessionnelClientEditForm(BootstrapMixin, ModelForm):
    class Meta:
        model = ProfClient
        fields = ['remarque']
        labels = {
            'remarque': "Remarques (pour le client)",
        }


class ProfFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    type_pro = forms.ChoiceField(choices=(('', 'Tous'),) + Professionnel.PROF_CHOICES, required=False)

    def filter(self, profs):
        if self.cleaned_data['nom']:
            profs = profs.filter(nom__icontains=self.cleaned_data['nom'])
        if self.cleaned_data['type_pro']:
            profs = profs.filter(type_pro=self.cleaned_data['type_pro'])
        return profs


class AlerteForm(BootstrapMixin, ModelForm):
    traite_le = forms.BooleanField(label="Cette alerte a été traitée", required=False)

    class Meta(BootstrapMixin.Meta):
        model = Alerte
        fields = ['traite_le', 'remarque']

    def _post_clean(self):
        # Replace boolean by current date.
        if self.cleaned_data.get('traite_le') is True:
            self.cleaned_data['traite_le'] = now()
        else:
            self.cleaned_data['traite_le'] = None
        return super()._post_clean()


def notify_other_teams(form, client, from_app=None):
    other_types = set([p.service for p in client.prestations_actuelles()]) - {from_app}
    if not other_types:
        return

    if 'date_deces' in form.changed_data:
        if from_app:
            msg = f"L’équipe «{from_app}» a marqué cette personne comme décédée"
        else:
            msg = "Cette personne a été marquée comme décédée depuis l’ERP"
        for app in other_types:
            Alerte.objects.create(
                persona=client.persona, cible=app, alerte=msg, recu_le=now(),
            )
        return
    changes = form.get_changed_string(changed_values=True)
    if changes:
        if from_app:
            msg = f"Modifications depuis l’application «{from_app}»: {changes}"
        else:
            msg = f"Modifications depuis l’ERP: {changes}"
        for app in other_types:
            Alerte.objects.create(
                persona=client.persona, cible=app, alerte=msg, recu_le=now()
            )

