from datetime import date, timedelta
from pathlib import Path
from unittest.mock import patch

from django.contrib.auth.models import Permission
from django.core.files import File
from django.test import TestCase
from django.urls import reverse

from client.models import Client, Persona
from common.choices import Services
from common.export import openxml_contenttype
from common.models import Utilisateur
from common.test_utils import read_xlsx

from ..models import Campagne, Don, Donateur


class BaseTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                "view_donateur", "change_donateur", "change_don", "add_campagne",
            ]))
        )
        Campagne.objects.create(nom="OSAD 2023", lancement=date(2023, 12, 1))
        Campagne.objects.create(nom="Noël 2024", lancement=date(2024, 10, 1))

    def setUp(self):
        super().setUp()
        #Patching httpx.get for all tests."""
        patcher = patch('httpx.get')
        patcher.start()
        self.addCleanup(patcher.stop)

    def _create_donateur(self, **kwargs):
        pers_data = dict(
            nom="Guruk", prenom="André", rue="rue du Crêt 111", npa="2525", localite="Le Landeron",
        ) | kwargs
        return Donateur.objects.create(persona=Persona.objects.create(**pers_data), politesse="Monsieur")


class DonateurTests(BaseTestCase):
    def test_donateur_adresse_active(self):
        donateur = self._create_donateur()
        self.assertEqual(
            Donateur.objects.avec_adresse(date.today()).get(pk=donateur.pk).adresse_active["npa"],
            "2525"
        )

    def test_donateur_edition(self):
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateur-edit", args=[donateur.pk]))
        self.assertContains(
            response,
            '<input type="text" name="prenom" maxlength="50" class="form-control" id="id_prenom" value="André">',
            html=True
        )

    def test_ajout_fichier(self):
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateur-fichier-add", args=[donateur.pk]))
        self.assertContains(response, "<b>Guruk André</b>")

    def test_donateur_nouveau(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateur-new"))
        self.assertContains(
            response,
            '<input type="text" name="prenom" maxlength="50" class="form-control" id="id_prenom">',
            html=True
        )
        response = self.client.post(reverse("donateur-new"), data={
            "nom": "Valjean",
            "prenom": "Jean",
            "genre": "M",
            "rue": "Rue du Bonheur 3",
            "npalocalite": "2000 Neuchâtel",
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 0,
        })
        donateur = Donateur.objects.get(persona__nom="Valjean")
        self.assertRedirects(response, reverse("donateur-edit", args=[donateur.pk]))
        self.assertEqual(str(donateur.persona.adresse()), "Rue du Bonheur 3, 2000 Neuchâtel")

    def test_donateur_from_persona(self):
        persona = Persona.objects.create(nom="Muller", prenom="Juju")
        self.client.force_login(self.user)
        url = reverse("donateur-from-persona", args=[persona.pk])
        response = self.client.get(url)
        self.assertContains(response, "Muller")
        response = self.client.post(url, data={})
        donateur = persona.donateur
        self.assertRedirects(response, reverse("donateur-edit", args=[donateur.pk]))

    def test_donateur_dons(self):
        donateur = self._create_donateur()
        don = Don.objects.create(
            date_don=date(2024, 10, 10), montant=50, donateur=donateur
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateur-dons", args=[donateur.pk]))
        self.assertQuerySetEqual(response.context["object_list"], [don])

    def test_donateur_archiver(self):
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        response = self.client.post(reverse("donateur-archive", args=[donateur.pk]), data={
            "motif_archive": "decede",
            "btn_action": "archiver",
        })
        self.assertRedirects(response, reverse("donateurs"))
        donateur.refresh_from_db()
        self.assertEqual(donateur.archive_le, date.today())
        self.assertEqual(donateur.motif_archive, Donateur.MotifsArchivage.DECEDE)

    def test_donateur_supprimer(self):
        donateur = self._create_donateur()
        don = Don.objects.create(
            donateur=donateur, date_don=date(2024, 10, 10), montant=50,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse("donateur-archive", args=[donateur.pk]), data={
            "motif_archive": "efface",
            "btn_action": "supprimer",
        })
        self.assertRedirects(response, reverse("donateurs"))
        don.refresh_from_db()
        self.assertIsNone(don.donateur)
        self.assertFalse(Donateur.objects.filter(pk=donateur.pk).exists())

    def test_donateur_client_mad(self):
        donateur = self._create_donateur()
        Client.objects.create(persona=donateur.persona, service=Services.ALARME)
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateur-edit", args=[donateur.pk]))
        self.assertContains(response, "logo-alarme.svg")

    def test_liste_donateurs(self):
        self._create_donateur()
        donateur2 = self._create_donateur(nom="Guran", prenom="John", rue="", npa="2300", localite="La Chaux-de-Fonds")
        self.client.force_login(self.user)
        response = self.client.get(reverse("donateurs") + "?nom_prenom=Gur")
        edit_url = reverse('donateur-edit', args=[donateur2.pk])
        self.assertContains(response, f'<a href="{edit_url}">Guran</a> John')
        response = self.client.get(reverse("donateurs") + "?export=1")
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_attribuer_don_a_client(self):
        don = Don.objects.create(
            date_don=date(2024, 10, 10), montant=50, debiteur={
                "Nm": "Guruk Andre",
                "PstlAdr": {"Ctry": "CH", "PstCd": "2525", "TwnNm": "Le Landeron", "StrtNm": "rue du Cret 111"}
            },
        )
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        response = self.client.get(reverse("recherche-donateur", args=[don.pk]))
        self.assertContains(
            response,
            '<input type="text" name="rue" autocomplete="off" class="form-control" id="id_rue">',
            html=True
        )
        response = self.client.get(reverse("recherche-donateur", args=[don.pk]) + "?nom_prenom=gur")
        self.assertQuerySetEqual(response.context["object_list"], [donateur])
        url = reverse('don-attribuer', args=[don.pk, donateur.pk])
        response = self.client.post(url, data={})
        self.assertEqual(response.json(), {
            'result': 'OK',
            'donateur': "Guruk André<br>rue du Crêt 111, 2525 Le Landeron",
        })
        donateur.refresh_from_db()
        self.assertQuerySetEqual(donateur.dons.all(), [don])

    def test_creer_donateur_depuis_don(self):
        don = Don.objects.create(
            date_don=date(2024, 10, 10), montant=50, debiteur={
                "Nm": "Justine Ririri",
                "PstlAdr": {"Ctry": "CH", "PstCd": "2034", "TwnNm": "Peseux", "StrtNm": "Rue du Vide 4"}
            },
        )
        self.client.force_login(self.user)
        url = reverse("don-creerdonateur", args=[don.pk])
        response = self.client.get(url)
        self.assertContains(response, 'value="Ririri"')
        self.assertContains(response, 'value="2034 Peseux"')
        response = self.client.post(url, data={
            "nom": "Ririri",
            "prenom": "Justine",
            "rue": "Rue du Vide 4",
            "npalocalite": "2034 Peseux",
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 1,
        })
        if 'html' in response['Content-Type']:
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {
            'client': 'Ririri Justine<br>Rue du Vide 4, 2034 Peseux',
            'donpk': don.pk,
            'result': 'OK',
        })
        donateur = Donateur.objects.get(persona__nom="Ririri")
        self.assertEqual(donateur.persona.journaux.count(), 1)
        self.assertEqual(donateur.dons.count(), 1)

    def test_debiteur_as_lines(self):
        don = Don(
            date_don=date(2024, 10, 10), montant=50, debiteur={
                "Nm": "Justine Ririri",
                "PstlAdr": {"AdrLine": ['RUE DE LA VOIRIE 18 CH 2112 MOTIE', 'RS']},
            },
        )
        self.assertEqual(
            don.debiteur_as_lines(),
            ["Justine Ririri", "Rue De La Voirie 18 Ch 2112 Motiers"]
        )

    def test_don_edit(self):
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        response = self.client.post(reverse("don-new", args=[donateur.pk]), data={
            "date_don": date.today(),
            "campagne": "",
            "montant": "45",
        })
        self.assertEqual(response.json()["result"], "OK")
        self.assertEqual(donateur.dons.first().montant, 45)
        response = self.client.post(reverse("don-edit", args=[donateur.dons.first().pk]), data={
            "date_don": date.today(),
            "campagne": "",
            "montant": "55",
        })
        self.assertEqual(response.json()["result"], "OK")
        self.assertEqual(donateur.dons.first().montant, 55)

    def test_adresse_principale_edit(self):
        donateur = self._create_donateur()
        self.client.force_login(self.user)
        edit_url = reverse('persona-adresse-princ-edit', args=[donateur.adresse().pk])
        response = self.client.get(edit_url)
        self.assertContains(response, "2525 Le Landeron")

        # Ajout nouvelle adresse
        des_le = date.today() + timedelta(days=4)
        with patch('httpx.get'):
            response = self.client.post(edit_url, data={
                'des_le': des_le,
                'rue': "Rue du Soleil 12",
                'npalocalite': "4321 Ailleurs",
            })
        donateur.refresh_from_db()
        self.assertEqual(donateur.adresse(des_le).localite, "Ailleurs")
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#clientAdresseBloc'})
        self.assertEqual(
            donateur.journaux.first().description,
            "Ancienne adresse: rue du Crêt 111, 2525 Le Landeron<br>"
            f"Nouvelle adresse dès le {des_le.strftime('%d.%m.%Y')}: Rue du Soleil 12, 4321 Ailleurs"
            "<br>Attention: la nouvelle adresse n’a pas pu être géolocalisée"
        )
        # Ancienne et nouvelle adresses sont accessibles
        new, old = donateur.adresse(des_le), donateur.adresse()
        self.assertNotEqual(old, new)
        self.assertEqual(new.principale.lower - old.principale.upper, timedelta(days=1))

    def test_campagnes(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("campagnes"))
        self.assertContains(response, "Noël 2024")

    def test_ajout_edition_campagne(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse("campagne-new"), data={
            "nom": "Nouvelle",
            "lancement": "2024-10-01",
        })
        self.assertRedirects(response, reverse("campagnes"))
        campagne = Campagne.objects.get(nom="Nouvelle")
        response = self.client.post(reverse("campagne-edit", args=[campagne.pk]), data={
            "nom": "Nouvelle campagne",
            "lancement": "2024-10-10",
        })
        self.assertRedirects(response, reverse("campagnes"))
        campagne.refresh_from_db()
        self.assertEqual(campagne.nom, "Nouvelle campagne")


class CAMTImportTests(BaseTestCase):
    def test_import_camt(self):
        self.client.force_login(self.user)
        donateur = self._create_donateur()
        campagne = Campagne.objects.create(nom="soutien 2024/OSAD", lancement=date(2024, 7, 1))
        with (Path(__file__).parent / 'camt.053.xml').open('rb') as fh:
            response = self.client.post(
                reverse('importer-dons'),
                data={'fichier': File(fh)},
                follow=True
            )
        self.assertContains(response, "Rapport d’importation")
        self.assertContains(response, "Aeschlimann")
        donateur.refresh_from_db()
        self.assertEqual(donateur.dons.count(), 1)
        self.assertEqual(donateur.no_wesser, 333444)
        self.assertEqual(Don.objects.count(), 3)
        self.assertEqual(campagne.don_set.count(), 3)
        today = date.today()
        response = self.client.get(
            reverse("importer-rapport", args=[today.year, today.month, today.day])
        )
        self.assertContains(response, "X. Aeschlimann<br>Rue du Plan 99<br>2000 Neuchâtel")


class StatsTests(BaseTestCase):
    def test_stats(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("stats-dons"))
        self.assertContains(response, "Nombre de dons")
        response = self.client.get(reverse("stats-dons") + "?export=1")
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_export_dons_annuels(self):
        donateur = self._create_donateur()
        donateur_sans_adr = self._create_donateur(nom="SDF", rue="", npa="", localite="")
        annee = date.today().year
        Don.objects.bulk_create([
            Don(donateur=donateur, date_don=date(annee - 1, 3, 4), montant=120),
            Don(donateur=donateur, date_don=date(annee - 1, 12, 31), montant=55),
            Don(donateur=donateur, date_don=date(annee, 1, 4), montant=100),
            Don(donateur=donateur_sans_adr, date_don=date(annee - 1, 10, 3), montant=50),
        ])
        self.client.force_login(self.user)
        response = self.client.post(reverse("export-annuel"), data={})
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        content = read_xlsx(response.content)
        self.assertEqual(len(content), 3)  # en-tête + 2 donateurs
        self.assertEqual(
            content[1],
            ["Monsieur", "Guruk", "André", "rue du Crêt 111", "2525 Le Landeron", None, 175]
        )
