from django.db import migrations


def populate_archive_le(apps, schema_editor):
    # This migration was only applicapble before the persona.archive_le field was removed.
    return
    Donateur = apps.get_model('crm', 'Donateur')
    Persona = apps.get_model('client', 'Persona')
    for pers in Persona.objects.filter(archive_le__isnull=False):
        Donateur.objects.filter(persona__id=pers.pk).update(archive_le=pers.archive_le)


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0055_persona_client_persona'),
        ('crm', '0004_donateur_archive_le'),
    ]

    operations = [
        migrations.RunPython(populate_archive_le, migrations.RunPython.noop),
    ]
