from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0005_populate_archive_le'),
    ]

    operations = [
        migrations.AddField(
            model_name='donateur',
            name='ne_pas_solliciter',
            field=models.BooleanField(default=False, verbose_name='Ne pas solliciter'),
        ),
    ]
