from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0006_donateur_ne_pas_solliciter'),
    ]

    operations = [
        migrations.AddField(
            model_name='donateur',
            name='motif_archive',
            field=models.CharField(blank=True, choices=[('decede', 'Décédé'), ('aucune', 'Aucune indication'), ('prob_fin', 'Problèmes financiers'), ('doublon', 'Double adhésion'), ('unique', 'Don unique'), ('non_cotis', 'Pas de paiement de cotisation'), ('autresorg', 'Soutien d’autres organisations'), ('ems', 'La personne n’est pas autonome / EMS'), ('retraite', 'Retraité'), ('refus', 'Courrier refusé'), ('colere', 'Colère envers l’association'), ('efface', 'Effacement des données')], max_length=10, verbose_name='Raison de l’archivage'),
        ),
    ]
