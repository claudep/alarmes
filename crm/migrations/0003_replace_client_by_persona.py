import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0057_remove_client_persona_fields'),
        ('crm', '0002_don_suite_deces'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='don',
            name='client',
        ),
        migrations.RemoveField(
            model_name='don',
            name='client_data',
        ),
        migrations.RemoveField(
            model_name='donateur',
            name='client',
        ),
        migrations.AddField(
            model_name='don',
            name='donateur_data',
            field=models.JSONField(blank=True, null=True, verbose_name='Données du donateur'),
        ),
        migrations.AddField(
            model_name='don',
            name='donateur',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dons', to='crm.donateur'),
        ),
        migrations.AddField(
            model_name='donateur',
            name='persona',
            field=models.OneToOneField(default='', on_delete=django.db.models.deletion.CASCADE, to='client.persona'),
            preserve_default=False,
        ),
    ]
