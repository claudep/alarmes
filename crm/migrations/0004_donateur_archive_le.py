from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_replace_client_by_persona'),
    ]

    operations = [
        migrations.AddField(
            model_name='donateur',
            name='archive_le',
            field=models.DateField(blank=True, null=True, verbose_name='Archivé le'),
        ),
    ]
