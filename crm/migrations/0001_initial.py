import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campagne',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, unique=True, verbose_name='Nom')),
                ('lancement', models.DateField(verbose_name='Date de début de campagne')),
                ('info_qr', models.CharField(blank=True, max_length=50, verbose_name='Info suppl. sur bulletin')),
            ],
        ),
        migrations.CreateModel(
            name='Origine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, unique=True, verbose_name='Nom')),
            ],
        ),
        migrations.CreateModel(
            name='Don',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateField(auto_now_add=True)),
                ('date_don', models.DateField(verbose_name='Date du don')),
                ('info_don', models.CharField(blank=True, max_length=200, verbose_name='Motif du don')),
                ('debiteur', models.JSONField(blank=True, null=True, verbose_name='Infos bancaires débiteur')),
                ('montant', models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Montant')),
                ('campagne', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.campagne')),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name="dons", to='client.client')),
            ],
        ),
        migrations.CreateModel(
            name='Donateur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('no_wesser', models.IntegerField(blank=True, null=True, verbose_name='N° adhérent Wesser')),
                ('client', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='client.client')),
                ('ne_pas_remercier', models.BooleanField(default=False, verbose_name='Ne pas remercier')),
                ('origine', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.origine', verbose_name="Origine")),
                ('politesse', models.CharField(blank=True, max_length=100, verbose_name='Politesse')),
            ],
        ),
    ]
