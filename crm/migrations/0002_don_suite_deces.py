from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deces',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=80, unique=True, verbose_name='Nom')),
            ],
        ),
        migrations.AddField(
            model_name='don',
            name='client_data',
            field=models.JSONField(blank=True, null=True, verbose_name='Données de client'),
        ),
        migrations.AddField(
            model_name='don',
            name='deces',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='crm.deces'),
        ),
    ]
