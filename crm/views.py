from datetime import date, timedelta
from difflib import SequenceMatcher
from operator import itemgetter

from defusedxml import ElementTree as ET
import xmltodict

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction
from django.db.models import Q, Sum
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, FormView, ListView, TemplateView, UpdateView, View

from client.forms import ClientFilterFormBase
from client.models import Journal, PersonaBaseQuerySet
from client.views import ClientAccessCheckMixin, ClientJournalMixin, ClientJournalView, ClientListViewBase
from common.choices import Services
from common.utils import strip_accents
from common.views import (
    CreateUpdateView, FichierDeleteView, FichierEditView, FichierListeView,
    FilterFormMixin
)

from . import forms
from .camt_parser import Camt053Parser
from .models import Campagne, Don, Donateur, Persona


class HomeView(TemplateView):
    template_name = "crm/index.html"


class DonateursListeView(ClientListViewBase):
    model = Donateur
    permission_required = 'crm.view_donateur'
    template_name = 'crm/donateur_list.html'
    filter_formclass = forms.DonateurFilterForm
    return_all_if_unbound = False

    def get_queryset(self):
        if self.is_archive:
            base_qs = Donateur.objects.filter(archive_le__isnull=False)
        else:
            base_qs = Donateur.objects.filter(archive_le__isnull=True)
        self.total_count = base_qs.count()
        return super(ClientListViewBase, self).get_queryset(base_qs=base_qs).avec_adresse(date.today()).order_by(
            'persona__nom', 'persona__prenom'
        ).prefetch_related("persona__telephone_set").distinct()

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(), "count": self.total_count}


class DonateurEditView(ClientAccessCheckMixin, ClientJournalMixin, CreateUpdateView):
    model = Donateur
    profile_model = Donateur  # Pour ClientAccessCheckMixin
    form_class = forms.DonateurEditForm
    template_name = "crm/donateur_edit.html"
    journal_add_message = "Création du donateur"
    journal_edit_message = "Modification du donateur: {fields}"
    success_url = reverse_lazy("donateurs")

    def get_success_url(self):
        return reverse("donateur-edit", args=[self.object.pk])

    def get_success_message(self, obj):
        if self.is_create:
            return "La création d’un nouveau donateur a réussi."
        else:
            return f"«{obj.nom_prenom}» a bien été modifié·e"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.is_create:
            context.update({
                'adresse': self.object.adresse(date.today()),
                'adresse_future': self.object.persona.adresseclient_set.filter(principale__startswith__gt=date.today()).first(),
                'can_be_archived': self.object.can_be_archived(self.request.user),
                'fichiers': self.object.persona.fichiers_app("crm").order_by('-quand'),
            })
            if client := self.object.persona.get_client():
                context["donateur"].services = client.prestations_actuelles(as_services=True)
        return context


class DonateurFromPersonaView(TemplateView):
    template_name = "crm/donateur_from_persona.html"

    def post(self, request, *args, **kwargs):
        persona = get_object_or_404(Persona, pk=self.kwargs["pers_pk"])
        with transaction.atomic():
            donateur = Donateur.objects.create(persona_id=self.kwargs["pers_pk"])
            Journal.objects.create(
                persona=donateur.persona, description=f"Création en tant que donateur",
                quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(reverse("donateur-edit", args=[donateur.pk]))

    def get_context_data(self, **kwargs):
        persona = get_object_or_404(Persona, pk=self.kwargs["pers_pk"])
        return {**super().get_context_data(**kwargs), "persona": persona}


class DonateurDonsView(ListView):
    template_name = "crm/donateur_dons.html"
    model = Don

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        self.donateur = get_object_or_404(Donateur, pk=kwargs["pk"])

    def get_queryset(self):
        return self.donateur.dons.order_by("-date_don")

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'donateur': self.donateur}


class DonEditView(PermissionRequiredMixin, CreateUpdateView):
    permission_required = "crm.change_don"
    template_name = "general_edit.html"
    pk_url_kwarg = "don_pk"
    model = Don
    form_class = forms.DonEditForm
    json_response = True

    def form_valid(self, form):
        if self.is_create:
            form.instance.donateur = get_object_or_404(Donateur, pk=self.kwargs["pk"])
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class DonateurFichierEditView(PermissionRequiredMixin, FichierEditView):
    parent_model = Persona
    permission_required = "crm.change_donateur"

    def get_related_object(self):
        donateur = get_object_or_404(Donateur, pk=self.kwargs['pk'])
        return donateur.persona


class DonateurFichierListeView(PermissionRequiredMixin, FichierListeView):
    parent_model = Persona
    permission_required = "crm.view_donateur"
    editurl_name = "donateur-fichier-edit"
    deleteurl_name = "donateur-fichier-delete"


class DonateurFichierDeleteView(PermissionRequiredMixin, FichierDeleteView):
    parent_model = Persona
    permission_required = "crm.change_donateur"


class DonateurJournalView(ClientJournalView):
    profile_model = Donateur
    template_name = "crm/donateur_journal.html"


class DonateurArchiveView(PermissionRequiredMixin, UpdateView):
    permission_required = "crm.change_donateur"
    form_class = forms.DonateurArchiveForm
    model = Donateur
    template_name = "crm/donateur_archiver.html"

    def form_valid(self, form):
        donateur = self.object
        if self.request.POST.get("btn_action") == "archiver":
            with transaction.atomic():
                donateur.archive_le = date.today()
                donateur.motif_archive = form.cleaned_data["motif_archive"]
                donateur.save(update_fields=["archive_le", "motif_archive"])
                Journal.objects.create(
                    persona=donateur.persona,
                    description=f"Archivage (motif: {donateur.motif_archive})",
                    quand=timezone.now(), qui=self.request.user
                )
            msg = f"La personne {donateur} a bien été archivée."
            messages.success(self.request, msg)
        elif self.request.POST.get("btn_action") == "supprimer":
            persona = donateur.persona
            with transaction.atomic():
                donateur.dons.update(donateur=None)
                if client := persona.get_client():
                    messages.warn(self.request, (
                        "Cette personne a ou a été cliente de services Croix-Rouge, il n’est pas "
                        "possible de la supprimer entièrement. Seules ses informations en tant que "
                        "donatrice seront supprimées."
                    ))
                    donateur.delete()
                else:
                    msg = f"La personne {donateur} a été complètement supprimée de la base de données."
                    persona.delete()
                    messages.success(self.request, msg)
        elif donateur.archive_le:
            # Désarchiver
            with transaction.atomic():
                donateur.archive_le = None
                donateur.motif_archive = ""
                donateur.save(update_fields=["archive_le"])
                Journal.objects.create(
                    persona=donateur.persona, description="Réactivation",
                    quand=timezone.now(), qui=self.request.user
                )
            msg = f"La personne {donateur} a bien été réactivée."
            messages.success(self.request, msg)
        return HttpResponseRedirect(reverse("donateurs"))


class ImportationDonsView(FormView):
    form_class = forms.ImportForm
    template_name = "crm/import_dons.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["recents"] = Don.objects.filter(
            created__gt=date.today() - timedelta(days=120)
        ).values_list("created", flat=True).distinct().order_by("-created")
        return context

    def form_valid(self, form):
        fichier = form.cleaned_data['fichier']
        parser = Camt053Parser(fichier.read().decode('utf-8'))
        group_header = parser.get_group_header()
        statement_info = parser.get_statement_info()
        transactions = parser.get_transactions()
        self.campagnes = {cp.nom: cp for cp in Campagne.objects.all()}
        with transaction.atomic():
            for trans in transactions:
                if trans["CreditDebitIndicator"] != "CRDT":
                    continue
                info_don = trans.get("RemittanceInformation", "") or ""
                donateur = None
                no_membre = None
                # If W in info_don, catch no membre wesser, and remove from info_don
                info_don_stripped, no_membre = Don.no_membre_from_info_don(info_don)
                if no_membre:
                    try:
                        donateur = Donateur.objects.get(no_wesser=no_membre)
                    except Donateur.DoesNotExist:
                        pass
                debit_info = xmltodict.parse(ET.tostring(trans['Debtor']), namespaces={'ns0': None})
                debit_info = {k: v for k, v in list(debit_info.values())[0].items() if not k.startswith('@')}
                if not donateur:
                    donateur = self.match_donateur(trans, debit_info)
                    if donateur and no_membre:
                        donateur.no_wesser = no_membre
                        donateur.save(update_fields=["no_wesser"])
                campagne = self.match_campagne(info_don)
                don = Don.objects.create(
                    donateur=donateur,
                    campagne=campagne,
                    date_don=trans['ValueDate'],
                    info_don=info_don_stripped if donateur else info_don,
                    debiteur=debit_info,
                    montant=trans['Amount'],
                )
        today = date.today()
        return HttpResponseRedirect(reverse('importer-rapport', args=[today.year, today.month, today.day]))

    def match_campagne(self, info_don):
        if not info_don:
            return None
        matches = sorted([
            (SequenceMatcher(None, cp.info_qr or cp.nom, info_don).ratio(), cp)
            for key, cp in self.campagnes.items()
        ], key=itemgetter(0), reverse=True)
        return matches[0][1] if matches and matches[0][0] >= 0.4 else None

    def match_donateur(self, transaction, debit_info):
        noms = [part.strip(",") for part in transaction['DebtorName'].split()]
        rue = ""
        npa = ""
        adr_lines = debit_info['PstlAdr'].get('AdrLine')
        if adr_lines and not isinstance(adr_lines, list):
            adr_lines = [adr_lines]
        if 'StrtNm' in debit_info['PstlAdr']:
            rue = debit_info['PstlAdr']['StrtNm']
            if rue_no := debit_info['PstlAdr'].get('BldgNb'):
                if rue_no not in rue:
                    rue = " ".join([rue, rue_no])
        elif adr_lines:
            rue = adr_lines[0]
        if 'PstCd' in debit_info['PstlAdr']:
            npa = debit_info['PstlAdr']['PstCd']
        elif adr_lines:
            npa = next((part for part in adr_lines[-1].split() if len(part) == 4 and part.isdigit()), None)

        # Rechercher une première fois dans les donateurs
        try:
            donateur = Donateur.objects.avec_adresse(date.today(), unaccent=True).get(
                (
                    Q(persona__nom__unaccent__icontains=noms[0]) |
                    Q(persona__nom__unaccent__icontains=noms[-1])
                ),
                adresse_active__rue__icontains=strip_accents(rue),
                adresse_active__npa=npa,
            )
        except (Donateur.DoesNotExist, Donateur.MultipleObjectsReturned):
            pass
        else:
            return donateur

        # Rechercher une deuxième fois plus largement dans Persona
        try:
            persona = Persona.objects.all().avec_adresse(date.today()).get(
                (
                    Q(nom__unaccent__icontains=noms[0]) |
                    Q(nom__unaccent__icontains=noms[-1])
                ),
                adresse_active__rue__icontains=strip_accents(rue),
                adresse_active__npa=npa,
            )
        except (Persona.DoesNotExist, Persona.MultipleObjectsReturned):
            pass
        else:
            return Donateur.objects.create(persona=persona)
        return None  # pas de correspondance unique et exacte trouvée.


class RapportImportationView(TemplateView):
    template_name = "crm/import_rapport.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dons'] = Don.objects.filter(
            created=date(self.kwargs['year'], self.kwargs['month'], self.kwargs['day'])
        ).annotate(
            adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                date.today(), outer_field_name='donateur__persona_id'
            )
        ).select_related("donateur__persona", "campagne").order_by('date_don')
        return context


class RechercheDonateurView(FilterFormMixin, ListView):
    template_name = "crm/recherche_donateur.html"
    model = Donateur
    filter_formclass = ClientFilterFormBase
    return_all_if_unbound = False

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Donateur.objects.exclude(persona__date_deces__isnull=False).avec_adresse(date.today())
        ).order_by("persona__nom", "persona__prenom")


class AttribuerDonView(View):
    def post(self, request, *args, **kwargs):
        don = get_object_or_404(Don, pk=kwargs['don_pk'])
        donateur = get_object_or_404(Donateur, pk=kwargs['donateur_pk'])
        don.donateur = donateur
        don.save()
        return JsonResponse({
            'result': 'OK',
            'donateur': "<br>".join([str(donateur), str(donateur.adresse())]),
        })


class CreerDonDonateurView(CreateView):
    """Créer donateur à partir du champ debiteur du don."""
    form_class = forms.DonateurEditForm
    template_name = "general_edit.html"

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        self.don = get_object_or_404(Don, pk=kwargs["don_pk"])

    def get_initial(self):
        lines = self.don.debiteur_as_lines()
        prenom, nom = lines[0].split(maxsplit=1)
        npa, localite = lines[-1].split(maxsplit=1)
        return {
            **super().get_initial(), "nom": nom, "prenom": prenom,
            "rue": lines[1], "npa": npa, "localite": localite,
            "no_wesser": Don.no_membre_from_info_don(self.don.info_don)[1] or "",
        }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if kwargs.get("data"):
            # Simuler absence des champs téléphone
            kwargs["data"] = kwargs["data"].copy()
            kwargs["data"]["telephone_set-TOTAL_FORMS"] = 0
            kwargs["data"]["telephone_set-INITIAL_FORMS"] = 0
        return kwargs

    def form_valid(self, form):
        with transaction.atomic():
            donateur = form.save()
            Journal.objects.create(
                persona=donateur.persona, description="Création", quand=timezone.now(),
                qui=self.request.user
            )
            self.don.donateur = donateur
            self.don.info_don = Don.no_membre_from_info_don(self.don.info_don)[0]
            self.don.save()
        return JsonResponse({
            'result': 'OK',
            'donpk': self.don.pk,
            'client': "<br>".join([str(donateur), str(donateur.adresse())]),
        })


class CampagneListView(PermissionRequiredMixin, ListView):
    model = Campagne
    permission_required = "crm.view_donateur"
    template_name = "crm/campagne_list.html"

    def get_queryset(self):
        return super().get_queryset().annotate(
            somme_dons=Sum("don__montant")
        ).order_by("-lancement")


class CampagneEditView(PermissionRequiredMixin, CreateUpdateView):
    model = Campagne
    permission_required = "crm.add_campagne"
    form_class = forms.CampagneEditForm
    template_name = "crm/campagne_edit.html"
    success_url = reverse_lazy("campagnes")

    def get_success_message(self, obj):
        if self.is_create:
            return "La campagne a bien été créée."
        else:
            return f"La campagne «{obj}» a bien été modifiée."
