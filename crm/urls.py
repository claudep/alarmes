from django.conf import settings
from django.urls import include, path

from client import views as client_views
from . import views, views_stats
from .models import Donateur

handler500 = 'common.views.error_view'

urlpatterns = [
    path('', include('common.urls')),
    path('', include('client.urls')),
    path('', views.HomeView.as_view(), name='home'),
    path('donateurs/', views.DonateursListeView.as_view(), name='donateurs'),
    path('donateurs/archives/', views.DonateursListeView.as_view(is_archive=True), name='donateurs-archives'),
    path('donateurs/nouveau/', views.DonateurEditView.as_view(is_create=True), name='donateur-new'),
    path('donateurs/from_persona/<int:pers_pk>/', views.DonateurFromPersonaView.as_view(), name='donateur-from-persona'),
    path('donateurs/<int:pk>/edition/', views.DonateurEditView.as_view(is_create=False), name='donateur-edit'),
    path('donateurs/<int:pk>/dons/', views.DonateurDonsView.as_view(), name='donateur-dons'),
    path('donateurs/<int:pk>/archiver/', views.DonateurArchiveView.as_view(), name='donateur-archive'),
    path('donateurs/importation/', views.ImportationDonsView.as_view(), name='importer-dons'),
    path('donateurs/imprapport/<int:year>/<int:month>/<int:day>/', views.RapportImportationView.as_view(),
        name='importer-rapport'),
    path('donateurs/chercher/<int:don_pk>/', views.RechercheDonateurView.as_view(), name='recherche-donateur'),
    path('donateurs/<int:pk>/dons/nouveau/', views.DonEditView.as_view(is_create=True), name='don-new'),
    path('donateurs/don/<int:don_pk>/edition/', views.DonEditView.as_view(is_create=False), name='don-edit'),
    path('donateurs/don/<int:don_pk>/attrib/<int:donateur_pk>/', views.AttribuerDonView.as_view(),
        name='don-attribuer'),
    path('donateurs/don/<int:don_pk>/creerdonateur/', views.CreerDonDonateurView.as_view(),
        name='don-creerdonateur'),

    # Fichiers
    path('<int:pk>/fichier/nouveau/', views.DonateurFichierEditView.as_view(is_create=True),
        name='donateur-fichier-add'),
    path('<int:pk>/fichier/<int:pk_file>/edition/', views.DonateurFichierEditView.as_view(is_create=False),
        name='donateur-fichier-edit'),
    path('<int:pk>/fichier/liste/', views.DonateurFichierListeView.as_view(),
        name='donateur-fichier-list'),
    path('<int:pk>/supprimer/', views.DonateurFichierDeleteView.as_view(),
        name='donateur-fichier-delete'),

    path('<int:pk>/journal/', views.DonateurJournalView.as_view(), name='donateur-journal'),
    path('<int:pk>/journal/nouveau/',
        client_views.ClientJournalEditView.as_view(is_create=True, profile_model=Donateur),
        name='client-journal-add'),
    path('<int:pk>/journal/<int:pk_jr>/edition/',
        client_views.ClientJournalEditView.as_view(is_create=False, profile_model=Donateur),
        name='client-journal-edit'),
    path('<int:pk>/journal/<int:pk_jr>/details/',
        client_views.ClientJournalDetailsView.as_view(profile_model=Donateur),
        name='client-journal-details'),

    path('campagnes/', views.CampagneListView.as_view(), name='campagnes'),
    path('campagnes/nouvelle/', views.CampagneEditView.as_view(is_create=True), name='campagne-new'),
    path('campagnes/<int:pk>/edition/', views.CampagneEditView.as_view(is_create=False), name='campagne-edit'),

    # Statistiques
    path('stats/', views_stats.StatsDonsView.as_view(), name='stats-dons'),
    path('stats/export_annuel/', views_stats.ExportAnnuelView.as_view(), name='export-annuel'),

    path('', include(f'{settings.CANTON_APP}.urls')),
]
