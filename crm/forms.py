from django import forms

from client.forms import AdresseFormMixin, ClientFilterFormBase, PersonaFormMixin, PersonaForm
from client.models import AdresseClient
from common.forms import (
    BootstrapMixin, DateInput, ModelForm, NPALocaliteMixin, ReadOnlyableMixin
)
from .models import Campagne, Don, Donateur


class ImportForm(forms.Form):
    fichier = forms.FileField(label="Dons à importer (fichier camt)")

    def clean_fichier(self):
        fichier = self.cleaned_data['fichier']
        if not fichier.name.endswith('.xml'):
            raise forms.ValidationError("Ce fichier ne semble pas être un fichier camt")
        return fichier


class DonateurFilterForm(ClientFilterFormBase):
    exclure_ne_pas_soll = forms.BooleanField(
        label="Exclure donateurs ne souhaitant pas être sollicités", required=False
    )

    def filter(self, donateurs):
        donateurs = super().filter(donateurs)
        if self.cleaned_data["exclure_ne_pas_soll"]:
            donateurs = donateurs.exclude(ne_pas_solliciter=True)
        return donateurs


class DonateurEditForm(BootstrapMixin, NPALocaliteMixin, AdresseFormMixin, PersonaFormMixin, ModelForm):
    langues = None
    langues_select = None

    class Meta:
        model = Donateur
        fields = [
            "politesse", "origine", "no_wesser", "ne_pas_remercier", "ne_pas_solliciter", "remarques"
        ]

    persona_fields = [
        'nom', 'prenom', 'genre', 'date_naissance', 'case_postale',
        'courriel', 'langues_select', 'langues', 'date_deces',
    ]
    adresse_fields = ["rue", "npa", "localite"]
    field_order = ['nom', 'prenom', 'genre', 'rue', 'case_postale', 'npalocalite']
    api_sync = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.instance.pk:
            del self.fields["langues"]
            del self.fields["langues_select"]
            del self.fields["date_deces"]


class DonateurArchiveForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Donateur
        fields = ["motif_archive"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields["motif_archive"].required = True


class CampagneEditForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Campagne
        fields = ["nom", "lancement", "info_qr"]
        widgets = {
            "lancement": DateInput,
        }


class DonEditForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Don
        fields = ["date_don", "campagne", "montant"]
        widgets = {
            "date_don": DateInput,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields["campagne"].queryset = self.fields["campagne"].queryset.order_by("-lancement")
