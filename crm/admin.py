from datetime import date

from django.contrib import admin

from common.admin import NullableAsBooleanListFilter
from .models import Campagne, Don, Donateur, Origine


@admin.register(Origine)
class OriginAdmin(admin.ModelAdmin):
    list_display = ["nom"]


@admin.register(Campagne)
class CampagneAdmin(admin.ModelAdmin):
    list_display = ["nom", "lancement", "info_qr"]


@admin.register(Donateur)
class DonateurAdmin(admin.ModelAdmin):
    list_display = ["persona", "rue", "npa", "localite", "origine", "remarques", "actif"]
    list_filter = [
        ('archive_le', NullableAsBooleanListFilter)
    ]
    raw_id_fields = ["persona"]
    ordering = ["persona__nom", "persona__prenom"]
    search_fields = ["persona__nom"]

    def get_queryset(self, request):
        return super().get_queryset(request).avec_adresse(date.today())

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.archive_le is None

    def rue(self, obj):
        return (obj.adresse_active or {}).get('rue', '')

    def npa(self, obj):
        return (obj.adresse_active or {}).get('npa', '')

    def localite(self, obj):
        return (obj.adresse_active or {}).get('localite', '')


@admin.register(Don)
class DonAdmin(admin.ModelAdmin):
    list_display = ["date_don", "donateur", "campagne", "deces", "montant"]
    list_filter = ["campagne"]
    raw_id_fields = ["donateur"]
