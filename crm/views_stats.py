from datetime import date

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Count, Q, Sum
from django.db.models.functions import TruncMonth
from django.views.generic import TemplateView, View

from common.export import ExpLine, OpenXMLExport
from common.stat_utils import DateLimitForm, Month, StatsMixin
from .models import Don, Donateur


class StatsDonsView(StatsMixin, TemplateView):
    template_name = "crm/stats/dons.html"
    form_class = DateLimitForm

    def get_stats(self, months):
        months = [m for m in months if not m.is_future()]
        query = Don.objects.filter(
            date_don__range=(self.date_start, self.date_end)
        ).annotate(
            month=TruncMonth("date_don"),
        ).values("month").annotate(
            nb_dons=Count("id"),
            sum_dons=Sum("montant"),
        ).order_by("month")
        by_month = {m: {
            "nb_dons": 0,
            "sum_dons": 0,
        } for m in months}
        by_month["nb_dons_total"] = 0
        by_month["sum_dons_total"] = 0
        for line in query:
            mkey = Month(line["month"].year, line["month"].month)
            by_month[mkey] = line
            by_month["nb_dons_total"] += line["nb_dons"]
            by_month["sum_dons_total"] += line["sum_dons"]
        return {
            "stats": by_month,
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "annee_passee": date.today().year - 1,
        }

    def export_lines(self, context):
        yield ExpLine([
            f"Statistiques des dons entre {self.date_start.strftime('%d.%m.%Y')} et "
            f"{self.date_end.strftime('%d.%m.%Y')}"
        ], bold=True)
        yield ExpLine([""] + [str(month) for month in context["months"]] + ["Total"], bold=True)
        yield ExpLine(
            ["Nombre de dons"] +
            [context["stats"][month]["nb_dons"] for month in context["months"]] +
            [context["stats"]["nb_dons_total"]
        ])
        yield ExpLine(
            ["Montant des dons"] +
            [context["stats"][month]["sum_dons"] for month in context["months"]] +
            [context["stats"]["sum_dons_total"]
        ])


class ExportAnnuelView(PermissionRequiredMixin, View):
    permission_required = "crm.change_donateur"

    def post(self, *args, **kwargs):
        annee = date.today().year - 1
        export = OpenXMLExport(sheet_title=f"Résumés dons {annee}", col_widths=[22, 28, 28, 30, 22, 22, 12])
        export.fill_data(self.export_lines(annee))
        return export.get_http_response(f"exportation_dons_{annee}.xlsx")

    def export_lines(self, annee):
        donateurs = Donateur.objects.avec_adresse(date.today()).annotate(
            dons_annuels=Sum("dons__montant", filter=Q(dons__date_don__year=annee))
        ).filter(dons_annuels__gt=0).order_by(
            "persona__nom", "persona__prenom"
        ).select_related("persona")

        yield ExpLine(["Politesse", "Nom", "Prénom", "Rue", "Localité", "Courriel", f"Montant {annee}"], bold=True)

        for donateur in donateurs:
            pers = donateur.persona
            adr = pers.adresse()
            yield ExpLine([
                donateur.politesse, pers.nom, pers.prenom, adr.rue if adr else "",
                f"{adr.npa} {adr.localite}" if (adr and adr.npa) else "", pers.courriel,
                donateur.dons_annuels,
            ])
