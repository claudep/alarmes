'use strict';

function containerFilled(ev) {
    const container = ev.target;
    // modal to link client to don
    attachHandlerSelector(container, '.client_list', 'click', async (evt) => {
        if (evt.target.classList.contains("select_client")) {
            const donPk = evt.target.closest("form").dataset.donpk;
            const attribURL = evt.target.dataset.chooseurl;
            // fetch post to link don to this client
            const formData = new FormData();
            addCSRFToken(formData);
            const response = await fetch(attribURL, {method: 'POST', body: formData});
            const json = await response.json();
            // replace "donateur non trouve" by this client
            const donateurDiv = document.querySelector(`#donateur_${donPk}`);
            donateurDiv.innerHTML = json.donateur;
            hide(donateurDiv.parentNode.querySelector(".creer_donateur"));
            // close popup
            const modal = bootstrap.Modal.getInstance(document.querySelector('#siteModal'));
            modal.hide()
        }
    });
}

window.addEventListener('DOMContentLoaded', () => {
    attachHandlerSelector(document, '#siteModal', 'containerfilled', containerFilled);
    attachHandlerSelector(document, 'body', 'modalsubmitted', (ev) => {
        const data = ev.detail;
        const donateurDiv = document.querySelector(`#donateur_${data.donpk}`);
        // Logique après soumission de la modale de création de donateur
        if (donateurDiv) {
            donateurDiv.innerHTML = data.client;
            hide(donateurDiv.parentNode.querySelector(".creer_donateur"));
        }
    });
});
