import re

from django.db import models
from django.urls import reverse

from client.models import Persona, PersonaBaseQuerySet, PersonaProxyFields


class Origine(models.Model):
    nom = models.CharField("Nom", max_length=50, unique=True)

    def __str__(self):
        return self.nom


class DonateurQuerySet(PersonaBaseQuerySet):
    adr_outer_field = "persona_id"


class Donateur(PersonaProxyFields, models.Model):
    """Profil pour partenaire donateur."""
    class MotifsArchivage(models.TextChoices):
        DECEDE = "decede", "Décédé"
        AUCUNE = "aucune", "Aucune indication"
        FINANCES = "prob_fin", "Problèmes financiers"
        DOUBLON = "doublon", "Double adhésion"
        UNIQUE = "unique", "Don unique"
        NONCOTIS = "non_cotis", "Pas de paiement de cotisation"
        AUTRESORG = "autresorg", "Soutien d’autres organisations"
        EMS = "ems", "La personne n’est pas autonome / EMS"
        RETRAITE = "retraite", "Retraité"
        REFUS = "refus", "Courrier refusé"
        COLERE = "colere", "Colère envers l’association"
        EFFACE = "efface", "Effacement des données"

    persona = models.OneToOneField(Persona, on_delete=models.CASCADE)
    politesse = models.CharField("Politesse", max_length=100, blank=True)
    origine = models.ForeignKey(Origine, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="Origine")
    ne_pas_remercier = models.BooleanField("Ne pas remercier", default=False)
    ne_pas_solliciter = models.BooleanField("Ne pas solliciter", default=False)
    remarques = models.TextField("Remarques", blank=True)
    no_wesser = models.IntegerField("N° adhérent Wesser", blank=True, null=True)
    archive_le = models.DateField("Archivé le", blank=True, null=True)
    motif_archive = models.CharField(
        "Raison de l’archivage", max_length=10, choices=MotifsArchivage, blank=True
    )

    objects = DonateurQuerySet.as_manager()

    def __str__(self):
        return self.persona.nom_prenom

    def get_absolute_url(self):
        return reverse("donateur-edit", args=[self.pk])

    def can_read(self, user):
        if user.has_perm("crm.view_donateur"):
            return True

    def can_edit(self, user):
        return user.has_perm("crm.change_donateur")

    def can_be_archived(self, user, *args):
        return self.can_edit(user)


class Campagne(models.Model):
    nom = models.CharField("Nom", max_length=50, unique=True)
    lancement = models.DateField("Date de début de campagne")
    info_qr = models.CharField("Info suppl. sur bulletin", max_length=50, blank=True)

    def __str__(self):
        return self.nom


class Deces(models.Model):
    nom = models.CharField("Nom", max_length=80, unique=True)

    def __str__(self):
        return f"Décès de {self.nom}"


class Don(models.Model):
    created = models.DateField(auto_now_add=True)
    donateur = models.ForeignKey(Donateur, on_delete=models.PROTECT, blank=True, null=True, related_name="dons")
    # Données de donateur non relationnelles quand le donateur ne doit pas être suivi (par ex. dons décès)
    donateur_data = models.JSONField("Données du donateur", blank=True, null=True)
    campagne = models.ForeignKey(Campagne, on_delete=models.SET_NULL, blank=True, null=True)
    deces = models.ForeignKey(Deces, on_delete=models.SET_NULL, blank=True, null=True)
    date_don = models.DateField("Date du don")
    info_don = models.CharField("Motif du don", max_length=200, blank=True)
    debiteur = models.JSONField("Infos bancaires débiteur", blank=True, null=True)
    montant = models.DecimalField("Montant", max_digits=9, decimal_places=2)

    def __str__(self):
        return f"Don de CHF {self.montant} du {self.date_don.strftime('%d.%m.%Y')}"

    @classmethod
    def no_membre_from_info_don(cls, info_don):
        if found := re.search(r"W ?(\d+)", info_don):
            no_membre = int(found.groups()[0])
            return (info_don.replace(found.group(), "").strip(), no_membre)
        return info_don, None

    def debiteur_as_lines(self):
        """Mettre en forme les données du champ JSON debiteur en lignes d'adresse."""
        if not self.debiteur:
            return []
        lines = [self.debiteur['Nm']]
        adr = self.debiteur["PstlAdr"]
        if "AdrLine" in adr:
            if isinstance(adr["AdrLine"], str):
                lines.append(adr["AdrLine"])
            elif isinstance(adr["AdrLine"], list):
                lines.append("".join(adr["AdrLine"]))
            if lines[-1].isupper():
                lines[-1] = lines[-1].title()
        else:
            lines.append(adr["StrtNm"])
            if adr.get("BldgNb") and not lines[-1].endswith(adr["BldgNb"]):
                lines[-1] += f" {adr['BldgNb']}"
            lines.append(" ".join([adr["PstCd"], adr["TwnNm"]]))
        return lines
