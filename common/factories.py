import factory

from django.conf import settings


class UtilisateurFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL

    email = factory.Faker("email")
    password = factory.Faker("password")


class SuperUserFactory(UtilisateurFactory):
    is_staff = True
    is_superuser = True
