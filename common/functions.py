from django.db.models import BooleanField
from django.db.models.expressions import Func


class PtDist(Func):
    function = "pt_distance"
    arity = 2


class Unaccent(Func):
    function= "unaccent"
    arity = 1


class IsAVS(Func):
    """
    Paramètres: date_evenement, tarif_avs_des, date_naissance, genre
    Return True si:
        client.tarif_avs_des <= date_evenement ou client en âge AVS
    """
    function = "is_avs"
    arity = 4
    output_field = BooleanField()


class IsOFASTransport(Func):
    """
    Paramètres: id transport, date_transport, tarif_avs_des, date_naissance, genre
    Return True si:
        categ transport est médical ou participatif, client.tarif_avs_des <= date_transport ou client en âge AVS
    """
    function = "is_ofas"
    arity = 5
    output_field = BooleanField()
