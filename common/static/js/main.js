'use strict';

function hide(selector_or_el) {
    if (typeof selector_or_el === 'string' || selector_or_el instanceof String) {
        document.querySelectorAll(selector_or_el).forEach(el => el.setAttribute('hidden', true));
    } else {
        selector_or_el.setAttribute('hidden', true);
    }
}
function show(selector_or_el) {
    if (typeof selector_or_el === 'string' || selector_or_el instanceof String) {
        document.querySelectorAll(selector_or_el).forEach(el => el.removeAttribute('hidden'));
    } else {
        selector_or_el.removeAttribute('hidden');
    }
}

const waiter = {
    'init': () => {
        this.overlay = document.querySelector('#waitOverlay');
        this.tout = null;
    },
    'show': () => {
        /* delay a bit if action is quick; */
        this.tout = setTimeout(() => show(this.overlay), 400);
    },
    'hide': () => {
        clearTimeout(this.tout);
        hide(this.overlay);
    }
}

async function openModal(ev) {
    const modalDiv = document.getElementById('siteModal');
    modalDiv.querySelector('.modal-dialog').style = '';
    if (modalDiv.dataset.oldhtmlcontent) {
        // Restore any content that was temporarily overwritten
        modalDiv.querySelector('.modal-content').innerHTML = modalDiv.dataset.oldhtmlcontent
        delete modalDiv.dataset.oldhtmlcontent;
    }
    const modal = new bootstrap.Modal(modalDiv);

    ev.preventDefault();
    const element = ev.currentTarget;
    if (!element.classList.contains('openmodal')) return false;  // because of ev. delegation
    ev.stopPropagation();
    const modalBody = modalDiv.querySelector('.modal-body'),
          modalWait = modalDiv.querySelector('.modal-wait');
    modalBody.innerHTML = '';
    hide(modalBody);
    let tout = setTimeout(() => show(modalWait), 400);
    modal.show();
    try {
        const resp = await fetch(element.href || element.dataset.modalurl);
        const html = await resp.text();
        clearTimeout(tout);
        modalBody.innerHTML = html;
        if (modalWait) hide(modalWait);
        show(modalBody);
        modalDiv.querySelector('.modal-title').textContent = (
            element.title || element.dataset.bsTitle || element.getAttribute("aria-label") || "Édition"
        );
        modalDiv.querySelector('#modal-save').textContent = element.dataset.savelabel || "Enregistrer";
        const frm = modalBody.querySelector('form');
        if (frm && frm.classList.contains('readonly')) {
            hide(modalDiv.querySelector('.modal-footer.edition'));
            show(modalDiv.querySelector('.modal-footer.readonly'));
        } else if (frm && !frm.classList.contains('no-button-bar')) {
            hide(modalDiv.querySelector('.modal-footer.readonly'));
            show(modalDiv.querySelector('.modal-footer.edition'));
            // Show/hide Supprimer button depending on data-deleteurl presence.
            if (element.dataset.deleteurl) {
                const modalDelete = modalDiv.querySelector('#modal-delete');
                modalDelete.textContent = element.dataset.deletelabel || "Supprimer";
                show(modalDelete);
                if (element.dataset.deletemethod) modalDelete.form.method = element.dataset.deletemethod;
                modalDelete.formaction = element.dataset.deleteurl;
                if (element.dataset.deleteconfirm) modalDelete.dataset.confirm = element.dataset.deleteconfirm;
            } else hide('#modal-delete');
        } else {
            hide(modalDiv.querySelector('.modal-footer.edition'));
        }
        attachModalHandlers(modalBody);
        modalDiv.dispatchEvent(new Event('containerfilled'));
    } catch(err) {
        console.log(err);
        if (modalWait) hide(modalWait);
    }
}

function syncTypeMissionAndAbo() {
    const tmField = document.getElementById('id_type_mission');
    const aboField = document.getElementById('id_abonnement');
    if (aboField && ['NEW', 'CHANGE'].includes(tmField.selectedOptions[0].dataset.code)) {
        show(aboField.closest('div'));
    } else if (aboField) {
        hide(aboField.closest('div'));
    }
}

function attachModalHandlers(modalBody) {
    attachHandlers(modalBody);
    // Hook form submit with modalSubmit
    modalBody.querySelectorAll('form:not(.normal_submit)').forEach(form => {
        form.addEventListener('submit', modalSubmit);
        form.addEventListener('change', monitorFormChange);
    });
    // Ensure save btn is enabled (after being disabled by avoidDoubleSubmission)
    const saveBtn = modalBody.parentNode.querySelector('#modal-save');
    if (saveBtn) {
        if (saveBtn.form.classList.contains('readonly')) saveBtn.setAttribute('disabled', true);
        else saveBtn.removeAttribute('disabled');
    }
    // Hide/show abo field in Mission form
    if (document.getElementById('id_type_mission')) {
        syncTypeMissionAndAbo();
        document.getElementById('id_type_mission').addEventListener('change', syncTypeMissionAndAbo);
    }
}

function setupAutocomplete(section) {
    section.querySelectorAll('.autocomplete').forEach(inp => {
        if (inp.id.includes('__prefix__')) return;
        new Autocomplete(inp, {
            maximumItems: 20,
            onInput: async (ac, value) => {
                if (value) {
                    const resp = await fetch(`${inp.dataset.searchurl}?q=${encodeURIComponent(value)}`);
                    const data = await resp.json();
                    if (data.result && data.result == 'error') {
                        ac.setData([{label: `<i>Erreur: ${data.message}</i>`, value: ''}])
                    } else {
                        // data expected as list of {'label'/'value'} objects.
                        ac.setData(data);
                    }
                } else ac.setData({});
            },
            onSelectItem: (data) => {
                if (inp.dataset.pkfield) {
                    const pkInput = inp.form.querySelector(`input[name=${inp.dataset.pkfield}]`);
                    pkInput.value = data.value;
                    pkInput.dispatchEvent(new Event('change'));
                }
                inp.dispatchEvent(new CustomEvent('itemselected', {detail: data}));
            }
        });
    });
}

/* Submit a form from a modal
 * If the form has the class 'normal_submit', a normal form submit happens.
 * Else the form is submitted by AJAX, the modal is closed on success or
 * re-displayed if the form contains errors.
 */
function modalSubmit(ev) {
    ev.preventDefault();
    const form = ev.target.form || ev.currentTarget;
    // get it soon as it can change in the flow of the function
    const activeElement = document.activeElement;
    const modal = document.getElementById('siteModal');
    if (form.method == 'post') avoidDoubleSubmit(form);
    if (form.classList.contains('normal_submit')) {
        form.submit();
        bootstrap.Modal.getInstance(modal).hide();
        return;
    }
    const formUrl = activeElement.formaction || activeElement.getAttribute("formaction") || form.action;
    const valid = form.reportValidity();
    if (!valid) return;
    form.dataset.changed = "false";
    if (ev.target.tagName == 'BUTTON' && ev.target.dataset.confirm) {
        var resp = confirm(ev.target.dataset.confirm);
        if (!resp) return;
    }
    if (form.method == 'get') {
        fetch(formUrl).then(resp => resp.text()).then(html => {
            const modalContent = modal.querySelector('.modal-content');
            modal.dataset.oldhtmlcontent = modalContent.innerHTML;
            modalContent.innerHTML = html;
            attachModalHandlers(modal.querySelector('.modal-body'));
        });
        return;
    }

    const data = new FormData(form);
    addCSRFToken(data);
    fetch(formUrl, {
        method: 'POST',
        body: data
    }).then(resp => {
        const contentType = resp.headers.get("content-type");
        if (!contentType || !contentType.includes("application/json")) {
            return resp.text().then(html => {
                // redisplay form (with probable errors)
                const modalBody = modal.querySelector('.modal-body')
                modalBody.innerHTML = html;
                attachModalHandlers(modalBody);
                throw new Error("The form did not validate.");
            });
        } else return resp.json();
    }).then(data => {
        // Post is success, hide modal and reload appropriate section
        bootstrap.Modal.getInstance(modal).hide();
        if (data.reload) reload(data.reload, data);
        document.querySelector("body").dispatchEvent(new CustomEvent('modalsubmitted', {detail: data}));
    }).catch(err => console.log(err.message));
}

async function reload(selector, data) {
    if (selector == 'page') {
        window.location.reload();
    } else if (selector.startsWith('/')) {
        window.location.replace(selector);
    } else if (selector.startsWith('page#')) {
        window.location.replace(window.location.pathname + '#' + selector.split('#')[1]);
        window.location.reload();
    } else if (selector == 'populateAC') {
        // The value returned by the popup should be used to populate an autocomplete widget combi.
        document.querySelector(`#${data.target}`).value = data.object.value;
        document.querySelector(`#${data.target}_select`).value = data.object.label;
    } else {
        const reloadElement = document.querySelector(selector);
        reloadElement.classList.add('reloading');
        const resp = await fetch(reloadElement.dataset.url, {method: 'GET'});
        let html = await resp.text();
        if (reloadElement.dataset.fragment) {
            // Extract a fragment of the response and use it as replacement.
            const parser = new DOMParser();
            const doc = parser.parseFromString(html, "text/html");
            const newElement = doc.querySelector(reloadElement.dataset.fragment);
            if (newElement) reloadElement.replaceWith(newElement);
            attachHandlers(newElement);
        } else {
            reloadElement.innerHTML = html;
            attachHandlers(reloadElement);
        }
        reloadElement.classList.remove('reloading');
    }
}

function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function submitAfterDelay(ev) {
    const code = (ev.keyCode || ev.which);
    // do nothing if it's an arrow key
    if (code == 37 || code == 38 || code == 39 || code == 40) {
        return;
    }
    submitForm(ev, ev.target.form);
}

function addCSRFToken(fdata) {
    if (!fdata.has('csrfmiddlewaretoken')) {
        fdata.append('csrfmiddlewaretoken', document.querySelector('input[name=csrfmiddlewaretoken]').value);
    }
}

async function submitFilterForm(form) {
    waiter.show();
    // we do not need empty filter fields
    const params = new URLSearchParams(Array.from(new FormData(form)).filter(val => val[1] != ""));
    // Set or remove bound_filter form class depending on form values
    const bound = params.entries().length > 0;
    bound ? form.classList.add('bound_filter') : form.classList.remove('bound_filter');
    const sep = form.action.includes('?') ? '&' : '?';
    // Add at least one empty param so the server knows the filter was emptied.
    if (!params.size) params.append(form.elements[0].name, "");
    const url = `${form.action}${sep}${params}`;
    const resp = await fetch(url, {headers: {'X-Requested-With': 'fetch'}});
    if ((resp.headers.get("Content-Disposition") || "").includes("attachment")) {
        // The result is a downloadable file
        if (document.querySelector("#id_listes")) document.querySelector("#id_listes").value = "";
        const blob = await resp.blob();
        const file = window.URL.createObjectURL(blob);
        waiter.hide();
        window.location.assign(file);
        return;
    }
    const html = await resp.text();
    const parser = new DOMParser();
    const doc = parser.parseFromString(html, "text/html");
    const fltTarget = form.dataset.target || '#filter_container';
    const oldBody = document.querySelector(fltTarget);
    let newBody = null;
    if (!resp.ok) {
        const msg = `Désolé, une erreur est survenue (${resp.statusText}). Veuillez réessayer plus tard.`
        newBody = parser.parseFromString(
            `<table><tbody id="filter_container"><tr><td colspan="3"><p class="alert alert-danger">${msg}</p></td></tr></tbody></table>`,
            "text/html"
        ).querySelector("tbody");
    } else {
        newBody = doc.querySelector(fltTarget);
    }
    if (newBody) oldBody.replaceWith(newBody);
    else oldBody.innerHTML = '';
    const pagin = document.querySelector('#pagination');
    if (pagin) {
        const newPagin = doc.querySelector('#pagination');
        if (newPagin) { pagin.replaceWith(newPagin); }
        else pagin.innerHTML = "";
    }
    const listCount = document.querySelector('.list-count');
    if (listCount) listCount.replaceWith(doc.querySelector('.list-count'));
    if (newBody) attachHandlers(newBody);
    form.dispatchEvent(new Event('filterapplied'));
    waiter.hide();
}

async function submitForm(ev, form) {
    if (ev.target.classList.contains('noajax')) {
        waiter.show();
        form.submit();
        return;
    }
    if (form.classList.contains('table-filter')) {
        submitFilterForm(form);
    } else if (form.classList.contains('ajax')) {
        const formData = new FormData(form);
        addCSRFToken(formData);
        const resp = await fetch(
            form.action,
            {method: 'POST', body: formData, headers: {'X-Requested-With': 'fetch'}}
        );
        const data = await resp.json();
        if (data.reload) reload(data.reload);
    } else {
        waiter.show();
        form.submit();
    }
}

async function submitButtonForm(btn) {
    /* The button might not be related to any form.
     * (typically a button embedded in an unrelated form)
     */
    const formData = new FormData();
    addCSRFToken(formData);
    const resp = await fetch(
        btn.formAction,
        {method: 'POST', body: formData, headers: {'X-Requested-With': 'fetch'}}
    );
    const data = await resp.json();
    return data;
}

function deleteFile(ev) {
    const fileDiv = ev.currentTarget.closest('div');
    const rep = confirm("Voulez-vous réellement supprimer ce fichier ?");
    if (!rep) return;
    const formData = new FormData();
    addCSRFToken(formData);
    fetch(
        fileDiv.dataset.deleteurl,
        {method: 'POST', body: formData}
    ).then(resp => resp.json()).then(data => {
        if (data.reload) reload(data.reload);
    });
}

function markFormsetForDeletion(ev) {
    const fsetLine = ev.currentTarget.closest('.fset-line');
    const rep = confirm("Voulez-vous réellement effacer cette ligne ?");
    if (!rep) return;
    fsetLine.classList.add('formset-deleted');
    fsetLine.querySelector('input[id$="-DELETE"]').value = 'on';
}

function incrementTotalForms(form, numb) {
    const formTotal = form.querySelector('[id$="-TOTAL_FORMS"]');
    const totalValue = parseInt(formTotal.value);
    formTotal.value = totalValue + numb;
    return totalValue + numb;
}

/* Dynamically add new empty form in a formset. */
function cloneMore(ev) {
    ev.preventDefault();
    const form = ev.target.closest('form');
    const emptyForm = form.querySelector('.empty_form');
    let newElement = emptyForm.cloneNode(true);
    newElement.classList.remove('empty_form');
    const newTotal = incrementTotalForms(form, 1);
    newElement.querySelectorAll('input').forEach(inp => {
        for (const attr of inp.attributes) {
            if (attr.value.includes('__prefix__')) {
               inp.setAttribute(attr.name, attr.value.replace('__prefix__', newTotal - 1));
            }
        }
    });
    const plusBtn = newElement.querySelector('button.openmodal');
    if (plusBtn) plusBtn.dataset.modalurl = plusBtn.dataset.modalurl.replace('__prefix__', newTotal - 1);
    show(newElement);
    emptyForm.parentNode.insertBefore(newElement, emptyForm);
    attachHandlers(newElement);
    return newElement;
}

const beforeUnloadListener = (event) => {
  event.preventDefault();
  return (event.returnValue = "Le formulaire a été modifié. Souhaitez-vous vraiment fermer cette fenêtre ?");
};

function monitorFormChange(ev) {
    ev.currentTarget.dataset.changed = "true";
}

function checkFormChanged(ev, parent) {
    const form = parent.querySelector('form:not(.unmonitor)');
    if (form && form.dataset.changed == "true") {
        const rep = confirm("Le formulaire a été modifié. Souhaitez-vous vraiment fermer cette fenêtre ?");
        if (!rep) {
            ev.preventDefault();
            return false;
        } else form.dataset.changed = "false";
    }
    return true;
}

async function findInstanceDups(ev) {
    console.log(ev.target.value);
    const doublDiv = document.querySelector('#doublons');
    const form = ev.target.closest("form");
    const nom = form.querySelector("#id_nom").value;
    const prenom = form.querySelector("#id_prenom");
    const dateNaiss = form.querySelector("#id_date_naissance");
    const npaLoc = form.querySelector("#id_npalocalite");
    let params = new URLSearchParams();
    if (prenom && prenom.value && dateNaiss && dateNaiss.value && npaLoc && npaLoc.value) {
        params.append("prenom", prenom.value);
        params.append("dateNaiss", dateNaiss.value);
        params.append("npaLoc", npaLoc.value);
    } else if (nom.length >= 2) {
        params.append("q", nom);
    } else {
        hide(doublDiv);
        return;
    }
    const resp = await fetch(`${doublDiv.dataset.url}?${params}`);
    if (!resp.ok) {
        console.error(resp.statusText);
        return;
    }
    const data = await resp.json();
    if (data.length) {
        const content = data.map(line => {
            const arch = line.archive ? " <i>(archivé-e)</i>" : "";
            return `<li><a href="${line.url}">${line.label}</a>, ${line.adresse}${arch}</li>`;
        }).join("\n");
        doublDiv.querySelector('#doublons_content').innerHTML = content;
        show(doublDiv);
    } else hide(doublDiv);
}

async function alarmSelected(ev) {
    const alarmeInp = document.getElementById('id_alarme');
    if (!alarmeInp.dataset.jsonurl) return;
    if (document.querySelector('#id_simspan')) document.querySelector('#id_simspan').remove();
    const alarmepk = ev.detail ? ev.detail.value : alarmeInp.value;
    if (!alarmepk) return;
    const resp = await fetch(`${alarmeInp.dataset.jsonurl}?pk=${alarmepk}`);
    if (!resp.ok) {
        console.error(`Unable to get alarme details: ${response.statusText}`);
        return;
    }
    const json = await resp.json();
    const SIMspan = ce(`<div id="id_simspan"><b>Carte SIM:</b> ${json[0].fields.carte_sim}</div>`);
    ev.target.parentNode.appendChild(SIMspan);
}

function professionnelSelected(ev) {
    const profInp = document.getElementById('id_professionnel');
    const remDiv = document.getElementById('remarque_professionnel');
    if (remDiv) {
        remDiv.textContent = JSON.parse(ev.detail.details).remarque;
    }
}

function contactExistantSelected(ev) {
    // Recharger le formulaire avec données initiales = contact
    const modal = bootstrap.Modal.getInstance(document.querySelector('#siteModal'));
    document.querySelector('.ReferentForm').dataset.changed = "false";
    modal.hide()
    const btn = document.querySelector('#new_contact_button');
    const oldurl = btn.dataset.modalurl;
    btn.dataset.modalurl += `?copy_from=${ev.detail.value}`;
    btn.click();
    btn.dataset.modalurl = oldurl;
}

function referentFormChanged(ev) {
    // Afficher/masquer liens PDF courriers en fonction des cases sélectionnées
    document.querySelectorAll("a.link_courrier").forEach((lk) => {
        const inps = document.querySelectorAll(`input[name=${lk.dataset.input}]`);
        if (Array.from(inps).some(inp => inp.checked)) lk.removeAttribute('hidden');
        else lk.setAttribute('hidden', true);
    })
}

function langueSelected(ev) {
    /* When a new language is selected, duplicate the last checkbox and replace
     * its values with the new language values.
     */
    const lgInp = document.getElementById('id_langues');
    const numChildren = lgInp
    const newDiv = lgInp.querySelector('div:last-child').cloneNode(true);
    const label = newDiv.querySelector('label');
    const input = newDiv.querySelector('input');
    input.value = ev.detail.value;
    input.id = input.id.replace(/_\d/, `_${lgInp.children.length}`);
    input.setAttribute('checked', true);
    label.replaceChild(ce(ev.detail.label), label.childNodes[1]);
    label.setAttribute('for', label.getAttribute('for').replace(/_\d/, `_${lgInp.children.length}`));
    lgInp.append(newDiv);
    document.getElementById('id_langues_select').value = '';
}

function articleFactureSelected(ev) {
    // Placer le descriptif de l'article sélectionné dans le libellé.
    const select = ev.currentTarget;
    let txt = select.options[select.selectedIndex].text;
    txt = txt.replace(/ *\([^)]*\)/g, "");  // Enlever no article
    const libelle = select.closest('form').querySelector('#id_libelle');
    if (libelle) libelle.value = txt;
}

function calculerAge(ev) {
    let ageInput;
    if (ev === undefined) {
        ageInput = document.querySelector('#id_date_naissance');
        if (!ageInput) return;
    } else {
        ageInput = ev.target;
    }
    if (!ageInput.value) return;
    const diff = new Date().getTime() - new Date(ageInput.value).getTime();
    const age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
    if (age > 150) return;
    let span = ageInput.parentNode.querySelector('span.age');
    if (!span) {
        span = document.createElement("span");
        span.classList.add('age');
        ageInput.parentNode.append(span);
    }
    span.textContent = `${age} ans`
}

function avoidDoubleSubmit(form) {
    // Avoid double form submission by disabling the submit button during 5 secs.
    const submitBtn = Array.from(form.elements).filter(
        el => el.nodeName === "BUTTON" && el.getAttribute("type") == 'submit'
    ).shift();
    if (submitBtn) {
        submitBtn.setAttribute('disabled', true);
        setTimeout(() => submitBtn.removeAttribute('disabled'), 5000);
    }
}

function feedbackLongOperation(btn) {
    // Add waiting logo inside the button
    const svgEl = document.querySelector('#loader-1');
    const svgNew = svgEl.cloneNode(true);
    btn.disabled = true;
    btn.appendChild(document.createElement('br'));
    btn.appendChild(svgNew);
    return true;
}

/* Ask server for *real* distance/duration through an API */
async function calculateDistance(ev) {
    const response = await fetch(ev.target.dataset.url);
    const data = await response.json();
    const dist = Math.round(data.distance / 100) / 10;
    const time = Math.round(data.duration / 60);
    ev.target.replaceWith(` / ${dist} km (${time} min.)`);
}

/*
 * Avoid a hidden pk input keeping a selected value when the user types anything
 * else in the field after the selection.
 */
function clearPKfield(ev) {
    const pkInput = document.querySelector(`input[name=${ev.target.dataset.pkfield}]`);
    pkInput.value = '';
}

function attachHandlerSelector(section, selector, event, func) {
    section.querySelectorAll(selector).forEach(item => {
        item.addEventListener(event, func);
    });
}

function attachHandlers(section) {
    attachHandlerSelector(section, 'button[data-confirm], input[data-confirm]', 'click', (ev) => {
        let resp = confirm(ev.currentTarget.dataset.confirm);
        if (!resp) { ev.preventDefault(); ev.stopImmediatePropagation(); }
    });
    attachHandlerSelector(section, 'form.debounce', 'submit', (ev) => avoidDoubleSubmit(ev.target));
    attachHandlerSelector(section, '.openmodal', 'click', openModal);
    attachHandlerSelector(section, '.goback', 'click', (ev) => window.history.back());
    attachHandlerSelector(
        section,
        'form.immediate_submit select, select.immediate_submit, input:not([type=text]).immediate_submit, form.immediate_submit input[type=date], form.immediate_submit input[type=checkbox]',
        'change', (ev) => {
            submitForm(ev, ev.target.form);
        }
    );
    attachHandlerSelector(
        section,
        'form.immediate_submit input[type=text], input[type=text].immediate_submit',
        'keyup',
        delay(submitAfterDelay, 500)
    );
    attachHandlerSelector(section, 'input[type=checkbox][data-visibilitytarget]', 'change', (ev) => {
        // For checkboxes able to show/hide some target section of a page
        const target = document.getElementById(ev.target.dataset.visibilitytarget);
        if (ev.target.checked) show(target);
        else hide(target);
    });
    attachHandlerSelector(section, 'input[data-pkfield]', 'input', clearPKfield);
    attachHandlerSelector(section, 'form.ajax', 'submit', (ev) => {
        ev.preventDefault();
        avoidDoubleSubmit(ev.target);
        submitForm(ev, ev.target);
    });
    attachHandlerSelector(section, 'form.noenter', 'keydown', (ev) => {
        // Avoid Enter submitting the form
        if (ev.keyCode==13 && document.activeElement.tagName != "TEXTAREA") {ev.preventDefault(); return false;}
    });
    if (section.querySelector('#doublons')) {
        attachHandlerSelector(section, '#id_nom', 'keydown', delay(findInstanceDups, 700));
        attachHandlerSelector(section, '#id_npalocalite', 'change', findInstanceDups);
    }
    attachHandlerSelector(section, 'span.delete-link', 'click', deleteFile);
    attachHandlerSelector(section, '.delete-formset-link', 'click', markFormsetForDeletion);
    attachHandlerSelector(section, '.add_more', 'click', cloneMore);
    attachHandlerSelector(section, '.calc-dist-icon', 'click', calculateDistance);
    attachHandlerSelector(section, '#id_alarme_select', 'itemselected', alarmSelected);
    attachHandlerSelector(section, '#id_alarme', 'change', alarmSelected);
    attachHandlerSelector(section, '#id_professionnel', 'itemselected', professionnelSelected);
    attachHandlerSelector(section, '#id_contact-contact_existant', 'itemselected', contactExistantSelected);
    attachHandlerSelector(section, '.ReferentForm', 'change', referentFormChanged);
    attachHandlerSelector(section, '#id_langues_select', 'itemselected', langueSelected);
    attachHandlerSelector(section, '#id_article', 'change', articleFactureSelected);
    attachHandlerSelector(section, '#id_date_naissance', 'change', calculerAge);
    calculerAge();
    referentFormChanged();
    // Enable drag and drop for repondant/referent contact lists.
    section.querySelectorAll('.contact-list-orderable').forEach(list => attachDragDropHandlers(list, 'list-group-item'));
    section.querySelectorAll('.div-sortable').forEach(body => attachDragDropHandlers(body, 'row'));
    attachHandlerSelector(section, '.parent-draggable', 'mousedown', (e) => e.target.parentNode.setAttribute('draggable', 'true'));
    attachHandlerSelector(section, '.parent-draggable', 'mouseup', (e) => e.target.parentNode.removeAttribute('draggable'));

    setupAutocomplete(section);
    // Initialize tooltips
    const tooltipTriggerList = [].slice.call(section.querySelectorAll('[data-bs-toggle="tooltip"]'))
    tooltipTriggerList.map((tooltipEl) => { return new bootstrap.Tooltip(tooltipEl) })
}

async function loadURLInTab(tab) {
    // Dynamically load tab content from its href attribute.
    const resp = await fetch(tab.href, {headers: {'X-Requested-With': 'fetch'}});
    const html = await resp.text();
    const tabTarget = document.querySelector(tab.dataset.bsTarget);
    tabTarget.innerHTML = html;
    attachHandlers(tabTarget);
    return tab;
}

function loadURLInSection(section) {
    // Dynamically load collapsed section content from its href attribute.
    return fetch(section.dataset.url, {headers: {'X-Requested-With': 'fetch'}})
    .then(resp => resp.text()).then(html => {
        section.innerHTML = html;
        attachHandlers(section);
        return section;
    });
}

var dragSrcEl = null;
function handleDragStart(ev) {
    dragSrcEl = this;
    ev.dataTransfer.effectAllowed = 'move';
    ev.dataTransfer.setData('text/html', this.outerHTML);
}
function handleDragOver(ev) {
    if (this.parentNode == dragSrcEl.parentNode) ev.preventDefault();
}
function handleDragEnter(ev) {
    ev.preventDefault();
}
async function handleDrop(ev, childrenClass) {
    /* Optionnally, a data-confirm attribute on the parent node contains a confirm message.
     * The data-reorder attribute on parent node contains a URL to send the new pk list to.
     * A .counter node in each sortable children is used to number the elements.
     * Children elements are supposed to have the `childrenClass` class.
     */
    ev.stopPropagation();
    ev.preventDefault();
    const dropEl = ev.currentTarget;
    if (dragSrcEl === dropEl) return;

    const parent = dragSrcEl.parentNode;
    const content = ev.dataTransfer.getData('text/html');
    if (parent.dataset.confirm) {
        let resp = confirm(parent.dataset.confirm);
        if (!resp) return;
    }
    // Exchange positions by pushing lower lines
    const children = Array.from(parent.querySelectorAll(`.${childrenClass}`));
    const contactPks = children.map(child => child.dataset.contact);
    console.debug(`before: ${contactPks}`);
    let currentpos = 0, droppedpos = 0;
    for (let it=0; it<children.length; it++) {
      if (dragSrcEl == children[it]) { currentpos = it; }
      if (dropEl == children[it]) { droppedpos = it; }
    }
    const moved = contactPks.splice(currentpos, 1);
    contactPks.splice(droppedpos, 0, moved);
    console.debug(`after: ${contactPks}`);

    function moveElements() {
        if (currentpos < droppedpos) {
          parent.insertBefore(dragSrcEl, dropEl.nextSibling);
        } else {
          parent.insertBefore(dragSrcEl, dropEl);
        }
    }

    function renumber() {
        // Update displayed numbering, or ORDER field numbering
        Array.from(parent.querySelectorAll(`.${childrenClass}`)).forEach((el, idx) => {
            const counter = el.querySelector('.counter');
            if (counter) counter.textContent = idx + 1;
            const order = el.querySelector('[name$=ORDER]');
            if (order) order.value = idx + 1;
        });
    }

    const orderURL = parent.dataset.reorder;
    if (orderURL) {
        let formData = new FormData();
        formData.append('pks', contactPks);
        addCSRFToken(formData);
        try {
            const response = await fetch(orderURL, {method: 'POST', body: formData});
            if (response.ok) {
                moveElements();
                renumber();
            } else alert("Désolé, le changement d’ordre a échoué sur le serveur");
        } catch (err) {
            alert("Désolé, le changement d’ordre a échoué sur le serveur");
        }
    } else {
        moveElements();
        renumber();
    }
}
function attachDragDropHandlers(list, childrenClass) {
    list.querySelectorAll(`.${childrenClass}`).forEach(item => {
        item.addEventListener('dragstart', handleDragStart);
        item.addEventListener('dragenter', handleDragEnter);
        item.addEventListener('dragover', handleDragOver);
        item.addEventListener('drop', (ev) => handleDrop(ev, childrenClass));
    });
}

/* event handler for when a drag started and mouse moves. */
var selected = null;
function mouseMoved (ev) {
    const xOffset = selected.mouseX - ev.pageX;
    const yOffset = selected.mouseY - ev.pageY;
    if (selected !== null) {
        selected.elem.style.left = (selected.x - xOffset) + 'px';
        selected.elem.style.top = (selected.y - yOffset) + 'px';
    }
}

window.addEventListener('DOMContentLoaded', () => {
    waiter.init();
    // Attach events for main modal.
    document.getElementById('modal-delete').addEventListener('click', modalSubmit);
    attachHandlers(document);
    // Client list search input
    const searchInput = document.querySelector('.selection_form #id_client');
    if (searchInput) {
        searchInput.addEventListener('keyup', delay(submitAfterDelay, 500));
        const len = searchInput.value.length;
        searchInput.setSelectionRange(len, len);
    }
    attachHandlerSelector(document, 'button.long-op', 'click', (ev) => {
        ev.preventDefault();
        const btn = ev.target;
        feedbackLongOperation(btn);
        btn.form.requestSubmit();
    });
    attachHandlerSelector(document, 'button.geoloc-retry', 'click', async (ev) => {
        ev.preventDefault();
        const btn = ev.target;
        const data = await submitButtonForm(btn);
        const alertDiv = btn.closest('div');
        if (data.result == "OK") {
            alertDiv.textContent = data.message;
            alertDiv.classList.remove('alert-danger');
            alertDiv.classList.add('alert-success');
        } else {
            alertDiv.textContent = data.message;
        }
    });

    // Make boostrap modals draggable
    document.querySelector('.modal-header').addEventListener('mousedown', (ev) => {
        selected = {
            elem: ev.target.closest('.modal-dialog'),
            mouseX: ev.pageX,
            mouseY: ev.pageY
        };
        selected.x = selected.elem.style.left.replace('px', '') || 0;
        selected.y = selected.elem.style.top.replace('px', '') || 0;
        document.addEventListener('mouseup', (ev) => {
            selected = null;
            document.removeEventListener('mousemove', mouseMoved);
        }, {once: true});
        document.addEventListener('mousemove', mouseMoved);
        return false;
    });
    // Monitor forms for unsaved data
    if (!window.location.toString().includes('/login/')) {
        document.querySelectorAll('form:not(.unmonitor)').forEach(form => {
            form.addEventListener('change', (ev) => addEventListener('pagehide', beforeUnloadListener));
            form.addEventListener('submit', (ev) => removeEventListener('pagehide', beforeUnloadListener));
        });
    }
    // Avoid closing modal with changed form without notice.
    document.querySelector('#siteModal').addEventListener('hide.bs.modal', (ev) => checkFormChanged(ev, ev.currentTarget));

    // Show specific tab on home page if a location hash is present
    if (["/", "/app/"].includes(window.location.pathname) && window.location.hash) {
        const triggerEl = document.querySelector(`.nav-tabs .nav-link[data-bs-target="${window.location.hash}"]`);
        if (triggerEl) {
            bootstrap.Tab.getOrCreateInstance(triggerEl).show();
        }
    }
    attachHandlerSelector(document, '#appTabs .nav-link', 'shown.bs.tab', ev => {
        const target = ev.target.dataset.bsTarget.replace('#', '');
        history.pushState({tab: target}, "", `#${target}`);
    });

    attachHandlerSelector(document, '.dynamic-content', 'shown.bs.collapse', (ev) => {
        if (ev.target == ev.currentTarget) loadURLInSection(ev.target);
    });

    if (typeof autosize === "function") autosize(document.querySelectorAll('textarea'));
    // Alarme-specific
    attachHandlerSelector(document, '#home-tabs button[data-bs-toggle="tab"]', 'shown.bs.tab', ev => {
        const target = ev.target.dataset.bsTarget.replace('#', '');
        history.pushState({tab: target}, "", `/tab-${target}`);
    });
    attachHandlerSelector(document, '#home-tabs a[data-bs-toggle="tab"]', 'shown.bs.tab', ev => {
        loadURLInTab(ev.target);
    });
    const signCanvas = document.querySelectorAll('.signature_canvas');
    if (signCanvas.length) {
        const pads = [];
        signCanvas.forEach((canv) => {
            let pad = new SignaturePad(canv);
            pads.push(pad);
            canv.parentNode.querySelector('.clear').addEventListener('click', () => {
                pad.clear();
            });
        });
        document.querySelector('button#save').addEventListener('click', (ev) => {
            ev.preventDefault();
            let dataFilled = true;
            pads.forEach((pad) => {
                if (pad.isEmpty()) {
                    dataFilled = false;
                    return alert("Une signature est manquante.");
                }
                document.getElementById(pad.canvas.dataset.input).value = pad.toDataURL('image/png');
            });
            if (dataFilled) ev.target.form.submit();
        });
    }
});
