"""
WSGI config for alarmes project.
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.alarme')

application = get_wsgi_application()
