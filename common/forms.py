from datetime import date, datetime, timedelta

from django import forms
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.postgres.forms import (
    BaseRangeField, DateRangeField as DjangoDateRangeField, RangeWidget
)
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.db.backends.postgresql.psycopg_any import DateTimeRange
from django.db.models import Q
from django.forms.utils import ErrorList as DjangoErrorList
from django.urls import reverse_lazy
from django.utils.dates import MONTHS
from django.utils.functional import cached_property
from django.utils.timezone import get_current_timezone, localtime

from common.choices import Languages
from common.distance import get_point_from_address
from common.models import Fichier, Utilisateur
from common.utils import RegionFinder, format_HM
from common.templatetags.common_utils import format_duree


class DateInput(forms.DateInput):
    input_type = 'date'

    def format_value(self, value):
        return str(value) if value is not None else None


class TimeInput(forms.TimeInput):
    input_type = 'time'

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('format', '%H:%M')
        super().__init__(*args, **kwargs)

    def format_value(self, value):
        if isinstance(value, datetime):
            value = localtime(value)
        return super().format_value(value)


class EqualValuesRangeFieldMixin:
    # https://forum.djangoproject.com/t/baserangefield-and-equal-lower-upper-values/35160
    def compress(self, values):
        if not values:
            return None
        lower, upper = values
        if lower is not None and upper is not None and lower == upper:
            raise forms.ValidationError(
                "La valeur supérieure doit être plus grande que la valeur inférieure",
                code="bound_ordering",
            )
        return super().compress(values)


class DateRangeWidget(RangeWidget):
    template_name = "widgets/daterange.html"

    def __init__(self, attrs=None):
        widgets = (DateInput, DateInput)
        super(RangeWidget, self).__init__(widgets, attrs)


class DateTimeRangeWidget(RangeWidget):
    template_name = "widgets/datetimerange.html"

    def __init__(self, attrs=None):
        widgets = (DateInput, TimeInput, DateInput, TimeInput)
        super(RangeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            low, up = localtime(value.lower), localtime(value.upper) if value.upper else None
            return low.date(), low.time(), up.date() if up else None, up.time() if up else None
        return (None, None, None, None)


class DateRangeField(EqualValuesRangeFieldMixin, DjangoDateRangeField):
    widget = DateRangeWidget

    def __init__(self, **kwargs):
        # Hack due to some possible bug in Django
        kwargs["widget"] = self.widget
        super().__init__(**kwargs)


class DateTimeRangeField(EqualValuesRangeFieldMixin, BaseRangeField):
    widget = DateTimeRangeWidget
    default_error_messages = {
        "invalid_date": "Saisissez une date valable.",
        "invalid_time": "Saisissez une heure valable.",
    }

    def __init__(self, *, input_date_formats=None, input_time_formats=None, **kwargs):
        errors = self.default_error_messages.copy()
        fields = (
            forms.DateField(
                input_formats=input_date_formats,
                error_messages={"invalid": errors["invalid_date"]},
            ),
            forms.TimeField(
                input_formats=input_time_formats,
                error_messages={"invalid": errors["invalid_time"]},
            ),
            forms.DateField(
                input_formats=input_date_formats,
                error_messages={"invalid": errors["invalid_date"]},
            ),
            forms.TimeField(
                input_formats=input_time_formats,
                error_messages={"invalid": errors["invalid_time"]},
            ),
        )
        super().__init__(fields=fields, widget=self.widget, **kwargs)

    def prepare_value(self, value):
        return value

    def compress(self, data_list):
        return DateTimeRange(
            datetime.combine(data_list[0], data_list[1], tzinfo=get_current_timezone()),
            datetime.combine(data_list[2], data_list[3], tzinfo=get_current_timezone()),
        )


class SplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, attrs=None):
        widgets = [DateInput(attrs={'class': 'inline'}), TimeInput(attrs={'class': 'inline'})]
        forms.MultiWidget.__init__(self, widgets, attrs)


class DurationInput(TimeInput):
    input_type = 'text'

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attrs', {'placeholder': 'HH:MM', 'class': 'duration'})
        super().__init__(*args, **kwargs)


class PriceInput(forms.TextInput):
    def __init__(self, attrs=None):
        super().__init__(attrs=attrs)
        self.attrs['pattern'] = r"[0-9]+[\.,]?([0-9][0,5])?"


class PermissiveHourValue:
    @classmethod
    def _convert_value(cls, value, err_msg):
        if len(value) == 4 and value.isdigit():
            value = f'{value[:2]}:{value[2:4]}'
        elif 'h' in value:
            value = value.replace('h', ':')
        elif '.' in value:
            value = value.replace('.', ':')
        if ':' not in value:
            raise forms.ValidationError(err_msg)
        if value.endswith(':'):
            value += '0'
        return value


class HMDurationField(PermissiveHourValue, forms.DurationField):
    """A duration field taking HH:MM as input."""
    widget = DurationInput

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        value = self._convert_value(value, "Indiquez une durée de type hh:mm")
        value += ':00'  # Simulate seconds
        return super().to_python(value)

    def prepare_value(self, value):
        if isinstance(value, timedelta):
            value = format_HM(value)
        return value


class MinutesDurationField(forms.DurationField):
    """A duration field taking minutes as input."""
    widget = forms.NumberInput

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        return timedelta(minutes=int(value))

    def prepare_value(self, value):
        if isinstance(value, timedelta):
            value = value.seconds // 60
        return value


class RegionField(forms.ChoiceField):
    def __init__(self, *, choices=(), empty_choice_label='--région--', include_autre=True, **kwargs):
        super().__init__(**kwargs)
        choices = [('', empty_choice_label)] + [
            (reg, reg) for reg in RegionFinder.regions().keys() if not reg.startswith('__')
        ] + ([('Autre', 'Autre')] if include_autre else [])
        self.choices = choices


class NPALocaliteField(forms.CharField):
    widget = forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'placeholder': 'N° postal ou nom de localité',
        'data-searchurl': reverse_lazy('npalocalite-search'),
    })

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.label:
            self.label = "NPA Localité"

    def clean(self, value):
        return str(value)


class NPALocaliteMixin(forms.Form):
    """
    Applied to forms having a NPALocaliteField, to split npa/localite in
    two.
    """
    npalocalite = NPALocaliteField(required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if 'npa' in self.fields:
            self.fields['npa'].widget = forms.HiddenInput()
            self.fields['npa'].required = False
        if 'localite' in self.fields:
            self.fields['localite'].widget = forms.HiddenInput()
            self.fields['localite'].required = False

    def get_initial_for_field(self, field, field_name):
        if field_name == 'npalocalite':
            value = ' '.join(
                [v for v in [self.initial.get('npa', ''), self.initial.get('localite', '')] if v]
            ).strip()
            field.choices = ((value, value),)
            return value
        return super().get_initial_for_field(field, field_name)

    @cached_property
    def changed_data(self):
        changed = [name for name, bf in self._bound_items() if bf._has_changed()]
        if 'npalocalite' in changed:
            changed.remove('npalocalite')
            if self.initial.get('npa', '') != self.cleaned_data['npa']:
                changed.append('npa')
            if self.initial.get('localite', '') != self.cleaned_data['localite']:
                changed.append('localite')
        return changed

    def _clean_form(self):
        if self.cleaned_data.get('npalocalite'):
            if self.cleaned_data.get('pays') == 'GB':
                self.cleaned_data['npa'] = ''
                self.cleaned_data['localite'] = self.cleaned_data['npalocalite']
            else:
                try:
                    npa, loc = self.cleaned_data['npalocalite'].split(' ', maxsplit=1)
                except ValueError:
                    self.add_error('npalocalite', "Vous devez saisir numéro postal et localité")
                else:
                    if not npa.isdigit():
                        self.add_error('npalocalite', "Le numéro postal doit être uniquement composé de chiffres.")
                    self.cleaned_data['npa'] = npa
                    self.cleaned_data['localite'] = loc.strip()
        super()._clean_form()


class GeoAddressFormMixin:
    """
    Classe mixin recherchant la géolocalisation à partir des champs rue/npa/localite saisis.
    Si geo_required=True et que la géolocalisation ne peut pas se faire, le formulaire
    ne sera pas valide.
    """
    geo_required = True

    def geo_enabled(self, instance):
        return settings.QUERY_GEOADMIN_FOR_ADDRESS

    def clean(self):
        cleaned_data = super().clean()
        if set(self.changed_data).intersection({'npa', 'localite', 'rue'}) and self.geo_enabled(self.instance):
            lon_lat = get_point_from_address(
                cleaned_data.get('rue'), f"{cleaned_data.get('npa')} {cleaned_data.get('localite')}"
            )
            if lon_lat:
                self.instance.empl_geo = list(lon_lat)
            elif self.geo_required:
                raise forms.ValidationError("Impossible de géolocaliser cette adresse.")
        return cleaned_data


class ErrorList(DjangoErrorList):
    def __init__(self, *args, error_class=None, **kwargs):
        super().__init__(*args, error_class="formfield-error", **kwargs)


def form_field(field, **kwargs):
    if field.__class__.__name__ == 'DateField' and 'widget' not in kwargs:
        kwargs['widget'] = DateInput
    return field.formfield(**kwargs)


class HiddenDeleteInlineFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput


class FormsetMixin:
    def formsets(self):
        fset = getattr(self, 'formset', None)
        return [fset] if fset else []

    def is_valid(self):
        return all([super().is_valid()] + [fset.is_valid() for fset in self.formsets()])

    @property
    def errors(self):
        errors = super().errors
        for fset in self.formsets():
            if not fset.is_valid():
                for subf in fset.forms:
                    if not subf.is_valid():
                        errors.update(subf.errors)
        return errors

    def has_changed_data(self):
        # A method from ModelForm
        return super().has_changed_data() or (
            any([any(f.changed_data for f in fset) for fset in self.formsets()])
        )

    def _get_changes(self, exclude=(), details=True):
        changes = super()._get_changes(exclude=exclude, details=details)
        for formset in self.formsets():
            for subform in formset.forms:
                if subform in formset.deleted_forms:
                    if details:
                        changes.append(f"suppression de {subform._change_prefix()} ({subform.instance})")
                    else:
                        changes.append(f'suppression de {subform._change_prefix()}')
                elif subform.has_changed():
                    if details:
                        changes.append(f'{subform._change_prefix()}: {", ".join(subform._get_changes())}')
                    else:
                        changes.append([
                        subform._changed_value(fname) for fname in subform.changed_data
                    ])
        return changes

    def save(self, **kwargs):
        parent = super().save(**kwargs)
        for fset in self.formsets():
            if not fset.instance.pk:
                fset.instance = parent
            fset.save(**kwargs)
        return parent


class ReadOnlyableMixin:
    def __init__(self, *args, readonly=False, **kwargs):
        self.readonly = readonly
        super().__init__(*args, **kwargs)
        if self.readonly:
            for field in self.fields.values():
                field.disabled = True


class BootstrapMixin:
    widget_classes = {
        'checkbox': 'form-check-input',
        'select': 'form-select',
    }
    required_css_class = 'required'

    class Meta:
        formfield_callback = form_field

    def __init__(self, *args, **kwargs):
        kwargs.update({'error_class': ErrorList})
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if getattr(field.widget, '_bs_enabled', False):
                continue
            widgets = getattr(field.widget, 'widgets', [field.widget])
            for widget in widgets:
                input_type = getattr(widget, 'input_type', '')
                class_name = self.widget_classes.get(input_type, 'form-control')
                if 'class' in widget.attrs:
                    if class_name not in widget.attrs['class']:
                        widget.attrs['class'] += ' ' + class_name
                else:
                    widget.attrs.update({'class': class_name})


class BSChoicesMixin:
    """
    Custom widget to set 'form-check' on container and 'form-check-input' on sub-options.
    """
    _bs_enabled = True

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['widget']['attrs']['class'] = 'form-check'
        return context

    def create_option(self, *args, attrs=None, **kwargs):
        attrs = attrs.copy() if attrs else {}
        if 'class' in attrs:
            attrs['class'] += ' form-check-input'
        else:
            attrs.update({'class': 'form-check-input'})
        return super().create_option(*args, attrs=attrs, **kwargs)


class BSCheckboxSelectMultiple(BSChoicesMixin, forms.CheckboxSelectMultiple):
    pass


class BSRadioSelect(BSChoicesMixin, forms.RadioSelect):
    pass


class SelectedCheckboxWidget(BSCheckboxSelectMultiple):
    def optgroups(self, name, value, attrs=None):
        # Only output checkboxes for selected values
        grps = super().optgroups(name, value, attrs=attrs)
        return [grp for grp in grps if grp[1][0]['selected'] is True or grp[1][0]['value'] == 'fr']


class LanguageMultipleChoiceField(forms.MultipleChoiceField):
    widget = SelectedCheckboxWidget

    def __init__(self, *, choices=(), **kwargs):
        super().__init__(choices=Languages.choices(), **kwargs)


class GroupSelectMultiple(BSCheckboxSelectMultiple):
    option_template_name = "widgets/group_checkbox_option.html"

    def create_option(self, name, value, *args, **kwargs):
        try:
            help_ = value.instance.groupinfo.description
        except ObjectDoesNotExist:
            help_= ''
        return {
            **super().create_option(name, value, *args, **kwargs),
            'help': help_,
        }


class AnneeForm(BootstrapMixin, forms.Form):
    year = forms.ChoiceField(choices=())

    def __init__(self, year_start=2022, **kwargs):
        super().__init__(**kwargs)
        self.fields['year'].choices = tuple(
            (str(y), str(y))
            for y in range(year_start, date.today().year + (1 if date.today().month < 12 else 2))
        )


class MoisForm(BootstrapMixin, forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2022, date.today().year + (1 if date.today().month < 12 else 2))
    )
    year = forms.ChoiceField(choices=YEAR_CHOICES)
    month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])


class ModelForm(forms.ModelForm):
    """
    Extension de forms.ModelForm avec la capacité de produire un résumé des
    valeurs modifiées lors de l'enregistrement du formulaire.
    """
    @staticmethod
    def to_readable(val, choices=None):
        if val is True:
            return 'vrai'
        elif val is False:
            return 'faux'
        elif val is None:
            return ''
        elif isinstance(val, timedelta):
            return format_duree(val)
        elif choices and val:
            return ", ".join([choices[ch] for ch in val])
        return val

    def _change_prefix(self):
        """Dans un contexte Formset, préfixe identifiant l'instance liée."""
        return self._meta.model._meta.verbose_name.lower()

    def _changed_value(self, fname, detail=False):
        if not self.fields[fname].label:
            return None
        if detail:
            try:
                model_field = self.instance._meta.get_field(fname)
            except FieldDoesNotExist:
                model_field = None
            try:
                choices = dict(model_field.base_field.choices)
            except (AttributeError, KeyError, TypeError):
                choices = None
            initial_value = self.initial.get(fname, '')
            if (
                model_field and model_field.get_internal_type() == 'ForeignKey' and
                isinstance(initial_value, int)
            ):
                prev_value = str(model_field.related_model.objects.get(pk=initial_value))
            else:
                prev_value = self.to_readable(initial_value, choices)
            new_value = self.to_readable(self.cleaned_data[fname], choices)
            if new_value == prev_value:
                return None
            if prev_value:
                return "{} (de «{}» à «{}»)".format(
                    self.fields[fname].label.lower(),
                    prev_value, new_value
                )
            elif fname != "ORDER":
                return "ajout de {} («{}»)".format(
                    self.fields[fname].label.lower(), new_value
                )
        else:
            return self.fields[fname].label.lower()

    def _get_changes(self, exclude=(), details=True):
        if details:
            # Détail des valeurs modifiées (ancienne et nouvelle valeur)
            return [val for val in [
                self._changed_value(fname, detail=True) for fname in self.changed_data if fname not in exclude
            ] if val is not None]
        else:
            # Uniquement nom des champs modifiés
            return [self._changed_value(fname) for fname in self.changed_data if fname not in exclude]

    def has_changed_data(self):
        return bool(self.changed_data)

    def get_changed_string(self, changed_values=True):
        if not self.has_changed_data():
            return ''
        changes = self._get_changes(details=changed_values)
        return ", ".join(c for c in changes if c is not None)


class UtilisateurFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}),
        required=False
    )
    groupe = forms.ModelChoiceField(queryset=Group.objects.all().order_by('name'), required=False)

    def filter(self, utils):
        if self.cleaned_data['nom']:
            term = self.cleaned_data['nom'].split()[0]
            utils = utils.filter(
                Q(last_name__unaccent__icontains=term) | Q(first_name__unaccent__icontains=term)
            )
        if self.cleaned_data['groupe']:
            utils = utils.filter(groups=self.cleaned_data['groupe'])
        return utils


class UtilisateurForm(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Utilisateur
        fields = ['last_name', 'first_name', 'email', 'tel', 'fonction', 'groups', 'is_active']
        widgets = {
            'groups': GroupSelectMultiple,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = self.fields['groups'].queryset.order_by('name')


class FichierForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Fichier
        fields = ['titre', 'typ', 'fichier']

    def __init__(self, app=None, **kwargs):
        super().__init__(**kwargs)
        # Seule l'alarme a tous les choix de type de doc.
        if app != 'alarme':
            self.fields['typ'].choices = self.fields['typ'].choices[:1]
