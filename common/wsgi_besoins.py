"""
WSGI config for besoins project.
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.besoins')

application = get_wsgi_application()
