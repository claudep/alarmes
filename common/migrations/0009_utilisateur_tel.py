import common.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0008_groupinfo'),
    ]

    operations = [
        migrations.AddField(
            model_name='utilisateur',
            name='tel',
            field=common.fields.PhoneNumberField(blank=True, max_length=18, verbose_name='Téléphone'),
        ),
    ]
