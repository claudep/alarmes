from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_fichier'),
    ]

    operations = [
        migrations.RunSQL(
"""CREATE FUNCTION pt_distance(arr1 double precision[], arr2 double precision[])
   RETURNS double precision AS $$
   SELECT SQRT(POW(69.1 * (arr1[2]::float -  arr2[2]::float), 2) + 
    POW(69.1 * (arr2[1]::float - arr1[1]::float) * COS(arr1[2]::float / 57.3), 2)
) * 1.609344;
   $$ LANGUAGE SQL IMMUTABLE;"""),
    ]
