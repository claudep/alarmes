from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_unaccent_extension'),
    ]

    operations = [
        migrations.AddField(
            model_name='utilisateur',
            name='fonction',
            field=models.CharField(blank=True, max_length=100, verbose_name='Fonction'),
        ),
    ]
