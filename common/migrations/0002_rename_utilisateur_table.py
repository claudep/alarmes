from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("common", "0001_initial"),
        ("auth", "__first__"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="utilisateur",
            options={"verbose_name": "user", "verbose_name_plural": "users"},
        ),
        migrations.AlterModelTable(
            name="utilisateur",
            table=None,
        ),
    ]
