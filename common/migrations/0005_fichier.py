import common.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '__first__'),
        ('common', '0004_utilisateur_fonction'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fichier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('titre', models.CharField(max_length=150, verbose_name='Titre')),
                ('fichier', models.FileField(upload_to=common.models.content_type_path, verbose_name='Fichier')),
                ('typ', models.CharField(
                    choices=[('doc', 'Document général'), ('quest', 'Questionnaire signé'), ('contrat', 'Contrat signé')],
                    default='doc', max_length=10, verbose_name='Type de document'
                )),
                ('quand', models.DateTimeField()),
                ('content_type', models.ForeignKey(on_delete=models.deletion.CASCADE, to='contenttypes.contenttype')),
                ('qui', models.ForeignKey(
                    blank=True, null=True, on_delete=models.deletion.SET_NULL, related_name='+',
                    to=settings.AUTH_USER_MODEL
                )),
            ],
        ),
    ]
