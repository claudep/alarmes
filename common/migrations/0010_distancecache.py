from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0009_utilisateur_tel'),
    ]

    operations = [
        migrations.CreateModel(
            name='DistanceCache',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('cle', models.CharField(max_length=150)),
                ('dist', models.DecimalField(decimal_places=1, max_digits=4)),
                ('temps', models.DurationField()),
            ],
            options={
                'indexes': [models.Index(models.F('cle'), name='index_key')],
            },
        ),
    ]
