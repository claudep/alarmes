from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_function_pt_distance'),
    ]

    operations = [
        migrations.CreateModel(
            name='PLZdb',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50)),
                ('plz', models.CharField(max_length=4)),
            ],
            options={
                'indexes': [
                    models.Index(fields=['nom'], name='common_plzd_nom_f87817_idx'),
                    models.Index(fields=['plz'], name='common_plzd_plz_88d16d_idx')
                ],
            },
        ),
    ]
