from datetime import date, timedelta
import csv

from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.core.cache import cache
from django.db import models
from django.db.models import Q
from django.db.models.functions import ExtractDay

from .distance import distance_real, get_point_from_address, wgs84_to_mn95
from .fields import PhoneNumberField
from .utils import current_app, icon_url

GEOADMIN_MAP_URL_TEMPLATE = (
    "https://map.geo.admin.ch/?lang=fr&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe"
    "&E={long}&N={lat}&zoom=8&crosshair=marker"
)
GOOGLE_MAP_URL_TEMPLATE = "https://www.google.com/maps/search/?api=1&query={lat}%2C{long}"


class GeolocMixin:
    def check_geolocalized(self, save=True):
        if self.empl_geo:
            return True
        return self.geolocalize(save=save)

    def geolocalize(self, save=True):
        lon_lat = get_point_from_address(self.rue, f"{self.npa} {self.localite}")
        if lon_lat:
            self.empl_geo = list(lon_lat)
            if save:
                self.save()
            return True
        elif self.empl_geo:
            self.empl_geo = None
            if save:
                self.save()
        return False

    @property
    def map_link(self):
        if not self.empl_geo:
            return None
        pt = wgs84_to_mn95(*self.empl_geo)
        return GEOADMIN_MAP_URL_TEMPLATE.format(long=pt.x, lat=pt.y)

    @property
    def gmap_link(self):
        if not self.empl_geo:
            return None
        wgs84_to_mn95(*self.empl_geo)
        return GOOGLE_MAP_URL_TEMPLATE.format(long=self.empl_geo[0], lat=self.empl_geo[1])


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Le courriel est obligatoire')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self.create_user(email, password, **extra_fields)


class Utilisateur(AbstractUser):
    """Main user model."""
    email = models.EmailField('Courriel', unique=True)
    tel = PhoneNumberField("Téléphone", blank=True)
    fonction = models.CharField('Fonction', max_length=100, blank=True)

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return self.nom_prenom if self.last_name else self.email

    @property
    def nom_prenom(self):
        return " ".join([self.last_name, self.first_name])

    @property
    def is_benevole(self):
        return hasattr(self, 'benevole') and not self.has_perm('client.change_client')

    def needs_2fa(self):
        # Bénévoles transport et bénévoles visite «simples» n’ont pas besoin de 2fa.
        if self.is_benevole and (
            current_app() == 'transport' or (
                current_app() == 'visite' and not self.benevole.has_activite("visites1")
            )
        ):
            return False
        return True

    def has_intervention_on(self, client, types=None):
        """
        Indique si cet utilisateur a une intervention à faire pour le client, ou
        s'il en a fait une les dix derniers jours.
        """
        if current_app() == 'alarme':
            missions = self.mission_set.annotate(
                diff_days=ExtractDay(date.today() - models.F('effectuee'))
            ).filter(client=client).filter(Q(effectuee=None) | Q(diff_days__lt=10))
            if types is not None:
                missions = missions.filter(type_mission__code__in=types)
            return missions.exists()
        if current_app() == 'visite':
            from visite.models import clients_en_attente_inscription

            if (
                self.is_benevole and self.benevole.has_activite("visites1") and
                clients_en_attente_inscription().filter(pk=client.pk).exists()
            ):
                # Accès pour "inscripteur"
                return True
        return False


class GroupInfo(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    def __str__(self):
        return f"Description of group {self.group.name}"


class DistanceFigee(models.Model):
    """
    Distance figée de manière statique entre deux points, pour éviter d'éventuelles
    variation du service d'interrogation des distances (par ex. pour une facturation
    stable des km d'un transport régulier).
    """
    created = models.DateTimeField(auto_now_add=True)
    empl_geo_dep = ArrayField(models.FloatField(), size=2, verbose_name="Empl. géo de départ")
    empl_geo_arr = ArrayField(models.FloatField(), size=2, verbose_name="Empl. géo d’arrivée")
    distance = models.DecimalField("Distance [km]", max_digits=5, decimal_places=1)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['empl_geo_dep', 'empl_geo_arr'], name='trajet_unique'
            ),
        ]
        verbose_name = "Distance figée"
        verbose_name_plural = "Distances figées"

    def __str__(self):
        return f"Distance figée de {self.distance} entre {self.empl_geo_dep} et {self.empl_geo_arr}"

    @classmethod
    def get_from_coords(cls, dep, arr):
        try:
            return DistanceFigee.objects.get(empl_geo_dep=dep, empl_geo_arr=arr)
        except DistanceFigee.DoesNotExist:
            return None

    def save(self, **kwargs):
        super().save(**kwargs)
        cache_key = str(hash(tuple(self.empl_geo_dep + self.empl_geo_arr)))
        cached = cache.delete(cache_key)


class DistanceCache(models.Model):
    """
    Mise en cache de la distance entre deux localités, utilisée pour améliorer
    le calcul approximatif de distance entre deux points (par rapport au vol d’oiseau)
    sans avoir à recourir à un appel d’API externe.
    """
    created = models.DateTimeField(auto_now_add=True)
    # La clé est construite selon `npa1city1$npa2city2` avec npa1 < npa2
    cle = models.CharField(max_length=150)
    dist = models.DecimalField(max_digits=4, decimal_places=1)
    temps = models.DurationField()

    class Meta:
        indexes = [
            models.Index("cle", name="index_key"),
        ]

    @classmethod
    def _make_key(cls, npa1, loc1, npa2, loc2):
        if npa1 < npa2:
            return f"{npa1}{loc1}${npa2}{loc2}"
        else:
            return f"{npa2}{loc2}${npa1}{loc1}"

    @classmethod
    def calc_dist(cls, loc0, loc1):
        lon_lat1 = get_point_from_address("Gare", f"{loc0[0]} {loc0[1]}")
        lon_lat2 = get_point_from_address("Gare", f"{loc1[0]} {loc1[1]}")
        dist = distance_real(lon_lat1, lon_lat2, with_geom=False, figee=False)
        dist_km = round(dist["distance"] / 1000, 1)
        cls.objects.create(
            cle=cls._make_key(loc0[0], loc0[1], loc1[0], loc1[1]),
            dist=dist_km, temps=timedelta(seconds=dist["duration"]),
        )
        return dist_km


class CachedDistCalculator:
    def __init__(self, loc_cibles, localites2):
        self.loc_cibles = loc_cibles
        cles = set()
        for npa1, loc1 in loc_cibles:
            for npa2, loc2 in localites2:
                cles.add(DistanceCache._make_key(npa1, loc1, npa2, loc2))
        self._cached = {
            result.cle: result.dist
            for result in DistanceCache.objects.filter(cle__in=cles)
        }

    def get_best(self, npa, localite, only_cached=False):
        """
        Renvoie None si même localité, sinon renvoie distance la plus proche entre
        localités self.loc_cibles et npa/localite.
        """
        dists = []
        for loc in self.loc_cibles:
            if (loc[0], loc[1]) == (npa, localite):
                return None
            key = DistanceCache._make_key(loc[0], loc[1], npa, localite)
            try:
                dists.append(self._cached[key])
            except KeyError:
                if only_cached:
                    return None
                dists.append(DistanceCache.calc_dist(loc, (npa, localite)))
                self._cached[key] = dists[-1]
        return sorted(dists)[0]


class TypeFichier(models.TextChoices):
    DOC_GENERAL = 'doc', "Document général"
    QUEST_ALARME = 'quest', "Questionnaire signé"
    CONTRAT_ALARME = 'contrat', "Contrat signé"


def content_type_path(instance, filename):
     return "fichiers/{0}/{1}".format(instance.content_type.model, filename)


class Fichier(models.Model):
    """
    Modèle générique de stockage de fichier pour différents autres modèles (clients, bénévoles, etc.).
    """
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")
    titre = models.CharField("Titre", max_length=150)
    fichier = models.FileField("Fichier", upload_to=content_type_path)
    typ = models.CharField("Type de document", max_length=10, choices=TypeFichier.choices, default='doc')
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    quand = models.DateTimeField()

    def __str__(self):
        return f'Fichier «{self.titre}» pour {self.content_object}'

    def delete(self, **kwargs):
        self.fichier.storage.delete(self.fichier.name)
        super().delete(**kwargs)

    @property
    def icon(self):
        return icon_url(self.fichier.name)


class PLZdb(models.Model):
    nom = models.CharField(max_length=50)
    plz = models.CharField(max_length=4)

    class Meta:
        indexes = [
            models.Index(fields=['nom']),
            models.Index(fields=['plz']),
        ]

    def __str__(self):
        return f"{self.plz} {self.nom}"

    @classmethod
    def import_from_csv(cls, csv_path):
        PLZdb.objects.all().delete()
        with open(csv_path) as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                PLZdb.objects.create(nom=row[0], plz=row[1])
