import unicodedata
from datetime import date, datetime
from decimal import Decimal, ROUND_HALF_UP
from functools import cache
from pathlib import Path

import calendar
from workalendar.europe import Neuchatel

from django.apps import apps
from django.conf import settings
from django.db.models import Q
from django.templatetags.static import static
from django.utils.dateformat import format as django_format

canton_app = apps.get_app_config(settings.CANTON_APP)
IMAGE_EXTS = ['.jpg', '.jpeg', '.png', '.tif', '.tiff', '.gif']


@cache
def current_app():
    for app in ['transport', 'visite', 'alarme', 'besoins', 'crm']:
        if apps.is_installed(app):
            return app
    raise RuntimeError("No current app detected")


def fact_policy():
    return canton_app.fact_policy


def arrondi_5(montant):
    if isinstance(montant, Decimal):
        return round(montant * Decimal('20')) / Decimal('20')
    return round(montant * 20) / 20


def tva_et_arrondi(montant):
    tva = (montant * settings.TAUX_TVA).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
    diff_arrond = arrondi_5(montant + tva) - (montant + tva)
    return tva, diff_arrond


def canton_abrev():
    return canton_app.abrev


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


class NoAPIException(Exception):
    pass


def get_erp_api():
    if settings.CANTON_APP == 'cr_ne':
        from cr_ne.views import get_api
        return get_api()
    raise NoAPIException(f"No canton API for '{settings.CANTON_APP}'")


def icon_url(file_name):
    ext = Path(file_name).suffix.lower()
    icon = 'master'  # default
    if ext in IMAGE_EXTS:
        icon = 'image'
    elif ext == '.pdf':
        icon = 'pdf'
    elif ext in ('.xls', '.xlsx', '.ods', '.csv'):
        icon = 'tsv'
    elif ext == '.odt':
        icon = 'odt'
    elif ext in ('.doc', '.docx'):
        icon = 'docx'
    return static(f'ficons/{icon}.svg')


def client_url_name():
    match current_app():
        case 'alarme':
            return 'client-edit'
        case 'transport':
            return 'transport-client-detail'
        case 'visite':
            return 'client-edit'
        case _:
            return ''


def same_month(date1, date2):
    return date1 and date2 and (date1.year, date1.month) == (date2.year, date2.month)


def last_day_of_month(_date):
    return _date.replace(day=calendar.monthrange(_date.year, _date.month)[1])


def format_HM(tdelta):
    seconds = round(tdelta.total_seconds())
    hours = seconds // 3600
    minutes = seconds % 3600 // 60
    return '{:02d}:{:02d}'.format(hours, minutes)


def format_mois_an(_date):
    return django_format(_date, 'F Y')


class WrongDateFormat(Exception):
    pass


def read_date(dt):
    """Convertir chaîne date lue dans un fichier d'importation vers une date Python."""
    if not dt or dt in {'00.00.0000', '- - -'}:
        return None
    if isinstance(dt, datetime):
        return dt.date()
    if not dt.strip():
        return None
    try:
        return date(*reversed([int(part) for part in dt.split('.')]))
    except ValueError:
        raise WrongDateFormat(f"{dt} is not a valid date.")


def date_range_overlap(range1, range2):
    latest_start = max(range1.lower, range2.lower)
    earliest_end = min(range1.upper or date(2100, 1, 1), range2.upper or date(2100, 1, 1))
    delta = (earliest_end - latest_start).days + 1
    return max(0, delta)


class RegionFinder:
    # chiffre supérieur de range non inclus
    regions_ne = {
        'Littoral Est': [range(2068, 2100), range(2523, 2526)],
        'Littoral Ouest': [range(2012, 2034)],
        'Montagnes': [range(2300, 2326), range(2400, 2420)],
        'Neuchâtel': [range(2000, 2012), range(2034, 2037), range(2067, 2068)],
        'Val-de-Ruz': [range(2037, 2066), range(2200, 2300)],
        'Val-de-Travers': [range(2100, 2200)],
        '__toutes__': [range(2000, 2326), range(2400, 2420), range(2523, 2526)],
    }
    regions_ju = {
        'Distr. Delémont': [range(2800, 2875)],
        'Distr. Porrentruy': [range(2882, 2887), range(2888, 2999)],
        'Distr. Franches-M.': [range(2301, 2400), range(2714, 2719), range(2887, 2888)],
        '__toutes__': [range(2301, 2400), range(2714, 2719), range(2800, 2999)],
    }

    @classmethod
    def regions(cls):
        return getattr(cls, f'regions_{canton_abrev().lower()}')

    @classmethod
    def get_region(cls, npa):
        """Renvoie le nom de la région à partir du NPA, 'Autre' si pas trouvé."""
        if not npa:
            return 'Autre'
        try:
            npa = int(npa)
        except ValueError:
            pass
        else:
            for nom, ranges in cls.regions().items():
                if nom == '__toutes__':
                    continue
                if any(npa in rg for rg in ranges):
                    return nom
        return 'Autre'

    @classmethod
    def get_region_filter(cls, field_name, reg_name):
        """
        Renvoie un filtre Q() pour filtrer des adresses selon une région.
        stop - 1 car la requête __range est inclusive.
        """
        regs = cls.regions()
        if reg_name == 'Autre':
            range1 = regs['__toutes__'][0]
            flt = Q(**{f'{field_name}__range': (str(range1.start), str(range1.stop - 1))})
            for rg in regs['__toutes__'][1:]:
                flt |= Q(**{f'{field_name}__range': (str(rg.start), str(rg.stop - 1))})
            return ~Q(flt)
        range1 = regs[reg_name][0]
        flt = Q(**{f'{field_name}__range': (str(range1.start), str(range1.stop - 1))})
        for rg in regs[reg_name][1:]:
            flt |= Q(**{f'{field_name}__range': (str(rg.start), str(rg.stop - 1))})
        return flt


class CRHolidays(Neuchatel):
    HOLIDAYS_TRANS = {
        'New year': 'Nouvel an',
        "Berchtold's Day": 'Deux de l’an',
        'Republic Day': 'Instauration de la République',
        'Good Friday': 'Vendredi saint',
        'Easter Monday': 'Lundi de Pâques',
        'Labour Day': 'Fête du Travail',
        'Ascension Thursday': 'Ascension',
        'Whit Monday': 'Lundi de Pentecôte',
        'National Holiday': 'Fête nationale',
        'Federal Thanksgiving Monday': 'Jeûne Fédéral',
        'Christmas Day': 'Noël',
        'Boxing Day': 'Lendemain de Noël',
    }
    extra_days = {
        2022: [
            (date(2022, 2, 28), "Congé supplémentaire"),
            (date(2022, 12, 27), "Congé supplémentaire"),
            (date(2022, 12, 28), "Congé supplémentaire"),
        ],
    }

    def holidays(self, year):
        return [
            (dt, self.HOLIDAYS_TRANS.get(hol_str, hol_str))
            for dt, hol_str in super().holidays(year)
        ]

    def holidays_dict(self, years):
        return dict(
            [item for sublist in [self.holidays(year) for year in years]
            for item in sublist]
        )

    def has_berchtolds_day(self, year):
        return True

    def get_variable_days(self, year):
        days = super().get_variable_days(year)
        # 26.12 is conditional in official calendar, add it inconditionally.
        if date(year, 12, 26) not in [day for day, _ in days]:
            days.append((date(year, 12, 26), self.boxing_day_label))
        days.append((date(year, 12, 31), 'St-Sylvestre'))
        return days
