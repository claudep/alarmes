from django.apps import apps
from django.conf import global_settings
from django.db.models import TextChoices
from django.utils.functional import classproperty
from django.utils.translation import gettext as _


class Services(TextChoices):
    ALARME = 'alarme', 'Alarme'
    TRANSPORT = 'transport', 'Transports'
    VISITE = 'visite', 'Visites'
    OSAD = 'osad', 'OSAD'

    @classproperty
    def choices_can(cls):
        if apps.is_installed('cr_ju'):
            # Clients transport utilisés pour les quest. de besoins
            return [c for c in cls.choices if c[0] in ['alarme', 'transport']]
        return cls.choices


class Languages:
    _choices = None

    @classmethod
    def choices(cls):
        if not cls._choices:
            cls._choices = tuple(
                (lg[0], _(lg[1]).lower()) for lg in global_settings.LANGUAGES
                if '-' not in lg[0]
            ) + (('zh', 'chinois'), ('gsw', 'suisse allemand'))
        return cls._choices

    @classmethod
    def keys(cls):
        return dict(cls.choices()).keys()


class Genres(TextChoices):
    FEMME = 'F', 'Femme'
    HOMME = 'M', 'Homme'


class Handicaps(TextChoices):
    CANNES = 'cannes', 'Cannes'
    DEAMBULATEUR = 'deamb', 'Déambulateur'
    CHAISE = 'chaise', 'Chaise roulante'
    VISION = 'vision', 'Handicap visuel'
    AUDITION = 'audition', 'Handicap auditif'
    SURPOIDS = 'poids', 'Surpoids'
    COGNITIFS = 'cognitif', 'Troubles cognitifs'


class Pays(TextChoices):
    CH = "CH", "Suisse"
    FR = "FR", "France"
    DE = "DE", "Allemagne"
    IT = "IT", "Italie"
    ES = "ES", "Espagne"
    PT = "PT", "Portugal"
    AT = "AT", "Autriche"
    GB = "GB", "Grande-Bretagne"
    US = "US", "États-Unis"
    FI = "FI", "Finlande"
    IL = "IL", "Israël"
