import re
from datetime import date
from io import BytesIO

import httpx

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.mail import mail_admins, send_mail
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import transaction
from django.db.models import Q
from django.http import (
    FileResponse, HttpResponse, HttpResponseRedirect, HttpResponseServerError,
    JsonResponse
)
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.module_loading import import_string
from django.views.generic import (
    DeleteView, ListView as DjangoListView, TemplateView, UpdateView, View
)
from django.views.static import serve

from two_factor.views import LoginView as DTFALoginView

from common.choices import Languages
from common.export import OpenXMLExport
from common.models import Fichier, PLZdb, Utilisateur
from client.models import Referent
from . import forms
from .utils import canton_abrev, current_app


def error_view(request):
    template = loader.get_template('500.html')
    return HttpResponseServerError(template.render({'main_logo': settings.MAIN_LOGO}))


class LoginView(DTFALoginView):
    redirect_authenticated_user = True


class NPALocaliteAutocomplete(View):
    # Read https://developer.post.ch/en/address-web-services-rest, section 4.4
    POST_API = 'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/autocomplete4'

    def get(self, request, *args, **kwargs):
        values = []
        q = request.GET.get('q').strip().lower().replace('saint', 'st')
        if q:
            if settings.POSTE_API_USER:
                values = self.search_from_api(q)
            else:
                values = self.search_from_db(q)
        return JsonResponse(values, safe=False)

    def search_from_api(self, q):
        params = {'dataset': 'plz_verzeichnis_v2', 'rows': 30, 'q': q}
        try:
            zip_part = re.match(r'\s*(\d+)\s?', q).group().strip()
        except AttributeError:
            zip_part = ""
        town_part = q.replace(zip_part, '').strip()
        params = {
            "request": {
                "ONRP": 0,
                "ZipCode": zip_part,
                "ZipAddition": "",
                "TownName": town_part,
                "STRID": 0,
                "StreetName": "",
                "HouseKey": 0,
                "HouseNo": "",
                "HouseNoAddition": ""
            },
            "zipOrderMode": 1,  # ZipTypeNotPoBox
            "zipFilterMode": 0
        }
        try:
            response = httpx.post(
                self.POST_API,
                auth=(settings.POSTE_API_USER, settings.POSTE_API_PASSWORD),
                json=params,
                headers={'Accept': "application/json"}
            )
        except httpx.HTTPError as err:
            return self.error_response(str(err))
        if response.status_code != 200:
            return self.error_response(response)
        json_resp = response.json()
        results = json_resp.get('QueryAutoComplete4Result', {}).get('AutoCompleteResult', [])
        values = [
            f"{res['ZipCode']} {res['TownName']}" for res in results
        ]
        return [
            {'value': val, 'label': val} for val in values
        ]

    def search_from_db(self, q):
        """In the case the API is unavailable."""
        if q.isdigit():
            query = PLZdb.objects.filter(plz__startswith=q)
        else:
            query = PLZdb.objects.filter(nom__icontains=q)
        return [
            {'value': f'{line.plz} {line.nom}', 'label': f'{line.plz} {line.nom}'}
            for line in query[:30]
        ]

    def error_response(self, response):
        mail_admins("Error API Swisspost", f"Error connecting to SwissPost: {response}")
        return {'result': 'error', 'message': "Échec de connexion à l’API SwissPost"}


class ReferentFactAutocomplete(View):
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q').strip()
        query = Referent.objects.exclude(
            Q(date_archive__isnull=False) | Q(facturation_pour=[])
        ).filter(
            Q(nom__unaccent__icontains=term)
        )[:15]
        referents = [
            {'label': f"{ref.nom_prenom}, {ref.rue_localite()}", 'value': ref.pk}
            for ref in query
        ]
        results = []
        seen_labels = set()
        for ref in referents:
            if ref['label'] in seen_labels:
                continue
            results.append(ref)
            seen_labels.add(ref['label'])
        return JsonResponse(results, safe=False)


class LanguesAutocompleteView(View):
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q', '').lower()
        if not term:
            raise Http404("Le terme de recherche est obligatoire")
        results = [{
            'label': lang[1], 'value': lang[0],
        } for lang in Languages.choices() if term in lang[1]]
        return JsonResponse(results, safe=False)


class GeoAddressMixin:
    def geo_enabled(self, instance):
        return settings.QUERY_GEOADMIN_FOR_ADDRESS

    def check_geolocalized(self, form, save=True):
        if (
            set(form.changed_data).intersection({'npa', 'localite', 'rue'}) and
            self.geo_enabled(form.instance)
        ):
            form.instance.geolocalize(save=save)

    def form_valid(self, form):
        self.check_geolocalized(form, save=False)
        return super().form_valid(form)


class ExportableMixin:
    export_class = OpenXMLExport
    export_file_name = None
    sheet_title = "Feuil1"
    col_widths = [50]

    def dispatch(self, request, *args, **kwargs):
        self.export_flag = request.GET.get('export', None) == '1'
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'exportable': True}

    def export_lines(self, context):
        raise NotImplementedError("Subclasses of ExportableMixin must implement export_lines()")

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = self.export_class(sheet_title=self.sheet_title, col_widths=self.col_widths)
            export.fill_data(self.export_lines(context))
            file_name = self.export_file_name or self.__class__.__name__.replace("View", "")
            if not file_name.endswith(".xlsx"):
                file_name += ".xlsx"
            return export.get_http_response(file_name)
        else:
            return super().render_to_response(context, **response_kwargs)


class JournalMixin:
    def _create_instance(self, **kwargs):
        raise NotImplementedError

    def journalize(self, form, changed_values=True, add_message=None):
        if self.is_create:
            if not add_message and not hasattr(self, 'journal_add_message'):
                return
            journal_msg = add_message or self.journal_add_message.format(obj=form.instance)
        else:
            changes = form.get_changed_string(changed_values=changed_values)
            if changes:
                journal_msg = self.journal_edit_message.format(obj=form.instance, fields=changes)
            else:
                return
        self._create_instance(
            description=journal_msg, quand=timezone.now(),
            qui=self.request.user if not self.request.user.is_anonymous else None
        )


class BenevoleMixin:
    """Accès limité à un compte de bénévole, définition de self.benev"""
    def dispatch(self, request, *args, **kwargs):
        try:
            self.benev = request.user.benevole
        except ObjectDoesNotExist:
            raise PermissionDenied("Aucun bénévole n’est lié à votre compte utilisateur.")
        return super().dispatch(request, *args, **kwargs)


class CreateUpdateView(UpdateView):
    """Mix generic Create and Update views."""
    template_name = 'general_edit.html'
    is_create = False
    json_response = False

    def get_object(self):
        return None if self.is_create else super().get_object()

    def get_client(self):
        return None

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'client': self.get_client()}

    def form_valid(self, form):
        with transaction.atomic():
            if self.is_create:
                self.object = form.save()
                if hasattr(self, 'journalize'):
                    self.journalize(form)
            else:
                # Journalize before saving form so it's possible to get previous
                # version of objects during journalization.
                if hasattr(self, 'journalize'):
                    self.journalize(form, changed_values=True)
                self.object = form.save()
        if not self.json_response:
            success_message = self.get_success_message(self.object)
            if success_message:
                messages.success(self.request, success_message)
            return HttpResponseRedirect(self.get_success_url())


class JournalizingDeleteView(DeleteView):
    json_response = False
    journal_delete_message = "Suppression de {obj}"

    def journalize_deletion(self, form, delete_message=None):
        journal_msg = delete_message or self.journal_delete_message
        self._create_instance(
            description=journal_msg.format(obj=self.object), quand=timezone.now(),
            qui=self.request.user if not self.request.user.is_anonymous else None
        )

    def form_valid(self, form):
        if not self.json_response:
            success_url = self.get_success_url()
        with transaction.atomic():
            self.journalize_deletion(form)
            self.object.delete()
        if not self.json_response:
            return HttpResponseRedirect(success_url)


class FichierEditView(CreateUpdateView):
    model = Fichier
    parent_model = None  # défini par sous-classes
    form_class = forms.FichierForm
    template_name = 'fichier_edit.html'
    pk_url_kwarg = 'pk_file'
    journal_add_message = "Ajout d’un fichier: {obj.titre}"
    journal_edit_message = "Modification du fichier {obj.titre}: {fields}"
    json_response = True

    def get_related_object(self):
        obj = get_object_or_404(self.parent_model, pk=self.kwargs['pk'])
        if not obj.can_edit(self.request.user):
            raise PermissionDenied(
                "Vous n’avez pas les permissions nécessaires pour modifier cet objet."
            )
        return obj

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'app': current_app(),
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'obj': self.get_related_object(),
        }

    def form_valid(self, form):
        parent_obj = self.get_related_object()
        if self.is_create:
            form.instance.content_object = parent_obj
            form.instance.qui = self.request.user
            form.instance.quand = timezone.now()
        elif 'fichier' in form.changed_data:
            # Ne modifier auteur et date que si un nouveau fichier a été envoyé
            form.instance.qui = self.request.user
            form.instance.quand = timezone.now()
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': '#fichiers-container'})


class FichierListeView(TemplateView):
    """Rechargement dynamique du bloc des fichiers uniquement."""
    template_name = 'fichier_list.html'
    # définis par sous-classes
    parent_model = None
    editurl_name = None
    deleteurl_name = None

    def get_context_data(self, **kwargs):
        obj = get_object_or_404(self.parent_model, pk=self.kwargs['pk'])
        fichiers_app = (
            obj.persona.fichiers_app(current_app())
            if hasattr(obj, "persona") else obj.fichiers_app(current_app())
        )
        return {
            **super().get_context_data(**kwargs),
            'parent': obj,
            'fichiers': fichiers_app.order_by('-quand'),
            'editurl_name': self.editurl_name,
            'deleteurl_name': self.deleteurl_name,
        }


class FichierDeleteView(DeleteView):
    model = Fichier
    parent_model = None  # défini par sous-classes

    def form_valid(self, form):
        parent = self.object.content_object
        if not parent.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer ce fichier.")
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': '#fichiers-container'})


class FilterFormMixin:
    filter_formclass = None
    return_all_if_unbound = True

    def get(self, request, *args, **kwargs):
        if self.filter_formclass:
            self.filter_form = self.filter_formclass(data=request.GET or None, **self.get_filterform_kwargs())
        else:
            self.filter_form = None
        return super().get(request, *args, **kwargs)

    def get_filterform_kwargs(self):
        return {}

    def get_queryset(self, base_qs=None):
        items = super().get_queryset() if base_qs is None else base_qs
        if self.filter_form.is_bound and self.filter_form.is_valid() and (
             self.return_all_if_unbound or any(self.filter_form.cleaned_data.values())
        ):
            items = self.filter_form.filter(items)
        elif not self.return_all_if_unbound:
            return self.model.objects.none()
        return items

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'form': self.filter_form,
        }


class BasePDFView(View):
    obj_class = None
    pdf_class = None
    produce_kwargs = {}

    def get_object(self):
        return get_object_or_404(self.obj_class, pk=self.kwargs[getattr(self, 'pk_url_kwarg', 'pk')])

    def get_pdf_class(self):
        if isinstance(self.pdf_class, str):
            return import_string(self.pdf_class % {'CANTON_APP': settings.CANTON_APP})
        return self.pdf_class

    def convert_to_png(self, buff):
        from pdf2image import convert_from_bytes
        from PIL import Image, ImageOps

        images = convert_from_bytes(buff.getvalue(), dpi=150)
        brd_width = 3
        merged_img = Image.new(
            'RGBA',
            (images[0].width + brd_width * 2, (images[0].height + brd_width * 2) * len(images))
        )
        height = 0
        for img in images:
            img_border = ImageOps.expand(img, border=brd_width, fill="gray")
            merged_img.paste(img_border, (0, height))
            height += img_border.height

        response = HttpResponse(content_type="image/png")
        merged_img.save(response, "PNG")
        return response

    def get(self, request, *args, as_img=False, **kwargs):
        instance = self.get_object()
        temp = BytesIO()
        PDFClass = self.get_pdf_class()
        pdf = PDFClass(temp)
        pdf.produce(instance, **self.produce_kwargs)
        filename = pdf.get_filename(instance)
        temp.seek(0)
        if as_img:
            return self.convert_to_png(temp)
        return FileResponse(temp, as_attachment=True, filename=filename)


class MediaServeView(View):
    def get(self, request, *args, **kwargs):
        if self.request.get_host() == 'localhost:8000':
            return serve(request, kwargs['path'], document_root=settings.MEDIA_ROOT)
        filepath = settings.MEDIA_ROOT / kwargs['path']
        response = HttpResponse()
        response['Content-Type'] = 'application/force-download'
        response['Content-Disposition'] = 'attachment; filename="%s"' % filepath.name
        response['X-Sendfile'] = str(filepath).encode('utf-8')
        return response


class ManifestView(TemplateView):
    content_type = "application/json"
    extra_context = {'canton': canton_abrev().lower()}


class GracefulPaginator(Paginator):
    def page(self, number):
        # We do this as unfortunately Django ListView calls .page instead if .get_page
        try:
            number = self.validate_number(number)
        except PageNotAnInteger:
            number = 1
        except EmptyPage:
            number = self.num_pages
        return super().page(number)


class ListView(DjangoListView):
    paginator_class = GracefulPaginator

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if context['paginator'] is not None:
            context['elided_page_range'] = context['paginator'].get_elided_page_range(context['page_obj'].number)
        return context


class UtilisateurListView(PermissionRequiredMixin, FilterFormMixin, ListView):
    template_name = 'utilisateurs.html'
    model = Utilisateur
    permission_required = 'common.change_utilisateur'
    filter_formclass = forms.UtilisateurFilterForm
    is_active = True
    paginate_by = 50

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Utilisateur.objects.filter(
                is_active=self.is_active
            ).prefetch_related('groups').order_by('last_name')
        )


class UtilisateurUpdateView(PermissionRequiredMixin, CreateUpdateView):
    template_name = 'utilisateur_edit.html'
    permission_required = 'common.change_utilisateur'
    model = Utilisateur
    form_class = forms.UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')

    def get_success_message(self, obj):
        if self.is_create:
            return f"Vous avez bien créé {obj}."
        else:
            return f"{obj} a bien été modifié"

    def form_valid(self, form):
        if self.is_create:
            pwd = get_random_string(length=10)
            form.instance.set_password(pwd)
            send_mail(
                "Nouveau compte pour application Croix-Rouge",
                "Bonjour\n\nUn nouveau compte a été créé pour vous:\n"
                f"Site: https://{settings.ALLOWED_HOSTS[0]}\n"
                f"Mot de passe: {pwd}\n\n"
                "Cordiales salutations",
                None,
                [form.instance.email]
            )
        return super().form_valid(form)


class UtilisateurOtpReinitView(PermissionRequiredMixin, View):
    permission_required = 'common.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        utilisateur.totpdevice_set.all().delete()
        messages.success(
            request,
            f'Le 2ème facteur d’authentification de «{utilisateur}» a été réinitialisé. '
            'Lors de sa prochaine connexion, cette personne devra reconfigurer son accès '
            'avec un nouveau code QR.'
        )
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))
