from datetime import date, datetime
from unittest.mock import patch

from httpx import HTTPError

from django import forms
from django.contrib.auth.models import Permission
from django.core import mail
from django.core.exceptions import ValidationError
from django.test import TestCase, override_settings
from django.urls import reverse

from client.models import AdresseClient, Client, Persona
from .fields import PhoneCharField, PhoneNumberValidator
from .forms import ModelForm, NPALocaliteMixin
from .models import Utilisateur
from .utils import RegionFinder, canton_abrev, read_date


class CommonTests(TestCase):
    def test_phone_validation(self):
        phonenumber_validator = PhoneNumberValidator()
        phonenumber_validator('032 950 10 05')
        phonenumber_validator('0033 380414860')
        phonenumber_validator('0033 3 80 41 48 60')
        phonenumber_validator('0049 4 04 92 92 111')
        phonenumber_validator('0033 380414860')
        phonenumber_validator('+33 380414860')
        phonenumber_validator('+44 208 1033 146')
        with self.assertRaises(ValidationError):
            phonenumber_validator('0044 60')
        with self.assertRaises(ValidationError):
            phonenumber_validator('+41')
        with self.assertRaises(ValidationError):
            phonenumber_validator('032 555 55 55 55')

        class SomeForm(forms.Form):
            tel = PhoneCharField()

        form = SomeForm(data={'tel': '+41'})
        self.assertFalse(form.is_valid())
        form = SomeForm(data={'tel': '+33 754 23 23 23 (mari)'})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['tel'], '+33 7 54 23 23 23')

    def test_npalocalite_formfield(self):
        class AdresseForm(NPALocaliteMixin, ModelForm):
            class Meta:
                model = AdresseClient
                fields = ['rue', 'npa', 'localite']

        form = AdresseForm()
        form_html = form.as_div()
        self.assertInHTML('<label for="id_npalocalite">NPA Localité :</label>', form_html)
        self.assertInHTML(
            '<input type="text" name="npalocalite" class="autocomplete" autocomplete="off" '
            'placeholder="N° postal ou nom de localité" data-searchurl="/npa/autocomplete/" id="id_npalocalite">',
            form_html
        )
        self.assertInHTML('<input type="hidden" name="npa" id="id_npa">', form_html)
        self.assertInHTML('<input type="hidden" name="localite" id="id_localite">', form_html)

        adr = AdresseClient.objects.create(
            persona=Persona.objects.create(nom="Doe"),
            rue="Rue Bleue", npa="2300", localite="La Chaux-de-Fonds"
        )
        form = AdresseForm(instance=adr)
        form_html = form.as_div()
        self.assertIn('value="2300 La Chaux-de-Fonds"', form_html)
        form_data = {
            'rue': 'Rue Rouge',
            'npalocalite': '2000',
        }
        form = AdresseForm(instance=adr, data=form_data)
        self.assertFalse(form.is_valid())
        form_data['npalocalite'] = '2000  Neuchâtel '  # Suppl. spaces should disappear
        form = AdresseForm(instance=adr, data=form_data)
        self.assertTrue(form.is_valid())
        form.save()
        adr.refresh_from_db()
        self.assertEqual(adr.npa, '2000')
        self.assertEqual(adr.localite, 'Neuchâtel')

    @override_settings(POSTE_API_USER='test')
    def test_npaLocalite_autocomplete(self):
        user = Utilisateur.objects.create_user('user@example.org', password='x')
        self.client.force_login(user)
        with patch('httpx.post', side_effect=HTTPError('error')):
            response = self.client.get(reverse('npalocalite-search') + '?q=xyz')
        self.assertEqual(
            response.json(),
            {'message': 'Échec de connexion à l’API SwissPost', 'result': 'error'}
        )

    def test_region_finder_get_region(self):
        if canton_abrev().lower() == "ne":
            self.assertEqual(RegionFinder.get_region('1800'), 'Autre')
            self.assertEqual(RegionFinder.get_region('2000'), 'Neuchâtel')
            self.assertEqual(RegionFinder.get_region('2523'), 'Littoral Est')
            self.assertEqual(RegionFinder.get_region('2333'), 'Autre')
        if canton_abrev().lower() == "ju":
            self.assertEqual(RegionFinder.get_region('2850'), 'Distr. Delémont')
            self.assertEqual(RegionFinder.get_region('2900'), 'Distr. Porrentruy')
            self.assertEqual(RegionFinder.get_region('2887'), 'Distr. Franches-M.')
        self.assertEqual(RegionFinder.get_region(''), 'Autre')
        self.assertEqual(RegionFinder.get_region('xxxx'), 'Autre')

    def test_region_finder_get_filter(self):
        if canton_abrev().lower() == "ne":
            npa, region = '2000', 'Neuchâtel'
            Client.objects.create(nom='Smith', npa='2300')
        if canton_abrev().lower() == "ju":
            npa, region = '2800', 'Distr. Delémont'
            Client.objects.create(nom='Smith', npa='2900')
        cl_local = Client.objects.create(nom='Doe', npa=npa)
        cl_vevey = Client.objects.create(nom='Smith', npa='1800')
        self.assertQuerySetEqual(
            Client.objects.avec_adresse(date.today()).filter(
                RegionFinder.get_region_filter('adresse_active__npa', region)
            ),
            [cl_local]
        )
        self.assertQuerySetEqual(
            Client.objects.avec_adresse(date.today()).filter(
                RegionFinder.get_region_filter('adresse_active__npa', 'Autre')
            ),
            [cl_vevey]
        )

    def test_region_finder_get_filter_upper_range(self):
        """
        La valeur haute d'un range() d'une région ne doit pas être incluse dans
        la recherche de cette région.
        """
        region_key, ranges = next(iter(RegionFinder.regions().items()))
        npa_up = ranges[0].stop
        Client.objects.create(nom='Doe', npa=npa_up)
        self.assertQuerySetEqual(
            Client.objects.avec_adresse(date.today()).filter(
                RegionFinder.get_region_filter('adresse_active__npa', region_key)
            ),
            []
        )

    def test_read_date(self):
        for val in ('00.00.0000', '- - -', '', None):
            self.assertIsNone(read_date(val))
        self.assertEqual(read_date('1.3.1965'), date(1965, 3, 1))
        self.assertEqual(read_date(datetime(1965, 3, 1, 12, 45)), date(1965, 3, 1))

    def test_utilisateur_nouveau(self):
        adminuser = Utilisateur.objects.create_user('admin@example.org', password='test')
        adminuser.user_permissions.add(Permission.objects.get(codename='change_utilisateur'))
        self.client.force_login(adminuser)
        response = self.client.get(reverse('utilisateur-add'))
        self.assertContains(response, "Un nouveau mot de passe")
        response = self.client.post(reverse('utilisateur-add'), data={
            'last_name': 'Baba',
            'first_name': 'Ali',
            'email': 'ali.baba@example.com',
            'is_active': 'on',
        })
        self.assertRedirects(response, reverse('utilisateur-list'))
        self.assertTrue(Utilisateur.objects.filter(last_name='Baba').exists())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['ali.baba@example.com'])

    def test_utilisateur_edition(self):
        adminuser = Utilisateur.objects.create_user('admin@example.org', password='test')
        adminuser.user_permissions.add(Permission.objects.get(codename='change_utilisateur'))
        other_user = Utilisateur.objects.create_user('other@example.org', password='test2')
        self.client.force_login(adminuser)
        response = self.client.post(reverse('utilisateur-edit', args=[other_user.pk]), data={
            'last_name': 'Baba',
            'first_name': 'Ali',
            'email': 'ali.baba@example.com',
            'is_active': 'on',
        })
        self.assertRedirects(response, reverse('utilisateur-list'))
        self.assertTrue(Utilisateur.objects.filter(last_name='Baba').exists())
        self.assertEqual(len(mail.outbox), 0)
