from datetime import date, timedelta

from django.template import Library
from django.templatetags.static import static
from django.utils.dates import MONTHS
from django.utils.html import format_html

from common.utils import RegionFinder, current_app
register = Library()


@register.filter
def get_item(obj, key):
    try:
        return obj.get(key) if obj is not None else obj
    except Exception:
        raise TypeError(f"Unable to get key '{key}' from obj '{obj}'")


@register.filter
def counter_total(counter):
    if counter is None:
        return "-"
    if isinstance(counter, dict):
        return sum(counter.values())
    return counter.total()


@register.filter
def get_field(form, key):
    """Obtenir un champ de formulaire d'après son nom."""
    return form[key]


@register.filter
def get_attribute(instance, field_name):
    """Obtenir un champ de formulaire d'après son nom."""
    return getattr(instance, field_name, None)


@register.filter
def to_int(value):
    return int(value) if value else value


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.filter
def as_tel(no_tel):
    if no_tel:
        return format_html('<a class="tel" href="tel:{}">{}</a>', no_tel, no_tel)
    return ''


@register.filter
def as_tel_icon(no_tel):
    if no_tel:
        return format_html(
            '<a class="tel" href="tel:{}" title="{}" data-bs-toggle="tooltip">'
            '<img class="icon blacksvg" src="{}"></a>',
            no_tel, f'Appeler {no_tel}',
            static('img/mobile.svg') if no_tel.startswith('07') else static("img/phone.svg"),
        )
    return ''


@register.filter
def as_email(email):
    if email:
        return format_html('<a href="mailto:{}">{}</a>', email, email)
    return ''


@register.filter
def as_email_icon(email):
    if email:
        return format_html(
            '<a href="mailto:{}" title="{}" data-bs-toggle="tooltip" class="opacity-75 ms-2"><img class="icon blacksvg" src="{}"></a>',
            email, f'Envoyer courriel à {email}', static('img/email.svg'),
        )
    return ''


@register.filter
def boolean_icon(field_val):
    icon_url = static(
        "admin/img/icon-%s.svg" % {True: "yes", False: "no", None: "unknown"}[field_val]
    )
    return format_html('<img src="{}" alt="{}">', icon_url, field_val)


@register.filter(is_safe=True)
def strip_colon(txt):
    return txt.replace(' :', '')


@register.filter
def region(npa):
    return RegionFinder.get_region(npa)


@register.filter
def format_duree(duree, fmt='jours', default=''):
    """
    Mise en forme d'une durée en HH:MM, ou x jours, HH:MM si plus de 7 jours.
    """
    if duree is None:
        return default
    elif not hasattr(duree, 'total_seconds'):
        return duree
    secondes = duree.total_seconds()
    heures = secondes // 3600
    minutes = (secondes % 3600) // 60
    if fmt == 'jours' and heures // 24 > 7:
        return '{} jours, {:02}:{:02}'.format(int(heures // 24), int(heures % 24), int(minutes))
    return '{:02}:{:02}'.format(int(heures), int(minutes))


@register.filter
def format_type(value, default=''):
    if hasattr(value, 'total_seconds'):
        return format_duree(value, fmt="HM", default=default)
    return str(value) if value is not None else default


@register.filter
def m_to_km(value):
    return round(value / 100) / 10


@register.filter
def sec_to_min(value):
    return round(value / 60)


@register.filter
def strip_seconds(duree):
    if duree is not None and str(duree).count(':') > 1:
        return ':'.join(format_duree(duree).split(':')[:2])
    return duree


@register.filter
def date_passed(dt):
    return dt > date.today()


@register.filter
def current_month(dt):
    return dt.year == date.today().year and dt.month == date.today().month


@register.filter
def previous_month(dt):
    day_prev_month = date.today().replace(day=1) - timedelta(days=4)
    return dt.year == day_prev_month.year and dt.month == day_prev_month.month


@register.filter
def mois_fr(dt):
    if dt:
        return MONTHS[dt.month]
    return ''


@register.filter
def monetize(dec):
    return f'{dec:.2f}' if dec else 0


@register.inclusion_tag("help_tooltip.html")
def help_tooltip(text):
    return {'text': text}


@register.filter
def base_template(request):
    template = 'base.html'
    if request.user.is_authenticated and request.user.is_benevole:
        match current_app():
            case 'alarme':
                template = 'alarme/app/base.html'
            case 'transport':
                template = 'transport/app/base.html'
            case 'visite':
                template = 'visite/app/base.html'
    return template


@register.filter
def wrap_in_url(text, url):
    if not url:
        return text
    return format_html('<a href="{}">{}</a>', url, text)
