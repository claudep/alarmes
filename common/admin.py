from datetime import datetime

from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as GroupAdminAuth, UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.utils.timezone import localtime

from .export import ExpLine, OpenXMLExport
from .models import DistanceCache, DistanceFigee, Fichier, GroupInfo, PLZdb, Utilisateur


class ExportAction:
    short_description = "Exporter la sélection dans une liste"
    __name__ = "export"

    def __init__(self, title, exclude=[], extra_fields=()):
        self.title = title
        self.excluded_fields = exclude
        self.extra_fields = extra_fields

    def get_queryset(self, queryset):
        return queryset

    def __call__(self, modeladmin, request, queryset):
        export = OpenXMLExport(self.title)
        fields = [f for f in modeladmin.model._meta.local_fields if not f.name in self.excluded_fields]
        headers = [f.verbose_name for f in fields] + [head for head, _ in self.extra_fields]
        export.write_line(ExpLine(headers, bold=True))
        field_names = [f.name for f in fields] + [fname for _, fname in self.extra_fields]
        for obj in self.get_queryset(queryset):
            export.write_line([self.serialize(obj, field) for field in field_names])
        return export.get_http_response(f"{self.title.lower().replace(' ', '_')}.xlsx")

    def serialize(self, obj, field_name):
        if hasattr(obj, f'get_{field_name}_display'):
            value = getattr(obj, f'get_{field_name}_display')()
        else:
            value = getattr(obj, field_name)
        return value


class ArrayFieldListFilter(admin.SimpleListFilter):
    """An admin list filter for ArrayFields."""

    def lookups(self, request, model_admin):
        """Return the filtered queryset."""
        queryset_values = model_admin.model.objects.values_list(
            self.parameter_name, flat=True
        )
        values = []
        for sublist in queryset_values:
            if sublist:
                for value in sublist:
                    if value:
                        values.append((value, value))
            else:
                values.append(("null", "-"))
        return sorted(set(values))

    def queryset(self, request, queryset):
        """Return the filtered queryset."""
        lookup_value = self.value()
        if lookup_value:
            lookup_filter = (
                {"{}__isnull".format(self.parameter_name): True}
                if lookup_value == "null"
                else {"{}__contains".format(self.parameter_name): [lookup_value]}
            )
            queryset = queryset.filter(**lookup_filter)
        return queryset


class NullableAsBooleanListFilter(admin.FieldListFilter):
    """Filtre de liste comme un booléen valant True si le champ cible n'est pas nul."""
    def __init__(self, field, request, params, model, model_admin, field_path):
        self.lookup_kwarg = f"{field_path}__isnull"
        self.lookup_val = params.get(self.lookup_kwarg)
        super().__init__(field, request, params, model, model_admin, field_path)

    def choices(self, changelist):
        for lookup, title in (
            (None, "Tous"),
            ('False', "Oui"),
            ('True', "Non"),
        ):
            yield {
                "selected": self.lookup_val == lookup,
                "query_string": changelist.get_query_string(
                    {self.lookup_kwarg: lookup}, []
                ),
                "display": title,
            }

    def expected_parameters(self):
        return [self.lookup_kwarg]


class UtilisateurCreationForm(UserCreationForm):
    class Meta:
        model = Utilisateur
        fields = ("email",)


@admin.register(Utilisateur)
class UtilisateurAdmin(UserAdmin):
    model = Utilisateur
    add_form = UtilisateurCreationForm
    list_display = ['email', 'is_staff', 'is_active', 'last_login']
    ordering = ('email',)
    search_fields = ('first_name', 'last_name', 'email')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        if 'username' in fieldsets[0][1]['fields']:
            # Remove username and add fonction
            fieldsets[0][1]['fields'] = [
                field for field in fieldsets[0][1]['fields'] if field != 'username'
            ]
            fieldsets[1][1]['fields'] += ('fonction',)
        return fieldsets


admin.site.unregister(Group)


class GroupInfoInline(admin.StackedInline):
    model = GroupInfo


@admin.register(Group)
class GroupAdmin(GroupAdminAuth):
    #With Django 5.1:
    #list_display = list(GroupAdminAuth.list_display) + ['groupinfo__description']
    list_display = list(GroupAdminAuth.list_display) + ['group_descr']
    inlines = [GroupInfoInline]

    def group_descr(self, obj):
        return obj.groupinfo.description


@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    list_display = ['app_label', 'model']


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ['name', 'content_type', 'codename']
    search_fields = ['codename']


@admin.register(DistanceFigee)
class DistanceFigeeAdmin(admin.ModelAdmin):
    list_display = ['created', 'empl_geo_dep', 'empl_geo_arr', 'distance']


@admin.register(DistanceCache)
class DistanceCacheAdmin(admin.ModelAdmin):
    list_display = ["cle", "dist", "temps"]


@admin.register(Fichier)
class FichierAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'titre', 'typ', 'qui', 'quand']
    search_fields = ['titre']


@admin.register(PLZdb)
class PLZdbAdmin(admin.ModelAdmin):
    list_display = ['nom', 'plz']
    search_fields = ['nom', 'plz']
