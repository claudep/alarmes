import csv
from dataclasses import dataclass, field
from datetime import date, datetime, timedelta
from io import StringIO
from tempfile import NamedTemporaryFile

from openpyxl import Workbook
from openpyxl.styles import Border, DEFAULT_FONT, Font, Side
from openpyxl.utils import get_column_letter

from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import Model
from django.http import HttpResponse
from django.utils.timezone import localtime

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


@dataclass
class ExpLine:
    values: list
    bold: list = ()
    alignment: int = None
    height: int = None
    col_widths: list = ()
    number_formats: dict = field(default_factory=dict)
    borders: str = None

    def __post_init__(self):
        if self.bold is True:
            self.bold = ["__all__"]
        self.values = [self._serialize(v) for v in self.values]

    def __iter__(self):
        return iter(self.values)

    def __getitem__(self, idx):
        return self.values[idx]

    @staticmethod
    def _serialize(value):
        if value is None:
            return ''
        if isinstance(value, datetime):
            return localtime(value).replace(tzinfo=None)
        if isinstance(value, DateRange):
            return "-".join([
                value.lower.strftime('%d.%m.%Y') if value.lower else "",
                value.upper.strftime('%d.%m.%Y') if value.upper else "",
            ])
        if isinstance(value, list):
            return ", ".join(str(v) for v in value)
        if isinstance(value, Model):
            return str(value)
        return value


class OpenXMLExport:
    default_font_size = 11

    def __init__(self, sheet_title=None, col_widths=None):
        DEFAULT_FONT.sz = self.default_font_size
        self.wb = Workbook()
        self.ws = self.wb.active
        if sheet_title:
            self.ws.title = sheet_title
        self.col_widths = col_widths
        self.bold = Font(name='Calibri', bold=True, sz=self.default_font_size)
        self.row_idx = 1

    def write_line(self, values):
        """values is either a simple list() or an ExpList object."""
        if isinstance(values, list):
            values = ExpLine(values)
        col_widths = values.col_widths or self.col_widths
        if values.borders == 'all':
            border_style = Border(
                left=Side(style='thin', color='FF000000'), right=Side(style='thin', color='FF000000'),
                top=Side(style='thin', color='FF000000'), bottom=Side(style='thin', color='FF000000')
            )
        else:
            border_style = None
        max_lines = 1
        for col_idx, value in enumerate(values, start=1):
            cell = self.ws.cell(row=self.row_idx, column=col_idx)
            if isinstance(value, timedelta):
                cell.number_format = '[h]:mm;@'
            if isinstance(value, date):
                cell.number_format = 'dd.mm.yyyy;@'
            if col_idx in values.number_formats:
                cell.number_format = values.number_formats[col_idx]
            cell.value = value
            if values.bold and (values.bold[0] == "__all__" or col_idx in values.bold):
                cell.font = self.bold
            if values.alignment:
                cell.alignment = values.alignment
            if border_style:
                cell.border = border_style
            if col_widths and len(col_widths) >= col_idx:
                self.ws.column_dimensions[get_column_letter(col_idx)].width = col_widths[col_idx - 1]
            if isinstance(value, str):
                max_lines = max(max_lines, value.count("\n") + 1)
        if values.height:
            self.ws.row_dimensions[self.row_idx].height = values.height
        elif max_lines > 1:
            self.ws.row_dimensions[self.row_idx].height = 14 * max_lines
        self.row_idx += 1

    def fill_data(self, generator):
        for row in generator:
            self.write_line(row)

    def add_sheet(self, title):
        self.wb.create_sheet(title)
        self.ws = self.wb[title]
        self.row_idx = 1

    def get_http_response(self, filename):
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type=openxml_contenttype)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response


class CSVExport:
    def __init__(self, **kwargs):
        self.buff = StringIO()
        self.writer = csv.writer(self.buff, **kwargs)

    def write_line(self, values, **kwargs):
        self.writer.writerow(values)

    def get_http_response(self, filename):
        assert filename.endswith(".csv")
        response = HttpResponse(content=self.buff.getvalue(), content_type='text/csv', charset='cp1252')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response
