import json
import shutil
import subprocess
import tempfile
from datetime import date
from io import BytesIO
from pathlib import Path
from unittest import mock

from openpyxl import load_workbook
from selenium import webdriver
from selenium.webdriver.common.by import By

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test.testcases import LiveServerThread

from benevole.models import Benevole, TypeActivite, TypeFrais
from common.models import Utilisateur
from common.utils import canton_app

MOCKED_OSRM_DURATION = 1100.3
MOCKED_OSRM_DISTANCE = 14141.2


class TempMediaRootMixin:
    @classmethod
    def setUpClass(cls):
        cls._temp_media = tempfile.mkdtemp()
        cls._overridden_settings = cls._overridden_settings or {}
        cls._overridden_settings['MEDIA_ROOT'] = Path(cls._temp_media)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls._temp_media)
        super().tearDownClass()


class MockResponse:
    def __init__(self, data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data

    @property
    def text(self):
        return self.data

    @property
    def content(self):
        return json.dumps(self.data).encode('utf-8')

    @property
    def is_success(self):
        return 200 <= self.status_code <= 299


response_by_detected_id = {
    98765: {"error": {
        "code": 400, "message": 'Field "journal" is required in JSON dict and must have a value'}
    },
    515151: {"error": {
        "code": 400,
        "message": (
            "The invoice with ID xxxxx is in 'paid' state ! The 'open' state of the "
            "invoice is required for this action"
        )}
    },
}

def mocked_httpx(url, **kwargs):
    """Mock a httpx GET or POST request."""
    data = None
    if url.endswith('/oauth2/token'):
        data = {'access_token': 'abcdef'}
    elif (benef_id := kwargs.get("json", {}).get("beneficiaryid", "")) in response_by_detected_id:
        return MockResponse(response_by_detected_id[benef_id], 200)
    elif "invoices/515151/status" in url:
        return MockResponse(response_by_detected_id[515151], 200)
    elif 'invoices' in url or 'expenses' in url or '/partners' in url:  # Call to CID
        data = {'id': 1234}
        if "invoices" in url:
            data["number"] = "F510/2025/0249"
    elif 'tel.search.ch' in url:
        sample_response = Path('transport/tests/telsearch-api-response.xml')
        data = sample_response.read_text()
    elif 'geo.admin.ch' in url:
        if 'Introuvable' in url:
            data = {'results': None}
        else:
            data = {'results': [{'attrs': {'lon': 1, 'lat': 2}}]}
    elif 'router.project-osrm.org' in url:
        data = {"code": "Ok", "routes": [{
            'duration': MOCKED_OSRM_DURATION, 'distance': MOCKED_OSRM_DISTANCE
        }]}
    return MockResponse(data, 200)


class CustomLiveServerThread(LiveServerThread):
    """Custom server thread to avoid httpx network calls during tests."""
    def run(self):
        with mock.patch('httpx.get', side_effect=mocked_httpx):
            super().run()


class LiveTestBase(TempMediaRootMixin, StaticLiveServerTestCase):
    server_thread_class = CustomLiveServerThread
    #driver = 'firefox'
    driver = 'chromium'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if cls.driver == 'firefox':
            cls.selenium = webdriver.Firefox()
            # TODO: try to detect date format from browser
            cls.date_fmt = "%d.%m.%Y"
        elif cls.driver == 'chromium':
            cls.selenium = webdriver.Chrome()
            cls.date_fmt = "%d/%m/%Y"
        cls.selenium.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    @classmethod
    def setUpTestData(cls):
        pass

    def goto(self, url):
        self.selenium.get(f'{self.live_server_url}{url}')

    def login(self):
        self.goto(reverse('login'))
        username_input = self.selenium.find_element(By.NAME, value="auth-username")
        username_input.send_keys(self.user.email)
        password_input = self.selenium.find_element(By.NAME, value="auth-password")
        password_input.send_keys('mepassword')
        self.selenium.find_element(By.XPATH, value='//button[@type="submit"]').click()

    def find_and_scroll_to(self, *args):
        element = self.selenium.find_element(*args)
        # scroll_to_element is buggy until Firefox 106 (https://github.com/SeleniumHQ/selenium/issues/11059)
        #ActionChains(self.selenium).scroll_to_element(element).perform()
        self.selenium.execute_script("window.scrollTo({left: 0, top: document.body.scrollHeight, behavior: 'instant'})")
        return element


class BaseDataMixin:
    """Data that should be available in every test class."""
    @classmethod
    def setUpTestData(cls):
        try:
            super().setUpTestData()
        except AttributeError:
            pass
        if canton_app.abrev == "NE":
            TypeFrais.objects.bulk_create([
                TypeFrais(
                    no='NF-B01', libelle="Indemnités kilométriques", services=['alarme', 'visite'],
                    montant_fixe=True
                ),
                TypeFrais(
                    no='NF-B05', libelle="Transports publics", services=['visite'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='NF-B06', libelle="Frais de parking", services=['alarme', 'transport', 'visite'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='NF-B07', libelle="Frais de repas", services=['alarme', 'transport'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='NF-B10', libelle="Indemnités kilométriques (<=6000km)", services=['transport'],
                    montant_fixe=True
                ),
                TypeFrais(
                    no='NF-B11', libelle="Indemnités kilométriques (>6000km)",  services=['transport'],
                    montant_fixe=True
                ),
                TypeFrais(
                    no='NF-B12', libelle="Forfait temps d’attente chauffeurs", services=['transport'],
                    montant_fixe=True
                ),
                TypeFrais(
                    no='NF-B13', libelle="Transport de plusieurs personnes", services=['transport'],
                    montant_fixe=True
                ),
                TypeFrais(no='NF-B51', libelle="Forfait téléphone", services=['alarme'], montant_fixe=True),
            ], ignore_conflicts=True)
        elif canton_app.abrev == "JU":
            TypeFrais.objects.bulk_create([
                TypeFrais(
                    no='B01', libelle="Indemnités kilométriques", services=['alarme', 'transport'],
                    montant_fixe=True
                ),
                TypeFrais(
                    no='B02', libelle="Indemnités kilométriques avec véhicule CR", services=['transport'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='B06', libelle="Frais divers", services=['alarme', 'transport'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='B07', libelle="Frais de repas", services=['alarme', 'transport'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no='B08', libelle="Forfait de temps d’attente", services=['transport'],
                    montant_fixe=False
                ),
                TypeFrais(
                    no="B10", libelle="Forfait de visite", services=["alarme"],
                    montant_fixe=True
                ),
            ], ignore_conflicts=True)

        TypeActivite.objects.bulk_create([
            TypeActivite(code=TypeActivite.TRANSPORT, nom='Transports', service='transport'),
            TypeActivite(code=TypeActivite.INSTALLATION, nom='Alarme (installation)', service='alarme'),
            TypeActivite(code=TypeActivite.VISITE_ALARME, nom='Alarme (visite)', service='alarme'),
            TypeActivite(code='visites', nom='Visites', service='visite'),
            TypeActivite(code='amf', nom='AMF'),
        ], ignore_conflicts=True)  # Une migration peut les avoir déjà créés.
        TypeActivite.service_map.cache_clear()

    @classmethod
    def _create_benevole(cls, avec_utilisateur=False, **kwargs):
        defaults = {
            "nom": "Alarme",
            "prenom": "Marco",
            "genre": "m",
            "date_naissance": date(1950, 4, 2),
            "langues": ["fr"],
            #("courriel", ""), ("rue", ""), ("npa", ""), ("localite", ""), ("empl_geo", None), ("id_externe", None)
        }
        defaults.update(**kwargs)
        defaults.setdefault("activites", [TypeActivite.INSTALLATION])
        if avec_utilisateur:
            defaults["utilisateur"] = Utilisateur.objects.create_user(
                "benev@example.org", "pwd", first_name="Marco", last_name="Alarme",
            )
        return Benevole.objects.create(**defaults)


def read_xlsx(resp_content):
    wb = load_workbook(BytesIO(resp_content), read_only=True)
    ws = wb.active
    result = []
    for line in ws.rows:
        result.append([cell.value for cell in line])
    return result


def read_response_pdf_text(response):
    with tempfile.NamedTemporaryFile(delete=True, mode='wb') as fh:
        if hasattr(response, 'content'):
            fh.write(response.content)
        else:
            fh.write(b"".join(response.streaming_content))
        subprocess.run(['pdftotext', fh.name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        with open(f'{fh.name}.txt', 'r') as f:
            content = f.read()
    return content
