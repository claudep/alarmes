import json
from math import sqrt

import httpx
from openrouteservice import Client, convert
from openrouteservice.exceptions import ApiError, HTTPError, Timeout as ORSTimeout
from requests.exceptions import ConnectionError

from django.conf import settings
from django.contrib.gis.gdal import CoordTransform, OGRGeometry, SpatialReference
from django.core.cache import cache

ctrans = CoordTransform(SpatialReference("4326"), SpatialReference("2056"))


class ORSUnavailable(Exception):
    pass


def get_point_from_address(street, city, retried=False):
    """
    Return longitude/latitude (srid 4326) or None.
    """
    if not street or not city:
        return None
    url = (
        'https://api3.geo.admin.ch/rest/services/api/SearchServer?'
        'type=locations&origins=address&searchText={}'
    ).format(', '.join([street, city]))
    try:
        try:
            response = httpx.get(url, timeout=5)
        except httpx.TimeoutException:
            return None
        if response.status_code != 200:
            return None
        content = json.loads(response.content.decode('utf-8'))
        results = content['results']
        if not results:
            if street[-1].isalpha() and not retried:
                # Sometimes the final street lettre is preventing the match
                # https://gitlab.com/croixrouge/transports/-/issues/81
                return get_point_from_address(street[:-1], city, retried=True)
            return None
        longitude = results[0]['attrs']['lon']
        latitude = results[0]['attrs']['lat']
        return longitude, latitude
    except Exception:
        if settings.TESTS_RUNNING and not hasattr(httpx.get, 'return_value'):
            raise Exception("Unmocked call to httpx.get")
        return None


def wgs84_to_mn95(lon, lat):
    pt = OGRGeometry(f"POINT ({lon} {lat})", 4326)
    pt.transform(ctrans)
    return pt


def mn95_to_wgs84(lon, lat):
    # Defined here because rarely used
    ctrans = CoordTransform(SpatialReference("2056"), SpatialReference("4326"))
    pt = OGRGeometry(f"POINT ({lon} {lat})", 2056)
    pt.transform(ctrans)
    return pt


def distance_vo(empl1, empl2, arrondi=1):
    """Calcul de distance à vol d’oiseau entre deux points, en km à 1 décimale."""
    if not empl1 or not empl2:
        return None
    if empl1 == empl2:
        return 0
    pt1 = wgs84_to_mn95(*empl1)
    pt2 = wgs84_to_mn95(*empl2)
    return round(sqrt(abs(pt1.x - pt2.x) ** 2 + abs(pt1.y - pt2.y) ** 2) / 1000, arrondi)


def distance_real(empl1, empl2, only_cache=False, with_geom=False, figee=True):
    """
    Distance query on https://openrouteservice.org/ (limit 2000/day, 40/min).
    Other possible distance APIs:
    - https://distancematrix.ai/
    - https://github.com/Project-OSRM/osrm-backend/wiki/Api-usage-policy
    If `only_cache` is True, return None if value not in the cache.

    Return a dict: {'distance': <distance in meters>, 'duration': <duration in secs>}
    If a fixed distance was set in db, returns:
        {'distance': <fixed distance [m]>, 'distance_calc': <distance from service [m]>, 'duration': <duration in secs>}
    """
    if not empl1 or not empl2:
        return None
    if empl1 == empl2:
        return {'distance': 0, 'duration': 0}
    cache_key = str(hash(tuple(empl1) + tuple(empl2) + (with_geom,)))
    cached = cache.get(cache_key)
    if cached:
        return json.loads(cached)
    elif only_cache:
        return None

    distance_figee = None
    if figee:
        from common.models import DistanceFigee

        dist_obj = DistanceFigee.get_from_coords(empl1, empl2)
        if dist_obj is not None:
            distance_figee = int(dist_obj.distance * 1000)

    coords = (empl1, empl2)
    try:
        client = Client(key=settings.OPENROUTE_API_KEY, timeout=10, retry_timeout=15)
        routes = client.directions(coords, geometry=with_geom, instructions=False)
    except (httpx.HTTPError, ApiError, ConnectionError, HTTPError, ORSTimeout):
        if settings.TESTS_RUNNING and not hasattr(Client, 'return_value'):
            raise Exception("Unmocked call to openrouteservice.Client")
        routes = osrm_fallback(coords)
        if routes is None:
            raise ORSUnavailable()
        result = routes
    else:
        # e.g. {'distance': 30524.4, 'duration': 2174.0}
        result = routes['routes'][0]['summary']
    if with_geom and "routes" in routes:
        polyline = convert.decode_polyline(routes['routes'][0]['geometry'])
        # Inverser les coordonnées long-lat en lat-long
        result['linestring'] = [[pt[1], pt[0]] for pt in polyline['coordinates']]
    if distance_figee:
        result['distance_calc'] = result['distance']
        result['distance'] = distance_figee
    if figee:
        cache.set(cache_key, json.dumps(result), 30 * 60)
    return result


def osrm_fallback(coords):
    """
    Service de secours, voir https://github.com/Project-OSRM/osrm-backend/wiki/Demo-server
    """
    coords_str = ";".join([
        ",".join(str(round(c, 6)) for c in coords[0]),
        ",".join(str(round(c, 6)) for c in coords[1]),
    ])
    try:
        resp = httpx.get(
            f"http://router.project-osrm.org/route/v1/driving/{coords_str}?overview=false",
            timeout=8
        )
    except httpx.TimeoutException:
        return None
    if resp.status_code != httpx.codes.OK:
        return None
    return resp.json()["routes"][0]
