from ipaddress import IPv4Network

from django.core.management.commands.test import Command as TestCommand
from django.test.utils import override_settings


class Command(TestCommand):
    def handle(self, *args, **options):
        with override_settings(
            PASSWORD_HASHERS=['django.contrib.auth.hashers.MD5PasswordHasher'],
            EXEMPT_2FA_NETWORKS=[IPv4Network('127.0.0.0/30')],
            TESTS_RUNNING=True,
        ):
            return super().handle(*args, **options)
