from datetime import date, timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Count, Max, Q
from django.utils.timezone import now

from benevole.models import Benevole
from client.models import Client, Journal, client_saved


class Command(BaseCommand):
    """
    Archivage automatique des clients alarme après un certain temps sans appareil actif.
    Généralement exécuté par un job cron toutes les semaines, ou tous les jours
    si le signal envoyé par nouvelles_adresses_clients est important pour l'instance.
    """
    def handle(self, **options):
        self.nouvelles_adresses_clients(options['verbosity'])
        self.archiver_clients(options['verbosity'])
        self.archiver_benevoles(options['verbosity'])

    def nouvelles_adresses_clients(self, verbosity):
        """Envoi du signal client_saved si une nouvelle adresse est valable dès aujourd'hui."""
        nouv_adresses = Client.objects.avec_adresse(date.today()).filter(
            adresse_active__valide_de=date.today().strftime('%Y-%m-%d')
        )
        for client in nouv_adresses:
            client_saved.send(sender=Client, instance=client, created=False, force=True)
        if verbosity > 1 and len(nouv_adresses):
            self.stdout.write(
                self.style.SUCCESS(f"Envoi notif. adresse modifiée pour {len(nouv_adresses)} client(s)")
            )

    def archiver_clients(self, verbosity):
        """Archiver ARCHIVER_CLIENTS_APRES jours après la fin de la dernière prestation."""
        to_archive = Client.objects.annotate(
            num_prests_active=Count('prestations', filter=Q(prestations__duree__endswith=None)),
            num_prests=Count('prestations'),
            last_dategap=date.today() - Max('prestations__duree__endswith'),
        ).filter(
            archive_le__isnull=True, num_prests__gt=0, num_prests_active=0,
            last_dategap__gte=timedelta(days=settings.ARCHIVER_CLIENTS_APRES)
        )
        for client in to_archive:
            client._archiver_fort(user=None, message="archivage automatique")
        if verbosity > 1:
            self.stdout.write(
                self.style.SUCCESS(f"{len(to_archive)} client(s) archivé(s)")
            )

    def archiver_benevoles(self, verbosity):
        to_archive = Benevole.objects.annotate(
            act_actives=Count('activite', filter=Q(activite__duree__contains=date.today()))
        ).filter(archive_le__isnull=True, act_actives=0)
        for benev in to_archive:
            with transaction.atomic():
                benev.archiver()
                Journal.objects.create(
                    benevole=benev.persona, description="Plus d’activité en cours, archivage du bénévole.",
                    quand=now(), qui=None
                )
        if verbosity > 1 and len(to_archive) > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{len(to_archive)} bénévole(s) archivé(s)")
            )
