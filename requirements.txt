django~=5.1
psycopg
phonenumberslite  # required by django-two-factor-auth
django_otp==1.5.1
django-two-factor-auth==1.17.0
python-dateutil==2.8.2
openpyxl==3.1.5
reportlab==4.2.5
workalendar==17.0.0
httpx==0.27.0
openrouteservice==2.3.3
django-leaflet==0.31.0
django-tinymce==4.1.0
django-admin-sortable2==2.2.2
# Incoming mail read:
imapclient
pypdf==3.17.4
pdf2image==1.17.0
beautifulsoup4
# transports-only
feedparser
svglib
qrbill==1.1.0
defusedxml==0.7.1
xmltodict
