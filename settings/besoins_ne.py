from .base import *

WSGI_APPLICATION = 'common.wsgi_besoins.application'

ROOT_URLCONF = 'besoins.urls'

DATABASES['default']['NAME'] = 'alarmes_ne'

STATIC_ROOT = BASE_DIR / 'static' / 'besoins'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    CANTON_APP,
    'besoins',
])

MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = ''#img/visite.svg'
