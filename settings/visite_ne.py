from .base import *

WSGI_APPLICATION = 'common.wsgi_visite.application'

ROOT_URLCONF = 'visite.urls'

DATABASES['default']['NAME'] = 'alarmes_ne'

STATIC_ROOT = BASE_DIR / 'static' / 'visite'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    'visite',
    'besoins',
    CANTON_APP,
    'api',
])
TEMPLATES[0]['OPTIONS']['context_processors'].append('visite.context_processors.visite')

MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = 'logos/visite.svg'

NOTESFRAIS_TRANSMISSION_URLNAME = 'notesfrais-transmission'
