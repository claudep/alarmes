from .base import *

WSGI_APPLICATION = 'common.wsgi_crm.application'

ROOT_URLCONF = 'crm.urls'

STATIC_ROOT = BASE_DIR / 'static' / 'crm'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    'crm',
    CANTON_APP,
    'api',
])
DATABASES['default']['NAME'] = 'alarmes_ne'
DEFAULT_FROM_EMAIL = 'contact@croix-rouge-ne.ch'

MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = ''

CID_SYNC_ENABLE = []
CID_API_URL = ""
CID_API_TOKEN = ""
