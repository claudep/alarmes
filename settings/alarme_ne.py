from .base import *

from django.urls import reverse_lazy

WSGI_APPLICATION = 'common.wsgi_alarme.application'

ROOT_URLCONF = 'alarme.urls'

# To ensure transport static files are still collected when alarme is deployed!
STATICFILES_DIRS = [
    BASE_DIR / 'transport/static',
]
STATIC_ROOT = BASE_DIR / 'static' / 'alarme'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    'alarme',
    'ape',
    CANTON_APP,
    'api',
    'besoins',
])
DATABASES['default']['NAME'] = 'alarmes_ne'
DEFAULT_FROM_EMAIL = 'contact@croix-rouge-ne.ch'

ALARMPOST_EMAIL = 'alarme-ne@2xlibre.net'
ALARMPOST_EMAIL_HOST = 'mail.2xlibre.net'
ALARMPOST_EMAIL_PASSWORD = ''

MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = 'logos/logo-alarme.svg'

FACTURES_TRANSMISSION_URL = reverse_lazy('factures-transmission', args=['alarme'])
NOTESFRAIS_TRANSMISSION_URLNAME = 'notesfrais-transmission'

CODE_ARTICLE_INTERV_SAMA = '52.501'
CODE_ARTICLE_ABO_SAMA = '52.500'

CID_SYNC_ENABLE = []
CID_API_URL = ""
CID_API_TOKEN = ""
