import dj_database_url
import os

from settings.visite_ne import *  # NOQA


ALLOWED_HOSTS = os.environ["ALLOWED_HOSTS"].split(",")
DATABASES = {"default": dj_database_url.parse(os.environ["DATABASE_URL"])}
EXEMPT_2FA_NETWORKS = [IPv4Network('127.0.0.0/30'), IPv4Network('192.168.0.0/16')]
DEBUG = True
SECRET_KEY = "no secret here"
