from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from .base import *

WSGI_APPLICATION = 'common.wsgi_transport.application'

DATABASES['default']['NAME'] = 'alarmes_ju'
DATABASES['default']['TEST'] = {'NAME': 'test_transport_ju'}

ROOT_URLCONF = 'transport.urls'

STATIC_ROOT = BASE_DIR / 'static' / 'transport'

CANTON_APP = 'cr_ju'
INSTALLED_APPS.extend([
    CANTON_APP,
    'transport',
    'besoins',
])

SERVER_EMAIL = 'technique@croix-rouge-jura.ch'
DEFAULT_FROM_EMAIL = 'technique@croix-rouge-jura.ch'
EMAIL_HOST = 'mails.croix-rouge-jura.ch'

MAIN_LOGO = 'img/logo-cr-ju.svg'
APP_LOGO = 'logos/transport.svg'

LIBELLE_MAUVAIS_PAYEUR = "Mauvais payeur"
FACTURES_TRANSMISSION_URL = reverse_lazy('factures-transport-export')
