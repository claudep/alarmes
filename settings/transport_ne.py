from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from .base import *

WSGI_APPLICATION = 'common.wsgi_transport.application'

DATABASES['default']['NAME'] = 'alarmes_ne'
DATABASES['default']['TEST'] = {'NAME': 'test_transport_ne'}

ROOT_URLCONF = 'transport.urls'

STATIC_ROOT = BASE_DIR / 'static' / 'transport'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    CANTON_APP,
    'transport',
    'api',
    'besoins',
])
DEFAULT_FROM_EMAIL = 'transports@croix-rouge-ne.ch'

CID_SYNC_ENABLE = []
CID_API_URL = ""
CID_API_TOKEN = ""

FACTURES_TRANSMISSION_URL = reverse_lazy('factures-transmission', args=['transport'])
NOTESFRAIS_TRANSMISSION_URLNAME = 'notesfrais-transmission'

MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = 'logos/transport.svg'

LIBELLE_MAUVAIS_PAYEUR = "Fonds Transport handicap"
TAUX_TVA = Decimal('0.081')
FACTURE_IBAN = "CH41 3000 0001 2000 1504 8"
SEARCH_CH_API_KEY = ''
