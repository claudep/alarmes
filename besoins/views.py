from datetime import date, timedelta

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.postgres.aggregates import ArrayAgg
from django.db import transaction
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import (
    CharField, Count, F, Max, OuterRef, Q, Subquery, Sum, Value as V,
)
from django.db.models.functions import Concat, TruncMonth
from django.db.models.lookups import LessThan
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView, UpdateView, View

from client.models import Alerte, Client, Journal
from client.views import ClientAccessCheckMixin, ClientDetailContextMixin, ClientListViewBase
from common.choices import Services
from common.export import ExpLine, OpenXMLExport
from common.forms import AnneeForm
from common.stat_utils import Month
from common.utils import canton_app, client_url_name, current_app
from common.views import FilterFormMixin, ListView

from .models import Question, Questionnaire, Ressource, Section, clients_besoins
from . import forms


class HomeView(FilterFormMixin, PermissionRequiredMixin, ListView):
    template_name = 'besoins/home.html'
    permission_required = 'besoins.view_questionnaire'
    filter_formclass = forms.FilterClientForm
    paginate_by = 20

    def get_queryset(self, base_qs=None):
        ordering = [F('last_quest').asc(nulls_first=True)] + canton_app.BESOINS_CLIENT_ORDERING
        base_qs = Questionnaire.clients_a_questionner().order_by(*ordering)
        return super().get_queryset(base_qs=base_qs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cette_annee = date.today().year
        context.update({
            'en_cours': Questionnaire.objects.filter(
                termine_le__isnull=True, client__archive_le__isnull=True,
            ).select_related('client').order_by('client__persona__nom'),
            'cette_annee': cette_annee,
            'quests_restant': context['paginator'].count,
        })
        return context


class ClientListView(ClientListViewBase):
    template_name = 'besoins/client_list.html'
    return_all_if_unbound = False

    def get_queryset(self):
        base_qs = clients_besoins().avec_adresse(date.today()).annotate(
            last_quest=Max('questionnaire__created'),
        ).order_by('persona__nom', 'persona__prenom')
        return super(ClientListViewBase, self).get_queryset(base_qs=base_qs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "besoins/base.html",
            'home_url': reverse('home'),
            'client_url_name': client_url_name(),
        }


class QuestionnaireView(ClientAccessCheckMixin, UpdateView):
    """Page de questionnaire, réutilisée pour différentes pages du questionnaire."""
    template_name = 'besoins/q-intro.html'
    model = Questionnaire
    pk_url_kwarg = 'qpk'
    section = None

    def dispatch(self, *args, **kwargs):
        self.section_key = self.kwargs['section']
        self.section = (
            get_object_or_404(Section, code=self.section_key) if self.section_key != 'intro' else None
        )
        sect_keys = ['intro'] + list(Section.objects.all().values_list('code', flat=True))
        if self.section_key == sect_keys[0]:
            self.previous_section_key = None
        else:
            self.previous_section_key = sect_keys[sect_keys.index(self.section_key) - 1]
        if self.section_key == sect_keys[-1]:
            self.next_section_key = None
        else:
            self.next_section_key = sect_keys[sect_keys.index(self.section_key) + 1]
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_template_names(self):
        return ['besoins/q-intro.html'] if self.section_key == 'intro' else ['besoins/q-generic.html']

    def get_object(self):
        if 'qpk' not in self.kwargs:
            # Si le questionneur revient sur quest/intro avec le Back du navigateur et
            # resoumet, il faut reprendre ici le questionnaire en cours, le cas échéant
            try:
                return self.client.questionnaire_set.filter(termine_le__isnull=True).first()
            except Questionnaire.DoesNotExist:
                return None
        return super().get_object()

    def get_form_class(self):
        if self.section_key == 'intro':
            return forms.IntroForm
        return self.section.get_form_class(self.object)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sections = Questionnaire.sections
        themes_status = {'intro': {'label': "Introduction", 'done': True}}
        themes_status.update({
            section.code: {
                'label': section.theme,
                'done': self.object.theme_ok(section) if self.object else False,
            }
            for section in Section.objects.all()
        })
        context.update({
            'client': self.client,
            'client_url_name': client_url_name(),
            'adresse': self.client.adresse(date.today()),
            'previous': (
                self.previous_section_key,
                "Introduction" if self.previous_section_key == "intro" else (
                    sections[self.previous_section_key].theme if self.previous_section_key else ''
                ),
            ),
            'next': (self.next_section_key, sections[self.next_section_key].theme if self.next_section_key else ''),
            'themes_status': themes_status,
            'questions': Question.objects.filter(section__code=self.section_key),
            'ressources': Ressource.objects.all(),
        })
        try:
            context['prev_quest'] = self.client.questionnaire_set.filter(termine_le__isnull=False).latest('termine_le')
        except Questionnaire.DoesNotExist:
            context['prev_quest'] = None
        context.update(self.client.contacts_as_context())
        return context

    def terminer(self, quest):
        quest.termine_le = timezone.now()
        msg = f"Le questionnaire pour {quest.client} a bien été terminé."
        with transaction.atomic():
            quest.save(update_fields=['termine_le'])
            Journal.objects.create(
                persona=quest.client.persona, description="Questionnaire des besoins rempli.",
                quand=timezone.now(), qui=self.request.user,
            )
            if quest.interet_alarme:
                if quest.client.prestations_actuelles(Services.ALARME):
                    raise ValueError("Le client est déjà client de l’alarme")
                Journal.objects.create(
                    persona=quest.client.persona,
                    description="Intérêt signifié pour le service alarme à la suite du questionnaire des besoins.",
                    quand=timezone.now(), qui=self.request.user,
                )
                msg = (
                    "Intérêt pour l’alarme suite au questionnaire des besoins: "
                    "merci de contacter cette personne."
                )
                if quest.chutes_details:
                    msg += f"\n\nRemarques concernant les craintes de chutes:\n{quest.chutes_details}"
                Alerte.objects.create(
                    persona=quest.client.persona, cible='alarme', alerte=msg,
                    recu_le=timezone.now(), par=self.request.user,
                )
                msg += " La personne a été signalée à l'équipe alarme."
            if quest.message_services:
                for prest in quest.client.prestations_actuelles():
                    if prest.service == current_app():
                        continue
                    Alerte.objects.create(
                        persona=quest.client.persona, cible=prest.service,
                        alerte=f"Info suite au questionnaire des besoins: «{quest.message_services}»",
                        recu_le=timezone.now(), par=self.request.user,
                    )
        messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('besoins:home'))

    def non_repondu(self, quest):
        quest.termine_le = timezone.now()
        msg = f"Le questionnaire pour {quest.client} a été marqué comme «non répondu»."
        with transaction.atomic():
            quest.save(update_fields=['termine_le'])
            Journal.objects.create(
                persona=quest.client.persona,
                description="Souhait de ne pas répondre au questionnaire des besoins.",
                quand=timezone.now(), qui=self.request.user,
            )
        messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('besoins:home'))

    def form_valid(self, form):
        if not form.instance.pk:
            form.instance.client = self.client
            form.instance.collab = self.request.user
        quest = form.save()
        if self.section_key == 'fin' and self.request.GET.get('termine') == '1':
            if quest.quest_ok():
                return self.terminer(quest)
            messages.error(
                self.request,
                "Désolé, il reste des questions ou des champs à compléter pour "
                "pouvoir terminer le questionnaire"
            )
        if self.section_key == 'intro' and quest.non_repondu:
            return self.non_repondu(quest)
        if self.section_key == 'intro' and self.request.GET.get('rappel') == '1':
            return HttpResponseRedirect(reverse('besoins:home'))
        if self.request.headers.get('x-requested-with') == 'fetch':
            return JsonResponse({'result': 'OK'})
        return HttpResponseRedirect(reverse('besoins:questionnaire', args=[
            self.client.pk, form.instance.pk,
            self.previous_section_key if self.request.GET.get('back') == '1' else (
                self.next_section_key or self.section_key
            )
        ]))


class QuestionnaireResumeView(ClientAccessCheckMixin, ClientDetailContextMixin, TemplateView):
    template_name = 'besoins/onglet_client.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'questionnaires': self.client.questionnaire_set.order_by('-created'),
        }


class QuestListBase(ListView):
    model = Questionnaire
    template_name = 'besoins/quest_list.html'

    def get(self, request, *args, **kwargs):
        self.service = self.request.GET.get('service', 'none')
        self.year = self.request.GET.get('year', date.today().year)
        self.month = self.request.GET.get('month', date.today().month)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        return super().get_context_data(**context) | {
            'note': (
                "* La colonne Services contient l'état actuel des prestations du "
                "client, ce qui ne correspond pas toujours à la situation au "
                "moment du questionnaire."
            ),
        }


class QuestListView(QuestListBase):

    def title(self):
        return f"Questionnaires de {self.month}.{self.year} du service «{self.service}»"

    def get_queryset(self):
        date_actif=(date(int(self.year), 1, 1), date(int(self.year), 12, 31))
        return Questionnaire.objects.annotate(
            services=ArrayAgg('client__prestations__service', filter=Q(
                client__prestations__duree__overlap=DateRange(*date_actif)), default=[]
            ),
        ).filter(
            services__contains=[self.service],
            termine_le__year=self.year, termine_le__month=self.month, non_repondu=''
        )


class QuestNonRepondusView(QuestListBase):
    """Liste des clients qui n’ont pas répondu au questionnaire."""

    def title(self):
        return f"Questionnaires non répondus - {self.month}.{self.year}"

    def get_queryset(self):
        return Questionnaire.objects.filter(
            termine_le__year=self.year, termine_le__month=self.month,
        ).exclude(non_repondu='')


class QuestExportView(PermissionRequiredMixin, View):
    permission_required = 'besoins.view_questionnaire'

    def get(self, request, *args, **kwargs):
        export = OpenXMLExport(col_widths=[16, 28, 28, 10])
        export.fill_data(self.export_quests(kwargs['year']))
        return export.get_http_response(f"questionnaires_{kwargs['year']}.xlsx")

    def export_quests(self, year):
        yield ExpLine(['Date du questionnaire', 'Client', 'Collaborateur questionneur', 'Durée'], bold=True)
        for quest in Questionnaire.objects.filter(termine_le__year=year, non_repondu='').annotate(
            nom_client=Concat("client__persona__nom", V(" "), "client__persona__prenom"),
            nom_collab=Concat("collab__last_name", V(" "), "collab__first_name"),
        ).values_list(
            'termine_le__date', 'nom_client', 'nom_collab', 'duree',
        ).order_by('termine_le'):
            yield ExpLine(quest)


class StatistiquesBaseView(TemplateView):
    form_class = AnneeForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        annee_form = self.form_class(year_start=2023, data=self.request.GET or {'year': date.today().year})
        context['date_form'] = annee_form
        if not annee_form.is_valid():
            return context
        self.year = int(annee_form.cleaned_data['year'])
        self.avec_osad = annee_form.cleaned_data.get('avec_osad')
        return context


class StatistiquesView(StatistiquesBaseView):
    template_name = 'besoins/stats.html'
    form_class = forms.StatsForm
    active = 'general'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        subq = Client.objects.avec_services(
            date_actif=(date(self.year, 1, 1), date(self.year, 12, 31)), date_debut=True
        ).filter(pk=OuterRef("client__pk"))
        query = Questionnaire.objects.filter(termine_le__year=self.year).annotate(
            month=TruncMonth('termine_le'),
            services=Subquery(subq.values('services')[:1]),
            date_debut=Subquery(subq.values('date_debut')[:1]),
        )
        if not self.avec_osad:
            query = query.exclude(services=[Services.OSAD])
        query = query.values('month').annotate(
            num_alarme=Count('id', filter=Q(
                non_repondu='', services__contains=[Services.ALARME]
            )),
            num_transport=Count('id', filter=Q(
                non_repondu='', services__contains=[Services.TRANSPORT]
            )),
            num_visite=Count('id', filter=Q(
                non_repondu='', services__contains=[Services.VISITE]
            )),
            num_osad=Count('id', filter=Q(
                non_repondu='', services__contains=[Services.OSAD]
            )),
            num_total=Count('id', filter=Q(non_repondu='')),
            temps=Sum('duree'),
            non_repondus=Count('id', filter=~Q(non_repondu='')),
            num_nouveaux=Count('id', filter=Q(non_repondu='') & Q(
                LessThan(F('termine_le') - F('date_debut'), timedelta(days=365))
            )),
        )
        keys = [f'num_{serv[0]}' for serv in Services.choices_can] + [
            'non_repondus', 'num_nouveaux', 'num_total',
        ]
        totaux = {key: 0 for key in keys}
        totaux['temps'] = timedelta()
        for line in query:
            for key in keys:
                totaux[key] += line.get(key, 0)
            totaux['temps'] += line['temps'] or timedelta()
        if not self.avec_osad:
            totaux.pop('num_osad', None)
        context.update({
            'months': [Month(self.year, i + 1) for i in range(12)],
            'results': {Month.from_date(line['month']): line for line in query},
            'totaux': totaux,
        })
        return context


class StatistiquesParCollabView(StatistiquesBaseView):
    template_name = 'besoins/stats-collab.html'
    active = 'collab'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query = Questionnaire.objects.filter(termine_le__year=self.year, non_repondu='').annotate(
            month=TruncMonth('termine_le'),
            nom_collab=Concat("collab__last_name", V(" "), "collab__first_name", output_field=CharField()),
        ).values(
            'month', 'nom_collab'
        ).annotate(
            num_quest=Count('id'),
            duree_quest=Sum('duree'),
        ).order_by('nom_collab')
        collabs = {}
        for line in query:
            if line['nom_collab'] not in collabs:
                collabs[line['nom_collab']] = {'total_num': 0, 'total_duree': timedelta()}
            collabs[line['nom_collab']][Month.from_date(line['month'])] = {'num': line['num_quest'], 'duree': line['duree_quest']}
            collabs[line['nom_collab']]['total_num'] += line['num_quest']
            collabs[line['nom_collab']]['total_duree'] += line['duree_quest'] or timedelta()
        context.update({
            'months': [Month(self.year, i + 1) for i in range(12)],
            'collabs': collabs,
        })
        return context
