from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0010_ressource'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='non_repondu_new',
            field=models.CharField(blank=True, choices=[
                ('refus', 'Ne souhaite pas répondre au questionnaire (ne sera plus contacté·e avant 18 mois).'),
                ('injoign', 'Non joignable (pas de réponse, mauvais numéro, etc.)')
            ], max_length=10),
        ),
    ]
