from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0003_questionnaire_non_repondu'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='message_services',
            field=models.TextField(blank=True, verbose_name='Information pour les services'),
        ),
    ]
