from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='interet_alarme',
            field=models.BooleanField(default=False),
        ),
    ]
