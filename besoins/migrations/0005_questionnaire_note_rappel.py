from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0004_questionnaire_message_services'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='note_rappel',
            field=models.TextField(blank=True, verbose_name='Note interne sur le rappel du client'),
        ),
    ]
