from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0008_questionnaire_satisfaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='duree',
            field=models.DurationField(null=True, verbose_name='Temps passé sur le questionnaire'),
        ),
    ]
