from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0007_populate_sections_questions'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='satisfaction',
            field=models.SmallIntegerField(blank=True, choices=[(-1, 'Pas de réponse'), (1, 'Oui'), (2, 'Non')], null=True),
        ),
        migrations.AddField(
            model_name='questionnaire',
            name='satisfaction_details',
            field=models.TextField(blank=True),
        ),
    ]
