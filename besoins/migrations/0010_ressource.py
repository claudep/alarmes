from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0009_questionnaire_duree'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ressource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=100)),
                ('order', models.SmallIntegerField(default=0)),
                ('web', models.URLField(blank=True)),
                ('coordonnees', models.TextField(blank=True)),
                ('descriptif', models.TextField(blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
