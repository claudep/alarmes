from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0002_questionnaire_interet_alarme'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='non_repondu',
            field=models.BooleanField(default=False),
        ),
    ]
