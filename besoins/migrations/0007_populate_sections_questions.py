from django.db import migrations

structure = {
    #'intro': {'theme': "Introduction"},
    'environnement': {
        'theme': "Environnement familial et social",
        'buts': "détecter des souffrances en lien avec la solitude et proposer "
                "prestation , détection, prévention épuisement proche aidant",
        'champs': [
            {"question": "Ressentez-vous le besoin de contacts extérieurs ?", "field": "besoin_contacts"},
            {"question": "Souffrez-vous de solitude ?", "field": "solitude"},
            {"question": "Avez-vous de l’aide de la part d’un proche ?", "field": "proche_aidant",
             "compl": "Si oui, un ou plusieurs et quelle fréquence ?"},
        ]
    },
    'logement': dict(
        theme="Conditions de logement",
        buts="Identifier les besoins avec la vie quotidienne, identifier les besoins d’adaptation du logement",
        champs=[
            {"question": "Faites-vous vos courses vous-même ?", "field": "aide_courses", "compl": "Si non, qui vous aide ?"},
            {"question": "Est-ce que vous arrivez à préparer vos repas vous-même ?", "field": "repas_auto",
             "compl": "Si non, auriez-vous besoin d’aide ou en recevez-vous déjà ?"},
            {"question": "Entretenez-vous seul-e votre logement ?", "field": "entretien_logement",
             "compl": "Si non, auriez-vous besoin d’aide ou en recevez-vous déjà ?"},
            {"question": "Avez-vous des moyens auxiliaires ?", "field": "moyens_aux",
             "compl": "Si oui lesquels : intérieur/extérieur (orientation ergo/médecin)"},
            {"question": "Avez-vous déjà chuté ? Est-ce que cela génère une inquiétude ?", "field": "chutes"},
        ],
    ),
    'sante': dict(
        theme="État de santé",
        buts="Proposer des solutions pour soulager des douleurs, proposer tous types de soins",
        champs=[
            {"question": "Avez-vous besoin d’aide pour votre toilette ou d’autres soins ? "
                         "(soins de base, aide à l’habillage, bas de contention…)",
             "field": "aide_soins"},
        ],
    ),
    'socioeco': dict(
        theme="Conditions socio-économiques",
        buts="Détecter les besoins en soutien administratif, détecter la précarité financière",
        champs=[
            {"question": "Avez-vous besoin d’aide pour vos tâches administratives (paiements, déclaration d’impôts, etc.) ?",
             "field": "besoin_admin"},
        ],
    ),
    'fin': dict(theme="Résumé/fin de l’entretien", champs=[
        {"question": "Informations pour les autres services", "field": "message_services",
         "aide": (
            "Ce contenu sera transmis aux services concernés par ce client sous "
            "forme d’alertes. Typiquement utilisé pour communiquer une modification "
            "de la situation d’un client."
         )
        },
        {"question": "Autres remarques générales sur l’entretien", "field": "remarques"},
    ]),
}


def populate_sections_questions(apps, schema_editor):
    Section = apps.get_model('besoins', 'Section')
    Question = apps.get_model('besoins', 'Question')

    for idx, (code, data) in enumerate(structure.items(), start=1):
        section = Section.objects.create(
            code=code, position=idx, theme=data['theme'], buts=data.get('buts', '')
        )
        for idx_quest, quest in enumerate(data['champs'], start=1):
            Question.objects.create(
                section=section, position=idx_quest, question=quest['question'],
                question_compl=quest.get('compl', ''), field=quest['field'], aide=quest.get('aide', '')
            )


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0006_section_question'),
    ]

    operations = [migrations.RunPython(populate_sections_questions, migrations.RunPython.noop)]
