from django.db import migrations


def migrate_non_repondus(apps, schema_editor):
    Questionnaire = apps.get_model('besoins', 'Questionnaire')
    Questionnaire.objects.filter(non_repondu=True).update(non_repondu_new='refus')


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0011_questionnaire_non_repondu_new'),
    ]

    operations = [
        migrations.RunPython(migrate_non_repondus, migrations.RunPython.noop),
    ]
