from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0005_questionnaire_note_rappel'),
    ]

    operations = [
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SlugField(max_length=15)),
                ('theme', models.CharField(max_length=150, verbose_name='Thème')),
                ('buts', models.TextField(blank=True, verbose_name='Buts')),
                ('position', models.SmallIntegerField(default=0)),
            ],
            options={
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=200, verbose_name='Question principale')),
                ('question_compl', models.CharField(blank=True, max_length=200, verbose_name='Question complémentaire')),
                ('field', models.CharField(max_length=50)),
                ('aide', models.TextField(blank=True)),
                ('position', models.SmallIntegerField(default=0)),
                ('section', models.ForeignKey(on_delete=models.deletion.CASCADE, to='besoins.section', related_name="questions")),
            ],
            options={
                "ordering": ['section__position', 'position'],
            },
        ),
    ]
