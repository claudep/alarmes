from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0012_migrate_non_repondus'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='questionnaire',
            name='non_repondu',
        ),
        migrations.RenameField(
            model_name='questionnaire',
            old_name='non_repondu_new',
            new_name='non_repondu',
        ),
    ]
