from datetime import date, timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now

from client.models import Alerte, Client, Prestation
from common.choices import Services
from common.export import openxml_contenttype
from common.models import Utilisateur
from common.stat_utils import Month
from common.utils import current_app
from .models import Questionnaire


class BesoinsTests(TestCase):
    fixtures = ["quest_ne"]

    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_client', 'view_questionnaire', 'change_questionnaire',
        ])))

    def _assertPostRedirects(self, response, url, **kwargs):
        if response.status_code == 200 and not response.context['form'].is_valid():
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, url, **kwargs)

    def test_flux_lineaire(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:q-intro', args=[client.pk]))
        self.assertContains(response, "Contacts/répondants")
        response = self.client.post(
            reverse('besoins:q-intro', args=[client.pk]), data={'aide_entretien': ''}, follow=True
        )
        quest = client.questionnaire_set.first()
        self.assertEqual(response.context['view'].section_key, 'environnement')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'environnement']), data={
            'solitude': '2', 'solitude_details': '',
            'besoin_contacts': '1', 'besoin_contacts_details': 'Souhaite plus de contacts',
            'proche_aidant': '2', 'proche_aidant_details': '',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'logement')
        self.assertContains(response, '<div><b>Thème:</b> Conditions de logement</div>', html=True)
        self.assertContains(
            response,
            '<div><b>Buts:</b> Identifier les besoins avec la vie quotidienne, identifier '
            'les besoins d’adaptation du logement</div>',
            html=True
        )
        self.assertContains(response, "Si non, qui vous aide ?")
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'logement']), data={
            'aide_courses': '1', 'aide_courses_details': 'Peine à trouver certaines choses',
            'repas_auto': '1', 'repas_auto_details': '',
            'entretien_logement': '1', 'entretien_logement_details': 'OK',
            'moyens_aux': '2', 'moyens_aux_details': '',
            'chutes': '2', 'chutes_details': '',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'sante')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'sante']), data={
            'aide_soins': '2', 'aide_soins_details': 'Pas de besoin',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'socioeco')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'socioeco']), data={
            'besoin_admin': '1', 'besoin_admin_details': 'Volontiers pour impôts.',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'fin')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'fin']) + '?termine=1', data={
            'satisfaction': '1', 'satisfaction_details': 'Énormément',
            'remarques': 'Proposé quelques services.',
            'duree': '17',
        })
        self._assertPostRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        quest.refresh_from_db()
        self.assertEqual(quest.termine_le.date(), date.today())
        self.assertEqual(quest.duree.total_seconds() // 60, 17)

    def test_resume(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME
        )
        quest = Questionnaire.objects.create(
            client=client, collab=self.user,
            solitude=1, besoin_contacts=2, besoin_contacts_details="Parfois oui",
            proche_aidant=-1,
        )
        self.assertEqual(len(list(quest.resume())), 10)
        quest.termine_le = now()
        quest.save()
        self.assertEqual(len(list(quest.resume())), 3)
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:resume', args=[str(client.pk)]))
        self.assertContains(response, f"Questionnaire du {date.today().strftime('%d.%m.%Y')}")

    def test_envoi_fetch_depuis_fin(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME
        )
        quest = Questionnaire.objects.create(client=client, collab=self.user)
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'fin']),
            data={
                'message_services': '', 'remarques': 'Blah', 'duree': '12',
            },
            HTTP_X_REQUESTED_WITH='fetch',
        )
        self.assertEqual(response.json(), {'result': 'OK'})
        quest.refresh_from_db()
        self.assertEqual(quest.remarques, "Blah")

    def test_interet_alarme(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64), service=Services.TRANSPORT
        )
        quest = Questionnaire.objects.create(
            client=jill, collab=self.user, interet_alarme=True, chutes_details="J'aimerais une montre alarme",
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('besoins:questionnaire', args=[jill.pk, quest.pk, 'logement'])
        )
        self.assertContains(
            response,
            '<input type="checkbox" name="interet_alarme" class="form-check-input" id="id_interet_alarme" checked>',
            html=True
        )
        response = self.client.post(
            reverse('besoins:questionnaire', args=[jill.pk, quest.pk, 'fin']) + '?termine=1',
            data={'satisfaction': '1', 'satisfaction_details': '', 'duree': '12', 'remarques': 'Proposé service alarme.'}
        )
        self._assertPostRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        jill.refresh_from_db()
        self.assertEqual([p.service for p in jill.prestations_actuelles()], ['transport'])
        self.assertEqual(
            jill.alertes.latest().alerte,
            "Intérêt pour l’alarme suite au questionnaire des besoins: merci de contacter cette personne.\n\n"
            "Remarques concernant les craintes de chutes:\nJ'aimerais une montre alarme"
        )

    def test_message_services(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64),
            service=[Services.ALARME, Services.TRANSPORT]
        )
        quest = Questionnaire.objects.create(
            client=jill, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:questionnaire', args=[jill.pk, quest.pk, 'fin']) + '?termine=1', data={
                'satisfaction': '1', 'satisfaction_details': '',
                'duree': '15', 'remarques': '', 'message_services': "Nouvelle adresse: rue des Fleurs 5"
            }
        )
        self._assertPostRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        num_alertes = 1 if current_app() in [Services.TRANSPORT, Services.ALARME] else 2
        self.assertEqual(jill.alertes.count(), num_alertes)
        self.assertEqual(
            jill.alertes.first().alerte,
            "Info suite au questionnaire des besoins: «Nouvelle adresse: rue des Fleurs 5»"
        )

    def test_note_rappel(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:q-intro', args=[client.pk]) + '?rappel=1',
            data={
                'aide_entretien': '',
                'non_repondu': '',
                'note_rappel': 'À rappeler dans 3 jours',
            },
            follow=True
        )
        self.assertRedirects(response, reverse('besoins:home'))
        quest = client.questionnaire_set.first()
        self.assertEqual(quest.note_rappel, 'À rappeler dans 3 jours')

    def test_non_reponse_refus(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('besoins:q-intro', args=[client.pk]), data={
            'aide_entretien': '',
            'non_repondu': Questionnaire.NonReponse.REFUSE,
            'remarques': "Pas le temps.",
        }, follow=True)
        quest = client.questionnaire_set.first()
        self.assertEqual(quest.termine_le.date(), date.today())
        self.assertEqual(quest.non_repondu, Questionnaire.NonReponse.REFUSE)
        self.assertEqual(quest.remarques, "Pas le temps.")
        response = self.client.get(reverse('besoins:home'))
        self.assertQuerySetEqual(response.context['object_list'], [])

    def test_quest_commence_puis_non_reponse(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', service=Services.ALARME
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:q-intro', args=[client.pk]), data={'aide_entretien': ''},
        )
        response = self.client.post(reverse('besoins:q-intro', args=[client.pk]), data={
            'aide_entretien': '',
            'non_repondu': Questionnaire.NonReponse.REFUSE,
            'remarques': "Pas le temps.",
        })
        self.assertEqual(client.questionnaire_set.count(), 1)

    def test_home(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365.25 * 65), service=Services.ALARME
        )
        # Client avec questionnaire non répondu de plus de 1½ ans
        clara = Client.objects.create(
            nom='Doe', prenom='Clara', genre='F',
            date_naissance=date.today() - timedelta(days=365.25 * 65), service=Services.ALARME
        )
        plus_de_1_an_et_demi = now() - timedelta(days=700)
        quest = Questionnaire.objects.create(
            client=clara, collab=self.user,
            non_repondu=Questionnaire.NonReponse.REFUSE, termine_le=plus_de_1_an_et_demi,
            remarques="Bla bla"
        )
        quest.created = plus_de_1_an_et_demi  # because of auto_now_add
        quest.save()
        # Client non AVS
        Client.objects.create(
            nom='Doe', prenom='John', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 64), service=Services.ALARME
        )
        # Client archivé, avec questionnnaire en cours
        cl_arch = Client.objects.create(
            nom='Doe', prenom='Hans', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 70), service=Services.ALARME,
            archive_le=date(2024, 1, 1),
        )
        Questionnaire.objects.create(client=cl_arch, collab=self.user)
        # Client "Mauvais payeur"
        Client.objects.create(
            nom='Doe', prenom='Jean', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 70), service=Services.ALARME,
            fonds_transport=True,
        )
        # Client avec quest non repondu depuis moins de 1½ ans
        client_non_rep = Client.objects.create(
            nom='Smith', prenom='Fred', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 80), service=Services.ALARME
        )
        moins_de_1_an_et_demi = now() - timedelta(days=420)
        quest = Questionnaire.objects.create(
            client=client_non_rep, collab=self.user,
            non_repondu=Questionnaire.NonReponse.REFUSE, termine_le=moins_de_1_an_et_demi,
            remarques="Bla bla"
        )
        quest.created = moins_de_1_an_et_demi  # because of auto_now_add
        quest.save()
        # Client sans plus aucun service actif (mais pas encore archivé)
        cl_inactif = Client.objects.create(
            nom='Old', prenom='Lily', genre='F',
            date_naissance=date.today() - timedelta(days=365.25 * 70),
        )
        Prestation.objects.create(
            client=cl_inactif, service=Services.ALARME,
            duree=(date.today() - timedelta(days=120), date.today() - timedelta(days=2)),
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:home'))
        self.assertQuerySetEqual(response.context['object_list'], [jill, clara])
        self.assertQuerySetEqual(response.context['en_cours'], [])

    def test_home_avec_filtre(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            rue="Hauptstr. 4", npa="4002", localite="Basel",
            date_naissance=date.today() - timedelta(days=365.25 * 65), service=Services.ALARME
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:home') + '?region=Autre')
        self.assertQuerySetEqual(response.context['object_list'], [jill])

    def test_client_list(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F', rue="Rue des Fleurs 4", npa="2400", localite="Le Locle",
            date_naissance=date.today() - timedelta(days=365.25 * 65), service=Services.ALARME
        )
        autre = Client.objects.create(
            nom='Autre', prenom='Personne', genre='F',
            date_naissance=date.today() - timedelta(days=365.25 * 70), service=Services.ALARME
        )
        # Client non AVS
        Client.objects.create(
            nom='Doe', prenom='John', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 64), service=Services.ALARME
        )
        # Client en home
        Client.objects.create(
            nom='Doe', prenom='Martin', genre='M',
            date_naissance=date.today() - timedelta(days=365.25 * 68), service=Services.ALARME,
            type_logement='home'
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:clients') + '?nom_prenom=doe')
        self.assertQuerySetEqual(response.context['object_list'], [jill])
        self.assertContains(response, "Rue des Fleurs 4")

    def _create_questionnaire_stats(self):
        today = date.today()
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=today - timedelta(days=365 * 70),
            service=[Services.TRANSPORT, Services.ALARME]
        )
        john = Client.objects.create(
            nom='Doe', prenom='Jonh', genre='M',
            date_naissance=today - timedelta(days=365 * 68),
            service=[Services.ALARME]
        )
        Questionnaire.objects.create(
            client=jill, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }, termine_le=now(),
        )
        Questionnaire.objects.create(
            client=john, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }, duree=timedelta(minutes=22), termine_le=now(),
        )
        # Ancien questionnaire non pris en compte dans les stats
        Questionnaire.objects.create(
            client=jill, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }, termine_le=now().replace(year=2022),
        )

    def test_exportation(self):
        self._create_questionnaire_stats()
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:quest-export', args=[date.today().year]))
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_statistiques(self):
        today = date.today()
        self._create_questionnaire_stats()
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:stats-besoins'))
        month_stats = response.context['results'][Month.from_date(today)]
        self.assertEqual(
            month_stats, month_stats | {
                'num_alarme': 2, 'num_transport': 1, 'num_total': 2, 'num_nouveaux': 2,
            }
        )
        self.assertContains(
            response,
            f'<td class="num"><a href="{reverse("besoins:liste-questionnaires")}?'
            f'year={today.year}&month={today.month}&service=alarme">2</a></td>',
            html=True
        )
        response = self.client.get(reverse('besoins:stats-besoins-collab'))
        collab_stats = response.context['collabs']['Valjean Jean']
        self.assertEqual(collab_stats, {
            Month.from_date(today): {'num': 2, 'duree': timedelta(minutes=22)},
            'total_num': 2, 'total_duree': timedelta(minutes=22),
        })

    def test_liste_quest_non_repondus(self):
        q_non_repondu = Questionnaire.objects.create(
            client=Client.objects.create(nom='A'), collab=self.user,
            non_repondu=Questionnaire.NonReponse.REFUSE, termine_le=now(), remarques="Bla bla"
        )
        Questionnaire.objects.create(
            client=Client.objects.create(nom='B'), collab=self.user,
            non_repondu='', termine_le=now(), remarques="Bla bla"
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:liste-non-repondus'))
        self.assertQuerySetEqual(response.context['object_list'], [q_non_repondu])
