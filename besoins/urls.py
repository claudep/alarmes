"""
Configuration d'URL utilisée pour installer une instance indépendante dédiée
à l'analyse des besoins (par ex. besoins.croix-rouge-<canton>.ch).
"""
from django.conf import settings
from django.urls import include, path

from besoins import views

handler500 = 'common.views.error_view'

urlpatterns = [
    path('', include('common.urls')),
    path('', include('client.urls')),

    path('', views.HomeView.as_view(), name='home'),
    path('', include(f'{settings.CANTON_APP}.urls')),
    path('', include('besoins.urls_app')),
]
