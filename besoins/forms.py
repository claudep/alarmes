from django import forms

from common.choices import Services
from common.forms import AnneeForm, BootstrapMixin, MinutesDurationField, RegionField
from common.utils import RegionFinder
from .models import Questionnaire


class FilterClientForm(forms.Form):
    region = RegionField(empty_choice_label='Toutes', include_autre=True, required=False)
    service = forms.ChoiceField(choices=[('', 'Tous')] + Services.choices_can, required=False)

    def filter(self, clients):
        if self.cleaned_data['region']:
            clients = clients.filter(
                RegionFinder.get_region_filter('adresse_active__npa', self.cleaned_data['region'])
            )
        if self.cleaned_data['service']:
            clients = clients.par_service(self.cleaned_data['service'])
        return clients


class FakeCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    def value_from_datadict(self, data, files, name):
        return data.get(name)


class IntroForm(forms.ModelForm):
    rappeler = forms.BooleanField(label="Rappeler plus tard", required=False)

    class Meta:
        model = Questionnaire
        fields = ['aide_entretien', 'rappeler', 'note_rappel', 'non_repondu', 'remarques']
        widgets = {
            # Affichage checkbox, mais comportement radio forcé par JS (un seul choix)
            'non_repondu': FakeCheckboxSelectMultiple,
        }

    def __init__(self, instance=None, **kwargs):
        kwargs['initial']['rappeler'] = True if instance and instance.note_rappel else False
        super().__init__(instance=instance, **kwargs)


class SectionForm(BootstrapMixin, forms.ModelForm):
    """Formulaire générique de questions utilisé par modelform_factory."""
    class Meta:
        model = Questionnaire
        fields = []
        field_classes = {'duree': MinutesDurationField}


class StatsForm(AnneeForm):
    avec_osad = forms.BooleanField(label="Avec OSAD", required=False)
