from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin
from tinymce.widgets import TinyMCE

from .models import Question, Questionnaire, Ressource, Section


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ["code", "theme", "buts", "position"]


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ["question", "question_compl", "section", "position"]


@admin.register(Questionnaire)
class QuestionnaireAdmin(admin.ModelAdmin):
    list_display = ['client', 'created', 'termine_le', 'collab']
    search_fields = ['client__persona__nom']
    raw_id_fields = ['client']


@admin.register(Ressource)
class RessourceAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['titre', 'web', 'order']

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ['coordonnees', 'descriptif']:
            return db_field.formfield(widget=TinyMCE())
        return super().formfield_for_dbfield(db_field, **kwargs)
