from datetime import date, timedelta

from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.db.models import F, Max, Min, Q
from django.forms import modelform_factory
from django.urls import reverse
from django.utils.functional import classproperty
from django.utils.html import format_html

from client.models import Client
from common.choices import Services
from common.forms import BSRadioSelect
from common.functions import IsAVS
from common.models import Utilisateur
from common.utils import canton_app


class Section(models.Model):
    """Section de questionnaire (groupement de questions sur une page)."""
    code = models.SlugField(max_length=15)
    theme = models.CharField("Thème", max_length=150)
    buts = models.TextField("Buts", blank=True)
    position = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ["position"]

    def __str__(self):
        return f"{self.theme} ({self.code})"

    def get_form_class(self, quest):
        from .forms import SectionForm

        fields = []
        for field_name in self.questions.values_list('field', flat=True):
            if field_name in ['message_services', 'remarques']:
                continue
            fields.extend([field_name, f'{field_name}_details'])
        widgets = {
            fname: BSRadioSelect for fname in fields if not fname.endswith('_details')
        }
        if self.code == 'logement' and not quest.client.prestations_actuelles(Services.ALARME):
            fields.append('interet_alarme')
        if self.code == 'fin':
            fields.extend(['duree', 'message_services', 'remarques'])
        return modelform_factory(Questionnaire, form=SectionForm, fields=fields, widgets=widgets)


class Question(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE, related_name="questions")
    question = models.CharField("Question principale", max_length=200)
    question_compl = models.CharField("Question complémentaire", max_length=200, blank=True)
    # Champ temporaire, en attendant class Reponse
    field = models.CharField(max_length=50)
    aide = models.TextField(blank=True)
    position = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ["section__position", "position"]

    def __str__(self):
        return f"{self.question} (Dans {self.section.theme})"

    @property
    def field_detail(self):
        try:
            Questionnaire._meta.get_field(f'{self.field}_details')
            return f'{self.field}_details'
        except FieldDoesNotExist:
            return ''


class Questionnaire(models.Model):
    class Reponse(models.IntegerChoices):
        PAS_DE_REPONSE = -1, "Pas de réponse"
        OUI = 1, "Oui"
        NON = 2, "Non"

    class NonReponse(models.TextChoices):
        REFUSE = 'refus', "Ne souhaite pas répondre au questionnaire (ne sera plus contacté·e avant 18 mois)."
        INJOIGNABLE = 'injoign', "Non joignable (pas de réponse, mauvais numéro, etc.)"

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    collab = models.ForeignKey(Utilisateur, on_delete=models.PROTECT)
    #statut = models.CharField(max_length=10, choices=...)
    non_repondu = models.CharField(max_length=10, choices=NonReponse, blank=True)
    note_rappel = models.TextField("Note interne sur le rappel du client", blank=True)
    termine_le = models.DateTimeField("Questionnaire terminé le", blank=True, null=True)
    aide_entretien = models.TextField(blank=True)
    interet_alarme = models.BooleanField(default=False)
    message_services = models.TextField("Information pour les services", blank=True)
    remarques = models.TextField("Autres remarques générales sur l’entretien", blank=True)
    duree = models.DurationField("Temps passé sur le questionnaire", null=True)

    # Champs du questionnaire
    # Un champ integer pour une réponse oui/non/pas de rép.,
    # un champ texte pour des détails (pour le oui ou le non).
    satisfaction = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    satisfaction_details = models.TextField(blank=True)
    solitude = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    solitude_details = models.TextField(blank=True)
    besoin_contacts = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    besoin_contacts_details = models.TextField(blank=True)
    proche_aidant = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    proche_aidant_details = models.TextField(blank=True)
    aide_courses = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    aide_courses_details = models.TextField(blank=True)
    repas_auto = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    repas_auto_details = models.TextField(blank=True)
    entretien_logement = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    entretien_logement_details = models.TextField(blank=True)
    moyens_aux = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    moyens_aux_details = models.TextField(blank=True)
    chutes = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    chutes_details = models.TextField(blank=True)
    aide_soins = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    aide_soins_details = models.TextField(blank=True)
    besoin_admin = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    besoin_admin_details = models.TextField(blank=True)

    def __str__(self):
        qdate = f"du {self.termine_le.strftime('%d.%m.%Y')}" if self.termine_le else "en cours"
        return f"Questionnaire {qdate} pour {self.client}"

    @classproperty
    def sections(cls):
        return {sect.code: sect for sect in Section.objects.all()}

    @classmethod
    def clients_a_questionner(cls):
        """
        Renvoie la liste des clients qui devraient être (re-)questionnés avec
        l'analyse des besoins.
        """
        delai_entre_quest = 360
        date_delai = date.today() - timedelta(days=delai_entre_quest)
        delai_entre_quest_refuse = 547  # cf. issue #51
        date_delai_refuse = date.today() - timedelta(days=delai_entre_quest_refuse)
        # TODO: exclude clients seulement alarme et en résiliation (#20)
        # Par API ?
        return clients_besoins().avec_adresse(date.today()).annotate(
            last_quest=Max('questionnaire__created'),
            last_quest_refuse=Max('questionnaire__created', filter=Q(questionnaire__non_repondu=cls.NonReponse.REFUSE)),
            debut_prest=Min("prestations__duree__startswith"),
        ).filter(
            Q(last_quest__isnull=True) | (
                Q(last_quest__date__lt=date_delai) & (
                    Q(last_quest_refuse=None) |
                    Q(last_quest_refuse__date__lt=date_delai_refuse)
                )
            )
        )

    def theme_ok(self, section):
        """Renvoie True si le thème donné est complètement termine, sinon False."""
        fields = [quest.field for quest in section.questions.all()]
        return all(getattr(self, fname) is not None for fname in fields)

    def quest_ok(self):
        """Renvoie True si toutes les questions du questionnaire ont été abordées."""
        return all(self.theme_ok(section) for section in self.sections.values())

    def get_reponse(self, field_name):
        try:
            return getattr(self, f'get_{field_name}_display')()
        except AttributeError:
            return getattr(self, field_name)

    def continuer_url(self):
        return reverse(
            'besoins:questionnaire',
            args=[self.client.pk, self.pk, 'intro' if self.note_rappel else 'environnement']
        )

    def resume(self):
        for question in Question.objects.exclude(section__code='fin'):
            reponse = self.get_reponse(question.field)
            if self.termine_le and reponse is None:
                # Par ex. une question créée après la fin du questionnaire.
                continue
            details = getattr(self, question.field_detail, '')
            if question.field == "chutes" and self.interet_alarme:
                details = format_html(
                    "{}<br><i>La personne a donné son accord pour signaler son intérêt "
                    "à une installation d’alarme</i>",
                    details
                )
            yield {
                'question': question,
                'reponse': reponse,
                'reponse_details': details,
            }


class Ressource(models.Model):
    titre = models.CharField(max_length=100)
    order = models.SmallIntegerField(default=0)
    web = models.URLField(blank=True)
    coordonnees = models.TextField(blank=True)
    descriptif = models.TextField(blank=True)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.titre


def clients_besoins():
    return Client.objects.avec_services().annotate(
        is_avs_db=IsAVS(date.today(), F("tarif_avs_des"), F("persona__date_naissance"), F("persona__genre"))
    ).exclude(
        Q(is_avs_db=False) |
        Q(archive_le__isnull=False) |
        Q(persona__date_deces__isnull=False) |
        Q(fonds_transport=True) |
        Q(type_logement__in=canton_app.BESOINS_TYPE_LOGEMENT_EXCLUDE) |
        Q(services=[])
    )
