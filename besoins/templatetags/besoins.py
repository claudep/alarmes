from django.template import Library
from django.urls import reverse
from django.utils.html import format_html

register = Library()


@register.simple_tag(takes_context=True)
def stat_numb(context, month, stat_key, default='-'):
    number = context['results'].get(month, {}).get(stat_key)
    if number is None:
        return default
    if 'total' in stat_key:
        return number
    return format_html(
        '<a href="{url}">{numb}</a>',
        url=reverse('besoins:liste-questionnaires') + (
            f'?year={month.year}&month={month.month}&service={stat_key.replace("num_", "")}'
        ),
        numb=number,
    )


@register.filter
def prev_reponse(prev_quest, field):
    return {
        True: "Oui", False: "Non", None: "Pas de réponse",
        -1: "Pas de réponse", 1: "Oui", 2: "Non",
    }.get(getattr(prev_quest, field, ''), getattr(prev_quest, field, ''))
