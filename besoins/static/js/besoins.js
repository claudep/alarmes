'use strict';

async function postForm(form) {
    const formData = new FormData(form);
    addCSRFToken(formData);
    const respo = await fetch(
        form.action,
        {method: 'POST', body: formData, headers: {'X-Requested-With': 'fetch'}}
    );
    form.dataset.changed = "false";
    return respo;
}

function chutesCheck() {
    const yesSelected = document.querySelector('#id_chutes').closest('form').elements.chutes.value == "1";
    yesSelected ? show('#interet_alarme_div') : hide('#interet_alarme_div');
    if (!yesSelected) document.querySelector('#id_interet_alarme').checked = false;
}

function copyResponse(ev) {
    const textarea = ev.target.closest(".quest_question").querySelector('textarea');
    if (textarea) textarea.value = ev.target.previousElementSibling.innerHTML.replace('<br>', '\n');
}

window.addEventListener('DOMContentLoaded', () => {
    attachHandlerSelector(document, '#aidePanel', 'shown.bs.offcanvas', (ev) => {
        document.querySelector("div.top-container").classList.add('offcanvas-open');
    });
    attachHandlerSelector(document, '#aidePanel', 'hidden.bs.offcanvas', (ev) => {
        document.querySelector("div.top-container").classList.remove('offcanvas-open');
    });

    /* When clicking on any theme link, ensure the current form is saved first. */
    attachHandlerSelector(document, '.theme_link', 'click', async (ev) => {
        const form = document.querySelector('#quest_form');
        ev.preventDefault();
        removeEventListener('pagehide', beforeUnloadListener);
        const response = await postForm(form);
        window.location.href = ev.target.href;
    });
    if (document.querySelector('#interet_alarme_div')) {
        attachHandlerSelector(document, '#id_chutes', 'change', chutesCheck);
        if (document.querySelector('#id_chutes')) chutesCheck();
    }
    if (document.querySelector('#id_rappeler')) {
        attachHandlerSelector(document, '#id_rappeler', 'change', (ev) => {
            if (ev.target.checked) show('#id_rappeler_suite');
            else {
                hide('#id_rappeler_suite');
                document.querySelector('#id_note_rappel').value='';
            }
        });
    }
    if (document.querySelector('#id_non_repondu')) {
        attachHandlerSelector(document, '#id_non_repondu', 'change', (ev) => {
            ev.target.checked ? show('#id_non_repondu_suite') : hide('#id_non_repondu_suite');
            // Simulate radios, uncheck other inputs
            ev.currentTarget.querySelectorAll("[id^='id_non_repondu_']").forEach(input => {
                if (ev.target == input) return;
                if (input.checked) input.checked = false;
            });
        });
    }
    attachHandlerSelector(document, '.copier_reponse', 'click', copyResponse);
});
