from datetime import date, datetime, timedelta

from django.template import Library
from django.templatetags.static import static
from django.utils.dateformat import format as django_format
from django.utils.html import format_html
from django.utils.timezone import localtime as tz_localtime

from common.distance import ORSUnavailable
from transport.models import Dispo, Transport

register = Library()


@register.filter
def ar_icon(field_val):
    icon_url = static(
        "img/%s.svg" % {True: "aller-retour", False: "aller"}[field_val]
    )
    return format_html(
        '<img class="icon blacksvg" src="{}" title="{}">',
        icon_url, {True: "Aller-retour", False: "Aller simple"}[field_val]
    )


@register.filter
def date_transp(dt):
    if not dt:
        return dt
    res = django_format(tz_localtime(dt) if isinstance(dt, datetime) else dt, 'D d.m.Y').upper()
    return res[:2] + res[3:]


@register.filter
def localtime(dt):
    if not dt:
        return dt
    return str(tz_localtime(dt).time())[:5]


@register.inclusion_tag('transport/transport_line.html', takes_context=True)
def transport_line(context, transport, cols=''):
    statuts = Transport.StatutChoices
    action_map = {
        statuts.SAISI: ['edit', 'attrib', 'hist', 'cancel'],
        statuts.ATTRIBUE: ['edit', 'hist', 'cancel'],
        statuts.CONFIRME: ['edit', 'hist', 'cancel'],
        statuts.EFFECTUE: ['rapport', 'hist', 'cancel'],
        statuts.RAPPORTE: ['hist'],
        statuts.CONTROLE: ['hist'],
        statuts.ANNULE: ['hist'],
    }
    if 'transport.change_transport' in context['perms']:
        action_map[statuts.EFFECTUE].insert(1, 'edit')
        action_map[statuts.RAPPORTE].insert(0, 'edit')
        if not transport.est_passe_lointain:
            action_map[statuts.CONTROLE].insert(0, 'edit')
    return {
        'request': context['request'],
        'transp': transport,
        'columns': [col for col in cols.split(',') if not col.startswith('-')],
        'confirm': transport.statut == Transport.StatutChoices.ATTRIBUE,
        'show_date': '-date' not in cols,
        'show_client': '-client' not in cols,
        'show_chauffeur': '-chauffeur' not in cols,
        'suppr_chauffeur': transport.statut < Transport.StatutChoices.RAPPORTE,
        'ANNULE': Transport.StatutChoices.ANNULE,
        'CONFIRME': Transport.StatutChoices.CONFIRME,
        'RAPPORTE': Transport.StatutChoices.RAPPORTE,
        'actions': action_map.get(transport.statut),
    }


@register.filter
def echeance_macaron(benev):
    if benev.macaron_valide and benev.macaron_valide.upper < date.today() + timedelta(days=30):
        return format_html(
            '<div class="delai-passe">Macaron échu le {}</div>',
            django_format(benev.macaron_valide.upper, 'd.m.Y')
        )
    return ''


@register.filter
def can_edit_rapport(transp, benev):
    return transp.can_edit_rapport(benev)


@register.filter
def timeline_style(obj):
    TOTAL_MINS = 840  # 06h00 - 20h00
    unsure = False
    if isinstance(obj, Transport):
        dep = (obj.heure_depart - obj.heure_depart.replace(hour=6, minute=0)).seconds // 60
        try:
            duree = round(obj.duree_calc.seconds // 60 / TOTAL_MINS * 100, 1)
        except ORSUnavailable:
            duree = 7200 // 60 / TOTAL_MINS * 100
            unsure = True
    if isinstance(obj, Dispo):
        tz_debut, tz_fin = tz_localtime(obj.debut), tz_localtime(obj.fin)
        if tz_debut.hour < 6:
            tz_debut = tz_debut.replace(hour=6, minute=0)
        if tz_fin.hour >= 20:
            tz_fin = tz_fin.replace(hour=20, minute=0)
        dep = (tz_debut - tz_debut.replace(hour=6, minute=0)).seconds // 60
        duree = round((tz_fin - tz_debut).seconds // 60 / TOTAL_MINS * 100, 1)
    style = f"left: {round(dep / TOTAL_MINS * 100, 1)}%; width: {duree}%;"
    if unsure:
        style += ' background-color: yellow;'
    return style


@register.filter
def as_html(obj):
    return obj.__html__() if hasattr(obj, '__html__') else str(obj)


@register.filter
def as_km(meters):
    return f"{round(meters / 1000, 1):g}"
