import math
from datetime import time
from django import template
from django.template.defaultfilters import linebreaksbr
from django.urls import reverse
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime

from ..models import Transport

register = template.Library()


@register.simple_tag
def mult(*args):
    return math.prod(args)


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.filter
def dispo_edit_url(dispo, user):
    if not dispo.can_edit(user):
        return ''
    if hasattr(dispo, 'edit_url'):
        return dispo.edit_url()
    if user.is_benevole:
        return reverse('benevole-self-dispo-edit', args=[dispo.pk])
    return reverse('benevole-dispo-edit', args=[dispo.pk])


@register.filter
def get_events(events, day_div):
    return events['events'].get(day_div)


@register.filter
def is_hour(time_):
    return time_.minute == 0


@register.filter
def is_meal(time_):
    return (time(12, 0) <= time_ < time(13, 30)) or (time(18, 30) <= time_ < time(19, 30))


@register.filter
def as_hour_min(dtime):
    return str(localtime(dtime).time())[:5]


@register.filter
def before_icons(event):
    icons = []
    # aller-retour icon?
    return mark_safe(' '.join(icons))


@register.filter
def event_details(event):
    return linebreaksbr(escape(event.description))


@register.filter
def tooltip_content(event):
    if not isinstance(event, Transport):
        return ""
    if event.retour:
        return (
            f"Aller-retour de {event.adresse_depart().localite} à {event.destination.localite}, "
            f"retour (dès {event.heure_depart_retour.strftime('%H:%M')}) à {event.adresse_arrivee().localite}"
        )
    else:
        return f"Aller simple de {event.adresse_depart().localite} à {event.destination.localite}"


@register.filter
def attente_top(transp):
    """Pourcentage du sommet du bloc de temps d'attente par rapport au transport complet."""
    return "%s%%" % int(
        (transp.heure_rdv - transp.heure_depart) /
        (transp.heure_arrivee - transp.heure_depart) * 100
    )


@register.filter
def attente_height(transp):
    """Pourcentage de hauteur du bloc de temps d'attente par rapport au transport complet."""
    return"%s%%" % int(
        transp.duree_rdv / (transp.heure_arrivee - transp.heure_depart) * 100
    )
