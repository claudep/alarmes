const heureDepartId = 'id_heure_depart';

const getCellValue = (tr, idx, dataname) => {
    return dataname == 'text' ? tr.children[idx].textContent :  tr.children[idx].dataset[dataname];
};
const comparer = (idx, dataname, asc) => (a, b) => ((v1, v2) =>
    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx, dataname), getCellValue(asc ? b : a, idx, dataname));

function sortTable(tbody, noCol, dataname='text') {
    Array.from(tbody.querySelectorAll('tr'))
        .sort(comparer(noCol, dataname, true)).forEach(tr => tbody.appendChild(tr) );
}

function getRadioValue(name) {
    for (let el of document.getElementsByName(name)) {
        if (el.checked) return el.value;
    }
}

function getClientId(form) {
    let clientId;
    if (form.elements.id_client) return form.elements.id_client.value;
    return document.querySelector('#client_existing').dataset.clientid;
}

async function apresSaisieDate(ev) {
    // Si la date est dans le passé, afficher un avertissement (non bloquant).
    const form = ev.target.form;
    const dateVal = form.elements.id_date.value;
    const now = new Date();
    now.setHours(0,0,0,0);
    if (dateVal && new Date(dateVal) < now) {
        ev.target.classList.add("is-invalid");
    } else ev.target.classList.remove("is-invalid");
    calculerDepartAller(ev);
}

async function calculerDepartAller(ev, manual=false) {
    const img = document.querySelector('#calculer_depart');
    const form = img.closest('form');
    const clientId = getClientId(form);
    const dateRdv = form.elements.id_date.value;
    const heureRdvInput = form.elements.id_heure_rdv;
    const heureDepInput = document.querySelector(`#${heureDepartId}`);
    const origInput = document.querySelector('#id_origine_adr');
    const origId = origInput ? origInput.value : null;
    // Liste des destinations jusqu'à la destination principale.
    const destIds = [];
    Array.from(document.querySelectorAll('input[id$="destination_adr"]')).some((inp) => {
        const val = inp.closest('.row').querySelector('input[id$="destination_adr"]').value;
        if (val) destIds.push(val);
        return inp.closest('.row').classList.contains('destination_principale');
    });
    if (!destIds || !dateRdv || !heureRdvInput.value || (!clientId && !origId)) {
        if (manual) alert("Vous devez remplir d’abord la destination et l’heure de rendez-vous.")
        return;
    }
    if (destIds.toString() == "-1") {
        // Retour à domcile, heure_depart = heure_rdv
        heureDepInput.value = heureRdvInput.value;
        heureDepInput.removeAttribute('disabled');
        return;
    }
    let formData = new FormData();
    formData.append('client', clientId);
    formData.append('origine', origId);
    destIds.forEach((val) => formData.append('destination', val));
    formData.append('date', dateRdv);
    formData.append('heure_rdv', heureRdvInput.value);
    addCSRFToken(formData);
    const resp = await fetch(img.dataset.url, {method: 'POST', body: formData});
    const data = await resp.json();
    if (data.result == 'OK') {
        heureDepInput.value = data.depart;
        heureDepInput.dataset.dureetrajet = data.duree_trajet;
        heureDepInput.removeAttribute('disabled');
    }
    else if (manual) alert(data.reason);
}

function calculerDepartRetour(ev) {
    const heureRdv = document.querySelector('#id_heure_rdv').value;
    const dureeRdv = document.querySelector('#id_duree_rdv').value;
    if (heureRdv && dureeRdv.includes(':')) {
        let dt = new Date();
        dt.setHours(parseInt(heureRdv.split(':')[0]) + parseInt(dureeRdv.split(':')[0]));
        dt.setMinutes(parseInt(heureRdv.split(':')[1]) + parseInt(dureeRdv.split(':')[1]));
        document.querySelector('#depart_retour').textContent = (
            `${dt.getHours().toString().padStart(2, '0')}:${dt.getMinutes().toString().padStart(2, '0')}`
        );
        // Arrivée retour
        const duree = document.querySelector(`#${heureDepartId}`).dataset.dureetrajet;
        if (duree) {
            dt.setHours(dt.getHours() + parseInt(duree.split(':')[0]));
            dt.setMinutes(dt.getMinutes() + parseInt(duree.split(':')[1]));
            document.querySelector('#arrivee_retour').textContent = (
                `${dt.getHours().toString().padStart(2, '0')}:${dt.getMinutes().toString().padStart(2, '0')}`
            );
        }
    }
}

/* Affichage des adresses et des transports récents du client qui vient d'être
 * sélectionné.
 */
async function detailsClient(ev) {
    async function _remplirDerniersTransports() {
        // Remplissage dynamique de la liste des derniers transports du client.
        const transpCont = document.querySelector('#derniers_transports');
        if (transpCont) {
            const resp = await fetch(
                `${transpCont.dataset.transportsurl}?client=${ev.detail.value}`, {method: 'GET'}
            );
            const html = await resp.text();
            transpCont.innerHTML = html
        }
    }
    _remplirDerniersTransports();
    const adrConts = document.querySelectorAll('#client_adr_container, #adresse_depart, #adresse_retour');
    if (adrConts) {
        const resp = await fetch(
            `${ev.target.dataset.clienturl}?id=${ev.detail.value}`, {method: 'GET'}
        );
        const data = await resp.json();
        const adresse = document.createElement('div');
        adresse.innerHTML = `${data.rue}<br>${data.npa} ${data.localite}`;
        if (!data.geolocalise) {
            const template = document.querySelector('#not_geolocalized');
            const clone = template.content.cloneNode(true);
            adresse.append(document.createElement("br"), clone);
        }
        adrConts.forEach(el => {
            el.innerHTML = "";
            el.appendChild(adresse.cloneNode(true));
        });
        document.querySelector('#remarques_client').innerHTML = data.remarques;
        data.fonds_transport ? show('#fonds_transport') : hide('#fonds_transport');
        if (data.type_logement == "home") {
            document.querySelector('#id_typ').value = "home";
        }
        const laaOption = document.querySelector("option[value='laa']");
        if (laaOption) {
            if (data.laa) {
                laaOption.removeAttribute("hidden");
                laaOption.parentNode.value = "laa";
            }
            else laaOption.setAttribute("hidden", true);
        }
    }
}

async function copierTransport(img) {
    if (img.dataset.origid) {
        // Origin != domicile
        hide('#texte_depart_autre'); show('#input_depart_autre');
        document.querySelector('#id_origine_adr').value = img.dataset.origid;
        document.querySelector('#id_origine_adr_select').value = img.dataset.origtext;
    }
    document.querySelector('#id_trajets-0-destination_adr').value = img.dataset.destid;
    document.querySelector('#id_trajets-0-destination_adr_select').value = img.dataset.desttext;
    document.querySelector('#id_remarques').value = img.dataset.remarques;
    const el = document.querySelector('#id_heure_rdv');
    el.value = img.dataset.heure_rdv;
    const ARValue = getRadioValue("retour");
    if (ARValue !== img.dataset.retour) {
        document.querySelector(`#id_retour_${ARValue == 'True' ? 1 : 0}`).checked = true;
        document.querySelector('input[name="retour"]').dispatchEvent(new Event('change'));
    }
    if (img.dataset.retour == 'True') {
        document.querySelector('#id_duree_rdv').value = img.dataset.duree_rdv;
    }
    document.querySelector('#id_typ').value = img.dataset.type;
    await calculerDepartAller();
    if (img.dataset.retour == 'True') calculerDepartRetour();
    el.form.dispatchEvent(new Event('change'));
}

function switchAR(ev) {
    const ARValue = getRadioValue("retour");
    if (ARValue == "True") {
        show('#col_duree_rdv');
        show('#ligne_depart_retour');
        show('#ligne_adresse_retour');
    } else {
        document.querySelector('#id_duree_rdv').value = '';
        hide('#col_duree_rdv');
        hide('#ligne_depart_retour');
        hide('#ligne_adresse_retour');
        // delete any additional trajet for retour
        let remove = false;
        for (let ligne of document.querySelectorAll('.ligne_destination')) {
            if (remove) {
                ligne.nextElementSibling.remove();
                removeTrajet(ligne);
            }  else if (ligne.classList.contains('destination_principale')) {
                remove = true;
            }
        }
    }
}

function addTrajet(ev) {
    const node = cloneMore(ev);
    ev.target.closest('.row').parentNode.insertBefore(node, ev.target.closest('.row'));
    attachHandlerSelector(node, 'input[id$="destination_adr"]', 'change', delay(calculerDepartAller, 200));
    attachHandlerSelector(node, '.openmodal', 'click', openModal);
    attachHandlerSelector(node, '.enlever-ligne', 'click', (ev) => {
        incrementTotalForms(ev.target.closest('form'), -1);
        ev.target.closest('.row').remove();
    });
    document.querySelectorAll('input[id$=ORDER]').forEach((el, idx) => {
        if (!el.id.includes('__prefix__')) el.value = idx;
    });
}

function removeTrajet(row) {
    if (!row.querySelector("[id$='-id']").value) {
        // Not yet saved, we can simply drop it
        incrementTotalForms(row.closest('form'), -1);
        row.remove();
    } else {
        row.querySelector("[id$='-DELETE']").value = 'on';
        hide(row);
    }
}

/*
 * Send to server to validate transport and move line client-side from Rapportés
 * to Contrôlés.
 */
function validateTransport(ev) {
    submitForm(ev, ev.target.closest('form')).then(() => {
        ev.target.closest('tr').remove();
        document.querySelector('#badge5').textContent = parseInt(document.querySelector('#badge5').textContent) - 1;
        document.querySelector('#badge6').textContent = parseInt(document.querySelector('#badge6').textContent) + 1;
    });
}

async function refusChauffeur(ev) {
    const chauffeurID = document.querySelector('#frm_attribution').elements.chauffeur.value;
    const curChoice = document.querySelector(`input#id_chauffeur_${chauffeurID}`);
    const nomChauffeur = curChoice.parentNode.querySelector('.nom_chauffeur').textContent;
    const rep = confirm(`Confirmez-vous que le chauffeur ${nomChauffeur} a refusé ce transport ?`);
    if (!rep) return false;
    const url = ev.target.dataset.url;
    let formData = new FormData();
    formData.append('chauffeur', chauffeurID);
    addCSRFToken(formData);
    waiter.show()
    try {
        const resp = await fetch(url, {method: 'POST', body: formData});
        if (resp.ok) {
            const data = await resp.json();
            if (data.result == 'OK') {
                // Move chauffeur to bottom of list and change icon
                const chauffDiv = curChoice.closest('div.chauffeur_choice');
                chauffDiv.parentNode.insertBefore(chauffDiv, null);
                const img = chauffDiv.querySelector('img');
                img.src = chauffDiv.parentNode.dataset.iconno;
                img.removeAttribute('hidden');
            }
            if (data.redirect && data.redirect.length) {
                waiter.hide();
                window.location.replace(data.redirect);
            }
        } else throw new Error(`Erreur ${resp.status}: ${resp.statusText}`);
    } catch(err) {
        alert(`Désolé, une erreur s’est produite (${err})`);
    } finally {
        waiter.hide();
    }
}

/* Filtrer (client-side) la liste des chauffeurs selon un terme de recherche */
function filtrerChauffeurs(ev) {
    const term = ev.target.value;
    if (term) {
        const searchRegex = new RegExp(term, 'gi');
        const stripRegex= new RegExp(/[\n\r]+|[\s]{2,}/, 'g');
        document.querySelectorAll('.chauffeur_choice').forEach(el => {
            const elText = el.textContent.replace(stripRegex, ' ').trim();
            if (elText.match(searchRegex)) show(el)
            else hide(el);
        });
    } else {
        document.querySelectorAll('.chauffeur_choice[hidden]').forEach(el => show(el));
    }
}

/* Fonction appelée lorsqu'une modale est affichée et remplie par son contenu dynamique */
function containerFilled(ev) {
    const container = ev.target;

    const dirSearch = document.querySelector('#id_search');
    if (dirSearch) {
        const ac = new Autocomplete(dirSearch, {
            maximumItems: 20,
            onSelectItem: (data) => {
                // Fill form fields with data from data.details
                const details = JSON.parse(data.details);
                container.querySelector('#id_nom').value = details.nom;
                if (details.npa && details.localite)  {
                    container.querySelector('#id_npalocalite').value = `${details.npa} ${details.localite}`;
                }
                if (details.rue) container.querySelector('#id_rue').value = details.rue;
                if (details.tel) container.querySelector('#id_tel').value = details.tel;
            }
        });
        document.querySelector('#button-search').addEventListener('click', async (ev) => {
            const btn = ev.currentTarget;
            if (dirSearch.value.length < 4) return;
            const resp = await fetch(`${btn.dataset.searchurl}?q=${dirSearch.value}`);
            const data = await resp.json();
            // data expected as list of {'label'/'value'} objects.
            ac.setData(data);
            ac.renderIfNeeded();
        });
        dirSearch.focus();
    }
    const annuleBox = document.querySelector('#id_annule');
    if (annuleBox) {
        annuleBox.addEventListener('change', ev => {
            const modalSave = document.querySelector('#modal-save');
            if (ev.target.checked) {
                modalSave.textContent = modalSave.dataset.oldtext;
                hide('#annule_revert_warning');
            } else {
                modalSave.dataset.oldtext = modalSave.textContent;
                modalSave.textContent = "Annuler l’annulation!";
                show('#annule_revert_warning');
            }
        });
    }
}

function sortTransportsByCookie(section) {
    // Seulement sur page Saisis pour le moment.
    const onSaisi = section.querySelector('#ffstatut1_container') || section.closest('#ffstatut1_container');
    if (onSaisi && Cookies.get('tritransports') == 'npa') {
        const tbody = section.classList.contains('transport_body') ? section : section.querySelector('.transport_body');
        if (tbody) sortTable(tbody, 3, dataname='npaorigin');
    }
}

function loadTransports(ev) {
    // Load transport list for a specific day when toggling a day details/summary.
    if (!ev.target.classList.contains('transp_group_details')) return;
    const details = ev.currentTarget;
    if (!details.dataset.loaded && details.dataset.url && details.hasAttribute('open')) {
        fetch(details.dataset.url, {headers: {'X-Requested-With': 'fetch'}})
        .then(resp => resp.text()).then(html => {
            const parser = new DOMParser();
            const doc = parser.parseFromString(html, "text/html");
            const oldBody = details.querySelector('tbody.transport_body');
            const newBody = doc.querySelector('tbody.transport_body');
            oldBody.replaceWith(newBody);
            details.dataset.loaded = true;
            attachHandlerSelector(newBody, 'form.validate_transport img', 'click', validateTransport);
            attachHandlers(newBody);
            sortTransportsByCookie(newBody);
            const cb = details.querySelector('.check_uncheck_all');
            if (cb) cb.removeAttribute('hidden');
        });
    }
}

function showMoreDays(ev) {
    const form = ev.target.form;
    const nextDay = form.elements.next_day;
    fetch(`${form.action}?start=${nextDay.value}`).then(resp => resp.text()).then(html => {
        var parser = new DOMParser();
        var doc = parser.parseFromString(html, "text/html");
        const container = document.querySelector('.days_container');
        const receivedDays = doc.querySelector('.days_container');
        container.append(receivedDays);
        // Update start value
        nextDay.value = doc.querySelector('#next_day').value;
    });
}

function checkUncheckBoxes(ev) {
    // Cocher ou décocher toutes les cases de sélection du jour.
    if (ev.target.classList.contains('check_uncheck_all')) {
        const shouldCheck = ev.target.checked;
        ev.target.closest('details').querySelectorAll('[name="selection"').forEach(cb => {
            cb.checked = shouldCheck;
        });
    }
}

function handleTransportListClick(ev) {
    // Boutons pour trier liste de transports par Heure rdv ou NPA d'origine.
    if (ev.target.tagName == "A" && ev.target.classList.contains("tri-hrdv")) {
        ev.preventDefault();
        sortTable(ev.target.closest('details').querySelector('.transport_list tbody'), 1);
        Cookies.remove('tritransports');
    } else if (ev.target.tagName == "A" && ev.target.classList.contains("tri-npa")) {
        ev.preventDefault();
        sortTable(ev.target.closest('details').querySelector('.transport_list tbody'), 3, dataname='npaorigin');
        Cookies.set('tritransports', 'npa', { expires: 1});
    }
}

async function loadObjectDayView(ev) {
    /* Lorsqu'un chauffeur ou véhicule est sélectionné dans la liste, les détails de son
       agenda sont affichés sur la droite, et l'interface est adaptée.
    */
    const input = ev.target;
    const form = input.closest('form');
    const remarkContainer = form.querySelector('#remark_container');
    const container = document.querySelector(`#planif_${input.name}_container`);
    if (form.dataset[`${input.name}checked`] == input.id) {
        // Uncheck already checked option
        container.innerHTML = "";
        hide(container);
        input.checked = false;
        form.dataset[`${input.name}checked`] = null;
        if (input.name == "chauffeur") {
            form.querySelector('#btn_attribuer').setAttribute('disabled', true);
            remarkContainer.innerHTML = "",
            remarkContainer.setAttribute('hidden', true);
            const btnRefus = document.querySelector('button[id=refuser]');
            if (btnRefus) (btnRefus.setAttribute('disabled', true));
        } else if (input.name == "vehicule") {
            form.querySelector('#btn_attribuer_vhc').setAttribute('disabled', true);
            document.querySelectorAll(".ne_conduit_pas").forEach((el) => hide(el));
        }
        return;
    }

    form.dataset[`${input.name}checked`] = input.id;
    resp = await fetch(input.dataset.agendaurl);
    html = await resp.text();
    container.innerHTML = html;
    container.removeAttribute('hidden');
    attachHandlers(container);
    if (input.name == "chauffeur") {
        form.querySelector('#btn_attribuer').removeAttribute('disabled');
        const chauffRems = input.closest(".row").querySelector(".chauffeur_remarques");
        if (chauffRems) {
            remarkContainer.textContent = chauffRems.dataset.bsOriginalTitle;
            remarkContainer.removeAttribute('hidden');
        }
        const btnRefus = document.querySelector('button[id=refuser]');
        if (btnRefus) (btnRefus.removeAttribute('disabled'));
    } else if (input.name == "vehicule") {
        form.querySelector('#btn_attribuer_vhc').removeAttribute('disabled');
        document.querySelectorAll(".ne_conduit_pas").forEach((el) => show(el));
    }
}

async function toggleChauffExterne(ev) {
    /* Case "Chauffeur externe" cochée ou décochée.*/
    if (ev.target.checked) {
        const chauffChecked = document.querySelector("input[name='chauffeur']:checked");
        if (chauffChecked) {
            chauffChecked.dispatchEvent(new Event('click'));
        }
        ev.target.form.querySelector('#btn_attribuer').removeAttribute('disabled');
    } else {
        ev.target.form.querySelector('#btn_attribuer').setAttribute('disabled', true);
    }
}


async function calcChauffeurDistances(nFirst) {
    /* Calcul distances plus précises sur page attribution */
    const span = document.querySelector("span[data-calcdisturl]");
    if (span) {
        let formData = new FormData();
        addCSRFToken(formData);
        const url = span.dataset.calcdisturl;
        const resp = await fetch(url, {method: 'POST', body: formData});
        const data = await resp.json();
        if (data["result"] == "OK" && data["type"] != "vo") {
            span.setAttribute("title", "Distance entre localités");
            span.setAttribute("class", data.type);
            span.textContent = `~ ${data.distance}km`;
        }
        delete span.dataset.calcdisturl;
        // launch next calc
        if (nFirst > 0) {
            calcChauffeurDistances(nFirst - 1);
        }
    }
}

function attachTabHandlers(tab) {
    if (tab.closest('ul').id == 'statutTabs') {
        const tabTarget = document.querySelector(tab.dataset.bsTarget);
        attachHandlerSelector(tabTarget, '#ffstatut1_container', 'click', handleTransportListClick);
        attachHandlerSelector(tabTarget, '.check_uncheck_all', 'click', checkUncheckBoxes);
        attachHandlerSelector(tabTarget, 'form.validate_transport img', 'click', validateTransport);
        attachHandlerSelector(tabTarget, '.transp_group_details', 'toggle', loadTransports);
        attachHandlerSelector(document, '.table-filter', 'filterapplied', (ev) => {
            attachTabHandlers(tab);
        });
        sortTransportsByCookie(tabTarget);
    } else if (tab.closest('ul').id == 'appTabs') {
        // Benev app
        attachHandlerSelector(document, '#more_dispo_days', 'click', showMoreDays);
    }
}

window.addEventListener('DOMContentLoaded', () => {
    attachHandlerSelector(document, '#id_client_select', 'itemselected', detailsClient);
    attachHandlerSelector(document, '#siteModal', 'containerfilled', containerFilled);
    attachHandlerSelector(document, '#calculer_depart', 'click', (ev) => calculerDepartAller(ev, true));

    const clientInput = document.querySelector('#id_client');
    if (clientInput && clientInput.value) {
        // Client initially filled, simulate a selection.
        const select = document.querySelector('#id_client_select');
        if (select) {
            select.dispatchEvent(
                new CustomEvent('itemselected', {detail: {label: select.value, value: clientInput.value}})
            );
        }
    }
    attachHandlerSelector(document, 'a[data-bs-toggle="tab"]', 'shown.bs.tab', ev => {
        if (!ev.target.getAttribute('href')) return;
        if (ev.target.dataset.statut) {
            history.pushState({tab: ev.target.dataset.statut}, "", ev.target.href);
            // We may implement onpopstate at some point to implement back behavior
        }
        loadURLInTab(ev.target).then((tab) => attachTabHandlers(tab));
    });
    // Chargement du contenu de l'onglet actif
    const activeHomeTab = document.querySelector('#statutTabs .nav-link.active, #appTabs .nav-link.active');
    if (activeHomeTab && activeHomeTab.getAttribute('href')) {
        loadURLInTab(activeHomeTab).then((tab) => attachTabHandlers(tab));
    }

    const formSaisie = document.querySelector('form.saisie');
    if (formSaisie) {
        attachHandlerSelector(document, '#edit_depart', 'click', (ev) => {
            hide('#texte_depart_autre'); show('#input_depart_autre');
        });
        attachHandlerSelector(document, '#edit_arrivee', 'click', (ev) => {
            hide('#texte_arrivee_autre'); show('#input_arrivee_autre');
        });
        attachHandlerSelector(document, 'input[name="retour"]', 'change', switchAR);
        switchAR();
        if (!document.querySelector('#id_heure_rdv').value) {
            Array.from(formSaisie.elements).forEach((el) => {
                if ([heureDepartId,].includes(el.id)) {
                    el.setAttribute('disabled', true);
                }
            });
        }
        attachHandlerSelector(document, '#id_origine_adr', 'change', delay(calculerDepartAller, 200));
        attachHandlerSelector(document, '#id_date', 'change', delay(apresSaisieDate, 200));
        attachHandlerSelector(document, 'input[id$="destination_adr"]', 'change', (ev) => {
            const destAct = ev.target.parentNode.querySelector('.destination_actuelle');
            if (destAct) destAct.textContent = '';
            calculerDepartAller();
        });
        attachHandlerSelector(document, '#id_heure_rdv', 'change', delay(calculerDepartAller, 200));
        attachHandlerSelector(document, '#id_heure_rdv', 'change', delay(calculerDepartRetour, 200));
        attachHandlerSelector(document, '#id_duree_rdv', 'keyup', delay(calculerDepartRetour, 200));
        attachHandlerSelector(document, '.ajout_destination', 'click', addTrajet);
        attachHandlerSelector(document, '.enlever-ligne:not(.empty)', 'click', (ev) => {
            removeTrajet(ev.target.closest('.row'));
        });
        attachHandlerSelector(document, '#derniers_transports', 'click', (ev) => {
            if (ev.target.classList.contains('copy-transport')) copierTransport(ev.target);
        });
    }
});
