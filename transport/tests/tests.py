from datetime import date, datetime, time, timedelta, timezone
from decimal import Decimal as D
from io import BytesIO
from unittest import skipIf
from unittest.mock import PropertyMock, patch
from urllib.parse import quote

import httpx
from dateutil import relativedelta
from freezegun import freeze_time
from pypdf import PdfReader

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, ignore_warnings, override_settings
from django.urls import reverse
from django.utils.timezone import get_current_timezone, localtime, now

from benevole.models import TypeActivite
from client.models import (
    AdresseClient, AdressePresence, Alerte, Client, Persona, Prestation, Referent
)
from common.choices import Handicaps, Services
from common.forms import HMDurationField
from common.distance import ORSUnavailable, distance_real, distance_vo
from common.export import openxml_contenttype
from common.models import DistanceFigee, Utilisateur
from common.stat_utils import Month
from common.test_utils import BaseDataMixin, TempMediaRootMixin, mocked_httpx, read_xlsx
from common.utils import RegionFinder, canton_app, fact_policy, format_mois_an
from transport.forms import (
    AdresseEditForm, CancelForm, DispoEditForm,  RapportForm, SaisieTransportForm
)
from transport.models import (
    Adresse, Benevole, Dispo, Facture, Frais, Preference, Refus, Regle, Trajet,
    TrajetCommun, Transport, TransportModel, Vehicule, VehiculeOccup
)
from transport.views import LotAFacturer

GEOADMIN_API_SEARCH = "https://api3.geo.admin.ch/rest/services/api/SearchServer"
TRANSPORT = Services.TRANSPORT


class FakeORSClient:
    def __init__(self, key, **kwargs):
        pass

    def directions(self, coords, **kwargs):
        # Renvoie la distance calculée à vol d'oiseau entre les coordonnées.
        # Renvoie une durée fixe de 16 min 20 sec.
        response = {'routes': [{
            'bbox': [6.820271, 46.996063, 6.944324, 47.095443],
            'summary': {'distance': distance_vo(*coords) * 1000, 'duration': 980.1}
        }]}
        if kwargs.get('geometry') is True:
            response['routes'][0]['geometry'] = (
                '}te~Gcseh@u@yAQASNEFCHE`@@J@J^~@CDi@aAGCE@BBv@|Aj@hAqAa@_@Q]YgAcA[l@WW_@YiB'
                'aAwCoCIKGNMXCHM\\ITGECCC?MQSYEGqAmDCEEMaBoEQ_@S]KOS[CEgAeBQ_@Wo@g@wACKqA}DG'
                'O_@y@_@w@{@kBCEWw@Sm@]sAk@uB_@uAIWM]IQMWUc@CE[c@_@c@w@}@[_@e@i@_@c@[]OSAAY_'
                '@KQU[Ye@g@gAMWGQa@iA_@mAg@iBOm@YuAQw@E[BE@G?ICGIKEAE?CBIO}@mB_@y@u@yAeFcK_@'
                'k@Y]u@o@[OgA_@cBa@]Q]Qg@c@W[q@cAYm@c@yA[cBmCeSgAyHq@cDaAyDe@_B[s@aBuCcFgIm@e'
                'A_AiByVok@qA{Cg@uAe@}A_@uBOaBKgC?gE@gAJ_AJKHODU?UCUISKKYSWWGSS{@k@gBa@}@{@g'
                'Be@iAuAgFgAyEo@gCSs@W_AFGDUEUCGGEM?GBGQg@oAc@_Ae@_AyByEuEsKUq@UcAMiAKmAA]C_@'
                'IaBEs@Iq@COCQe@mBAIIQa@}@[k@m@}@QWQWg@s@o@aAg@o@_@m@m@kADGBUAKCICGMCE?GBGLMU'
                '{B}Dq@mAKQmAwBUe@S]Uc@kIuPMUMYyAsCYk@y@}Ai@kAQ]q@uA[k@OYBGBQCQEGECK@EBCFCP@H'
                'UPu@f@e@d@QRILKNIJkEpFKLSVGHSXMYgHuNS_@GOcDsGKSU\\GH{@tA_@n@OTQVm@~@u@lAe@v@'
                'MRMUg@{@gAkBi@_AIMCE?AGIcDwFIOcBuCOWMUuEcIs@oA}AmCk@aAgAmBe@y@Ua@U_@OTIPeAdBc'
                'A~A}@{Am@cAuBoDYe@s@uAWOi@YMC[B[HULu@\\GBWLm@VMF}@^WLQJ]NQBQB]GSCMC}AWCASCiAK'
                'YCOAuABWHUPMUQ[qEuHGMIO'
            )
        return response


class DataMixin(BaseDataMixin):
    @classmethod
    def setUpTestData(cls):
        patch('common.distance.Client', new=FakeORSClient).start()
        try:
            cls.addClassCleanup(patch.stopall)
        except AttributeError:
            # When called from LiveTests.setUp
            pass
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(
                content_type__model='client', codename__in=['view_client', 'change_client'])
            )
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_transport', 'change_transport', 'view_benevole', 'view_vehicule', 'gestion_finances',
                'change_vehiculeoccup',
            ])),
        )
        cls.trclient = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Léa', genre='F',
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.815, 47.094], valide_des=date(2022, 1, 1),
        )
        cls.hne_ne = Adresse.objects.create(
            nom='Hôpital neuchâtelois', rue='Rue de la Maladière 45', npa='2000', localite='Neuchâtel',
            empl_geo=[6.943, 46.996], validite=(date(2020, 1, 1), None),
        )
        cls.demain_1130 = datetime.combine(
            date.today() + timedelta(days=1), time(11, 30), tzinfo=get_current_timezone()
        )

    @classmethod
    def create_transport(cls, adr_interm_aller=None, adr_interm_retour=None, typ=Trajet.Types.MEDIC, **kwargs):
        origine = kwargs.pop('origine', None)
        destination = kwargs.pop('destination', None)
        heure_depart = kwargs.pop('heure_depart', kwargs['heure_rdv'] - timedelta(seconds=1525))
        kwargs['date'] = kwargs['heure_rdv'].date()
        if 'chauffeur' in kwargs:
            kwargs['chauff_aller_dist'] = D("4200")
        transport = Transport.objects.create(**kwargs)
        if not transport.client.adresse().empl_geo:
            raise ValueError("Unable to create transport with non-gelocalized clients")
        destination_domicile = not transport.retour and origine is not None
        if adr_interm_aller:
            tr = Trajet.objects.create(
                transport=transport, typ=typ,
                heure_depart=heure_depart,
                origine_domicile=origine is None, origine_adr=origine,
                destination_domicile=False, destination_adr=adr_interm_aller,
                destination_princ=False,
            )
            tr.calc_trajet()
            origine = adr_interm_aller
            heure_depart += timedelta(seconds=600)
        aller = Trajet.objects.create(
            transport=transport, typ=typ,
            heure_depart=heure_depart,
            origine_domicile=origine is None,
            origine_adr=origine,
            destination_domicile=destination_domicile,
            destination_adr=destination,
            destination_princ=True,
        )
        aller.calc_trajet()
        if transport.retour:
            assert transport.duree_rdv is not None, "La durée de rdv est obligatoire"
            heure_depart = transport.heure_rdv + transport.duree_rdv
            origine_adr = aller.destination
            if adr_interm_retour:
                tr = Trajet.objects.create(
                    transport=transport, typ='medic',
                    heure_depart=heure_depart,
                    origine_domicile=False, origine_adr=origine_adr,
                    destination_domicile=False, destination_adr=adr_interm_retour,
                    destination_princ=False,
                )
                tr.calc_trajet()
                heure_depart += timedelta(hours=1)
                origine_adr = adr_interm_retour
            tr = Trajet.objects.create(
                transport=transport, typ='medic',
                heure_depart=heure_depart,
                origine_domicile=False, origine_adr=origine_adr,
                destination_domicile=True,
                destination_adr=None,
                destination_princ=False,
            )
            tr.calc_trajet()
        if (
            transport.statut >= Transport.StatutChoices.RAPPORTE and
            fact_policy().CALCUL_KM_AUTO and not transport.km
        ):
            transport.calculer_km_auto()
        return transport

    @classmethod
    def create_modele(cls, **kwargs):
        dest = kwargs.pop('destination', cls.hne_ne)
        modele_data = {
            'client': cls.trclient,
            'date': (date.today() - timedelta(days=60)).strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'destination': dest.pk if dest else None,
            'retour': False,
            'typ': 'medic',
            'repetition': 'DAILY',
            'repetition_fin': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': dest or '-1',
            'trajets-0-destination_adr_select': '',
            'trajets-0-destination_princ': 'True',
        }
        modele_data.update(kwargs)
        if modele_data['retour']:
            modele_data.update({
                'duree_rdv': '00:45',
                'trajets-TOTAL_FORMS': '2',
                'trajets-1-id': '',
                'trajets-1-ORDER': '1',
                'trajets-1-destination_princ': 'False',
            })
        form = SaisieTransportForm(data={**modele_data, 'client_select': 'any'}, instance=None)
        if not form.is_valid():
            cls.fail(cls, [form.errors] + form.formset.errors)
        transp = form.save()
        if "vehicule" in kwargs:
            transp.modele.vehicule = kwargs["vehicule"]
            transp.modele.save()
        return transp.modele

    @classmethod
    def create_chauffeur(cls, **kwargs):
        benev_kwargs = dict(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT],
            empl_geo=[6.82, 47.087],  # CdF
        )
        benev_kwargs.update(kwargs)
        chauffeur = cls._create_benevole(
            **benev_kwargs,
            utilisateur=Utilisateur.objects.create_user(
                'benev@example.org', '1234',
                first_name=benev_kwargs["prenom"], last_name=benev_kwargs["nom"],
            )
        )
        chauffeur.utilisateur.user_permissions.add(Permission.objects.get(codename='view_transport'))
        return chauffeur


class TransportTests(TempMediaRootMixin, DataMixin, TestCase):

    def test_transport_admin(self):
        user = Utilisateur.objects.create_superuser(
            'admin@example.org', 'mepassword', first_name='Super', last_name='Admin',
        )
        self.client.force_login(user)
        response = self.client.get(reverse("admin:transport_transport_changelist") + "?q=test")
        self.assertEqual(response.context["results"], [])
        response = self.client.get(reverse("admin:transport_transportmodel_changelist") + "?q=test")
        self.assertEqual(response.context["results"], [])

    def test_est_passe(self):
        transport = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True
        )
        self.assertFalse(transport.est_passe)
        hier = date.today() - timedelta(days=1)
        hier_1130 = datetime.combine(hier, time(11, 30), tzinfo=get_current_timezone())
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=hier_1130, duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True
        )
        self.assertTrue(transport.est_passe)
        transport = self.create_transport(
            client=self.trclient, heure_rdv=hier_1130, destination=self.hne_ne,
            retour=False
        )
        # Simuler impossibilité de calculer heure_arrivee (e.g. serveur ORS down)
        with patch('transport.models.Transport.heure_arrivee', new_callable=PropertyMock) as mocked:
            mocked.return_value = None
            transport.est_passe
            self.assertTrue(transport.est_passe)

    def test_client_derniers_transports(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-client-derniers') + f'?client={self.trclient.pk}')
        self.assertNotContains(response, "data-origid")
        self.assertContains(response, f'data-destid="{self.hne_ne.pk}"')
        self.assertContains(response, f"{self.demain_1130.strftime('%d.%m.%Y')} à 11:10 (Rdv. 11:30)")

        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        AdressePresence.objects.create(
            adresse=AdresseClient.objects.create(persona=self.trclient.persona, hospitalisation=True),
            depuis=date.today() - timedelta(days=5)
        )
        self.create_transport(
            client=self.trclient,
            origine=pharma, destination=self.hne_ne,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        response = self.client.get(reverse('transport-client-derniers') + f'?client={self.trclient.pk}')
        self.assertContains(response, f'data-origid="{pharma.pk}"')
        self.assertContains(response, f'data-destid="{self.hne_ne.pk}"')
        self.assertContains(response, "Actuellement à l’hôpital")

    @patch('httpx.get', side_effect=mocked_httpx)
    def test_adresse_ajout(self, mock):
        self.user.user_permissions.add(
            Permission.objects.get(codename='change_adresse'),
        )
        self.client.force_login(self.user)
        form_data = {
            'nom': 'Dentiste Toutdoux',
            'rue': 'Seyon 10',
            'npalocalite': '2000 Neuchâtel',
            'tel': '+41321234567',
            'validite_0': '2023-01-05',
            'validite_1': '',
        }
        response = self.client.post(reverse('transport-address-add'), data=form_data)
        self.assertEqual(response.json()['result'], "OK")
        toutdoux = Adresse.objects.get(nom="Dentiste Toutdoux")
        self.assertEqual(toutdoux.tel, "032 123 45 67")
        # Contrôles unicité sur le nom (ajout + édition)
        response = self.client.post(reverse('transport-address-add'), data=form_data)
        error_msg = "Il existe déjà une adresse pour ce nom dans la même plage de validité."
        self.assertContains(response, error_msg)
        adresse = Adresse.objects.create(
            nom="Dentiste", rue='Seyon 10', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        response = self.client.post(reverse('transport-address-edit', args=[adresse.pk]), data=form_data)
        self.assertContains(response, error_msg)
        # Si ancienne archivée, nouvelle OK
        toutdoux.validite = (toutdoux.validite.lower, date.today())
        toutdoux.save(update_fields=['validite'])
        response = self.client.post(reverse('transport-address-add'), data={
            **form_data, 'validite_0': date.today() + timedelta(days=1),
        })
        self.assertEqual(response.json()['result'], "OK")

    def test_adresse_liste(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel', tel='032 222 11 00',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-addresses'))
        self.assertContains(response, '<th>Nom</th><th>Adresse</th>')
        self.assertQuerySetEqual(response.context['object_list'], [self.hne_ne, pharma])
        # Filtrage
        response = self.client.get(reverse('transport-addresses') + '?nom=pharma')
        self.assertQuerySetEqual(response.context['object_list'], [pharma])
        response = self.client.get(reverse('transport-addresses') + '?tel=211')
        self.assertQuerySetEqual(response.context['object_list'], [pharma])
        response = self.client.get(reverse('transport-addresses') + '?npalocalite=neuchat')
        self.assertQuerySetEqual(response.context['object_list'], [self.hne_ne, pharma])

    def test_adresse_autocomplete(self):
        dans_2_jours = date.today() + timedelta(days=2)
        _, a2, a3 = Adresse.objects.bulk_create([
            Adresse(nom='Hôpital ancien',  empl_geo=[6.9, 46.9],
                    validite=(date(2020, 1, 1), date.today() - timedelta(days=2))),
            Adresse(nom='Hôpital actuel',  empl_geo=[6.9, 46.9],
                    validite=(date(2020, 1, 1), dans_2_jours)),
            Adresse(nom='Hôpital nouveau',  empl_geo=[6.9, 46.9],
                    validite=(dans_2_jours, None)),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('adresse-autocomplete') + "?q=hop")
        self.assertEqual(response.json(), [
            {'label': "Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel", 'value': self.hne_ne.pk},
            {'label': f"Hôpital actuel, -,   (jusqu’au {dans_2_jours.strftime('%d.%m.%Y')})", 'value': a2.pk},
            {'label': f"Hôpital nouveau, -,   (dès le {dans_2_jours.strftime('%d.%m.%Y')})", 'value': a3.pk},
        ])

    def test_adresse_archive(self):
        self.user.user_permissions.add(
            Permission.objects.get(codename='change_adresse'),
        )
        adr = Adresse.objects.create(
            nom='Hôpital neuchâtelois à archiver', rue='Rue de la Maladière 45', npa='2000', localite='Neuchâtel',
            empl_geo=[6.94, 46.99], validite=(date(2020, 1, 1), None),
        )
        self.client.force_login(self.user)
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(reverse('transport-address-edit', args=[adr.pk]), data={
                'nom': adr.nom, 'rue': adr.rue, 'npalocalite': '2000 Neuchâtel',
                'validite_0': '2020-01-01', 'validite_1': '2024-08-31',
            })
        self.assertEqual(response.json()['result'], "OK")
        self.assertIs(Adresse.objects.actives().filter(pk=adr.pk).exists(), False)

    def test_calcul_depart(self):
        self.client.force_login(self.user)
        form_data = {
            'client': self.trclient.pk,
            'origine': '',
            'destination': [self.hne_ne.pk],
            'date': date.today() + timedelta(days=5),
            'heure_rdv': '10:00',
        }
        calcul_url = reverse('transport-calculer-depart')

        for heure_rdv, depart_expected in [
            ('10:00', '09:35'),
            ('10:25', '10:00'),  # Tester l'arrondi aux alentours de l'heure
            ('00:15', '23:50'),  # Tester passage jour
            ('00:25', '00:00'),  # Arrondi heure près du passage du jour
        ]:
            with self.subTest(f"Rendez-vous à {heure_rdv}"):
                form_data['heure_rdv'] = heure_rdv
                response = self.client.post(calcul_url, data=form_data)
                self.assertEqual(response.json(), {
                    'result': 'OK', 'duree_trajet': '00:16', 'depart': depart_expected
                })

        # Retour vers domicile
        form_data['origine'] = self.hne_ne.pk
        form_data['destination'] = '-1'
        form_data['heure_rdv'] = '10:25'
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:16', 'depart': '10:00'})
        # Ne pas crasher si origine manque
        form_data['origine'] = ''
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(
            response.json(),
            {'result': 'error', 'reason': 'Origine et destination doivent être différentes.'}
        )
        # Plusieurs destinations
        adr2 = Adresse.objects.create(
            nom='Permanence Volta', rue='Rue Numa-Droz 187', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.8145, 47.095], validite=(date(2020, 1, 1), None),
        )
        form_data['destination'] = [adr2.pk, self.hne_ne.pk]
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:32', 'depart': '09:40'})

    def test_dist_calc_chauffeur_annule(self):
        hier_1130 = self.demain_1130 - timedelta(days=2)
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport_aller = self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=hier_1130, heure_depart=hier_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.ANNULE,
        )
        self.assertEqual(transport_aller.dist_calc_chauffeur, D("8400"))

    def test_transport_avec_adresse_depose(self):
        adr = AdresseClient.objects.create(
            persona=self.trclient.persona, rue="Rue du Stand 1", npa="2000", localite="Neuchâtel",
            depose=True, empl_geo=[6.8, 47.1]
        )
        AdressePresence.objects.create(adresse=adr, depuis=date.today())
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.SAISI,
        )
        self.assertEqual(transport.origine.rue, "Rue du Stand 1")
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.SAISI,
        )
        self.assertEqual(transport.destination.rue, "Rue du Stand 1")

    def test_edit_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(
            response,
            f'<input type="date" name="date" value="{self.demain_1130.date()}" '
            'class="form-control" required="" id="id_date">',
            html=True
        )
        self.assertContains(response, 'value="11:10"')  # Heure de départ
        # Now POSTing (changed: typ, remarques, heure_depart, heure_rdv, duree_rdv)
        form_data = {
            'typ': 'emplettes',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '15:40',
            'heure_rdv': '16:00',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '1',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '2',
        }
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(transport.journaux.count(), 1)
        trajet1, trajet2 = transport.trajets_tries
        self.assertTrue(trajet1.destination_princ)
        self.assertEqual(localtime(trajet1.heure_depart).time(), time(15, 40))
        self.assertEqual(trajet1.typ, 'emplettes')
        self.assertEqual(localtime(trajet2.heure_depart).time(), time(16, 45))
        self.assertEqual(trajet2.typ, 'emplettes')
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(response, '<option value="emplettes" selected>Emplettes à deux</option>', html=True)
        # Change only typ:
        form_data.update({
            'trajets-0-id': trajet1.pk,
            'trajets-1-id': trajet2.pk,
            'typ': 'medic',
        })
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        transport.refresh_from_db()
        trajet1, trajet2 = list(transport.trajets.all())
        self.assertEqual(trajet1.typ, 'medic')
        self.assertEqual(trajet2.typ, 'medic')

    def test_edit_transport_aller(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        # L'adresse retour est masquée, mais elle doit contenir l'adresse de départ pour le cas
        # où le transport est transformé en aller-retour.
        self.assertContains(
            response,
            '<p id="adresse_retour" class="fst-italic">Rue des Crêtets 92, 2300 La Chaux-de-Fonds</p>',
            html=True
        )

    def test_edit_transport_intermediaire_aller(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            adr_interm_aller=pharma,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.assertEqual(transport.destination, self.hne_ne)
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(response, 'Pharmacie')
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data={
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:40',  # modifié
            'heure_rdv': '12:00',  # modifié
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'origine_adr': '',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': pharma.pk,
            'trajets-0-destination_adr_select': 'Pharma',
            'trajets-0-destination_princ': 'False',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-ORDER': '2',
            'trajets-1-destination_adr': self.hne_ne.pk,
            'trajets-1-destination_adr_select': 'HNE',
            'trajets-1-destination_princ': 'True',
            'trajets-2-id': '',
            'trajets-2-ORDER': '',
            'trajets-2-destination_princ': 'False',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(len(transport.trajets_tries), 2)
        self.assertEqual(transport.destination, self.hne_ne)

    def test_edit_transport_changed_data(self):
        """
        Enregistrer un transport sans modifier les champs, changed_data doit être vide.
        """
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        premier_trajet = transport.trajets_tries[0]
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
        }
        form = SaisieTransportForm(data=form_data, instance=transport, initial={'typ': premier_trajet.typ})
        self.assertEqual(form.changed_data, [])

    def test_edit_transport_date(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        premier_trajet = transport.trajets_tries[0]
        dans4jours = (self.demain_1130 + timedelta(days=4)).date()
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': dans4jours.strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
        }
        form = SaisieTransportForm(data=form_data, instance=transport, initial={'typ': premier_trajet.typ})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.changed_data, ['date'])
        form.save()
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(transport.date, dans4jours)
        self.assertEqual([traj.heure_depart.date() for traj in transport.trajets_tries], [dans4jours])

    def test_edit_transport_dest_avec_chauffeur(self):
        """
        Les champs chauff_aller_dist/chauff_retour_dist doivent être recalculés si les
        trajets changent et que le chauffeur est déjà attribué.
        """
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE,
            chauffeur=chauffeur,
        )
        transport.chauff_aller_dist = transport.chauff_retour_dist = 100
        transport.save()
        autre_dest = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        premier_trajet = transport.trajets_tries[0]
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.strftime('%Y-%m-%d'),
            'heure_depart': transport.heure_depart.strftime('%H:%M'),
            'heure_rdv': transport.heure_rdv.strftime('%H:%M'),
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': autre_dest.pk,
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport.refresh_from_db()
        self.assertEqual(transport.chauff_aller_dist, 9400)
        self.assertEqual(transport.chauff_retour_dist, 8200)

    def test_edit_transport_rapporte_ar_vers_aller_simple(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            duree_eff=timedelta(hours=1), km=D(10.5), temps_attente=timedelta(hours=3),
            statut=Transport.StatutChoices.RAPPORTE,
        )
        form_data = {
            'typ': 'medic',
            'remarques': 'Aller-retour vers aller simple',
            'retour': 'False',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '01:10',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '1',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-destination_adr': '-1',
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '2',
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(len(transport.trajets_tries), 1)
        self.assertFalse(transport.retour)
        self.assertIsNone(transport.temps_attente)

    def test_edit_transport_simple_vers_allerretour(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        form_data = {
            'typ': 'medic',
            'remarques': 'Aller simple vers aller-retour',
            'retour': 'True',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',  # manquant
            'trajets-INITIAL_FORMS': '1',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '0',
            'trajets-1-id': '',
            'trajets-1-destination_adr': '-1',
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '1',
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertEqual(
            response.context['form'].errors,
            {'duree_rdv': ['La durée de rendez-vous est obligatoire.']}
        )

    def test_edit_transport_allerretour_vers_simple(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, duree_rdv=timedelta(minutes=30), retour=True,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        transport.calc_chauffeur_dists(save=True)
        chauff_aller_dist_before = transport.chauff_aller_dist
        chauff_retour_dist_before = transport.chauff_retour_dist
        form_data = {
            'typ': 'medic',
            'remarques': 'Aller-retour vers aller simple',
            'retour': 'False',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '1',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-destination_adr': '-1',
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '2',
        }
        self.client.force_login(self.user)
        self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        transport.refresh_from_db()
        self.assertFalse(transport.retour)
        # Tester si distances chauffeur ont été calculées correctement
        self.assertEqual(transport.chauff_aller_dist, chauff_aller_dist_before)
        self.assertNotEqual(transport.chauff_retour_dist, chauff_retour_dist_before)

    def test_edit_transport_retour_dom(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70),
            origine=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertEqual(
            response.context['form'].formset.forms[0].instance,
            transport.trajets_tries[0]
        )
        self.assertContains(
            response,
            f'<input type="hidden" name="origine_adr" value="{self.hne_ne.pk}" '
            'class="form-control" id="id_origine_adr">',
            html=True
        )
        self.assertContains(
            response,
            '<p class="fst-italic" id="adresse_depart">Hôpital neuchâtelois, '
            'Rue de la Maladière 45, 2000 Neuchâtel</p>',
            html=True
        )

    def test_edit_transport_without_ors(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        transport.trajets.update(dist_calc=None, duree_calc=None)
        self.client.force_login(self.user)
        with patch('transport.models.distance_real', side_effect=ORSUnavailable()):
            response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(response, "est actuellement indisponible")

    def test_attribution_transport(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.795, 47.08],
            remarques="Pas plus de 50km",
        )
        self.trclient.handicaps = [Handicaps.CHAISE, Handicaps.AUDITION, Handicaps.DEAMBULATEUR]
        self.trclient.save()
        allerretour = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        allersimple = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-attrib', args=[allerretour.pk]))
        self.assertEqual(len(response.context['chauffeurs']), 1)
        self.assertContains(
            response, '<div class="alert alert-warning">Handicap auditif, Déambulateur</div>', html=True
        )
        detail_url = reverse("transport-client-detail", args=[self.trclient.pk])
        self.assertContains(response, f'Pour <b><a href="{detail_url}">Donzé Léa</a></b>')
        # Remarques du chauffeur sur icon dans span
        self.assertContains(response, 'title="Pas plus de 50km"')
        response = self.client.get(reverse('transport-attrib', args=[allersimple.pk]))
        self.assertContains(response, f'Pour <b><a href="{detail_url}">Donzé Léa</a></b>')

        response = self.client.post(reverse('transport-attrib', args=[allerretour.pk]), data={
            'chauffeur': chauffeur.pk,
        })
        self.assertRedirects(response, reverse('gestion'))
        allerretour.refresh_from_db()
        self.assertEqual(allerretour.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(allerretour.chauff_aller_dist, 2200)
        self.assertEqual(allerretour.chauff_retour_dist, 2200)

    def test_attribution_transport_avec_vehicule(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.795, 47.08]
        )
        vehicule = Vehicule.objects.create(
            modele='Vrooom'
        )
        allersimple = self.create_transport(
            client=self.trclient,  heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-attrib', args=[allersimple.pk]))
        self.assertContains(response, 'Réserver un véhicule')
        # Première fois sans chauffeur
        response = self.client.post(reverse('transport-attrib', args=[allersimple.pk]), data={
            'vehicule': vehicule.pk,
        })
        self.assertRedirects(response, reverse('gestion'))
        allersimple.refresh_from_db()
        self.assertEqual(allersimple.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(allersimple.chauffeur)
        self.assertEqual(allersimple.vehicule, vehicule)
        # Cette fois avec chauffeur
        response = self.client.post(reverse('transport-attrib', args=[allersimple.pk]), data={
            'chauffeur': chauffeur.pk,
            'vehicule': vehicule.pk,
        })
        self.assertRedirects(response, reverse('gestion'))
        allersimple.refresh_from_db()
        self.assertEqual(allersimple.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(allersimple.chauffeur, chauffeur)
        self.assertEqual(allersimple.vehicule, vehicule)

    def test_attribution_transport_avec_vehicule_chauffeur_externe(self):
        vehicule = Vehicule.objects.create(
            modele='Vrooom'
        )
        allersimple = self.create_transport(
            client=self.trclient,  heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-attrib', args=[allersimple.pk]), data={
            'chauffeur': '',
            'chauffeur_externe': 'on',
            'vehicule': vehicule.pk,
        })
        self.assertRedirects(response, reverse('gestion'))
        allersimple.refresh_from_db()
        self.assertEqual(allersimple.statut, Transport.StatutChoices.CONFIRME)
        self.assertEqual(allersimple.vehicule, vehicule)

    def test_desattribution_transport(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-attrib-remove', args=[transport.pk]))
        self.assertRedirects(response, reverse('gestion'))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(transport.chauffeur)

    def test_refus_transport(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI, chauffeur=None,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-refuse', args=[transport.pk]), data={
            'chauffeur': chauffeur.pk,
        })
        self.assertEqual(response.json(), {'result': 'OK', 'redirect': None})
        self.assertEqual(Refus.objects.filter(chauffeur=chauffeur).count(), 1)

    def test_autoattribution_transport(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        modele = self.create_modele(
            date=self.demain_1130 - timedelta(days=4),
            heure_depart='15:15',
            heure_rdv='15:30',
        )

        self.client.force_login(self.user)
        auto_url = reverse(
            'transport-autoattrib',
            args=[self.demain_1130.year, self.demain_1130.month, self.demain_1130.day]
        )
        response = self.client.post(auto_url, data={})
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))
        transport.refresh_from_db()
        # Chauffeur sans dispo, pas d'attribution auto
        self.assertIsNone(transport.chauffeur)

        Dispo.objects.create(
            chauffeur=chauffeur,
            debut=self.demain_1130 - timedelta(days=7, hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=6),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(auto_url, data={}, follow=True)
        self.assertContains(response, "2 transports ont été attribués pour le ")
        self.assertEqual(chauffeur.transports.count(), 2)
        transport.refresh_from_db()
        self.assertEqual(transport.chauffeur, chauffeur)
        self.assertEqual(transport.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(len(transport.trajets_tries), 2)
        transport2 = modele.transports.get(date=self.demain_1130.date())
        self.assertEqual(transport2.chauffeur, chauffeur)
        self.assertEqual(transport2.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(len(transport2.trajets_tries), 1)

    def test_annulation_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        form_data = {
            'motif_annulation': Transport.AnnulationChoices.CLIENT_ANNUL,
            'raison_annulation': "Trouvé autre moyen",
            'km': '4',
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'true',
        }
        response = self.client.post(reverse('transport-cancel', args=[transport.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)
        self.assertEqual(transport.date_annulation.date(), date.today())
        self.assertEqual(transport.journaux.latest().description, "Annulation du transport")
        # Annuler l'annulation!
        form_data["annule"] = ""
        response = self.client.post(reverse('transport-cancel', args=[transport.pk]), data=form_data)
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(transport.date_annulation)
        self.assertEqual(transport.raison_annulation, '')
        self.assertEqual(transport.statut_fact, Transport.FacturationChoices.FACTURER)
        self.assertIsNone(transport.defrayer_chauffeur)
        self.assertEqual(
            transport.journaux.latest().description,
            "Le transport qui était annulé a été réactivé"
        )

    def test_annulation_transport_erreurs(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        form = CancelForm(instance=transport, data={
            'raison_annulation': "Va savoir",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': True,
            'km': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['km'], ["Vous devez indiquer les kilomètres pour défrayer le chauffeur."])
        # Ce scénario n'est plus testé, car cette option de statut_fact est actuellement masquée (#355)
        '''
        form = CancelForm(instance=transport, data={
            'raison_annulation': "Va savoir",
            'statut_fact': Transport.FacturationChoices.FACTURER_ANNUL_KM,
            'defrayer_chauffeur': False,
            'km': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['km'], ["Vous devez indiquer les kilomètres pour facturer les km du chauffeur."])
        '''

    def test_confirmation_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        self.client.force_login(self.user)
        confirm_url = reverse('transport-confirm')
        response = self.client.post(confirm_url, data={'selection': []}, follow=True)
        self.assertContains(response, "Vous n’avez pas sélectionné de transport")
        response = self.client.post(confirm_url, data={'selection': [str(transport.pk)]}, follow=True)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.CONFIRME]))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.CONFIRME)

    def test_liste_transports_vide(self):
        self.client.force_login(self.user)
        saisi_url = reverse('transports-by-status', args=[Transport.StatutChoices.SAISI])
        response = self.client.get(saisi_url, HTTP_X_REQUESTED_WITH='fetch')
        # Django 4.2:
        #response = self.client.get(saisi_url, headers={'X-Requested-With': 'fetch'})
        self.assertContains(response, "Cette liste est actuellement vide.")

    def test_liste_transports_saisis_par_jour(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_modele(
            date=self.demain_1130 - timedelta(days=4),
            heure_depart='15:15',
            heure_rdv='15:30',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse(
            'transports-by-status-and-day',
            args=[Transport.StatutChoices.SAISI, self.demain_1130.strftime('%Y-%m-%d')]
        ), HTTP_X_REQUESTED_WITH='fetch')
        # Tester présence transport unique + occurrence modèle pour ce jour
        self.assertEqual(
            [tr.heure_depart.hour for tr in response.context['object_list'][self.demain_1130.date()]['transports']],
            [11, 15]
        )

    def test_liste_transports_filtres(self):
        chauffeur = self._create_benevole(nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT])
        client2 = Client.objects.create(
            service=TRANSPORT, nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        adr2 = Adresse.objects.create(
            nom='Permanence Volta', rue='Rue Numa-Droz 187', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.8145, 47.095], validite=(date(2020, 1, 1), None),
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=adr2, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130 + timedelta(seconds=7200),
            origine=adr2, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130 + timedelta(seconds=14400),
            duree_rdv=timedelta(minutes=70), destination=adr2, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur,
        )
        # Modèle aller-retour vers NE
        self.create_modele(
            date=self.demain_1130.strftime('%Y-%m-%d'),
            retour=True,
            repetition='WEEKLY',
        )
        # Modèle pour aller simple retour domicile CdF (pendant 15j = 2x)
        self.create_modele(
            date=self.demain_1130.strftime('%Y-%m-%d'),
            origine_adr=self.hne_ne.pk,
            destination=None,
            repetition='WEEKLY',
            repetition_fin=(self.demain_1130 + timedelta(days=15)).strftime('%Y-%m-%d'),
        )

        def count_transports(response):
            return sum(val['len'] for val in response.context['object_list'].values())

        self.client.force_login(self.user)
        saisi_url = reverse('transports-by-status', args=[Transport.StatutChoices.SAISI])
        response = self.client.get(saisi_url, HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(count_transports(response), 15)
        self.assertNotContains(response, "Défraiements du mois précédent")
        response = self.client.get(
            f'{saisi_url}?date={self.demain_1130.strftime("%Y-%m-%d")}', HTTP_X_REQUESTED_WITH='fetch'
        )
        self.assertEqual(count_transports(response), 5)
        response = self.client.get(f'{saisi_url}?client=mûll', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(count_transports(response), 2)
        response = self.client.get(f'{saisi_url}?destination=chaux', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(
            {dt: ([tr.client for tr in lst['transports']], lst['filled'])
             for dt, lst in response.context['object_list'].items()
            },
            {
                self.demain_1130.date(): ([client2], True),
                self.demain_1130.date() + timedelta(days=7): ([self.trclient], True),
                self.demain_1130.date() + timedelta(days=14): ([self.trclient], True),
            }
        )

        attr_url = reverse('transports-by-status', args=[Transport.StatutChoices.ATTRIBUE])
        response = self.client.get(f'{attr_url}?chauffeur=dupl', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_liste_transports_controles(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT], empl_geo=[6.81, 47.11]
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE, chauffeur=chauffeur,
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
        )
        # Transport facturé
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
            facture=Facture.objects.create(
                client=self.trclient, mois_facture=date(2023, 1, 1), date_facture=date(2023, 2, 10),
                nb_transp=1,
            ),
        )
        # Transport non facturé mais ne doit pas l'être
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
            facture=None, statut_fact=Transport.FacturationChoices.PAS_FACTURER,
        )
        self.client.force_login(self.user)
        url = reverse('transports-by-status', args=[Transport.StatutChoices.CONTROLE])
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(
            sum(val['len'] for val in response.context['object_list'].values()),
            1
        )

    def test_transports_par_jour(self):
        # Test minimal pour le moment
        chaque_jour = Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0]
        regions = RegionFinder.regions()
        region1 = list(regions.items())[0]
        npa_region1 = str(region1[1][0][0])
        adr = self.trclient.adresse()
        adr.npa = npa_region1
        adr.save()
        # Chauffeur avec disponibilité et transport
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT],
            npa=npa_region1, localite="Région1", empl_geo=[6.81, 47.11],
        )
        # Dispo ancien chauffeur ne doit pas apparaître
        ancien_chauffeur = self._create_benevole(
            nom="Duplain", prenom="Georges", activites=[TypeActivite.TRANSPORT],
            npa="2800", localite="Delémont", empl_geo=[6.81, 47.11],
        )
        ancien_chauffeur.activite_set.all().update(duree=("2024-10-01", "2024-12-01"))
        Dispo.objects.create(
            chauffeur=ancien_chauffeur,
            debut=datetime(2024, 10, 2, 6, 0, tzinfo=timezone.utc),
            fin=datetime(2024, 10, 2, 15, 0, tzinfo=timezone.utc),
            regle=chaque_jour,
        )

        Dispo.objects.create(
            chauffeur=chauffeur,
            debut=self.demain_1130 - timedelta(hours=5),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        # Chauffeur avec disponibilité annulée
        chauffeur_abs = self._create_benevole(
            nom="Absente", prenom="Julie", activites=[TypeActivite.TRANSPORT], empl_geo=[6.83, 47.01]
        )
        dispo_a_annuler = Dispo.objects.create(
            chauffeur=chauffeur_abs,
            debut=self.demain_1130 - timedelta(days=7, hours=5),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=1),
        )
        dispo_a_annuler.cancel_for(self.demain_1130.date())
        # Chauffeur avec dispo, mais en vacances
        chauffeur_vac = self._create_benevole(
            nom="Vacances", prenom="Lise", activites=[TypeActivite.TRANSPORT],
        )
        Dispo.objects.create(
            chauffeur=chauffeur_vac,
            debut=self.demain_1130 - timedelta(hours=3),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        Dispo.objects.create(
            chauffeur=chauffeur_vac, categorie='absence',
            debut=now().replace(hour=6, minute=0),
            fin=now().replace(hour=22, minute=0),
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence=now() + timedelta(days=7),
        )

        url_par_jour = reverse("transports-by-day", args=[
            self.demain_1130.year, self.demain_1130.month, self.demain_1130.day
        ])
        self.client.force_login(self.user)
        response = self.client.get(url_par_jour)
        self.assertEqual(list(response.context['chauffeurs'].keys()), [chauffeur])
        self.assertEqual(list(response.context['chauffeurs_dispos'].keys()), [])
        self.assertContains(response, "Duplain Irma</a> (Région1)")
        # Tester filtre par région
        response = self.client.get(url_par_jour + f"?region={region1[0]}")
        self.assertEqual(list(response.context["chauffeurs"].keys()), [chauffeur])
        region2 = list(regions.items())[1]
        response = self.client.get(url_par_jour + f"?region={region2[0]}")
        self.assertEqual(list(response.context["chauffeurs"].keys()), [])

    def test_transports_par_jour_absence_milieu_dispo(self):
        def demain_a(_time):
            return datetime.combine(
                date.today() + timedelta(days=1), _time, tzinfo=get_current_timezone()
            )

        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT], empl_geo=[6.82, 47.087]
        )
        Dispo.objects.create(
            chauffeur=chauffeur,
            debut=demain_a(time(8, 0)), fin=demain_a(time(17, 0)),
        )
        Dispo.objects.create(
            chauffeur=chauffeur, categorie="absence",
            debut=demain_a(time(11, 0)),
            fin=demain_a(time(13, 30)),
        )
        Dispo.objects.create(
            chauffeur=chauffeur, categorie="absence",
            debut=demain_a(time(15, 0)),
            fin=demain_a(time(16, 0)),
        )
        demain = date.today() + timedelta(days=1)
        url_par_jour = reverse("transports-by-day", args=[
            demain.year, demain.month, demain.day
        ])
        self.client.force_login(self.user)
        response = self.client.get(url_par_jour)
        self.assertEqual(list(response.context["chauffeurs_dispos"].keys()), [chauffeur])
        # La dispo est partagée en 2 par l'absence
        self.assertEqual(
            [localtime(disp.debut).time()
             for disp in response.context["chauffeurs_dispos"][chauffeur]["dispos"]],
            [time(8, 0), time(13, 30), time(16, 0)]
        )

    def test_timeline_style_tag(self):
        from transport.templatetags.transport_utils import timeline_style

        dispo = Dispo(debut=self.demain_1130, fin=self.demain_1130 + timedelta(hours=5))
        self.assertEqual(timeline_style(dispo), "left: 39.3%; width: 35.7%;")
        dispo = Dispo(
            debut=self.demain_1130 - timedelta(hours=11),
            fin=self.demain_1130 + timedelta(hours=11)
        )
        self.assertEqual(timeline_style(dispo), "left: 0.0%; width: 100.0%;")


class TransportSaisieTests(DataMixin, TestCase):
    def sample_transport_data(self, **kwargs):
        in_5_days = date.today() + timedelta(days=5)
        return {
            'client_select': 'Don',
            'client': self.trclient.pk,
            'depart_autre': '',
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
        } | kwargs

    def test_saisie_transport(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-saisie'))
        self.assertContains(response, "Saisir un nouveau transport")
        form_data = self.sample_transport_data(**{
            'retour': "True",
            'duree_rdv': "00:45",
            'trajets-TOTAL_FORMS': '2',
            'trajets-1-id': '',
            'trajets-1-ORDER': '',
            'trajets-1-destination_adr': '',
            'trajets-1-destination_adr_select': '',
            'trajets-1-destination_princ': 'False',
        })
        response = self.client.post(reverse('transport-saisie'), data=form_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        in_5_days = date.today() + timedelta(days=5)
        self.assertEqual(
            transp.heure_rdv.astimezone(get_current_timezone()),
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 10, 30, tzinfo=get_current_timezone())
        )
        self.assertEqual(localtime(transp.trajets_tries[0].heure_depart).time(), time(10, 15))
        self.assertEqual(localtime(transp.trajets_tries[1].heure_depart).time(), time(11, 15))
        # Distance et durée sont calculées
        self.assertEqual(transp.duree_calc, timedelta(seconds=980) * 2 + timedelta(minutes=45))
        self.assertEqual(transp.dist_calc, 14600 * 2)
        self.assertEqual(
            transp.heure_depart_retour,
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 11, 15, tzinfo=get_current_timezone())
        )
        self.assertEqual(
            transp.heure_arrivee,
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 11, 31, tzinfo=get_current_timezone())
        )
        self.assertTrue(transp.trajets_tries[0].destination_princ)
        self.assertFalse(transp.trajets_tries[1].destination_princ)

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    def test_saisie_transport_ors_unavailable(self, mocked):
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data())
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))

    def test_saisie_transport_aller(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data())
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertFalse(transp.retour)
        self.assertEqual(len(transp.trajets.all()), 1)
        self.assertEqual(transp.heure_arrivee.time(), time(10, 31))

    def test_saisie_transport_aller_vers_domicile(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data(**{
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': 'any',
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
        }))
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(transp.heure_arrivee.time(), time(10, 31))
        self.assertEqual(len(transp.trajets.all()), 1)
        trajet = transp.trajets.first()
        self.assertEqual(trajet.origine_domicile, False)
        self.assertEqual(trajet.origine_adr, self.hne_ne)
        self.assertEqual(trajet.destination_domicile, True)
        self.assertIsNone(trajet.destination_adr)
        self.assertTrue(transp.trajets_tries[0].destination_princ)

    def test_saisie_transport_aller_vers_domicile_et_retour(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data(**{
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': 'any',
            'retour': 'True',
            'duree_rdv': '02:00',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
            'trajets-1-id': '',
            'trajets-1-destination_adr': self.hne_ne.pk,
            'trajets-1-destination_adr_select': 'any',
            'trajets-1-destination_princ': 'False',
        }))
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        traj1, traj2 = transp.trajets_tries
        self.assertEqual(traj1.origine_domicile, False)
        self.assertEqual(traj1.destination_domicile, True)
        self.assertEqual(traj2.origine_domicile, True)
        self.assertEqual(traj2.destination_domicile, False)

    def test_saisie_transport_domicile_vers_domicile(self):
        """Transport avec trajet domicile-domicile n'est pas autorisé."""
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data(**{
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
        }))
        self.assertContains(response, "L’origine et la destination ne peuvent pas être les deux le domicile")

    def test_saisie_transport_trajet_intermediaire(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data(**{
            'retour': 'True',
            'duree_rdv': '00:45',
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
            'trajets-1-id': '',
            'trajets-1-ORDER': '2',
            'trajets-1-destination_princ': 'False',
            'trajets-1-destination_adr': '',
            'trajets-1-destination_adr_select': '',
            # Trajet intermédiaire (au retour)
            'trajets-2-id': '',
            'trajets-2-ORDER': '1',
            'trajets-2-destination_adr': pharma.pk,
            'trajets-2-destination_adr_select': 'Pharma',
            'trajets-2-destination_princ': 'False',
        }))
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(len(transp.trajets_tries), 3)
        self.assertIs(transp.trajets_tries[0].destination_princ, True)
        self.assertIs(transp.trajets_tries[1].destination_princ, False)
        # Distance et durée sont calculées
        self.assertEqual(transp.duree_calc, timedelta(seconds=980) * 3 + timedelta(minutes=45))
        self.assertEqual(transp.dist_calc, 9800 * 3)

    def test_saisie_transport_trajet_intermediaire_aller_simple(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651], validite=(date(2020, 1, 1), None),
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data(**{
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': '',
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'RHNe',
            'trajets-0-destination_princ': 'True',
            'trajets-2-id': '',
            'trajets-2-ORDER': '0',
            'trajets-2-destination_adr': pharma.pk,
            'trajets-2-destination_adr_select': 'Pharma',
            'trajets-2-destination_princ': 'False',
            'trajets-1-id': '',
            'trajets-1-ORDER': '2',
        }))
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(len(transp.trajets_tries), 2)
        self.assertIs(transp.trajets_tries[0].destination_princ, False)
        self.assertIs(transp.trajets_tries[1].destination_princ, True)

    def test_saisie_depart_egal_destination(self):
        form = SaisieTransportForm(data=self.sample_transport_data(**{
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': '',
            'duree_rdv': '00:45',
            'retour': 'on',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': '',
        }), instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ["L’origine et la destination d’un trajet doivent être différents"] * 2
        )

    def test_saisie_transport_client_initial(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-saisie') + f'?client={self.trclient.pk}')
        self.assertContains(
            response,
            f'<input type="hidden" name="client" value="{self.trclient.pk}" class="form-control" id="id_client">',
            html=True
        )
        self.assertContains(response, f'value="{str(self.trclient)}"')

    def test_saisie_transport_client_en_chaise(self):
        self.trclient.handicaps = [Handicaps.CHAISE]
        self.trclient.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-client-derniers') + f'?client={self.trclient.pk}')
        self.assertContains(response, "Cette personne est en chaise roulante")
        response = self.client.post(reverse('transport-saisie'), data=self.sample_transport_data())
        transp = self.trclient.transports.first()
        self.assertRedirects(response, reverse('transport-attrib', args=[transp.pk]))

    def test_saisie_transport_sans_destination(self):
        form_data = self.sample_transport_data(**{
            'heure_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': '',
            'trajets-0-destination_adr_select': '',
        })
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'heure_rdv': ['Ce champ est obligatoire.']})
        form_data['heure_rdv'] = '12:00'
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.formset.errors, [{'destination_adr': ['Ce champ est obligatoire.']}])

    def test_saisie_transport_sans_heure_depart(self):
        form_data = self.sample_transport_data(**{
            'heure_depart': '',
        })
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'heure_depart': ['Ce champ est obligatoire.']})

    def test_saisie_transport_sans_duree_rdv(self):
        form_data = self.sample_transport_data(**{
            'retour': 'on',
            'duree_rdv': '',
        })
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'duree_rdv': ['La durée de rendez-vous est obligatoire.']})
        form_data['retour'] = ''
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertTrue(form.is_valid())


class TrajetsCommunsTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = cls.create_chauffeur()

        hne_ne = Adresse.objects.create(
            nom='HNE - Neuchâtel', rue='Rue de la Maladière 45', npa='2000', localite='Neuchâtel',
            empl_geo=[6.943, 46.996], validite=(date(2020, 1, 1), None),
        )
        # Autre adresse proche de HNE-NE, possiblement destination commune
        ne_qqpart = Adresse.objects.create(
            nom='Clos Brochet', rue='Rue du Crêt-Taconnet 13', npa='2000', localite='Neuchâtel',
            empl_geo=[6.942, 46.998], validite=(date(2020, 1, 1), None),
        )

        cls.client2 = Client.objects.create(
            service=TRANSPORT, nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        cls.client3 = Client.objects.create(
            service=TRANSPORT, nom='Schmid', prenom='Germain',
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23), empl_geo=[6.820, 47.071]
        )
        cls.transport_princ = cls.create_transport(
            client=cls.trclient,
            heure_rdv=cls.demain_1130 - timedelta(minutes=10),
            duree_rdv=timedelta(minutes=70), destination=hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=cls.chauffeur
        )
        # 2 trajets potentiellement communs
        cls.transport2 = cls.create_transport(
            client=cls.client2,
            heure_rdv=cls.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=cls.chauffeur
        )
        cls.transport3 = cls.create_transport(
            client=cls.client3,
            heure_rdv=cls.demain_1130 + timedelta(minutes=50),
            duree_rdv=timedelta(minutes=70), destination=hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=cls.chauffeur
        )
        # Pas possible car trop éloigné dans le temps
        cls.create_transport(
            client=cls.client3,
            heure_rdv=cls.demain_1130 + timedelta(hours=2),
            duree_rdv=timedelta(minutes=70), destination=hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=cls.chauffeur
        )
        # Pas possible car annulé.
        cls.create_transport(
            client=cls.client2,
            heure_rdv=cls.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=ne_qqpart, retour=True,
            statut=Transport.StatutChoices.ANNULE, chauffeur=cls.chauffeur
        )

    def test_trajets_communs(self):
        self.client.force_login(self.user)
        url = reverse('trajet-a-plusieurs', args=[self.transport_princ.trajets_tries[0].pk])
        response = self.client.get(url)
        self.assertContains(response, '<input type="checkbox"', count=2)
        response = self.client.post(url, data={'trajets': [self.transport2.trajets_tries[0].pk]})
        self.assertEqual(response.json()['result'], 'OK')
        commun = TrajetCommun.objects.first()
        self.assertQuerySetEqual(
            commun.trajet_set.all().order_by('pk'),
            [self.transport_princ.trajets_tries[0], self.transport2.trajets_tries[0]]
        )
        # Édition
        response = self.client.get(url)
        self.assertContains(
            response,
            f'<input type="checkbox" name="trajets" value="{self.transport2.trajets_tries[0].pk}" '
            'id="id_trajets_0" class="form-check-input" checked>',
            html=True
        )
        response = self.client.post(url, data={'trajets': [self.transport3.trajets_tries[0].pk]})
        self.assertEqual(response.json()['result'], 'OK')
        commun.refresh_from_db()
        self.assertQuerySetEqual(
            commun.trajet_set.all().order_by('pk'),
            [self.transport_princ.trajets_tries[0], self.transport3.trajets_tries[0]]
        )
        # Suppression
        response = self.client.post(url, data={'trajets': []})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(TrajetCommun.objects.all().count(), 0)

    def test_trajets_communs_par_rapport(self):
        self.client.force_login(self.chauffeur.utilisateur)
        url = reverse('benevole-rapport', args=[self.transport_princ.pk])
        response = self.client.get(url)
        self.assertContains(
            response,
            f'<input type="checkbox" name="traj0-trajets" value="{self.transport2.trajets_tries[0].pk}" '
            'id="id_traj0-trajets_0" class="form-check-input">',
            html=True
        )
        rapport_data = {
            'annule': '',
            'traj0-trajets': str(self.transport2.trajets_tries[0].pk),
            'km': '28.5',
            'duree_eff': "00:42",
            'temps_attente': "00:15",
            'rapport_chauffeur': "Tout est OK.",
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(url, data=rapport_data)
        self.assertRedirects(response, reverse('home-app'))
        self.assertEqual(TrajetCommun.objects.all().count(), 1)
        # Enlever la coche
        del rapport_data['traj0-trajets']
        response = self.client.post(url, data=rapport_data)
        self.assertRedirects(response, reverse('home-app'))
        self.assertEqual(TrajetCommun.objects.all().count(), 0)

    def test_trajets_communs_annulation(self):
        tc = TrajetCommun.objects.create(chauffeur=self.chauffeur, date=self.demain_1130.date())
        self.transport_princ.trajets_tries[0].commun = tc
        self.transport_princ.trajets_tries[0].save()
        self.transport2.trajets_tries[0].commun = tc
        self.transport2.trajets_tries[0].save()
        self.transport3.trajets_tries[0].commun = tc
        self.transport3.trajets_tries[0].save()
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-cancel', args=[self.transport3.pk]), {
            'motif_annulation': Transport.AnnulationChoices.CLIENT_ANNUL,
            'raison_annulation': "Trouvé autre moyen",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(tc.trajet_set.count(), 2)
        self.client.post(reverse('transport-cancel', args=[self.transport2.pk]), {
            'motif_annulation': Transport.AnnulationChoices.CLIENT_ANNUL,
            'raison_annulation': "Trouvé autre moyen",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(TrajetCommun.objects.count(), 0)

    def test_trajets_communs_annulation_par_rapport(self):
        tc = TrajetCommun.objects.create(chauffeur=self.chauffeur, date=self.demain_1130.date())
        self.transport_princ.trajets_tries[0].commun = tc
        self.transport_princ.trajets_tries[0].save()
        self.transport2.trajets_tries[0].commun = tc
        self.transport2.trajets_tries[0].save()
        self.transport3.trajets_tries[0].commun = tc
        self.transport3.trajets_tries[0].save()
        self.client.force_login(self.chauffeur.utilisateur)
        rapport_data = {
            'annule': 'on',
            'km': '',
            'duree_eff': "",
            'temps_attente': "",
            'rapport_chauffeur': "J'ai personne vu!",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(
            reverse('benevole-rapport', args=[self.transport3.pk]), data=rapport_data
        )
        self.assertEqual(tc.trajet_set.count(), 2)
        self.client.post(
            reverse('benevole-rapport', args=[self.transport2.pk]), data=rapport_data
        )
        self.assertEqual(TrajetCommun.objects.count(), 0)

    def test_trajet_commun_modele_occurrence(self):
        modele = self.create_modele(
            retour=True,
            repetition_fin=(now() + timedelta(days=4)).strftime('%Y-%m-%d'),
        )
        concret = modele.transports.first()
        tc = TrajetCommun.objects.create(date=concret.date, chauffeur=self.chauffeur)
        trajet = concret.trajets_tries[0]
        trajet.commun = tc
        trajet.save()
        self.assertTrue(concret.has_trajet_commun)
        occs = modele.generer_de_a(now(), now() + timedelta(days=10))
        self.assertFalse(occs[0].has_trajet_commun)


class TransportRapportTests(TempMediaRootMixin, DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = cls._create_benevole(
            nom="Dupond", prenom="Lily3", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        cls.chauffeur.utilisateur = Utilisateur.objects.create_user(
            'benev@example.org', '1234', first_name="Dupond", last_name="Lily3",
        )

    def test_rapport_conforme(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        Frais.objects.create(transport=transport, typ='repas', cout='17.5')
        fr_park = Frais.objects.create(
            transport=transport, typ=Frais.TypeFrais.PARKING, cout='4.5'
        )
        self.assertFalse(transport.rapport_complet)
        self.assertFalse(transport.rapport_conforme)
        transport.km = (transport.dist_calc_chauffeur * D("1.03")) / 1000
        transport.duree_eff = timedelta(hours=3)
        transport.temps_attente = timedelta(hours=2)
        self.assertTrue(transport.rapport_conforme)
        # Tester divers critères qui rend le rapport non conforme
        transport.km = (transport.dist_calc_chauffeur * D("1.06")) / 1000
        self.assertFalse(transport.rapport_conforme)
        transport.km = (transport.dist_calc_chauffeur * D("1.03")) / 1000
        transport.temps_attente = timedelta(hours=4)
        self.assertFalse(transport.rapport_conforme)
        transport.temps_attente = timedelta(hours=2)
        transport.rapport_chauffeur = "Aïe aïe."
        self.assertFalse(transport.rapport_conforme)
        transport.rapport_chauffeur = ""
        fr_park.cout = 8
        fr_park.save()
        self.assertFalse(transport.rapport_conforme)
        fr_park.cout = 4.5
        fr_park.save()
        self.assertTrue(transport.rapport_conforme)

    def test_rapport_transport_par_admin(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': '',
            'km': '28.5',
            'duree_eff': "00:42",
            'temps_attente': "00:30",
            'rapport_chauffeur': "Tout est OK.",
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': transport.pk,
            'frais-0-cout': '14.5',
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png'),
        }
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.journaux.latest().description, "Rapport initial du transport")
        self.assertEqual(transport.statut, Transport.StatutChoices.RAPPORTE)
        self.assertEqual(transport.frais.count(), 1)
        self.assertTrue(transport.frais.first().justif.name.startswith("justificatifs/test"))
        # Resending same data twice should not duplicating frais formsets
        rapport_data['frais-0-justif'] = SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png')
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        transport.refresh_from_db()
        self.assertEqual(transport.frais.count(), 1)

        # Second edition
        rapport_data.update({
            'frais-INITIAL_FORMS': '1',
            'temps_attente': "00:35",
            'frais-0-id': transport.frais.first().pk,
            'frais-0-justif': '',
        })
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        self.assertEqual(
            transport.journaux.latest().description,
            "Modification du rapport: temps d’attente (de «00:30» à «00:35»)"
        )

    def test_rapport_transport_conforme(self):
        """Le rapport est conforme et passe directement en "contrôlé"."""
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': '',
            'km': int(transport.dist_calc / 1000) + 0.5,
            'duree_eff': "00:42",
            'temps_attente': "00:30",
            'rapport_chauffeur': "",
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': transport.pk,
            'frais-0-cout': '14.5',
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type="image/png"),
        }
        rapport_url = reverse("transport-rapport", args=[transport.pk])
        response = self.client.get(rapport_url)
        # Le temps d'attente est vide par défaut
        self.assertContains(response, (
            '<input type="text" name="temps_attente" placeholder="HH:MM" '
            'class="duration form-control" aria-describedby="id_temps_attente_helptext"'
            ' id="id_temps_attente">'
        ))
        response = self.client.post(rapport_url, data=rapport_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.CONTROLE)
        self.assertEqual(transport.temps_attente, timedelta(minutes=30))

    def test_rapport_aller_retour_attente_vide(self):
        """
        Pour un transport aller-retour non annulé, le temps d'attente peut être
        obligatoire (selon réglage TEMPS_ATTENTE_OBLIGATOIRE).
        """
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        rapport_data = {
            "km": "28.5",
            "duree_eff": "00:42",
            "frais-INITIAL_FORMS": "0",
            "frais-TOTAL_FORMS": "0",
        }
        form = RapportForm(instance=transport, data=rapport_data, user=self.chauffeur.utilisateur)
        if fact_policy().TEMPS_ATTENTE_OBLIGATOIRE:
            self.assertFalse(form.is_valid())
            self.assertEqual(form.errors, {"temps_attente": ["Vous devez saisir un temps d’attente"]})
        else:
            self.assertTrue(form.is_valid())
        rapport_data["annule"] = "on"
        rapport_data["rapport_chauffeur"] = "Client absent"
        form = RapportForm(instance=transport, data=rapport_data, user=self.chauffeur.utilisateur)
        self.assertTrue(form.is_valid())

    def test_rapport_frais_repas(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        rapport_data = {
            'km': '28.5',
            'duree_eff': "00:42",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': '',
            'frais-0-cout': '',
        }
        form = RapportForm(instance=transport, data=rapport_data, files={
            "frais-0-justif": "",
        }, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.formset.errors, [
            {"__all__": ["Un justificatif est obligatoire pour les frais de repas."],
             "cout": ["Ce champ est obligatoire."]}
        ])
        rapport_data['frais-0-cout'] = '22.60'
        form = RapportForm(instance=transport, data=rapport_data, files={
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png'),
        }, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.formset.errors,
            [{'cout': ['Les frais de repas ne sont remboursés que jusqu’à 20 CHF.']}]
        )

    def test_rapport_deux_parkings(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        rapport_data = {
            'km': '28.5',
            'duree_eff': "00:42",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '2',
            'frais-0-id': '',
            'frais-0-typ': 'parking',
            'frais-0-transport': transport.pk,
            'frais-0-cout': '2.50',
            'frais-1-id': '',
            'frais-1-typ': 'parking',
            'frais-1-transport': transport.pk,
            'frais-1-cout': '1.25',
        }
        form = RapportForm(instance=transport, data=rapport_data, user=self.chauffeur.utilisateur)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.formset.forms[0].instance.description(), "Frais de parking")
        self.assertEqual(form.formset.empty_form.fields["typ"].initial, "parking")
        instance = form.save()
        self.assertQuerySetEqual(
            instance.frais.all().order_by("-cout"),
            [D("2.50"), D("1.25")],
            transform=lambda fr: fr.cout
        )

    @skipIf(fact_policy().CALCUL_KM_AUTO, "Le calcul des km est automatique sur cette instance")
    def test_rapport_km_durees(self):
        """Contrôles sur la saisie des km et durée dans le formulaire du rapport de transport."""
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130, duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        form_data = {
            "km": "0", "duree_eff": "00:42", "temps_attente": "00:15",
            'frais-INITIAL_FORMS': '0', 'frais-TOTAL_FORMS': '0',
        }
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertTrue(form.is_valid())
        form_data['km'] = "-10"
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'km': ["La saisie de km négatifs n’est pas autorisée."]})
        form_data['km'] = "651"
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'km': ["La saisie de plus de 650km n’est pas autorisée."]})
        form_data.update({'km': '12', 'duree_eff': "21h00", 'temps_attente': "15h42"})
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'duree_eff': ["La durée effective ne peut pas dépasser 20 heures."],
            'temps_attente': ["Le temps d'attente ne peut pas dépasser 15 heures."],
        })

    @skipIf(
        fact_policy().CALCUL_DUREE_AUTO,
        "Le calcul de al durée effective est automatique sur cette instance"
    )
    def test_rapport_attente_plus_de_3h(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130, duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        form_data = {
            'km': '20', 'duree_eff': "02:50", 'temps_attente': "3:15",
            'frais-INITIAL_FORMS': '0', 'frais-TOTAL_FORMS': '0',
        }
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'rapport_chauffeur': ["Pour un temps d’attente de plus de 3 heures, merci d’indiquer un commentaire."],
            'duree_eff': ['La durée effective doit être plus élevée que le temps d’attente.'],
        })

    def test_rapport_transport_aller(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), origine=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-rapport', args=[transport.pk]))
        # Le temps d'attente est tout de même visible pour les admins
        self.assertIn('temps_attente', response.context['form'].fields)
        self.assertNotContains(response, "Frais de repas")
        self.assertEqual(len(response.context['form'].formset.forms), 1)
        post_data = {
            'km': '25.0',
            'duree_eff': '00:45',
            'temps_attente': '',
            'rapport_chauffeur': '',
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=post_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        # Correction
        post_data['km'] = '25.4'
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=post_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))

    def test_rapport_transport_annule(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': 'on',
            'km': '',
            'duree_eff': "",
            'temps_attente': "",
            'statut_fact': '1',
            'rapport_chauffeur': "J'ai personne vu!",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=rapport_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.journaux.latest().description, "Rapport initial du transport (marqué comme annulé)")
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)
        self.assertIsNone(transport.duree_eff)
        self.assertIsNone(transport.defrayer_chauffeur)
        self.assertEqual(transport.statut_fact, Transport.FacturationChoices.FACTURER_ANNUL)
        # Apparaît dans les transports rapportés (même si annulé)
        response = self.client.get(
            reverse('transports-by-status', args=[Transport.StatutChoices.RAPPORTE]),
            HTTP_X_REQUESTED_WITH='fetch'
        )
        self.assertIn(transport, response.context['object_list'][self.demain_1130.date()]['transports'])

    def test_rapport_transport_annule_temps_attente(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        rapport_data = {
            "annule": "on",
            "km": "145",
            "duree_eff": "04:30",
            "temps_attente": "03:30",
            "rapport_chauffeur": "",
            "frais-INITIAL_FORMS": "0",
            "frais-TOTAL_FORMS": "0",
        }
        form = RapportForm(instance=transport, user=self.chauffeur.utilisateur, data=rapport_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            "rapport_chauffeur": [
                "Vous devez expliquer les circonstances de l’annulation.",
            ],
            "temps_attente": [
                "Vous ne pouvez pas indiquer de temps d’attente pour un transport annulé."
            ],
        })

    def test_rapport_chauffeur_externe(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur_externe=True,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': '',
            'km': '85',
            'duree_eff': "1:45",
            'temps_attente': "0:30",
            'statut_fact': '1',
            'rapport_chauffeur': "OK",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=rapport_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))


class ChauffeurTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = cls._create_benevole(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.TRANSPORT], empl_geo=[6.82, 47.087]
        )

    def test_liste_chauffeurs(self):
        benev = self._create_benevole(
            nom="Doe", prenom="Jill", activites=[TypeActivite.TRANSPORT],
            macaron_valide=(date.today() - timedelta(days=364), date.today()),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['transport']))
        self.assertContains(response, 'Liste des chauffeurs')
        self.assertContains(
            response,
            f'<div class="delai-passe">Macaron échu le {date.today().strftime("%d.%m.%Y")}</div>',
            html=True
        )
        response = self.client.get(reverse('benevoles', args=['transport']) + '?macaron_echu=on')
        self.assertQuerySetEqual(response.context['object_list'], [benev])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_chauffeur_transports(self):
        """Liste des transports à venir, par chauffeur."""
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONFIRME, chauffeur=self.chauffeur,
            remarques="Remarque OK"
        )
        annule = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ANNULE, chauffeur=self.chauffeur,
            remarques="Annulation il y a 3 jour",
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-transports', args=[self.chauffeur.pk]))
        self.assertContains(response, '<h4>Prochains transports de Duplain Irma</h4>')
        self.assertQuerySetEqual(response.context['object_list'], [transport, annule])
        client_url = reverse('transport-client-detail', args=[self.trclient.pk])
        self.assertContains(response, f'<a href="{client_url}">Donzé Léa</a>', html=True)

        # Test PDF list
        response = self.client.post(reverse('benevole-transports-pdf', args=[self.chauffeur.pk]), data={
            'date_de': date.today(),
            'date_a': date.today() + timedelta(days=5),
        })
        self.assertEqual(response.headers['Content-Type'], 'application/pdf')
        fact_text = pdf_text(response, pages=[0])[0]
        self.assertIn("Remarque OK", fact_text)
        self.assertNotIn("Annulation", fact_text)

        # Pas de transports dans cet intervalle.
        response = self.client.post(reverse('benevole-transports-pdf', args=[self.chauffeur.pk]), data={
            'date_de': date.today(),
            'date_a': date.today(),
        }, follow=True)
        self.assertContains(response, "Aucun transport dans la plage de dates indiquée")

    def test_chauffeur_transports_archives(self):
        """Liste des transports passés, par chauffeur."""
        hier_1130 = datetime.combine(date.today() - timedelta(days=1), time(11, 30), tzinfo=get_current_timezone())
        transport1 = self.create_transport(
            client=self.trclient,
            heure_rdv=hier_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE, chauffeur=self.chauffeur,
        )
        transport2 = self.create_transport(
            client=self.trclient,
            heure_rdv=hier_1130 - timedelta(days=4),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=self.chauffeur,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-transports-archives', args=[self.chauffeur.pk]))
        self.assertContains(response, '<h4>Transports passés de Duplain Irma</h4>')
        self.assertQuerySetEqual(response.context['object_list'], [transport1, transport2])
        # Avec filtre
        response = self.client.get(
            reverse('benevole-transports-archives', args=[self.chauffeur.pk]) +
            f'?statut={Transport.StatutChoices.CONTROLE}'
        )
        self.assertQuerySetEqual(response.context['object_list'], [transport2])

    def test_ajout_preference(self):
        self.client.force_login(self.user)
        new_pref_url = reverse('transport-client-newpref', args=[self.trclient.pk])
        response = self.client.get(new_pref_url)
        self.assertContains(
            response,
            '<label for="id_typ_0"><input type="radio" name="typ" value="pref" required id="id_typ_0"'
            ' class="form-check-input">😀 Préféré</label>',
            html=True
        )
        post_data = {
            'chauffeur': self.chauffeur.pk, 'typ': "pref", 'remarque': "Top!",
        }
        response = self.client.post(new_pref_url, data=post_data)
        self.assertEqual(response.json()['result'], "OK")
        response = self.client.post(new_pref_url, data=post_data)
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ["Il existe déjà une préférence ou incompatibilité entre ce client et ce chauffeur"]
        )

    def test_chauffeur_create_dispo(self):
        self.client.force_login(self.user)
        form_data = {
            'date': '2022-11-01',
            'heure_de': '10:30',
            'heure_a': '12:15',
            'repetition': 'WEEKLY',
            'repetition_fin': '2023-05-20',
        }
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        response = self.client.get(reverse('benevole-agenda', args=[self.chauffeur.pk, 2022, 45]))
        self.assertContains(response, "10:30 - 12:15")
        # Cannot add overlapping dispo
        form_data['date'] = '2022-11-08'
        form_data['heure_de'] = '11:30'
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['Cette disponibilité se chevauche avec une autre déjà existante.']}
        )
        # Si l'occurrence en conflit est annulée, autorisation d'ajout par dessus.
        dispo = self.chauffeur.dispos.first()
        dispo.cancel_for(date(2022, 11, 8))
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')

    def test_chauffeur_edit_repetition(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(
                nom='Une semaine sur deux', frequence='WEEKLY', params='interval:2'
            )[0],
        )
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=dispo, initial={})
        self.assertInHTML(
            '<option value="BI-WEEKLY" selected>Une semaine sur deux</option>', str(form)
        )

    def test_chauffeur_create_absence(self):
        Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 + timedelta(days=7),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONFIRME, chauffeur=self.chauffeur,
        )
        self.client.force_login(self.user)
        form_data = {
            'date': self.demain_1130.date(),
            'heure_de': '06:00',
            'heure_a': '22:00',
            'repetition': '',
        }
        response = self.client.post(reverse('benevole-absence-add', args=[self.chauffeur.pk]), data=form_data)
        if response.headers['Content-Type'].startswith('text/html'):
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json()['result'], 'OK')
        form_data["date"] = transport.date
        response = self.client.post(reverse("benevole-absence-add", args=[self.chauffeur.pk]), data=form_data)
        self.assertIn(
            "Au moins un transport vous a été attribué durant cette absence.",
            response.context["form"].errors["__all__"][0],
        )
        # Si le transport est seulement Attribué, il revient automatiquement en Saisi.
        transport.statut = Transport.StatutChoices.ATTRIBUE
        transport.save()
        response = self.client.post(reverse("benevole-absence-add", args=[self.chauffeur.pk]), data=form_data)
        self.assertEquals(response.json(), {"result": "OK", "reload": "page"})
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(transport.chauffeur)

    def test_chauffeur_dispo_form(self):
        # Heures sans séparateur
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=None, data={
            'date': '2022-11-01',
            'heure_de': '1430',
            'heure_a': '15h15',
        })
        self.assertTrue(form.is_valid())
        self.assertIn("Indication facultative sur la dispo", str(form))
        # Erreur format heure
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=None, data={
            'date': '2022-11-01',
            'heure_de': '14x30',
            'heure_a': '1515',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'heure_de': ['Saisissez une heure valide au format hh:mm']}
        )
        # Durée non correcte ou trop faible
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=None, data={
            'date': '2022-11-01',
            'heure_de': '14:30',
            'heure_a': '14:35',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'heure_de': ['Impossible de saisir une disponibilité de moins de 15 minutes']}
        )
        # Absence
        form = DispoEditForm(chauffeur=self.chauffeur, categ='absence', instance=None, data={
            "date": "2022-11-01",
            "heure_de": "00:00",
            "heure_a": "00:00",
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {"heure_de": ["Impossible de saisir une disponibilité de moins de 15 minutes"]}
        )

    def test_chauffeur_dispo_form_passe(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence = self.demain_1130 - timedelta(days=2)
        )
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=dispo, initial={}, data={
            'date': dispo.debut.date(),
            'heure_de': '10:30',
            'heure_a': '14:45',
            'repetition': 'DAILY',
            'repetition-fin': dispo.fin_recurrence
        })
        self.assertTrue(form.is_valid(), form.errors)

    def _test_chauffeur_delete_dispo(self, dispo):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=self.chauffeur,
        )
        self.user.user_permissions.add(Permission.objects.get(codename='change_dispo'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-dispo-delete', args=[dispo.pk]), {})
        # Pas de suppression car transport existant sur dispo
        self.assertEqual(response.json()['result'], 'OK')
        self.assertTrue(Dispo.objects.filter(pk=dispo.pk).exists())
        response = self.client.get(reverse('gestion'))
        self.assertContains(
            response,
            "Impossible de supprimer cette disponibilité car les transports suivants sont déjà attribués"
        )
        transport.delete()
        response = self.client.post(reverse('benevole-dispo-delete', args=[dispo.pk]), {})
        self.assertFalse(Dispo.objects.filter(pk=dispo.pk).exists())

    def test_chauffeur_delete_dispo_recur(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        self._test_chauffeur_delete_dispo(dispo)

    def test_chauffeur_delete_dispo_unique(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        self._test_chauffeur_delete_dispo(dispo)

    def test_chauffeur_toggle_occurrence(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        self.user.user_permissions.add(Permission.objects.get(codename='change_dispo'))
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('benevole-dispo-edit', args=[dispo.pk]) + f'?occ={quote(self.demain_1130.isoformat())}'
        )
        self.assertContains(response, "Annuler l’occurrence du ")
        toggle_url = reverse('benevole-dispo-toggle', args=[dispo.pk])
        response = self.client.post(toggle_url, data={'occ_date': self.demain_1130.isoformat()})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(dispo.exceptions.filter(annule=True).count(), 1)
        response = self.client.post(toggle_url, data={'occ_date': self.demain_1130.isoformat()})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(dispo.exceptions.filter(annule=True).count(), 0)

    def test_chauffeur_day_view(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=self.chauffeur,
        )
        self.client.force_login(self.user)
        demain = self.demain_1130.date()
        response = self.client.get(reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transport.pk]))
        self.assertEqual(response.context['start_hour'], response.context['view'].START_HOUR)
        self.assertContains(
            response, f'<div class="event-cell hour" data-day="{demain}" data-hour="10:00">',
            html=True
        )
        Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(hours=5),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        response = self.client.get(reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transport.pk]))
        self.assertEqual(response.context['start_hour'], 6)
        transp_non_attrib = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(hours=6),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=None,
        )
        response = self.client.get(
            reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transp_non_attrib.pk])
        )
        self.assertEqual(response.context['start_hour'], 5)

    def test_chauffeur_day_overlapping(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=self.chauffeur,
        )
        transp_non_attrib = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(hours=1),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=None,
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transp_non_attrib.pk])
        )
        self.assertContains(response, "width: calc(50.0% - 22px);")


class ChoixChauffeurTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur1 = cls._create_benevole(
            nom="Dupond", prenom="Lily1", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        cls.chauffeur2 = cls._create_benevole(
            nom="Duplain", prenom="Irma2", activites=['transport'], empl_geo=[6.85, 47.013]  # Genev.-sur
        )
        cls.chauffeur3 = cls._create_benevole(
            nom="Smith", prenom="John3", activites=['transport'], empl_geo=[6.885, 46.985]  # Peseux
        )
        benev = cls._create_benevole(nom="Temp", activites=['transport'], empl_geo=[6.885, 46.985])
        benev.activite_set.all().update(inactif=True)
        cls.allerretour = cls.create_transport(
            client=cls.trclient,  # habitant CdF
            heure_rdv=cls.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=cls.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )

    def test_chauffeurs_potentiels_distance(self):
        """Tri sur critère distance uniquement."""
        chauffeur_futur = self._create_benevole(
            nom="XY", prenom="Jean", activites=["transport"], empl_geo=[6, 46],
        )
        chauffeur_futur.activite_set.update(duree=(date.today() + timedelta(days=4), None))
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur2, self.chauffeur3])

    def test_chauffeurs_potentiels_distance0(self):
        """Le chauffeur habite même immeuble que le client."""
        chauffeur4 = self._create_benevole(
            nom="Schmid", prenom="Hans4", activites=['transport'],
            empl_geo=self.trclient.adresse(date.today()).empl_geo
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [chauffeur4, self.chauffeur1, self.chauffeur2, self.chauffeur3])
        self.assertEqual(chauffeurs[0].distance_tr, (0, "vo"))

    def test_chauffeurs_potentiels_conflit(self):
        cl1 = Client.objects.create(
            service=TRANSPORT, nom="Donzé1", prenom="Léa1",
            npa="2000", localite="Neuchâtel", empl_geo=[6.81, 7.0],
        )
        self.create_transport(
            client=cl1,
            heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False,
            chauffeur=self.chauffeur1,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])

    def test_chauffeurs_potentiels_transp_annule(self):
        """Un transport annulé ne doit pas interférer avec les chauffeurs potentiels."""
        cl1 = Client.objects.create(
            service=TRANSPORT, nom="Donzé1", prenom="Léa1",
            npa="2000", localite="Neuchâtel", empl_geo=[6.81, 7.0],
        )
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        self.create_transport(
            client=cl1,
            heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False,
            chauffeur=self.chauffeur1,
            statut=Transport.StatutChoices.ANNULE,
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur2, self.chauffeur3])

    @patch('common.distance.httpx.get', side_effect=mocked_httpx)
    def test_chauffeurs_potentiels_dispo(self, mocked):
        """
        Chauffeur avec dispo reçoit la priorité (1 ou 2 selon matching complet
        ou partiel avec la dispo), quelle que soit la distance,
        mais reçoit une priorité négative si la dispo est annulée.
        """
        Dispo.objects.create(
            chauffeur=self.chauffeur3,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        # Chauffeur2 a une dispo, mais annulee
        dispo2 = Dispo.objects.create(
            chauffeur=self.chauffeur2,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        dispo2.refresh_from_db()
        dispo2.cancel_for(self.demain_1130.date())
        dispos2 = Dispo.get_for_person(self.chauffeur2, now(), now() + timedelta(days=4))
        self.assertTrue(dispos2[0].cancelled)
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur1, self.chauffeur2])
        self.assertEqual([c.priorite_dispo for c in chauffeurs], [1, 3, 5])

        # Autre transport attribué au chauffeur3, il sera donc déja occupé pour le transport 'allerretour'
        autre_client = Client.objects.create(
            nom='Donzé', prenom='Georges', no_debiteur=9991,
            rue='Rue des Crêtets 90', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.81, 7.0]
        )
        self.create_transport(
            client=autre_client,
            chauffeur=self.chauffeur3,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur3, self.chauffeur2])
        self.assertEqual([c.priorite_dispo for c in chauffeurs], [3, 4, 5])

    def test_chauffeurs_potentiels_absence(self):
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur2, self.chauffeur3])
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        # Absence 2 jours avant, jusqu'à 2 jours après
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=datetime.combine(
                date.today() - timedelta(days=2), time(6, 0), tzinfo=get_current_timezone()
            ),
            fin=datetime.combine(
                date.today() - timedelta(days=2), time(22, 0), tzinfo=get_current_timezone()
            ),
            categorie=Dispo.CategChoices.ABSENCE,
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence=datetime.combine(
                date.today() + timedelta(days=2), time(22, 0), tzinfo=get_current_timezone()
            ),
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])

    def test_chauffeurs_potentiels_preference(self):
        Preference.objects.create(client=self.trclient, chauffeur=self.chauffeur3, typ='pref')
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur1, self.chauffeur2])
        Preference.objects.create(client=self.trclient, chauffeur=self.chauffeur1, typ='incomp')
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur2, self.chauffeur1])

    def test_chauffeurs_potentiels_incompats(self):
        self.trclient.handicaps = ['deamb', 'cannes']
        self.trclient.save()
        self.chauffeur1.incompats = ['poids', 'deamb']
        self.chauffeur1.save()
        self.chauffeur2.incompats = ['poids']
        self.chauffeur2.save()
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])

    def test_chauffeurs_potentiels_refus(self):
        # Chauffeur1 a une dispo, mais a refusé transport
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        Refus.objects.create(transport=self.allerretour, chauffeur=self.chauffeur1)
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])
        self.assertEqual([c.priorite_dispo for c in chauffeurs], [3, 3, 6])

    def test_dispo_partielle_avec_recurrence(self):
        # Dispo partielle, démarre après le début du transport
        dispo_debut = self.allerretour.heure_depart - timedelta(days=2) + timedelta(minutes=20)
        chaque_jour = Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0]
        dispo1 = Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=dispo_debut,
            fin=dispo_debut + timedelta(hours=3),
            regle=chaque_jour,
        )
        # Dispo partielle, démarre avant le début du transport (mais finit avant la fin)
        dispo_debut = self.allerretour.heure_depart - timedelta(days=2) - timedelta(hours=2)
        dispo2 = Dispo.objects.create(
            chauffeur=self.chauffeur2,
            debut=dispo_debut,
            fin=dispo_debut + timedelta(hours=2, minutes=20),
            regle=chaque_jour,
        )
        dispos = Dispo.get_for_period(self.allerretour.heure_depart, self.allerretour.heure_arrivee)
        self.assertEqual(dispos, [dispo2, dispo1])

    def test_dispo_partielle_avec_recurrence_jour_entier(self):
        # Problème potentiel avec stockage UTC qui change le jour de début de la dispo
        dispo_debut = (self.allerretour.heure_depart - timedelta(days=2)).replace(hour=0, minute=0)
        chaque_jour = Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0]
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=dispo_debut,
            fin=dispo_debut.replace(hour=23, minute=59),
            regle=chaque_jour,
        )
        dispos = Dispo.get_for_period(self.allerretour.heure_depart, self.allerretour.heure_arrivee)
        self.assertEqual(dispos, [dispo])


class TransportModeleTests(DataMixin, TestCase):
    def test_model_fin(self):
        modele = self.create_modele(
            date=date.today(),
            repetition='DAILY',
            repetition_fin=(now() + timedelta(days=4)).strftime('%Y-%m-%d'),
        )
        occs = modele.generer_de_a(now(), now() + timedelta(days=20))
        self.assertEqual(len(occs), 4)

    def test_model_suspension(self):
        modele = self.create_modele(date=date.today())
        modele.suspension_depuis = date.today() + timedelta(days=4)
        modele.save()
        occs = modele.generer_de_a(now(), now() + timedelta(days=20))
        self.assertEqual(len(occs), 3)

    def test_get_occurrence_final_day(self):
        dans_deux_jours = date.today() + timedelta(days=2)
        modele = self.create_modele(date=date.today(), repetition_fin=dans_deux_jours)
        modele.refresh_from_db()
        self.assertIsNotNone(modele.get_occurrence(dans_deux_jours))
        self.assertIsNone(modele.get_occurrence(dans_deux_jours + timedelta(days=1)))

    def test_get_occurrence_suspension_et_fin(self):
        """Ne pas produire d'occurrence si date suspension > date fin."""
        modele = self.create_modele(date=date.today() - timedelta(days=7))
        modele.suspension_depuis = date.today() + timedelta(days=1)
        modele.fin_recurrence = now() - timedelta(days=1)
        modele.save()
        self.assertEqual(
            modele.generer_de_a(now() - timedelta(days=1), now() + timedelta(days=5)),
            []
        )

    def test_saisie_transport_modele(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'True',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '00:45',
            'repetition': 'WEEKLY',
            'repetition_fin': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': '',
            'trajets-1-ORDER': '1',
            'trajets-1-destination_adr': '',
        })
        modele = TransportModel.objects.first()
        self.assertEqual(modele.regle.frequence, 'WEEKLY')
        transport = Transport.objects.get(client=self.trclient)
        self.assertEqual(transport.modele, modele)
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 30))
        self.assertEqual(modele.heure_depart, time(10, 15))

    def test_saisie_transport_modele_aller_simple(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'repetition': 'WEEKLY',
            'repetition_fin': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
        })
        modele = TransportModel.objects.first()
        self.assertEqual(modele.regle.frequence, 'WEEKLY')
        transport = Transport.objects.get(client=self.trclient)
        self.assertEqual(transport.modele, modele)
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 30))

    def test_edit_transport_modele(self):
        """Édition du modèle lui-même."""
        modele = self.create_modele(
            date=date.today() - timedelta(days=70), retour=True, repetition='WEEKLY'
        )
        transport = modele.get_occurrence(date.today() + timedelta(days=7))
        transport.concretiser()

        self.client.force_login(self.user)
        response = self.client.get(reverse('transportmodel-edit', args=[modele.pk]))
        self.assertContains(
            response,
            '<input type="time" name="heure_rdv" value="10:30" class="form-control" required="" id="id_heure_rdv">',
            html=True
        )
        self.assertContains(response, "Occurrences futures")
        response = self.client.post(reverse('transportmodel-edit', args=[modele.pk]), data={
            'heure_depart': '10:10',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'remarques': 'Rdv plus tard',
            'suspension_depuis': '',
            'fin_recurrence': '',
        })
        self.assertRedirects(response, reverse('gestion'))
        modele.refresh_from_db()
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 45))
        self.assertEqual(modele.heure_depart, time(10, 10))

    def test_edit_transport_modele_ajout_vehicule(self):
        modele = self.create_modele(
            date=date.today() - timedelta(days=70), retour=True, repetition="WEEKLY"
        )
        jour = modele.get_next_occurrence().debut.date()
        vehicule = Vehicule.objects.create(modele="Kangoo", no_plaque="JU 111111", empl_geo=[6.83, 47.01])
        self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(jour, modele.heure_rdv.time(), tzinfo=get_current_timezone()),
            destination=self.hne_ne, retour=False,
            vehicule=vehicule,
            statut=Transport.StatutChoices.SAISI,
        )

        self.client.force_login(self.user)
        response = self.client.post(reverse("transportmodel-edit", args=[modele.pk]), data={
            "heure_depart": "10:10",
            "heure_rdv": "10:45",
            "duree_rdv": "00:45",
            "vehicule": vehicule.pk,
            "remarques": "",
            "suspension_depuis": "",
            "fin_recurrence": "",
        }, follow=True)
        self.assertContains(response, "Attention, il y a un conflit potentiel pour l’occurrence du")

    def test_edit_jour_non_existant(self):
        modele = self.create_modele(repetition='WEEKLY')
        demain = date.today() + timedelta(days=1)
        url = reverse('transport-model-edit', args=[modele.pk, demain.strftime('%Y%m%d')])
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertContains(
            response,
            f"Aucune occurrence de ce transport trouvée pour le {demain.strftime('%d.%m.%Y')}",
            status_code=404
        )

    def test_liste_transports_avec_occurrences(self):
        # Passé
        self.create_modele(
            date=date.today() - timedelta(days=60),
            repetition='WEEKLY',
            repetition_fin=(date.today() - timedelta(days=1)).strftime('%Y-%m-%d'),
        )
        self.create_modele(repetition='WEEKLY')
        self.client.force_login(self.user)
        with patch('transport.tests.tests.FakeORSClient.directions', side_effect=Exception(
            'directions() should not be called when listing occurrences'
        )):
            cache.clear()
            response = self.client.get(
                reverse('transports-by-status', args=[Transport.StatutChoices.SAISI]),
                HTTP_X_REQUESTED_WITH='fetch'
            )
            # 2 from test data + 9 new from modele
            self.assertEqual(
                sum(val['len'] for val in response.context['object_list'].values()),
                11
            )
            # Liste avec occurrence non concrétisée.
            cache.clear()
            response = self.client.get(
                reverse('transports-by-status-and-day', args=[
                    Transport.StatutChoices.SAISI, (date.today() + timedelta(days=3)).strftime('%Y-%m-%d')
                ]),
                HTTP_X_REQUESTED_WITH='fetch'
            )
            self.assertEqual(
                sum(val['len'] for val in response.context['object_list'].values()),
                1
            )

    def test_liste_transports_par_client(self):
        """Les futures occurences apparaissent dans la liste des prochains transports du client."""
        self.create_modele(repetition='WEEKLY')
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-client-detail', args=[self.trclient.pk]))
        self.assertEqual(len(response.context['prochains_transports']), 9)

    def test_concretiser_transport_apres_demenagement(self):
        self.trclient.adresses.nouvelle(
            rue="Fbg de l'Hôpital 1", npa="2000", localite="Neuchâtel", empl_geo=[6.93, 47]
        )
        modele = self.create_modele(retour=True, repetition='WEEKLY')
        self.assertEqual(len(self.trclient.adresses), 2)
        transport = modele.get_occurrence(date.today() + timedelta(days=3))
        transport.concretiser()
        self.assertEqual(str(transport.adresse_depart()), "Fbg de l'Hôpital 1, 2000 Neuchâtel")
        premier_transport = modele.transports.first()
        self.assertEqual(str(premier_transport.adresse_depart()), "Rue des Crêtets 92, 2300 La Chaux-de-Fonds")
        self.assertGreater(premier_transport.dist_calc, transport.dist_calc)

    def test_attrib_transport_from_models(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        modele = self.create_modele(retour=True, repetition='WEEKLY')
        # Si heure de départ modifiée a posteriori, elle est respectée au moment
        # de concrétiser un transport
        modele.heure_depart = '10:10'
        modele.save()
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-attrib', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        client_url = reverse("transport-client-detail", args=[self.trclient.pk])
        self.assertContains(
            response, f'Pour <b><a href="{client_url}">{self.trclient}</a></b>'
        )
        self.assertContains(
            response, '<div class="col-sm-3 horaire">Dép: 10:10</div>', html=True
        )

        response = self.client.post(url, data={'chauffeur': chauffeur.pk})
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(transport.chauffeur, chauffeur)
        self.assertEqual(transport.heure_depart.time(), time(10, 10))

        # D'autres envois du même formulaire ne doivent pas créer plus de transports
        response = self.client.post(url, data={'chauffeur': chauffeur.pk}, follow=True)
        self.assertEqual(
            str(list(response.context['messages'])[0]),
            'Le formulaire a déjà été envoyé pour cette occurrence.'
        )
        self.assertEqual(Transport.objects.filter(modele=modele, date=next_occ_date).count(), 1)

    def test_attrib_transport_depuis_modele_avec_vehicule(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        modele = self.create_modele()
        vehicule = Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111', empl_geo=[6.83, 47.01])
        self.client.force_login(self.user)
        next_occ_date = date.today() + timedelta(days=3)
        url = reverse('transport-model-attrib', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, 'JU 111111')
        response = self.client.post(url, data={'chauffeur': chauffeur.pk, 'vehicule': vehicule.pk})
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.vehicule, vehicule)

    def test_cancel_transport_from_models(self):
        modele = self.create_modele(repetition='WEEKLY')
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-cancel', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, f'<h3>Transport pour {self.trclient}')

        response = self.client.post(url, {
            'motif_annulation': Transport.AnnulationChoices.CLIENT_ANNUL,
            'raison_annulation': "Trouvé autre moyen",
            'km': '',
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(response.json()['result'], 'OK')
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)

    def test_refus_transport_modele(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        modele = self.create_modele()
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        url = reverse('transport-model-refuse', args=[
            modele.pk, in_5_days.strftime('%Y%m%d')
        ])
        response = self.client.post(url, data={'chauffeur': chauffeur.pk})
        self.assertEqual(response.json(), {
            'result': 'OK',
            'redirect': reverse('transport-attrib', args=[modele.transports.get(date=in_5_days).pk]),
        })
        self.assertEqual(Refus.objects.filter(chauffeur=chauffeur).count(), 1)
        self.assertEqual(Refus.objects.filter(chauffeur=chauffeur).first().transport.modele, modele)

    def test_edit_transport_from_models(self):
        modele = self.create_modele(retour=True, repetition='WEEKLY')
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, '<h2>Édition d’un transport</h2>', html=True)
        self.assertContains(response, 'Pour: <span class="fw-bold">Donzé Léa</span>')
        self.assertContains(
            response,
            '<p class="fst-italic" id="adresse_depart">Rue des Crêtets 92, 2300 La Chaux-de-Fonds</p>',
            html=True
        )
        self.assertContains(
            response,
            '<div class="destination_actuelle">Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel</div>',
            html=True
        )
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': next_occ_date.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': '',
            'trajets-1-ORDER': '1',
            'trajets-1-destination_adr': '',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)

    def test_edit_transport_from_models_retour_domicile(self):
        modele = self.create_modele(
            origine_adr=self.hne_ne,
            destination=None,
            repetition='WEEKLY',
        )
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'False',
            'date': next_occ_date.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'origine_adr': self.hne_ne.pk,
            'trajets-INITIAL_FORMS': '1',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': modele.transports.first().trajets_tries[0].pk,
            'trajets-0-destination_adr': '-1',
            'trajets-0-destination_adr_select': '',
            'trajets-0-destination_princ': 'True',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = modele.transports.get(date=next_occ_date)
        self.assertEqual(transport.origine, self.hne_ne)
        self.assertEqual(transport.destination.empl_geo, self.trclient.adresse().empl_geo)

    def test_edit_occurrence_autre_date(self):
        """
        Si la date d'une occurrence est modifiée, une annulation est créée pour
        la date d'origine pour que la répétition ne la recrée pas.
        """
        modele = self.create_modele(retour=True, repetition='WEEKLY')
        modele._check_trajets()
        next_occ_date = (now() + timedelta(days=3)).date()
        self.client.force_login(self.user)
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': (next_occ_date + timedelta(days=1)).strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': str(modele.trajets_tries[0].pk),
            'trajets-0-ORDER': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': str(modele.trajets_tries[1].pk),
            'trajets-1-ORDER': '2',
            'trajets-1-destination_princ': 'False',
            'trajets-1-destination_adr': '-1',
        })
        self.assertRedirects(response, reverse('gestion'))
        transps = modele.transports.filter(date__gt=date.today()).order_by('date')
        self.assertEqual(len(transps), 2)
        self.assertEqual(transps[0].statut, Transport.StatutChoices.ANNULE)
        self.assertEqual(transps[0].date, next_occ_date)
        self.assertEqual(transps[0].raison_annulation, "Report de cette occurrence à une autre date.")
        self.assertEqual(transps[0].statut_fact, Transport.FacturationChoices.PAS_FACTURER)
        self.assertIs(transps[0].defrayer_chauffeur, False)
        self.assertGreater(len(transps[0].trajets_tries), 0)
        self.assertEqual(transps[1].statut, Transport.StatutChoices.SAISI)
        self.assertEqual(transps[1].date, next_occ_date + timedelta(days=1))
        self.assertGreater(len(transps[1].trajets_tries), 0)


class FacturesTests(DataMixin, TempMediaRootMixin, TestCase):
    def test_facturer_transport(self):
        self.maxDiff = None
        day_in_passed_month = date.today() - relativedelta.relativedelta(months=1)
        client2 = Client.objects.create(
            service=TRANSPORT, nom='Schmid', prenom='Germain', no_debiteur=999,
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23), empl_geo=[6.820, 47.094],
            valide_des=day_in_passed_month,
        )
        tr1 = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(day_in_passed_month, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE,
        )
        tr1.km = tr1.km_calc()
        tr1.save()
        Frais.objects.create(transport=tr1, typ=Frais.TypeFrais.REPAS, cout=D("17.90"))
        Frais.objects.create(transport=tr1, typ=Frais.TypeFrais.PARKING, cout=D("3.45"))
        self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(day_in_passed_month, time(15, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.create_transport(
            client=client2,
            heure_rdv=datetime.combine(day_in_passed_month, time(15, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
        )
        fpolicy = fact_policy()
        tarif = fpolicy.tarif_km(tr1)
        kms = D("30") if canton_app.abrev == "JU" else D("29.2")  # kms arrondis ver le haut pour JU
        self.assertEqual(
            tr1.donnees_facturation(),
            {
                'no': tr1.pk,
                'type_fact': "transport",
                'avs': True,
                'rendez-vous': localtime(tr1.heure_rdv),
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'retour': True,
                'temps_attente': None,
                'km': kms,
                'tarif_km': tarif,
                'cout_km': round(kms * tarif, 2),
                'cout_forfait': fpolicy.FORFAIT_ALLER_RETOUR,
                'frais': [
                    {'descr': "Facturation des frais de repas", 'cout': D("17.90"), 'typ': 'repas'},
                    {'descr': "Facturation des frais de parking", 'cout': D("3.45"), 'typ': 'parking'}
                ],
                'total': round(kms * tarif, 2) + fpolicy.FORFAIT_ALLER_RETOUR + D("17.90") + D("3.45"),
            }
        )
        self.client.force_login(self.user)
        fact_url = reverse('facturer-transports', args=[day_in_passed_month.year, day_in_passed_month.month])
        response = self.client.get(fact_url)
        self.assertContains(
            response,
            "Attention, il semble qu’il reste un certain nombre de transports non contrôlés (1)."
        )
        response = self.client.post(
            fact_url, data={'date_facture': date.today()}, follow=True
        )
        self.assertContains(
            response, f"Les factures ont été générées pour {format_mois_an(day_in_passed_month)}"
        )
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        transp = self.trclient.factures_transports.first()
        self.assertEqual(transp.nb_transp, 1)
        self.assertEqual(transp.date_facture, date.today())
        self.assertEqual(client2.factures_transports.count(), 1)
        # Un deuxième envoi ne doit pas générer les mêmes factures à double
        response = self.client.post(
            fact_url, data={'date_facture': date.today()}, follow=True
        )
        self.assertIn('text/html', response['Content-Type'])
        self.assertContains(response, "Aucun transport à facturer pour ce mois")

    def test_facturation_aller_simple(self):
        """Les km retour sont aussi facturés pour un aller simple."""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
        )
        self.assertEqual(sum(traj.dist_calc for traj in tr.trajets_tries), 14600)
        donnees_fact = tr.donnees_facturation()
        kms = D("30") if canton_app.abrev == "JU" else D("29.2")  # kms arrondis ver le haut pour JU
        self.assertEqual(donnees_fact['km'], kms)
        self.assertEqual(donnees_fact['cout_km'], kms * fact_policy().tarif_km(tr))

    def test_non_facturation(self):
        """Un transport peut être marqué comme à ne pas facturer"""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
            statut_fact=Transport.FacturationChoices.PAS_FACTURER,
        )
        self.assertIsNone(tr.donnees_facturation())

    def test_facturer_transport_annule(self):
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ANNULE,
            statut_fact=Transport.FacturationChoices.PAS_FACTURER,
            km=D(12),
        )
        self.assertIsNone(tr.donnees_facturation())
        un_du_mois_prec = (date.today().replace(day=1) - timedelta(days=2)).replace(day=1)
        self.assertQuerySetEqual(
            LotAFacturer(un_du_mois_prec, billed=False).transports,
            []
        )

        tr.statut_fact = Transport.FacturationChoices.FACTURER_ANNUL
        tr.save()
        self.maxDiff = None
        cout_annul = fact_policy().cout_annulation(tr)
        tarif = fact_policy().tarif_km(tr)
        donnees_fact = tr.donnees_facturation()
        cout_forfait = donnees_fact.pop("cout_forfait", 0)  # Testé au niveau cantonal
        self.assertEqual(
            donnees_fact,
            {
                'no': tr.pk,
                'type_fact': "transport",
                'avs': True,
                'rendez-vous': tr.heure_rdv,
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'retour': True,
                'temps_attente': None,
                'annulation': cout_annul,
                'km': D(0),
                'tarif_km': tarif,
                'cout_km': D(0),
                'total': cout_annul + cout_forfait,
            }
        )
        tr.statut_fact = Transport.FacturationChoices.FACTURER_ANNUL_KM
        tr.save()
        donnees_fact = tr.donnees_facturation()
        self.assertEqual(
            {
                k: v for k, v in donnees_fact.items()
                if k not in ["cout_km", "tarif_km", "cout_forfait", "total"]
            },
            {
                'no': tr.pk,
                'type_fact': "transport",
                'avs': True,
                'rendez-vous': tr.heure_rdv,
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'retour': True,
                'temps_attente': None,
                'annulation': cout_annul,
                'km': D(12),
            }
        )
        # Chiffres exacts dépendant du canton
        self.assertGreater(donnees_fact['cout_km'], D('10'))
        self.assertEqual(donnees_fact['total'], cout_annul + donnees_fact['cout_km'] + cout_forfait)
        self.assertQuerySetEqual(
            LotAFacturer(un_du_mois_prec, billed=False).transports,
            [tr]
        )
        self.client.force_login(self.user)
        self.client.post(
            reverse('facturer-transports', args=[un_du_mois_prec.year, un_du_mois_prec.month]),
            data={'date_facture': date.today()}
        )
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        self.assertEqual(self.trclient.factures_transports.first().nb_transp, 1)

    def test_tarif_avs_des(self):
        self.trclient.persona.date_naissance = date(date.today().year - 62, 2, 10)
        self.trclient.persona.save()
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE
        )
        fpolicy = fact_policy()
        tarif_non_avs = fpolicy.tarif_km(tr)
        self.trclient.tarif_avs_des = tr.date - timedelta(days=2)
        self.trclient.save()
        self.assertGreater(tarif_non_avs, fpolicy.tarif_km(tr))
        self.trclient.tarif_avs_des = tr.date + timedelta(days=2)
        self.trclient.save()
        self.assertEqual(fpolicy.tarif_km(tr), tarif_non_avs)

    def test_autre_debiteur(self):
        referent = Referent.objects.create(
            client=self.trclient, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            complement="Avocat conseil", id_externe=12345,
            facturation_pour=['transp'],
        )
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        self.client.post(
            reverse('facturer-transports', args=[heure.date().year, heure.date().month]),
            data={'date_facture': date.today()}
        )
        facture = self.trclient.factures_transports.first()
        self.assertEqual(facture.autre_debiteur, referent)

    def test_liste_factures_non_transmises(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('factures-non-transmises'))
        self.assertEqual(len(response.context['facture_list']), 0)

    def test_exportation_pour_compta(self):
        Facture.objects.create(
            client=self.trclient, mois_facture=date(2024, 10, 1), date_facture=date(2024, 11, 4),
            nb_transp=2,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse("exporter-compta", args=["2024", "10"]), data={})
        resp_lines = response.content.decode("latin-1").splitlines()
        self.assertTrue(
            resp_lines[0].startswith(
                "DocumentCode;Client;ID Client;Domicile;Statut AVS;No facture;"
            )
        )
        self.assertTrue(
            resp_lines[1].startswith(
                f"FA;DONZÉ Léa;{self.trclient.pk};2300 La Chaux-de-Fonds;Oui;;31.10.2024;04.11.2024;"
            )
        )

    def test_refacturation(self):
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('facturer-transports', args=[heure.date().year, heure.date().month]),
            data={'date_facture': date.today()}
        )
        self.assertRedirects(response, reverse('finances'))
        facture = self.trclient.factures_transports.first()
        edit_url = reverse('client-editer-facture', args=[facture.pk])
        # Régénérer facture en remplaçant l'ancienne
        response = self.client.post(edit_url, data={'date_facture': date.today()})
        self.assertRedirects(response, reverse('client-transports-factures', args=[self.trclient.pk]))
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        # Régénérer facture déjà exportée, l'ancienne sera annulée mais conservée
        self.trclient.factures_transports.update(exporte=now())
        facture = self.trclient.factures_transports.first()
        edit_url = reverse('client-editer-facture', args=[facture.pk])
        with patch('httpx.put'):
            response = self.client.post(edit_url, data={'date_facture': date.today(), 'raison': ''})
            self.assertEqual(response.context['form'].errors, {'raison': ['Ce champ est obligatoire.']})
            response = self.client.post(edit_url, data={'date_facture': date.today(), 'raison': 'Bof'})
        self.assertRedirects(response, reverse('client-transports-factures', args=[self.trclient.pk]))
        self.assertEqual(self.trclient.factures_transports.count(), 2)
        facture.refresh_from_db()
        self.assertTrue(facture.annulee)
        self.assertEqual(facture.annulee_msg, "Bof")
        self.assertTrue(self.trclient.factures_transports.filter(annulee=False).exists())

    def test_refacturation_remplacer_plus_de_transport(self):
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('facturer-transports', args=[heure.date().year, heure.date().month]),
            data={'date_facture': date.today()}
        )
        self.assertRedirects(response, reverse('finances'))
        facture = self.trclient.factures_transports.first()
        transport.statut_fact = Transport.FacturationChoices.PAS_FACTURER
        transport.save()
        # Régénérer facture en remplaçant l'ancienne -> pas de nouvelle facture
        response = self.client.post(
            reverse('client-editer-facture', args=[facture.pk]),
            data={'date_facture': date.today()}
        )
        self.assertRedirects(response, reverse('client-transports-factures', args=[self.trclient.pk]))
        self.assertEqual(self.trclient.factures_transports.count(), 0)

    def test_chauffeur_non_defraye(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01],
            ne_pas_defrayer=True,
        )
        day_prev_month = date.today().replace(day=1) - timedelta(days=4)
        rzv = datetime.combine(day_prev_month, time(11, 30), tzinfo=get_current_timezone())
        tr1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=rzv,
            km=D('23.4'), temps_attente=timedelta(minutes=45),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        calculateur = canton_app.fact_policy.calculateur_frais(day_prev_month.replace(day=1))
        calculateur.calculer_frais(benevole=chauffeur, enregistrer=True)
        msg = "Les frais n’ont pas été calculés pour Duplain Irma"
        with self.assertRaisesMessage(RuntimeError, msg):
            note = calculateur.note_frais_pour(chauffeur)


class VehiculeTests(DataMixin, TestCase):
    def test_vehicule_on_home(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertNotContains(response, "Véhicules")
        Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111')
        response = self.client.get(reverse('home'))
        self.assertContains(response, "Véhicules")

    def test_vehicule_list(self):
        vh = Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111')
        self.client.force_login(self.user)
        response = self.client.get(reverse('vehicules'))
        self.assertQuerySetEqual(response.context['object_list'], [vh])

    def test_vehicule_details(self):
        vh = Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111')
        self.client.force_login(self.user)
        response = self.client.get(reverse('vehicule-detail', args=[vh.pk]))
        self.assertContains(response, "Kangoo")

    def test_vehicule_transports(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        vh = Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111')
        heure_transport = datetime.combine(
            date.today() + timedelta(days=2), time(11, 30), tzinfo=get_current_timezone()
        )
        transp_futur = self.create_transport(
            client=self.trclient, chauffeur=chauffeur, vehicule=vh,
            heure_rdv=heure_transport, destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONFIRME,
        )
        transp_passe = self.create_transport(
            client=self.trclient, chauffeur=chauffeur, vehicule=vh,
            heure_rdv=heure_transport - timedelta(days=5),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('vehicule-transports', args=[vh.pk]))
        self.assertContains(
            response, '<img class="icon blacksvg" src="/static/img/aller.svg" title="Aller simple">',
            html=True
        )
        self.assertQuerySetEqual(response.context["object_list"], [transp_futur])
        response = self.client.get(reverse('vehicule-archives', args=[vh.pk]))
        self.assertQuerySetEqual(response.context["object_list"], [transp_passe])

    def _create_vh_transp_occup_modele(self):
        vh = Vehicule.objects.create(modele="Kangoo", no_plaque="JU 111111")
        heure_transport = datetime.combine(
            date.today(), time(16, 30), tzinfo=get_current_timezone()
        )
        tr = self.create_transport(
            client=self.trclient, vehicule=vh,
            heure_rdv=heure_transport, destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI
        )
        modele = self.create_modele(
            date=heure_transport.date() - timedelta(days=7), repetition="WEEKLY",
            vehicule=vh,
        )
        occup = VehiculeOccup.objects.create(
            vehicule=vh,
            duree=(heure_transport - timedelta(hours=9), heure_transport - timedelta(hours=6)),
            description="Garage"
        )
        occup.refresh_from_db()
        return vh, tr, occup, modele

    def test_vehicule_agenda_view(self):
        vh, tr, occup, modele = self._create_vh_transp_occup_modele()
        self.client.force_login(self.user)
        response = self.client.get(reverse("vehicule-agenda", args=[
            vh.pk, tr.date.isocalendar().year, tr.date.isocalendar().week
        ]))
        self.assertContains(response, "Transport pour Donzé Léa")
        self.assertContains(response, "Garage")
        day_events = response.context["agendas"][0]["week_events"][date.today()]["events"]
        self.assertEqual(day_events[time(7, 30)], [occup])
        self.assertEqual(day_events[time(10, 15)][0].modele, modele)
        self.assertEqual(day_events[time(16, 0)], [tr])

    def test_vehicule_day_dispos_view(self):
        vh, tr, occup, modele = self._create_vh_transp_occup_modele()
        tr_cible = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today(), time(12, 00), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('vehicule-dispo-transport', args=[vh.pk, tr_cible.pk]))
        day_events = response.context["agenda"]["week_events"][date.today()]["events"]
        self.assertEqual(day_events[time(7, 30)], [occup])
        self.assertEqual(day_events[time(10, 15)][0].modele, modele)
        self.assertEqual(day_events[time(11, 30)], [tr_cible])
        self.assertEqual(day_events[time(16, 0)], [tr])

    def test_vehicule_indisponibilites(self):
        vh = Vehicule.objects.create(modele='Kangoo', no_plaque='JU 111111')
        today = date.today()
        self.client.force_login(self.user)
        response = self.client.post(reverse("vehicule-indispo-add", args=[vh.pk, "indispo"]), data={
            'duree_0': [today.strftime('%Y-%m-%d')], 'duree_1': ['12:45'],
            'duree_2': [today.strftime('%Y-%m-%d')], 'duree_3': ['14:15'],
            'description': "Garage",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        indispo = vh.indisponibilites.first()
        self.assertTrue(indispo.can_edit(self.user))
        response = self.client.post(reverse('vehicule-indispo-edit', args=[indispo.pk]), data={
            'duree_0': [today.strftime('%Y-%m-%d')], 'duree_1': ['13:00'],
            'duree_2': [today.strftime('%Y-%m-%d')], 'duree_3': ['15:00'],
            'description': "Garage",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        indispo.refresh_from_db()
        self.assertEqual(localtime(indispo.duree.upper).time(), time(15, 0))
        response = self.client.post(reverse('vehicule-indispo-delete', args=[indispo.pk]), data={})
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(vh.indisponibilites.count(), 0)

    def test_vehicule_reservations(self):
        vh = Vehicule.objects.create(modele="Kangoo", no_plaque="JU 111111")
        today = date.today()
        self.client.force_login(self.user)
        response = self.client.post(reverse("vehicule-indispo-add", args=[vh.pk, "reserv"]), data={
            "client_select": "xy",
            "client": self.trclient.pk,
            "duree_0": [today.strftime("%Y-%m-%d")], "duree_1": ["12:45"],
            "duree_2": [today.strftime("%Y-%m-%d")], "duree_3": ["14:15"],
            "description": "Réservation excursion",
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        reserv = vh.indisponibilites.first()
        self.assertTrue(reserv.can_edit(self.user))
        response = self.client.post(reverse("vehicule-indispo-edit", args=[reserv.pk]), data={
            "client_select": "xy",
            "client": self.trclient.pk,
            "duree_0": [today.strftime("%Y-%m-%d")], "duree_1": ["13:00"],
            "duree_2": [today.strftime("%Y-%m-%d")], "duree_3": ["15:00"],
            "kms": "36",
            "description": reserv.description,
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        reserv.refresh_from_db()
        self.assertEqual(localtime(reserv.duree.upper).time(), time(15, 0))
        self.assertEqual(reserv.kms, 36)
        response = self.client.post(reverse("vehicule-indispo-delete", args=[reserv.pk]), data={})
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertEqual(vh.indisponibilites.count(), 0)

    def test_calc_chauffeur_dists_avec_vehicule(self):
        vh = Vehicule.objects.create(modele="Kangoo", no_plaque="JU 111111", empl_geo=[6.74, 46.88])
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        heure_transport = datetime.combine(
            date.today() + timedelta(days=2), time(11, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=self.trclient, chauffeur=chauffeur, vehicule=vh,
            heure_rdv=heure_transport, destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONFIRME,
        )
        transport.calc_chauffeur_dists(save=False)
        self.assertEqual(transport.chauff_aller_dist, 40500)
        self.assertEqual(transport.chauff_aller_duree, timedelta(seconds=1960))
        self.assertEqual(transport.chauff_retour_dist, 36100)
        self.assertEqual(transport.chauff_retour_duree, timedelta(seconds=1960))


class ClientTests(DataMixin, TestCase):
    def test_ajout_client(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-new'))
        self.assertContains(response, '<h2 class="col">Nouveau client</h2>', html=True)
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(reverse('client-new'), data={
                'nom': 'Donzé', 'prenom': 'Léa',
                'rue': 'Rue des Crêtets 92', 'npalocalite': '2300 La Chaux-de-Fonds',
                'date_naissance': '1952-10-23',
                "telephone_set-INITIAL_FORMS": 0, "telephone_set-TOTAL_FORMS": 0,
            })
            mocked.assert_called_once()  # geolocation
        client = Client.objects.get(persona__date_naissance=date(1952, 10, 23))
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual(client.prestations_actuelles(as_services=True), [TRANSPORT])

    def test_archiver_client_avec_transports(self):
        heure_transport = datetime.combine(
            date.today(), time(11, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient, heure_rdv=heure_transport,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        transp_futur = self.create_transport(
            client=self.trclient, heure_rdv=heure_transport + timedelta(days=10),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.SAISI
        )
        modele = self.create_modele()

        self.client.force_login(self.user)
        arch_url = reverse('client-archive', args=[self.trclient.pk])
        response = self.client.post(arch_url, {})
        self.assertRedirects(response, reverse('clients'))
        self.trclient.refresh_from_db()
        self.assertEqual(self.trclient.prestations.first().duree.upper, date.today())
        transp_futur.refresh_from_db()
        self.assertEqual(transp_futur.statut, Transport.StatutChoices.ANNULE)
        self.assertEqual(transp_futur.raison_annulation, "Archivage du client")
        modele.refresh_from_db()
        self.assertEqual(modele.fin_recurrence.date(), date.today())

    @freeze_time("2022-03-22")
    def test_client_avs(self):
        clients = Client.objects.bulk_create([
            # AVS
            Client(persona=Persona.objects.create(
                nom='01', genre='M', empl_geo=[7, 47], date_naissance=date(2022 - 65, 2, 3)
            )),
            Client(persona=Persona.objects.create(
                nom='02', genre='F', empl_geo=[7, 47], date_naissance=date(2022 - 64, 2, 3))),
            Client(
                persona=Persona.objects.create(
                    nom='03', genre='M', empl_geo=[7, 47], date_naissance=date(2022 - 61, 12, 3),
                ),
                tarif_avs_des=date.today() - timedelta(days=2),
            ),
            # Non AVS
            Client(persona=Persona.objects.create(
                nom='04', genre='M', empl_geo=[7, 47], date_naissance=date(2022 - 65, 10, 3)
            )),
            Client(persona=Persona.objects.create(
                nom='05', genre='F', empl_geo=[7, 47], date_naissance=date(2022 - 64, 10, 3)
            )),
        ])
        heure_transport = datetime.combine(
            date.today(), time(11, 30), tzinfo=get_current_timezone()
        )
        self.assertIs(clients[0].is_avs(heure_transport.date()), True)
        self.assertIs(clients[1].is_avs(heure_transport.date()), True)
        self.assertIs(clients[2].is_avs(heure_transport.date()), True)

        self.assertIs(clients[3].is_avs(heure_transport.date()), False)
        self.assertIs(clients[4].is_avs(heure_transport.date()), False)

        with ignore_warnings(category=RuntimeWarning):
            for cl in clients:
                self.create_transport(
                    client=cl, heure_rdv=heure_transport,
                    duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
                    statut=Transport.StatutChoices.CONTROLE
                )
        # Fonction is_ofas en base de données
        query = Transport.objects.avec_is_ofas().values_list(
            'client__persona__nom', 'is_ofas'
        ).order_by('client__persona__nom')
        self.assertQuerySetEqual(
            query,
            [('01', True), ('02', True), ('03', True), ('04', False), ('05', False)]
        )

    def test_client_edit(self):
        """L'édition d'un client alarme crée une alerte pour l'alarme."""
        client_data = {
            'nom': 'Donzé', 'prenom': 'Léa',
            'rue': 'Rue des Crêtets 92', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-23',
        }
        client = Client.objects.create(**{
            **client_data,
            'service': [Services.ALARME, TRANSPORT],
            'empl_geo': [6.82058572769165, 47.093910217285156],
        })
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertNotContains(response, reverse("client-add-service", args=[client.pk]))
        self.assertContains(
            response,
            '<input type="text" name="nom" maxlength="60" class="form-control" required="" id="id_nom" value="Donzé">',
            html=True
        )
        self.assertContains(
            response,
            '<input type="date" name="date_naissance" class="form-control" id="id_date_naissance" value="1950-05-23">',
            html=True
        )
        self.assertContains(
            response,
            '<label for="id_remarques_ext_transport">Remarques (visibles par bénévoles transports)&nbsp:</label>',
            html=True
        )
        client_data.update({
            'nom': 'Jobin',
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 1,
            "telephone_set-0-tel": "077 888 88 88",
            'handicaps': ['deamb'],
        })
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.post(
                reverse('client-edit', args=[client.pk]), data=client_data, follow=True
            )
        self.assertEqual(
            [msg.message for msg in list(response.context['messages'])],
            ['«Jobin Léa» a bien été modifié·e']
        )
        self.assertEqual(client.alertes.count(), 1)
        alerte = client.alertes.first()
        self.assertEqual(
            alerte.alerte,
            "Modifications depuis l’application «transport»: nom (de «Donzé» à «Jobin»), "
            "téléphone: ajout de numéro («077 888 88 88»), ajout de handicaps («Déambulateur»)"
        )
        # Si le client n'est que 'transport', il peut être archivé.
        hier = date.today() - timedelta(days=1)
        prest_al = client.prestations.filter(service=Services.ALARME).update(duree=(hier, hier))
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertContains(response, "Archiver")

    def test_referent_edit(self):
        ref = Referent.objects.create(
            client=self.trclient, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            facturation_pour=['transp'],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-referent-edit', args=[self.trclient.pk, ref.pk]))
        self.assertContains(response, "Arthur")
        self.assertNotContains(response, "courrier initial")

    def test_client_archive_transports(self):
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=self.demain_1130 - timedelta(days=4),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=self.demain_1130 - timedelta(days=2),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI
        )
        self.client.force_login(self.user)
        url = reverse('client-transports-archives', args=[self.trclient.pk])
        response = self.client.get(url)
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(f"{url}?statut={Transport.StatutChoices.CONTROLE}")
        self.assertEqual(len(response.context['object_list']), 1)

    def test_client_factures(self):
        self.client.force_login(self.user)
        url = reverse('client-transports-factures', args=[self.trclient.pk])
        response = self.client.get(url)
        self.assertEqual(self.trclient.factures_transports.count(), 0)
        self.assertContains(response, "Aucune facture à ce jour")

    def test_client_details_json(self):
        self.client.force_login(self.user)
        details_url = reverse("client-details-transport") + f"?id={self.trclient.pk}"
        response = self.client.get(details_url)
        self.assertEqual(
            response.json(),
            {'localite': 'La Chaux-de-Fonds', 'npa': '2300', 'rue': 'Rue des Crêtets 92',
             'fonds_transport': False, 'type_logement': '', 'laa': None,
             'remarques': '', 'geolocalise': True}
        )
        # Maintenant avec adresse dépose
        adr = AdresseClient.objects.create(
            persona=self.trclient.persona, rue="Rue du Stand 1", npa="2000", localite="Neuchâtel",
            depose=True, empl_geo=[6.8, 47.1]
        )
        AdressePresence.objects.create(adresse=adr, depuis=date.today())
        self.trclient.refresh_from_db()
        response = self.client.get(details_url)
        self.assertEqual(
            response.json(),
            {'localite': 'Neuchâtel', 'npa': '2000', 'rue': 'Rue du Stand 1',
             'fonds_transport': False, 'type_logement': '', 'laa': None,
             'remarques': '', 'geolocalise': True}
        )

    def test_client_liste_archive(self):
        cl1 = Client.objects.create(
            service=TRANSPORT, nom='Donzé1', prenom='Léa1', archive_le=date.today(),
        )
        # Afficher aussi les clients archivés pour les transports (=ayant eu un transport dans le passé),
        # mais actifs dans un autre service
        cl2 = Client.objects.create(
            service=Services.ALARME, nom='Donzé2', prenom='Léa2', archive_le=None,
        )
        Prestation.objects.create(client=cl2, service=Services.TRANSPORT, duree=(
            date.today() - timedelta(days=120), date.today() - timedelta(days=80)
        ))
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-archives') + '?nom_prenom=don')
        self.assertQuerySetEqual(response.context['object_list'], [cl1, cl2])

    def test_client_list_filter(self):
        client = Client.objects.create(
            service=TRANSPORT, nom='Donzé1', prenom='Léa1',
        )
        Referent.objects.create(
            client=client, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            facturation_pour=['transp'],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients') + '?listes=autres_debit')
        self.assertContains(response, "Clients avec autre débiteur")
        self.assertEqual(len(response.context['object_list']), 1)

    def test_export_tous_clients(self):
        Client.objects.create(
            service=TRANSPORT, nom='Donzé1', prenom='Léa1',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients') + '?listes=exp_complet')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        content = read_xlsx(response.content)
        # en-têtes + 2 clients
        self.assertEqual(len(content), 3)

    def test_client_list_alertes(self):
        client = Client.objects.create(
            service=TRANSPORT, nom='Donzé1', prenom='Léa1',
            rue="Rue Principale 4", npa="1111", localite="Quelquepart"
        )
        al = Alerte.objects.create(
            persona=client.persona, cible=Services.TRANSPORT, alerte="Test", recu_le=now()
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-alertes'))
        self.assertQuerySetEqual(response.context['object_list'], [al])
        self.assertContains(response, "1111 Quelquepart")

    def test_client_decede(self):
        client_tr = self.trclient
        client_altr = Client.objects.create(
            service=[Services.ALARME, TRANSPORT], nom='Donzé1', prenom='Léa1',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        self.client.force_login(self.user)
        self.client.post(reverse('client-edit', args=[client_tr.pk]), data={
            'nom': 'Donzé', 'prenom': 'Léa',
            'rue': 'Rue des Crêtets 92', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-23', 'npalocalite': '2300 La Chaux-de-Fonds',
            'date_deces': '2022-11-11',
            "telephone_set-INITIAL_FORMS": 0, "telephone_set-TOTAL_FORMS": 0,
        })
        client_tr.refresh_from_db()
        # Archivage direct si le client n'est client que de l'application courante
        self.assertEqual(client_tr.prestations.first().duree.upper, date.today())

        self.client.post(reverse('client-edit', args=[client_altr.pk]), data={
            'nom': 'Donzé1', 'prenom': 'Léa1',
            'rue': 'Rue des Crêtets 94', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-24', 'npalocalite': '2300 La Chaux-de-Fonds',
            'date_deces': '2022-11-11',
            "telephone_set-INITIAL_FORMS": 0, "telephone_set-TOTAL_FORMS": 0,
        })
        client_altr.refresh_from_db()
        self.assertIsNone(client_altr.archive_le)
        self.assertEqual(client_altr.prestations_actuelles(as_services=True), ['alarme'])
        self.assertEqual(
            client_altr.alertes.first().alerte,
            "L’équipe «transport» a marqué cette personne comme décédée"
        )


class StatsTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = cls._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )

    def test_stats_index(self):
        mois_proch = datetime.combine(
            date.today() + relativedelta.relativedelta(months=1),
            time(11, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            chauffeur=self.chauffeur, retour=True
        )
        self.create_transport(
            client=self.trclient, heure_rdv=mois_proch,
            destination=self.hne_ne, retour=False
        )
        self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.ANNULE,
            motif_annulation=Transport.AnnulationChoices.AUCUN_CHAUFFEUR,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-index'))
        self.assertContains(response, '<td>Nbre de clients</td>')

    def test_stats_chauffeurs(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-chauffeurs'))
        self.assertContains(response, '<td>Nbre de chauffeurs actifs')
        self.assertEqual(response.context['stats']['nb_chauffeurs_dispos']['total'], 1)

    def test_stats_ofas(self):
        client_non_avs = Client.objects.create(
            service=TRANSPORT, nom='Smith', prenom='Jill',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(date.today().year - 60, 5, 23),
            empl_geo=[6.815, 47.094], valide_des='2023-01-01',
        )
        heure = now().replace(year=2023)
        self.create_transport(
            client=self.trclient, heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            km=20, retour=True, statut=Transport.StatutChoices.CONTROLE,
        )
        self.create_transport(
            client=client_non_avs, heure_rdv=heure,
            destination=self.hne_ne, retour=False,
            km=13, chauffeur=self.chauffeur,
            statut=Transport.StatutChoices.CONTROLE,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-ofas') + '?year=2023')
        self.assertEqual(
            response.context['stats'][Month(year=2023, month=date.today().month)], {
            'month': date(2023, date.today().month, 1),
            'count_cl': 2,
            'count_chauff': 1,
            'count_transp': 3,  # Aller-retour compte pour 2
            'sum_km': D('33.0'),
            'types_traj': {'autres': D('13.0'), 'medic': D('20.0')},
            'sum_duree': timedelta(0),
        })
        response = self.client.get(reverse('stats-ofas') + '?year=2023&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)


class OtherTests(DataMixin, TestCase):
    def setUp(self):
        super().setUp()
        # Vider cache des distances
        cache.clear()

    def test_date_transp_tag(self):
        from transport.templatetags.transport_utils import date_transp

        self.assertEqual(
            date_transp(datetime(2023, 3, 12, 12, 0, tzinfo=get_current_timezone())),
            'DI 12.03.2023'
        )
        self.assertEqual(
            date_transp(datetime(2023, 3, 11, 23, 30, tzinfo=timezone.utc)),
            'DI 12.03.2023'
        )
        self.assertEqual(
            date_transp(date(2023, 3, 12)),
            'DI 12.03.2023'
        )

    @freeze_time("2023-03-22")
    def test_cancel_dispo_through_dst(self):
        dispo = Dispo.objects.create(
            chauffeur=self._create_benevole(nom="Duplain", prenom="Irma", activites=['transport']),
            debut=now().replace(hour=14, minute=0, microsecond=0),
            fin=now().replace(hour=17, minute=30, microsecond=0),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        dispo.cancel_for((dispo.debut + timedelta(days=7)).date())
        start = now().replace(hour=1)
        end = now().replace(hour=1) + timedelta(days=15)
        occs = dispo.get_occurrences(start, end)
        self.assertEqual(
            [o.debut.date() for o in occs if not o.cancelled],
            [date(2023, 3, 22), date(2023, 4, 5)]
        )
        # Même test avec chevauchement entre dispo et événement
        start = (now() + timedelta(days=7)).replace(hour=14, minute=30)
        occs = dispo.get_occurrences(start, start + timedelta(hours=2))
        self.assertIs(list(occs)[0].cancelled, True)

    def test_search_ch_view(self):
        self.client.force_login(self.user)
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.get(reverse('searchch-autocomplete') + '?q=john+meier')
        results = response.json()
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0], {
            'label': 'Meier John, Marienfeldstrasse 92, 8252 Schlatt', 'value': 'urn:uuid:b4f420fda52419f2',
            'details': {
                'nom': 'Meier John', 'npa': '8252', 'localite': 'Schlatt',
                'rue': 'Marienfeldstrasse 92', 'tel': '+41526544230',
            }
        })
        self.assertEqual(results[1]['label'], "John Meier IT Consulting, Unterdorfstrasse 22, 4143 Dornach")

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    @patch('common.distance.httpx.get', side_effect=httpx.TimeoutException("Timeout"))
    def test_openrouteservice_unavailable(self, *args):
        with self.assertRaises(ORSUnavailable):
            distance_real((1, 1), (2, 2))

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    @patch('common.distance.httpx.get', side_effect=mocked_httpx)
    def test_openrouteservice_backup(self, *args):
        """If openrouteservice is offline, backup is router.project-osrm.org."""
        from common.test_utils import MOCKED_OSRM_DURATION, MOCKED_OSRM_DISTANCE

        self.assertEqual(
            distance_real((1, 1), (3, 3)),
            {'duration': MOCKED_OSRM_DURATION, 'distance': MOCKED_OSRM_DISTANCE}
        )

    def test_distance_figee(self):
        dep = [6.94, 46.995]
        arr = [6.82, 47.096]
        DistanceFigee.objects.create(
            empl_geo_dep=dep, empl_geo_arr=arr, distance=D('7.7')
        )
        result = distance_real(dep, arr)
        self.assertEqual(result['distance_calc'], 14500)
        self.assertEqual(result['distance'], 7700)

    def test_trajet_details(self):
        transport = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, duree_rdv=timedelta(minutes=70),
            retour=True
        )
        self.client.force_login(self.user)
        details_url = reverse('trajets-details', args=[transport.pk])
        response = self.client.get(details_url)
        self.assertContains(response, "14.6km")
        chauffeur = self.create_chauffeur()
        transport.chauffeur = chauffeur
        transport.save()
        transport.calc_chauffeur_dists(save=True)
        response = self.client.get(details_url)
        self.assertContains(response, "Trajet aller du chauffeur (Duplain Irma):")
        self.assertContains(response, "Distance totale estimée")

    def test_trajet_force_distance(self):
        transport = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, duree_rdv=timedelta(minutes=70),
            retour=True
        )
        trajet = transport.trajets_tries[0]
        self.assertEqual(trajet.dist_calc, 14600)
        self.client.force_login(self.user)
        details_url = reverse('trajets-details', args=[transport.pk])
        response = self.client.post(reverse('trajet-set-distance', args=[trajet.pk]), data={
            'distance_km': "",
        }, follow=True)
        self.assertRedirects(response, details_url)
        self.assertContains(response, "Vous devez indiquer une distance en km")
        response = self.client.post(reverse('trajet-set-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })
        self.assertRedirects(response, details_url)
        trajet.refresh_from_db()
        self.assertEqual(trajet.dist_calc, 10400)
        # Figer la distance, applicable pour tout nouveau transport similaire
        response = self.client.post(reverse('trajet-set-fixed-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })
        self.assertRedirects(response, details_url)
        nouveau = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, duree_rdv=timedelta(minutes=70),
            retour=True
        )
        self.assertEqual(nouveau.trajets_tries[0].dist_calc, 10400)
        # Nouvelle requête pour même trajet dne doit pas planter
        response = self.client.post(reverse('trajet-set-fixed-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })

    def test_duration_field(self):
        field = HMDurationField()
        good_values = (
            ('12:30', timedelta(hours=12, minutes=30)),
            ('12h30', timedelta(hours=12, minutes=30)),
            ('12.30', timedelta(hours=12, minutes=30)),
            ('0045', timedelta(minutes=45)),
            ('2:0', timedelta(hours=2)),
            ('2h', timedelta(hours=2)),
        )
        for in_value, expected in good_values:
            with self.subTest(in_value):
                self.assertEqual(field.to_python(in_value), expected)
        bad_values = (
            'ah30', '2',
        )
        for in_value in bad_values:
            with self.subTest(in_value):
                with self.assertRaises(ValidationError):
                    field.to_python(in_value)

    @override_settings(QUERY_GEOADMIN_FOR_ADDRESS=False)
    def test_phonenumber_normalization(self):
        form = AdresseEditForm(instance=None, data={
            'nom': "Dr Schmid", 'rue': "Rue du Musée 2", 'npalocalite': "2000 Neuchâtel",
            'tel': "+41325556677", 'validite_0': '2023-01-01',
        })
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Adresse.objects.get(nom="Dr Schmid").tel, "032 555 66 77")
        form = AdresseEditForm(instance=None, data={
            'nom': "Dr Akena", 'rue': "10 Place de la Gare", 'npalocalite': "25500 Morteau",
            'tel': "+33388660000", 'validite_0': '2023-01-01',
        })
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Adresse.objects.get(nom="Dr Akena").tel, "+33 3 88 66 00 00")

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    @patch('common.distance.httpx.get', side_effect=httpx.TimeoutException("Timeout"))
    def test_trajet_distance_entre(self, *args):
        client = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Léa', no_debiteur=999,
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.820, 47.094]
        )
        with self.assertRaises(ORSUnavailable):
            Trajet.distance_entre(client, client.adresse(), self.hne_ne)
        with self.assertRaises(ORSUnavailable):
            Trajet.distance_entre(client, self.hne_ne, client.adresse())

        heure = now() - timedelta(days=10)
        transp = Transport.objects.create(client=client, date=heure.date(), heure_rdv=heure)
        Trajet.objects.create(
            transport=transp,
            heure_depart=heure,
            origine_domicile=True, destination_adr=self.hne_ne, dist_calc=20120
        )
        Trajet.objects.create(
            transport=transp,
            heure_depart=heure,
            origine_adr=self.hne_ne, destination_domicile=True, dist_calc=20140
        )
        self.assertEqual(Trajet.distance_entre(client, client.adresse(), self.hne_ne), 20120)
        self.assertEqual(Trajet.distance_entre(client, self.hne_ne, client.adresse()), 20140)


def pdf_text(response, pages=(0,)):
    reader = PdfReader(BytesIO(response.getvalue()))
    return [reader.pages[idx_page].extract_text() for idx_page in pages]
