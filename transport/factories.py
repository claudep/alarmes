import factory
from factory.django import DjangoModelFactory

from client.factories import ClientFactory
from . import models


class TransportFactory(DjangoModelFactory):
    class Meta:
        model = models.Transport

    client = factory.SubFactory(ClientFactory)
    heure_rdv = factory.Faker('date_time')
    date = factory.LazyAttribute(lambda o: o.heure_rdv.date())
    statut = models.Transport.StatutChoices.CONTROLE
    retour = False
