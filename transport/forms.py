import copy
import unicodedata
from datetime import date, datetime, timedelta

from django import forms
from django.db import transaction
from django.db.models import F, Max, Q, Value
from django.urls import reverse_lazy
from django.utils.html import format_html
from django.utils.timezone import get_current_timezone, localtime, make_aware, now

from benevole.forms import BenevoleFilterForm
from client.forms import (
    ClientAutresDebiteursFilter, ClientEditFormBase, ClientFilter, ClientFilterFormBase
)
from client.models import Client
from common.distance import ORSUnavailable
from common.forms import (
    BootstrapMixin, BSCheckboxSelectMultiple, BSRadioSelect, DateInput,
    DateRangeField, DateTimeRangeField, GeoAddressFormMixin, HiddenDeleteInlineFormSet,
    HMDurationField, ModelForm, NPALocaliteMixin, PermissiveHourValue,
    RegionField, TimeInput
)
from common.functions import PtDist
from common.templatetags.common_utils import format_duree
from common.utils import RegionFinder, fact_policy
from .models import (
    Adresse, Benevole, Dispo, Frais, JournalTransport, Preference, Regle, Trajet,
    TrajetCommun, Transport, TransportModel, Vehicule, VehiculeOccup,
)

REPET_CHOICES = (
    ('', 'Aucune'),
    ('WEEKLY', 'Chaque semaine'),
    ('BI-WEEKLY', 'Une semaine sur deux'),
    ('DAILY', 'Chaque jour'),
    ('DAILY-open', 'Chaque jour, du lundi au vendredi'),
)

DOMICILE_VALUE = Adresse(nom="Domicile")


class AllerRetourWidget(BSRadioSelect):
    template_name = 'transport/widgets/allerretour.html'


class RepetitionMixin:
    def __init__(self, **kwargs):
        if kwargs.get("instance") and getattr(kwargs["instance"], "regle", None):
            # Transformation d'une valeur de Règle en une valeur de REPET_CHOICES.
            obj = kwargs["instance"]
            kwargs["initial"]["repetition"] = {v: k for k, v in REPET_CHOICES}[obj.regle.nom]
            if obj.fin_recurrence:
                kwargs["initial"]["repetition_fin"] = obj.fin_recurrence.date()
        super().__init__(**kwargs)

    def set_repetition(self, modele):
        if 'repetition' in self.changed_data:
            repet = self.cleaned_data.get('repetition')
            if repet == 'WEEKLY':
                modele.regle, _ = Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')
            elif repet == 'BI-WEEKLY':
                modele.regle, _ = Regle.objects.get_or_create(
                    nom='Une semaine sur deux', frequence='WEEKLY', params='interval:2'
                )
            elif repet == 'DAILY':
                modele.regle, _ = Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')
            elif repet == 'DAILY-open':
                modele.regle, _ = Regle.objects.get_or_create(
                    nom='Chaque jour, du lundi au vendredi', frequence='DAILY',
                    params='byweekday:MO,TU,WE,TH,FR'
                )
        if 'repetition_fin' in self.changed_data:
            repet_fin = self.cleaned_data.get('repetition_fin')
            if repet_fin:
                modele.fin_recurrence = make_aware(
                    datetime(repet_fin.year, repet_fin.month, repet_fin.day, 23, 59)
                )


class DestinationField(forms.ModelChoiceField):
    def clean(self, value):
        if value == "-1":
            return DOMICILE_VALUE
        return super().clean(value)


class TrajetForm(BootstrapMixin, ModelForm):
    destination_adr = DestinationField(queryset=Adresse.objects.all(), widget=forms.HiddenInput, required=False)
    destination_adr_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Destination client…',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('adresse-autocomplete'),
        'data-pkfield': 'destination_adr',
    }), required=False)

    class Meta:
        model = Trajet
        fields = ['destination_adr', 'destination_princ']
        widgets = {'destination_princ': forms.HiddenInput}

    def __init__(self, **kwargs):
        if kwargs.get("instance") and kwargs['instance'].destination_domicile:
            kwargs['initial'] = {'destination_adr': -1}
        super().__init__(**kwargs)
        self.fields['destination_adr_select'].widget.attrs['data-pkfield'] = f'{self.prefix}-destination_adr'


class BaseTrajetFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput
    ordering_widget = forms.HiddenInput


class HeureField(forms.TimeField):
    def has_changed(self, initial, data):
        if isinstance(initial, str) and isinstance(data, str) and initial == data:
            return False
        try:
            return not datetime.strptime(data, '%H:%M').time() == localtime(initial).time()
        except Exception:
            pass
        return super().has_changed(initial, data)


class SaisieTransportForm(BootstrapMixin, RepetitionMixin, ModelForm):
    client = forms.ModelChoiceField(queryset=Client.objects.all(), widget=forms.HiddenInput)
    client_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Client…',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('client-autocomplete', args=['tous']),
        'data-clienturl': reverse_lazy('client-details-transport'),
        'data-pkfield': 'client',
    }))
    origine_adr = forms.ModelChoiceField(queryset=Adresse.objects.all(), widget=forms.HiddenInput, required=False)
    origine_adr_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Départ du transport… (domicile par défaut)',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('adresse-autocomplete'),
        'data-pkfield': 'origine_adr',
    }), required=False)
    heure_depart = HeureField(label="Heure de départ", widget=TimeInput, required=True)
    heure_rdv = HeureField(label="Heure du rendez-vous", widget=TimeInput(), required=True)
    duree_rdv = HMDurationField(required=False)
    typ = forms.ChoiceField(label="Type de déplacement", choices=Trajet.Types.choices, required=True)
    retour = forms.BooleanField(
        widget=AllerRetourWidget(choices=((True, 'Aller-retour'), (False, 'Aller simple'))),
        initial=True, required=False,
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = Transport
        exclude = ['chauffeur']
        fields = [
            "client", "date", "heure_rdv", "duree_rdv", "retour", "vehicule", "remarques",
        ]

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        if not kwargs.get("instance") or not Vehicule.objects.exists():
            del self.fields["vehicule"]
        self.ors_available = True
        TrajetFormSet = forms.inlineformset_factory(
            Transport, Trajet, form=TrajetForm, formset=BaseTrajetFormSet,
            extra=0 if (self.instance.has_trajets and self.instance.retour) else 1,
            min_num=1, can_delete=True, can_order=True
        )
        self.formset = TrajetFormSet(
            data=data, instance=kwargs['instance'],
        )
        if kwargs['instance']:
            # Surtout important pour les instances de transport en mémoire provenant des modèles.
            self.formset._queryset = copy.deepcopy(kwargs['instance'].trajets_tries)
        self.formset.forms[0].fields['destination_adr'].required = True
        if self.instance.pk is None:
            self.formset.forms[0].fields['destination_princ'].initial = True
        else:
            self.fields['heure_depart'].initial = self.instance.heure_depart.strftime('%H:%M')
            if self.instance.retour:
                try:
                    duree = format_duree(self.instance.duree_retour)
                except ORSUnavailable:
                    self.ors_available = False
                else:
                    self.fields['heure_depart'].widget.attrs['data-dureetrajet'] = duree
        if self.instance.pk or self.instance.modele_id:
            del self.fields['client']
            del self.fields['client_select']
            del self.fields['repetition']
            del self.fields['repetition_fin']

    def is_valid(self):
        return all([self.formset.is_valid(), super().is_valid()])

    def clean_client(self):
        client = self.cleaned_data.get('client')
        if client and not client.date_naissance:
            raise forms.ValidationError("Impossible de créer un transport pour un client sans date de naissance")
        return client

    def clean(self):
        cleaned_data = super().clean()
        client = cleaned_data.get('client')
        if client and not client.adresse(cleaned_data['date']).check_geolocalized():
            self.add_error('client', "L’adresse de cette personne ne peut pas être géolocalisée.")
        if cleaned_data.get('heure_rdv'):
            cleaned_data['heure_rdv'] = datetime.combine(
                cleaned_data['date'], cleaned_data['heure_rdv'], tzinfo=get_current_timezone()
            )
        if cleaned_data.get('retour') and not cleaned_data.get('duree_rdv'):
            self.add_error('duree_rdv', "La durée de rendez-vous est obligatoire.")
        if self.errors or not len(self.formset.forms) or self.formset.total_error_count() > 0:
            return cleaned_data

        def check_orig_dest(traj):
            if traj.origine_adr and traj.destination_adr and traj.origine_adr == traj.destination_adr:
                self.add_error(None, "L’origine et la destination d’un trajet doivent être différents")
            elif traj.origine_domicile and traj.destination_domicile:
                # Not using add_error, because later calling trajet.destination will crash
                raise forms.ValidationError("L’origine et la destination ne peuvent pas être les deux le domicile")

        def set_destination(trajet, dest):
            if dest is DOMICILE_VALUE or dest is None:
                trajet.destination_domicile = True
                trajet.destination_adr = None
            else:
                trajet.destination_domicile = False
                trajet.destination_adr = dest
            check_orig_dest(trajet)

        self.trajets = []
        heure_premier_depart = datetime.combine(
            cleaned_data['date'], cleaned_data['heure_depart'],
            tzinfo=get_current_timezone()
        )
        trajet_initial = True
        ordered_forms = self.formset.ordered_forms
        for idx, subform in enumerate(ordered_forms):
            if subform.cleaned_data.get('destination_adr') is None:
                continue
            if trajet_initial:
                self.trajets.append(
                    Trajet(
                        transport=None, typ=self.cleaned_data.get('typ'),
                        heure_depart=heure_premier_depart,
                        origine_domicile=cleaned_data['origine_adr'] is None,
                        origine_adr=cleaned_data['origine_adr'],
                        destination_princ=subform.cleaned_data['destination_princ'],
                    )
                )
                set_destination(self.trajets[-1], subform.cleaned_data['destination_adr'])
                trajet_initial = False
                continue
            if self.trajets[-1].destination_princ and not cleaned_data['retour']:
                break
            is_last_subform = idx >= len(ordered_forms) - 1
            if not is_last_subform or not cleaned_data['retour']:
                self.trajets.append(
                    Trajet(
                        transport=None, typ=cleaned_data.get('typ'),
                        origine_domicile=False,
                        origine_adr=self.trajets[-1].destination,
                        destination_princ=subform.cleaned_data['destination_princ'],
                    )
                )
                set_destination(self.trajets[-1], subform.cleaned_data['destination_adr'])

        if cleaned_data['retour']:
            # If last subform.destination_adr is set, use that here
            # Extra form in initial creation is not in ordered_form (extra unchanged form)
            last_form = ordered_forms[-1] if len(ordered_forms) > 1 else self.formset.forms[-1]
            last_destination = last_form.cleaned_data.get('destination_adr')
            if last_destination and last_destination is not DOMICILE_VALUE:
                dest_adr = self.formset.forms[-1].cleaned_data.get('destination_adr')
                dest_dom = False
            else:
                dest_adr = None
                dest_dom = True
            self.trajets.append(
                Trajet(
                    transport=None, typ=cleaned_data.get('typ'),
                    origine_domicile=self.trajets[-1].destination_domicile,
                    origine_adr=self.trajets[-1].destination if not self.trajets[-1].destination_domicile else None,
                    destination_princ=False,
                )
            )
            set_destination(self.trajets[-1], last_destination)
        return cleaned_data

    def save(self, **kwargs):
        should_save_trajets = (
            self.instance.pk is None or
            self.formset.has_changed() or
            len(set(self.changed_data) - set([
                # Champs qui ne nécessitent pas un recalcul des trajets (pour typ, voir plus bas)
                'typ', 'remarques',
            ]))
        )
        with transaction.atomic():
            if not self.instance.pk and self.instance.modele:
                self.instance.save()
            transp = super().save(**kwargs)
            if self.cleaned_data.get('repetition'):
                modele = TransportModel.objects.create(
                    client=transp.client,
                    heure_depart=self.cleaned_data['heure_depart'],
                    heure_rdv=transp.heure_rdv,
                    duree_rdv=transp.duree_rdv,
                    retour=transp.retour,
                    remarques=transp.remarques,
                )
                self.set_repetition(modele)
                transp.modele = modele
                transp.save()
                modele.save()

            if should_save_trajets:
                transp.trajets.all().delete()
                del transp.trajets_tries
                trajet_prec = None
                for trajet in self.trajets:
                    trajet.transport = transp
                    if not trajet.heure_depart and trajet_prec:
                        if trajet_prec.destination_princ:
                            trajet.heure_depart = transp.heure_rdv + transp.duree_rdv
                        else:
                            # En attendant que trajet possède une durée de rdv
                            trajet.heure_depart = trajet_prec.heure_arrivee
                    trajet.save()
                    trajet.calc_trajet()
                    trajet_prec = trajet
                if transp.chauffeur:
                    transp.calc_chauffeur_dists(save=True)
                if transp.statut >= Transport.StatutChoices.RAPPORTE:
                    policy = fact_policy()
                    if policy.CALCUL_KM_AUTO:
                        transp.calculer_km_auto()
                    if not transp.chauffeur_externe and policy.CALCUL_DUREE_AUTO:
                        transp.calculer_duree_auto()

            elif 'typ' in self.changed_data:
                for trajet in transp.trajets.all():
                    trajet.typ = self.cleaned_data['typ']
                    trajet.save(update_fields=['typ'])
        return transp


class TrajetModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, traj):
        return format_html(
            "{heure}: Trajet pour <b>{client}</b><br><i>de:</i> {origine}<br><i>à:</i> {dest}",
            client=traj.transport.client,
            origine=traj.origine.__html__(sep=', '),
            dest=traj.destination.__html__(sep=', '),
            heure=localtime(traj.heure_depart).strftime('%H:%M')
        )


class TrajetAPlusieursForm(BootstrapMixin, forms.Form):
    trajets = TrajetModelMultipleChoiceField(
        queryset=None, widget=BSCheckboxSelectMultiple, required=False
    )
    max_dist = 1.5  # Km entre deux orig. ou dest. possiblement en commun.

    def __init__(self, trajet=None, **kwargs):
        self.trajet = trajet
        if trajet.commun_id:
            kwargs['initial']['trajets'] = trajet.commun.trajet_set.all()
        super().__init__(**kwargs)
        # Définir les trajets communs possibles
        self.fields['trajets'].queryset = Trajet.objects.exclude(
            Q(transport__pk=self.trajet.transport_id) |
            Q(transport__statut=Transport.StatutChoices.ANNULE)
        ).annotate(
            dist_orig=PtDist(
                F('origine_adr__empl_geo'),
                trajet.origine_adr.empl_geo if trajet.origine_adr else Value([0.0, 0.0])
            ),
            dist_dest=PtDist(
                F('destination_adr__empl_geo'),
                trajet.destination_adr.empl_geo if trajet.destination_adr else Value([0.0, 0.0])
            ),
            diff_temps=F('heure_depart') - trajet.heure_depart,
        ).filter(
            Q(transport__chauffeur=self.trajet.transport.chauffeur) &
            Q(heure_depart__date=self.trajet.heure_depart.date()) & (
                (Q(origine_adr__isnull=False) & Q(dist_orig__lt=self.max_dist)) |
                (Q(destination_adr__isnull=False) & Q(dist_dest__lt=self.max_dist))
            ) &
            Q(diff_temps__range=(timedelta(minutes=-61), timedelta(minutes=61)))
        ).select_related('transport', 'transport__client').order_by('heure_depart')

    def save(self):
        if not self.cleaned_data['trajets']:
            if self.trajet.commun:
                self.trajet.commun.delete()
        else:
            if not self.trajet.commun:
                self.trajet.commun = TrajetCommun.objects.create(
                    date=self.trajet.heure_depart.date(), chauffeur=self.trajet.transport.chauffeur
                )
                self.trajet.save(update_fields=['commun'])
            else:
                self.trajet.commun.trajet_set.exclude(pk=self.trajet.pk).update(commun=None)
            for traj in self.cleaned_data['trajets']:
                traj.commun = self.trajet.commun
                traj.save(update_fields=['commun'])


class TransportModelForm(BootstrapMixin, ModelForm):
    heure_rdv = forms.TimeField(label="Heure du rendez-vous", widget=TimeInput, required=True)
    fin_recurrence = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = TransportModel
        fields = [
            "heure_depart", "heure_rdv", "duree_rdv", "remarques", "suspension_depuis",
            "fin_recurrence", "vehicule",
        ]
        field_classes = {
            'duree_rdv': HMDurationField,
        }
        widgets = {
            'heure_depart': TimeInput,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not Vehicule.objects.exists():
            del self.fields["vehicule"]

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data['heure_rdv'] = datetime.combine(
            self.instance.heure_rdv.date(), cleaned_data['heure_rdv'], tzinfo=localtime(self.instance.heure_rdv).tzinfo
        )
        return cleaned_data


class TransportFilterForm(BootstrapMixin, forms.Form):
    # immediate_submit needed on individual widgets, as the widgets may be outside of the form DOM node
    date = forms.DateField(widget=DateInput(attrs={'class': 'immediate_submit'}), required=False)
    client = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom client…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )
    destination = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Destination…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )
    region = RegionField(
        widget=forms.Select(attrs={'class': 'immediate_submit'}),
        include_autre=False, required=False
    )
    chauffeur = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom chauffeur…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )

    def __init__(self, statut=None, **kwargs):
        self.name = f"ffstatut{statut}"
        super().__init__(**kwargs)
        if statut == Transport.StatutChoices.SAISI:
            del self.fields['chauffeur']
        for fname in self.fields:
            self.fields[fname].widget.attrs['form'] = self.name

    def filter(self, transports):
        if self.cleaned_data['date']:
            transports = transports.filter(date=self.cleaned_data['date'])
        if self.cleaned_data['client']:
            transports = transports.filter(
                Q(client__persona__nom__unaccent__icontains=self.cleaned_data['client']) |
                Q(client__persona__prenom__unaccent__icontains=self.cleaned_data['client'])
            )
        if self.cleaned_data['destination']:
            transports = transports.filter(
                Q(trajets__destination_adr__nom__unaccent__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__rue__unaccent__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__npa__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__localite__unaccent__icontains=self.cleaned_data['destination'])
            )
        if self.cleaned_data.get('region'):
            transports = transports.avec_adresse_client().filter(
                RegionFinder.get_region_filter('adresse_client__npa', self.cleaned_data['region'])
            )
        if self.cleaned_data.get('chauffeur'):
            transports = transports.filter(
                Q(chauffeur__persona__nom__unaccent__icontains=self.cleaned_data['chauffeur'])
            )
        return transports

    def filter_occurrences(self, occurrences):
        def normalize(txt):
            return unicodedata.normalize('NFKD', txt.lower()).encode('ASCII', 'ignore').decode()

        if self.cleaned_data['date'] and occurrences:
            occurrences = [occ for occ in occurrences if occ.date == self.cleaned_data['date']]
        if self.cleaned_data['client'] and occurrences:
            client = occurrences[0].client
            term = normalize(self.cleaned_data['client'])
            if term in normalize(client.nom) or term in normalize(client.prenom):
                pass
            else:
                occurrences = []
        if self.cleaned_data['destination'] and occurrences:
            dest = occurrences[0].destination
            term = normalize(self.cleaned_data['destination'])
            if (
                term in normalize(getattr(dest, 'nom', '')) or term in normalize(dest.rue) or
                term in normalize(dest.npa) or term in normalize(dest.localite)
            ):
                pass
            else:
                occurrences = []
        if self.cleaned_data.get('region') and occurrences:
            client = occurrences[0].client
            if RegionFinder.get_region(client.adresse().npa) == self.cleaned_data.get('region'):
                pass
            else:
                occurrences = []
        # Pas de chauffeur défini pour les occurrences en mémoire
        return occurrences


class RegionFilterForm(BootstrapMixin, forms.Form):
    region = RegionField(
        widget=forms.Select(attrs={'class': 'immediate_submit'}),
        include_autre=False, required=False
    )

    def filter_transports(self, transports):
        if self.cleaned_data.get('region'):
            transports = transports.avec_adresse_client().filter(
                RegionFinder.get_region_filter('adresse_client__npa', self.cleaned_data['region'])
            )
        return transports

    def filter_benevoles(self, benevoles):
        if self.cleaned_data.get('region'):
            benevoles = benevoles.filter(
                RegionFinder.get_region_filter('adresse_active__npa', self.cleaned_data['region'])
            )
        return benevoles


class TransportArchivesFilterForm(TransportFilterForm):
    statut = forms.ChoiceField(
        choices=[('', '-------')] + [
            c for c in Transport.StatutChoices.choices if c[0] >= Transport.StatutChoices.EFFECTUE
        ],
        required=False
    )

    def filter(self, transports):
        transports = super().filter(transports)
        if self.cleaned_data['statut']:
            transports = transports.filter(statut=self.cleaned_data['statut'])
        return transports


class ClientEditForm(NPALocaliteMixin, ClientEditFormBase):
    class Meta(ClientEditFormBase.Meta):
        fields = [
            'no_debiteur', 'type_logement', 'type_logement_info', 'etage', 'nb_pieces',
            'ascenseur', 'code_entree', 'tarif_avs_des', 'fonds_transport', 'handicaps',
            'remarques_int_transport', 'remarques_ext_transport',
        ]

    persona_fields = [
        'nom', 'prenom', 'genre', 'date_naissance', 'case_postale',
        'courriel', 'langues_select', 'langues', 'date_deces',
    ]
    benev_hidden_fields = {'code_entree', 'tarif_avs_des', 'fonds_transport', 'remarques_int_transport'}
    field_order = [
        'nom', 'prenom', 'genre', 'date_naissance', 'rue', 'npalocalite',
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance.pk and (self.instance.age() or 60) >= 65:
            # Afficher tarif_avs_des seulement pour clients de moins de 65 ans.
            self.fields.pop('tarif_avs_des', None)


class PreferenceEditForm(BootstrapMixin, ModelForm):
    typ = forms.ChoiceField(label='', choices=Preference.PREF_TYPE, widget=BSRadioSelect)

    class Meta:
        model = Preference
        fields = ['chauffeur', 'typ', 'remarque']

    def __init__(self, client=None, **kwargs):
        self.client = client
        super().__init__(**kwargs)
        if kwargs['instance'] and kwargs['instance'].pk:
            del self.fields['chauffeur']
            del self.fields['typ']
        else:
            self.fields['chauffeur'].queryset = Benevole.objects.par_domaine(
                'transport', actifs=True
            )

    def clean(self):
        cleaned_data = super().clean()
        # Uniqueness not auto checked because the client field is not in the form.
        if not self.instance.pk and Preference.objects.filter(
            client=self.client, chauffeur=cleaned_data.get('chauffeur')
        ).exists():
            self.add_error(None, "Il existe déjà une préférence ou incompatibilité entre ce client et ce chauffeur")
        return cleaned_data


class TousClientsFilter(ClientFilter):
    label = "Exportation de tous les clients actifs"
    export_only = True

    def filter(self, clients):
        return clients.annotate(
            dernier_transport=Max('transports__date', filter=~Q(
                transports__statut=Transport.StatutChoices.ANNULE
            ))
        )

    def extra_headers(self):
        return ["Dernier transport"]

    def extra_values(self, client):
        return [client.dernier_transport]


class ClientFilterForm(ClientFilterFormBase):
    recherche_deb = True
    filters = {
        'autres_debit': ClientAutresDebiteursFilter(['transp']),
        'exp_complet': TousClientsFilter(),
    }


class ChauffeurFilterForm(BenevoleFilterForm):
    macaron_echu = forms.BooleanField(label="Échéance macaron proche", required=False)
    LIST_CHOICES = (
        ('', '-------'),
        ('date_cours_cond', "Avec date dernier cours de conduite"),
    )

    def filter(self, benevs):
        benevs = super().filter(benevs)
        if self.cleaned_data.get('macaron_echu'):
            benevs = benevs.filter(macaron_valide__endswith__lt=date.today() + timedelta(days=30))
        return benevs


class AdresseFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off'}), required=False)
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off'}), required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )

    def filter(self, adresses):
        if self.cleaned_data['nom']:
            adresses = adresses.filter(nom__unaccent__icontains=self.cleaned_data['nom'])
        if self.cleaned_data['npa_localite']:
            adresses = adresses.filter(
                Q(npa__icontains=self.cleaned_data['npa_localite']) |
                Q(localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            adresses = adresses.filter(tel__nospaces__icontains=self.cleaned_data['tel'].replace(' ', ''))
        return adresses


class AdresseEditForm(BootstrapMixin, NPALocaliteMixin, GeoAddressFormMixin, ModelForm):
    """Édition d'adresses de destination. La géolocalisation est obligatoire."""
    target = forms.CharField(required=False, widget=forms.HiddenInput)
    search = forms.CharField(
        label="Recherche directories",
        widget=forms.TextInput(attrs={'placeholder': 'Nom, rue, code postal, n° tél'}),
        required=False
    )
    rue = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nom de rue'}), required=True)

    class Meta:
        model = Adresse
        fields = ['nom', 'rue', 'npa', 'localite', 'tel', 'remarques', 'validite']
        field_classes = {'validite': DateRangeField}

    field_order = ['search', 'nom', 'npalocalite', 'rue']
    geo_required = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if kwargs['instance']:
            del self.fields['search']

    def geo_enabled(self, instance):
        if self.cleaned_data["validite"].upper and self.cleaned_data["validite"].upper < date.today():
            return False
        return super().geo_enabled(instance)


class CancelForm(BootstrapMixin, ModelForm):
    annule = forms.BooleanField(label="Le transport a été annulé", required=False, initial=True)

    class Meta:
        model = Transport
        fields = [
            "motif_annulation", "raison_annulation", "statut_fact", "defrayer_chauffeur", "km",
        ]

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        if not self.instance.est_annule():
            del self.fields['annule']
        self.fields['statut_fact'].choices = [
            c for c in self.fields['statut_fact'].choices
            if c[0] in fact_policy().TRANSP_ANNULATION_STATUTS
        ]
        for fname in ["motif_annulation", "statut_fact", "defrayer_chauffeur"]:
            self.fields[fname].required = True

    def clean(self):
        data = super().clean()
        if data['defrayer_chauffeur'] and not data.get('km'):
            self.add_error('km', "Vous devez indiquer les kilomètres pour défrayer le chauffeur.")
        if data.get('statut_fact') in {
            Transport.FacturationChoices.FACTURER_ANNUL_KM,
            Transport.FacturationChoices.FACTURER_KM,
        } and not data.get('km'):
            self.add_error('km', "Vous devez indiquer les kilomètres pour facturer les km du chauffeur.")
        if data.get("motif_annulation") == Transport.AnnulationChoices.AUTRE and not data["raison_annulation"]:
            self.add_error("raison_annulation", "Merci de préciser les raisons de l’annulation")
        return data

    def save(self, **kwargs):
        if not self.instance.pk:
            self.instance.concretiser()
        return super().save(**kwargs)


class AttributionForm(BootstrapMixin, ModelForm):
    page_origine = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Transport
        fields = ['chauffeur', 'chauffeur_externe', 'vehicule']
        widgets = {
            'chauffeur': BSRadioSelect,
            'vehicule': BSRadioSelect,
        }

    def save(self, **kwargs):
        if not self.instance.pk:
            # Occurrence de modèle en mémoire.
            self.instance.concretiser()
            transp = self.instance
        else:
            transp = super().save(**kwargs)
        if transp.chauffeur and transp.statut < Transport.StatutChoices.ATTRIBUE:
            transp.statut = Transport.StatutChoices.ATTRIBUE
            transp.calc_chauffeur_dists()
            transp.save()
        elif transp.chauffeur_externe:
            transp.statut = Transport.StatutChoices.CONFIRME
            transp.save(update_fields=['statut'])
        return transp


class ConfirmationForm(forms.Form):
    selection = forms.ModelMultipleChoiceField(queryset=Transport.objects.all())


class FraisTransportForm(BootstrapMixin, ModelForm):
    MAX_FRAIS_REPAS = 20

    class Meta:
        model = Frais
        fields = ['cout', 'justif', 'typ']
        widgets = {
            'typ': forms.HiddenInput,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.instance.typ:
            default_typ = kwargs.get('initial', {}).get('typ', Frais.TypeFrais.PARKING)
            self.fields['typ'].initial = default_typ
            self.instance.typ = default_typ

    @property
    def is_filled(self):
        return self.instance.cout is not None

    def clean(self):
        cleaned_data = super().clean()
        est_repas = cleaned_data.get("typ") == Frais.TypeFrais.REPAS
        if cleaned_data.get('cout') == 0:
            self.add_error(None, "Si vous n’avez pas de frais, laissez plutôt ces champs vides.")
        elif (
            not self.errors and est_repas and cleaned_data.get("cout") > self.MAX_FRAIS_REPAS
        ):
            self.add_error("cout", f"Les frais de repas ne sont remboursés que jusqu’à {self.MAX_FRAIS_REPAS} CHF.")
        if est_repas and not cleaned_data.get("justif"):
            self.add_error(None, "Un justificatif est obligatoire pour les frais de repas.")
        return cleaned_data


class RapportForm(BootstrapMixin, ModelForm):
    annule = forms.BooleanField(label="Le transport n’a pas eu lieu", required=False)

    class Meta:
        model = Transport
        fields = ['km', 'duree_eff', 'temps_attente', 'rapport_chauffeur', 'statut_fact']
        labels = {
            'duree_eff': "Durée effective",
        }
        field_classes = {
            'duree_eff': HMDurationField,
            'temps_attente': HMDurationField,
        }

    def __init__(self, data=None, files=None, user=None, **kwargs):
        super().__init__(data=data, files=files, **kwargs)
        self.by_admin = user.has_perm('transport.change_transport')
        policy = fact_policy()
        self.fields["temps_attente"].help_text = policy.ATTENTE_HELP_TEXT
        if self.by_admin:
            # Masquer seulement si pas de valeur (pour produire le calcul auto à l'enregistrement)
            if policy.CALCUL_KM_AUTO and not self.instance.km and not self.instance.chauffeur_externe:
                del self.fields['km']
            if policy.CALCUL_DUREE_AUTO and not self.instance.duree_eff:
                del self.fields['duree_eff']
            self.fields['statut_fact'].choices = [
                c for c in self.fields['statut_fact'].choices
                if c[0] in policy.TRANSP_FACTURATION_STATUTS
            ]
        else:
            # Pour chauffeurs:
            if policy.CALCUL_KM_AUTO:
                del self.fields['km']
            if policy.CALCUL_DUREE_AUTO:
                del self.fields['duree_eff']
            if not self.instance.retour:
                del self.fields['temps_attente']
            # Les chauffeurs ne doivent pas pouvoir modifier ni voir ce statut.
            del self.fields['statut_fact']

        if self.is_bound and int(data.get('frais-INITIAL_FORMS')) == 0 and self.instance.frais.exists():
            # Détection d'une double soumission de formulaire, suppression frais existants
            self.instance.frais.all().delete()

        extra = 2 - self.instance.frais.all().count()
        initial = [{'typ': Frais.TypeFrais.REPAS}, {'typ': Frais.TypeFrais.PARKING}]
        if not self.instance.retour and extra == 2:
            extra = 1
            initial = initial[1:]
        FraisFormSet = forms.inlineformset_factory(
            Transport, Frais, form=FraisTransportForm, formset=HiddenDeleteInlineFormSet,
            extra=extra,
        )
        self.formset = FraisFormSet(
            instance=self.instance, data=data, files=files, initial=initial[:extra],
        )
        self.trajet_forms = [form for form in [
            TrajetAPlusieursForm(trajet=trajet, prefix=f'traj{idx}', data=data, initial={})
            for idx, trajet in enumerate(self.instance.trajets_tries)
        ] if len(form.fields['trajets'].queryset)]
        self.has_trajets_communs = any(form.initial for form in self.trajet_forms)

    def clean(self):
        data = super().clean()
        policy = fact_policy()
        if 'duree_eff' not in self.fields and self.instance.chauffeur and not self.instance.chauff_aller_duree:
            self.instance.calc_chauffeur_dists(save=True)
            if not self.instance.chauff_aller_duree:
                self.add_error(None, (
                    "La durée du transport du domicile du chauffeur jusqu’au départ "
                    "n’a pas encore pu être calculée. Veuillez réessayer plus tard."
                ))
        if data['annule']:
            if not data.get('rapport_chauffeur'):
                self.add_error('rapport_chauffeur', "Vous devez expliquer les circonstances de l’annulation.")
            if data.get("temps_attente"):
                self.add_error(
                    "temps_attente",
                    "Vous ne pouvez pas indiquer de temps d’attente pour un transport annulé."
                )
        else:
            if not self.errors and 'duree_eff' in self.fields and not data.get('duree_eff'):
                self.add_error('duree_eff', "Vous devez saisir la durée effective.")
            if 'km' in self.fields and data.get('km') == '':
                self.add_error('km', "Vous devez saisir le nombre de kilomètres.")
        temps_attente = data.get('temps_attente') or timedelta()
        if (
            policy.TEMPS_ATTENTE_OBLIGATOIRE and self.instance.retour and
            data.get("temps_attente") is None and not data.get("annule") and
            not "temps_attente" in self.errors
        ):
            self.add_error("temps_attente", "Vous devez saisir un temps d’attente")
        if temps_attente > timedelta(hours=3) and not data.get("rapport_chauffeur"):
            self.add_error(
                "rapport_chauffeur",
                "Pour un temps d’attente de plus de 3 heures, merci d’indiquer un commentaire."
            )
        if (
            temps_attente and 'duree_eff' in self.fields and
            temps_attente >= data.get('duree_eff', timedelta())
        ):
            self.add_error(
                'duree_eff',
                "La durée effective doit être plus élevée que le temps d’attente."
            )
        return data

    def clean_km(self):
        km = self.cleaned_data.get('km')
        if km and km < 0:
            raise forms.ValidationError("La saisie de km négatifs n’est pas autorisée.")
        if km and km > 650:
            raise forms.ValidationError("La saisie de plus de 650km n’est pas autorisée.")
        return km

    def clean_duree_eff(self):
        duree = self.cleaned_data.get('duree_eff')
        if duree and duree > timedelta(hours=20):
            raise forms.ValidationError("La durée effective ne peut pas dépasser 20 heures.")
        return duree

    def clean_temps_attente(self):
        duree = self.cleaned_data.get('temps_attente')
        if duree and duree > timedelta(hours=15):
            raise forms.ValidationError("Le temps d'attente ne peut pas dépasser 15 heures.")
        return duree

    def is_valid(self):
        validated = [super().is_valid()]
        if self.formset is not None:
            validated.append(self.formset.is_valid())
        validated.extend(form.is_valid() for form in self.trajet_forms)
        return all(validated)

    def save(self, **kwargs):
        transport = super().save(**kwargs)
        policy = fact_policy()
        if self.cleaned_data['annule'] is True:
            transport.annuler()
            # Si le transport est annulé par les admins, on estime que le chauffeur ne s'est pas déplacé.
            if self.by_admin:
                return transport
        if not transport.km and policy.CALCUL_KM_AUTO:
            transport.calculer_km_auto()
        if not transport.duree_eff and not transport.chauffeur_externe and policy.CALCUL_DUREE_AUTO:
            transport.calculer_duree_auto()
        if transport.est_annule():
            return transport

        if not transport.retour and not transport.temps_attente and policy.TEMPS_ATTENTE_ALLER:
            transport.temps_attente = policy.TEMPS_ATTENTE_ALLER
            transport.save(update_fields=["temps_attente"])
        if transport.rapport_complet:
            transport.statut = Transport.StatutChoices.RAPPORTE
            if transport.rapport_conforme:
                transport.statut = Transport.StatutChoices.CONTROLE
            transport.save(update_fields=['statut'])
        if self.formset is not None:
            if not self.formset.instance.pk:
                self.formset.instance = transport
            self.formset.save(**kwargs)
        for form in self.trajet_forms:
            form.save()
        return transport


class FacturerMoisForm(BootstrapMixin, forms.Form):
    date_facture = forms.DateField(label="Date de la facturation", widget=DateInput, required=True)


class RefacturerForm(BootstrapMixin, forms.Form):
    date_facture = forms.DateField(label="Date de facture", widget=DateInput, required=True)
    raison = forms.CharField(
        label="Raison de l’annulation/correction de la facture", widget=forms.Textarea,
        required=True
    )

    def __init__(self, facture=None, **kwargs):
        super().__init__(**kwargs)
        if facture.exporte is None:
            # L'ancienne facture sera effacée, pas besoin de la raison.
            del self.fields['raison']


class PermissiveTimeField(PermissiveHourValue, forms.TimeField):
    input_formats = ['%H:%M', '%H', '%H.%M']

    def to_python(self, value):
        value = value.strip()
        value = self._convert_value(value, "Saisissez une heure valide au format hh:mm")
        return super().to_python(value)


class DispoEditForm(BootstrapMixin, RepetitionMixin, ModelForm):
    date = forms.DateField(label="Date", widget=DateInput)
    # person = forms.ModelChoiceField(label="Infirmière", queryset=Utilisateur.objects.none(), required=True)
    heure_de = PermissiveTimeField(
        label="De",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    heure_a = PermissiveTimeField(
        label="À",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )
    description = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Indication facultative sur l’absence'}),
        required=False
    )

    class Meta:
        model = Dispo
        fields = [
            'date', 'heure_de', 'heure_a', 'repetition', 'repetition_fin', 'description',
        ]

    def __init__(self, chauffeur=None, categ=None, **kwargs):
        self.chauffeur = chauffeur
        self.categ = categ
        super().__init__(**kwargs)
        if kwargs['instance']:
            self.fields['date'].widget = forms.HiddenInput()
        if categ == "absence":
            self.fields["description"].widget.attrs["placeholder"] = "Indication facultative sur l’absence"
        else:
            self.fields["description"].widget.attrs["placeholder"] = "Indication facultative sur la dispo"

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors and not (
            self.instance.pk and 'repetition_fin' in self.changed_data and cleaned_data.get('repetition_fin')
        ):
            # Check overlap (only in the future, and avoid check when setting the end)
            start_date = cleaned_data.get('date')
            if self.instance.pk:
                next_occ = self.instance.get_next_occurrence()
                start_date = localtime(next_occ.debut).date() if next_occ else None
            if start_date:
                start = make_datetime(start_date, cleaned_data.get('heure_de'))
                end = make_datetime(start_date, cleaned_data.get('heure_a'))
                overlap = Dispo.get_for_person(
                    self.chauffeur, start + timedelta(seconds=1), end,
                    exclude=[self.instance] if self.instance.pk else [],
                    categ=self.categ,
                )
                if any(occ.cancelled is False for occ in overlap):
                    self.add_error(None, forms.ValidationError(
                        "Cette disponibilité se chevauche avec une autre déjà existante."
                    ))
        if "heure_a" in cleaned_data and "heure_de" in cleaned_data:
            duree = (
                datetime.combine(date.today(), cleaned_data["heure_a"]) -
                datetime.combine(date.today(), cleaned_data["heure_de"])
            )
            if duree < timedelta(minutes=15):
                self.add_error("heure_de", "Impossible de saisir une disponibilité de moins de 15 minutes")
        if self.categ == "absence" and not self.errors:
            # Contrôle qu'il n'y ait pas de transport confirmé
            start_date = cleaned_data.get("date")
            start = make_datetime(start_date, cleaned_data.get("heure_de"))
            end = make_datetime(start_date, cleaned_data.get("heure_a"))
            overlaps = []
            for transp in self.chauffeur.transports.filter(date=start_date):
                if transp.heure_depart < end and transp.heure_arrivee > start:
                    if transp.statut == Transport.StatutChoices.CONFIRME:
                        overlaps.append(transp)
                    elif transp.statut == Transport.StatutChoices.ATTRIBUE:
                        transp.statut = Transport.StatutChoices.SAISI
                        transp.chauffeur = None
                        transp.save(update_fields=["statut", "chauffeur"])
                        JournalTransport.objects.create(
                            transport=transp,
                            description="Le chauffeur a ajouté une absence dans la période de ce transport",
                            quand=now(), qui=self.chauffeur.utilisateur,
                        )
            if overlaps:
                self.add_error(None, (
                    "Au moins un transport vous a été attribué durant cette absence. "
                    "Veuillez appeler le bureau des transports pour leur signaler votre absence."
                ))
        return cleaned_data

    def save(self, **kwargs):
        if not self.instance.chauffeur_id:
            self.instance.chauffeur = self.chauffeur
        if set(self.changed_data).intersection({'date', 'heure_de', 'heure_a'}):
            self.instance.debut = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_de'])
            self.instance.fin = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_a'])
            if self.instance.pk:
                for occ in self.instance.exceptions.all():
                    occ.orig_start = make_datetime(occ.orig_start, self.cleaned_data['heure_de'])
                    occ.save()
        self.set_repetition(self.instance)
        return super().save(**kwargs)


class VehiculeForm(BootstrapMixin, NPALocaliteMixin, ModelForm):
    class Meta:
        model = Vehicule
        fields = ["modele", "no_plaque", "chaises_ok", "responsable", "rue", "npa", "localite"]


class VehiculeOccupEditForm(BootstrapMixin, ModelForm):
    class Meta:
        model = VehiculeOccup
        fields = ['duree', 'description']
        field_classes = {
            'duree': DateTimeRangeField,
        }

    def __init__(self, vehicule=None, **kwargs):
        self.vehicule = vehicule
        super().__init__(**kwargs)

    def clean(self):
        cleaned_data = super().clean()
        duree = cleaned_data.get("duree")
        if duree and duree.lower == duree.upper or duree.lower > duree.upper:
            raise forms.ValidationError("«Jusqu’au» doit être plus grand que «Depuis le»")
        return cleaned_data

    def save(self, **kwargs):
        if not self.instance.vehicule_id:
            self.instance.vehicule = self.vehicule
        return super().save(**kwargs)


class VehiculeReservEditForm(VehiculeOccupEditForm):
    client = forms.ModelChoiceField(queryset=Client.objects.all(), widget=forms.HiddenInput)
    client_select = forms.CharField(widget=forms.TextInput(attrs={
        "class": "autocomplete",
        "placeholder": "Client…",
        "autocomplete": "off",
        "data-searchurl": reverse_lazy("client-autocomplete", args=["tous"]),
        "data-clienturl": reverse_lazy("client-details-transport"),
        "data-pkfield": "client",
    }))

    class Meta(VehiculeOccupEditForm.Meta):
        fields = ["client", "duree", "kms", "description"]


class DateRangeSelectionForm(forms.Form):
    date_de = forms.DateField(label="Du", widget=DateInput, required=True)
    date_a = forms.DateField(label="Au", widget=DateInput, required=True)


def make_datetime(dt, hour, timezone=None):
    return make_aware(datetime(dt.year, dt.month, dt.day, hour.hour, hour.minute), timezone=timezone)
