from collections import Counter, defaultdict
from copy import copy
from datetime import date, datetime, time, timedelta, timezone as tz
from decimal import Decimal
from itertools import chain
from operator import attrgetter

from dateutil import rrule
from stdnum.ch import esr

from django.conf import settings
from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import (
    ArrayField, DateRangeField, DateTimeRangeField, RangeOperators
)
from django.core.cache import cache
from django.db import models
from django.db.backends.postgresql.psycopg_any import DateTimeRange
from django.db.models import Count, F, Max, Min, OuterRef, Prefetch, Q, Sum
from django.db.models.functions import TruncMonth
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import format_html

from client.models import Client, PersonaBaseQuerySet, Referent
from benevole.models import Benevole, CalculateurFraisBase, LigneFrais, TypeActivite, TypeFrais
from common.distance import ORSUnavailable, distance_real, distance_vo
from common.fields import PhoneNumberField
from common.functions import IsOFASTransport
from common.models import CachedDistCalculator, GeolocMixin, Utilisateur
from common.utils import NoAPIException, canton_abrev, canton_app, fact_policy, get_erp_api

RRULE_FREQ_MAP = {
    "YEARLY": rrule.YEARLY,
    "MONTHLY": rrule.MONTHLY,
    "WEEKLY": rrule.WEEKLY,
    "DAILY": rrule.DAILY,
    "HOURLY": rrule.HOURLY,
    "MINUTELY": rrule.MINUTELY,
    "SECONDLY": rrule.SECONDLY,
}
RRULE_WDAY_MAP = {'MO': 0, 'TU': 1, 'WE': 2, 'TH': 3, 'FR': 4, 'SA': 5, 'SU': 6}


class Regle(models.Model):
    # https://tools.ietf.org/html/rfc5545
    # See also https://dateutil.readthedocs.io/en/stable/rrule.html
    FREQ_CHOICES = (
        ("YEARLY", "Chaque année"),
        ("MONTHLY", "Chaque mois"),
        ("WEEKLY", "Chaque semaine"),
        ("DAILY", "Chaque jour"),
        ("HOURLY", "Chaque heure"),
        ("MINUTELY", "Chaque minute"),
        ("SECONDLY", "Chaque seconde"),
    )
    nom = models.CharField("Nom", max_length=40)
    description = models.TextField("Description", blank=True)
    frequence = models.CharField("Fréquence", choices=FREQ_CHOICES, max_length=10)
    params = models.TextField("Params", blank=True)

    class Meta:
        verbose_name = "Règle"
        verbose_name_plural = "Règles"

    def __str__(self):
        return "Règle %s - params %s" % (self.nom, self.params)


class OccurrenceExistError(Exception):
    pass


class OccurrenceMixin:
    def get_occurrences(self, start, end=None):
        """
        Return list of date occurrences of this event between start and end
        (including overlaps).
        """
        start_rule = self.get_rrule()
        # _exceptions used in .memory_instance()
        self._exceptions = self.get_exceptions()
        initial_offset = timezone.localtime(self.debut).utcoffset()
        # Adjust start/end to consider timezone change from initial event timezone
        start = fix_occurrence_date(start, initial_offset, reverse=True)
        if end:
            fix_occurrence_date(end, initial_offset, reverse=True)
        # FIXME: limit results if end is None and event has no end.
        if start_rule:
            # Check if previous occurrence is overlapping `start`
            prev_occ = start_rule.before(start)
            if prev_occ:
                prev_occ = fix_occurrence_date(prev_occ, initial_offset)
                if prev_occ + (self.fin - self.debut) > start:
                    yield self.memory_instance(prev_occ)
            for occ_start in start_rule.xafter(start, inc=True):
                occ_start = fix_occurrence_date(occ_start, initial_offset)
                if end and occ_start >= end:
                    break
                yield self.memory_instance(occ_start)
        elif (end is None or self.debut < end) and self.fin > start:
            yield self

    def get_next_occurrence(self, _from=None):
        return next(self.get_occurrences(_from or timezone.now()), None)

    def get_exceptions(self):
        return {}

    def memory_instance(self, start):
        raise NotImplementedError

    def get_rrule(self):
        if self.regle is None:
            return
        frequency = RRULE_FREQ_MAP[self.regle.frequence]

        def convert_param(key, value):
            if key == 'byweekday':
                return key, [RRULE_WDAY_MAP[v] for v in value.split(',')]
            elif key == 'interval':
                return key, int(value)
            else:
                return key, value

        if self.regle.params:
            params = {key: val for key, val in [
                convert_param(*(param.split(':'))) for param in self.regle.params.split(';')
            ]}
        else:
            params = {}
        return rrule.rrule(frequency, dtstart=self.debut, until=self.fin_recurrence, **params)


class AdresseManager(models.Manager):
    def actives(self):
        # Aussi inclure les adresses 'futures'
        return self.filter(Q(validite__contains=date.today()) | Q(validite__startswith__gt=date.today()))


class Adresse(GeolocMixin, models.Model):
    nom = models.CharField("Nom", max_length=120)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    tel = PhoneNumberField("Téléphone", blank=True)
    remarques = models.TextField(blank=True)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    validite = DateRangeField("Validité")

    est_adresse_client = False

    objects = AdresseManager()

    class Meta:
        constraints = [
            # Pas deux mêmes noms avec une validité en parallèle.
            ExclusionConstraint(
                name="nom_validite_overlap",
                expressions=[
                    ("nom", RangeOperators.EQUAL),
                    ("validite", RangeOperators.OVERLAPS),
                ],
                violation_error_message="Il existe déjà une adresse pour ce nom dans la même plage de validité.",
            ),
        ]

    def __str__(self):
        return f"{self.nom}, {self.rue or '-'}, {self.npa} {self.localite}"

    @property
    def validite_suffixe(self):
        if self.validite.lower > date.today():
            return f" (dès le {self.validite.lower.strftime('%d.%m.%Y')})"
        if self.validite.upper is not None:
            return f" (jusqu’au {self.validite.upper.strftime('%d.%m.%Y')})"
        return ""

    def str_avec_validite(self):
        return "".join([str(self), self.validite_suffixe])

    def adresse(self):
        if self.rue:
            return f"{self.rue}, {self.npa} {self.localite}"
        elif self.localite:
            return f"{self.npa} {self.localite}"
        return ""

    def __eq__(self, other):
        if not hasattr(other, 'rue'):
            return False
        return (self.rue == other.rue and self.empl_geo == other.empl_geo)

    def __hash__(self):
        return hash(self.rue + str(self.empl_geo))

    def __html__(self, sep='<br>'):
        return format_html(
            '{}{}%s<span class="adresse">{}, {} {}</span>' % sep,
            self.nom, self.validite_suffixe, self.rue or "-", self.npa, self.localite
        )


class VehiculeManager(models.Manager):
    def actifs(self):
        return self.get_queryset().filter(archive_le__isnull=True)


class Vehicule(GeolocMixin, models.Model):
    modele = models.CharField("Marque/Modèle", max_length=60)
    no_plaque = models.CharField("N° de plaque", max_length=10, blank=True)
    chaises_ok = models.BooleanField("Accepte chaises roulantes", default=False)
    responsable = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, null=True, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    archive_le = models.DateField("Date d’archivage", blank=True, null=True)

    objects = VehiculeManager()

    def __str__(self):
        return f"{self.modele} {self.no_plaque}"

    def get_all_events(self, start, end):
        """
        Renvoie tous les événements (transports, indisponibilités) liés à ce
        véhicule entre `start` et `end`.
        """
        indispos = self.indisponibilites.filter(
            duree__overlap=DateTimeRange(start, end)
        ).order_by("duree__startswith")
        transports = self.transports.filter(
            date__gte=start.date(), date__lte=end.date()
        ).exclude(statut=Transport.StatutChoices.ANNULE).order_by("heure_rdv")
        # Ajout occurrences depuis modèles de transports
        occs = TransportModel.toutes_occurrences(
            vehicule=self, _date=(start.date(), end.date()), prefetch_trajets=True
        )
        return list(transports) + occs + list(indispos)


class VehiculeOccup(models.Model):
    vehicule = models.ForeignKey(Vehicule, on_delete=models.CASCADE, related_name='indisponibilites')
    duree = DateTimeRangeField("Durée d’indisponibilité")
    description = models.TextField("Description")
    # Champs pour réservation autonome du véhicule
    kms = models.IntegerField("Kilomètres effectués", blank=True, null=True)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, blank=True, null=True)
    facture = models.ForeignKey("Facture", on_delete=models.SET_NULL, blank=True, null=True)

    ev_category = 'nodispo'

    def __str__(self):
        return f"{self.vehicule}: indisponibilité de {self.duree.lower} à {self.duree.upper}"

    def can_edit(self, user):
        return user.has_perm('transport.change_vehiculeoccup')

    def edit_url(self):
        return reverse('vehicule-indispo-edit', args=[self.pk])

    @property
    def debut(self):
        return timezone.localtime(self.duree.lower) if self.duree else None

    @property
    def fin(self):
        return timezone.localtime(self.duree.upper) if self.duree else None

    def spans(self):
        """Return number of 15min slices it occupies."""
        return round((self.fin - self.debut).seconds / 60 / 15, 1)

    def match(self, referent):
        return 1 if 'transp' in referent.facturation_pour else 0

    def donnees_facturation(self):
        fpolicy = fact_policy()
        cout_km = fpolicy.cout_km(self, self.kms)
        return {
            "no": self.pk,
            "type_fact": "reservation",
            "km": self.kms,
            "tarif_km": fpolicy.tarif_km(self),
            "cout_km": cout_km,
            "total": cout_km,
        }


class TransportModelQuerySet(models.QuerySet):
    def prefetch_first_trajets(self):
        """Prefetch first trajets for each TransportModel and cache them in .trajets_tries"""
        query = self.annotate(first_transp=Min('transports__pk'))
        trajets = list(Trajet.objects.filter(
            transport__in=query.values_list('first_transp', flat=True)
        ).select_related(
            'transport', 'transport__client', 'origine_adr', 'destination_adr'
        ).order_by('heure_depart'))
        trajets_par_model = defaultdict(list)
        for tr in trajets:
            trajets_par_model[tr.transport.modele_id].append(tr)
        for mod in query:
            mod.trajets_tries = trajets_par_model[mod.pk]
        return query


class TransportModel(OccurrenceMixin, models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='models')
    heure_depart = models.TimeField("Heure de départ", null=True)
    heure_rdv = models.DateTimeField("Heure de rendez-vous")
    duree_rdv = models.DurationField("Durée du rendez-vous", null=True, blank=True)
    retour = models.BooleanField(default=True)
    vehicule = models.ForeignKey(
        Vehicule, on_delete=models.PROTECT, blank=True, null=True,
    )
    remarques = models.TextField("Remarques", blank=True)
    regle = models.ForeignKey(
        Regle, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    suspension_depuis = models.DateField("Suspension dès le", null=True, blank=True)
    fin_recurrence = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    objects = TransportModelQuerySet.as_manager()

    def __str__(self):
        return f"Modèle de transport pour {self.client} ({self.regle}, dès {self.heure_rdv.date()})"

    def _check_trajets(self):
        """
        Populer self.trajets_tries avec les trajets de la première occurrence
        concrète (ou liste vide, s'il n'y en a pas encore).
        """
        if not hasattr(self, 'trajets_tries'):
            first = self.transports.order_by('heure_rdv').first()
            self.trajets_tries = first.trajets_tries if first else []

    @classmethod
    def toutes_occurrences(
        cls, client=None, vehicule=None, _date=None, filter_func=None, prefetch_trajets=True, for_n_days=60,
    ):
        """
        Renvoyer toutes les occurrences de modèles pour les `for_n_days` (déf: 60) prochains jours.
        """
        modeles = cls.objects.filter(
            Q(fin_recurrence__isnull=True) | Q(fin_recurrence__gt=timezone.now())
        ).select_related('client', 'regle').prefetch_related(
            # prefetch (future) real instances
            Prefetch(
                'transports',
                queryset=Transport.objects.filter(date__gte=date.today()),
                to_attr='futurs_transports'
            )
        )
        if client is not None:
            modeles = modeles.filter(client=client)
        if vehicule is not None:
            modeles = modeles.filter(vehicule=vehicule)
        # As late as possible as it evaluates the query
        if prefetch_trajets:
            modeles = modeles.prefetch_first_trajets()

        modele_transports = []
        if isinstance(_date, tuple):
            date_from, date_to = _date
        elif _date:
            date_from, date_to = _date, _date + timedelta(days=1)
        else:
            date_from, date_to = date.today(), date.today() + timedelta(days=for_n_days)
        for modele in modeles:
            # Générer futures occurrences
            _from = datetime.combine(date_from, time(0, 0), tzinfo=timezone.get_current_timezone())
            _to = datetime.combine(date_to, time(23, 59), tzinfo=timezone.get_current_timezone())
            occs = modele.generer_de_a(_from, _to)
            if filter_func:
                occs = filter_func(occs)
            modele_transports.extend(occs)
        return modele_transports

    @property
    def debut(self):
        # for OccurrenceMixin
        return self.heure_rdv

    @property
    def fin(self):
        self._check_trajets()
        return self.trajets_tries[-1].heure_arrivee

    def memory_instance(self, occ_start):
        return Transport(
            client=self.client, date=occ_start.date(),
            heure_rdv=timezone.localtime(occ_start), duree_rdv=self.duree_rdv,
            modele=self, retour=self.retour, vehicule=self.vehicule,
            remarques=self.remarques, statut=Transport.StatutChoices.SAISI
        )

    def get_occurrence(self, day):
        start = datetime.combine(day, time(0, 0), tzinfo=timezone.get_current_timezone())
        end = datetime.combine(day, time(23, 59), tzinfo=timezone.get_current_timezone())
        return next(self.get_occurrences(start=start, end=end), None)

    def generer_de_a(self, debut, fin):
        """Renvoyer les occurrences futures jusqu'à `fin`, en excluant les occurrences concrétisées."""
        existing_dates = {tr.date for tr in getattr(self, 'futurs_transports', self.transports.all())}
        if self.suspension_depuis and (
            not self.fin_recurrence or self.fin_recurrence.date() > self.suspension_depuis
        ):
            # Temporary (in memory) set fin_recurrence to suspension_depuis
            dt = self.suspension_depuis
            self.fin_recurrence = timezone.make_aware(datetime(dt.year, dt.month, dt.day, 0, 0))
        return [
            occ for occ in self.get_occurrences(debut, end=fin)
            if occ.date not in existing_dates
        ]


class TransportQuerySet(models.QuerySet):
    def avec_is_ofas(self):
        return self.annotate(
            is_ofas=IsOFASTransport(
                F('id'), F('date'), F('client__tarif_avs_des'),
                F('client__persona__date_naissance'), F('client__persona__genre')
            ),
        )

    def avec_adresse_client(self):
        return self.annotate(
            adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                OuterRef('date'), outer_field_name='client__persona_id'
            )
        )

    def avec_adresse_chauffeur(self):
        return self.annotate(
            adresse_chauffeur=PersonaBaseQuerySet.adresse_active_subquery(
                OuterRef('date'), outer_field_name='chauffeur__persona_id'
            )
        )


class FacturationPolicyBase:
    MIN_KM_FACTURES = Decimal('4.0')
    CALCUL_KM_AUTO = False
    CALCUL_DUREE_AUTO = False
    # Nbre d'heures avant passage auto de effectué en contrôlé
    TRANSPORT_RAPPORTE_AUTO_HEURES = None
    # Temps d'attente compté automatiquement pour les transports aller
    TEMPS_ATTENTE_ALLER = None
    TEMPS_ATTENTE_OBLIGATOIRE = False

    def tarif_km(self, transport):
        raise NotImplementedError

    def kms_a_facturer(self, transport):
        if transport.est_annule():
            if transport.statut_fact in {
                Transport.FacturationChoices.FACTURER_ANNUL_KM, Transport.FacturationChoices.FACTURER_KM
            }:
                kms_a_facturer = transport.km
            else:
                kms_a_facturer = Decimal(0)
        else:
            kms_a_facturer = max(transport.km_calc(), self.MIN_KM_FACTURES)
        return kms_a_facturer

    def cout_km(self, transport, kms):
        if kms is None:
            raise ValueError(f"Unable to compute cout_km when kms is None (for {transport})")
        return round(kms * self.tarif_km(transport), 2)

    def cout_attente(self, transport):
        raise NotImplementedError

    def cout_forfait(self, transport):
        raise NotImplementedError

    def cout_annulation(self, transport):
        raise NotImplementedError

    def calculer_frais(self, benevole, data, mois):
        raise NotImplementedError

    def export_frais_chauffeurs(self, liste_notes, un_du_mois):
        raise NotImplementedError

    def montant_total(self, fact_data):
        return None


class Transport(models.Model):
    class StatutChoices(models.IntegerChoices):
        SAISI = 1, "Saisi"
        ATTRIBUE = 2, "Attribué"
        CONFIRME = 3, "Confirmé"
        EFFECTUE = 4, "Effectué"
        RAPPORTE = 5, "Rapporté"
        CONTROLE = 6, "Contrôlé"
        ANNULE = 10, "Annulé"

    class FacturationChoices(models.IntegerChoices):
        FACTURER = 1, "Facturer le transport"
        FACTURER_ANNUL = 2, "Facturer frais d’annulation"
        FACTURER_ANNUL_KM = 3, "Facturer frais d’annulation et km chauffeur"
        FACTURER_KM = 4, "Facturer les km du chauffeur"
        PAS_FACTURER = 99, "Ne rien facturer"

    class AnnulationChoices(models.TextChoices):
        HORS_DELAI = "horsdelai", "demande hors délai"
        AUCUN_CHAUFFEUR = "pas_chauff", "pas de chauffeur disponible"
        CLIENT_ANNUL = "client_ann", "annulation par le client (à l’avance)"
        CLIENT_ABS = "client_abs", "annulé sur place (absence, refus, etc.)"
        AUTRE = "autre", "autre motif"

    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='transports')
    modele = models.ForeignKey(
        TransportModel, on_delete=models.SET_NULL, null=True, blank=True, related_name='transports'
    )
    chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.PROTECT, related_name='transports'
    )
    chauffeur_externe = models.BooleanField(default=False)
    chauff_aller_dist = models.PositiveIntegerField("Distance chauffeur-départ [m]", blank=True, null=True)
    chauff_retour_dist = models.PositiveIntegerField("Distance arrivée-chauffeur [m]", blank=True, null=True)
    chauff_aller_duree = models.DurationField("Durée chauffeur-départ", blank=True, null=True)
    chauff_retour_duree = models.DurationField("Durée arrivée-chauffeur", blank=True, null=True)
    chauffeur_vu = models.BooleanField("Vu par chauffeur", default=False)
    vehicule = models.ForeignKey(
        Vehicule, on_delete=models.PROTECT, blank=True, null=True, related_name='transports'
    )
    date = models.DateField("Date du transport")
    heure_rdv = models.DateTimeField("Heure de rendez-vous")
    duree_rdv = models.DurationField("Durée du rendez-vous", null=True, blank=True)
    retour = models.BooleanField(default=True)
    duree_eff = models.DurationField("Durée effective du trajet", null=True, blank=True)
    # Total km, y.c. trajets depuis et vers domicile chauffeur.
    km = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)
    statut = models.PositiveSmallIntegerField(choices=StatutChoices, default=StatutChoices.SAISI)
    temps_attente = models.DurationField("Temps d’attente", null=True, blank=True)
    remarques = models.TextField("Remarques", blank=True)
    rapport_chauffeur = models.TextField("Commentaires du chauffeur", blank=True)
    date_annulation = models.DateTimeField("Date d’annulation", blank=True, null=True)
    motif_annulation = models.CharField(
        "Motif d’annulation", max_length=10, choices=AnnulationChoices, blank=True
    )
    raison_annulation = models.TextField("Raison de l’annulation", blank=True)
    statut_fact = models.PositiveSmallIntegerField(
        "Statut de facturation", choices=FacturationChoices, default=FacturationChoices.FACTURER
    )
    defrayer_chauffeur = models.BooleanField("Défrayer les km du chauffeur", null=True)
    facture = models.ForeignKey("Facture", on_delete=models.SET_NULL, null=True, blank=True)

    ev_category = 'transport'
    aller_ret_map = {
        True: {'label': "Aller-retour"},
        False: {'label': "Aller simple"}
    }

    objects = TransportQuerySet.as_manager()

    class Meta:
        indexes = [
            models.Index(fields=['statut'], name='index_statut'),
            models.Index(fields=['heure_rdv'], name='index_heure_rdv'),
        ]
        permissions = [
            ("gestion_finances", "Autorisé à gérer finances et facturation"),
        ]

    def __str__(self):
        return f"Transport pour {self.client} le {self.date.strftime('%d.%m.%Y')}"

    def __lt__(self, other):
        # Certaines vues (TransportByStatusView) annotent les transports avec `depart`.
        return getattr(self, 'depart', self.heure_rdv) < getattr(other, 'depart', other.heure_rdv)

    @classmethod
    def check_effectues(self, chauffeur=None):
        """
        Vérifier si certains transports confirmés doivent être passés en effectués,
        ou transports effectués en transports contrôlés.
        """
        statuts = Transport.StatutChoices
        averifier = Transport.objects.filter(statut=statuts.CONFIRME, date__lte=date.today())
        if chauffeur is not None:
            averifier = averifier.filter(chauffeur=chauffeur)
        for transp in averifier:
            if transp.est_passe:
                transp.statut = statuts.EFFECTUE
                transp.save(update_fields=["statut"])
        policy = fact_policy()
        if heures := policy.TRANSPORT_RAPPORTE_AUTO_HEURES:
            # Marquer les transports effectués depuis plus de x heures comme contrôlés.
            averifier = Transport.objects.filter(
                chauffeur__isnull=False,
                statut=statuts.EFFECTUE,
                heure_rdv__lte=timezone.now() - timedelta(hours=heures)
            )
            for transp in averifier:
                transp.calculer_km_auto()
                transp.calculer_duree_auto()
                transp.statut = statuts.CONTROLE
                transp.temps_attente = transp.duree_rdv or policy.TEMPS_ATTENTE_ALLER
                transp.save(update_fields=["statut", "temps_attente"])

    @property
    def has_trajets(self):
        return bool(self.trajets_tries)

    @property
    def has_trajet_commun(self):
        if not self.pk:
            return False
        return any(tr.commun_id for tr in self.trajets_tries)

    @cached_property
    def trajets_tries(self):
        if self.pk:
            return sorted(list(self.trajets.all()), key=attrgetter('heure_depart'))
        if self.modele:
            # Création des trajets en mémoire (par copie partielle)
            self.modele._check_trajets()
            trajets_tries = []
            for idx, traj in enumerate(self.modele.trajets_tries):
                trajet = copy(traj)
                trajet.transport = self
                trajet.commun = None
                trajet.to_date(self.heure_rdv)
                if idx == 0 and self.modele.heure_depart:
                    trajet.heure_depart = datetime.combine(
                        self.date, self.modele.heure_depart, tzinfo=timezone.get_current_timezone()
                    )
                trajets_tries.append(trajet)
            return trajets_tries
        return []

    def concretiser(self):
        if self.modele.transports.filter(date=self.date).exists():
            raise OccurrenceExistError()
        trajets = self.trajets_tries
        self.save()
        for traj in trajets:
            traj.pk = None
            traj._state.adding = True
            traj.transport = self
            traj.commun = None
            traj.dist_calc = None
            traj.duree_calc = None
            traj.calc_trajet(save=False)
            traj.save()

    def annuler(self, raison='', statut_fact=None):
        self.statut = Transport.StatutChoices.ANNULE
        self.date_annulation = timezone.now()
        if raison:
            self.raison_annulation = raison
        if statut_fact:
            self.statut_fact = statut_fact
        self.save(update_fields=['statut', 'date_annulation', 'raison_annulation', 'statut_fact'])
        # Enlever des trajets communs
        communs = list(TrajetCommun.objects.filter(trajet__in=self.trajets.all()))
        self.trajets.filter(commun__isnull=False).update(commun=None)
        # Delete TrajetCommun if only one trajet remaining
        for commun in communs:
            if commun.trajet_set.count() < 2:
                commun.delete()

    def est_annule(self):
        return self.statut == Transport.StatutChoices.ANNULE

    @property
    def duree_calc(self):
        """
        Renvoie la durée totale estimée du transport pour le chauffeur.
        Temps de trajet doublé pour compter le retour en cas d'aller simple.
        """
        duree = sum(
            [traj.get_duree_calc(raise_ors=True) for traj in self.trajets_tries],
            timedelta()
        )
        if self.retour is False:
            duree += duree
        else:
            duree += self.temps_attente if self.temps_attente is not None else self.duree_rdv
        return duree

    @property
    def duree_calc_chauffeur(self):
        transp_duree = self.duree_calc if not self.est_annule() else timedelta(0)
        # Ajout marge de 10min. pour prise en charge client
        marge = timedelta(seconds=10 * 60) if transp_duree else timedelta(0)
        return (
            (self.chauff_aller_duree or timedelta()) + transp_duree +
            (self.chauff_retour_duree or timedelta()) + marge
        )

    @property
    def duree_retour(self):
        """Durée du trajet retour, seulement pour un aller-retour."""
        duree = timedelta(0)
        if self.retour is False:
            return duree
        retour = False
        for traj in self.trajets_tries:
            if retour:
                duree +=  traj.get_duree_calc(raise_ors=True)
            if traj.destination_princ:
                retour = True
        return duree

    @property
    def dist_calc(self):
        return sum(traj.get_dist_calc() for traj in self.trajets_tries)

    @property
    def dist_calc_chauffeur(self):
        """
        En mètres. Si pas dist réelle, se rabattre sur estimation distances
        chauffeur à vol d'oiseau + 15%
        Si transport annulé, ne compte que les distances du chauffeur au départ et retour.
        """
        transp_dist = self.dist_calc if not self.est_annule() else 0
        if not self.chauffeur_id:
            return transp_dist * (1 if self.retour else 2)
        chauff_empl_geo = self.chauffeur.adresse(quand=self.date).empl_geo
        dist_vers_depart = self.chauff_aller_dist or (
            int(distance_vo(chauff_empl_geo, self.adresse_depart().empl_geo) * 1150)
        )
        if self.est_annule():
            return dist_vers_depart * 2
        return dist_vers_depart + transp_dist + (
            self.chauff_retour_dist or (
                int(distance_vo(self.trajets_tries[-1].destination.empl_geo, chauff_empl_geo) * 1150)
            )
        )

    @property
    def est_passe(self):
        if self.heure_arrivee:
            return (self.heure_arrivee + timedelta(seconds=900)) < timezone.now()
        # Estimation
        return (
            self.heure_rdv + (
                self.duree_rdv if self.duree_rdv is not None else timedelta(0)
            )  + timedelta(hours=4)
        ) < timezone.now()

    @property
    def est_passe_lointain(self):
        """Considéré comme lointain si plus vieux que 2 mois depuis le 1 du mois en cours."""
        return self.date < (date.today().replace(day=1) - timedelta(days=61))

    def adresse_depart(self):
        return self.trajets_tries[0].origine

    def adresse_arrivee(self):
        return self.trajets_tries[-1].destination

    @cached_property
    def depart_geo(self):
        return self.adresse_depart().empl_geo

    @property
    def heure_depart(self):
        return timezone.localtime(self.trajets_tries[0].heure_depart) if self.trajets_tries else None

    @property
    def origine(self):
        return self.trajets_tries[0].origine

    @property
    def trajet_dest_princ(self):
        return next((traj for traj in self.trajets_tries if traj.destination_princ), None)

    @property
    def destination(self):
        return self.trajet_dest_princ.destination

    @property
    def vers_domicile(self):
        return len(self.trajets_tries) == 1 and self.trajets_tries[0].destination_domicile

    @property
    def heure_depart_retour(self):
        traj_princ = self.trajet_dest_princ
        if not traj_princ:
            return None
        next_ind = self.trajets_tries.index(traj_princ) + 1
        try:
            return timezone.localtime(self.trajets_tries[next_ind].heure_depart)
        except IndexError:
            return None

    @property
    def heure_arrivee(self):
        """
        Estimation +/- grossière de l'heure de fin du transport (compte tenu d'un
        retour dans tous les cas)
        """
        arr_dernier_trajet = self.trajets_tries[-1].heure_arrivee
        if arr_dernier_trajet is None:
            # Peut arriver si le service externe de calcul est indisponible
            temps_aller = self.heure_rdv - self.trajets_tries[-1].heure_depart
            if temps_aller != timedelta(0):
                heure = timezone.localtime(self.heure_rdv) + temps_aller
            else:
                heure = None
        else:
            heure = max(self.trajets_tries[-1].heure_arrivee, timezone.localtime(self.heure_rdv))
        if heure:
            return heure.replace(second=0)
        return heure

    def can_edit(self, user):
        return False  # Edit on an calendar point of view.

    def can_edit_rapport(self, benev):
        """
        Édition possible d'un rapport de transport par un bénévole.
        Dès le 2 d'un mois, seuls les transports du mois courant sont éditables.
        """
        if date.today().day == 1:
            un_du_mois = (date.today() - timedelta(days=1)).replace(day=1)
        else:
            un_du_mois = date.today().replace(day=1)
        return self.date >= un_du_mois and self.chauffeur == benev

    def chauffeurs_potentiels(self):
        """
        Renvoie une liste hiérarchisée des chauffeurs potentiels:
        1. chauffeurs dispos, triés par préférence (ou incompat.) client, distance
        2. chauffeurs non dispos, même tri
        """
        chauffeurs = list(
            Benevole.objects.par_domaine(
                "transport", actifs=True, depuis=self.date, jusqua=self.date + timedelta(days=1)
            ).avec_adresse(self.date).prefetch_related(
                Prefetch('preferences', queryset=Preference.objects.filter(client=self.client))
            )
        )
        debut = self.heure_depart
        fin = self.heure_arrivee
        dispos = Dispo.get_for_period(debut, fin)
        # Répartir dispos en: OK, annulées, absences
        dispos_ok, dispos_cancel, dispos_abs = [], [], []
        for dispo in dispos:
            dispo._match = 'full' if (dispo.fin >= fin and dispo.debut <= debut) else 'partial'
            if dispo.categorie == 'absence':
                dispos_abs.append(dispo)
            elif dispo.cancelled:
                dispos_cancel.append(dispo)
            else:
                dispos_ok.append(dispo)
        # Get all chauffeurs having at least one overlapping attributed trajet
        chauffeurs_overlap_transp = set(Trajet.objects.annotate(
            heure_arrivee=F('heure_depart') + F('duree_calc')
        ).exclude(
            transport__statut=Transport.StatutChoices.ANNULE
        ).filter(
            transport__chauffeur__isnull=False, heure_arrivee__gt=debut, heure_depart__lt=fin
        ).values_list('transport__chauffeur_id', flat=True))
        chauffeurs_dispos_full = set()
        chauffeurs_dispos_partial = set()
        chauffeurs_dispos_cancelled = set(dispo.chauffeur_id for dispo in dispos_abs)
        chauffeurs_refus = set(self.refus.values_list('chauffeur_id', flat=True) if self.pk else [])
        for dispo in dispos_ok:
            if dispo.chauffeur_id not in chauffeurs_overlap_transp:
                if dispo._match == 'full':
                    chauffeurs_dispos_full.add(dispo.chauffeur_id)
                else:
                    chauffeurs_dispos_partial.add(dispo.chauffeur_id)
        for dispo in dispos_cancel:
            if (dispo.chauffeur_id not in chauffeurs_dispos_full and
                dispo.chauffeur_id not in chauffeurs_dispos_partial):
                chauffeurs_dispos_cancelled.add(dispo.chauffeur_id)
        client_handis = set(self.client.handicaps_verbose)
        for chauffeur in chauffeurs:
            # Annoter les chauffeurs avec préférences, incompatibilités
            chauffeur.pref = next(iter(pref for pref in chauffeur.preferences.all()), None)
            chauffeur.incompats_client = client_handis & set(chauffeur.incompats_verbose)
            # Définir la priorité du chauffeur
            #  1. Transport entièrement dans dispo libre
            #  2. Transport partiellement dans dispo libre
            #  3. Autres chauffeurs
            #  4. Transport dans dispo annulée (ou chauffeur ne conduit pas ce véhicule)
            #     ou autre trajet en même temps.
            #  5. Transport refusé par chauffeur
            if chauffeur.pk in chauffeurs_refus:
                chauffeur.priorite_dispo = 6
            elif chauffeur.pk in chauffeurs_dispos_cancelled:
                chauffeur.priorite_dispo = 5
            elif chauffeur.pk in chauffeurs_overlap_transp:
                chauffeur.priorite_dispo = 4
            elif chauffeur.pk in chauffeurs_dispos_full:
                chauffeur.priorite_dispo = 1
            elif chauffeur.pk in chauffeurs_dispos_partial:
                chauffeur.priorite_dispo = 2
            else:
                chauffeur.priorite_dispo = 3
        self.calculer_distances_chauffeurs(chauffeurs)
        return sorted(
            chauffeurs,
            key=lambda ch: (
                ch.priorite_dispo,
                {'pref': -1, 'incomp': 1, None: len(ch.incompats_client)}.get(ch.pref and ch.pref.typ),
                ch.distance_tr[0] if (ch.distance_tr and ch.distance_tr[0] is not None) else 1000
            )
        )

    def vehicules_potentiels(self):
        vehicules = Vehicule.objects.exclude(archive_le__isnull=False)
        for vh in vehicules:
            vh.distance_vo = self.distance_vo_avec(vh) if vh.empl_geo else 100
        return sorted(vehicules, key=lambda vh: vh.distance_vo or 100)

    def calculer_distances_chauffeurs(self, chauffeurs, force_calc=False):
        """Annoter les chauffeurs avec distance au transport."""
        chauffeurs_dispos = []
        for chauffeur in chauffeurs:
            if chauffeur.priorite_dispo <= 1:
                chauffeurs_dispos.append(chauffeur)
        loc_cibles = [self.adresse_depart()]
        if not self.retour:
            loc_cibles.append(self.destination)
        loc_chauffeurs_dispos = set([
            (chauffeur.adresse_active["npa"], chauffeur.adresse_active["localite"])
            for chauffeur in chauffeurs_dispos
        ])
        cached_dists = CachedDistCalculator([(loc.npa, loc.localite) for loc in loc_cibles], loc_chauffeurs_dispos)
        for chauffeur in chauffeurs:
            npa, loc = chauffeur.adresse_active["npa"], chauffeur.adresse_active["localite"]
            nouvelle_dist = cached_dists.get_best(npa, loc, only_cached=not force_calc)
            if nouvelle_dist:
                chauffeur.distance_tr = nouvelle_dist, "city"
                continue
            # En dernier recours, vol d'oiseau
            chauffeur.distance_tr = self.distance_vo_avec(chauffeur), "vo"

    def distance_vo_avec(self, obj):
        obj_geo = getattr(obj, "empl_geo", None) or obj.adresse(quand=self.date).empl_geo
        if self.retour:
            return distance_vo(obj_geo, self.depart_geo)
        # Pour un aller simple, le chauffeur peut aussi habiter près de la destination.
        dists = [d for d in [
            distance_vo(obj_geo, self.depart_geo),
            distance_vo(obj_geo, self.destination.empl_geo),
        ] if d is not None]
        return min(dists) if dists else None

    def calc_chauffeur_dists(self, save=False):
        """
        Calcule les distances réelles entre domicile chauffeur et départ, arrivée et domicile chauffeur.
        """
        try:
            chauff_empl_geo = self.chauffeur.adresse(quand=self.date).empl_geo
            if self.vehicule_id and self.vehicule.empl_geo:
                # si vehicule, calculer "crochet" par garage
                vehic_empl_geo = self.vehicule.empl_geo
                aller = Counter(distance_real(chauff_empl_geo, vehic_empl_geo))
                aller.update(distance_real(
                    vehic_empl_geo, self.adresse_depart().empl_geo
                ))
                retour = Counter(distance_real(
                    self.adresse_arrivee().empl_geo, vehic_empl_geo
                ))
                retour.update(distance_real(vehic_empl_geo, chauff_empl_geo))
            else:
                aller = distance_real(
                    chauff_empl_geo, self.adresse_depart().empl_geo
                )
                retour = distance_real(
                    self.adresse_arrivee().empl_geo, chauff_empl_geo
                )
            if aller:
                self.chauff_aller_dist = aller.get('distance')
                self.chauff_aller_duree = convert_duration_seconds(aller.get('duration'))
            if retour:
                self.chauff_retour_dist = retour.get('distance')
                self.chauff_retour_duree = convert_duration_seconds(retour.get('duration'))
        except ORSUnavailable:
            pass
        else:
            if save:
                self.save(update_fields=[
                    'chauff_aller_dist', 'chauff_retour_dist',
                    'chauff_aller_duree', 'chauff_retour_duree',
                ])

    def calculer_km_auto(self):
        self.km = round(self.dist_calc_chauffeur / 1000, 1)
        self.save(update_fields=["km"])

    def calculer_duree_auto(self):
        self.duree_eff = self.duree_calc_chauffeur
        self.save(update_fields=["duree_eff"])

    def match(self, referent):
        return 1 if (
            'transp' in referent.facturation_pour and (
                canton_abrev().lower() == "ne" or  # NE n'utilise pas LAA
                not referent.no_sinistre or
                self.trajets_tries[0].typ == Trajet.Types.LAA
            )
        ) else 0

    @property
    def rapport_complet(self):
        if self.est_annule() and self.rapport_chauffeur:
            return True
        if self.km and self.duree_eff:
            return True
        return False

    @property
    def temps_attente_conforme(self):
        return not self.temps_attente or self.temps_attente <= timedelta(hours=3)

    @property
    def km_conforme(self):
        dist_calc = self.dist_calc_chauffeur
        return ((self.km * 1000 - dist_calc) / dist_calc) <= 0.05

    @property
    def rapport_conforme(self):
        if not self.rapport_complet:
            return False
        if (
            not self.temps_attente_conforme or
            self.rapport_chauffeur != "" or
            any(fr.non_conforme for fr in self.frais.all()) or
            not self.km_conforme
        ):
            return False
        return True

    def km_calc(self):
        """
        Calcul des km à facturer pour ce transport (sans tenir compte des km du et vers domicile chauffeur).
        """
        metres = sum([tr.dist_calc for tr in self.trajets_tries])
        if self.retour is False:
            # En cas d'aller simple, le kilométrage du retour est additionné (retour du chauffeur)
            metres_retour = Trajet.distance_entre(self.client, self.adresse_arrivee(), self.adresse_depart())
            if metres_retour is None:
                metres = metres * 2
            else:
                metres += metres_retour
        return round(metres / Decimal(1000), 1)

    def frais_total(self):
        return sum(fr.cout for fr in self.frais.all())

    def cout_attente(self):
        """
        Facturation du temps d’attente. Renvoie (quantité, montant total).
        """
        return fact_policy().cout_attente(self)

    def donnees_facturation(self):
        """Renvoyer dictionnaire avec données de facturation"""
        if self.statut_fact == Transport.FacturationChoices.PAS_FACTURER:
            return None
        fpolicy = fact_policy()
        infos = {
            'no': self.pk,
            "type_fact": "transport",
            'rendez-vous': timezone.localtime(self.heure_rdv),
            'type': self.trajets_tries[0].get_typ_display(),
            'depart': str(self.adresse_depart()),
            'destination': str(self.destination),
            'retour': self.retour,
            'temps_attente': self.temps_attente,
            'avs': self.client.is_avs(self.heure_depart.date()),
            'tarif_km': fpolicy.tarif_km(self),
        }
        kms_a_facturer = fpolicy.kms_a_facturer(self)
        cout_km = fpolicy.cout_km(self, kms_a_facturer)
        forfait = fpolicy.cout_forfait(self)
        infos.update({
            "km": kms_a_facturer,
            "cout_km": cout_km,
            "cout_forfait": forfait,
        })
        if self.est_annule():
            cout_annul = fpolicy.cout_annulation(self)
            infos.update({
                "annulation": cout_annul,
                "total": cout_km + cout_annul + forfait,
            })
        else:
            cout_total = cout_km + forfait
            nb_attente, cout_attente = self.cout_attente()
            if cout_attente > 0:
                infos['nb_attente'] = nb_attente
                infos['cout_attente'] = cout_attente
                cout_total += infos['cout_attente']

            liste_frais = []
            for frais in self.frais.filter(cout__gt=0):
                cout_total += frais.cout
                liste_frais.append({
                    'descr': "Facturation des frais de repas" if frais.typ == 'repas' else "Facturation des frais de parking",
                    'typ': frais.typ,
                    'cout': frais.cout,
                })
            infos.update({
                'frais': liste_frais,
                'total': cout_total,
            })
        return infos

    @classmethod
    def get_for_person(cls, chauffeur, start, end):
        # Granularité à la journée pour le moment.
        start = start.date()  # astimezone(timezone.utc)
        end = end.date()  # astimezone(timezone.utc)
        # To be tested
        return chauffeur.transports.filter(
            date__gte=start, date__lte=end
        ).order_by('heure_rdv')

    ### Event API ### NOQA
    @property
    def debut(self):
        """Event API"""
        return self.heure_depart

    @property
    def fin(self):
        """Event API"""
        return self.heure_arrivee

    @property
    def description(self):
        """Event API"""
        return f"Transport pour {self.client}"

    def spans(self):
        """
        Event API.
        Return number of 15min slices it occupies.
        """
        return round((self.fin - self.debut).seconds / 60 / 15, 1)

    def url_attrib(self):
        if self.pk:
            return reverse('transport-attrib', args=[self.pk])
        else:
            return reverse('transport-model-attrib', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])

    def url_edit(self):
        if self.pk:
            return reverse('transport-edit', args=[self.pk])
        else:
            return reverse('transport-model-edit', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])

    def url_cancel(self):
        if self.pk:
            return reverse('transport-cancel', args=[self.pk])
        else:
            return reverse('transport-model-cancel', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])


class TrajetCommun(models.Model):
    """Trajet effectué par un même chauffeur pour plusieurs clients."""
    date = models.DateField()
    chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.PROTECT, related_name='transports_communs'
    )

    def __str__(self):
        return f"Trajet commun du {self.date} par {self.chauffeur}"

    def nombre_clients(self):
        return self.trajet_set.count()


class Trajet(models.Model):
    class Types(models.TextChoices):
        MEDIC = 'medic', 'Médico-thérapeutique'
        PARTICIP = 'particip', 'Participatif-intégratif'
        LAA = 'laa', 'Accident (LAA)'
        EMPLETTES = 'emplettes', 'Emplettes à deux'
        HOME = 'home', 'Home'

    transport = models.ForeignKey(Transport, related_name="trajets", on_delete=models.CASCADE)
    typ = models.CharField("Type de déplacement", max_length=10, choices=Types.choices)
    heure_depart = models.DateTimeField("Heure de départ")
    origine_domicile = models.BooleanField(default=True)
    origine_adr = models.ForeignKey(Adresse, null=True, blank=True, on_delete=models.PROTECT, related_name="+")
    destination_domicile = models.BooleanField(default=False)
    destination_adr = models.ForeignKey(Adresse, null=True, blank=True, on_delete=models.PROTECT, related_name="+")
    destination_princ = models.BooleanField("Destination principale", default=False)
    duree_calc = models.DurationField("Durée estimée du trajet", null=True, blank=True)
    dist_calc = models.IntegerField("Distance estimée du trajet [m]", blank=True, null=True)
    commun = models.ForeignKey(TrajetCommun, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return (
            f"Trajet depuis «{self.origine}» jusqu’à «{self.destination}» "
            f"pour «{self.transport.client if self.transport else '<Transport à définir>'}» le «{self.heure_depart}»"
        )

    @property
    def origine(self):
        return (
            self.transport.client.adresses.depose(self.heure_depart.date()) if self.origine_domicile
            else self.origine_adr
        )

    @property
    def destination(self):
        return (
            self.transport.client.adresses.depose(self.heure_depart.date()) if self.destination_domicile
            else self.destination_adr
        )

    def get_duree_calc(self, raise_ors=False):
        if self.duree_calc is None:
            self.calc_trajet(raise_ors=raise_ors)
        return self.duree_calc

    def get_dist_calc(self, raise_ors=False):
        if self.dist_calc is None:
            self.calc_trajet(raise_ors=raise_ors)
        return self.dist_calc

    @property
    def heure_arrivee(self):
        duree = self.get_duree_calc()
        if duree is None:
            return None
        return timezone.localtime(self.heure_depart) + self.duree_calc

    def calc_trajet(self, save=True, raise_ors=False):
        try:
            dist_real = distance_real(self.origine.empl_geo, self.destination.empl_geo)
        except ORSUnavailable:
            if raise_ors:
                raise
            return False
        if dist_real is not None and 'duration' in dist_real:
            self.duree_calc = convert_duration_seconds(dist_real['duration'])
            self.dist_calc = round(dist_real['distance'])
            if save:
                self.save(update_fields=['duree_calc', 'dist_calc'])
            return True
        return False

    @classmethod
    def distance_entre(cls, client, origine, destination):
        """Recherche de la distance d'un trajet en cherchant d'abord un trajet existant récent dans la BD."""
        filtre = {}
        if isinstance(origine, Adresse):
            filtre['origine_adr'] = origine
        else:
            filtre['origine_domicile'] = True
        if isinstance(destination, Adresse):
            filtre['destination_adr'] = destination
        else:
            filtre['destination_domicile'] = True
        existing = Trajet.objects.annotate(month=TruncMonth('heure_depart')).filter(
            transport__client=client, dist_calc__isnull=False, heure_depart__date__gt=date.today() - timedelta(days=40)
        ).filter(**filtre)
        if existing:
            return existing[0].dist_calc

        # Query the distance service
        dist = distance_real(origine.empl_geo, destination.empl_geo)
        if dist is not None and 'duration' in dist:
            return round(dist['distance'])
        return None

    def remove_cached_distance(self):
        """Delete any cache entry set in distance_real."""
        for bool_val in (True, False):
            cache_key = str(hash(
                tuple(self.origine.empl_geo + self.destination.empl_geo) + (bool_val,)
            ))
            cache.delete(cache_key)

    def to_date(self, dt):
        initial_offset = timezone.localtime(self.heure_depart).utcoffset()
        # Adjust start/end to consider timezone change from initial event timezone
        heure = self.heure_depart.replace(year=dt.year, month=dt.month, day=dt.day)
        self.heure_depart = fix_occurrence_date(heure, initial_offset, reverse=False)


class JournalTransport(models.Model):
    transport = models.ForeignKey(
        Transport, on_delete=models.CASCADE, verbose_name='Transport', related_name='journaux'
    )
    description = models.TextField()
    quand = models.DateTimeField()
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        get_latest_by = "quand"

    def __str__(self):
        return f"{self.quand}: {self.description}"


class Refus(models.Model):
    transport = models.ForeignKey(Transport, related_name="refus", on_delete=models.CASCADE)
    chauffeur = chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.CASCADE, related_name='+'
    )

    def __str__(self):
        return f"Refus du transport {self.transport} par {self.chauffeur}"


class Frais(models.Model):
    class TypeFrais(models.TextChoices):
        REPAS = 'repas', "Frais de repas"
        PARKING = 'parking', "Frais de parking"

    transport = models.ForeignKey(Transport, related_name="frais", on_delete=models.CASCADE)
    descriptif = models.TextField("Descriptif", blank=True)
    cout = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Coût")
    typ = models.CharField("Type de frais", max_length=7, choices=TypeFrais.choices)
    justif = models.FileField(upload_to="justificatifs", blank=True, verbose_name="Justificatif")

    def __str__(self):
        return f"Frais ({self.cout}) pour le transport {self.transport if self.transport_id else '<non défini>'}"

    def description(self):
        return self.get_typ_display()

    @property
    def non_conforme(self):
        return self.typ != 'repas' and self.cout >= 7


class Preference(models.Model):
    PREF_TYPE = (
        ('pref', '😀 Préféré'),
        ('incomp', '😡 Incompatible'),
    )
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="preferences")
    chauffeur = models.ForeignKey(Benevole, on_delete=models.CASCADE, related_name="preferences")
    typ = models.CharField("Type de lien", max_length=10, choices=PREF_TYPE)
    remarque = models.TextField(blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='client_chauffeur_unique', fields=['client', 'chauffeur']),
        ]

    def __str__(self):
        return f"Lien «{self.typ}» entre {self.client} et {self.chauffeur}"


class Facture(models.Model):
    """Facture mensuelle de tous les transports d'un client."""
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="factures_transports")
    mois_facture = models.DateField("Mois comptable")
    no = models.CharField("N° de facture", max_length=20, blank=True)
    date_facture = models.DateField("Date de facture")
    autre_debiteur = models.ForeignKey(
        Referent, on_delete=models.PROTECT, null=True, blank=True, verbose_name="Autre débiteur"
    )
    nb_transp = models.PositiveSmallIntegerField("Nb de transports")
    montant_total = models.DecimalField(
        "Montant total", max_digits=6, decimal_places=2, null=True, blank=True
    )
    fichier_pdf = models.FileField("Facture PDF", upload_to="transports/factures", blank=True)
    annulee = models.BooleanField("Facture annulée", default=False)
    annulee_msg = models.TextField("Raison de l’annulation", blank=True)
    exporte = models.DateTimeField("Exporté vers compta", blank=True, null=True)
    export_err = models.TextField("Erreur d’exportation", blank=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='no_facture_unique', fields=['no']),
        ]

    def __str__(self):
        return f"Facture de {self.mois_facture.strftime('%m %Y')} pour {self.client}"

    @classmethod
    def next_no(cls):
        return str(
            int(cls.objects.filter(no__startswith="1").aggregate(no_max=Max("no"))["no_max"] or 1000000) + 1
        )

    @classmethod
    def esr_num(self, no_fact, no_debiteur, with_spaces=False):
        num = str(no_debiteur).zfill(14) + str(no_fact).zfill(12)
        num += esr.calc_check_digit(num)
        if with_spaces:
            return esr.format(num)
        return num

    @classmethod
    def non_transmises(cls):
        return cls.objects.filter(exporte__isnull=True)

    def get_transports(self):
        return self.transport_set.all().order_by('heure_rdv')

    def get_items(self):
        # Mélange transports et réservations
        return sorted(
            chain(
                self.get_transports().avec_is_ofas().prefetch_related('frais'),
                self.vehiculeoccup_set.all()
            ),
            key=lambda obj: obj.heure_rdv if isinstance(obj, Transport) else obj.duree.lower
        )

    def annuler(self, raison=''):
        self.annulee = True
        self.annulee_msg = raison
        if self.exporte:
            # Annuler dans l'ERP
            try:
                api = get_erp_api()
            except NoAPIException:
                pass
            else:
                api.cancel_invoice('transport', self)
        self.save()

    def recalc_montant_total(self, save=False):
        dfact_list = []
        for transp in self.get_transports():
            dfact = transp.donnees_facturation()
            if dfact is None:
                continue
            dfact_list.append(dfact)
        montant = fact_policy().montant_total(dfact_list)
        if save and montant != self.montant_total:
            self.montant_total = montant
            self.save(update_fields=["montant_total"])
        return montant

    def fs_path(self, check_exists=False):
        path = settings.MEDIA_ROOT.joinpath(
            'transports', 'factures',
            f'fact_{self.client.pk}_{self.mois_facture.year}_{self.mois_facture.month}.pdf'
        )
        if check_exists and not path.exists():
            return None
        return path


class Dispo(OccurrenceMixin, models.Model):
    class CategChoices(models.TextChoices):
        DISPO = 'dispo', 'Disponibilité'
        ABSENCE = 'absence', 'Absence'

    chauffeur = models.ForeignKey(Benevole, on_delete=models.CASCADE, related_name='dispos')
    debut = models.DateTimeField("Début", db_index=True)
    fin = models.DateTimeField("Fin", db_index=True, null=True, blank=True)
    description = models.TextField("Description", blank=True)
    categorie = models.CharField("Catégorie", max_length=10, choices=CategChoices.choices, default='dispo')
    regle = models.ForeignKey(
        Regle, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    fin_recurrence = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    cancelled = False

    def __lt__(self, other):
        return self.debut < other.debut

    def __str__(self):
        txt = f"Dispo de {self.chauffeur}"
        if self.regle:
            txt += (
                f" le {self.debut.strftime('%A')} de {self.debut.strftime('%H:%M')} "
                f"à {self.fin.strftime('%H:%M')}, dès le {self.debut.date().strftime('%d.%m.%Y')}"
            )
            if self.fin_recurrence:
                txt += f"jusqu’au {self.fin_recurrence.strftime('%d.%m.%Y')}"
        else:
            txt += f" - unique le {self.debut.strftime('%a %d.%m.%Y %H:%M')} jusqu’à {self.fin.strftime('%H:%M')}"
        return txt

    def __repr__(self):
        return f"Dispo(debut={self.debut}, fin={self.fin})"

    @property
    def ev_category(self):
        return self.categorie

    def spans(self):
        """Return number of 15min slices it occupies."""
        return round((self.fin - self.debut).seconds / 60 / 15, 1)

    def can_edit(self, user):
        return getattr(user, 'benevole', None) == self.chauffeur or user.has_perm('transport.change_dispo')

    def cancel_for(self, day):
        initial_offset = timezone.localtime(self.debut).utcoffset()
        day_start = fix_occurrence_date(
            datetime.combine(day, self.debut.time(), tzinfo=self.debut.tzinfo),
            initial_offset, reverse=False
        )
        Occurrence.objects.create(dispo=self, orig_start=day_start, annule=True)

    @classmethod
    def _select_dispos(cls, qs, start, end, categ=None):
        """Return dispos that at least overlap the start-end interval."""
        start = start.astimezone(tz.utc)
        end = end.astimezone(tz.utc)
        unique_dispos = qs.filter(
            regle__isnull=True, fin__gte=start, debut__lt=end
        ).select_related('chauffeur')
        if categ is not None:
            unique_dispos = unique_dispos.filter(categorie=categ)
        recur_dispos = qs.filter(
            regle__isnull=False
        ).filter(
            models.Q(fin_recurrence__gte=start) | models.Q(fin_recurrence__isnull=True)
        ).select_related('regle', 'chauffeur').prefetch_related('exceptions')
        if categ is not None:
            recur_dispos = recur_dispos.filter(categorie=categ)
        dispos = list(unique_dispos)
        for ev in recur_dispos:
            dispos.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(dispos)

    @classmethod
    def get_for_person(cls, benev, start, end, exclude=None, categ=None):
        # categ=None means all categs
        qs = benev.dispos
        if exclude:
            qs = qs.exclude(pk__in=[ev.pk for ev in exclude])
        return cls._select_dispos(qs, start, end, categ=categ)

    @classmethod
    def get_for_period(cls, start, end):
        qs = cls.objects.filter(
            Q(chauffeur__activite__type_act__code=TypeActivite.TRANSPORT) &
            Q(chauffeur__activite__duree__contains=start.date()) &
            Q(chauffeur__activite__inactif=False)
        )
        return cls._select_dispos(qs, start, end)

    def get_exceptions(self):
        return {occ.orig_start: occ for occ in self.exceptions.all()} if self.pk else {}

    def memory_instance(self, occ_start):
        duration = self.fin - self.debut
        return MemOccurrence(
            id=self.id, debut=occ_start, fin=occ_start + duration,
            description=self.description, categorie=self.categorie,
            chauffeur=self.chauffeur, excepts=self._exceptions,
        )


class MemOccurrence(Dispo):
    class Meta:
        proxy = True

    is_recurring = True

    def __init__(self, *args, excepts={}, **kwargs):
        super().__init__(*args, **kwargs)
        if self.debut in excepts and excepts[self.debut].annule:
            self.cancelled = True

    def __repr__(self):
        return f"MemOccurrence(debut={self.debut}, fin={self.fin})"


class Occurrence(models.Model):
    """Recurring event exceptions."""
    dispo = models.ForeignKey(Dispo, on_delete=models.CASCADE, related_name='exceptions')
    orig_start = models.DateTimeField("Début d’origine")
    annule = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["dispo", "orig_start"], name="dispo_exception_unique"
            ),
        ]

    def __str__(self):
        return f"Exception du {self.orig_start.strftime('%d.%m.%Y')} pour la dispo «{self.dispo}»"


class CalculateurFraisTransport(CalculateurFraisBase):
    """
    Calculateur de frais bénévoles à partir des transports.
    Cette classe est destinée à être surchargée dans le fichier transport_policies.py cantonal.
    """
    service = 'transport'
    TARIF_CHAUFFEUR = None
    FORFAIT_ATTENTE = None
    type_map = canton_app.TYPE_FRAIS_MAP

    @staticmethod
    def sum_by_codes(lignes, codes):
        return sum([l.montant for l in lignes if l.libelle.no in codes])

    @staticmethod
    def get_by_code(lignes, code, default=0):
        return next((l.quantite for l in lignes if l.libelle.no == code), default)

    def _ajout_transport(self, chauffeurs, transp):
        if transp.chauffeur.ne_pas_defrayer:
            return
        chauffeurs.setdefault(transp.chauffeur, {key: Decimal(0) for key in [
            'kms', 'kms_vhc', 'nb_attente', 'nb_plusieurs', 'frais_repas', 'frais_divers',
        ]})
        if transp.vehicule_id:
            chauffeurs[transp.chauffeur]['kms_vhc'] += max(transp.km or 0, fact_policy().MIN_KM_FACTURES)
        else:
            chauffeurs[transp.chauffeur]['kms'] += max(transp.km or 0, fact_policy().MIN_KM_FACTURES)
        nb, _ = transp.cout_attente()
        chauffeurs[transp.chauffeur]['nb_attente'] += nb
        chauffeurs[transp.chauffeur]['frais_repas'] += transp.repas or 0
        chauffeurs[transp.chauffeur]['frais_divers'] += transp.autres_frais or 0

    def _finaliser(self, chauffeurs):
        """Calculs complémentaires après l'ajout de tous les transports."""
        pass

    def frais_par_benevole(self, benevole=None):
        base_qs = Transport.objects.all() if benevole is None else Transport.objects.filter(chauffeur=benevole)
        transports = base_qs.annotate(
            month=TruncMonth('heure_rdv', output_field=models.DateField()),
            repas=Sum('frais__cout', filter=Q(frais__typ='repas')),
            autres_frais=Sum('frais__cout', filter=~Q(frais__typ='repas')),
        ).filter(
            Q(month=self.mois) & (
                Q(statut=Transport.StatutChoices.CONTROLE) |
                (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur=True))
            )
        ).select_related('chauffeur__persona').order_by('chauffeur__persona__nom')
        chauffeurs = {}
        for transp in transports:
            self._ajout_transport(chauffeurs, transp)
        self._finaliser(chauffeurs)
        return chauffeurs

    def lignes_depuis_data(self, benevole, data):
        lignes = []
        if data.get('frais_repas', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=self.type_map['repas']),
                quantite=Decimal('1'), montant_unit=data['frais_repas']
            ))
        if data.get('frais_divers', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=self.type_map['divers']),
                quantite=Decimal('1'), montant_unit=data['frais_divers']
            ))
        if data.get('parking', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=self.type_map['divers']),
                quantite=data['parking'],
            ))
        if data.get('nb_attente', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=self.type_map['transp_attente']),
                quantite=data['nb_attente'], montant_unit=self.FORFAIT_ATTENTE,
            ))
        if 'kms_lt_6000' not in self.type_map:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=self.type_map['kms_chauffeurs']),
                quantite=data['kms'], montant_unit=self.TARIF_CHAUFFEUR,
            ))
        return lignes


def fix_occurrence_date(dt, initial_offset, reverse=False):
    if reverse:
        tz_diff = initial_offset - timezone.localtime(dt).utcoffset()
    else:
        tz_diff = timezone.localtime(dt).utcoffset() - initial_offset
    if tz_diff:
        return dt - tz_diff
    return dt


def convert_duration_seconds(duration):
    return timedelta(seconds=round(duration))


def color_is_dark(c):
    if c == 'black':
        return True
    elif c == 'white':
        return False
    # Assume a rgb '#aabbcc' syntax
    rgb = int(c.strip('#'), 16)
    red = rgb >> 16 & 0xff
    green = rgb >> 8 & 0xff
    blue = rgb >> 0 & 0xff
    luma = 0.2126 * red + 0.7152 * green + 0.0722 * blue  # per ITU-R BT.709
    return luma < 120
