from datetime import date, timedelta

from django.core.management.base import BaseCommand
from django.db.models import Max

from client.models import Client


class Command(BaseCommand):
    """
    Archivage automatique des clients transport après un certain temps sans transport effectué.
    Généralement exécuté par un job cron toutes les semaines.
    """
    def handle(self, **options):
        self.archiver_clients_transport(options['verbosity'])

    def archiver_clients_transport(self, verbosity):
        to_archive = Client.objects.par_service('transport').annotate(
            last_transport=Max('transports__date')
        ).filter(
            archive_le__isnull=True,
            last_transport__lt=date.today() - timedelta(days=365)
        )
        for client in to_archive:
            if verbosity > 1:
                print(f"Archivage de {client} ({client.pk})")
            client.archiver(
                None, 'transport',
                check=False,
                message="Archivage automatique car plus de transport pendant 1 année"
            )
        if verbosity > 1:
            self.stdout.write(
                self.style.SUCCESS(f"{len(to_archive)} client(s) transport archivé(s)")
            )
