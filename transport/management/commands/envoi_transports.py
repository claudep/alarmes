from collections import defaultdict
from datetime import date, datetime
from io import BytesIO

from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Min

from transport.models import Transport
from transport.pdf import TransportsPDF
from transport.templatetags.transport_utils import date_transp

message = (
    "Bonjour,\n\n"
    "Vous trouverez ci-joint la liste des transports Croix-Rouge qui vous ont "
    "été attribués du {jour_deb} au {jour_fin}.\n\n"
    "Cordiales salutations.\n\n"
    "L’équipe des transports Croix-Rouge"
)

class Command(BaseCommand):
    help = "Envoyer les transports de certains jours à certains clients."

    def add_arguments(self, parser):
        parser.add_argument('--chauffeurs', default='tous')
        parser.add_argument('--jour-deb', required=True)
        parser.add_argument('--jour-fin', required=True)

    def handle(self, **options):
        try:
            self.jour_deb = datetime.strptime(options['jour_deb'], '%d.%m.%Y').date()
        except ValueError:
            raise CommandError(f"Impossible de convertir {options['jour_deb']} en date.")
        if self.jour_deb < date.today():
            raise CommandError("La date de début ne peut pas être dans le passé")
        try:
            self.jour_fin = datetime.strptime(options['jour_fin'], '%d.%m.%Y').date()
        except ValueError:
            raise CommandError(f"Impossible de convertir {options['jour_fin']} en date.")
        if self.jour_deb > self.jour_fin:
            raise CommandError("La date de fin doit être ultérieure à la date de début")

        transports = Transport.objects.filter(
            statut=Transport.StatutChoices.CONFIRME, date__range=(self.jour_deb, self.jour_fin)
        ).annotate(depart=Min('trajets__heure_depart')).order_by('depart')
        if options['chauffeurs'] != 'tous':
            transports = transports.filter(chauffeur__courriel=options['chauffeurs'])
        if not transports:
            return "Aucun transport pour la période indiquée."

        chauffeurs = defaultdict(list)
        for transp in transports:
            chauffeurs[transp.chauffeur].append(transp)

        for chauffeur, transports in chauffeurs.items():
            # Ne pas envoyer si le chauffeur s'est connecté une première fois à l'application
            if chauffeur.utilisateur and chauffeur.utilisateur.last_login:
                self.stderr.write(f"Le chauffeur {chauffeur} s’est déjà connecté à l`application")
                continue
            if not chauffeur.courriel:
                self.stderr.write(f"Le chauffeur {chauffeur} ne possède pas de courriel")
                continue
            self.stdout.write(f"Envoi de {len(transports)} transport(s) à {chauffeur}")
            self.envoi_chauffeur(chauffeur, transports)

    def envoi_chauffeur(self, chauffeur, transports):
        jour_deb=date_transp(self.jour_deb)
        jour_fin=date_transp(self.jour_fin)
        msg = message.format(jour_deb=jour_deb, jour_fin=jour_fin)
        temp = BytesIO()
        pdf = TransportsPDF(temp)
        pdf.produce(chauffeur, transports, self.jour_deb, self.jour_fin)
        temp.seek(0)

        email = EmailMessage(
            f"Transports Croix-Rouge du {jour_deb} au {jour_fin}",
            msg,
            None,
            [chauffeur.courriel]
        )
        email.attach(
            f"transports_{self.jour_deb.strftime('%d_%m_%Y')}_{self.jour_fin.strftime('%d_%m_%Y')}.pdf",
            temp.getvalue(),
            "application/pdf",
        )
        email.send()
        print(f"Message envoyé à {chauffeur}, {chauffeur.courriel}")
