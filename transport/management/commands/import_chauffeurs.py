import argparse

from openpyxl import load_workbook

from django.core.management.base import BaseCommand
from django.db import transaction

from benevole.models import Activite, Benevole, TypeActivite
from common.utils import read_date


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('fichier', type=argparse.FileType('rb'))

    def handle(self, **options):
        wb = load_workbook(options['fichier'])
        ws = wb.active
        self.act_transport = TypeActivite.objects.get(code=TypeActivite.TRANSPORT)
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                if not any(val for val in values.values()):
                    print("Ligne entièrement vide, fin d'importation")
                    break
                self.import_chauffeur(values)
            #raise Exception("Importation en phase de test")

    def import_chauffeur(self, values):
        genre = {'H': 'M', 'M': 'M', 'F': 'F'}.get(values['Sexe'][0])
        try:
            chauffeur = Benevole.objects.get(nom=values['Nom'], prenom=values['Prénom'])
        except Benevole.DoesNotExist:
            pass
        else:
            self.stdout.write(f"Le chauffeur {chauffeur} existe déjà")
            # Compléter avec nouvelles valeurs
            if 'transport' not in chauffeur.get_activites_display().lower():
                Activite.objects.create(
                    benevole=chauffeur, type_act=self.act_transport, duree=(read_date(values["Date d'engagement"]), None),
                )
            chauffeur.date_naissance = read_date(values['Date de naissance'])
            chauffeur.no_plaques = clean(values['Numéro de plaque'])
            if not chauffeur.genre:
                chauffeur.genre = genre
            chauffeur.full_clean()
            chauffeur.save()
            return

        npa, lieu = values["No postal et Localité"].split(maxsplit=1)
        if not npa or not lieu:
            self.stdout.write(
                f"Code postal et localité sont obligatoires, importation de {values['Nom']} {values['Prénom']} annulée"
            )
            return

        raise NotImplementedError("Importation des no de tél. à refaire")
        chauffeur = Benevole(
            nom=values['Nom'], prenom=values['Prénom'], langues=['fr'],
            genre=genre, date_naissance=read_date(values['Date de naissance']),
            rue=clean(values["Rue et No"]),
            npa=npa, localite=lieu, tel_prive=clean(values['Téléphone privé']),
            #tel_prof=clean(values['Téléphone professionnel']), tel_mobile=clean(values['Mobile']),
            courriel=values['Email'],
            no_plaques=clean(values['Numéro de plaque']),
            remarques=clean(values["Particularités du chauffeur"]),
        )
        chauffeur.full_clean()
        chauffeur.save()
        Activite.objects.create(
            benevole=chauffeur, type_act=self.act_transport, duree=(read_date(values["Date d'engagement"]), None),
        )


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val
