import argparse
from datetime import date

from openpyxl import load_workbook

from django.core.management.base import BaseCommand
from django.db import transaction

from client.models import AdresseClient, Client, Prestation
from common.choices import Services
from common.utils import read_date


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('fichier', type=argparse.FileType('rb'))

    def handle(self, **options):
        wb = load_workbook(options['fichier'])
        ws = wb.active
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                self.import_clients(values)
            #raise Exception("Importation en phase de test")

    def import_clients(self, values):
        try:
            client = Client.objects.get(nom=values['Nom'], prenom=values['Prénom'], npa=values['Code Postal'])
        except Client.DoesNotExist:
            pass
        else:
            self.stdout.write(f"Le client {client} existe déjà")
            adresse = client.adresse()
            data_local = f'{adresse.rue}/{adresse.npa}/{adresse.localite}'
            data_import = f"{values['Rue']} {values['Numéro']}/{values['Code Postal']}/{values['Lieu']}"
            if data_local != data_import:
                self.stdout.write(f"Différence entre «{data_local}» et «{data_import}»")
            # Compléter avec nouvelles valeurs
            if Services.TRANSPORT not in client.prestations_actuelles(as_services=True):
                Prestation.objects.create(client=client, service=Services.TRANSPORT, debut=(date.today(), None))
            client.no_debiteur = clean(values['Navision ID'])
            client.save()
            return

        if not values['Code Postal'] or not values['Lieu']:
            self.stdout.write(
                f"Code postal et localité sont obligatoires, importation de {values['Nom']} {values['Prénom']} annulée"
            )
            return

        client = Client(
            nom=values['Nom'], prenom=values['Prénom'], langues=['fr'],
            genre=values['Sexe'], date_naissance=read_date(values['Date de naissance']),
            tel_1=clean(values['privé Téléphone']), courriel=clean(values['privé Courriel']),
            no_debiteur=clean(values['Navision ID']),
        )
        client.full_clean()
        client.save()
        AdresseClient.objects.create(
            client=client, principale=(date.today(), None),
            rue=" ".join([val for val in [values['Rue'], str(values['Numéro'])] if val]),
            npa=values['Code Postal'], localite=values['Lieu'],
        )
        Prestation.objects.create(client=client, service=Services.TRANSPORT, duree=(date.today(), None))


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val
