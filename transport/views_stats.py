from collections import Counter
from datetime import date, timedelta

from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import (
    Case, Count, OuterRef, Prefetch, Q, Subquery, Sum, Value, When
)
from django.db.models.functions import Coalesce, TruncMonth
from django.forms import Select
from django.views.generic import TemplateView

from benevole.models import Activite, Benevole
from common.export import ExpLine
from common.forms import AnneeForm, RegionField
from common.stat_utils import DateLimitForm, Month, StatsMixin
from common.utils import RegionFinder

from .models import Facture, Trajet, Transport


class StatForm(DateLimitForm):
    region = RegionField(
        widget=Select(attrs={'class': 'immediate_submit', 'form': 'date_select_form'}),
        empty_choice_label="-- toutes régions --", include_autre=False, required=False
    )


class StatsTransportsView(StatsMixin, TemplateView):
    template_name = 'transport/stats/transports.html'
    active = 'transports'
    form_class = StatForm
    stat_items = {
        'clients': {'label': 'Nbre de clients'},
        'transports': {
            'label': 'Transports effectués',
            'help': 'Allers simples + 2 * Allers-retours',
        },
        'allers': {'label': 'Allers simples'},
        'allers_retours': {'label': 'Allers-retours'},
        'km_chauffeurs': {
            'label': 'Km effectués',
            'help': 'Km déclarés par les chauffeurs, du départ de leur domicile '
                    'au retour à leur domicile.',
        },
        'clients_emplettes': {'label': 'Clients pour emplettes à deux'},
        'chauffeurs_emplettes': {'label': 'Chauffeurs pour emplettes à deux'},
        'duree_emplettes': {'label': 'Heures bénévoles pour emplettes à deux'},
        'annules': {'label': "Transports annulés", "sub_items": [
            (ch.label, ch.value) for ch in Transport.AnnulationChoices
        ]},
        'total_factures': {'label': 'Total facturé aux clients (CHF)'},
    }
    categ_keys = [str(tp) for tp in Trajet.Types]

    def get_transports_qs(self, start, end):
        transports = Transport.objects.filter(date__range=(start, end)).annotate(
            month=TruncMonth("date"),
        )
        region = self.date_form.cleaned_data.get('region')
        if region:
            transports = transports.avec_adresse_client().filter(
                RegionFinder.get_region_filter('adresse_client__npa', region)
            )
        return transports

    def get_stats(self, months):
        months = [m for m in months if not m.is_future()]
        start, _ = months[0].limits()
        _, end = months[-1].limits()
        base_qs = self.get_transports_qs(start, end)
        stats = base_qs.values('month').annotate(
            count_tr_al=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=False)),
            count_tr_ar=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=True)),
            count_cl=Count('client', distinct=True, filter=~Q(statut=Transport.StatutChoices.ANNULE)),
            sum_km=Sum('km', filter=~Q(defrayer_chauffeur=False), default=0),
            sum_duree=Sum('duree_eff', default=timedelta()),
        )
        stats_annul = base_qs.filter(
            statut=Transport.StatutChoices.ANNULE
        ).values("month", "motif_annulation").annotate(total=Count("motif_annulation"))

        by_month = {m: {
                'clients': 0,
                'transports': 0,
                'allers': 0,
                'allers_retours': 0,
                'annules': {ch.value: 0 for ch in Transport.AnnulationChoices},
                'km_chauffeurs': 0,
                'total_factures': 0,
                'tr_categs': {key: 0 for key in self.categ_keys},
                'km_categs': {key: 0 for key in self.categ_keys},
                'clients_emplettes': 0,
                'chauffeurs_emplettes': 0,
                'duree_emplettes': timedelta(),
        } for m in months}
        for line in stats:
            by_month[Month(line['month'].year, line['month'].month)].update({
                'clients': line['count_cl'],
                'transports': line['count_tr_al'] + 2 * line['count_tr_ar'],
                'allers': line['count_tr_al'], 'allers_retours': line['count_tr_ar'],
                'km_chauffeurs': line['sum_km'],
            })
        for line in stats_annul:
            by_month[
                Month(line["month"].year, line["month"].month)
            ]["annules"][line["motif_annulation"] or "autre"] = line["total"]
        nb_clients_total = base_qs.aggregate(
            total=Count('client', distinct=True, filter=~Q(statut=Transport.StatutChoices.ANNULE))
        )['total']

        # Transports par type de trajet (médico-thérapeutique, etc.)
        stats_categ = base_qs.annotate(
            type_traj=ArrayAgg('trajets__typ', distinct=True)
        ).values('month', 'trajets__typ').annotate(
            count_tr_al=Count(
                'pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=False),
                distinct=True
            ),
            count_tr_ar=Count(
                'pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=True),
                distinct=True
            ),
            liste_cl=ArrayAgg(
                'client_id', distinct=True, filter=(~Q(statut=Transport.StatutChoices.ANNULE)),
                default=Value([]),
            ),
            liste_ch=ArrayAgg(
                'chauffeur_id', distinct=True, filter=(~Q(statut=Transport.StatutChoices.ANNULE)),
                default=Value([]),
            ),
            sum_duree=Sum('duree_eff', default=timedelta()),
        )
        clients_emplettes = set()
        chauffeurs_emplettes = set()
        for line in stats_categ:
            mkey = Month(line['month'].year, line['month'].month)
            by_month[mkey]['tr_categs'][line['trajets__typ']] = (
                line['count_tr_al'] + 2 * line['count_tr_ar']
            )
            if line['trajets__typ'] == Trajet.Types.EMPLETTES:
                by_month[mkey]['clients_emplettes'] = len(set(line['liste_cl']))
                by_month[mkey]['chauffeurs_emplettes'] = len(set(line['liste_ch']))
                by_month[mkey]['duree_emplettes'] = line['sum_duree']
                clients_emplettes |= set(line['liste_cl'])
                chauffeurs_emplettes |= set(line['liste_ch'])

        # KMs par type de trajet
        kms_categ = base_qs.exclude(defrayer_chauffeur=False).annotate(
            month=TruncMonth("date"),
            type_traj=Subquery(Trajet.objects.filter(transport=OuterRef("pk")).values("typ")[:1])
        ).values('month', 'type_traj').annotate(
            sum_km=Sum('km', default=0),
        )
        for line in kms_categ:
            mkey = Month(line['month'].year, line['month'].month)
            by_month[mkey]['km_categs'][line['type_traj']] = line['sum_km']

        stats_factures = Facture.objects.filter(
            mois_facture__range=(start, end), annulee=False
        ).values('mois_facture').annotate(
            sum_facts=Sum('montant_total'),
        )
        for line in stats_factures:
            mkey = Month(line['mois_facture'].year, line['mois_facture'].month)
            if mkey in by_month:
                by_month[mkey]['total_factures'] = line['sum_facts']

        counter_keys = (
            list(self.stat_items.keys()) +
            [f'tr_{k}' for k in self.categ_keys] +
            [f'km_{k}' for k in self.categ_keys]
        )
        counters = self.init_counters(counter_keys, months)
        counters["annules"]["total"] = Counter({ch.value: 0 for ch in Transport.AnnulationChoices})
        for month in months:
            if month not in by_month:
                continue
            for key in self.stat_items.keys():
                month_val = by_month[month][key] or counters[key][month].__class__()
                if key == "annules":
                    counters[key][month] = month_val
                else:
                    counters[key][month] += month_val
                if key not in ['clients', 'clients_emplettes', 'chauffeurs_emplettes']:
                    if isinstance(counters[key]["total"], Counter):
                        counters[key]["total"].update(month_val)
                    else:
                        counters[key]['total'] += month_val
            for key in self.categ_keys:
                counters[f'tr_{key}'][month] += by_month[month]['tr_categs'][key]
                counters[f'tr_{key}']['total'] += by_month[month]['tr_categs'][key]
                counters[f'km_{key}'][month] += by_month[month]['km_categs'][key]
                counters[f'km_{key}']['total'] += by_month[month]['km_categs'][key]
        counters['clients']['total'] = nb_clients_total
        counters['clients_emplettes']['total'] = len(clients_emplettes)
        counters['chauffeurs_emplettes']['total'] = len(chauffeurs_emplettes)

        return {
            "stat_items": self.stat_items,
            "stats": {key: counters[key] for key in counter_keys},
        }

    def export_lines(self, context):
        months = context['months']
        yield ExpLine(['Statistique transports'] + [str(month) for month in months] + ['Total'], bold=True)
        for key, data in self.stat_items.items():
            if key == "annules":
                yield (
                    [data["label"]] +
                    [sum(context["stats"][key].get(month, {}).values()) for month in months] +
                    [context["stats"][key]["total"].total()]
                )
                for label, subkey in data["sub_items"]:
                    yield (
                        [f"  dont {label}"] +
                        [context["stats"]["annules"].get(month, {}).get(subkey, "-") for month in months] +
                        [context["stats"]["annules"]["total"].get(subkey, "-")]
                    )
            else:
                yield (
                    [data["label"]] +
                    [context["stats"][key].get(month, "-") for month in months] +
                    [context["stats"][key]["total"]]
                )
            if key == 'allers_retours':
                for label, key in [(tp.label, str(tp)) for tp in Trajet.Types]:
                    yield (
                        [f'  dont {label}'] +
                        [context['stats'][f'tr_{key}'].get(month, "-") for month in months] +
                        [context['stats'][f'tr_{key}']['total']]
                    )
            if key == 'km_chauffeurs':
                for label, key in [(tp.label, str(tp)) for tp in Trajet.Types]:
                    yield (
                        [f'  dont {label}'] +
                        [context['stats'][f'km_{key}'].get(month, "-") for month in months] +
                        [context['stats'][f'km_{key}']['total']]
                    )


class StatsAnneeForm(AnneeForm):
    def __init__(self, data=None, **kwargs):
        if not data or 'year' not in data:
            data['year'] = date.today().year
        super().__init__(data=data, **kwargs)

    @property
    def start(self):
        return date(int(self.cleaned_data['year']), 1, 1)

    @property
    def end(self):
        return date(int(self.cleaned_data['year']), 12, 31)


class StatsOFASTransportsView(StatsTransportsView):
    active = 'transports-ofas'
    template_name = 'transport/stats/ofas.html'
    form_class = StatsAnneeForm
    labels = {
        'count_cl': "Nbre de clients actifs",
        'count_chauff': "Nbre de chauffeurs actifs",
        'count_transp': "Nbre de transports effectués",
        'sum_km': "Km parcourus",
        'km_medic': "*dont Médico-thérapeutique avec contribution OFAS",
        'km_particip': "*dont Participatif-intégratif avec contribution OFAS",
        'km_autres': "*autres km (sans contribution OFAS)",
        'sum_duree': "Heures chauffeurs",
    }

    def get_transports_qs(self, start, end):
        return Transport.objects.avec_is_ofas().annotate(
            month=TruncMonth("date"),
        ).filter(
            statut__gte=Transport.StatutChoices.EFFECTUE,
            date__range=(start, end),
        )

    def get_stats(self, year):
        start = self.date_form.start
        end = self.date_form.end
        base_qs = self.get_transports_qs(start, end)
        stats_qs = base_qs.values("month").annotate(
            count_cl=Count("client", distinct=True, filter=~Q(statut=Transport.StatutChoices.ANNULE)),
            count_chauff=Count('chauffeur', distinct=True),
            count_transp=Coalesce(Sum(
                Case(When(retour=True, then=2), default=1),
                filter=~Q(statut=Transport.StatutChoices.ANNULE)
            ), 0),
            sum_km=Sum('km', filter=~Q(defrayer_chauffeur=False), default=0),
            sum_duree=Sum('duree_eff', default=timedelta()),
        ).order_by("month")
        by_month = {m: {} for m in self.get_months()}
        total_keys = [k for k in self.labels.keys() if not k.startswith("km_")]
        totals = None
        for idx, line in enumerate(stats_qs):
            mkey = Month(line["month"].year, line["month"].month)
            by_month[mkey] = line
            if idx == 0:
                totals = line.copy()
                totals["types_traj"] = {"medic": 0, "particip": 0, "autres": 0}
            else:
                for key in total_keys:
                    totals[key] += line[key] or type(totals[key])()

        kms_par_categ = base_qs.exclude(defrayer_chauffeur=False).filter(is_ofas=True).annotate(
            type_traj=Subquery(
                Trajet.objects.filter(transport=OuterRef("pk")).values("typ")[:1]
            )
        ).values("month", "type_traj").annotate(
            sum_km=Sum("km", default=0),
        )
        for line in kms_par_categ:
            mkey = Month(line["month"].year, line["month"].month)
            by_month[mkey].setdefault("types_traj", {})
            by_month[mkey]["types_traj"][line["type_traj"]] = line["sum_km"]

        # Finalisation des totaux
        for month_line in by_month.values():
            if "types_traj" not in month_line:
                continue
            month_line["types_traj"]["autres"] = (
                month_line["sum_km"] -
                month_line["types_traj"].get("medic", 0) -
                month_line["types_traj"].get("particip", 0)
            )
            totals["types_traj"]["medic"] += month_line["types_traj"].get("medic", 0)
            totals["types_traj"]["particip"] += month_line["types_traj"].get("particip", 0)
            totals["types_traj"]["autres"] += month_line["types_traj"].get("autres", 0)

        totals["count_cl"] = base_qs.aggregate(
            total=Count("client", distinct=True, filter=~Q(statut=Transport.StatutChoices.ANNULE))
        )["total"]
        totals["count_chauff"] = base_qs.aggregate(
            total=Count("chauffeur", distinct=True)
        )["total"]

        return {
            "annee": start.year,
            "labels": self.labels,
            "stats": by_month,
            "totals": totals,
        }

    def export_lines(self, context):
        months = context['months']
        yield ExpLine(['Statistique transports', context['annee']], bold=True)
        for key in context['labels'].keys():
            line = [context['labels'][key]]
            if key.startswith("km_"):
                line.extend([context["stats"][m].get("types_traj", {}).get(key[3:]) for m in months])
                line.append(context["totals"]["types_traj"][key[3:]])
            else:
                line.extend([context["stats"][m].get(key, 0) for m in months])
                line.append(context["totals"][key])
            yield line


class StatsChauffeursView(StatsMixin, TemplateView):
    template_name = 'transport/stats/chauffeurs.html'
    active = 'chauffeurs'
    stat_items = {
        'nb_chauffeurs': {
            'label': 'Nbre de chauffeurs actifs',
            'help': 'Chauffeurs ayant eu au moins un transport durant la période analysée.',
        },
        'nb_chauffeurs_dispos': {
            'label': 'Nbre de chauffeurs disponibles',
        },
        'km_chauffeurs': {
            'label': 'Km effectués',
            'help': 'Km déclarés par les chauffeurs, du départ de leur domicile '
                    'au retour à leur domicile.',
        },
        'frais_chauffeurs': {
            'label': 'Frais des chauffeurs',
            'help': "Frais de repas et autres frais divers (parking, etc.).",
        },
        'duree_chauffeurs': {'label': 'Durée effective des chauffeurs'},
        'nb_formations': {'label': 'Nbre de formations'},
        'duree_formations': {'label': 'Durée des formations'},
    }

    def get_stats(self, months):
        months = [m for m in months if not m.is_future()]
        start, _ = months[0].limits()
        _, end = months[-1].limits()
        by_month = {m: {
            'nb_chauffeurs': 0,
            'nb_chauffeurs_dispos': 0,
            'km_chauffeurs': 0,
            'frais_chauffeurs': 0,
            'duree_chauffeurs': timedelta(),
        } for m in months}
        transport_qs = Transport.objects.filter(date__range=(start, end)).annotate(
            month=TruncMonth("date"),
        )

        stats = transport_qs.values('month').annotate(
            sum_km=Sum('km', filter=~Q(defrayer_chauffeur=False), default=0),
            sum_duree=Sum('duree_eff', default=timedelta()),
        )
        for line in stats:
            by_month[Month(line['month'].year, line['month'].month)].update({
                'km_chauffeurs': line['sum_km'],
                'duree_chauffeurs': line['sum_duree'],
            })

        # Le calcul avec annotations sur des tables externes doit se faire dans une requête séparée
        # pour éviter de doubler certaines annotations.
        stats_frais = transport_qs.values('month').annotate(
            sum_frais=Sum('frais__cout', default=0),
        )
        for line in stats_frais:
            by_month[Month(line['month'].year, line['month'].month)]['frais_chauffeurs'] = line['sum_frais']

        # Nombre de chauffeurs
        chauffeurs_qs = transport_qs.exclude(chauffeur=None).values('month', 'chauffeur').annotate(Count('id'))
        chauffeur_ids = set()
        for line in chauffeurs_qs:
            by_month[Month.from_date(line['month'])]['nb_chauffeurs'] += 1
            chauffeur_ids.add(line['chauffeur'])
        nb_chauffeurs_total = len(chauffeur_ids)

        chauffeurs_dispos = Benevole.objects.par_activite(
            "transport", depuis=start, jusqua=end
        ).annotate(
            nb_formations=Count("formations", filter=Q(formations__quand__range=(start, end))),
            duree_formations=Sum("formations__duree", filter=Q(formations__quand__range=(start, end))),
        ).prefetch_related(
            Prefetch('activite_set', queryset=Activite.objects.filter(type_act__code='transport'),
                     to_attr="activites_transports")
        )

        regions = [reg for reg in RegionFinder.regions() if reg != '__toutes__']
        counter_keys = list(self.stat_items.keys()) + [f'reg_{r}' for r in regions]
        counters = self.init_counters(counter_keys, months)
        for month in months:
            if month not in by_month:
                continue
            debut_mois, fin_mois = month.limits()
            for key in self.stat_items.keys():
                if key == 'nb_chauffeurs_dispos':
                    chauff_regions = [
                        chauff.region for chauff in chauffeurs_dispos if any(
                            act.active_entre(debut_mois, fin_mois) for act in chauff.activites_transports
                        )
                    ]
                    counters[key][month] = len(chauff_regions)
                elif key.endswith("formations"):
                    continue
                counters[key][month] += by_month[month][key]
                counters[key]['total'] += by_month[month][key]
            for key in regions:
                counters[f'reg_{key}'][month] += chauff_regions.count(key)

        counters['nb_chauffeurs']['total'] = nb_chauffeurs_total
        counters['nb_chauffeurs_dispos']['total'] = len(chauffeurs_dispos)
        for chauffeur in chauffeurs_dispos:
            counters['nb_formations']['total'] += chauffeur.nb_formations
            counters['duree_formations']['total'] += chauffeur.duree_formations or timedelta(0)

        return {
            'labels': list(self.stat_items.values()),
            'stats': {key: counters[key] for key in counter_keys},
            'regions': zip(regions, [f'reg_{r}' for r in regions]),
        }

    def export_lines(self, context):
        months = context['months']
        yield ExpLine(['Statistique chauffeurs'] + [str(month) for month in months] + ['Total'], bold=True)
        for key, data in self.stat_items.items():
            yield (
                [data['label']] +
                [context['stats'][key][month] for month in months] +
                [context['stats'][key]['total']]
            )
            if key == 'nb_chauffeurs_dispos':
                for label, key in context['regions']:
                    yield (
                        [f'    {label}'] + [context['stats'][key][month] for month in months] + []
                    )
