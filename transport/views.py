import math
from collections import defaultdict
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from io import BytesIO
from itertools import chain, groupby
from operator import attrgetter
from pathlib import Path

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import RedirectURLMixin
from django.db.backends.postgresql.psycopg_any import DateRange
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.db import transaction
from django.db.models import Count, F, Max, Min, Prefetch, Q
from django.db.models.functions import TruncMonth
from django.http import FileResponse, Http404, HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import SafeString
from django.utils.text import slugify
from django.views.generic import (
    CreateView, DeleteView, DetailView, FormView, TemplateView, UpdateView, View
)

import httpx

from api.core import ApiError
from benevole.models import Benevole, Formation, LigneFrais, NoteFrais
from benevole.views import BenevoleListView
from client.models import Client, Journal, PersonaBaseQuerySet, Prestation
from client.views import (
    ClientArchiveView as ClientArchiveViewBase, ClientDetailContextMixin,
    ClientEditViewBase, ClientListViewBase
)
from common.choices import Handicaps, Services
from common.distance import ORSUnavailable, distance_real
from common.export import CSVExport, ExpLine
from common.forms import MoisForm
from common.functions import IsAVS
from common.models import DistanceFigee
from common.templatetags.common_utils import format_duree
from common.utils import (
    CRHolidays, canton_abrev, current_app, fact_policy, format_mois_an, last_day_of_month
)
from common.views import (
    BasePDFView, CreateUpdateView, FilterFormMixin, GeoAddressMixin, JournalMixin,
    ListView
)
from . import forms
from .factures import FacturePDF, RemboursementPDF
from .models import (
    Adresse, Dispo, Facture, Frais, JournalTransport, Occurrence, OccurrenceExistError,
    Preference, Refus, Trajet, Transport, TransportModel, Vehicule,
    VehiculeOccup,
)
from .pdf import TransportsPDF
from .utils import generer_notes_frais


class TransportJournalMixin(JournalMixin):
    is_create = False
    journal_add_message = "Création du transport"
    journal_edit_message = "Modification du transport: {fields}"

    def _create_instance(self, **kwargs):
        JournalTransport.objects.create(
            transport=self.object, **kwargs
        )


class AccueilTransportView(TemplateView):
    """
    Accueil pour transports. Les permissions sont gérées dans le template
    pour chaque bloc.
    """
    template_name = "transport/home.html"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "acces_vehicules": (
                Vehicule.objects.actifs().exists() and
                self.request.user.has_perm("transport.view_vehicule")
            ),
        }


class GestionTransportView(PermissionRequiredMixin, TemplateView):
    """Aperçu pour gestion transports."""
    template_name = 'transport/gestion.html'
    permission_required = 'transport.view_transport'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        Transport.check_effectues()

        transp_query = Transport.objects.exclude(
            Q(facture__isnull=False) |
            (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur__isnull=False)) |
            (Q(statut=Transport.StatutChoices.CONTROLE) & Q(statut_fact=Transport.FacturationChoices.PAS_FACTURER))
        ).order_by('statut')
        counts = {
            line['statut']: line['tot']
            for line in transp_query.values('statut').annotate(tot=Count('id'))
        }
        if Transport.StatutChoices.RAPPORTE in counts and Transport.StatutChoices.ANNULE in counts:
            counts[Transport.StatutChoices.RAPPORTE] += counts[Transport.StatutChoices.ANNULE]

        context.update({
            'tabs': [
                (stat.label, stat.value, counts.get(stat.value, 0), reverse('transports-by-status', args=[stat.value]))
                for stat in Transport.StatutChoices
                if stat.value <= Transport.StatutChoices.CONTROLE
            ],
            'current_tab': self.kwargs.get('num', 1),
        })
        return context


class TransportByStatusView(TemplateView):
    """
    Liste 'brute' de transports par statut, généralement affichée dynamiquement
    sous un onglet.
    """
    template_name = 'transport/transports_list.html'
    filter_formclass = forms.TransportFilterForm
    line_compl_map = {
        Transport.StatutChoices.SAISI: '-date,-chauffeur',
        Transport.StatutChoices.ATTRIBUE: 'confirmer',
    }

    def dispatch(self, request, *args, **kwargs):
        if not self.request.headers.get('X-Requested-With'):
            return HttpResponseRedirect(reverse('gestion-with-tab', args=[kwargs['num']]))
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.GET:
            filter_data = request.GET
            if any(request.GET.values()):
                self.request.session['transport_form_data'] = filter_data
            else:
                filter_data = None
                self.request.session.pop('transport_form_data', None)
        else:
            filter_data = self.request.session.get('transport_form_data', None)
            if (
                filter_data and self.kwargs['num'] == Transport.StatutChoices.SAISI and
                [k for k, v in filter_data.items() if v] == ['chauffeur']
            ):
                filter_data = None
        self.filter_form = self.filter_formclass(statut=self.kwargs['num'], data=filter_data)
        self.filtered_by = []
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return (
            ['transport/transports_saisis.html'] if self.kwargs['num'] == Transport.StatutChoices.SAISI
            else self.template_name
        )

    def get_queryset(self):
        if self.kwargs['num'] == Transport.StatutChoices.RAPPORTE:
            transports = Transport.objects.filter(
                Q(statut=Transport.StatutChoices.RAPPORTE) |
                (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur__isnull=True))
            )
        else:
            transports = Transport.objects.filter(statut=self.kwargs['num'], facture__isnull=True)
            if self.kwargs['num'] == Transport.StatutChoices.CONTROLE:
                transports = transports.exclude(statut_fact=Transport.FacturationChoices.PAS_FACTURER)
        limit_date_to = None
        if 'date' in self.kwargs:
            limit_date_to = datetime.strptime(self.kwargs['date'], '%Y-%m-%d').date()
            transports = transports.filter(date=limit_date_to)

        transports = transports.annotate(depart=Min('trajets__heure_depart')
        ).select_related(
            *(['client'] if self.kwargs['num'] == Transport.StatutChoices.SAISI else ['client', 'chauffeur'])
        ).prefetch_related(
            Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
            Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr'))
        ).order_by('depart')

        if self.filter_form.is_bound and self.filter_form.is_valid():
            transports = self.filter_form.filter(transports)
            self.filtered_by = [k for k, v in self.filter_form.cleaned_data.items() if v]

        if self.kwargs['num'] == Transport.StatutChoices.SAISI:
            # Ajouter dans les transports saisis les occurrences des transports répétitifs
            if self.filter_form.is_bound and self.filter_form.is_valid():
                filter_func = self.filter_form.filter_occurrences
            else:
                filter_func = None
            occs = TransportModel.toutes_occurrences(
                _date=limit_date_to, filter_func=filter_func, prefetch_trajets=True
            )
            transports = sorted(list(transports) + occs)
        return transports

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'filter_form': self.filter_form,
            'statut': self.kwargs['num'],
            'line_compl': self.line_compl_map.get(self.kwargs['num'], ''),
            'km_auto_calc': fact_policy().CALCUL_KM_AUTO,
        })
        transports = self.get_queryset()
        # Mise en forme: {<date>: [<transports>], ...}
        if self.filtered_by and self.filtered_by != ['region']:
            # Si filtre actif (autre que region), tous les transports sont affichés
            context['object_list'] = {
                dt: {'transports': list(transps), 'filled': True}
                for dt, transps in groupby(transports, key=attrgetter('date'))
            }
            for line in context['object_list'].values():
                line['len'] = len(line['transports'])
        else:
            # Sinon seule la première date contient les transports,
            # les autres seront chargés dynamiquement on cliquant sur la date du jour.
            transp_iter = groupby(transports, key=attrgetter('date'))
            try:
                dt, transps = next(transp_iter)
            except StopIteration:
                context['object_list'] = {}
            else:
                context['object_list'] = {dt: {'transports': list(transps), 'filled': True}}
                context['object_list'][dt]['len'] = len(context['object_list'][dt]['transports'])
                context['object_list'].update({
                    dt: {'len': len(list(transps)), 'transports': [], 'filled': False}
                    for dt, transps in transp_iter
                })
        return context


class TransportByDayView(TemplateView):
    template_name = 'transport/par_jour.html'
    filter_formclass = forms.RegionFilterForm

    def get(self, request, *args, **kwargs):
        self.filter_form = self.filter_formclass(data=request.GET)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jour = date(self.kwargs['year'], self.kwargs['month'], self.kwargs['day'])
        transports = Transport.objects.filter(date=jour).exclude(statut=Transport.StatutChoices.ANNULE)
        if self.filter_form.is_bound and self.filter_form.is_valid():
            transports = self.filter_form.filter_transports(transports)
        avec_chauffeurs = transports.filter(
            chauffeur__isnull=False
        ).avec_adresse_chauffeur().select_related("chauffeur").order_by("chauffeur")
        chauffeurs = {
            ch: {'transports': list(transps), 'dispos': []}
            for ch, transps in groupby(avec_chauffeurs, key=lambda t: t.chauffeur)
        }
        for ch, transps in chauffeurs.items():
            ch.localite = transps["transports"][0].adresse_chauffeur["localite"]
        # Compléter par chauffeurs avec dispos
        jour_dispos = Dispo.get_for_period(
            datetime.combine(jour, time(0, 0)), datetime.combine(jour, time(23, 59))
        )
        chauffeurs_dispos = {}
        absences = defaultdict(list)
        for dispo in jour_dispos:
            if dispo.cancelled:
                continue
            if dispo.categorie == Dispo.CategChoices.ABSENCE:
                absences[dispo.chauffeur].append(dispo)
            else:
                if dispo.chauffeur in chauffeurs:
                    chauffeurs[dispo.chauffeur]['dispos'].append(dispo)
                else:
                    if dispo.chauffeur in chauffeurs_dispos:
                        chauffeurs_dispos[dispo.chauffeur]['dispos'].append(dispo)
                    else:
                        chauffeurs_dispos[dispo.chauffeur] = {'transports': [], 'dispos': [dispo]}
        ch_dispo_queryset = Benevole.objects.filter(
            pk__in=[ch.pk for ch in chauffeurs_dispos]
        ).avec_adresse(jour)
        if self.filter_form.is_bound and self.filter_form.is_valid():
            ch_dispo_queryset = self.filter_form.filter_benevoles(ch_dispo_queryset)
        ch_avec_adr = ch_dispo_queryset.in_bulk()
        for ch in list(chauffeurs_dispos.keys()):
            if ch.pk not in ch_avec_adr:
                del chauffeurs_dispos[ch]
                continue
            ch.localite = (
                ch_avec_adr[ch.pk].adresse_active["localite"]
                if ch_avec_adr[ch.pk].adresse_active else ""
            )

        def dispo_diff_absences(dispo, absences):
            # Enlever la ou les absences du temps de la dispo
            # (possiblement en créant plusieurs sous-dispos)
            dispos = [dispo]
            for abs_ in absences:
                for disp in list(dispos):
                    if disp.debut < abs_.fin and disp.fin > abs_.debut:
                        dispos.remove(disp)
                        if abs_.debut > disp.debut:
                            dispos.append(Dispo(
                                chauffeur=disp.chauffeur, categorie=disp.categorie,
                                debut=disp.debut, fin=abs_.debut,
                            ))
                        if disp.fin > abs_.fin:
                            dispos.append(Dispo(
                                chauffeur=disp.chauffeur, categorie=disp.categorie,
                                debut=abs_.fin, fin=disp.fin,
                            ))
            return dispos

        def flatten(xss):
            return [x for xs in xss for x in xs]

        # Filtrer les dispos selon les absences
        for chauff, abslist in absences.items():
            if chauff in chauffeurs:
                chauffeurs[chauff]["dispos"] = flatten([
                    dispo_diff_absences(dispo, abslist) for dispo in chauffeurs[chauff]["dispos"]
                ])
            if chauff in chauffeurs_dispos:
                chauffeurs_dispos[chauff]["dispos"] = flatten([
                    dispo_diff_absences(dispo, abslist) for dispo in chauffeurs_dispos[chauff]["dispos"]
                ])
                if not chauffeurs_dispos[chauff]['dispos']:
                    del chauffeurs_dispos[chauff]
        context.update({
            'filter_form': self.filter_form,
            'jour': jour,
            'jours_semaine': [date.fromisocalendar(*jour.isocalendar()[:2], d) for d in range(1, 8)],
            'non_attribues': transports.filter(chauffeur__isnull=True, chauffeur_externe=False),
            'chauffeurs': chauffeurs,
            'chauffeurs_dispos': chauffeurs_dispos,
        })
        return context


class TransportSaisieView(TransportJournalMixin, CreateView):
    template_name = 'transport/saisie.html'
    form_class = forms.SaisieTransportForm
    is_create = True

    def get_initial(self):
        initial = {**super().get_initial(), 'typ': Trajet.Types.MEDIC}
        if self.request.GET.get('client'):
            try:
                client = Client.objects.get(pk=self.request.GET.get('client'))
            except Client.DoesNotExist:
                pass
            else:
                initial['client'] = client.pk
                initial['client_select'] = str(client)
        return initial

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'libelle_mauvais_payeur': settings.LIBELLE_MAUVAIS_PAYEUR,
        }

    def get_success_url(self):
        if Handicaps.CHAISE in (self.object.client.handicaps or []):
            # Rediriger immédiatement vers attribution si client en chaise (#436)
            return reverse("transport-attrib", args=[self.object.pk])
        return reverse('transport-client-detail', args=[self.object.client_id])

    def form_valid(self, form):
        response = super().form_valid(form)
        if Services.TRANSPORT not in self.object.client.prestations_actuelles(as_services=True):
            Prestation.objects.create(
                client=self.object.client, service=Services.TRANSPORT, duree=(date.today(), None)
            )
        self.journalize(form)
        return response


class TransportCalculerDepart(View):
    def post(self, request, *args, **kwargs):
        try:
            date_transport = date.fromisoformat(request.POST['date'])
            heure_transport = datetime.strptime(request.POST['heure_rdv'], '%H:%M').time()
            origine_pk = request.POST['origine']
            dest_pks = request.POST.getlist('destination')
            client_pk = request.POST['client']
        except Exception as err:
            return JsonResponse(
                {'result': 'error', 'reason': f"Une erreur s'est produite {err}"}
            )

        heure_rdv = datetime.combine(
            date_transport, heure_transport, tzinfo=timezone.get_current_timezone()
        )
        orig = (
            get_object_or_404(Adresse, pk=origine_pk)
            if (origine_pk and origine_pk not in {'null', 'None'}) else None
        )
        client = get_object_or_404(Client, pk=client_pk) if client_pk else None
        if orig:
            orig_geo = orig.empl_geo
        else:
            orig_geo = client.adresse(date_transport).empl_geo
        dests_geo = []
        for value in dest_pks:
            # Toutes les destinations jusqu'à la destination principale
            if value == "-1" and client:
                dests_geo.append(client.adresse(date_transport).empl_geo)
            elif value:
                dests_geo.append(get_object_or_404(Adresse, pk=value).empl_geo)
        if not orig_geo or not dests_geo:
            return JsonResponse({'result': 'error', 'reason': "Désolé, certaines infos ne sont pas géolocalisées."})
        cur_orig = orig_geo
        duree_transport = timedelta(0)
        try:
            for dest in dests_geo:
                if cur_orig == dest:
                    return JsonResponse(
                        {'result': 'error', 'reason': "Origine et destination doivent être différentes."}
                    )
                duree_transport += timedelta(seconds=distance_real(cur_orig, dest)['duration'])
                cur_orig = dest
        except ORSUnavailable:
            return JsonResponse({
                'result': 'error',
                'reason': (
                    "Le service Openrouteservice n’est actuellement pas disponible "
                    "pour calculer automatiquement l’heure de départ"
                ),
            })
        # Ajouter 10% de marge à la durée calculée, mais au minimum 10 min.
        # Pas de duree calculée au-dessous de 15 min.
        depart = heure_rdv - max(
            duree_transport + max(duree_transport * 0.1, timedelta(minutes=10)), timedelta(minutes=15)
        )
        round_min = round(depart.minute / 5) * 5
        if round_min == 60:
            depart = (depart + timedelta(hours=1)).replace(minute=0)
        else:
            depart = depart.replace(minute=round_min)
        return JsonResponse({
            'result': 'OK',
            'depart': depart.strftime('%H:%M'),
            'duree_trajet': format_duree(duree_transport),
        })


class FromModeleMixin:
    """
    Get Transport either from plain id, or from a generated instance from
    TransportModel if urlpattern has moddate.
    """
    def get_object(self):
        if 'moddate' not in self.kwargs:
            return get_object_or_404(Transport, pk=self.kwargs['pk'])
        else:
            pk = self.kwargs.get('transport_pk') or self.kwargs['pk']
            modele = get_object_or_404(TransportModel, pk=pk)
            dt = datetime.strptime(str(self.kwargs['moddate']), '%Y%m%d')
            occ = modele.get_occurrence(dt)
            if not occ:
                raise Http404(
                    f"Aucune occurrence de ce transport trouvée pour le {dt.strftime('%d.%m.%Y')}"
                )
            return occ


class TransportEditView(
    PermissionRequiredMixin, TransportJournalMixin, RedirectURLMixin, FromModeleMixin, UpdateView
):
    template_name = 'transport/saisie.html'
    permission_required = "transport.change_transport"
    model = Transport
    form_class = forms.SaisieTransportForm
    success_url = reverse_lazy('gestion')
    next_page = success_url

    def get_initial(self):
        premier_trajet = self.object.trajets_tries[0]
        return {
            **super().get_initial(),
            'origine_adr': premier_trajet.origine_adr,
            'heure_depart': self.object.heure_depart.time(),
            'typ': premier_trajet.typ,
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'depart_retour': (
                self.object.heure_depart_retour.time()
                if self.object.retour and self.object.heure_depart_retour else '--:--'
            ),
            'arrivee_retour': (
                self.object.heure_arrivee.time()
                if self.object.retour else '--:--'
            ),
        }

    def form_valid(self, form):
        with transaction.atomic():
            if 'retour' in form.changed_data and form.cleaned_data['retour'] is False and form.instance.temps_attente:
                # Un transport aller-retour modifié en aller simple ne peut plus contenir de temps d'attente
                form.instance.temps_attente = None
            response = super().form_valid(form)
            self.journalize(form)
            if form.instance.modele and 'date' in form.changed_data:
                # Annuler l'occurrence à la date originale
                occ = form.instance.modele.get_occurrence(form.initial['date'])
                if occ:
                    occ.statut = Transport.StatutChoices.ANNULE
                    occ.motif_annulation = Transport.AnnulationChoices.AUTRE
                    occ.raison_annulation = "Report de cette occurrence à une autre date."
                    occ.date_annulation = timezone.now()
                    occ.statut_fact = Transport.FacturationChoices.PAS_FACTURER
                    occ.defrayer_chauffeur = False
                    occ.concretiser()
        return response


class TransportModelEditView(UpdateView):
    template_name = 'transport/modele_edit.html'
    model = TransportModel
    form_class = forms.TransportModelForm
    success_url = reverse_lazy('gestion')

    def get_initial(self):
        initial = super().get_initial()
        if self.object.regle:
            if self.object.regle.nom == 'Chaque jour, du lundi au vendredi':
                initial['repetition'] = 'DAILY-open'
            else:
                initial['repetition'] = self.object.regle.frequence
            if self.object.fin_recurrence:
                initial['fin_recurrence'] = self.object.fin_recurrence.date()
        return initial

    def get_context_data(self, **kwargs):
        next_tr = next(
            iter(self.object.toutes_occurrences(client=self.object.client, prefetch_trajets=False)),
            None
        )
        first_instance = self.object.transports.order_by('heure_rdv').first()
        adr_origine = first_instance.adresse_depart()
        adr_dest = first_instance.destination
        context = {
            **super().get_context_data(**kwargs),
            'origine': {
                'texte': str(adr_origine), 'pk': adr_origine.pk if isinstance(adr_origine, Adresse) else None,
            },
            'destination': {
                'texte': str(adr_dest), 'pk': adr_dest.pk if isinstance(adr_dest, Adresse) else None,
            },
            'concrete_future_occs': self.object.transports.filter(date__gte=date.today()),
            'next_date': next_tr.date if next_tr else None,
        }
        return context

    def form_valid(self, form):
        # Si un véhicule est défini, vérifier d'éventuels conflits pour les 6 prochains mois
        if vhc := form.cleaned_data.get("vehicule"):
            def overlaps(occ, heures):
                return not(occ.debut >= heures[1] or occ.fin <= heures[1])

            prochaines_occ = form.instance.toutes_occurrences(for_n_days=180)
            trajets_meme_vhc = [
                (trajet.heure_depart, trajet.heure_arrivee) for trajet in Trajet.objects.filter(
                    transport__vehicule=vhc, transport__date__gte=date.today()
                ).select_related("transport")
            ]
            for occ in prochaines_occ:
                does_overlap = any(overlaps(occ, heures) for heures in trajets_meme_vhc)
                if does_overlap:
                    dt = occ.debut.date().strftime("%d.%m.%Y")
                    messages.warning(
                        self.request,
                        f"Attention, il y a un conflit potentiel pour l’occurrence du {dt} de ce modèle de transport."
                    )
        return super().form_valid(form)


class TransportCancelView(TransportJournalMixin, FromModeleMixin, UpdateView):
    model = Transport
    form_class = forms.CancelForm
    template_name = 'transport/transport_cancel_form.html'
    success_url = reverse_lazy('gestion')
    journal_edit_message = "Annulation du transport"

    def get_success_url(self):
        return reverse('transport-client-detail', args=[self.object.client.pk])

    def form_valid(self, form):
        uncancel = (
            self.object.est_annule() and
            'annule' in form.fields and not form.cleaned_data.get('annule')
        )
        with transaction.atomic():
            super().form_valid(form)
            if uncancel:
                if self.object.est_passe:
                    self.object.statut = Transport.StatutChoices.EFFECTUE
                else:
                    if self.object.chauffeur:
                        self.object.statut = Transport.StatutChoices.ATTRIBUE
                    else:
                        self.object.statut = Transport.StatutChoices.SAISI
                self.object.date_annulation = None
                self.object.motif_annulation = ""
                self.object.raison_annulation = ""
                self.object.statut_fact = Transport.FacturationChoices.FACTURER
                self.object.defrayer_chauffeur = None
                msg = "Le transport qui était annulé a été réactivé"
                self.object.save()
                JournalTransport.objects.create(
                    transport=self.object, description=msg,
                    quand=timezone.now(), qui=self.request.user
                )
            else:
                self.object.annuler()
                msg = "Le transport a bien été annulé"
                self.journalize(form)
        messages.success(self.request, msg)
        return JsonResponse({'result': 'OK', 'reload': self.get_success_url()})


class TransportCalcDistChauffeur(FromModeleMixin, View):
    """
    Calcul de la distance (entre localités) entre transport et chauffeur (pour page attribution).
    """
    def post(self, request, *args, **kwargs):
        transport = self.get_object()
        chauffeur = get_object_or_404(
            Benevole.objects.avec_adresse(transport.date), pk=self.kwargs["chauff_pk"]
        )
        chauffeur.priorite_dispo = 1
        transport.calculer_distances_chauffeurs([chauffeur], force_calc=True)
        dist, typ = chauffeur.distance_tr
        return JsonResponse({"result": "OK", "distance": dist, "type": typ})


class TransportAutoAttribView(View):
    def post(self, request, *args, **kwargs):
        dt = date(kwargs['year'], kwargs['month'], kwargs['day'])
        transports = Transport.objects.filter(date=dt, statut=Transport.StatutChoices.SAISI)
        model_occs = TransportModel.toutes_occurrences(_date=dt, prefetch_trajets=True)
        successes = failures = 0
        for transp in chain(transports, model_occs):
            chauffeurs = [ch for ch in transp.chauffeurs_potentiels() if ch.priorite_dispo == 1]
            if not chauffeurs:
                failures += 1
            else:
                attrib_form = forms.AttributionForm(instance=transp, data={'chauffeur': chauffeurs[0]})
                if attrib_form.is_valid():
                    transport = attrib_form.save()
                    JournalTransport.objects.create(
                        transport=transport,
                        description=f"Attribution automatique d’un chauffeur: {transp.chauffeur}",
                        quand=timezone.now(), qui=request.user
                    )
                    successes += 1
                else:
                    failures += 1
        messages.success(
            request,
            f"{successes} transports ont été attribués pour le {dt.strftime('%d.%m.%Y')}. "
            f"{failures} transports n’ont pas pu être attribués."
        )
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))


class TransportAttribView(PermissionRequiredMixin, TransportJournalMixin, FromModeleMixin, UpdateView):
    model = Transport
    permission_required = "transport.change_transport"
    form_class = forms.AttributionForm
    template_name = 'transport/attribution.html'
    success_url = reverse_lazy('gestion')
    journal_edit_message = "Attribution d’un chauffeur: {obj.chauffeur}"

    def get_initial(self):
        initial = super().get_initial()
        initial['page_origine'] = self.request.headers.get('Referer', reverse('gestion'))
        return initial

    def get_context_data(self, **kwargs):
        client = self.object.client
        return {
            **super().get_context_data(**kwargs),
            "chaise": Handicaps.CHAISE in (client.handicaps or []),
            "autres_handis": client.get_handicaps_display(exclude=Handicaps.CHAISE),
            "chauffeurs": self.object.chauffeurs_potentiels(),
            "vehicules": self.object.vehicules_potentiels(),
            "transports_prec": client.transports.filter(
                chauffeur__isnull=False).exclude(statut=Transport.StatutChoices.ANNULE
            ).order_by('-heure_rdv')[:6],
        }

    def form_valid(self, form):
        try:
            with transaction.atomic():
                try:
                    super().form_valid(form)
                except OccurrenceExistError:
                    raise SuspiciousOperation("Le formulaire a déjà été envoyé pour cette occurrence.")
                self.journalize(form)
        except SuspiciousOperation as err:
            messages.error(self.request, str(err))
        return HttpResponseRedirect(form.cleaned_data['page_origine'] or reverse('gestion'))


class TransportAttribRemoveView(View):
    model = Transport
    journal_edit_message = "Suppression du chauffeur"

    def post(self, request, *args, **kwargs):
        transport = get_object_or_404(self.model, pk=kwargs['pk'])
        for field_name in [
            "chauffeur", "chauff_aller_dist", "chauff_retour_dist", "chauff_aller_duree", "chauff_retour_duree",
        ]:
            setattr(transport, field_name, None)
        transport.chauffeur_vu = False
        transport.statut = Transport.StatutChoices.SAISI
        with transaction.atomic():
            transport.save()
            JournalTransport.objects.create(
                transport=transport, description=self.journal_edit_message, quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(reverse('gestion'))


class TransportRefuseView(FromModeleMixin, View):
    def post(self, request, *args, **kwargs):
        transport = self.get_object()
        chauffeur = get_object_or_404(Benevole, pk=request.POST.get('chauffeur'))
        redirect = None
        with transaction.atomic():
            if not transport.pk:
                transport.concretiser()
                redirect = reverse('transport-attrib', args=[transport.pk])
            Refus.objects.create(transport=transport, chauffeur=chauffeur)
            JournalTransport.objects.create(
                transport=transport, description=f"{chauffeur} a refusé ce transport",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'redirect': redirect})


class TransportConfirmView(TransportJournalMixin, FormView):
    form_class = forms.ConfirmationForm
    journal_edit_message = "Confirmation du transport"

    def form_invalid(self, form):
        messages.error(self.request, "Vous n’avez pas sélectionné de transport")
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))

    def form_valid(self, form):
        for transport in form.cleaned_data['selection']:
            transport.statut = transport.StatutChoices.CONFIRME
            transport.save()
            # Simulate some variables for journalize
            JournalTransport.objects.create(
                transport=transport, description=self.journal_edit_message, quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.CONFIRME]))


class TransportRapportView(PermissionRequiredMixin, TransportJournalMixin, RedirectURLMixin, UpdateView):
    model = Transport
    permission_required = 'transport.view_transport'
    form_class = forms.RapportForm
    base_template = 'transport/base.html'
    template_name = 'transport/rapport.html'
    next_page = reverse_lazy('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE])
    par_chauffeur = False
    journal_add_message = "Rapport initial du transport"
    journal_edit_message = "Modification du rapport: {fields}"
    statut_fact_annulation = Transport.FacturationChoices.FACTURER_ANNUL

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        compl = ", temps d'attente compris" if self.object and self.object.retour else ""
        return {
            **context,
            "num_filled_frais_forms": len([frm for frm in context["form"].formset.forms if frm.is_filled]),
            'help_duree': (
                "Durée totale du transport compté depuis le départ de votre domicile "
                f"jusqu’à votre retour à domicile{compl}."
            ),
        }

    def form_valid(self, form):
        self.is_create = form.initial['km'] is None  # So add_message is used
        with transaction.atomic():
            transport = form.save()
            if self.is_create and transport.est_annule():
                add_message = "Rapport initial du transport (marqué comme annulé)"
                transport.statut_fact = self.statut_fact_annulation
                transport.save(update_fields=["statut_fact"])
            else:
                add_message = self.journal_add_message
            self.journalize(form, changed_values=True, add_message=add_message)
        return HttpResponseRedirect(self.get_success_url())


class TransportCheckView(TransportJournalMixin, View):
    def post(self, request, *args, **kwargs):
        transport = get_object_or_404(Transport, pk=self.kwargs['pk'])
        transport.statut = transport.StatutChoices.CONTROLE
        transport.save()
        JournalTransport.objects.create(
            transport=transport, description="Validation finale du transport", quand=timezone.now(), qui=request.user
        )
        return JsonResponse({'result': 'OK'})


class FinancesHomeView(PermissionRequiredMixin, TemplateView):
    permission_required = 'transport.gestion_finances'
    template_name = 'transport/finances.html'

    def get(self, request, *args, **kwargs):
        default_month = date.today().replace(day=1) - timedelta(days=10)
        form_data = {
            'year': request.GET.get('year', str(default_month.year)),
            'month': request.GET.get('month', str(default_month.month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'year': self.un_du_mois.year,
            'month': self.un_du_mois.month,
            'month_str': format_mois_an(self.un_du_mois),
            'date_form': self.form,
            'a_facturer': LotAFacturer(self.un_du_mois, billed=False).exists(),
            'non_transmises': Facture.non_transmises().count(),
        }


class TransportFacturerMixin(PermissionRequiredMixin, TransportJournalMixin):
    permission_required = 'transport.gestion_finances'
    journal_edit_message = "Facturation du transport"
    # Génération des factures PDF désactivée (à supprimer?)
    creer_pdf = False

    def get_initial(self):
        return {**super().get_initial(), 'date_facture': date.today()}


class TransportFacturerMoisView(TransportFacturerMixin, FormView):
    """Facturation mensuelle des transports aux clients."""
    form_class = forms.FacturerMoisForm
    template_name = 'transport/facturer_form.html'

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'form_classes': 'unmonitor normal_submit',
        }
        un_du_mois = date.today().replace(day=1)
        context.update({
            # Contrôler si transports non contrôlés
            "non_controles": Transport.objects.filter(
                date__lt=un_du_mois,
                statut__in=[Transport.StatutChoices.EFFECTUE, Transport.StatutChoices.RAPPORTE]
            ).count(),
            # Réservations sans kms
            "occups_sans_km": VehiculeOccup.objects.filter(
                duree__startswith__date__lt=un_du_mois, client__isnull=False, kms__isnull=True
            ).count(),
        })
        return context

    def form_valid(self, form):
        from pypdf import PdfMerger

        date_facture = form.cleaned_data['date_facture']
        merger = PdfMerger(strict=False) if self.creer_pdf else None
        un_du_mois = date(self.kwargs['year'], self.kwargs['month'], 1)

        a_facturer = LotAFacturer(un_du_mois, client=None, billed=False)
        if not a_facturer.exists():
            messages.warning(self.request, "Aucun transport à facturer pour ce mois")
            return HttpResponseRedirect(
                reverse('gestion-with-tab', args=[Transport.StatutChoices.CONTROLE])
            )
        with transaction.atomic():
            a_facturer.facturer(date_facture, ecraser=False, merger=merger)
        if self.creer_pdf:
            # Renvoie un gros PDF avec toutes les factures
            temp = BytesIO()
            merger.write(temp)
            merger.close()
            temp.seek(0)
            response = FileResponse(
                temp, as_attachment=True,
                filename=f'factures_transports_{un_du_mois.month}_{un_du_mois.year}.pdf'
            )
        else:
            messages.success(self.request, f"Les factures ont été générées pour {format_mois_an(un_du_mois)}")
            response = HttpResponseRedirect(reverse('finances'))
        return response


class TransportRefacturerView(TransportFacturerMixin, FormView):
    form_class = forms.RefacturerForm
    template_name = 'general_edit.html'

    def dispatch(self, *args, **kwargs):
        self.facture = get_object_or_404(Facture, pk=self.kwargs['fact_pk'])
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'facture': self.facture,
        }

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'form_classes': 'unmonitor normal_submit',
        }
        return context

    def form_valid(self, form):
        mois = self.facture.mois_facture
        client = self.facture.client
        date_facture = form.cleaned_data['date_facture']

        if self.facture.exporte is not None:
            # Déjà transmise à la compta: annuler et év. refacturer
            try:
                self.facture.annuler(raison=form.cleaned_data['raison'])
            except ApiError as err:
                messages.error(self.request, str(err))
                return HttpResponseRedirect(
                    reverse('client-transports-factures', args=[self.facture.client_id])
                )
            a_facturer = LotAFacturer(mois, client=client)
            if a_facturer.exists():
                a_facturer.facturer(date_facture)
                msg = "La facture a été annulée et une nouvelle facture a été créée."
            else:
                msg = (
                    "La facture a été annulée, sans refaire d’autre facture puisqu'il "
                    "n'y a plus de transports à facturer."
                )
        else:
            # Pas encore transmise à la compta: effacer et év. refacturer
            self.facture.delete()
            a_facturer = LotAFacturer(mois, client=client)
            if a_facturer.exists():
                a_facturer.facturer(date_facture, ecraser=True)
                msg = "La facture a été annulée et une nouvelle facture a été créée."
            else:
                msg = (
                    "La facture a été effacée, sans refaire d’autre facture puisqu'il "
                    "n'y a plus de transports à facturer."
                )
                    
        messages.success(self.request, msg)
        return HttpResponseRedirect(
            reverse('client-transports-factures', args=[client.pk])
        )


class FacturesDuMois(PermissionRequiredMixin, View):
    """Joindre toutes les factures du mois indiqué dans un gros PDF."""
    permission_required = 'transport.gestion_finances'

    def get(self, request, *args, **kwargs):
        from pypdf import PdfWriter

        mois = date(kwargs['year'], kwargs['month'], 1)
        merger = PdfWriter()
        for fact in Facture.objects.filter(mois_facture=mois, annulee=False):
            merger.append(fact.fichier_pdf.path)
        temp = BytesIO()
        merger.write(temp)
        merger.close()
        temp.seek(0)
        return FileResponse(
            temp, as_attachment=True,
            filename=f'factures_transports_{mois.month}_{mois.year}.pdf'
        )


class FactureListView(PermissionRequiredMixin, ListView):
    permission_required = 'transport.gestion_finances'
    paginate_by = 25
    # FIXME: could also be the base view for ClientListeFacturesView


class FacturesNonTransmisesView(FactureListView):
    template_name = 'transport/factures.html'

    def get_queryset(self):
        return Facture.non_transmises().order_by('-date_facture', 'client__persona__nom')
        #when filter mixin is added: return super().get_queryset(base_qs=Facture.non_transmises())

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'api_export': settings.FACTURES_TRANSMISSION_URL,
            'title': 'Factures non transmises à la compta',
        }


class ComptaExportView(PermissionRequiredMixin, View):
    """Exporter les factures pour tous les clients du mois en paramètre."""
    permission_required = 'transport.gestion_finances'
    headers = [
        'DocumentCode', 'Client', 'ID Client', 'Domicile', 'Statut AVS',
        'No facture', 'Mois facturé', 'Date de facture', 'Facturé à',
        'Montant total', 'Nbre transports', 'Repas', 'Parking', "Unités d'attente",
    ]

    def post(self, request, *args, **kwargs):
        try:
            export, filename = self.export_as_csv()
        except ValueError as err:
            messages.error(request, str(err))
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', ''))
        return export.get_http_response(filename)

    def export_as_csv(self):
        un_du_mois_prec = date(year=self.kwargs['year'], month=self.kwargs['month'], day=1)
        der_du_mois = last_day_of_month(un_du_mois_prec)
        factures = Facture.objects.annotate(
            adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                der_du_mois, outer_field_name='client__persona_id'
            ),
            is_avs_debut=IsAVS(
                un_du_mois_prec, F("client__tarif_avs_des"), F("client__persona__date_naissance"),
                F("client__persona__genre")
            ),
            is_avs_fin=IsAVS(
                der_du_mois, F("client__tarif_avs_des"), F("client__persona__date_naissance"),
                F("client__persona__genre")
            ),
        ).filter(
            mois_facture=un_du_mois_prec, annulee=False
        ).select_related("client", "autre_debiteur").order_by(
            "client__persona__nom", "client__persona__prenom"
        )
        export = CSVExport(delimiter=';')
        export.write_line(ExpLine(self.headers, bold=True))
        for facture in factures:
            fact_code = 'NC' if facture.annulee else 'FA'
            transports = facture.get_transports().prefetch_related("frais")
            frais_repas = sum([
                sum([fr.cout for fr in tr.frais.all() if fr.typ == Frais.TypeFrais.REPAS])
                for tr in transports
            ])
            frais_parking = sum([
                sum([fr.cout for fr in tr.frais.all() if fr.typ == Frais.TypeFrais.PARKING])
                for tr in transports
            ])
            unites_attente = sum([tr.cout_attente()[0] for tr in transports])
            npa_loc = (
                f"{facture.adresse_client['npa']} {facture.adresse_client['localite']}"
                if facture.adresse_client else ""
            )
            export.write_line([
                fact_code, f'{facture.client.nom.upper()} {facture.client.prenom}',
                facture.client.pk,
                npa_loc,
                "Partiel" if (facture.is_avs_debut != facture.is_avs_fin) else (
                    "Oui" if facture.is_avs_debut else "Non"
                ), facture.no,
                der_du_mois.strftime('%d.%m.%Y'), facture.date_facture.strftime('%d.%m.%Y'),
                facture.autre_debiteur.get_full_name() if facture.autre_debiteur else '',
                facture.montant_total, facture.nb_transp, frais_repas, frais_parking,
                unites_attente,
            ])
        file_name = f'transports_{un_du_mois_prec.year}_{un_du_mois_prec.month:02}_export.csv'
        return export, file_name


class DefraiementView(PermissionRequiredMixin, ListView):
    template_name = 'transport/defraiements.html'
    permission_required = 'transport.gestion_finances'

    def get(self, request, *args, **kwargs):
        form_data = {
            'year': request.GET.get('year', str(date.today().year)),
            'month': request.GET.get('month', str(date.today().month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        self.calculateur = fact_policy().calculateur_frais(self.un_du_mois)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        # Mélange possible entre notes de frais calculées dynamiquement et
        # notes de frais enregistrées (benevole.NoteFrais).
        notes_existantes = {
            nt.benevole_id: nt for nt in NoteFrais.objects.filter(
                service='transport', mois=self.un_du_mois
            ).prefetch_related(
                Prefetch('lignes', queryset=LigneFrais.objects.all().select_related('libelle'))
            )
        }
        # TODO: si plus de x mois, ne prendre en compte que les notes existantes
        frais_dynamiques = self.calculateur.frais_par_benevole()
        chauffeurs = sorted(frais_dynamiques.keys(), key=attrgetter('nom'))
        liste_notes = []
        for chauff in chauffeurs:
            frais_data = frais_dynamiques[chauff]
            note = notes_existantes.get(chauff.pk)
            if note:
                lignes = note.lignes.all()
            else:
                lignes = self.calculateur.lignes_depuis_data(chauff, frais_data)

            liste_notes.append({
                **self.calculateur.get_note_frais_values(lignes),
                'benevole': chauff,
                'date_export': note.date_export if note else None,
            })
        return liste_notes

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        note_columns = self.calculateur.get_note_frais_columns()
        totaux = {key: Decimal('0') for key in dict(note_columns).values()}
        non_transmises = len([note for note in context['object_list'] if not note['date_export']])
        for line in context['object_list']:
            for key in totaux.keys():
                totaux[key] += line[key]
        context.update({
            'date_form': self.form,
            'note_columns': note_columns,
            'totaux': totaux,
            'mois_passe': (self.un_du_mois + timedelta(days=32)).replace(day=1) <= date.today(),
            'non_transmises': non_transmises,
        })
        if settings.NOTESFRAIS_TRANSMISSION_URLNAME:
            context['transmission_url'] = reverse(
                settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[self.un_du_mois.year, self.un_du_mois.month]
            )
        return context


class ChauffeursDefraiementExportView(PermissionRequiredMixin, View):
    permission_required = 'transport.gestion_finances'

    def post(self, request, *args, **kwargs):
        un_du_mois = date(year=kwargs['year'], month=kwargs['month'], day=1)

        if not NoteFrais.objects.filter(service='transport', mois=un_du_mois).exists():
            generer_notes_frais(un_du_mois)

        liste_notes = NoteFrais.objects.filter(
            service='transport', mois=un_du_mois
        ).prefetch_related('lignes').order_by('benevole__persona__nom', 'benevole__persona__prenom')

        num_lines = len(liste_notes)
        if not num_lines:
            messages.error(request, "Aucun frais n’a été enregistré pour ce mois")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('finances')))

        export = fact_policy().export_frais_chauffeurs(liste_notes, un_du_mois)
        return export.get_http_response(f'defraiements_{un_du_mois.year}_{un_du_mois.month:02}.xlsx')


class TrajetsDetailView(TemplateView):
    template_name = 'transport/trajet.html'

    def get_context_data(self, **kwargs):
        transport = get_object_or_404(Transport, pk=self.kwargs['pk'])
        trajets = []
        for trajet in transport.trajets_tries:
            route = distance_real(
                trajet.origine.empl_geo, trajet.destination.empl_geo, with_geom=True, figee=False
            )
            trajets.append({
                'trajet': trajet,
                'route': route,  # line in 'linestring' key
                'figee': DistanceFigee.get_from_coords(
                    trajet.origine.empl_geo, trajet.destination.empl_geo
                ),
            })
        return {
            **super().get_context_data(**kwargs),
            'transport': transport,
            'trajets': trajets,
        }


class TrajetSetDistanceView(View):
    fixed = False

    def post(self, request, *args, **kwargs):
        trajet = get_object_or_404(Trajet, pk=self.kwargs['pk'])
        km_str = request.POST.get('distance_km')
        if not km_str:
            messages.error(request, "Vous devez indiquer une distance en km.")
            return HttpResponseRedirect(reverse('trajets-details', args=[trajet.transport.pk]))
        try:
            km = Decimal(km_str)
        except TypeError:
            messages.error(request, "Impossible de considérer «{km}» comme une distance en km.")
        else:
            trajet.dist_calc = int(km * 1000)
            with transaction.atomic():
                trajet.save()
                JournalTransport.objects.create(
                    transport=trajet.transport, description=f"Forcer un trajet à {km}km",
                    quand=timezone.now(), qui=request.user,
                )
                if self.fixed:
                    dist, created = DistanceFigee.objects.get_or_create(
                        empl_geo_dep=trajet.origine.empl_geo,
                        empl_geo_arr=trajet.destination.empl_geo,
                        defaults={'distance': km},
                    )
                    if not created:
                        dist.distance = km
                        dist.save()
                    trajet.remove_cached_distance()

        return HttpResponseRedirect(reverse('trajets-details', args=[trajet.transport.pk]))


class TrajetRemoveDistanceView(View):
    def post(self, request, *args, **kwargs):
        trajet = get_object_or_404(Trajet, pk=self.kwargs['pk'])
        fixed = get_object_or_404(DistanceFigee, pk=self.request.POST.get('fixed_pk'))
        fixed.delete()
        return HttpResponseRedirect(reverse('trajets-details', args=[trajet.transport.pk]))


class TrajetAPlusieursView(FormView):
    template_name = 'transport/trajet_a_plusieurs.html'
    form_class = forms.TrajetAPlusieursForm

    def dispatch(self, *args, **kwargs):
        self.trajet = get_object_or_404(Trajet.objects.select_related('transport'), pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'trajet': self.trajet}

    def form_valid(self, form):
        form.save()
        return JsonResponse({
            'result': 'OK',
            'reload': 'page',
        })


class ClientListView(ClientListViewBase):
    template_name = 'transport/client_list.html'
    filter_formclass = forms.ClientFilterForm
    return_all_if_unbound = False
    client_types = ['transport']

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "transport/base.html",
            'home_url': reverse('home'),
            'client_url_name': 'transport-client-detail',
        }


class ClientEditView(ClientDetailContextMixin, ClientEditViewBase):
    template_name = 'transport/client_edit.html'
    form_class = forms.ClientEditForm
    client_type = 'transport'
    tab_id = 'edition'

    def get_success_url(self):
        if self.is_create:
            return reverse('client-edit', args=[self.object.pk])
        return reverse('transport-client-detail', args=[self.object.pk])

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "transport/base.html",
        }

    def form_valid(self, form):
        with transaction.atomic():
            response = super().form_valid(form)
            if 'date_deces' in form.changed_data:
                self.object.archiver(self.request.user, 'transport')
        return response


class ClientArchiveView(ClientArchiveViewBase):
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        # Si transports futurs ou modeles actifs, les annuler
        annules = 0
        for transp_futur in self.client.transports.filter(date__gte=date.today()):
            transp_futur.annuler(
                raison="Archivage du client", statut_fact=Transport.FacturationChoices.PAS_FACTURER
            )
            annules += 1
        termines = 0
        for model in self.client.models.filter(
            Q(fin_recurrence__isnull=True) | Q(fin_recurrence__date__gt=date.today())
        ):
            model.fin_recurrence = timezone.now()
            model.save()
            termines += 1
        msgs = []
        if annules > 1:
            msgs.append(f"{annules} transports planifiés ont été annulés")
        elif annules > 0:
            msgs.append(f"{annules} transport planifié a été annulé")
        if termines > 1:
            msgs.append(f"{termines} mandats réguliers ont été marqués comme terminés")
        elif termines > 0:
            msgs.append(f"{termines} mandats réguliers ont été marqués comme terminés")
        if msgs:
            messages.success(request, " et ".join(msgs))
        return response


class ClientDetailView(ClientDetailContextMixin, TemplateView):
    template_name = 'transport/client_detail.html'

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prochains = self.client.transports.filter(date__gte=date.today())
        occs = TransportModel.toutes_occurrences(client=self.client, prefetch_trajets=False)
        transports = sorted(list(prochains) + occs)
        vhc_occups = self.client.vehiculeoccup_set.filter(
            duree__endswith__date__gt=timezone.now()
        ).order_by("duree__startswith")
        if vhc_occups:
            transports = merge_occups(transports, vhc_occups)
        context.update({
            'client': self.client,
            'adresse': self.client.adresse(),
            'prochains_transports': transports,
            'preferences': self.client.preferences.filter(
                chauffeur__archive_le__isnull=True,
            ).select_related('chauffeur'),
            "dans_service_actuel": current_app() in self.client.prestations_actuelles(as_services=True),
        })
        return context


class ClientPrefEditView(CreateUpdateView):
    model = Preference
    form_class = forms.PreferenceEditForm
    template_name = 'transport/preference_edit.html'
    pk_url_kwarg = 'pk_pref'
    json_response = True

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'client': get_object_or_404(Client, pk=self.kwargs['pk']),
        }

    def form_valid(self, form):
        with transaction.atomic():
            # TODO: edit only remarque field
            if not form.instance.client_id:
                form.instance.client = form.client
                if form.cleaned_data['typ'] == 'pref':
                    add_message = f"Ajout préférence pour chauffeur {form.cleaned_data['chauffeur']}"
                else:
                    add_message = f"Ajout incompatibilité avec chauffeur {form.cleaned_data['chauffeur']}"
                Journal.objects.create(
                    persona=form.instance.client.persona, description=add_message,
                    quand=timezone.now(), qui=self.request.user
                )
            super().form_valid(form)
        return JsonResponse({
            'result': 'OK',
            'reload': 'page',
        })


class ClientPrefDeleteView(DeleteView):
    model = Preference

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer cette préférence.")
        with transaction.atomic():
            self.object.delete()
            if self.object.typ == 'pref':
                del_message = f"Suppression préférence pour chauffeur {self.object.chauffeur}"
            else:
                del_message = f"Suppression incompatibilité avec chauffeur {self.object.chauffeur}"
            Journal.objects.create(
                persona=self.object.client.persona, description=del_message,
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientTransportsView(
    PermissionRequiredMixin, ClientDetailContextMixin, FilterFormMixin, ListView
):
    model = Transport
    permission_required = 'transport.view_transport'
    filter_formclass = forms.TransportArchivesFilterForm
    paginate_by = 25
    template_name = "transport/par_client_archives.html"
    passes = True  # Utilisé uniquement pour les archives pour le moment

    def get(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        if self.passes:
            qs = self.client.transports.filter(
                date__lt=date.today()
            ).prefetch_related(
                Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
                Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr')),
            ).order_by('-heure_rdv')
            # super() will apply filtering
            return super().get_queryset(base_qs=qs)
        else:
            return self.client.transports.filter(date__gte=date.today()).order_by('heure_rdv')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        occups_passees = self.client.vehiculeoccup_set.filter(
            duree__endswith__date__lt=date.today()
        ).order_by("-duree__startswith")
        if occups_passees:
            context["object_list"] = merge_occups(context["object_list"], occups_passees, desc=True)
        return {
            **context,
            'client': self.client,
            'filter_form': self.filter_form,
        }


class ClientListeFacturesView(PermissionRequiredMixin, ClientDetailContextMixin, TemplateView):
    permission_required = 'transport.view_transport'
    template_name = 'transport/client_factures.html'

    def get_context_data(self, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'factures': self.client.factures_transports.order_by('-mois_facture'),
            'trans_url': settings.FACTURES_TRANSMISSION_URL,
        }


class ClientDerniersTransportsView(PermissionRequiredMixin, TemplateView):
    """Affichage dynamique des derniers transports du client dans certaines pages."""
    template_name = 'transport/client_derniers.html'
    permission_required = 'transport.view_transport'

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.request.GET.get('client'))
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'chaise': Handicaps.CHAISE in (self.client.handicaps or []),
            'transports': self.client.transports.order_by('-heure_rdv')[:5],
        }


class ClientDetailsJsonView(View):
    """
    Détails du client demandés par le formulaire de saisie de nouveau transport
    lors de la sélection d’un client.
    """
    def get(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=request.GET.get('id'))
        adr = client.adresses.depose()
        return JsonResponse({
            'rue': adr.rue, 'npa': adr.npa, 'localite': adr.localite,
            'type_logement': client.type_logement,
            'fonds_transport': client.fonds_transport,
            'remarques': '<br>'.join(
                rem for rem in [client.remarques_int_transport, client.remarques_ext_transport] if rem
            ),
            'geolocalise': adr.empl_geo is not None,
            'laa': client.is_laa(),
        })


class AdresseListeView(FilterFormMixin, ListView):
    model = Adresse
    filter_formclass = forms.AdresseFilterForm
    paginate_by = 30
    template_name = 'transport/adresse_list.html'

    def get_queryset(self):
        return super().get_queryset(base_qs=Adresse.objects.actives().order_by('nom'))


class AdresseEditView(GeoAddressMixin, PermissionRequiredMixin, CreateUpdateView):
    """
    Appelée soit depuis un formulaire de saisie de transport (avec param "target"
    contenant l'id du composant à remplir avec la nouvelle adresse),
    soit depuis la liste de toutes les destinations (bouton Ajouter une adresse).
    """
    template_name = 'transport/adresse_edit.html'
    model = Adresse
    permission_required = 'transport.change_adresse'
    form_class = forms.AdresseEditForm
    json_response = True

    def get_initial(self):
        initial = {
            **super().get_initial(),
            'target': self.request.GET.get('target'),
        }
        if self.is_create:
            initial['validite'] = DateRange(date.today(), None)
        return initial

    def get_success_url(self):
        return None

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'futurs': Transport.objects.filter(
                Q(date__gte=date.today()) & (Q(trajets__origine_adr=self.object) |
                Q(trajets__destination_adr=self.object))
            ).distinct().order_by('date') if not self.is_create else [],
        }
        return context

    def form_valid(self, form):
        super().form_valid(form)
        adresse = self.object
        target = form.cleaned_data.get('target')
        if self.is_create and not target:
            messages.success(self.request, f"L’adresse «{adresse}» a bien été créée.")
        return JsonResponse({
            'result': 'OK',
            'reload': 'populateAC' if (self.is_create and target) else 'page',
            'target': target,
            'object': {'value': adresse.pk, 'label': str(adresse)},
        })


class AdresseAutocompleteView(View):
    """Endpoint for autocomplete search for Adresse."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        results = [
            {'label': adr.str_avec_validite(), 'value': adr.pk}
            for adr in Adresse.objects.actives().filter(
                Q(nom__unaccent__icontains=term) |
                Q(localite__unaccent__icontains=term) |
                Q(rue__unaccent__icontains=term)
            )[:20]
        ]
        if 'domicile du client'.startswith(term.lower()):
            results.insert(0, {'label': "Domicile du client", 'value': '-1'})
        return JsonResponse(results, safe=False)


class SearchChAutocompleteView(View):
    """Search on search.ch for new Adresse proposals."""
    # https://tel.search.ch/api/help
    api_base = 'https://tel.search.ch/api/'

    def get(self, request, *args, **kwargs):
        import feedparser
        term = request.GET.get('q')
        # Limiter d'abord à NE…
        resp = httpx.get(self.api_base, params={
            'q': term, 'wo': canton_abrev().lower(), 'key': settings.SEARCH_CH_API_KEY
        })
        feed = feedparser.parse(resp.text)
        if len(feed.entries) == 0:
            # … puis étendre si on n'obtient pas de résultats
            resp = httpx.get(self.api_base, params={'q': term, 'key': settings.SEARCH_CH_API_KEY})
            feed = feedparser.parse(resp.text)
        results = []
        for line in feed.entries:
            nom = ' '.join([txt for txt in [line.get('tel_name'), line.get('tel_firstname')] if txt])
            rue = ' '.join([item for item in [line.get('tel_street'), line.get('tel_streetno')] if item])
            results.append({
                'label': f"{nom}, {rue}, "
                         f"{line.get('tel_zip')} {line.get('tel_city')}",
                'value': line.id,
                'details': {
                    'nom': nom,
                    'npa': line.get('tel_zip'),
                    'localite': line.get('tel_city'),
                    'rue': rue,
                    'tel': line.get('tel_phone'),
                }
            })
        return JsonResponse(results, safe=False)


class ChauffeurListView(BenevoleListView):
    template_name = 'transport/chauffeur_list.html'
    filter_formclass = forms.ChauffeurFilterForm
    extra_export_cols = [
        ('N° plaque', 'no_plaques'), ('Macaron', 'macaron_valide'),
        ('Dernier cours de conduite', 'dernier_cours_cond')
    ]

    def get_queryset(self):
        qs = super().get_queryset()
        if (
            self.filter_form.is_bound and self.filter_form.is_valid() and
            self.filter_form.cleaned_data.get('listes') == 'date_cours_cond'
        ) or self.export_flag:
            qs = qs.annotate(
                dernier_cours_cond=Max(
                    'formations__quand', filter=Q(formations__categorie__code="conduite")
                )
            )
            self.show_cours_cond = True
        return qs


class ChauffeurTransportsView(FilterFormMixin, ListView):
    model = Transport
    filter_formclass = forms.TransportArchivesFilterForm
    paginate_by = 25
    passes = False

    def get(self, *args, **kwargs):
        self.chauffeur = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_template_names(self):
        return ['transport/par_chauffeur_archives.html'] if self.passes else ['transport/par_chauffeur.html']

    def get_queryset(self):
        if self.passes:
            qs = self.chauffeur.transports.filter(
                date__lt=date.today()
            ).select_related('client').prefetch_related(
                'frais',
                Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
                Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr')),
            ).order_by('-heure_rdv')
            # super() will apply filtering
            return super().get_queryset(base_qs=qs)
        else:
            return self.chauffeur.transports.filter(date__gte=date.today()).order_by('heure_rdv')

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'benevole': self.chauffeur, 'chauffeur': self.chauffeur,
            'filter_form': self.filter_form,
        }
        if self.passes:
            liste_mois = sorted(
                self.chauffeur.transports.annotate(
                    month=TruncMonth('date')).values_list('month', flat=True
                ).distinct(), reverse=True
            )
            if liste_mois and liste_mois[0] == date.today().replace(day=1):
                liste_mois = liste_mois[1:]
            context['remboursements'] = liste_mois
        return context


class ChauffeurTransportsPDFView(FormView):
    form_class = forms.DateRangeSelectionForm
    template_name = 'transport/selection_date_transports.html'

    def dispatch(self, *args, **kwargs):
        if "pk" in self.kwargs:
            self.chauffeur = get_object_or_404(Benevole, pk=self.kwargs["pk"])
        else:
            # Depuis l'app chauffeur
            self.chauffeur = get_object_or_404(Benevole, utilisateur=self.request.user)
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        return {'date_de': date.today(), 'date_a': date.today() + timedelta(days=1)}

    def form_valid(self, form):
        transports = self.chauffeur.transports.filter(
            date__range=(form.cleaned_data["date_de"], form.cleaned_data["date_a"])
        ).filter(
            statut__gte=Transport.StatutChoices.CONFIRME,
            statut__lt=Transport.StatutChoices.ANNULE,
        ).order_by("heure_rdv")
        if not transports:
            messages.error(self.request, "Aucun transport dans la plage de dates indiquée")
            return HttpResponseRedirect(reverse('benevole-transports', args=[self.chauffeur.pk]))
        temp = BytesIO()
        pdf = TransportsPDF(temp)
        pdf.produce(
            transports[0].chauffeur, transports, form.cleaned_data["date_de"], form.cleaned_data["date_a"]
        )
        temp.seek(0)
        return FileResponse(
            temp, as_attachment=True,
            filename=(f'transports_{slugify(" ".join([self.chauffeur.nom, self.chauffeur.prenom]))}'
                      f'_{form.cleaned_data["date_de"]}_{form.cleaned_data["date_a"]}.pdf')
        )


class ChauffeurRembPDFView(PermissionRequiredMixin, BasePDFView):
    permission_required = 'transport.view_transport'
    obj_class = Benevole
    pdf_class = RemboursementPDF

    def get(self, request, *args, **kwargs):
        chauffeur = self.get_object()
        mois = date(self.kwargs["year"], self.kwargs["month"], 1)
        self.produce_kwargs = {
            'month': mois,
            'transports': chauffeur.transports.filter(
                date__year=mois.year, date__month=mois.month
            ).exclude(
                defrayer_chauffeur=False
            ).prefetch_related('trajets', 'frais').order_by('heure_rdv'),
        }
        return super().get(request, *args, **kwargs)


class AgendaMixin:
    START_HOUR = 8
    END_HOUR = 19
    NUM_DAYS = 7

    def events_by_day(self, events, week_start, num_days=NUM_DAYS):
        """Return a dict {day: {'time(8, 30)': [events], ...}."""
        monday = week_start.date() if isinstance(week_start, datetime) else week_start
        week_days = [
            monday + timedelta(days=i)
            for i in range(num_days)
        ]
        years = list({week_days[0].year, week_days[-1].year})
        holidays = CRHolidays().holidays_dict(years)
        week_events = {
            dt: {'events': [], 'cols': 0, 'holiday': holidays.get(dt)} for dt in week_days
        }
        # Start by putting each event in its week day
        for event in events:
            try:
                week_events[timezone.localtime(event.debut).date()]['events'].append(event)
            except KeyError:
                pass
        # Checking events overlap
        for day in week_events:
            num_cols = day_overlap(week_events[day]['events'])
            week_events[day]['cols'] = num_cols
            week_events[day]['col_width'] = round(100.0 / num_cols, 2)
        # Putting events as a dict with the hour quarter as key
        for day in week_events:
            events = week_events[day]['events']
            week_events[day]['events'] = {}
            for event in events:
                hour = round_quarter(timezone.localtime(event.debut))
                week_events[day]['events'].setdefault(hour, []).append(event)
        return week_events

    def get_start_end_dates(self):
        if self.kwargs['year'] == 0:  # current week
            year, week_num, _ = date.today().isocalendar()
        else:
            year, week_num = self.kwargs['year'], self.kwargs['week']
        week_start = timezone.make_aware(
            datetime.strptime(f'{year}-{week_num}-1', "%G-%V-%w")
        )
        return week_start, week_start + timedelta(days=self.NUM_DAYS)

    def prev_next_urls(self, obj, url_name, start, end):
        # Prev/next links
        prev_iso = (start - timedelta(days=self.NUM_DAYS)).isocalendar()
        next_iso = end.isocalendar()
        return (
            reverse(url_name, args=[obj.pk, prev_iso.year, prev_iso.week]),
            reverse(url_name, args=[obj.pk, next_iso.year, next_iso.week]),
        )

    def get_day_divs(self, start_hour, end_hour):
        return [time(h, m) for h in range(start_hour, end_hour + 1) for m in range(0, 60, 15)]


class ChauffeurAgendaView(AgendaMixin, TemplateView):
    template_name = "agenda-week.html"

    def get_chauffeur(self):
        return get_object_or_404(Benevole, pk=self.kwargs['pk'])

    def filter_transports(self, queryset):
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chauffeur = self.get_chauffeur()
        ag_start, ag_end = self.get_start_end_dates()
        dispos = list(Dispo.get_for_person(chauffeur, ag_start, ag_end))
        transports = list(self.filter_transports(
            Transport.get_for_person(chauffeur, ag_start, ag_end).exclude(statut=Transport.StatutChoices.ANNULE)
        ))
        prev_url, next_url = self.prev_next_urls(chauffeur, 'benevole-agenda', ag_start, ag_end)
        start_hour = min(self.START_HOUR, *(
            [self.START_HOUR] + [timezone.localtime(d.debut).hour for d in dispos] +
            [t.heure_depart.hour for t in transports]
        ))
        end_hour = max(self.END_HOUR, *(
            [self.END_HOUR] + [timezone.localtime(d.fin).hour for d in dispos] +
            [t.heure_arrivee.hour for t in transports]
        ))

        week_events = self.events_by_day(dispos + transports, ag_start, num_days=self.NUM_DAYS)
        context.update({
            'breadcrumbs': [(reverse("home"), "Accueil")],
            'benevole': chauffeur,
            'base': 'transport/base.html',
            'show_tabs': True,
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agendas': [{
                'title': f'Agenda pour {chauffeur}',
                'chauffeur': chauffeur,
                'week_events': week_events,
                'prev_events': prev_url,
                'next_events': next_url,
                'has_holidays': any(item['holiday'] for item in week_events.values()),
                'add_buttons': [{
                    'href': reverse('benevole-dispo-add', args=[chauffeur.pk]),
                    'title': 'Ajouter une dispo',
                    'style': 'info',
                }, {
                    'href': reverse('benevole-absence-add', args=[chauffeur.pk]),
                    'title': 'Ajouter une absence',
                    'style': 'warning',
                }],
                'classes': 'week-grid' if (ag_end - ag_start).days >= 5 else 'day-grid',
            }],
        })
        if self.request.user.has_perm("benevole.view_benevole"):
            context["breadcrumbs"].extend([
                (reverse("benevoles", args=["transport"]), "Chauffeurs"),
                (None, str(chauffeur)),
            ])
        else:
            context["breadcrumbs"].extend([
                (reverse("home-app-dispos"), "Disponibilités"),
                (None, self.day.date()),
            ])
        return context


class ChauffeurDispoEditView(CreateUpdateView):
    model = Dispo
    categ = 'dispo'
    form_class = forms.DispoEditForm
    template_name = 'transport/chauffeur-dispo-edit.html'
    json_response = True

    def get_chauffeur(self):
        if self.object and self.object.chauffeur_id:
            return self.object.chauffeur
        elif 'chauffeur_pk' in self.kwargs:
            return Benevole.objects.get(pk=self.kwargs['chauffeur_pk'])
        else:
            return self.request.user.benevole

    def get_initial(self):
        initial = super().get_initial()
        if not self.is_create:
            initial['date'] = timezone.localtime(self.object.debut).date()
            initial['heure_de'] = timezone.localtime(self.object.debut).time()
            initial['heure_a'] = timezone.localtime(self.object.fin).time()
        elif 'date' in self.request.GET:
            try:
                initial['date'] = datetime.strptime(self.request.GET['date'], '%Y-%m-%d').date()
            except ValueError:
                pass
        return initial

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'chauffeur': self.get_chauffeur(),
            'categ': (self.object.categorie if self.object else None) or self.categ,
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_benev_app = '/app/' in self.request.path
        if self.object and self.object.debut.date() > date.today():
            context['delete_url'] = (
                reverse('benevole-self-dispo-delete', args=[self.object.pk]) if is_benev_app
                else reverse('benevole-dispo-delete', args=[self.object.pk])
            )
        if 'occ' in self.request.GET:
            occ_date = datetime.fromisoformat(self.request.GET['occ'])
            context.update({
                'occ_date': occ_date,
                'occ_obj': self.object.exceptions.filter(orig_start=occ_date).first(),
                'toggle_url': (
                    reverse('benevole-self-dispo-toggle', args=[self.object.pk]) if is_benev_app
                    else reverse('benevole-dispo-toggle', args=[self.object.pk])
                )
            })
        return context

    def form_valid(self, form):
        if self.is_create:
            form.instance.categorie = self.categ
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDispoToggleView(View):
    def post(self, request, *args, **kwargs):
        dispo = get_object_or_404(Dispo, pk=self.kwargs['pk'])
        if not dispo.can_edit(request.user):
            raise PermissionDenied()
        occ_date = datetime.fromisoformat(request.POST['occ_date']).date()
        with transaction.atomic():
            try:
                occ = dispo.exceptions.select_for_update().get(orig_start__date=occ_date)
                occ.delete()
            except Occurrence.DoesNotExist:
                dispo.cancel_for(occ_date)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDispoDeleteView(DeleteView):
    model = Dispo

    def form_valid(self, form):
        if not self.object.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer cette disponibilité.")
        # Check transports
        transports_lies = []
        transports_futurs = list(self.object.chauffeur.transports.filter(date__gte=date.today()))
        if transports_futurs:
            date_min = min(t.heure_rdv for t in transports_futurs)
            date_max = max(t.date for t in transports_futurs)
            dispo_occs = self.object.get_occurrences(
                date_min, datetime.combine(date_max, time(23, 59), tzinfo=date_min.tzinfo)
            )
            for transp in transports_futurs:
                if any((transp.debut > occ.debut and transp.fin < occ.fin) for occ in dispo_occs):
                    transports_lies.append(transp)
        if not transports_lies:
            self.object.delete()
        else:
            msg = "Impossible de supprimer cette disponibilité car les transports suivants sont déjà attribués:<br>"
            msg += "<br>".join(str(t) for t in transports_lies)
            messages.error(self.request, SafeString(msg))
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDayView(AgendaMixin, FromModeleMixin, TemplateView):
    """
    Affichage dispos et transports d'un chauffeur pour le jour d'un transport à
    placer (pour l'attribution)
    """
    template_name = "agenda_grid.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chauffeur = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        if 'moddate' in self.kwargs:
            transport = self.get_object()
        else:
            transport = get_object_or_404(Transport, pk=self.kwargs['transport_pk'])
        transport.ev_category = 'nouveau-transport'
        jour = transport.heure_rdv.replace(hour=0, minute=0)
        dispos = Dispo.get_for_person(chauffeur, jour, jour + timedelta(days=1))
        transports = Transport.get_for_person(
            chauffeur, jour, jour + timedelta(days=1)
        ).exclude(statut=Transport.StatutChoices.ANNULE)
        start_hour = min(*(
            [self.START_HOUR, transport.heure_depart.hour] +
            [timezone.localtime(d.debut).hour for d in dispos] +
            [t.heure_depart.hour for t in transports]
        ))
        end_hour = max(*(
            [self.END_HOUR, transport.heure_arrivee.hour] +
            [timezone.localtime(d.fin).hour for d in dispos if d.fin] +
            [t.heure_arrivee.hour for t in transports]
        ))
        context.update({
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agenda': {
                'week_events': self.events_by_day(list(dispos) + list(transports) + [transport], jour, num_days=1),
            }
        })
        return context


class VehiculeListView(PermissionRequiredMixin, ListView):
    permission_required = 'transport.view_vehicule'
    model = Vehicule
    template_name = 'transport/vehicules.html'


class VehiculeDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'transport.view_vehicule'
    model = Vehicule
    context_object_name = 'vehicule'
    template_name = 'transport/vehicule_detail.html'


class VehiculeEditView(PermissionRequiredMixin, GeoAddressMixin, CreateUpdateView):
    permission_required = 'transport.change_vehicule'
    model = Vehicule
    form_class = forms.VehiculeForm
    json_response = True

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VehiculeAgendaView(PermissionRequiredMixin, AgendaMixin, TemplateView):
    permission_required = 'transport.view_vehicule'
    template_name = "agenda-week.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        vehicule = get_object_or_404(Vehicule, pk=self.kwargs['pk'])
        ag_start, ag_end = self.get_start_end_dates()
        events = vehicule.get_all_events(ag_start, ag_end)

        prev_url, next_url = self.prev_next_urls(vehicule, 'vehicule-agenda', ag_start, ag_end)
        start_hour = min(self.START_HOUR, *(
            [self.START_HOUR] + [ev.debut.hour for ev in events]
        ))
        end_hour = max(self.END_HOUR, *(
            [self.END_HOUR] + [ev.fin.hour for ev in events]
        ))

        context.update({
            "breadcrumbs": [
                (reverse("home"), "Accueil"),
                (reverse("vehicules"), "Véhicules"),
                (None, str(vehicule)),
            ],
            'vehicule': vehicule,
            'base': 'transport/base.html',
            'show_tabs': True,
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agendas': [{
                'title': f'Agenda pour {vehicule}',
                'vehicule': vehicule,
                'week_events': self.events_by_day(events, ag_start, num_days=self.NUM_DAYS),
                'prev_events': prev_url,
                'next_events': next_url,
                'add_buttons': [{
                    'href': reverse("vehicule-indispo-add", args=[vehicule.pk, "indispo"]),
                    'title': 'Ajouter une indisponibilité',
                    'style': 'warning',
                }, {
                    "href": reverse("vehicule-indispo-add", args=[vehicule.pk, "reserv"]),
                    "title": "Ajouter une réservation",
                    "style": "warning",
                }],
                'classes': 'week-grid' if (ag_end - ag_start).days >= 5 else 'day-grid',
            }],
        })
        return context


class VehiculeDayView(AgendaMixin, FromModeleMixin, TemplateView):
    """
    Affichage dispos et transports d'un véhicule pour le jour d'un transport à
    placer (pour l'attribution).
    """
    template_name = "agenda_grid.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        vehicule = get_object_or_404(Vehicule, pk=self.kwargs['pk'])
        if 'moddate' in self.kwargs:
            transport = self.get_object()
        else:
            transport = get_object_or_404(Transport, pk=self.kwargs['transport_pk'])
        transport.ev_category = 'nouveau-transport'
        jour = transport.heure_rdv.replace(hour=0, minute=0)
        events = vehicule.get_all_events(jour, jour + timedelta(days=1))
        start_hour = min(*(
            [self.START_HOUR, transport.heure_depart.hour] + [ev.debut.hour for ev in events]
        ))
        end_hour = max(*(
            [self.END_HOUR, transport.heure_arrivee.hour] + [ev.fin.hour for ev in events]
        ))
        context.update({
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agenda': {
                'week_events': self.events_by_day(events + [transport], jour, num_days=1),
            }
        })
        return context


class VehiculeIndispoView(CreateUpdateView):
    model = VehiculeOccup
    template_name = "transport/vehicule_indispo_edit.html"
    json_response = True

    def get_form_class(self):
        if self.is_create:
            if self.kwargs["type"] == "reserv":
                return forms.VehiculeReservEditForm
            return forms.VehiculeOccupEditForm
        else:
            if self.object.client_id:
                return forms.VehiculeReservEditForm
            return forms.VehiculeOccupEditForm

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'vehicule': (
                get_object_or_404(Vehicule, pk=self.kwargs['pk'])
                if self.object is None else self.object.vehicule
            ),
        }

    def get_initial(self):
        initial = super().get_initial()
        if self.object and self.object.client_id:
            initial["client_select"] = str(self.object.client)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.is_create:
            context['delete_url'] = reverse('vehicule-indispo-delete', args=[self.object.pk])
        return context

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VehiculeIndispoDeleteView(DeleteView):
    model = VehiculeOccup

    def form_valid(self, form):
        if not self.object.can_edit(self.request.user):
            raise PermissionDenied(
                "Vous n’avez pas les permissions nécessaires pour supprimer cette indisponibilité."
            )
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VehiculeTransportsView(PermissionRequiredMixin, ListView):
    permission_required = 'transport.view_vehicule'
    template_name = 'transport/vehicule_transports.html'
    active = "transports"

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        self.vehicule = get_object_or_404(Vehicule, pk=self.kwargs['pk'])

    def get_queryset(self):
        return self.vehicule.transports.filter(
            statut__lt=Transport.StatutChoices.EFFECTUE
        ).order_by("date")

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(), 'vehicule': self.vehicule}


class VehiculeArchivesView(VehiculeTransportsView):
    paginate_by = 25
    active = "archives"

    def get_queryset(self):
        return self.vehicule.transports.filter(
            statut__gte=Transport.StatutChoices.EFFECTUE
        ).order_by("-date")


class LotAFacturer:
    def __init__(self, un_du_mois, billed=None, client=None):
        self.un_du_mois = un_du_mois
        self.billed = billed
        self.client = client
        self.transports = self.get_transports()
        self.reservations = self.get_reservations()
        self.fpolicy = fact_policy()

    def exists(self):
        return self.transports.exists() or self.reservations.exists()

    def facturer(self, date_facture, **kwargs):
        _a_fact = chain(self.transports, self.reservations)
        if self.client:
            self.facturer_client(self.client, date_facture, _a_fact, **kwargs)
        else:
            # Grouper par client, puis facturer
            for client, a_fact in groupby(_a_fact, key=lambda t: t.client):
                self.facturer_client(client, date_facture, a_fact, **kwargs)

    def facturer_client(self, client, date_fact, a_facturer, ecraser=True, creer_pdf=False, merger=None):
        # 1 facture différente par adresse de facturation
        factures = {}
        donnees_fact = []
        for transport in a_facturer:
            fact = transport.donnees_facturation()
            if fact is None:
                continue
            debiteur = client.adresse_facturation(transport)
            if not debiteur in factures:
                facture = Facture(
                    client=client,
                    mois_facture=self.un_du_mois,
                    date_facture=date_fact
                )
                if debiteur is not client:
                    facture.autre_debiteur = debiteur
                facture.donnees_fact = []
                factures[debiteur] = facture
            factures[debiteur].donnees_fact.append(fact)

        for debiteur, facture in factures.items():
            if creer_pdf:
                temp = BytesIO()
                facture_pdf = FacturePDF(temp)
                fact_result = facture_pdf.produce(client, debiteur, facture.donnees_fact, facture.no, date_fact)
                if not fact_result:
                    return None
                # Écrire le fichier PDF sur le système de fichier
                if ecraser:
                    fact_path = Path(facture.fichier_pdf.path)
                else:
                    fact_path = facture.fs_path(check_exists=False)
                    while fact_path.exists():
                        # Suffix to not overwrite (can stack: ..._v2_v2_v2.pdf)
                        fact_path = fact_path.with_name(fact_path.stem + '_v2' + fact_path.suffix)
                    fact_path.parent.mkdir(parents=True, exist_ok=True)
                    facture.fichier_pdf.name = str(fact_path.relative_to(settings.MEDIA_ROOT))
                fact_path.write_bytes(temp.getvalue())
                if merger is not None:
                    temp.seek(0)
                    merger.append(temp)
                facture.montant_total = fact_result['total']
            else:
                facture.montant_total = self.fpolicy.montant_total(facture.donnees_fact)

            facture.no = Facture.next_no()
            facture.nb_transp = len(facture.donnees_fact)
            facture.save()
            Transport.objects.filter(
                pk__in=[data["no"] for data in facture.donnees_fact if data["type_fact"] == "transport"]
            ).update(facture=facture)
            VehiculeOccup.objects.filter(
                pk__in=[data["no"] for data in facture.donnees_fact if data["type_fact"] == "reservation"]
            ).update(facture=facture)
        return factures

    def _filters(self):
        qfilters = {}
        if self.un_du_mois is not None:
            assert self.un_du_mois.day == 1
            qfilters["month"] = self.un_du_mois
        if self.billed is not None:
            qfilters["facture__isnull"] = not self.billed
        if self.client is not None:
            qfilters["client"] = self.client
        return qfilters

    def get_transports(self):
        qfilters = {
            'statut__in': [Transport.StatutChoices.CONTROLE, Transport.StatutChoices.ANNULE],
            'statut_fact__lt': Transport.FacturationChoices.PAS_FACTURER,
        } | self._filters()
        return Transport.objects.avec_is_ofas().annotate(
            month=TruncMonth('date')
        ).filter(
            **qfilters
        ).prefetch_related('frais').order_by('client__persona__nom', 'client__id', 'heure_rdv')

    def get_reservations(self):
        return VehiculeOccup.objects.annotate(
            month=TruncMonth("duree__startswith__date")
        ).filter(**self._filters())


def get_client_tabs(view, client, tabs):
    tabs.insert(1, {
        'id': 'edition', 'title': "Édition",
        'url': reverse('client-edit', args=[client.pk])
    })
    if view.request.user.has_perm('transport.view_facture'):
        tabs.insert(-1, {
            'id': 'factures', 'title': "Factures",
            'url': reverse('client-transports-factures', args=[client.pk])
        })
    tabs.insert(-1, {
        'id': 'archives', 'title': "Archives",
        'url': reverse('client-transports-archives', args=[client.pk])
    })
    return tabs


ClientDetailContextMixin.app_tabs_methods['transport'] = get_client_tabs


def round_quarter(time_):
    return time(time_.hour, math.floor(time_.minute / 15) * 15)


def day_overlap(event_list):
    """
    Attach a column attribute to each event in event_list, so as no events overlap
    """
    event_list = [ev for ev in event_list if not isinstance(ev, Dispo)]
    cols = [[]]

    def overlaps(ev, other):
        return ev.debut < other.fin and ev.fin > other.debut

    def find_column_for_event(ev):
        for idx, evts in enumerate(cols):
            overlapping = [other for other in evts if overlaps(ev, other)]
            if overlapping:
                for e in overlapping:
                    e.overlaps = True
                ev.overlaps = True
                continue
            else:
                # Add event to this col
                cols[idx].append(ev)
                ev.column = idx
                break
        else:
            # Add event to a new col
            cols.append([ev])
            ev.column = len(cols) - 1

    # Set column for each rzv event related to its dispo
    for ev in event_list:
        find_column_for_event(ev)

    for ev in event_list:
        ev.leftoffset = ev.column * (100.0 / len(cols))
        if ev.leftoffset != 0:
            ev.leftoffset = f'{ev.leftoffset}%'
    return len(cols)


def merge_occups(transports, vhc_occups, desc=False):
    # Insertion des occups véhicule dans une liste de transports, le cas échéant
    return sorted(
        list(transports) + list(vhc_occups),
        key=lambda obj: obj.heure_depart if hasattr(obj, "heure_depart") else obj.duree.lower,
        reverse=desc
    )
    if not transports:
        return vhc_occups
    transports = list(transports)
    occup_idx = 0
    import pdb; pdb.set_trace()
    for transp in transports[:]:
        while vhc_occups[occup_idx].duree.lower.date() >= transp.date:
            transports.insert(
                transports.index(transp), vhc_occups[occup_idx]
            )
            occup_idx += 1
            if occup_idx >= len(vhc_occups):
                break
    return transports
