from django.db import transaction

from benevole.models import NoteFrais
from common.utils import canton_app
from transport.models import Transport


def generer_notes_frais(mois, chauffeur=None):
    calculateur = canton_app.fact_policy.calculateur_frais(mois)
    if chauffeur is not None:
        num_created = calculateur.calculer_frais(chauffeur, enregistrer=True, refaire=True)
    else:
        num_created = calculateur.calculer_frais(chauffeur, enregistrer=True, refaire=False)
    return num_created
