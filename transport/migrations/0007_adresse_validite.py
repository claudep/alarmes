import django.contrib.postgres.fields.ranges
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0006_transport_chauffeur_externe'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresse',
            name='validite',
            field=django.contrib.postgres.fields.ranges.DateRangeField(null=True, verbose_name='Validité'),
        ),
    ]
