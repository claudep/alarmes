from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("transport", "0001_squashed_0047_create_client_prestations"),
    ]

    operations = [
        migrations.RunSQL("""DROP FUNCTION is_ofas(integer, date, boolean, date, varchar);"""),
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_ofas(
  transport_pk int, transport_date date, tarif_avs_des date, date_naissance date, genre varchar(1)
) returns boolean
language plpgsql as $$
declare is_ofas boolean;
        num_years integer := EXTRACT(YEAR FROM transport_date) - EXTRACT(YEAR FROM date_naissance);
begin
   SELECT ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND ((tarif_avs_des IS NOT NULL AND tarif_avs_des <= transport_date) OR num_years >= 65 OR (genre = 'F' AND num_years >= 64)) into is_ofas from transport_trajet
   WHERE transport_id = transport_pk;
   RETURN is_ofas;
end;$$;""")
    ]
