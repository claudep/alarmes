from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0009_adresse_contrainte_exclusion'),
    ]

    operations = [
        migrations.AddField(
            model_name='facture',
            name='annulee_msg',
            field=models.TextField(blank=True, verbose_name='Raison de l’annulation'),
        ),
        migrations.AlterField(
            model_name='facture',
            name='annulee',
            field=models.BooleanField(default=False, verbose_name='Facture annulée'),
        ),
    ]
