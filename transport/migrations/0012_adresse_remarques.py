from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0011_chauffeur_aller_retour__duree'),
    ]

    operations = [
        migrations.AddField(
            model_name='adresse',
            name='remarques',
            field=models.TextField(blank=True),
        ),
    ]
