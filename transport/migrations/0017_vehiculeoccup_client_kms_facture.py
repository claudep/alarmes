from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '__first__'),
        ('transport', '0016_remove_transport_date_facture'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiculeoccup',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='client.client'),
        ),
        migrations.AddField(
            model_name='vehiculeoccup',
            name='facture',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='transport.facture'),
        ),
        migrations.AddField(
            model_name='vehiculeoccup',
            name='kms',
            field=models.IntegerField(blank=True, null=True, verbose_name='Kilomètres effectués'),
        ),
    ]
