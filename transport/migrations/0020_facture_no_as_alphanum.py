from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0019_transportmodel_vehicule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facture',
            name='no',
            field=models.CharField(blank=True, default='', max_length=20, verbose_name='N° de facture'),
            preserve_default=False,
        ),
    ]
