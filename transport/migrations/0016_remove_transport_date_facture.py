from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0015_populate_transport_facture'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transport',
            name='date_facture',
        ),
    ]
