from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0005_vehicule'),
    ]

    operations = [
        migrations.AddField(
            model_name='transport',
            name='chauffeur_externe',
            field=models.BooleanField(default=False),
        ),
    ]
