from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0017_vehiculeoccup_client_kms_facture'),
    ]

    operations = [
        migrations.AddField(
            model_name='transport',
            name='motif_annulation',
            field=models.CharField(blank=True, choices=[
                ('horsdelai', 'demande hors délai'), ('pas_chauff', 'pas de chauffeur disponible'),
                ('client_ann', 'annulation par le client (à l’avance)'),
                ('client_abs', 'annulé sur place (absence, refus, etc.)'), ('autre', 'autre motif'),
            ], max_length=10, verbose_name='Motif d’annulation'),
        ),
    ]
