import common.models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField, DateTimeRangeField
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0004_reuse_is_avs_in_is_ofas'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Vehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modele', models.CharField(max_length=60, verbose_name='Marque/Modèle')),
                ('no_plaque', models.CharField(blank=True, max_length=10, verbose_name='N° de plaque')),
                ('chaises_ok', models.BooleanField(default=False, verbose_name='Accepte chaises roulantes')),
                ('rue', models.CharField(blank=True, max_length=120, verbose_name='Rue')),
                ('npa', models.CharField(blank=True, max_length=5, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=30, verbose_name='Localité')),
                ('empl_geo', ArrayField(base_field=models.FloatField(), blank=True, null=True, size=2)),
                ('responsable', models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('archive_le', models.DateField(blank=True, null=True, verbose_name='Date d’archivage')),
            ],
            bases=(common.models.GeolocMixin, models.Model),
        ),
        migrations.CreateModel(
            name='VehiculeOccup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duree', DateTimeRangeField(verbose_name='Durée d’indisponibilité')),
                ('description', models.TextField(verbose_name='Description')),
                ('vehicule', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='indisponibilites', to='transport.vehicule')),
            ],
        ),
        migrations.AddField(
            model_name='transport',
            name='vehicule',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='transports', to='transport.vehicule'),
        ),
    ]
