from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("transport", "0002_is_ofas_from_tarif_avs_des"),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_ofas(
  transport_pk int, transport_date date, tarif_avs_des date, date_naissance date, genre varchar(1)
) returns boolean
language plpgsql as $$
declare is_ofas boolean;
        age_int integer := EXTRACT(YEAR FROM age(transport_date, date_naissance));
begin
   SELECT ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND (
     (tarif_avs_des IS NOT NULL AND tarif_avs_des <= transport_date) OR
      age_int >= 65 OR (genre = 'F' AND age_int >= 64)
   ) into is_ofas from transport_trajet
   WHERE transport_id = transport_pk;
   RETURN is_ofas;
end;$$;""")
    ]
