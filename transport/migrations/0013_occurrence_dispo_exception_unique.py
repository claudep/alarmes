from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0012_adresse_remarques'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='occurrence',
            constraint=models.UniqueConstraint(fields=('dispo', 'orig_start'), name='dispo_exception_unique'),
        ),
    ]
