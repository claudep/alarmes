from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields.ranges import DateRangeField
from django.contrib.postgres.operations import BtreeGistExtension
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0008_fill_adresse_validite'),
    ]

    operations = [
        BtreeGistExtension(),
        migrations.RemoveConstraint(
            model_name='adresse',
            name='nom_non_archive_unique',
        ),
        migrations.AlterField(
            model_name='adresse',
            name='validite',
            field=DateRangeField(verbose_name='Validité'),
        ),
        migrations.AddConstraint(
            model_name='adresse',
            constraint=ExclusionConstraint(
                expressions=[('nom', '='), ('validite', '&&')], name='nom_validite_overlap',
                violation_error_message="Il existe déjà une adresse pour ce nom dans la même plage de validité.",
            ),
        ),
        migrations.RemoveField(
            model_name='adresse',
            name='date_archive',
        ),
    ]
