from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0029_is_avs_function_revision21"),
        ("transport", "0003_is_ofas_from_exact_birthdate"),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_ofas(
  transport_pk int, transport_date date, tarif_avs_des date, date_naissance date, genre varchar(1)
) returns boolean
language plpgsql as $$
declare is_ofas boolean;
        age_int integer := EXTRACT(YEAR FROM age(transport_date, date_naissance));
begin
   SELECT ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND (
     is_avs(transport_date, tarif_avs_des, date_naissance, genre)
   ) into is_ofas from transport_trajet
   WHERE transport_id = transport_pk;
   RETURN is_ofas;
end;$$;""")
    ]
