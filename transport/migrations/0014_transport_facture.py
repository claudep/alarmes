from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0013_occurrence_dispo_exception_unique'),
    ]

    operations = [
        migrations.AddField(
            model_name='transport',
            name='facture',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='transport.facture'),
        ),
    ]
