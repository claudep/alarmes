from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0018_transport_motif_annulation'),
    ]

    operations = [
        migrations.AddField(
            model_name='transportmodel',
            name='vehicule',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='transport.vehicule'),
        ),
    ]
