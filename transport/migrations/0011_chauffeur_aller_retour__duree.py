from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0010_facture_annulee_msg'),
    ]

    operations = [
        migrations.AddField(
            model_name='transport',
            name='chauff_aller_duree',
            field=models.DurationField(blank=True, null=True, verbose_name='Durée chauffeur-départ'),
        ),
        migrations.AddField(
            model_name='transport',
            name='chauff_retour_duree',
            field=models.DurationField(blank=True, null=True, verbose_name='Durée arrivée-chauffeur'),
        ),
    ]
