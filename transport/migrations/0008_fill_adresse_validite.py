from datetime import date, timedelta
from django.db import migrations


def fill_adresse_validite(apps, schema_editor):
    Adresse = apps.get_model('transport', 'Adresse')
    Adresse.objects.update(validite=(date(2020, 1, 1), None))
    for adr in Adresse.objects.filter(date_archive__isnull=False):
        adr.validite = (date(2020, 1, 1), adr.date_archive)
        adr.save(update_fields=['validite'])
        try:
            adr2 = Adresse.objects.exclude(pk=adr.pk).get(nom=adr.nom, date_archive=None)
        except Adresse.DoesNotExist:
            pass
        else:
            adr2.validite = (adr.date_archive + timedelta(days=1), None)
            adr2.save(update_fields=['validite'])


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0007_adresse_validite'),
    ]

    operations = [migrations.RunPython(fill_adresse_validite, migrations.RunPython.noop)]
