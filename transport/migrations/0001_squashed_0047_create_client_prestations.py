from django.conf import settings
from django.db import migrations, models
import django.contrib.postgres.fields
import django.db.models.deletion

import common.fields
import common.models
import transport.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('benevole', '__first__'),
        ('client', '0022_remove_client_type'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Adresse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=120, verbose_name='Nom')),
                ('rue', models.CharField(blank=True, max_length=120, verbose_name='Rue')),
                ('npa', models.CharField(max_length=5, verbose_name='NPA')),
                ('localite', models.CharField(max_length=30, verbose_name='Localité')),
                ('tel', common.fields.PhoneNumberField(blank=True, max_length=18, verbose_name='Téléphone')),
                ('empl_geo', django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), blank=True, null=True, size=2)),
                ('date_archive', models.DateField(blank=True, null=True, verbose_name='Archivée le')),
            ],
            bases=(common.models.GeolocMixin, models.Model),
        ),
        migrations.AddConstraint(
            model_name='adresse',
            constraint=models.UniqueConstraint(
                condition=models.Q(('date_archive__isnull', True)), fields=('nom',),
                name='nom_non_archive_unique'
            ),
        ),
        migrations.CreateModel(
            name='Regle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=40, verbose_name='Nom')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('frequence', models.CharField(choices=[('YEARLY', 'Chaque année'), ('MONTHLY', 'Chaque mois'), ('WEEKLY', 'Chaque semaine'), ('DAILY', 'Chaque jour'), ('HOURLY', 'Chaque heure'), ('MINUTELY', 'Chaque minute'), ('SECONDLY', 'Chaque seconde')], max_length=10, verbose_name='Fréquence')),
                ('params', models.TextField(blank=True, verbose_name='Params')),
            ],
            options={
                'verbose_name': 'Règle',
                'verbose_name_plural': 'Règles',
            },
        ),
        migrations.CreateModel(
            name='Dispo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.DateTimeField(db_index=True, verbose_name='Début')),
                ('fin', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Fin')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('categorie', models.CharField(choices=[('dispo', 'Disponibilité'), ('absence', 'Absence')], default='dispo', max_length=10, verbose_name='Catégorie')),
                ('fin_recurrence', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Dernière occurrence')),
                ('chauffeur', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='dispos', to='benevole.benevole')),
                ('regle', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='transport.regle', verbose_name='Règle')),
            ],
            bases=(transport.models.OccurrenceMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Facture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='factures_transports', to='client.client')),
                ('autre_debiteur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='client.referent', verbose_name='Autre débiteur')),
                ('mois_facture', models.DateField(verbose_name='Mois comptable')),
                ('no', models.BigIntegerField(blank=True, null=True, verbose_name='N° de facture')),
                ('date_facture', models.DateField(verbose_name='Date de facture')),
                ('nb_transp', models.PositiveSmallIntegerField(verbose_name='Nb de transports')),
                ('montant_total', models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Montant total')),
                ('fichier_pdf', models.FileField(blank=True, upload_to='transports/factures', verbose_name='Facture PDF')),
                ('annulee', models.BooleanField(default=False)),
                ('exporte', models.DateTimeField(blank=True, null=True, verbose_name='Exporté vers compta')),
                ('export_err', models.TextField(blank=True, verbose_name='Erreur d’exportation')),
                ('id_externe', models.BigIntegerField(blank=True, null=True)),
            ],
        ),
        migrations.AddConstraint(
            model_name='facture',
            constraint=models.UniqueConstraint(fields=('no',), name='no_facture_unique'),
        ),
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orig_start', models.DateTimeField(verbose_name='Début d’origine')),
                ('annule', models.BooleanField(default=False)),
                ('dispo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exceptions', to='transport.dispo')),
            ],
        ),
        migrations.CreateModel(
            name='Preference',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', models.CharField(choices=[('pref', '😀 Préféré'), ('incomp', '😡 Incompatible')], max_length=10, verbose_name='Type de lien')),
                ('chauffeur', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='preferences', to='benevole.benevole')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='preferences', to='client.client')),
                ('remarque', models.TextField(blank=True)),
            ],
        ),
        migrations.AddConstraint(
            model_name='preference',
            constraint=models.UniqueConstraint(fields=('client', 'chauffeur'), name='client_chauffeur_unique'),
        ),
        migrations.CreateModel(
            name='TrajetCommun',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('chauffeur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='transports_communs', to='benevole.benevole')),
            ],
        ),
        migrations.CreateModel(
            name='Trajet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typ', models.CharField(choices=[('medic', 'Médico-thérapeutique'), ('particip', 'Participatif-intégratif'), ('emplettes', 'Emplettes à deux'), ('home', 'Home')], max_length=10, verbose_name='Type de déplacement')),
                ('heure_depart', models.DateTimeField(verbose_name='Heure de départ')),
                ('origine_domicile', models.BooleanField(default=True)),
                ('origine_adr', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='transport.adresse')),
                ('destination_domicile', models.BooleanField(default=False)),
                ('destination_adr', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='transport.adresse')),
                ('destination_princ', models.BooleanField(default=False, verbose_name='Destination principale')),
                ('duree_calc', models.DurationField(blank=True, null=True, verbose_name='Durée estimée du trajet')),
                ('dist_calc', models.IntegerField(blank=True, null=True, verbose_name='Distance estimée du trajet [m]')),
                ('commun', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='transport.trajetcommun')),
            ],
        ),
        migrations.CreateModel(
            name='TransportModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='models', to='client.client')),
                ('heure_depart', models.TimeField(null=True, verbose_name='Heure de départ')),
                ('heure_rdv', models.DateTimeField(verbose_name='Heure de rendez-vous')),
                ('duree_rdv', models.DurationField(blank=True, null=True, verbose_name='Durée du rendez-vous')),
                ('retour', models.BooleanField(default=True)),
                ('regle', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='transport.regle', verbose_name='Règle')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('suspension_depuis', models.DateField(blank=True, null=True, verbose_name='Suspension dès le')),
                ('fin_recurrence', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Dernière occurrence')),
            ],
            bases=(transport.models.OccurrenceMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Transport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chauffeur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='transports', to='benevole.benevole')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='transports', to='client.client')),
                ('modele', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='transports', to='transport.transportmodel')),
                ('chauff_aller_dist', models.PositiveIntegerField(blank=True, null=True, verbose_name='Distance chauffeur-départ [m]')),
                ('chauff_retour_dist', models.PositiveIntegerField(blank=True, null=True, verbose_name='Distance arrivée-chauffeur [m]')),
                ('chauffeur_vu', models.BooleanField(default=False, verbose_name='Vu par chauffeur')),
                ('date', models.DateField(verbose_name='Date du transport')),
                ('heure_rdv', models.DateTimeField(verbose_name='Heure de rendez-vous')),
                ('duree_rdv', models.DurationField(blank=True, null=True, verbose_name='Durée du rendez-vous')),
                ('retour', models.BooleanField(default=True)),
                ('duree_eff', models.DurationField(blank=True, null=True, verbose_name='Durée effective du trajet')),
                ('km', models.DecimalField(blank=True, decimal_places=1, max_digits=5, null=True)),
                ('statut', models.PositiveSmallIntegerField(choices=[(1, 'Saisi'), (2, 'Attribué'), (3, 'Confirmé'), (4, 'Effectué'), (5, 'Rapporté'), (6, 'Contrôlé'), (10, 'Annulé')], default=1)),
                ('temps_attente', models.DurationField(blank=True, null=True, verbose_name='Temps d’attente')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('rapport_chauffeur', models.TextField(blank=True, verbose_name='Commentaires du chauffeur')),
                ('date_annulation', models.DateTimeField(blank=True, null=True, verbose_name='Date d’annulation')),
                ('raison_annulation', models.TextField(blank=True, verbose_name='Raison de l’annulation')),
                ('statut_fact', models.PositiveSmallIntegerField(choices=[
                    (1, 'Facturer le transport'), (2, 'Facturer frais d’annulation'),
                    (3, 'Facturer frais d’annulation et km chauffeur'), (4, 'Facturer les km du chauffeur'),
                    (99, 'Ne rien facturer')
                ], default=1, verbose_name='Statut de facturation')),
                ('defrayer_chauffeur', models.BooleanField(null=True, verbose_name='Défrayer les km du chauffeur')),
                ('date_facture', models.DateField(blank=True, null=True, verbose_name='Date de facture')),
            ],
            options={
                'permissions': [('gestion_finances', 'Autorisé à gérer finances et facturation')],
            },
        ),
        migrations.AddIndex(
            model_name='transport',
            index=models.Index(fields=['statut'], name='index_statut'),
        ),
        migrations.AddIndex(
            model_name='transport',
            index=models.Index(fields=['heure_rdv'], name='index_heure_rdv'),
        ),
        migrations.CreateModel(
            name='JournalTransport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transport', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='journaux', to='transport.transport', verbose_name='Transport')),
                ('description', models.TextField()),
                ('quand', models.DateTimeField()),
                ('qui', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'get_latest_by': 'quand',
            },
        ),
        migrations.CreateModel(
            name='Refus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chauffeur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='benevole.benevole')),
                ('transport', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='refus', to='transport.transport')),
            ],
        ),
        migrations.CreateModel(
            name='Frais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transport', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='frais', to='transport.transport')),
                ('descriptif', models.TextField(blank=True, verbose_name='Descriptif')),
                ('cout', models.DecimalField(decimal_places=2, max_digits=5, verbose_name='Coût')),
                ('typ', models.CharField(choices=[('repas', 'Frais de repas'), ('parking', 'Frais de parking')], max_length=7, verbose_name='Type de frais')),
                ('justif', models.FileField(blank=True, upload_to='justificatifs', verbose_name='Justificatif')),
            ],
        ),
        migrations.CreateModel(
            name='MemOccurrence',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('transport.dispo',),
        ),
        migrations.AddField(
            model_name='trajet',
            name='transport',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trajets', to='transport.transport'),
        ),
        migrations.RunSQL(
            sql="""
CREATE OR REPLACE FUNCTION is_ofas(
  transport_pk int, transport_date date, tarif_avs_acquis boolean, date_naissance date, genre varchar(1)
) returns boolean
language plpgsql as $$
declare is_ofas boolean;
        num_years integer := EXTRACT(YEAR FROM transport_date) - EXTRACT(YEAR FROM date_naissance);
begin
   SELECT ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND (tarif_avs_acquis OR num_years >= 65 OR (genre = 'F' AND num_years >= 64)) into is_ofas from transport_trajet
   WHERE transport_id = transport_pk;
   RETURN is_ofas;
end;$$;""",
        ),
    ]
