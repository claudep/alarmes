from django.db import migrations, models
from django.db.models.functions import TruncMonth

PAS_FACTURER = 99


class StatutChoices(models.IntegerChoices):
    SAISI = 1, "Saisi"
    ATTRIBUE = 2, "Attribué"
    CONFIRME = 3, "Confirmé"
    EFFECTUE = 4, "Effectué"
    RAPPORTE = 5, "Rapporté"
    CONTROLE = 6, "Contrôlé"
    ANNULE = 10, "Annulé"


def populate_transport_fact(apps, schema_editor):
    Transport = apps.get_model('transport', 'Transport')
    Facture = apps.get_model('transport', 'Facture')

    for facture in Facture.objects.all():
        qfilters = {
            'statut__in': [StatutChoices.CONTROLE, StatutChoices.ANNULE],
            'statut_fact__lt': PAS_FACTURER,
            'month': facture.mois_facture,
            'client': facture.client,
        }
        transports = Transport.objects.annotate(
            month=TruncMonth('date')
        ).filter(**qfilters).update(facture=facture)


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0014_transport_facture'),
    ]

    operations = [migrations.RunPython(populate_transport_fact, migrations.RunPython.noop)]
