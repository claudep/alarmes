from django.contrib import admin, messages
from django.db.models.deletion import Collector, ProtectedError
from django.utils.dateformat import format as django_format

from . import models


@admin.register(models.Adresse)
class AdresseAdmin(admin.ModelAdmin):
    list_display = ['nom', 'rue', 'npa', 'localite', 'active']
    search_fields = ['nom', 'rue', 'npa', 'localite']
    actions = ['fusionner']

    @admin.display(boolean=True)
    def active(self, obj):
        return obj.validite.upper is None

    @admin.action(description='Fusionner les deux adresses sélectionnées')
    def fusionner(self, request, queryset):
        if len(queryset) != 2:
            self.message_user(request, "Il faut sélectionner exactement 2 adresses.", level=messages.ERROR)
        good, bad = list(queryset)
        models.Trajet.objects.filter(origine_adr=bad).update(origine_adr=good)
        models.Trajet.objects.filter(destination_adr=bad).update(destination_adr=good)
        collector = Collector(using='default')
        err_msg = "Impossible de supprimer la seconde instance, il reste des dépendances"
        try:
            collector.collect([bad])
        except ProtectedError:
            self.message_user(request, err_msg, level=messages.ERROR)
        else:
            if not collector.dependencies:
                bad.delete()
                self.message_user(request, "La fusion s’est déroulée avec succès")
            else:
                self.message_user(request, err_msg, level=messages.ERROR)


@admin.register(models.Dispo)
class DispoAdmin(admin.ModelAdmin):
    list_display = ['chauffeur', 'debut_avec_jour', 'fin_heure', 'regle', 'fin_recurrence', 'categorie']
    list_filter = ['categorie']
    search_fields = ["chauffeur__persona__nom"]

    def debut_avec_jour(self, obj):
        return django_format(obj.debut, "l j.m.Y H:i")

    @admin.display(description="Jusqu’à")
    def fin_heure(self, obj):
        return obj.fin.strftime("%H:%M")


@admin.register(models.Occurrence)
class OccurrenceAdmin(admin.ModelAdmin):
    list_display = ['chauffeur', 'orig_start', 'annule']
    list_select_related = ['dispo']
    raw_id_fields = ["dispo"]
    search_fields = ["dispo__chauffeur__persona__nom"]
    date_hierarchy = "orig_start"

    @admin.display()
    def chauffeur(self, obj):
        return obj.dispo.chauffeur


@admin.register(models.Facture)
class FactureAdmin(admin.ModelAdmin):
    raw_id_fields = ['client', 'autre_debiteur']
    list_display = ['client', 'mois_facture', 'date_facture', 'montant_total', 'exporte', 'annulee']
    date_hierarchy = "date_facture"
    search_fields = ['client__persona__nom', 'no']


@admin.register(models.Frais)
class FraisAdmin(admin.ModelAdmin):
    raw_id_fields = ['transport']
    list_display = ['get_benev', 'cout', 'descriptif', 'typ']
    search_fields = ["transport__chauffeur__persona__nom"]

    @admin.display(description='Chauffeur')
    def get_benev(self, obj):
        return obj.transport.chauffeur


@admin.register(models.JournalTransport)
class JournalTransportAdmin(admin.ModelAdmin):
    raw_id_fields = ['transport']
    list_display = ['quand', 'transport', 'description', 'qui']


@admin.register(models.Regle)
class RegleAdmin(admin.ModelAdmin):
    list_display = ['nom', 'description', 'frequence', 'params']


@admin.register(models.Trajet)
class TrajetAdmin(admin.ModelAdmin):
    raw_id_fields = ['origine_adr', 'destination_adr', 'transport', 'commun']
    list_display = ['transport', 'heure_depart', 'duree_calc', 'dist_calc']


class TrajetInline(admin.StackedInline):
    model = models.Trajet
    extra = 0
    raw_id_fields = ['origine_adr', 'destination_adr', 'commun']


@admin.register(models.Transport)
class TransportAdmin(admin.ModelAdmin):
    list_display = ['client', 'chauffeur', 'heure_rdv', 'statut']
    list_filter = ["statut", "retour"]
    date_hierarchy = 'date'
    raw_id_fields = ["client", "modele", "chauffeur", "facture"]
    search_fields = ['client__persona__nom']
    inlines = [TrajetInline]


@admin.register(models.TransportModel)
class TransportModelAdmin(admin.ModelAdmin):
    list_display = ['client', 'heure_rdv', 'regle', 'fin_recurrence', 'suspension_depuis']
    search_fields = ['client__persona__nom']
    raw_id_fields = ['client']


@admin.register(models.Vehicule)
class VehiculeAdmin(admin.ModelAdmin):
    list_display = ['modele', 'no_plaque', 'responsable']


@admin.register(models.VehiculeOccup)
class VehiculeOccupAdmin(admin.ModelAdmin):
    list_display = ["vehicule", "client", "duree", "description"]
    raw_id_fields = ["client", "facture"]
