from django.contrib.postgres.fields.ranges import DateRangeField
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0034_benevole_contacts'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='macaron_valide',
            field=DateRangeField(blank=True, null=True, verbose_name='Durée de validité du macaron'),
        ),
    ]
