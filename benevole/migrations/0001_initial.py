import common.fields
import common.models
from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Benevole",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=50, verbose_name="Nom")),
                ("prenom", models.CharField(max_length=50, verbose_name="Prénom")),
                (
                    "genre",
                    models.CharField(
                        blank=True,
                        choices=[("F", "Femme"), ("M", "Homme")],
                        max_length=1,
                        verbose_name="Genre",
                    ),
                ),
                (
                    "date_naissance",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date de naissance"
                    ),
                ),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                ("npa", models.CharField(max_length=5, verbose_name="NPA")),
                ("localite", models.CharField(max_length=30, verbose_name="Localité")),
                (
                    "empl_geo",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.FloatField(), blank=True, null=True, size=2
                    ),
                ),
                (
                    "tel_prive",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. privé"
                    ),
                ),
                (
                    "tel_mobile",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. mobile"
                    ),
                ),
                (
                    "tel_prof",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. prof."
                    ),
                ),
                (
                    "courriel",
                    models.EmailField(
                        blank=True, max_length=254, verbose_name="Courriel"
                    ),
                ),
                (
                    "no_debiteur",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° de débiteur"
                    ),
                ),
                ("formation", models.TextField(blank=True, verbose_name="Formation")),
                (
                    "langues",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            blank=True,
                            choices=[
                                ("fr", "français"),
                                ("de", "allemand"),
                                ("it", "italien"),
                                ("en", "anglais"),
                                ("es", "espagnol"),
                                ("pt", "portugais"),
                                ("tr", "turc"),
                                ("cz", "tchèque"),
                            ],
                            max_length=2,
                        ),
                        blank=True,
                        null=True,
                        size=None,
                    ),
                ),
                (
                    "activites",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            blank=True,
                            choices=[
                                ("installation", "Installation"),
                                ("visite", "Visite"),
                                ("formation", "Formation"),
                                ("transport", "Transport"),
                            ],
                            max_length=15,
                        ),
                        blank=True,
                        null=True,
                        size=None,
                        verbose_name="Activités",
                    ),
                ),
                ("absences", models.TextField(blank=True, verbose_name="Absences")),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "archive_le",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
                (
                    "no_plaques",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="Numéro(s) de plaque"
                    ),
                ),
                (
                    "transport_depuis",
                    models.DateField(
                        blank=True, null=True, verbose_name="Chauffeur depuis"
                    ),
                ),
                (
                    "utilisateur",
                    models.OneToOneField(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            bases=(common.models.GeolocMixin, models.Model),
        ),
    ]
