from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0044_migrate_type_formation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formation',
            name='categorie',
        ),
        migrations.RenameField(
            model_name='formation',
            old_name='categ',
            new_name='categorie',
        ),
        migrations.AlterField(
            model_name='formation',
            name='categorie',
            field=models.ForeignKey(on_delete=models.deletion.PROTECT, to='benevole.typeformation'),
        ),
    ]
