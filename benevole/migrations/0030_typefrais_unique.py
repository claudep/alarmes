from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0029_benevole_formations'),
    ]

    operations = [
        migrations.AlterField(
            model_name='typefrais',
            name='no',
            field=models.CharField(max_length=10, unique=True, verbose_name='No article'),
        ),
    ]
