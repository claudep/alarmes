from django.contrib.postgres.fields.ranges import DateRangeField
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0019_migrate_duree'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activite',
            name='debut',
        ),
        migrations.RemoveField(
            model_name='activite',
            name='fin',
        ),
        migrations.AlterField(
            model_name='activite',
            name='duree',
            field=DateRangeField(verbose_name='Durée'),
        ),
    ]
