from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0042_benevole_ne_pas_defrayer'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeFormation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=60, verbose_name='Type de formation')),
                ('code', models.CharField(max_length=15, verbose_name='Code')),
            ],
        ),
        migrations.AddField(
            model_name='formation',
            name='categ',
            field=models.ForeignKey(null=True, on_delete=models.deletion.PROTECT, to='benevole.typeformation'),
        ),
    ]
