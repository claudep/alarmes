from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0027_remove_benevole_no_debiteur'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='conduit_vehicule',
            field=models.BooleanField(default=False, verbose_name='Conduit les véhicules C-R'),
        ),
    ]
