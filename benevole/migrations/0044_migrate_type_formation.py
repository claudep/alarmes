from django.db import migrations


def migrate_type_formation(apps, schema_editor):
    Formation = apps.get_model('benevole', 'Formation')
    TypeFormation = apps.get_model('benevole', 'TypeFormation')
    cond = TypeFormation.objects.create(nom="Cours de conduite", code="conduite")
    autre = TypeFormation.objects.create(nom="Autre formation", code="autre")
    Formation.objects.filter(categorie="conduite").update(categ=cond)
    Formation.objects.filter(categorie="autre").update(categ=autre)


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0043_typeformation_formation_categ'),
    ]

    operations = [
        migrations.RunPython(migrate_type_formation, migrations.RunPython.noop)
    ]
