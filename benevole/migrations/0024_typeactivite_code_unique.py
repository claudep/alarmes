from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0023_remove_old_activite_field'),
    ]

    operations = [
        migrations.AlterField(
            model_name='typeactivite',
            name='code',
            field=models.CharField(max_length=20, unique=True),
        ),
    ]
