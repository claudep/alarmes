from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0020_remove_activite_debut_fin'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeActivite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=20)),
                ('nom', models.CharField(max_length=50)),
                ('service', models.CharField(blank=True, choices=[
                    ('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'),
                    ('savd', 'Soins à domicile')
                ], max_length=12)),
            ],
        ),
        migrations.AddField(
            model_name='activite',
            name='type_act',
            field=models.ForeignKey(default=None, on_delete=models.deletion.PROTECT, to='benevole.typeactivite', null=True),
            preserve_default=False,
        ),
    ]
