from django.db import migrations


def migrate_type_activite(apps, schema_editor):
    Activite = apps.get_model('benevole', 'Activite')
    TypeActivite = apps.get_model('benevole', 'TypeActivite')
    TypeActivite.objects.bulk_create([
        TypeActivite(code='installation', nom='Alarme (installation)', service='alarme'),
        TypeActivite(code='visite', nom='Alarme (visite)', service='alarme'),
        TypeActivite(code='formation', nom='Alarme (formation)', service='alarme'),
        TypeActivite(code='transport', nom='Transports', service='transport'),
        TypeActivite(code='visites', nom='Visites', service='visite'),
        TypeActivite(code='cafe', nom='Café Rencontres', service=''),
        TypeActivite(code='boutique', nom='Boutiques', service=''),
        TypeActivite(code='maml', nom='MAML', service=''),
        TypeActivite(code='tische', nom='FT-HT', service=''),
        TypeActivite(code='mdc', nom='MDC', service=''),
        TypeActivite(code='amf', nom='AMF', service=''),
    ])
    for act in Activite.objects.all():
        act.type_act = TypeActivite.objects.get(code=act.activite.lower())
        act.save()


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0021_typeactivite'),
    ]

    operations = [
        migrations.RunPython(migrate_type_activite),
    ]
