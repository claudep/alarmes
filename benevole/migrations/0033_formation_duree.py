from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0032_benevole_vehicule_nb_portes'),
    ]

    operations = [
        migrations.AddField(
            model_name='formation',
            name='duree',
            field=models.DurationField(blank=True, null=True, verbose_name='Durée de la formation'),
        ),
    ]
