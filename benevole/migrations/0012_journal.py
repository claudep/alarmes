from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('benevole', '0011_remove_benevole_champs_activites'),
    ]

    operations = [
        migrations.CreateModel(
            name='Journal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('quand', models.DateTimeField()),
                ('auto', models.BooleanField(default=True)),
                ('benevole', models.ForeignKey(
                    on_delete=models.deletion.CASCADE, related_name='journaux', to='benevole.benevole'
                )),
                ('qui', models.ForeignKey(
                    blank=True, null=True, on_delete=models.deletion.SET_NULL, related_name='+',
                    to=settings.AUTH_USER_MODEL
                )),
            ],
            options={
                'get_latest_by': 'quand',
            },
        ),
        migrations.AlterModelOptions(
            name='activite',
            options={'verbose_name': 'Activité'},
        ),
    ]
