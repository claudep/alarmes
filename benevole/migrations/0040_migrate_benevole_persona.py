from django.db import migrations
from django.core.exceptions import ObjectDoesNotExist


def migrate_benevoles(apps, schema_editor):
    Benevole = apps.get_model('benevole', 'Benevole')
    Journal = apps.get_model('benevole', 'Journal')
    Persona = apps.get_model('client', 'Persona')
    JournalClient = apps.get_model('client', 'Journal')
    AdresseClient = apps.get_model('client', 'AdresseClient')
    Telephone = apps.get_model('client', 'Telephone')
    Fichier = apps.get_model('common', 'Fichier')
    ContentType = apps.get_model("contenttypes", "ContentType")
    fields = [
        "nom", "prenom", "genre", "date_naissance", "courriel", "langues",
    ]
    benevole_ct = ContentType.objects.get_for_model(Benevole)
    persona_ct = ContentType.objects.get_for_model(Persona)
    for benevole in Benevole.objects.all():
        try:
            prem_journ = benevole.journaux.earliest().quand.date()
        except Journal.DoesNotExist:
            prem_journ = None
        try:
            debut_act = benevole.activite_set.earliest("duree__startswith").duree.lower
        except ObjectDoesNotExist:
            debut_act = None
        date_cree = min([dt for dt in [prem_journ, debut_act] if dt])
        benevole.persona = Persona.objects.create(
            cree_le=date_cree, **{f: getattr(benevole, f) for f in fields}
        )
        benevole.save(update_fields=["persona"])
        idx = 1
        if benevole.tel_prive:
            Telephone.objects.create(persona=benevole.persona, tel=benevole.tel_prive, priorite=idx)
            idx += 1
        if benevole.tel_mobile:
            Telephone.objects.create(persona=benevole.persona, tel=benevole.tel_mobile, priorite=idx)
            idx += 1
        if benevole.tel_prof:
            Telephone.objects.create(persona=benevole.persona, tel=benevole.tel_prof, remarque="N° prof.", priorite=idx)

        AdresseClient.objects.create(
            persona=benevole.persona, rue=benevole.rue, npa=benevole.npa, localite=benevole.localite,
            pays=benevole.pays, empl_geo=benevole.empl_geo, principale=(date_cree, None)
        )
        Fichier.objects.filter(content_type=benevole_ct, object_id=benevole.pk).update(
            content_type=persona_ct, object_id=benevole.persona_id
        )
        for journal in benevole.journaux.all():
            JournalClient.objects.create(
                persona=benevole.persona,
                description=journal.description,
                quand=journal.quand,
                qui=journal.qui,
                auto=journal.auto,
            )


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('benevole', '0039_benevole_persona'),
        ('common', '0005_fichier'),
        ('client', '__first__'),
    ]

    operations = [
        migrations.RunPython(migrate_benevoles, migrations.RunPython.noop),
    ]
