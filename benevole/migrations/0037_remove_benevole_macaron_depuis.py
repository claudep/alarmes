from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0036_migration_macaron'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='benevole',
            name='macaron_depuis',
        ),
    ]
