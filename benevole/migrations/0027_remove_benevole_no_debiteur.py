from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0026_alter_benevole_langues'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='benevole',
            name='no_debiteur',
        ),
    ]
