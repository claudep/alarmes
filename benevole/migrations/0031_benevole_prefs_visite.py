from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0030_typefrais_unique'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='prefs_visite',
            field=models.TextField(blank=True, verbose_name='Préférences pour les visites'),
        ),
    ]
