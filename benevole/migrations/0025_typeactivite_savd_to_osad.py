from django.db import migrations, models


def savd_to_osad(apps, schema_editor):
    TypeActivite = apps.get_model('benevole', 'TypeActivite')
    TypeActivite.objects.filter(service='savd').update(service='osad')


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0024_typeactivite_code_unique'),
    ]

    operations = [
        migrations.AlterField(
            model_name='typeactivite',
            name='service',
            field=models.CharField(blank=True, choices=[('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD')], max_length=12),
        ),
        migrations.RunPython(savd_to_osad),
    ]
