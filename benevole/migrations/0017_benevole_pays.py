from django.db import migrations, models

from common.choices import Pays

class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0016_lignefrais_montant_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='pays',
            field=models.CharField(choices=Pays, default='CH', max_length=2, verbose_name='Pays'),
        ),
    ]
