from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0014_lignefrais_quantite_montant_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='typefrais',
            name='montant_fixe',
            field=models.BooleanField(
                default=True,
                help_text='Indique si le prix est fixé pour l’article ou si le montant peut varier',
                verbose_name='Montant fixe'),
        ),
    ]
