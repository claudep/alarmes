from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0031_benevole_prefs_visite'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='nb_portes_vhc',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre de portes'),
        ),
        migrations.AddField(
            model_name='benevole',
            name='vehicule',
            field=models.CharField(blank=True, max_length=50, verbose_name='Type de véhicule'),
        ),
    ]
