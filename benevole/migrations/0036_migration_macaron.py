from datetime import timedelta

from django.db import migrations


def migrate_macaron(apps, schema_editor):
    Benevole = apps.get_model('benevole', 'Benevole')
    for benev in Benevole.objects.filter(macaron_depuis__isnull=False):
        benev.macaron_valide = (benev.macaron_depuis, benev.macaron_depuis + timedelta(days=365))
        benev.save(update_fields=["macaron_valide"])


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0035_benevole_macaron_valide'),
    ]

    operations = [
        migrations.RunPython(migrate_macaron)
    ]
