from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0033_formation_duree'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='contacts',
            field=models.TextField(blank=True, verbose_name='Contact en cas d’urgence'),
        ),
    ]
