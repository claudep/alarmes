from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0006_benevole_id_externe"),
    ]

    operations = [
        migrations.AlterField(
            model_name="benevole",
            name="genre",
            field=models.CharField(
                choices=[("F", "Femme"), ("M", "Homme")],
                max_length=1,
                verbose_name="Genre",
            ),
        ),
    ]
