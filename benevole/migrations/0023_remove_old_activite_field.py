from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0022_migrate_type_activite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activite',
            name='activite',
        ),
        migrations.AlterField(
            model_name='activite',
            name='type_act',
            field=models.ForeignKey(on_delete=models.deletion.PROTECT, to='benevole.typeactivite'),
        ),
    ]
