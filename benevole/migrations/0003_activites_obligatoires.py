import common.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0002_benevole_macaron_depuis"),
    ]

    operations = [
        migrations.AlterField(
            model_name="benevole",
            name="activites",
            field=common.fields.ChoiceArrayField(
                base_field=models.CharField(
                    blank=True,
                    choices=[
                        ("installation", "Installation"),
                        ("visite", "Visite alarme"),
                        ("formation", "Formation"),
                        ("transport", "Transport"),
                        ("visites", "Visites"),
                    ],
                    max_length=15,
                ),
                default=[],
                size=None,
                verbose_name="Activités",
            ),
            preserve_default=False,
        ),
    ]
