from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0028_benevole_conduit_vehicule'),
    ]

    operations = [
        migrations.CreateModel(
            name='Formation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=200, verbose_name='Titre')),
                ('descriptif', models.TextField(blank=True, verbose_name='Descriptif')),
                ('categorie', models.CharField(choices=[('conduite', 'Cours de conduite'), ('autre', 'Autre formation')], max_length=12, verbose_name='Catégorie')),
                ('quand', models.DateField(verbose_name='Date de la formation')),
                ('attestation', models.FileField(blank=True, upload_to='attestations', verbose_name='Attestation')),
            ],
        ),
        migrations.AddField(
            model_name='benevole',
            name='formations',
            field=models.ManyToManyField(blank=True, to='benevole.formation'),
        ),
    ]
