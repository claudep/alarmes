from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0038_benevole_no_macaron'),
        ('client', '0066_remove_client_persona_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='persona',
            field=models.OneToOneField(null=True, on_delete=models.deletion.PROTECT, to='client.persona'),
        ),
    ]
