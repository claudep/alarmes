from django.contrib.postgres.fields import ArrayField
from django.db import migrations, models

from common.choices import Languages


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0025_typeactivite_savd_to_osad'),
    ]

    operations = [
        migrations.AlterField(
            model_name='benevole',
            name='langues',
            field=ArrayField(
                base_field=models.CharField(blank=True, choices=Languages.choices(), max_length=3),
                blank=True, null=True, size=None
            ),
        ),
    ]
