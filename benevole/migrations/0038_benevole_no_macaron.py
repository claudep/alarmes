from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0037_remove_benevole_macaron_depuis'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='no_macaron',
            field=models.CharField(blank=True, max_length=15, verbose_name='Numéro du macaron'),
        ),
    ]
