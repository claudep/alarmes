from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0041_remove_benevole_obsolete_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='benevole',
            name='ne_pas_defrayer',
            field=models.BooleanField(default=False, verbose_name='Ne pas défrayer',
            help_text="Ce bénévole ne souhaite jamais être défrayé (repas, déplacements, etc.)"),
        ),
    ]
