from django.db import migrations


def migrate_duree(apps, schema_editor):
    Activite = apps.get_model('benevole', 'Activite')
    # Not possible (https://code.djangoproject.com/ticket/25591):
    # Activite.objects.update(duree=(F('debut'), F('fin')))
    for act in Activite.objects.all():
        act.duree = (act.debut, act.fin)
        act.save()


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0018_activite_duree'),
    ]

    operations = [
        migrations.RunPython(migrate_duree)
    ]
