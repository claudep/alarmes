from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="benevole",
            name="macaron_depuis",
            field=models.DateField(
                blank=True, null=True, verbose_name="Date d’obtention du macaron"
            ),
        ),
    ]
