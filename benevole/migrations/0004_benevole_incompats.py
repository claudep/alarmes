from django.db import migrations, models

from common.choices import Handicaps
from common.fields import ChoiceArrayField


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0003_activites_obligatoires"),
    ]

    operations = [
        migrations.AddField(
            model_name="benevole",
            name="incompats",
            field=ChoiceArrayField(
                base_field=models.CharField(
                    blank=True,
                    choices=Handicaps.choices,
                    max_length=10,
                ),
                blank=True,
                null=True,
                size=None,
                verbose_name="Incompatibilités",
            ),
        ),
    ]
