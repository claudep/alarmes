from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0040_migrate_benevole_persona'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='benevole',
            name='courriel',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='date_naissance',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='empl_geo',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='genre',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='langues',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='localite',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='nom',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='npa',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='pays',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='prenom',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='rue',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='tel_mobile',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='tel_prive',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='tel_prof',
        ),
        migrations.DeleteModel(
            name='Journal',
        ),
    ]
