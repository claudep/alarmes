import common.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0012_journal'),
    ]

    operations = [
        migrations.CreateModel(
            name='TypeFrais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no', models.CharField(max_length=10, verbose_name='No article')),
                ('libelle', models.CharField(max_length=100, verbose_name='Libellé')),
                ('services', common.fields.ChoiceArrayField(base_field=models.CharField(choices=[
                    ('alarme', 'Alarme'), ('transport', 'Transports'), ('visite', 'Visites'), ('osad', 'OSAD')
                ], max_length=10), size=None)),
            ],
            options={'verbose_name': 'Type de frais', 'verbose_name_plural': 'Types de frais'},
        ),
        migrations.CreateModel(
            name='LigneFrais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('montant', models.DecimalField(decimal_places=2, max_digits=7)),
                ('libelle', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='benevole.typefrais')),
                ('note', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lignes', to='benevole.notefrais')),
            ],
            options={'verbose_name': 'Ligne de frais', 'verbose_name_plural': 'Lignes de frais'},
        ),
        migrations.RemoveField(
            model_name='notefrais',
            name='montant',
        ),
    ]
