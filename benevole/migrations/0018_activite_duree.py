from django.contrib.postgres.fields import DateRangeField
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0017_benevole_pays'),
    ]

    operations = [
        migrations.AddField(
            model_name='activite',
            name='duree',
            field=DateRangeField(blank=True, null=True, verbose_name='Durée'),
        ),
    ]
