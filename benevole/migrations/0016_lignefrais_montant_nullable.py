from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0015_typefrais_montant_fixe'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lignefrais',
            name='montant_unit',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True),
        ),
    ]
