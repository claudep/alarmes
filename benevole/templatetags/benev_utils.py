from django.template import Library

register = Library()


@register.filter
def liste_classes(benev):
    """Classes CSS pour ligne bénévole en mode liste."""
    classes = ["inactif"] if benev.inactif() else []
    return " ".join(classes)
