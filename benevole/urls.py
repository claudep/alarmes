from django.urls import path

from . import views

urlpatterns = [
    path('<str:app>/', views.BenevoleListView.as_view(), name='benevoles'),
    path('<str:app>/archives/', views.BenevoleListView.as_view(is_archive=True), name='benevoles-archives'),
    path('<str:app>/nouveau/', views.BenevoleEditView.as_view(is_create=True), name='benevole-new'),
    path('<str:app>/<int:pk>/edition/', views.BenevoleEditView.as_view(is_create=False),
        name='benevole-edit'),
    path('<str:app>/<int:pk>/creercompte/', views.BenevoleCreateAccountView.as_view(),
        name='benevole-new-account'),
    path('<str:app>/autocomplete/', views.BenevoleAutocompleteView.as_view(),
        name='benevole-autocomplete'),
    path('<str:app>/<int:pk>/archive/', views.BenevoleArchiveView.as_view(), name='benevole-archive'),
    path('<str:app>/<int:pk>/notesfrais/', views.BenevoleNotesFraisView.as_view(),
        name='benevole-notesfrais'),
    path('<str:app>/notesfrais/<int:pk>/refaire/', views.BenevoleNotesFraisRefaire.as_view(),
        name='benevole-notesfrais-refaire'),
    # Formations
    path('<str:app>/<int:pk>/formations/', views.BenevoleFormationsListView.as_view(),
        name='benevole-formations'),
    path('<str:app>/<int:pk>/formations/nouvelle/', views.BenevoleFormationEditView.as_view(is_create=True),
        name='benevole-formation-add'),
    path('<str:app>/<int:pk>/formations/<int:pk_form>/edition/', views.BenevoleFormationEditView.as_view(is_create=False),
        name='benevole-formation-edit'),
    path('<str:app>/<int:pk>/formations/<int:pk_form>/supprimer/', views.BenevoleFormationDeleteView.as_view(),
        name='benevole-formation-delete'),
    path('<str:app>/formations/export/', views.BenevoleFormationsExportView.as_view(),
        name='benevole-formations-export'),
    # Journal
    path('<str:app>/<int:pk>/journal/', views.BenevoleJournalView.as_view(), name='benevole-journal'),
    path('<str:app>/<int:pk>/journal/nouveau/', views.BenevoleJournalEditView.as_view(is_create=True),
        name='benevole-journal-add'),
    path('<str:app>/<int:pk>/journal/<int:pk_jr>/edition/', views.BenevoleJournalEditView.as_view(is_create=False),
        name='benevole-journal-edit'),
    path('<str:app>/<int:pk>/journal/<int:pk_jr>/supprimer/', views.BenevoleJournalDeleteView.as_view(),
        name='benevole-journal-delete'),
    # Fichiers
    path('<int:pk>/fichier/nouveau/', views.BenevoleFichierEditView.as_view(is_create=True),
        name='benevole-fichier-add'),
    path('<int:pk>/fichier/<int:pk_file>/edition/', views.BenevoleFichierEditView.as_view(is_create=False),
        name='benevole-fichier-edit'),
    path('<int:pk>/fichier/liste/', views.BenevoleFichierListeView.as_view(),
        name='benevole-fichier-list'),
    path('<int:pk>/supprimer/', views.BenevoleFichierDeleteView.as_view(),
        name='benevole-fichier-delete'),
]
