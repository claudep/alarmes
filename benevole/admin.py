from datetime import date

from django.conf import settings
from django.contrib import admin
from django.db.models import Prefetch

from common.admin import NullableAsBooleanListFilter, ExportAction
from .models import (
    Activite, Benevole, Formation, LigneFrais, NoteFrais, TypeActivite,
    TypeFormation, TypeFrais, benevole_saved
)


class ActiviteBenevoleListFilter(admin.SimpleListFilter):
    title = "Activité"
    parameter_name = "activite"

    def lookups(self, request, model_admin):
        return [(ta.code, ta.nom) for ta in TypeActivite.objects.order_by('service')]

    def queryset(self, request, queryset):
        val = self.value()
        if val:
            return queryset.par_activite(val)
        return queryset


class ActiviteInline(admin.TabularInline):
    model = Activite
    extra = 0


@admin.register(Benevole)
class BenevoleAdmin(admin.ModelAdmin):
    ordering = ['persona__nom']
    list_display = ['persona', 'rue', 'npa', 'localite', 'activites', 'archive_le', 'actif']
    list_filter = [ActiviteBenevoleListFilter, ('archive_le', NullableAsBooleanListFilter)]
    search_fields = ['persona__nom']
    raw_id_fields = ["persona"]
    inlines = [ActiviteInline]
    actions = [
        ExportAction("Liste bénévoles"), 'sync_benev',
    ]

    def get_queryset(self, request):
        return super().get_queryset(request).avec_adresse(date.today()).prefetch_related(
            Prefetch("activite_set", queryset=Activite.objects.select_related("type_act"))
        )

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.archive_le is None

    def rue(self, obj):
        return (obj.adresse_active or {}).get('rue', '')

    def npa(self, obj):
        return (obj.adresse_active or {}).get('npa', '')

    def localite(self, obj):
        return (obj.adresse_active or {}).get('localite', '')

    def activites(self, obj):
        return [act.type_act.nom for act in obj.activites_en_cours()]

    @admin.action(description="Synchroniser le bénévole avec l’ERP")
    def sync_benev(modeladmin, request, queryset):
        from api.signals import benevole_sync_receiver
        for benev in queryset.all():
            benevole_sync_receiver(sender=Benevole, instance=benev, created=False, force=True)

    def get_actions(self, request):
        actions = super().get_actions(request)
        try:
            can_sync = 'benevoles' in settings.CID_SYNC_ENABLE
        except AttributeError:
            can_sync = False
        if not can_sync or not request.user.is_superuser:
            actions.pop('sync_benev', None)
        return actions

    def save_model(self, request, obj, *args, **kwargs):
        self.is_new = obj.pk is None
        super().save_model(request, obj, *args, **kwargs)

    def save_related(self, request, form, formsets, *args, **kwargs):
        fset_changed = formsets[0].has_changed()
        super().save_related(request, form, formsets, *args, **kwargs)
        if fset_changed:
            del form.instance._prefetched_objects_cache
            benevole_saved.send(sender=Benevole, instance=form.instance, created=self.is_new)


@admin.register(TypeActivite)
class TypeActiviteAdmin(admin.ModelAdmin):
    list_display = ['code', 'nom', 'service']


@admin.register(TypeFormation)
class TypeFormationAdmin(admin.ModelAdmin):
    list_display = ["nom", "code"]


@admin.register(Formation)
class FormationAdmin(admin.ModelAdmin):
    list_display = ['titre', 'categorie', 'quand']
    search_fields = ['titre']


class LigneFraisInline(admin.TabularInline):
    model = LigneFrais
    extra = 0


@admin.register(NoteFrais)
class NoteFraisAdmin(admin.ModelAdmin):
    list_display = ['benevole', 'service', 'mois', 'kms', 'date_export']
    list_filter = ['service']
    date_hierarchy = 'mois'
    search_fields = ['benevole__persona__nom']
    inlines = [LigneFraisInline]


@admin.register(TypeFrais)
class TypeFraisAdmin(admin.ModelAdmin):
    list_display = ['no', 'libelle', 'services', 'montant_fixe']
    ordering = ['no']


@admin.register(LigneFrais)
class LigneFraisAdmin(admin.ModelAdmin):
    list_display = ['note', 'libelle', 'montant']
    list_filter = ["libelle"]
    raw_id_fields = ["note"]
