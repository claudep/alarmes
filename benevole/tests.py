from datetime import date, datetime, time, timedelta
from pathlib import Path
from unittest.mock import patch

from django.contrib.auth.models import Group, Permission
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import get_current_timezone, now

from client.models import Client, Journal, Persona
from common.choices import Languages, Services
from common.export import openxml_contenttype
from common.models import Fichier, Utilisateur
from common.test_utils import BaseDataMixin, TempMediaRootMixin, mocked_httpx, read_xlsx
from common.utils import canton_app, current_app

from .forms import BenevoleForm
from .models import Activite, Benevole, Formation, NoteFrais, TypeActivite, TypeFormation

NOT_SET = object()


class BenevoleTestsMixin(BaseDataMixin, TempMediaRootMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            Permission.objects.get(codename='view_benevole'),
            Permission.objects.get(codename='change_benevole')
        )
        Group.objects.bulk_create([
            Group(name='Bénévoles'),
            Group(name='Intervenants alarme'),
        ])
        cls.benevole_list_url = (
            reverse('intervenants') if current_app() == 'alarme'
            else reverse('benevoles', args=[current_app()])
        )
        cls.typef = TypeFormation.objects.create(nom="Cours de conduite", code="conduite")

    def assertJournalMessage(self, obj, message, user=NOT_SET):
        try:
            journal = obj.persona.journaux.latest()
        except ObjectDoesNotExist:
            self.fail("Aucune entrée de journal trouvée")
        self.assertEqual(journal.description, message)
        self.assertEqual(journal.qui, self.user if user is NOT_SET else user)


class BenevoleTests(BenevoleTestsMixin):
    def test_benevole_new(self):
        app = current_app()
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-new', args=[app]))
        self.assertContains(
            response,
            '<input type="text" name="nom" maxlength="60" class="form-control" required="" id="id_nom">',
            html=True
        )
        # Test for custom daterangefield widget
        self.assertContains(
            response,
            '<label for="id_activite_set-0-duree_0" class="d-inline">Depuis le&nbsp;:</label>',
            html=True
        )
        form_data = {
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'genre': 'M',
            'rue': "Rue des Fleurs 3",
            'npalocalite': '2345 Petaouchnok',
            'courriel': 'dupond@example.org',
            'langues': ['fr', 'it'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '0',
            'telephone_set-TOTAL_FORMS': '0',
            'telephone_set-INITIAL_FORMS': '0',
        }
        response = self.client.post(reverse('benevole-new', args=[app]), data=form_data)
        self.assertEqual(
            response.context['form'].act_formset.errors,
            [{'type_act': ['Ce champ est obligatoire.'], 'duree': ['Ce champ est obligatoire.']}]
        )
        form_data.update({
            'activite_set-0-benevole': '',
            'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
            'activite_set-0-duree_0': date.today().strftime('%d.%m.%Y'),
        })
        with patch('httpx.get', side_effect=mocked_httpx) as mock:
            response = self.client.post(reverse('benevole-new', args=[app]), data=form_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors or response.context['form'].act_formset.errors)
        self.assertRedirects(response, self.benevole_list_url)
        benevole = Benevole.objects.get(persona__nom='Dupond')
        self.assertEqual(str(benevole), "Dupond Ladislas")
        self.assertEqual(benevole.langues, ['fr', 'it'])
        self.assertEqual(benevole.adresse().empl_geo, [1, 2])
        self.assertEqual(benevole.activite_set.count(), 1)
        self.assertEqual(benevole.activite_set.first().duree.lower, date.today())
        self.assertEqual(benevole.activite_set.first().duree.upper, None)
        self.assertEqual(benevole.journaux.latest().description, "Création du bénévole")
        self.assertEqual(
            set([grp.name for grp in benevole.utilisateur.groups.all()]),
            {'Bénévoles'}
        )
        # 2ème soumission même données doit échouer
        response = self.client.post(reverse('benevole-new', args=[app]), data=form_data)
        self.assertContains(
            response, "Il existe déjà un bénévole avec le même nom et la même date de naissance"
        )
        # Now edit
        response = self.client.get(reverse('benevole-edit', args=[app, benevole.pk]))
        it_index = list(Languages.keys()).index('it')
        self.assertContains(
            response,
            f'<input type="checkbox" name="langues" value="it" id="id_langues_{it_index}" '
            'class="form-check-input" checked>',
            html=True
        )
        form_data['langues'].append('en')
        form_data.update({
            'activite_set-TOTAL_FORMS': '2',
            'activite_set-INITIAL_FORMS': '1',
            'activite_set-0-benevole': benevole.pk,
            'activite_set-0-id': benevole.activite_set.first().pk,
            'activite_set-1-benevole': benevole.pk,
            'activite_set-1-id': '',
            'activite_set-1-type_act': '',
            'activite_set-1-duree_0': '',
        })
        response = self.client.post(reverse('benevole-edit', args=[app, benevole.pk]), data=form_data)
        self.assertRedirects(response, self.benevole_list_url)
        benevole.refresh_from_db()
        self.assertEqual(benevole.activite_set.count(), 1)
        self.assertEqual(benevole.activite_set.first().type_act.code, 'installation')
        self.assertEqual(benevole.langues, ['fr', 'it', 'en'])
        self.assertEqual(
            benevole.journaux.latest().description,
            "Modification du bénévole: langues (de «français, italien» à «français, italien, anglais»)"
        )
        response = self.client.get(reverse('benevole-journal', args=[app, benevole.pk]))
        self.assertQuerySetEqual(response.context['object_list'], benevole.journaux.order_by('-quand'))

    def test_benevole_form_durees_egales(self):
        form_data = {
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'genre': 'M',
            'npalocalite': '2345 Petaouchnok',
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '0',
            'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
            'activite_set-0-duree_0': '2024-08-01',
            'activite_set-0-duree_1': '2024-08-01',
        }
        form = BenevoleForm(instance=None, data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.act_formset.forms[0].errors,
            {'duree': ['La valeur supérieure doit être plus grande que la valeur inférieure']}
        )

    def test_benevole_list(self):
        """Test des listes de bénévoles alarme et transport, actifs et archivés."""
        il_y_a_2_mois = (datetime.now() - timedelta(days=60)).date()
        benevs = [
            self._create_benevole(
                nom='Müller', prenom='Irma', genre='F', langues=['fr'],
                npa="2800", localite="Delémont",
                activites=[TypeActivite.INSTALLATION],
                date_naissance=date.today() - timedelta(days=77 * 365.25),
            ),
            self._create_benevole(
                nom='Vieux', prenom='Alarme', genre='F',
                activites=[TypeActivite.INSTALLATION],
                archive_le=il_y_a_2_mois, langues=['fr'],
            ),
            self._create_benevole(
                nom='Schmid', prenom='Marco', genre='M', langues=['fr'],
                activites=[TypeActivite.TRANSPORT],
                date_naissance=date.today() - timedelta(days=62 * 365.25),
            ),
            self._create_benevole(
                nom='Vieux', prenom='Transp', genre='M',
                activites=[TypeActivite.TRANSPORT],
                archive_le=il_y_a_2_mois, langues=['fr'],
            ),
            self._create_benevole(
                nom='SansActivité', prenom='Nina', genre='F', langues=['fr'],
                activites=[]
            ),
        ]
        # Fin d'activité dans le futur ne doit pas affecter les listes
        Activite.objects.filter(benevole__persona__nom='Müller').update(
            duree=(date.today(), date.today() + timedelta(days=30))
        )
        # Début d'activité dans le futur (supposé proche) doit compter comme
        # une activité en cours.
        Activite.objects.filter(benevole__persona__nom='Schmid').update(
            duree=(date.today() + timedelta(days=3), None)
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['alarme']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        self.assertContains(response, "2800 Delémont")
        response = self.client.get(reverse('benevoles-archives', args=['alarme']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[1]])
        response = self.client.get(reverse('benevoles', args=['transport']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[2]])
        response = self.client.get(reverse('benevoles-archives', args=['transport']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[3]])
        # Filter
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?nom=mull')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?nom=xyz')
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?tel=078')
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?npa_localite=dele')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?activites=all')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0], benevs[4], benevs[2]])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?activites=all&nom=sans')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[4]])
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?age=>75')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?age=77')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?age=>=80')
        self.assertQuerySetEqual(response.context['object_list'], [])
        # Export
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_benevole_list_temp_inactif(self):
        """La note inscrite sur une activité en pause apparaît en mode liste."""
        app = current_app()
        try:
            activites = [TypeActivite.par_domaine(app)[0].code]
        except IndexError:
            self.skipTest("Aucun activité bénévole pour l'app {app}")
        benev = self._create_benevole(activites=activites)
        benev.activite_set.update(inactif=True, note="En voyage jusqu’en fin d’année")
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=[app]))
        self.assertQuerySetEqual(response.context['object_list'], [benev])
        self.assertContains(
            response,
            '<div class="inactif_msg">En voyage jusqu’en fin d’année</div>'
        )

    def test_get_activites_display(self):
        b1 = self._create_benevole(
            activites=[TypeActivite.INSTALLATION, TypeActivite.VISITE_ALARME],
        )
        self.assertEqual(b1.get_activites_display(), "Alarme (installation), Alarme (visite)")
        today = date.today()
        act_vis = b1.activite_set.get(type_act__code=TypeActivite.VISITE_ALARME)
        act_vis.duree = (today, today + timedelta(days=3))
        act_vis.save()
        self.assertEqual(b1.get_activites_display(), "Alarme (installation), Alarme (visite)")
        act_vis.duree = (today - timedelta(days=30), today - timedelta(days=3))
        act_vis.save()
        self.assertEqual(b1.get_activites_display(), "Alarme (installation)")

    def test_benevole_activite_future(self):
        """
        Une activité démarrant dans le futur est incluse dans les activités en cours,
        sachant que ce futur est très généralement très proche.
        """
        b1 = self._create_benevole()
        activite = b1.activite_set.first()
        activite.duree = (date.today() + timedelta(days=2), None)
        activite.save()
        self.assertEqual(len(b1.activites_en_cours()), 1)

    def test_benevole_par_activite(self):
        today = date.today()
        b1 = self._create_benevole(
            activites=[TypeActivite.INSTALLATION, TypeActivite.VISITE_ALARME],
        )
        # Pas OK: archivé
        b_arch = self._create_benevole(
            nom='Archive', prenom='Marco', genre='M', langues=['fr'], activites=[TypeActivite.VISITE_ALARME],
            archive_le=today,
        )
        # Pas OK: autre activite
        self._create_benevole(nom='Transport', activites=[TypeActivite.TRANSPORT])
        # Pas OK: activite concernée est terminée
        b4 = self._create_benevole(
            nom='Transport-Alarme', activites=[TypeActivite.TRANSPORT, TypeActivite.VISITE_ALARME],
        )
        b4.activite_set.filter(type_act__code=TypeActivite.VISITE_ALARME).update(
            duree=(today - timedelta(days=30), today - timedelta(days=1))
        )
        # OK: activité concernée jusqu'à demain
        b5 = self._create_benevole(
            nom='Transport-Alarme2', activites=[TypeActivite.TRANSPORT, TypeActivite.VISITE_ALARME],
        )
        b5.activite_set.filter(type_act__code=TypeActivite.VISITE_ALARME).update(
            duree=(today, today + timedelta(days=1))
        )

        self.assertQuerySetEqual(
            Benevole.objects.par_activite(TypeActivite.VISITE_ALARME), [b1, b5]
        )
        self.assertQuerySetEqual(
            Benevole.objects.par_activite(TypeActivite.VISITE_ALARME, include=b4),
            [b1, b4, b5]
        )
        self.assertQuerySetEqual(
            Benevole.objects.par_activite(
                TypeActivite.VISITE_ALARME, depuis=today - timedelta(days=5), jusqua=today
            ),
            [b1, b_arch, b4, b5]
        )

    def test_benevole_par_domaine(self):
        b1 = self._create_benevole(nom='Transport', activites=[TypeActivite.TRANSPORT])
        b2 = self._create_benevole(nom='Archive', activites=[TypeActivite.TRANSPORT], archive_le=date.today())
        b3 = self._create_benevole(activites=[TypeActivite.INSTALLATION, TypeActivite.VISITE_ALARME])
        b_inactif = self._create_benevole(
            nom='Transport-Alarme', activites=[TypeActivite.TRANSPORT, TypeActivite.INSTALLATION],
        )
        b_inactif.activite_set.filter(
            type_act=TypeActivite.objects.get(code=TypeActivite.INSTALLATION)
        ).update(inactif=True)
        # Début d'activité dans le futur (supposé proche) doit compter comme
        # une activité en cours.
        Activite.objects.filter(benevole__persona__nom='Transport').update(
            duree=(date.today() + timedelta(days=3), None)
        )

        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport'), [b1, b_inactif])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport', actifs=None), [b2, b1, b_inactif])
        self.assertQuerySetEqual(
            Benevole.objects.par_domaine('transport', depuis=date.today() - timedelta(days=30), jusqua=date.today()),
            [b2, b_inactif]
        )
        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport', include=b3), [b3, b1, b_inactif])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('alarme'), [b3])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('alarme', actifs=None), [b3, b_inactif])
        self.assertQuerySetEqual(
            Benevole.objects.par_domaine('alarme', depuis=date.today() - timedelta(days=30), jusqua=date.today()),
            [b3]
        )

    def test_benevole_autocomplete(self):
        benevole = self._create_benevole(
            nom='Müller', prenom='Irma', rue="Av du Stand 12", npa="2300", localite="La Chaux-de-Fonds",
            activites=[TypeActivite.INSTALLATION, TypeActivite.VISITE_ALARME]
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-autocomplete', args=['tous']) + '?q=mul')
        self.assertEqual(response.json(), [{
            'label': 'Müller Irma', 'value': benevole.pk,
            'adresse': 'Av du Stand 12, 2300 La Chaux-de-Fonds',
            'types': ['installation', 'visite'],
            'url': benevole.get_absolute_url(),
        }])

    def test_benevole_creer_compte(self):
        benevole = self._create_benevole(
            nom='Müller', prenom='Irma', genre='F', courriel='irma@example.org',
            activites=[TypeActivite.INSTALLATION],
        )
        self.assertIsNone(benevole.utilisateur)
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('benevole-new-account', args=[current_app(), benevole.pk]),
            data={}
        )
        self.assertRedirects(response, reverse('benevole-edit', args=[current_app(), benevole.pk]))
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("Compte pour application", mail.outbox[0].subject)
        benevole.refresh_from_db()
        self.assertEqual(benevole.utilisateur.first_name, 'Irma')
        self.assertEqual(benevole.utilisateur.last_name, 'Müller')
        self.assertEqual(benevole.utilisateur.email, 'irma@example.org')
        self.assertEqual(
            set([grp.name for grp in benevole.utilisateur.groups.all()]),
            {'Bénévoles'}
        )

    def test_benevole_compte_inactif(self):
        benevole = self._create_benevole(
            utilisateur=Utilisateur.objects.create_user("irma@example.org", "pw", is_active=False)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-edit', args=[current_app(), benevole.pk]))
        self.assertContains(response, "Le compte de connexion de cette personne est désactivé.")

    def test_benevole_archiver_par_fin_activite(self):
        """Plus d'activité en cours, auto-archivage."""
        benevole = self._create_benevole(
            nom='Müller', prenom='Irma', genre='F', langues=['fr'], activites=[TypeActivite.INSTALLATION],
            utilisateur=Utilisateur.objects.create_user(
                'irma@example.org', 'pass', first_name='Irma', last_name='Müller',
            )
        )
        ilya_10_jours = date.today() - timedelta(days=10)
        dans_3_jours = date.today() + timedelta(days=3)
        form_data = {
            'nom': 'Müller',
            'prenom': 'Irma',
            'genre': 'F',
            'date_naissance': "1950-04-02",
            'npalocalite': '',
            'langues': ['fr'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '1',
            'activite_set-0-id': benevole.activite_set.first().pk,
            'activite_set-0-activite': TypeActivite.INSTALLATION,
            'activite_set-0-duree_0': ilya_10_jours.strftime('%d.%m.%Y'),
            'activite_set-0-duree_1': dans_3_jours.strftime('%d.%m.%Y'),
            'telephone_set-TOTAL_FORMS': 0,
            'telephone_set-INITIAL_FORMS': 0,
        }
        self.client.force_login(self.user)
        app = current_app()
        response = self.client.post(reverse('benevole-edit', args=[app, benevole.pk]), data=form_data)
        self.assertRedirects(response, self.benevole_list_url)
        benevole.refresh_from_db()
        # Pas encore archivé si la date de fin d'activité est dans le futur
        self.assertIsNone(benevole.archive_le)
        today_str = date.today().strftime('%Y-%m-%d')
        ilya_10_jours_str = ilya_10_jours.strftime('%Y-%m-%d')
        self.assertEqual(
            benevole.persona.journaux.latest().description,
            "Modification du bénévole: activité Alarme (installation): durée "
            f"(de «[{today_str}, None)» à «[{ilya_10_jours_str}, {dans_3_jours.strftime('%Y-%m-%d')})»)"
        )

        form_data['activite_set-0-duree_1'] = date.today().strftime('%d.%m.%Y')
        response = self.client.post(reverse('benevole-edit', args=[app, benevole.pk]), data=form_data)
        self.assertRedirects(response, self.benevole_list_url)
        benevole.refresh_from_db()
        self.assertEqual(benevole.archive_le, date.today())
        self.assertFalse(benevole.utilisateur.is_active)
        dernier, avantdernier = benevole.journaux.order_by('-quand')[:2]
        self.assertEqual(
            avantdernier.description,
            "Modification du bénévole: activité Alarme (installation): durée "
            f"(de «[{ilya_10_jours_str}, {dans_3_jours.strftime('%Y-%m-%d')})» à "
            f"«[{ilya_10_jours_str}, {today_str})»)"
        )
        self.assertEqual(
            dernier.description,
            "Plus d’activité en cours, archivage du bénévole."
        )

    def test_benevole_desarchiver_par_nouvelle_activite(self):
        benevole = self._create_benevole(
            nom='Müller', prenom='Irma', genre='F', langues=['fr'], activites=[],
            archive_le=date(2023, 10, 31),
            utilisateur=Utilisateur.objects.create_user(
                'irma@example.org', 'pass', first_name='Irma', last_name='Müller', is_active=False
            )
        )
        form_data = {
            'nom': 'Müller',
            'prenom': 'Irma',
            'genre': 'F',
            'npalocalite': '2000 Neuchâtel',
            'langues': ['fr'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '0',
            'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
            'activite_set-0-duree_0': date.today().strftime('%d.%m.%Y'),
            'activite_set-0-duree_1': '',
            'telephone_set-TOTAL_FORMS': 0,
            'telephone_set-INITIAL_FORMS': 0,
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-edit', args=[current_app(), benevole.pk]), data=form_data)
        self.assertRedirects(response, self.benevole_list_url)
        benevole.refresh_from_db()
        self.assertTrue(benevole.utilisateur.is_active)
        self.assertIsNone(benevole.archive_le)

    def test_benevole_archiver(self):
        benevole = self._create_benevole(
            utilisateur=Utilisateur.objects.create_user('irma@example.org', 'pw', last_name='Müller'),
        )
        Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345',
            service=Services.ALARME, visiteur=benevole.utilisateur,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-archive', args=[current_app(), benevole.pk]), {}, follow=True)
        self.assertEqual(
            str(list(response.context['messages'])[0]),
            'Alarme Marco a bien été archivé. Pour 1 client(s) '
            'dont le bénévole était visiteur, le champ visiteur a été vidé.'
        )

    def test_benevole_archived_edit(self):
        benevole = self._create_benevole(archive_le=date.today())
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-edit', args=[current_app(), benevole.pk]))
        self.assertContains(response, "Marco")

    def test_refaire_note_frais(self):
        benevole = self._create_benevole(
            activites=[TypeActivite.INSTALLATION, TypeActivite.TRANSPORT],
            utilisateur=Utilisateur.objects.create_user("irma@example.org", password="pwd"),
        )
        day_prev_month = date.today().replace(day=1) - timedelta(days=4)
        app = current_app()
        Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345',
            service=[Services.ALARME, Services.TRANSPORT],
        )
        match app:
            case 'alarme':
                from alarme.factories import MissionFactory

                MissionFactory(
                    intervenant=benevole.utilisateur, effectuee=day_prev_month, km=12.5
                )
            case 'transport':
                from transport.factories import TransportFactory

                TransportFactory(
                    chauffeur=benevole,
                    heure_rdv=datetime.combine(day_prev_month, time(11, 30), tzinfo=get_current_timezone()),
                )
            case _:
                self.skipTest(f"Notes de frais pas encore implémentées pour l'app «{current_app()}»")

        calculateur = canton_app.fact_policy.calculateur_frais(day_prev_month.replace(day=1))
        calculateur.calculer_frais(benevole=benevole, enregistrer=True)
        note = calculateur.note_frais_pour(benevole)
        self.client.force_login(self.user)
        list_url = reverse('benevole-notesfrais', args=[app, benevole.pk])
        response = self.client.post(
            reverse('benevole-notesfrais-refaire', args=[app, note.pk]),
            headers={"referer": list_url}
        )
        self.assertRedirects(response, list_url)

    def test_creation_journal(self):
        benev = self._create_benevole()
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("benevole-journal-add", kwargs={"app": current_app(), "pk": benev.pk}),
            data={"description": "Test manuel"},
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(benev.journaux.count(), 1)

    def test_suppression_ligne_journal(self):
        benev = self._create_benevole()
        user2 = Utilisateur.objects.create_user(
            'me2@example.org', 'me2password', first_name='Gil', last_name='Plaf',
        )
        journal = Journal.objects.create(
            persona=benev.persona, description="Plop", qui=user2, quand=now(), auto=False
        )
        self.client.force_login(self.user)
        delete_url = reverse("benevole-journal-delete", args=[current_app(), journal.persona.benevole.pk, journal.pk])
        response = self.client.post(delete_url, data={})
        self.assertEqual(response.status_code, 403)
        self.client.force_login(journal.qui)
        response = self.client.post(delete_url, data={})
        self.assertEqual(response.json(), {'reload': 'page', 'result': 'OK'})
        self.assertFalse(Journal.objects.filter(pk=journal.pk).exists())

    def test_benevole_fichier_add(self):
        benev = self._create_benevole()
        self.client.force_login(self.user)
        url = reverse('benevole-fichier-add', args=[benev.persona_id])
        response = self.client.get(url)
        self.assertContains(
            response, '<input type="file" name="fichier" class="form-control" required id="id_fichier">',
            html=True
        )
        with Path(__file__).open(mode='rb') as fh:
            response = self.client.post(url, data={
                'titre': 'Titre',
                'typ': 'doc',
                'fichier': File(fh),
            })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#fichiers-container'})
        self.assertEqual(benev.fichiers.count(), 1)

    def test_benevole_fichier_edit(self):
        benev = self._create_benevole()
        with Path(__file__).open(mode='rb') as fh:
            fichier = Fichier.objects.create(
                content_object=benev, titre="Document", typ='doc',
                fichier=File(fh, name="test.py"), qui=self.user, quand=now()
            )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('benevole-fichier-edit', args=[benev.persona_id, fichier.pk]), data={
                'titre': 'Autre document',
                'typ': 'doc',
            },
        )
        fichier.refresh_from_db()
        self.assertEqual(fichier.titre, "Autre document")


class BenevoleFormationTests(BenevoleTestsMixin):
    def test_formation_add(self):
        benev = self._create_benevole(activites=[TypeActivite.TRANSPORT])
        typef = TypeFormation.objects.get_or_create(nom="Autre formation", code="autre")[0]
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('benevole-formation-add', args=[current_app(), benev.pk]), data={
                'titre': 'Autre formation',
                'categorie': typef.pk,
                'quand': '2024-07-03',
            },
        )
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(benev.formations.count(), 1)
        self.assertJournalMessage(benev, "Ajout d’une formation «Autre formation»")

    def test_formation_list(self):
        benev = self._create_benevole()
        formation = Formation.objects.create(titre="Cours 1", quand="2024-04-13", categorie=self.typef)
        benev.formations.add(formation)
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-formations', args=[current_app(), benev.pk]))
        self.assertQuerySetEqual(response.context['object_list'], [formation])

    def test_formation_delete(self):
        benev = self._create_benevole()
        formation = Formation.objects.create(titre="Cours 1", quand="2024-04-13", categorie=self.typef)
        benev.formations.add(formation)
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-formation-delete', args=[current_app(), benev.pk, formation.pk]))
        self.assertEqual(benev.formations.count(), 0)
        self.assertJournalMessage(benev, "Suppression de Formation «Cours 1» du 13.04.2024")

    def test_formation_export(self):
        form1 = Formation.objects.create(titre="Cours 1", quand="2024-01-13", categorie=self.typef)
        form2 = Formation.objects.create(titre="Cours 2", quand="2024-02-13", categorie=self.typef)
        form3 = Formation.objects.create(titre="Cours 3", quand="2024-03-13", categorie=self.typef)
        benev = self._create_benevole()
        Activite.objects.create(
            benevole=benev, type_act=TypeActivite.objects.get(code='installation'),
            duree=('2024-02-01', None),
        )
        benev.formations.add(form1, form2, form3)
        Benevole.objects.par_activite('alarme')
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('benevole-formations-export', args=['alarme']) +
            '?start_month=2&start_year=2024&end_month=4&end_year=2024'
        )
        content = read_xlsx(response.content)
        self.assertEqual(len(content), 4)  # 2 headers, 2 lines
        self.assertEqual(content[2][0], "Alarme Marco")
