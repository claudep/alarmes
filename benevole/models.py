from collections import defaultdict
from datetime import date, timedelta
from decimal import Decimal
from functools import cache, partial

from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.fields import ArrayField, DateRangeField
from django.db import models, transaction
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import FilteredRelation, Q
from django.dispatch import Signal
from django.urls import reverse
from django.utils.crypto import get_random_string

from client.models import Persona, PersonaBaseQuerySet, PersonaProxyFields
from common.choices import Genres, Handicaps, Languages, Pays, Services
from common.fields import ChoiceArrayField, PhoneNumberField
from common.models import Fichier, GeolocMixin, Utilisateur
from common.utils import RegionFinder, arrondi_5, current_app

benevole_saved = Signal()


class TypeActivite(models.Model):
    code = models.CharField(max_length=20, unique=True)
    nom = models.CharField(max_length=50)
    service = models.CharField(max_length=12, choices=Services, blank=True)

    INSTALLATION = 'installation'
    TRANSPORT = 'transport'
    VISITE_ALARME = 'visite'

    def __str__(self):
        return self.nom

    @classmethod
    @cache
    def service_map(cls):
        """Mapping des activités par service."""
        mapping = defaultdict(list)
        for act in TypeActivite.objects.exclude(service=''):
            mapping[act.service].append(act)
            mapping[act.get_service_display()].append(act)
        return dict(mapping)

    @classmethod
    def par_domaine(cls, domaine):
        return cls.service_map().get(domaine, [])

    @classmethod
    def par_label(cls, label):
        return next((
            act for act in TypeActivite.objects.all() if act.code == label or act.nom == label
        ), None)


class BenevoleQuerySet(PersonaBaseQuerySet):
    adr_outer_field = "persona_id"

    def create(self, **kwargs):
        activites = kwargs.pop("activites", [])
        with transaction.atomic():
            id_externe = kwargs.pop("id_externe", None)
            if "persona" not in kwargs:
                persona, kwargs = self.create_persona(**kwargs)
                kwargs["persona"] = persona
            instance = super().create(id_externe=id_externe, **kwargs)
            for typ in activites:
                start = kwargs['archive_le'] - timedelta(days=30) if kwargs.get('archive_le') else date.today()
                Activite.objects.create(
                    benevole=instance, type_act=TypeActivite.objects.get(code=typ),
                    duree=(start, kwargs.get('archive_le')),
                )
        return instance

    def par_activite(self, activite, depuis=None, jusqua=None, include=None):
        filtre = Q(activite__type_act__code=activite)
        if depuis is None and jusqua is None:
            filtre &= Q(activite__duree__contains=date.today())
        else:
            # Extend upper bound which is not included by default
            filtre &= Q(activite__duree__overlap=DateRange(depuis, jusqua + timedelta(days=1)))
        if include:
            filtre |= Q(pk=include.pk)
        return self.filter(filtre).distinct().order_by("persona__nom", "persona__prenom")

    def par_domaine(self, domaine, actifs=True, depuis=None, jusqua=None, include=None):
        """
        `include` permet d'intégrer une personne quel que soit le domaine, par
        exemple dans le cas où une instance inverse a déjà un bénévole sélectionné
        et que celui-ci ne fait plus partie du domaine.
        Si `actifs` est False, tous les bénévoles ayant eu une fois une activité
        du domaine concerné sont renvoyés.
        """
        types_act = TypeActivite.par_domaine(domaine)
        if actifs:
            situ_actuelle = depuis is None and jusqua is None
            depuis = depuis or date.today()
            jusqua = (jusqua or date.today()) + timedelta(days=1)
            cond_duree = Q(activite__duree__overlap=DateRange(depuis, jusqua))
            if situ_actuelle:
                # Inclure les activites commençant dans le futur (supposé proche)
                cond_duree |= Q(activite__duree__startswith__gt=date.today())
            query = self.annotate(
                activites_actives=FilteredRelation(
                    "activite", condition=(
                        Q(activite__inactif=False) & cond_duree
                    )
                )
            )
            filtre = Q(activites_actives__type_act__in=types_act)
        else:
            query = self
            filtre = Q(activite__type_act__in=types_act)
        if include:
            filtre |= Q(pk=include.pk)
        return query.filter(filtre).distinct().order_by("persona__nom", "persona__prenom")


class Benevole(PersonaProxyFields, models.Model):
    utilisateur = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    persona = models.OneToOneField(Persona, on_delete=models.PROTECT, null=True)
    id_externe = models.BigIntegerField(null=True, blank=True)
    formation = models.TextField("Formation", blank=True)
    absences = models.TextField("Absences", blank=True)
    contacts = models.TextField("Contact en cas d’urgence", blank=True)
    remarques = models.TextField("Remarques", blank=True)
    ne_pas_defrayer = models.BooleanField(
        "Ne pas défrayer", default=False,
        help_text="Ce bénévole ne souhaite jamais être défrayé (repas, déplacements, etc.)"
    )
    archive_le = models.DateField("Archivé le", blank=True, null=True)
    # Spécifique aux transports
    vehicule = models.CharField("Type de véhicule", max_length=50, blank=True)
    nb_portes_vhc = models.PositiveSmallIntegerField("Nbre de portes", null=True, blank=True)
    no_plaques = models.CharField("Numéro(s) de plaque", max_length=30, blank=True)
    macaron_valide = DateRangeField("Durée de validité du macaron", blank=True, null=True)
    no_macaron = models.CharField("Numéro du macaron", max_length=15, blank=True)
    conduit_vehicule = models.BooleanField("Conduit les véhicules C-R", default=False)
    incompats = ChoiceArrayField(
        models.CharField(max_length=10, choices=Handicaps.choices, blank=True),
        blank=True, null=True, verbose_name='Incompatibilités'
    )
    # Spécifique visite
    prefs_visite = models.TextField("Préférences pour les visites", blank=True)
    formations = models.ManyToManyField("Formation", blank=True)

    objects = BenevoleQuerySet.as_manager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_externe'], name="benevole_id_externe_unique"),
        ]

    def __str__(self):
        return self.nom_prenom

    @property
    def nom_prenom(self):
        return " ".join([self.nom, self.prenom])

    def age(self, quand=None):
        if not self.date_naissance:
            return None
        age = ((quand or date.today()) - self.date_naissance).days / 365.25
        return int(age * 10) / 10  # 1 décimale arrondi vers le bas

    @property
    def incompats_verbose(self):
        return [dict(Handicaps.choices)[v] for v in (self.incompats or [])]

    @property
    def region(self):
        npa = self.adresse().npa
        return RegionFinder.get_region(npa) if npa else None

    def can_edit(self, user):
        return user.has_perm('benevole.change_benevole')

    def save(self, **kwargs):
        if self.utilisateur_id:
            # Keep Benevole.nom/prenom/courriel in sync with Utilisateur fields
            if self.nom != self.utilisateur.last_name:
                self.utilisateur.last_name = self.nom
                self.utilisateur.save()
            if self.prenom != self.utilisateur.first_name:
                self.utilisateur.first_name = self.prenom
                self.utilisateur.save()
            if self.courriel and self.courriel != self.utilisateur.email:
                self.utilisateur.email = self.courriel
                self.utilisateur.save()
        return super().save(**kwargs)

    def fichiers_app(self, app):
        return self.fichiers.all()

    def create_account(self, groups=()):
        if Utilisateur.objects.filter(email=self.courriel).exists():
            raise ValueError(f"Un compte existe déjà pour le courriel {self.courriel}.")
        pwd = get_random_string(length=10)
        user = Utilisateur.objects.create_user(
            self.courriel, pwd,
            first_name=self.prenom, last_name=self.nom,
        )
        for grp_name in tuple(groups) + ("Bénévoles",):
            try:
                grp = Group.objects.get(name=grp_name)
            except Group.DoesNotExist:
                continue
            user.groups.add(grp)
        self.utilisateur = user
        self.save()
        return pwd

    def archiver(self):
        msg = ''
        with transaction.atomic():
            if self.utilisateur_id:
                visites = self.utilisateur.visites.filter(archive_le__isnull=True)
                num_visites = visites.count()
                if num_visites:
                    visites.update(visiteur=None)
                    msg = (
                        f"Pour {num_visites} client(s) dont le bénévole était visiteur, "
                        "le champ visiteur a été vidé."
                    )
                self.utilisateur.is_active = False
                self.utilisateur.save()
            self.archive_le = date.today()
            self.save(update_fields=['archive_le'])
        transaction.on_commit(
            partial(benevole_saved.send, sender=Benevole, instance=self, created=False)
        )
        return msg

    def desarchiver(self):
        self.archive_le = None
        with transaction.atomic():
            self.save(update_fields=['archive_le'])
            if self.utilisateur_id:
                self.utilisateur.is_active = True
                self.utilisateur.save()
        benevole_saved.send(sender=Benevole, instance=self, created=False)

    def get_full_name(self):
        return f"{self.prenom} {self.nom}"

    def get_absolute_url(self):
        # Lien vers premier onglet de bénévole
        if apps.is_installed('transport'):
            return reverse('benevole-transports', args=[self.pk])
        return reverse('benevole-edit', args=[current_app(), self.pk])

    def inactif(self, domaine=None):
        """
        Indique si le bénévole est inactif pour le domaine `domaine`,
        c'est-à-dire que toutes les activités du domaine sont marquées comme 'inactif'
        """
        if domaine is None:
            domaine = current_app()
        acts_par_domaine = TypeActivite.par_domaine(domaine)
        benev_acts = [act for act in self.activite_set.all() if act.type_act in acts_par_domaine]
        inact = all(act.inactif for act in benev_acts)
        if inact:
            self.inactif_msg = "".join(act.note for act in benev_acts if act.note)
        return inact

    def activites_en_cours(self):
        """
        Activités en cours (selon champ duree), y compris activité future, dans le sens
        où une date de début dans le futur implique un début d'activité très prochain
        (et évite de pousser le bénévole dans les archives en attendant).
        """
        today = date.today()
        if 'activite_set' in getattr(self, '_prefetched_objects_cache', {}):
            return [
                act for act in self.activite_set.all()
                if not act.duree.upper or act.duree.upper > today
            ]
        return self.activite_set.select_related("type_act").filter(
            Q(duree__contains=today) | Q(duree__startswith__gt=today)
        )

    def has_activite(self, act_code):
        return any(act.type_act.code == act_code for act in self.activites_en_cours())

    def get_activites_display(self):
        return ', '.join([act.type_act.nom for act in self.activites_en_cours()])


class Activite(models.Model):
    benevole = models.ForeignKey(Benevole, on_delete=models.CASCADE)
    type_act = models.ForeignKey(TypeActivite, on_delete=models.PROTECT)
    duree = DateRangeField("Durée")
    inactif = models.BooleanField("Temporairement inactif", default=False)
    note = models.TextField("Notes", blank=True)

    def __str__(self):
        return f"Activité «{self.type_act.nom}» pour {self.benevole}"

    @property
    def service(self):
        return self.type_act.get_service_display() if self.type_act.service else self.type_act.nom

    def active_entre(self, start, end):
        return self.duree.lower < end and (self.duree.upper is None or self.duree.upper >= start)

    class Meta:
        verbose_name = "Activité"


class NoteFrais(models.Model):
    benevole = models.ForeignKey(Benevole, on_delete=models.CASCADE)
    mois = models.DateField()
    service = models.CharField(max_length=15, choices=Services)
    kms = models.DecimalField(max_digits=5, decimal_places=1)
    date_export = models.DateTimeField(blank=True, null=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='mois_service_unique', fields=['benevole', 'mois', 'service'])
        ]
        verbose_name = "Note de frais"
        verbose_name_plural = "Notes de frais"

    def __str__(self):
        return f"Note de frais «{self.service}» pour {self.benevole}, {self.mois.month}.{self.mois.year}"

    def somme_pour(self, code):
        return sum([lig.montant or 0 for lig in self.lignes.all() if lig.libelle.no == code] + [Decimal(0)])

    def somme_totale(self):
        return sum([lig.montant or 0 for lig in self.lignes.all()] + [Decimal(0)])

    def quantite_pour(self, code):
        return sum([lig.quantite for lig in self.lignes.all() if lig.libelle.no == code] + [Decimal(0)])


class TypeFrais(models.Model):
    no = models.CharField("No article", max_length=10, unique=True)
    libelle = models.CharField("Libellé", max_length=100)
    services = ChoiceArrayField(
        models.CharField(max_length=10, choices=Services),
    )
    montant_fixe = models.BooleanField(
        "Montant fixe", default=True,
        help_text="Indique si le prix est fixé pour l’article ou si le montant peut varier"
    )

    class Meta:
        verbose_name = "Type de frais"
        verbose_name_plural = "Types de frais"

    def __str__(self):
        return f"Type de frais «{self.libelle}» ({self.no})"


class LigneFrais(models.Model):
    note = models.ForeignKey(NoteFrais, on_delete=models.CASCADE, related_name="lignes")
    libelle = models.ForeignKey(TypeFrais, on_delete=models.PROTECT)
    quantite = models.DecimalField(max_digits=7, decimal_places=2)
    montant_unit = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta:
        verbose_name = "Ligne de frais"
        verbose_name_plural = "Lignes de frais"

    def __str__(self):
        return (
            f"Ligne de frais ({self.libelle.libelle}, {self.montant}) "
            f"pour «{self.note if self.note_id else '<note virtuelle'}»"
        )

    @property
    def montant(self):
        return arrondi_5(self.quantite * self.montant_unit) if self.montant_unit else None


class TypeFormation(models.Model):
    nom = models.CharField("Type de formation", max_length=60)
    code = models.CharField("Code", max_length=15)

    def __str__(self):
        return self.nom


class Formation(models.Model):
    titre = models.CharField("Titre", max_length=200)
    descriptif = models.TextField("Descriptif", blank=True)
    categorie = models.ForeignKey(TypeFormation, on_delete=models.PROTECT)
    quand = models.DateField("Date de la formation")
    duree = models.DurationField("Durée de la formation", blank=True, null=True)
    attestation = models.FileField("Attestation", upload_to="attestations", blank=True)

    def __str__(self):
        return f"Formation «{self.titre}» du {self.quand.strftime('%d.%m.%Y')}"

    def can_edit(self, user):
        return user.has_perm('benevole.change_benevole')


class CalculateurFraisBase:
    """
    Calculateur de frais bénévoles à partir de leurs interventions.
    Cette classe est destinée à être surchargée dans les différents secteurs.
    """
    service = None

    def __init__(self, mois):
        self.mois = mois
        self.benevoles = {}

    def frais_par_benevole(self, benevole=None):
        """Renvoie un dictionnaire avec le(s) bénévole(s) comme clé."""
        return {}

    def lignes_depuis_data(self, benevole, data):
        """Renvoie une liste d'instances LigneFrais en mémoire."""
        return []

    def calculer_frais(self, benevole=None, enregistrer=False, refaire=False):
        # si benevole=None, pour tous les bénévoles.
        frais_data = self.frais_par_benevole(benevole=benevole)
        created = 0
        for benevole, data in frais_data.items():
            if benevole.ne_pas_defrayer:
                continue
            lignes = self.lignes_depuis_data(benevole, data)
            if enregistrer:
                existant = benevole.notefrais_set.filter(service=self.service, mois=self.mois)
                if refaire and existant:
                    existant.delete()
                elif existant:
                    self.benevoles[benevole] = existant.first()
                    continue
                note = NoteFrais.objects.create(
                    benevole=benevole,
                    service=self.service,
                    mois=self.mois,
                    kms=data['kms'],
                )
                for ligne in lignes:
                    ligne.note = note
                LigneFrais.objects.bulk_create(lignes)
                self.benevoles[benevole] = note
                created += 1
            else:
                self.benevoles[benevole] = lignes
        return created

    def note_frais_pour(self, benevole):
        if benevole not in self.benevoles:
            raise RuntimeError(f"Les frais n’ont pas été calculés pour {benevole}")
        return self.benevoles[benevole]
