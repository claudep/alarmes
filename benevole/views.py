from datetime import date, timedelta
from functools import partial

from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Count, Prefetch, Q
from django.http import Http404, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import DeleteView, ListView, View

from client.forms import JournalForm
from client.models import Journal, Persona
from common.export import ExpLine, OpenXMLExport
from common.stat_utils import DateLimitForm
from common.utils import canton_abrev, canton_app, current_app
from common.views import (
    CreateUpdateView, ExportableMixin, FichierDeleteView, FichierEditView,
    FichierListeView, FilterFormMixin, GeoAddressMixin, JournalizingDeleteView,
    JournalMixin
)
from .models import Activite, Benevole, Formation, NoteFrais, TypeActivite, benevole_saved
from . import forms


def benev_list_url_data(app):
    if app == 'alarme':
        return {'url': reverse('intervenants'), 'title': "Liste des intervenant·e·s"}
    return {'url': reverse('benevoles', args=[app]), 'title': "Liste des bénévoles"}


class BenevoleJournalMixin(JournalMixin):
    def _create_instance(self, **kwargs):
        Journal.objects.create(persona=getattr(self, 'benevole', self.object).persona, **kwargs)


class BenevoleListView(ExportableMixin, FilterFormMixin, PermissionRequiredMixin, ListView):
    model = Benevole
    filter_formclass = forms.BenevoleFilterForm
    permission_required = 'benevole.view_benevole'
    template_name = 'benevole/benevole_list.html'
    paginate_by = 25
    is_archive = False
    extra_export_cols = []  # Liste de tuples ("<nom col>", "<nom champ de modèle>")

    @property
    def col_widths(self):
        return [18, 18, 25, 25, 30, 16] + [15] * len(self.activites) + [60]

    def dispatch(self, *args, **kwargs):
        self.activites = TypeActivite.objects.all().order_by('id')
        return super().dispatch(*args, **kwargs)

    def get_queryset(self, base_qs=None):
        today = date.today()
        base_qs = base_qs or Benevole.objects.all()
        if self.filter_form.is_bound and self.filter_form.is_valid():
            activites = self.filter_form.cleaned_data['activites']
        else:
            activites = ""
        if activites == "all":
            if self.is_archive:
                self.queryset = base_qs.filter(archive_le__isnull=False)
            else:
                self.queryset = base_qs.filter(archive_le__isnull=True)
        else:
            if activites == "":
                acts = TypeActivite.par_domaine(self.kwargs['app'])
            else:
                acts = [TypeActivite.objects.get(code=activites)]
            self.queryset = base_qs.annotate(
                app_inactive=Count('activite', filter=(
                    Q(activite__type_act__in=acts) & ~Q(activite__duree__contains=today)
                )),
                app_active=Count('activite', filter=(
                    Q(activite__type_act__in=acts) & (
                        Q(activite__duree__contains=today) | Q(activite__duree__startswith__gt=today)
                    )
                )),
            )
            if self.is_archive:
                self.queryset = self.queryset.filter(app_inactive__gt=0, app_active=0)
            else:
                self.queryset = self.queryset.filter(app_active__gt=0)
        self.queryset = self.queryset.prefetch_related(
            Prefetch('activite_set', queryset=Activite.objects.select_related('type_act'))
        ).select_related("utilisateur").avec_adresse(today).order_by(
            "persona__nom", "persona__prenom"
        )
        return super().get_queryset()

    def get_context_data(self, **kwargs):
        app = self.kwargs['app']
        labels = {
            'alarme': {
                'breadcrumb': 'Bénévoles', 'title': 'Liste des bénévoles alarme', 'new': 'Nouveau bénévole'
            },
            'transport': {
                'breadcrumb': 'Chauffeurs', 'title': 'Liste des chauffeurs bénévoles', 'new': 'Nouveau chauffeur'
            },
            'visite': {
                'breadcrumb': 'Visiteurs', 'title': 'Liste des bénévoles visite', 'new': 'Nouveau bénévole'
            },
        }.get(app)
        back_url_data = benev_list_url_data(current_app())
        if self.is_archive:
            labels['title'] += ' archivés'
        return {
            **super().get_context_data(**kwargs),
            'app': self.kwargs['app'],
            'labels': labels,
            'back_url_data': back_url_data,
            'agenda_url_name': 'benevole-agenda' if apps.is_installed('transport') else '',
        }

    def export_lines(self, context):
        export_cols = ['Nom', 'Prénom', 'Rue', 'Localité', 'Courriel', 'Téls', 'Date de naissance']
        yield ExpLine(
            export_cols + [col[0] for col in self.extra_export_cols] +
            [act.nom for act in self.activites] + ['Remarques'],
            bold=True
        )
        for benev in self.get_queryset():
            activites = {act.type_act: act for act in benev.activites_en_cours()}
            adr = benev.persona.adresse()
            yield [
                benev.nom, benev.prenom, adr.rue if adr else "",
                f"{adr.npa} {adr.localite}" if adr else "", benev.courriel,
            ] + [
                "\n".join([tel.tel for tel in benev.telephones()]),
                benev.date_naissance,
            ] + [
                getattr(benev, col[1]) for col in self.extra_export_cols
            ] + [
                (f"x ({activites[type_act].duree.lower.strftime('%d.%m.%Y')}-)"
                 if type_act in activites else '')
                for type_act in self.activites
            ] + [benev.remarques]


class BenevoleEditView(PermissionRequiredMixin, BenevoleJournalMixin, CreateUpdateView):
    model = Benevole
    permission_required = 'benevole.change_benevole'
    template_name = 'benevole/benevole.html'
    labels = {
        'alarme': {
            'breadcrumb1': 'Intervenant·e·s', 'breadcrumb2': 'Nouveau bénévole',
            'breadcrumb_url': reverse_lazy('intervenants'),
            'success': "La création d’un nouveau bénévole a réussi.",
        },
        'transport': {
            'breadcrumb1': 'Chauffeurs', 'breadcrumb2': 'Nouveau chauffeur',
            'breadcrumb_url': reverse_lazy('benevoles', args=['transport']),
            'success': "La création d’un nouveau chauffeur a réussi.",
        },
        'visite': {
            'breadcrumb1': 'Bénévoles', 'breadcrumb2': 'Nouveau bénévole',
            'breadcrumb_url': reverse_lazy('benevoles', args=['visite']),
            'success': "La création d’un nouveau bénévole a réussi.",
        },
        'default': {
            'breadcrumb1': 'Bénévoles', 'breadcrumb2': 'Nouveau bénévole',
            'breadcrumb_url': '',
            'success': "La création d’un nouveau bénévole a réussi.",
        },
    }
    journal_add_message = "Création du bénévole"
    journal_edit_message = "Modification du bénévole: {fields}"

    def dispatch(self, *args, **kwargs):
        self.app = self.kwargs['app']
        self.current_labels = self.labels.get(self.app, self.labels["default"])
        if not self.current_labels['breadcrumb_url']:
            self.current_labels['breadcrumb_url'] = reverse_lazy('benevoles', args=[self.app])
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        if self.object is None:
            return {'langues': ['fr']}
        else:
            return super().get_initial()

    def get_form_class(self):
        return forms.BenevoleTransportForm if self.app == 'transport' else forms.BenevoleForm

    def get_success_url(self):
        return self.current_labels['breadcrumb_url']

    def get_success_message(self, obj):
        if self.is_create:
            return self.current_labels['success']
        else:
            return f"«{obj.nom} {obj.prenom}» a bien été modifié"

    def form_valid(self, form):
        form._send_save_signal = False  # Delay signal
        is_new = form.instance.pk is None
        response = super().form_valid(form)
        if 'courriel' in form.changed_data and self.object.utilisateur_id:
            messages.warning(
                self.request,
                "La modification du courriel implique également que cette "
                "personne se connecte avec cette nouvelle adresse."
            )
        # Auto archivage si plus d'activité active
        act_actives = self.object.activites_en_cours()
        if not act_actives and not self.object.archive_le:
            msg = self.object.archiver()
            messages.warning(self.request, f"Sans activité en cours, {self.object} a été archivé. {msg}")
            self._create_instance(
                description="Plus d’activité en cours, archivage du bénévole.",
                quand=timezone.now(), qui=self.request.user
            )
        elif act_actives and self.object.archive_le:
            self.object.desarchiver()
            self._create_instance(
                description="Nouvelle activité active, réactivation du bénévole.",
                quand=timezone.now(), qui=self.request.user
            )
        else:
            transaction.on_commit(
                partial(benevole_saved.send, sender=Benevole, instance=self.object, created=is_new)
            )
        # Création automatique d'un compte si une activité alarme est en cours
        if (
            'alarme' in [act.service.lower() for act in act_actives]
            and not self.object.utilisateur_id and self.object.courriel
        ):
            self.object.create_account()
        return response

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'app': self.app,
            'labels': self.current_labels,
            'fichiers': self.object.fichiers_app(current_app()).order_by('-quand') if self.object else [],
        }
        if not self.is_create:
            if hasattr(self.object, 'mission_set'):
                context['missions'] = self.object.mission_set.all().order_by('-effectuee', 'delai')
            context.update({
                "adresse": self.object.adresse(date.today()),
                "adresse_future": self.object.persona.adresseclient_set.filter(principale__startswith__gt=date.today()).first(),
            })

        if hasattr(self.object, 'preferences'):
            context['preferences'] = self.object.preferences.filter(
                client__archive_le__isnull=True
            ).select_related('client').order_by('client__persona__nom')
        return context


class BenevoleCreateAccountView(PermissionRequiredMixin, View):
    permission_required = 'benevole.change_benevole'

    def post(self, request, *args, **kwargs):
        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        try:
            password = benevole.create_account()
            assert password is not None
        except ValueError as err:
            messages.error(request, str(err))
            return HttpResponseRedirect(reverse('benevole-edit', args=[self.kwargs['app'], benevole.pk]))

        send_mail(
            f"[Croix-Rouge {canton_abrev()}] Compte pour application",
            "Bonjour\n\nUn nouveau compte a été créé pour que vous ayez accès à l’application Croix-Rouge:\n"
            f"Site: https://{settings.ALLOWED_HOSTS[0]}\n"
            f"Mot de passe: {password}\n\n"
            "Cordiales salutations",
            None,
            [benevole.courriel]
        )
        messages.success(
            request,
            f"Un compte a bien été créé pour {benevole} et son mot de passe lui a été envoyé par courriel."
        )
        return HttpResponseRedirect(reverse('benevole-edit', args=[self.kwargs['app'], benevole.pk]))


class BenevoleAutocompleteView(View):
    """Endpoint for autocomplete search for Benevole."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        if not term:
             raise Http404("Le terme de recherche est obligatoire")
        base_qs = Benevole.objects.filter(archive_le__isnull=True).prefetch_related(
            Prefetch(
                'activite_set',
                queryset=Activite.objects.filter(duree__upper_inf=True).select_related('type_act')
            )
        )
        if kwargs['app'] != 'tous':
            base_qs = base_qs.par_domaine(kwargs['app'])
        results = []
        for benev in base_qs.filter(persona__nom__unaccent__icontains=term).avec_adresse(date.today())[:20]:
            adr = benev.adresse_active
            results.append({
                'label': str(benev), 'value': benev.pk,
                'adresse': f"{adr['rue']}, {adr['npa']} {adr['localite']}",
                'types': [act.type_act.code for act in benev.activite_set.all()],
                'url': benev.get_absolute_url(),
            })
        return JsonResponse(results, safe=False)


class BenevoleFormationsListView(PermissionRequiredMixin, ListView):
    permission_required = 'benevole.view_benevole'
    model = Formation
    template_name = 'benevole/benevole_formations.html'
    paginate_by = 20

    def get(self, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.benevole.formations.order_by('-quand')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
        }


class BenevoleFormationEditView(PermissionRequiredMixin, BenevoleJournalMixin, CreateUpdateView):
    permission_required = 'benevole.change_benevole'
    model = Formation
    form_class = forms.FormationForm
    pk_url_kwarg = 'pk_form'
    json_response = True
    journal_add_message = "Ajout d’une formation «{obj.titre}»"
    journal_edit_message = "Modification de la formation «{obj.titre}» : {fields}"

    def form_valid(self, form):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        super().form_valid(form)
        self.benevole.formations.add(self.object)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleFormationDeleteView(PermissionRequiredMixin, BenevoleJournalMixin, JournalizingDeleteView):
    permission_required = 'benevole.change_benevole'
    model = Formation
    pk_url_kwarg = 'pk_form'
    json_response = True

    def form_valid(self, form):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleFormationsExportView(PermissionRequiredMixin, View):
    """
    Exportation de toutes les formations des bénévoles dans un espace de temps
    donné (selon querystring).
    """
    permission_required = 'benevole.change_benevole'

    def get(self, request, *args, **kwargs):
        date_form = DateLimitForm(request.GET)
        if not date_form.is_valid():
            return HttpResponseBadRequest("Invalid date data provided.")
        self.date_start = date_form.start
        self.date_end = date_form.end
        export = OpenXMLExport(sheet_title="Formations bénévoles", col_widths=[24, 30, 30, 18, 13, 8])
        export.fill_data(self.export_lines())
        return export.get_http_response(self.__class__.__name__.replace('View', '') + '.xlsx')

    def export_lines(self):
        yield ExpLine([
            f"Formations des bénévoles entre {self.date_start.strftime('%d.%m.%Y')} et {self.date_end.strftime('%d.%m.%Y')}"
        ], bold=True)
        yield ExpLine([
            "Bénévole", "Titre", "Descriptif", "Catégorie", "Date de la formation", "Durée"
        ], bold=True)
        through_model = Formation._meta.get_field("benevole").through
        formations = through_model.objects.filter(
            benevole__in=Benevole.objects.par_domaine(
                self.kwargs['app'], depuis=self.date_start, jusqua=self.date_end
            ),
            formation__quand__range=(self.date_start, self.date_end),
        ).select_related('benevole', 'formation')
        for frm in formations:
            formation = frm.formation
            yield([
                frm.benevole.nom_prenom, formation.titre, formation.descriptif, formation.categorie.nom,
                formation.quand.strftime('%d.%m.%Y'), formation.duree or ''
            ])


class BenevoleJournalView(PermissionRequiredMixin, ListView):
    model = Journal
    paginate_by = 20
    template_name = 'benevole/benevole_journal.html'
    permission_required = 'benevole.change_benevole'

    def get(self, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.benevole.persona.journaux.order_by('-quand')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
        }


class BenevoleJournalEditView(PermissionRequiredMixin, CreateUpdateView):
    model = Journal
    form_class = JournalForm
    pk_url_kwarg = 'pk_jr'
    json_response = True
    permission_required = 'benevole.change_benevole'

    def form_valid(self, form):
        if not form.instance.persona_id:
            form.instance.persona = get_object_or_404(Benevole, pk=self.kwargs['pk']).persona
        form.instance.qui = self.request.user
        form.instance.quand = timezone.now()
        form.instance.auto = False
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleJournalDeleteView(DeleteView):
    model = Journal
    pk_url_kwarg = 'pk_jr'

    def form_valid(self, form):
        if self.object.qui != self.request.user:
            raise PermissionDenied("Vous ne pouvez supprimer que vos propres notes.")
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleNotesFraisView(PermissionRequiredMixin, ListView):
    model = NoteFrais
    paginate_by = 20
    template_name = 'benevole/benevole_notesfrais.html'
    permission_required = 'benevole.change_benevole'

    def get(self, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.benevole.notefrais_set.prefetch_related('lignes').order_by('-mois')

    def create_note(self, calculator):
        notes = calculator.calculer_frais(benevole=self.benevole, enregistrer=False)
        lignes = calculator.benevoles.get(self.benevole)
        if lignes:
            # Simuler une note de frais pour le template
            fake_note = {
                'mois': calculator.mois, 'service': self.kwargs['app'], 'date_export': None,
                'lignes': {'all': lignes}
            }
            fake_note['somme_totale'] = sum([lig.montant or 0 for lig in fake_note['lignes']['all']])
            return fake_note

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
        }
        if context['page_obj'].number == 1:
            # Si page=1 et pas de notes pour le service en cours le mois courant et le mois passé,
            # calculer dynamiquement s'il y a des frais pour ces mois.
            context['object_list'] = list(context['object_list'])
            ce_mois = date.today().replace(day=1)
            mois_passe = (ce_mois - timedelta(days=2)).replace(day=1)
            calc_ce_mois = canton_app.fact_policy.calculateur_frais(ce_mois)
            calc_mois_passe = canton_app.fact_policy.calculateur_frais(mois_passe)
            notes_app = [note for note in context['object_list'] if note.service == self.kwargs['app']]
            if not notes_app or notes_app[0].mois < mois_passe:
                note = self.create_note(calc_mois_passe)
                if note:
                    context['object_list'].insert(0, note)
            if not notes_app or notes_app[0].mois < ce_mois:
                note = self.create_note(calc_ce_mois)
                if note:
                    context['object_list'].insert(0, note)

        if settings.NOTESFRAIS_TRANSMISSION_URLNAME:
            context['trans_url'] = reverse(settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[0, 0])
        return context


class BenevoleNotesFraisRefaire(PermissionRequiredMixin, View):
    permission_required = 'benevole.change_benevole'

    def post(self, request, *args, **kwargs):
        note = get_object_or_404(NoteFrais, pk=self.kwargs['pk'])
        benev = note.benevole
        with transaction.atomic():
            calculateur = canton_app.fact_policy.calculateur_frais(note.mois)
            calculateur.calculer_frais(benevole=benev, enregistrer=True, refaire=True)
        messages.success(
            request,
            f"La note de frais «{note.service}» de «{benev}» pour {note.mois} a été recaculée."
        )
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class BenevoleArchiveView(PermissionRequiredMixin, View):
    """
    Vue actuellement non utilisée, l'archivage se faisant par cessation de toutes les activités.
    À supprimer si la méthode actuelle se confirme.
    """
    permission_required = 'benevole.change_benevole'

    def post(self, request, *args, **kwargs):
        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        if benevole.archive_le:
            # Désarchiver
            if not request.user.has_perm('benevole.change_benevole'):
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour réactiver cette personne.")
            with transaction.atomic():
                benevole.desarchiver()
            messages.success(request, f"{benevole} a bien été réactivé.")
        else:
            # Archiver
            if not request.user.has_perm('benevole.change_benevole'):
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour archiver cette personne.")
            with transaction.atomic():
                msg = benevole.archiver()
                success_msg = f"{benevole} a bien été archivé. {msg}"
            messages.success(request, success_msg)
        return HttpResponseRedirect(reverse('benevoles', args=[self.kwargs['app']]))


class BenevoleFichierEditView(PermissionRequiredMixin, FichierEditView):
    parent_model = Persona
    permission_required = 'benevole.change_benevole'


class BenevoleFichierListeView(PermissionRequiredMixin, FichierListeView):
    parent_model = Persona
    permission_required = 'benevole.view_benevole'
    editurl_name = 'benevole-fichier-edit'
    deleteurl_name = 'benevole-fichier-delete'


class BenevoleFichierDeleteView(PermissionRequiredMixin, FichierDeleteView):
    parent_model = Persona
    permission_required = 'benevole.change_benevole'
