from datetime import date, timedelta
from functools import partial

from django import forms
from django.db import transaction
from django.db.models import F, Q, Value
from django.db.models.signals import pre_save
from django.urls import reverse_lazy

from client.forms import AdresseFormMixin, PersonaFormMixin
from common.forms import (
    BootstrapMixin, DateInput, DateRangeField, FormsetMixin, HMDurationField,
    LanguageMultipleChoiceField, ModelForm, NPALocaliteMixin
)
from common.models import Utilisateur
from common.utils import current_app

from .models import Activite, Benevole, Formation, TypeActivite, benevole_saved


class BenevActiviteForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Activite
        fields = ['type_act', 'duree', 'inactif', 'note']
        field_classes = {
            'duree': DateRangeField,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance.pk is not None:
            self.fields['type_act'].disabled = True

    def _change_prefix(self):
        return f"activité {self.instance.type_act.nom}"

    def clean_duree(self):
        duree = self.cleaned_data.get('duree')
        if duree and not duree.lower:
            raise forms.ValidationError("La date de début d’activité est obligatoire")
        return duree


class BenevoleForm(
    BootstrapMixin, NPALocaliteMixin, AdresseFormMixin, PersonaFormMixin, FormsetMixin, ModelForm
):
    langues = LanguageMultipleChoiceField(label="Langues", required=False)
    langues_select = forms.CharField(
        label="Langues",
        widget=forms.TextInput(attrs={
            'class': 'form-control autocomplete',
            'autocomplete': 'off',
            'placeholder': 'Ajouter une langue',
            'data-searchurl': reverse_lazy('langue-autocomplete'),
        }),
        required=False
    )
    persona_fields = [
        'nom', 'prenom', 'genre', 'date_naissance', 'courriel', 'langues',
    ]

    class Meta(BootstrapMixin.Meta):
        model = Benevole
        exclude = ['utilisateur']
        fields = [
            'formation', 'absences', 'remarques',
        ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._send_save_signal = True
        ActivitesFormSet = forms.inlineformset_factory(
            Benevole, Activite, form=BenevActiviteForm, extra=1, min_num=1, can_delete=False,
        )
        self.act_formset = ActivitesFormSet(instance=kwargs['instance'], data=kwargs.get('data'))

    def formsets(self):
        return [self.formset, self.act_formset]

    def clean_courriel(self):
        courriel = self.cleaned_data.get('courriel')
        if courriel and self["courriel"]._has_changed() and Utilisateur.objects.filter(email=courriel).exists():
            raise forms.ValidationError(
                "Il existe déjà un utilisateur de cette application avec ce courriel."
            )
        return courriel

    def clean(self):
        cleaned_data = super().clean()
        if self.instance.pk is None and (
            Benevole.objects.filter(
                persona__nom=cleaned_data.get('nom'), persona__prenom=cleaned_data.get('prenom'),
                persona__date_naissance=cleaned_data.get('date_naissance')
            ).exists()
        ):
            raise forms.ValidationError(
                "Il existe déjà un bénévole avec le même nom et la même date de naissance"
            )
        return cleaned_data

    def save(self, **kwargs):
        pre_save.send(sender=Benevole, instance=self.instance)
        is_new = self.instance.pk is None
        benevole = super().save(**kwargs)
        if self._send_save_signal:
            transaction.on_commit(
                partial(benevole_saved.send, sender=Benevole, instance=benevole, created=is_new)
            )
        return benevole


class BenevoleTransportForm(BenevoleForm):
    class Meta(BenevoleForm.Meta):
        fields = [f for f in BenevoleForm.Meta.fields if f != "formation"] + [
            'contacts', 'no_plaques', 'vehicule', 'nb_portes_vhc', 'incompats',
            'macaron_valide', 'no_macaron', 'conduit_vehicule', 'ne_pas_defrayer',
        ]
        field_classes = {
            'macaron_valide': DateRangeField,
        }

    def clean_macaron_valide(self):
        duree = self.cleaned_data.get('macaron_valide')
        if duree and (not duree.upper or not duree.lower):
            raise forms.ValidationError("Vous devez saisir à la fois le début et la fin de validité.")
        return duree


class BenevoleFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}),
        required=False
    )
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Tél', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    activites = forms.ChoiceField(choices=(), required=False)
    age = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': '>80 ou <70', 'autocomplete': 'off', 'size': '5'}),
        required=False
    )
    listes = forms.ChoiceField(
        choices=(),
        widget=forms.Select(attrs={'class': 'w-auto ms-auto immediate_submit noajax', 'form': 'benev-filter-form'}),
        required=False
    )
    LIST_CHOICES = ()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.LIST_CHOICES:
            self.fields['listes'].choices = self.LIST_CHOICES
        else:
            del self.fields['listes']
        acts = TypeActivite.par_domaine(current_app())
        self.fields['activites'].choices = (
            ('', '------'), ('all', 'Toutes activites')
        ) + tuple((act.code, act.nom) for act in acts)

    def filter(self, benevs):
        # Le filtrage selon l'activité se fait dans la vue.
        if self.cleaned_data['nom']:
            term = self.cleaned_data['nom'].split()[0]
            benevs = benevs.filter(
                Q(persona__nom__unaccent__icontains=term) | Q(persona__prenom__unaccent__icontains=term)
            )
        if self.cleaned_data['npa_localite']:
            benevs = benevs.filter(
                Q(persona__adresseclient__npa__icontains=self.cleaned_data['npa_localite']) |
                Q(persona__adresseclient__localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            benevs = benevs.filter(
                Q(persona__telephone__tel__nospaces__icontains=self.cleaned_data['tel'].replace(' ', ''))
            ).distinct()
        if self.cleaned_data['age']:
            op_str = "".join([lt for lt in self.cleaned_data['age'] if lt in ['<', '>', '=']])
            try:
                age_q = timedelta(days=float("".join(
                    [lt for lt in self.cleaned_data['age'] if lt.isdigit() or lt in ['.', ',']]
                )) * 365.25)
            except ValueError:
                pass
            else:
                if not op_str:
                    age_filter = {"age_duree__range": (
                        # Déduire 4 jours pour prendre en compte les diff. annuelles
                        age_q - timedelta(days=4), age_q + timedelta(days=365.25)
                    )}
                else:
                    op = {'<': 'lt', '>': 'gt', '<=': 'lte', '>=': 'gte'}.get(op_str)
                    age_filter = {f"age_duree__{op}": age_q}
                benevs = benevs.annotate(
                    age_duree=date.today() - F('persona__date_naissance')
                ).filter(**age_filter)

        return benevs


class FormationForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Formation
        fields = '__all__'
        field_classes = {
            'duree': HMDurationField,
        }
        widgets = {
            'quand': DateInput,
        }
