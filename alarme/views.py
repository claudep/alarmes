from datetime import date, datetime, timedelta
from decimal import Decimal
from functools import partial
from io import BytesIO
from itertools import groupby
from operator import attrgetter, itemgetter

from dateutil import relativedelta

from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import RedirectURLMixin
from django.contrib.postgres.aggregates import ArrayAgg
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.core.serializers import serialize
from django.db import IntegrityError, transaction
from django.db.models import Count, DateField, F, Max, Min, Prefetch, Q, Sum
from django.db.models.functions import Trunc, TruncMonth
from django.http import (
    HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
)
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.utils.functional import cached_property
from django.utils.module_loading import import_string
from django.utils.text import slugify
from django.views.generic import (
    CreateView, DeleteView, FormView, TemplateView, UpdateView, View,
)

from benevole.models import Benevole, LigneFrais, NoteFrais, TypeActivite
from client.models import (
    AdressePresence, Alerte, Client, Journal, PersonaBaseQuerySet, Referent
)
from client.views import (
    ClientAccessCheckMixin, ClientDetailContextMixin, ClientEditViewBase,
    ClientJournalMixin, ClientListViewBase
)
from common.choices import Services
from common.export import ExpLine
from common.forms import MoisForm
from common.models import Fichier, TypeFichier, Utilisateur
from common.utils import canton_abrev, canton_app, same_month
from common.views import (
    BasePDFView, CreateUpdateView, ExportableMixin, FilterFormMixin,
    JournalMixin, ListView, UtilisateurUpdateView
)
from . import forms
from . import models
from .pdf import ConventionLocPDF, QuestionnairePDF


class AlarmeAccessMixin:
    """
    Mixin vérifiant que l'utilisateur connecté peut avoir un accès minimal
    (collaborateur ou bénévole) aux vues de l'alarme.
    """
    def dispatch(self, request, *args, **kwargs):
        has_access = (
            request.user.has_perm('alarme.view_alarme') or
            Benevole.objects.par_domaine('alarme').filter(utilisateur=request.user).exists()
        )
        if not has_access:
            raise PermissionDenied("Désolé, vous n'avez pas les permissions suffisantes.")
        return super().dispatch(request, *args, **kwargs)


def user_profiles(user):
    """Profils possibles: gestion, besoins, atelier, benevole."""
    profiles = []
    if any([
        user.has_perm('alarme.change_installation'),
        user.has_perm('alarme.view_facture')
    ]):
        return ['gestion']
    if user.has_perm('alarme.change_materiel'):
        profiles.append('atelier')
    if user.has_perm('besoins.view_questionnaire'):
        profiles.append('besoins')
    if user.is_benevole or "Intervenants alarme" in [gr.name for gr in user.groups.all()]:
        profiles.append('app')
    return profiles


class HomeView(TemplateView):
    template_name = 'alarme/index.html'

    def dispatch(self, request, *args, **kwargs):
        profiles = user_profiles(request.user)
        if 'gestion' in profiles:
            return super().dispatch(request, *args, **kwargs)
        if len(profiles) > 1:
            return HttpResponseRedirect(reverse('home-choice'))
        if 'besoins' in profiles:
            return HttpResponseRedirect(reverse('besoins:home'))
        return HttpResponseRedirect(reverse('home-app'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = date.today()
        context.update({
            'nouveaux': {
                'clients': Client.objects.par_service(Services.ALARME).avec_adresse(today).annotate(
                    num_interv=Count('mission'), num_installs=Count('installation'),
                ).filter(
                    num_interv=0, num_installs=0, partenaire_de__isnull=True
                ),
                'missions': models.Mission.objects.filter(
                    client__isnull=False, effectuee__isnull=True, type_mission__code='NEW',
                ).annotate(
                    adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                        today, outer_field_name='client__persona_id'
                    )
                ).order_by('delai').select_related('client', 'type_mission', 'intervenant'),
            },
            'missions': models.Mission.objects.filter(
                client__isnull=False, effectuee__isnull=True
            ).exclude(type_mission__code='NEW').order_by('delai'
            ).annotate(
                adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                    today, outer_field_name='client__persona_id'
                )
            ).select_related('client', 'type_mission', 'intervenant'),
            'mat_en_attente': {
                "alarmes": models.Installation.objects.filter(
                    date_fin_abo__lte=today, retour_mat__isnull=True
                ).select_related("client").annotate(
                    adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                        today, outer_field_name="client__persona_id"
                    )
                ).order_by('date_fin_abo'),
                "app_stock": Utilisateur.objects.annotate(
                    stock=Count("alarme"),
                    act_alarme=Count(
                        "benevole__activite",
                        filter=Q(benevole__activite__type_act__code__in=[
                            TypeActivite.VISITE_ALARME, TypeActivite.INSTALLATION
                        ]) & Q(benevole__activite__duree__contains=date.today())
                    ),
                ).filter(
                    Q(stock__gt=0) & (
                        Q(is_active=False) | (Q(benevole__isnull=False) & Q(act_alarme=0))
                    )
                ),
            },
            'alertes': list(Alerte.objects.filter(
                cible='alarme', traite_le__isnull=True
            ).select_related("persona").order_by("recu_le")),
            'absences': AdressePresence.objects.annotate(
                services=ArrayAgg(
                    'adresse__persona__client__prestations__service',
                    filter=Q(adresse__persona__client__prestations__duree__contains=today)
                )
            ).filter(
                Q(services__contains=[Services.ALARME]) &
                Q(depuis__lte=today) &
                (Q(jusqua__isnull=True) | Q(jusqua__gt=today))
            ).order_by('dernier_contact'
            ).select_related('adresse__persona__client'),
            'current_tab': (
                self.kwargs['tabname']
                if self.kwargs.get('tabname') in ('interventions', 'alertes', 'absences', 'materiel')
                else 'interventions'
            ),
            'ape_url': reverse('apparts') if apps.is_installed('ape') else None,
        })
        hospits = Client.objects.par_service(Services.ALARME).hospitalises().values_list("persona_id", flat=True)
        for alerte in context['alertes']:
            alerte.client_hospitalise = alerte.persona_id in hospits
        context['alertes_has_rems'] = any(al.remarque for al in context['alertes'])
        context.update({
            "nbre_nouveaux": len(context["nouveaux"]["clients"]) + len(context["nouveaux"]["missions"]),
            "nbre_mat_attente": (
                len(context["mat_en_attente"]["alarmes"]) +
                len(context["mat_en_attente"]["app_stock"])
            ),
        })
        if self.request.user.has_perm('alarme.view_facture'):
            context['factures'] = models.Facture.non_transmises().count()
        return context


class HomeChoiceView(AlarmeAccessMixin, TemplateView):
    template_name = 'alarme/index-choice.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'profiles': user_profiles(self.request.user),
        }


class HomeAppView(AlarmeAccessMixin, TemplateView):
    template_name = 'alarme/app/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        clients_visites = clients_a_visiter(self.request.user).prefetch_related(
            Prefetch('installation_set', queryset=models.Installation.objects.select_related(
                'alarme', 'alarme__modele', 'abonnement'
            )),
        ).order_by(F('derniere').asc(nulls_first=True), 'persona__nom', 'persona__prenom')
        a_visiter = []
        afaire = 0
        today = date.today()
        for client in clients_visites:
            typ_en_cours = set([interv.type_mission.code for interv in client.interv_en_cours])
            if not client.interv_en_cours:
                client.href = reverse('benevole-visite-new', args=[client.pk])
            elif typ_en_cours != {'VISITE'}:
                # Une autre intervention est pendante, ignorer ce client.
                continue
            elif client.interv_en_cours[0].intervenant != self.request.user:
                # Une visite est planifiée avec un autre bénévole
                continue
            else:
                client.planifiee_le = client.interv_en_cours[0].planifiee
                client.href = reverse('benevole-mission-edit', args=[client.interv_en_cours[0].pk])
            if (
                not client.date_resiliation() and (
                    client.derniere is None or
                    (today - client.derniere) > timedelta(days=settings.DELAI_ENTRE_VISITES)
                )
            ):
                client.afaire = True
                afaire += 1
            else:
                client.afaire = False
            client.next_interv = client.derniere + timedelta(days=182) if client.derniere else None
            a_visiter.append(client)
        missions = self.request.user.mission_set.filter(effectuee=None).select_related(
            "client", "type_mission"
        ).annotate(
            adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                today, outer_field_name="client__persona_id"
            )
        )
        # La personne a-t-elle suivi une formation pour l'analyse des besoins
        can_question = self.request.user.is_benevole and (
            self.request.user.benevole.formations.filter(
                categorie__code="besoins", quand__lte=date.today()
            ).exists()
        )
        if can_question:
            from besoins.models import Questionnaire

            quests_a_faire = Questionnaire.clients_a_questionner().filter(
                pk__in=missions.values_list("client__pk", flat=True)
            )
            for mission in missions:
                mission.question_besoins = True
        context.update({
            'missions_ouvertes': models.Mission.objects.filter(
                effectuee__isnull=True, intervenant__isnull=True
            ).order_by('delai').select_related(
                'client', 'type_mission'
            ),
            'mes_missions': missions,
            'visites': a_visiter,
            'visites_a_faire': afaire,
        })
        return context


def controles(request):
    referent_statut_filter = Q(referent__repondant__isnull=False)
    if 'courrier_debiteur' in canton_app.pdf_doc_registry:
        referent_statut_filter |= Q(referent__facturation_pour__overlap=['al-abo', 'al-tout'])
    context = {
        'clients_sans_visiteurs': Client.objects.par_service(Services.ALARME).annotate(
            num_installs=Count('installation', filter=Q(installation__date_fin_abo__isnull=True))
        ).filter(num_installs__gt=0, visiteur=None).order_by('persona__nom'),
        'courriers_non_envoyes': Client.objects.par_service(Services.ALARME).annotate(
            non_envoi=Count('referent', filter=Q(
                (referent_statut_filter) &
                Q(referent__courrier_envoye__isnull=True) &
                Q(referent__date_archive__isnull=True)
            )),
            num_installs=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
        ).filter(
            archive_le=None, num_installs__gt=0,
        ).filter(
            Q(non_envoi__gt=0) | Q(courrier_envoye__isnull=True)
        ).order_by('persona__nom', 'persona__prenom'),
    }
    return render(request, 'alarme/controles.html', context=context)


class RefreshSelectView(View):
    def get(self, *args, **kwargs):
        model = {
            'ModeleAlarme': models.ModeleAlarme,
            'TypeMateriel': models.TypeMateriel,
        }.get(kwargs['modelname'])
        return HttpResponse('\n'.join(
            ['<option value="" selected>---------</option>'] +
            [f'<option value="{item.pk}">{item.nom}</option>' for item in model.objects.all().order_by('nom')]
        ))


class ClientListView(ClientListViewBase):
    template_name = 'alarme/client_list.html'
    filter_formclass = forms.ClientFilterForm
    client_types = ['alarme']
    return_all_if_unbound = False

    def get_queryset(self):
        clients = super().get_queryset().exclude(partenaire_de__isnull=False)
        return clients.annotate(
            date_tout_debut=Min('installation__date_debut')
        ).prefetch_related(Prefetch(
            'installation_set', to_attr='installations',
            queryset=models.Installation.objects.filter(date_fin_abo__isnull=True).select_related(
                'alarme', 'alarme__modele', 'abonnement'
            )
        ))

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "alarme/base.html",
            'home_url': reverse('home'),
            'client_url_name': 'client-edit',
        }


class ClientEditView(ClientDetailContextMixin, ClientEditViewBase):
    form_class = forms.ClientForm
    template_name = 'alarme/client.html'
    client_type = Services.ALARME

    def get_queryset(self):
        return self.model._default_manager.all().annotate(
            date_tout_debut=Min('installation__date_debut')
        )

    def possede_quest_contrat(self, client):
        return client.fichiers.filter(
            typ__in=[TypeFichier.CONTRAT_ALARME, TypeFichier.QUEST_ALARME]
        ).count() > 1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='alarme/base.html')
        client = self.object
        if not self.is_create:
            install = client.derniere_alarme()
            if not install and (partenaire := client.get_partenaire_de()):
                install = client.partenaire_de.derniere_alarme()
            context.update({
                'install': install,
                'materiels': client.materiel_actuel(),
                'missions': client.mission_set.filter(effectuee__isnull=True),
                'rabais_auto': client.rabaisauto_set.order_by("-duree__startswith").first(),
                'quest_contrat_dispo': (
                    not client.get_partenaire_de() and (
                        not context["form"].readonly or not self.possede_quest_contrat(client)
                    )
                ),
            })
        return context


class ClientDonneesMedicView(ClientAccessCheckMixin, ClientJournalMixin, UpdateView):
    model = Client
    form_class = forms.DonneesMedicForm
    template_name = 'general_edit.html'
    journal_edit_message = "Modification des données médicales ({fields})"
    is_create = False

    def get_client(self):
        return self.object

    def form_valid(self, form):
        with transaction.atomic():
            super().form_valid(form)
            self.journalize(form, changed_values=True)
        return JsonResponse({'result': 'OK', 'reload': ''})


def get_appareil_abo(client):
    """
    Renvoi d'un tuple (appareil, abonnement) pour le client.
    En priorité, renvoie appareil/abo de la mission en cours (install ou changement)
    Sinon, renvoie appareil/abo actuellement installés, ou (None, None)
    """
    install_actu = client.alarme_actuelle()
    try:
        mission_en_cours = client.mission_set.filter(type_mission__code__in=['NEW', 'CHANGE']).latest('delai')
    except models.Mission.DoesNotExist:
        mission_en_cours = None

    appareil, abo = None, None
    if mission_en_cours:
        if mission_en_cours.alarme:
            appareil = mission_en_cours.alarme
        if mission_en_cours.abonnement:
            abo = mission_en_cours.abonnement

    if appareil is None and install_actu:
        appareil = install_actu.alarme
    if abo is None and install_actu:
        abo = install_actu.abonnement
    return appareil, abo


class ClientContratView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = '%(CANTON_APP)s.pdf.ContratPDF'

    def get(self, request, *args, **kwargs):
        appareil, abo = get_appareil_abo(self.client)
        if appareil is None:
            messages.error(request, "Il n'y a pas encore d’appareil attribué à ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        self.produce_kwargs = {'appareil': appareil, 'abo': abo}
        if request.GET.get('forsign') == 'yes':
            self.produce_kwargs['pour_signature'] = True
        return super().get(request, *args, as_img=request.GET.get('asimg') == 'yes', **kwargs)


class DocPourSignatureBaseView(ClientAccessCheckMixin, TemplateView):
    sig_form_class = None  # SimpleSignForm ou DoubleSignForm
    doc_type = None  # Contenu de Fichier.typ
    obj_name = ""
    fichier_titre_template = "Alarme - {obj_name} signé du {date}"
    success_message = None

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        self.appareil, self.abo = get_appareil_abo(self.client)

    def dispatch(self, request, *args, **kwargs):
        if self.appareil is None:
            messages.error(request, "Il n'y a pas encore d’appareil attribué à ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_class(self):
        raise NotImplementedError

    def get_pdf_context(self, form):
        return {}

    def post(self, request, *args, **kwargs):
        if self.read_only:
            messages.error(request, "Vous n'avez pas les droits d'édition pour ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        sig_form = self.sig_form_class(data=request.POST)
        if not sig_form.is_valid():
            messages.error(request, "Une erreur a été détectée dans l’envoi de la signature, veuillez réessayer")
            return self.render_to_response(self.get_context_data())
        # Produire le fichier PDF avec la signature PNG dans sig_data.
        PDFClass = self.get_pdf_class()
        pdf_temp = BytesIO()
        pdf = PDFClass(pdf_temp)
        pdf.produce(self.client, **self.get_pdf_context(sig_form))
        # Enregistrer le document signé dans media/...
        saved_name = default_storage.save(f"clients/{pdf.get_filename(self.client)}", pdf_temp)
        # Lier au client par un fichier
        with transaction.atomic():
            Fichier.objects.create(
                content_object=self.client.persona,
                titre=self.fichier_titre_template.format(
                    obj_name=self.obj_name, date=date.today().strftime('%d.%m.%Y')
                ),
                typ=self.doc_type, fichier=saved_name, qui=request.user, quand=timezone.now()
            )
            Journal.objects.create(
                persona=self.client.persona,
                description=f"Enregistrement d’un fichier «{self.obj_name}» signé par le client.",
                quand=timezone.now(), qui=request.user
            )
        messages.success(request, self.success_message)
        return HttpResponseRedirect(self.client.get_absolute_url())


class ClientContratSignatureView(DocPourSignatureBaseView):
    pdf_class = '%(CANTON_APP)s.pdf.ContratPDF'
    template_name = 'alarme/doc_a_signer.html'
    sig_form_class = forms.DoubleSignForm
    doc_type = 'contrat'
    obj_name = "Contrat"
    success_message = "Le contrat a bien été signé et enregistré"

    def get_pdf_class(self):
        return import_string(self.pdf_class % {'CANTON_APP': settings.CANTON_APP})

    def get_pdf_context(self, form):
        return {
            'appareil': self.appareil,
            'abo': self.abo,
            'signature_client': form.cleaned_data['sig_data_cl'],
            'signature_cr': form.cleaned_data['sig_data_cr'],
            'signature_lieu': form.cleaned_data['lieu'],
            'signature_cr_nom': f"{self.request.user.last_name} {self.request.user.first_name}",
        }

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            client=self.client, form=self.sig_form_class(),
            pdf_url_name='client-contrat',
            **kwargs,
        )


class ClientQuestionnaireView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = 'alarme.pdf.QuestionnairePDF'

    def get(self, request, *args, **kwargs):
        client = self.get_object()
        appareil, abo = get_appareil_abo(client)
        try:
            install_mission = client.mission_set.filter(type_mission__code__in=['NEW', 'CHANGE']).latest('delai')
        except models.Mission.DoesNotExist:
            install_mission = None
        self.produce_kwargs = {
            'install_mission': install_mission,
            'appareil': appareil,
            'abo': abo,
            'pour_signature': request.GET.get('forsign') == 'yes',
        }
        return super().get(request, *args, as_img=request.GET.get('asimg') == 'yes', **kwargs)


class ClientQuestionnaireSignatureView(DocPourSignatureBaseView):
    template_name = 'alarme/questionnaire_a_signer.html'
    success_message = "Le questionnaire a bien été signé et enregistré"
    sig_form_class = forms.SimpleSignForm
    doc_type = 'quest'
    obj_name = "Questionnaire"

    def dispatch(self, request, *args, **kwargs):
        try:
            self.install_mission = self.client.mission_set.filter(
                type_mission__code__in=['NEW', 'CHANGE']
            ).latest('delai')
        except models.Mission.DoesNotExist:
            messages.error(
                request,
                "Il n'y a pas encore d’intervention d’installation prévue pour ce client"
            )
            return HttpResponseRedirect(self.client.get_absolute_url())
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_class(self):
        return QuestionnairePDF

    def get_pdf_context(self, form):
        return {
            'appareil': self.appareil,
            'abo': self.abo,
            'install_mission': self.install_mission,
            'signature_file': form.cleaned_data['sig_data'],
            'signature_lieu': form.cleaned_data['lieu'],
        }

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            client=self.client, form=self.sig_form_class(),
            pdf_url_name='client-questionnaire',
            cr_canton={
                'JU': 'Croix-Rouge suisse Canton du Jura',
                'NE': 'Croix-Rouge neuchâteloise',
            }.get(canton_abrev()),
            **kwargs
        )


class ClientConvLocalisationView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = 'alarme.pdf.ConventionLocPDF'

    def get(self, request, *args, **kwargs):
        client = self.get_object()
        if request.GET.get('forsign') == 'yes':
            self.produce_kwargs['pour_signature'] = True
        return super().get(request, *args, as_img=request.GET.get('asimg') == 'yes', **kwargs)


class ClientConvLocalisationSignView(DocPourSignatureBaseView):
    template_name = 'alarme/doc_a_signer.html'
    success_message = "La convention de localisation a bien été signée et enregistrée"
    sig_form_class = forms.SimpleSignForm
    doc_type = 'contrat'
    obj_name = "Convention"
    fichier_titre_template = "Alarme - Convention de localisation signée du {date}"

    def get_pdf_class(self):
        return ConventionLocPDF

    def get_pdf_context(self, form):
        return {
            'signature_file': form.cleaned_data['sig_data'],
            'signature_lieu': form.cleaned_data['lieu'],
        }

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            client=self.client, form=self.sig_form_class(),
            pdf_url_name='client-localisation',
            **kwargs
        )


class ClientCourrierView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = '%(CANTON_APP)s.pdf.CourrierAlarmePDF'


class ReferentCourrierView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Referent
    pk_url_kwarg = 'pk_ref'

    def get_pdf_class(self):
        try:
            pdf_class = canton_app.pdf_doc_registry[self.kwargs['type']]['class']
        except KeyError:
            raise Http404(f"Type de courrier {self.kwargs['type']} inconnu")
        return import_string(pdf_class)


class ClientMissionsView(
    ClientAccessCheckMixin, PermissionRequiredMixin, ClientDetailContextMixin, ListView
):
    model = models.Mission
    template_name = 'alarme/client_missions.html'
    permission_required = 'alarme.change_mission'
    paginate_by = 25

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(
            client=self.client
        ).annotate(frais_total=Sum('frais__cout')).order_by('-effectuee', '-delai')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientEnvoiCentraleView(FormView):
    form_class = forms.EnvoiCentraleForm
    template_name = 'alarme/apercu_envoi.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour envoyer des données.")
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        install = self.client.derniere_alarme()
        try:
            mission = self.client.mission_set.filter(effectuee=None).earliest('delai')
        except models.Mission.DoesNotExist:
            mission = None
        if install:
            alarme = install.alarme
        elif mission:
            alarme = mission.alarme
        else:
            alarme = None
        return default_centrale_message(self.client, alarme, mission)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'recipient': settings.CENTRALE_EMAIL,
        }

    def form_valid(self, form):
        envoi_centrale(self.request, form.cleaned_data['subject'], form.cleaned_data['body'])
        Journal.objects.create(
            persona=self.client.persona, description=f"Envoi des données du jour à {settings.CENTRALE_EMAIL}",
            details=(
                f"<b>Destinataire:</b> {settings.CENTRALE_EMAIL}\n"
                f"<b>Sujet:</b> {form.cleaned_data['subject']}\n"
                f"<b>Message:</b>\n{form.cleaned_data['body']}"
            ),
            quand=timezone.now(), qui=self.request.user
        )
        messages.success(self.request, f"Le message a bien été envoyé à {settings.CENTRALE_EMAIL}")
        return HttpResponseRedirect(reverse('client-edit', args=[self.client.pk]))


class ClientFacturesView(ClientAccessCheckMixin, ClientDetailContextMixin, ListView):
    model = models.Facture
    paginate_by = 25
    template_name = 'alarme/client_factures.html'

    def get_queryset(self):
        return super().get_queryset().filter(client=self.client).order_by('-date_facture')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        need_send = any(fact.exporte is None for fact in context['object_list'])
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'trans_url': settings.FACTURES_TRANSMISSION_URL if need_send else '',
        }


class FactureEditView(ClientJournalMixin, CreateUpdateView):
    model = models.Facture
    form_class = forms.FactureForm
    pk_url_kwarg = 'pk_fact'
    template_name = 'alarme/facture_edit.html'
    journal_edit_message = "Modification d’une facture ({fields})"
    json_response = True

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_client(self):
        return self.client

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }

    def form_valid(self, form):
        form.instance.client = self.client
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})

    def journalize(self, form, add_message=None, **kwargs):
        add_msg = "Création d’une facture"
        if form.instance.montant:
            add_msg += f" de CHF {form.instance.montant}"
        if form.instance.date_facture:
            add_msg += f" du {form.instance.date_facture.strftime('%d.%m.%Y')}"
        add_msg += f" ({form.instance.libelle})"
        super().journalize(form, add_message=add_msg, **kwargs)


class FactureDeleteView(DeleteView):
    model = models.Facture
    pk_url_kwarg = 'pk_fact'

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.object.delete()
        msg = f"Suppression de la facture du {self.object.date_facture}"
        if self.object.montant:
            msg += f" ({self.object.montant})"
        Journal.objects.create(
            persona=self.object.client.persona, description=msg,
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class FacturationView(PermissionRequiredMixin, TemplateView):
    template_name = 'alarme/facturation.html'
    permission_required = 'alarme.view_facture'

    def get_context_data(self, **kwargs):
        mois_factures = models.Facture.objects.filter(
            mois_facture__isnull=False
        ).annotate(
            month=TruncMonth('mois_facture'),
        ).values('month').annotate(num_fact=Count('id')).order_by('-month')
        mois_a_facturer = None
        if settings.FACTURES_TRIMESTRIELLES:
            if date.today().month in (3, 6, 9, 12):
                # filtrer sur nbre de facture > 50 pour détecter un mois complet facturé
                last_mois = mois_factures.values('month').annotate(num_fact=Count('id')).filter(num_fact__gt=50).order_by('-month').first()
                mois_suivant = date.today().replace(day=1) + relativedelta.relativedelta(months=1)
                if last_mois and mois_suivant > last_mois['month']:
                    mois_a_facturer = mois_suivant
        else:
            # Mois précédent
            mois_a_facturer = date.today().replace(day=1) - relativedelta.relativedelta(months=1)
        context = {
            **super().get_context_data(**kwargs),
            'mois_factures': mois_factures.values_list('month', flat=True).order_by('-month'),
            'mois_a_facturer': mois_a_facturer,
            'fact_trimestre': settings.FACTURES_TRIMESTRIELLES,
            'non_transmises': models.Facture.non_transmises().count(),
        }
        if apps.is_installed('ape'):
            from ape.models import Facture

            context.update({
                'factures_ape_url': reverse('appart-factures'),
                'ape_non_transmises': Facture.objects.filter(exporte__isnull=True).count(),
            })
        return context


class ArticlesView(TemplateView):
    template_name = 'alarme/articles.html'

    def get_context_data(self, **kwargs):
        return {'articles': models.ArticleFacture.objects.all().order_by('code')}


class ArticleEditView(PermissionRequiredMixin, CreateUpdateView):
    permission_required = "alarme.change_articlefacture"
    model = models.ArticleFacture
    form_class = forms.ArticleFactureForm
    json_response = True

    def form_valid(self, form):
        super().form_valid(form)
        messages.success(self.request, f"L’article {self.object} a bien été modifié.")
        return JsonResponse({"result": "OK", "reload": "page"})


class FacturerMoisView(PermissionRequiredMixin, FormView):
    form_class = forms.FacturerForm
    template_name = 'general_edit.html'
    permission_required = 'alarme.add_facture'

    def get_initial(self):
        return {**super().get_initial(), 'date_facture': date.today()}

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'form_classes': 'unmonitor normal_submit'}

    def form_valid(self, form):
        dt = form.cleaned_data['date_facture']
        # Existing factures not sent to ERP, change date_facture to dt
        models.Facture.objects.filter(exporte__isnull=True).update(date_facture=dt)
        mois = date(self.kwargs['year'], self.kwargs['month'], 1)
        try:
            with transaction.atomic():
                num_fact = models.Installation.generer_factures(mois, date_factures=dt)
                num_fact += models.MaterielClient.generer_factures(mois, date_factures=dt)
                if apps.is_installed('ape'):
                    from ape.models import Appart
                    Appart.generer_factures(mois, date_factures=dt)
                models.Facture.objects.filter(date_facture=None).update(date_facture=dt)
        except Exception as err:
            messages.error(self.request, f"Désolé, une erreur s’est produite durant la facturation: {err}")
        else:
            messages.success(self.request, f"{num_fact} factures ont été générées pour le mois de {django_format(mois, 'F Y')}")
        return HttpResponseRedirect(reverse('facturation'))


class FactureListView(PermissionRequiredMixin, FilterFormMixin, ExportableMixin, ListView):
    model = models.Facture
    filter_formclass = forms.FactureFilterForm
    title = "Factures"
    paginate_by = 25
    template_name = 'alarme/factures.html'
    permission_required = 'alarme.view_facture'
    col_widths = [18, 45, 50, 40, 10]

    def get_queryset(self, **kwargs):
        return super().get_queryset(**kwargs).select_related('client', 'install__abonnement'
            ).order_by('-date_facture', 'client__persona__nom')

    def export_lines(self, context):
        yield ExpLine(['Date de facture', 'Client', 'Libellé', 'Article', 'Montant'], bold=True)
        for facture in self.get_queryset():
            yield [
                facture.date_facture, str(facture.client), facture.libelle,
                str(facture.article), facture.montant
            ]

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'title': self.title,
        }


class FacturesNonTransmisesView(FactureListView):
    def get_queryset(self):
        return super().get_queryset(base_qs=self.model.non_transmises())

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'csv_export': settings.FACTURES_EXPORT_URL,
            'api_export': settings.FACTURES_TRANSMISSION_URL,
            'title': 'Factures non transmises à la compta',
        }


class FacturesMoisView(FactureListView):
    def get_queryset(self):
        return super().get_queryset().annotate(
            month=Trunc('mois_facture', 'month', output_field=DateField())
        ).filter(
            month=date(self.kwargs['year'], self.kwargs['month'], 1)
        ).order_by('client__persona__nom')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'title': 'Factures mensuelles de ' + django_format(date(self.kwargs['year'], self.kwargs['month'], 1), 'F Y'),
        }


class DefraiementView(PermissionRequiredMixin, ListView):
    template_name = 'alarme/defraiements.html'
    permission_required = 'alarme.view_facture'

    def get(self, request, *args, **kwargs):
        form_data = {
            'year': request.GET.get('year', str(date.today().year)),
            'month': request.GET.get('month', str(date.today().month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        # Mélange possible entre notes de frais calculées dynamiquement et
        # notes de frais enregistrées (benevole.NoteFrais).
        self.notes_existantes = {
            nt.benevole_id: nt for nt in NoteFrais.objects.filter(
                service='alarme', mois=self.un_du_mois
            ).prefetch_related(
                Prefetch('lignes', queryset=LigneFrais.objects.all().select_related('libelle'))
            )
        }
        calculateur = canton_app.fact_policy.calculateur_frais(self.un_du_mois)
        frais_dynamiques = calculateur.frais_par_benevole()
        type_map = canton_app.TYPE_FRAIS_MAP
        for benevole, frais_data in frais_dynamiques.items():
            note = self.notes_existantes.get(benevole.pk)
            frais_data['note_frais'] = note
            if note:
                frais_data['frais_sum'] = sum([
                    lig.montant_unit for lig in note.lignes.all()
                    if lig.libelle.no in [type_map['repas'], type_map['divers']]
                ])
                frais_data['kms'] = next((
                    lig.quantite for lig in note.lignes.all() if lig.libelle.no == type_map['kms']
                ), 0)
                frais_data['montant_total'] = note.somme_totale()
            else:
                frais_data['frais_sum'] = frais_data['frais_repas'] + frais_data['frais_divers']
                frais_data['montant_total'] = frais_data['frais_sum'] + (
                    (frais_data['kms'] * calculateur.TARIF_KM_ALARME) if frais_data['kms'] else Decimal(0)
                ) + (
                    frais_data.get("visites_defrayees", 0) * calculateur.TARIF_VISITES_DEFRAYEES
                )
        return list(frais_dynamiques.values())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        totaux = {
            "frais_sum": Decimal("0"), "kms": Decimal("0"),
            "num_visites": 0, "visites_defrayees": 0,
            "montant_total": Decimal("0")
        }
        tout_envoye = True
        for line in context['object_list']:
            if not line['note_frais'] or line['note_frais'].date_export is None:
                tout_envoye = False
            totaux['frais_sum'] += line['frais_sum']
            totaux['kms'] += line['kms']
            totaux['num_visites'] += line['num_visites']
            totaux["visites_defrayees"] += line.get("visites_defrayees", 0)
            totaux['montant_total'] += line.get('montant_total', Decimal('0'))
        context.update({
            'date_form': self.form,
            'totaux': totaux,
            'tout_envoye': tout_envoye,
            'bouton_figer': not self.notes_existantes and not same_month(self.un_du_mois, date.today()),
            'mois_passe': (self.un_du_mois + timedelta(days=32)).replace(day=1) <= date.today(),
        })
        if settings.NOTESFRAIS_TRANSMISSION_URLNAME and not same_month(self.un_du_mois, date.today()):
            context['transmission_url'] = reverse(
                settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[self.un_du_mois.year, self.un_du_mois.month]
            )
        return context


class DefraiementFigerView(PermissionRequiredMixin, View):
    permission_required = 'alarme.view_facture'

    def post(self, request, *args, **kwargs):
        un_du_mois = date(year=kwargs['year'], month=kwargs['month'], day=1)
        calculateur = canton_app.fact_policy.calculateur_frais(un_du_mois)
        created = calculateur.calculer_frais(enregistrer=True)
        messages.success(request, f"{created} notes ont été figées avec succès.")
        return HttpResponseRedirect(reverse('defraiements') + f"?year={kwargs['year']}&month={kwargs['month']}")


class DefraiementExportView(View):
    def post(self, request, *args, **kwargs):
        un_du_mois = date(year=kwargs['year'], month=kwargs['month'], day=1)
        liste_notes = NoteFrais.objects.filter(
            service='alarme', mois=un_du_mois
        ).prefetch_related('lignes').order_by('benevole__persona__nom', 'benevole__persona__prenom')
        num_lines = len(liste_notes)
        if not num_lines:
            messages.error(request, "Aucun frais n’a été enregistré pour ce mois")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('facturation')))

        export = canton_app.fact_policy.export_frais_benevoles(liste_notes, un_du_mois)
        return export.get_http_response(f'defraiements_alarme_{un_du_mois.year}_{un_du_mois.month:02}.xlsx')


class InstallationEditView(ClientJournalMixin, CreateUpdateView):
    """
    A priori, ne devrait être utilisée que pour modifier une installation existante,
    la création étant gérée par le moyen des interventions.
    """
    model = models.Installation
    form_class = forms.InstallationForm
    template_name = 'alarme/install_edit.html'
    journal_add_message = "Installation d’un nouvel appareil ({obj})"
    journal_edit_message = "Modification de l’installation alarme ({fields})"
    json_response = True

    def get_client(self):
        client = None
        if self.object:
            client = self.object.client
        elif 'client' in self.request.GET:
            client = Client.objects.get(pk=self.request.GET['client'])
        if client and not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_initial(self):
        initial = super().get_initial()
        if 'client' in self.request.GET:
            initial['client'] = Client.objects.get(pk=self.request.GET['client'])
        if 'alarme' in self.request.GET:
            initial['alarme'] = models.Alarme.objects.get(pk=self.request.GET['alarme'])
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['alarme'] = context['form'].instance.alarme if context['form'].instance.alarme_id else context['form'].initial.get('alarme')
        return context

    def journalize(self, form, **kwargs):
        if form.cleaned_data.get('nouvelle_alarme'):
            journal_msg = f"Changement d’appareil, ancien: {form.instance.alarme}, nouveau: {form.cleaned_data['nouvelle_alarme']}"
            Journal.objects.create(
                persona=self.get_client().persona, description=journal_msg,
                quand=timezone.now(), qui=self.request.user
            )
        else:
            super().journalize(form, **kwargs)

    def form_valid(self, form):
        super().form_valid(form)
        check_archive_client(self.object.client)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class InstallationCloseView(TemplateView):
    """Formulaire de résiliation"""
    template_name = 'alarme/uninstall.html'

    def dispatch(self, *args, **kwargs):
        self.install = get_object_or_404(models.Installation, pk=kwargs['pk'])
        if not self.install.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.edit_mode = self.install.date_fin_abo is not None
        self.uninstall_tm = models.TypeMission.objects.filter(code='UNINSTALL').first()
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(**{**kwargs,
            'client': self.install.client,
            'install_form': forms.UninstallForm(instance=self.install),
            'mission_form': forms.MissionSimpleCreateForm(
                initial={'type_mission': self.uninstall_tm}
            ) if not self.edit_mode else None,
        })

    def post(self, request, *args, **kwargs):
        install_form = forms.UninstallForm(instance=self.install, data=request.POST)
        frms = [install_form]
        if not self.edit_mode:
            mission_form = forms.MissionSimpleCreateForm(
                data=request.POST, initial={'type_mission': self.uninstall_tm}
            )
            if mission_form.has_changed():
                frms.append(mission_form)
                mission_form.instance.client = self.install.client
        else:
            mission_form = None
        if all([form.is_valid() for form in frms]):
            with transaction.atomic():
                for form in frms:
                    form.save()
                date_fin_abo = install_form.cleaned_data['date_fin_abo']

                # Le cas échéant, clore également le lien client appart de l'APE
                try:
                    # clientappart_set depends on ape app being installed
                    clientappart = self.install.client.clientappart_set.filter(duree__endswith=None).first()
                except AttributeError:
                    clientappart = None
                if clientappart:
                    clientappart.duree = (clientappart.duree.lower, date_fin_abo)
                    clientappart.save()

                if install_form.cleaned_data['mat_achete']:
                    self.install.alarme.date_archive = date_fin_abo
                    self.install.alarme.save()
                    models.JournalAlarme.objects.create(
                        alarme=self.install.alarme, description="Archivage de l’appareil",
                        quand=timezone.now(), qui=self.request.user
                    )
                    self.install.retour_mat = date_fin_abo
                    self.install.save(update_fields=['retour_mat'])
                    check_archive_client(self.install.client)
                if self.edit_mode:
                    message = f"Résiliation modifiée: {form.get_changed_string()}"
                else:
                    message = (
                        f"Fin de l’abonnement le {django_format(date_fin_abo, 'd.m.Y')},"
                        f" motif {self.install.get_motif_fin_display()}"
                    )
                    if self.install.remarques:
                        message += f", {self.install.remarques}"
                Journal.objects.create(
                    persona=self.install.client.persona,
                    description=message,
                    quand=timezone.now(), qui=request.user
                )
            return JsonResponse({'result': 'OK', 'reload': 'page'})
        else:
            return self.render_to_response(self.get_context_data(
                install_form=install_form, mission_form=mission_form
            ))


class InstallationCloseCancelView(FormView):
    """Formulaire d’anulation de résiliation"""
    template_name = 'general_edit.html'
    form_class = forms.CancelWithCommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].intro_text = (
            "Voulez-vous vraiment annuler cette résiliation ? "
            "Le cas échéant, vous devez aussi aller annuler l’intervention correspondante. "
            "La remarque sera ajoutée dans le journal."
        )
        return context

    def form_valid(self, form):
        install = get_object_or_404(models.Installation, pk=self.kwargs['pk'])
        with transaction.atomic():
            install.date_fin_abo = None
            install.motif_fin = ''
            install.save()
            Journal.objects.create(
                persona=install.client.persona,
                description=f"La résiliation a été annulée: {form.cleaned_data['comment']}",
                quand=timezone.now(), qui=self.request.user
            )
            prest = install.client.prestations.filter(service=Services.ALARME).latest('duree')
            if prest and prest.duree.upper is not None:
                prest.duree = (prest.duree.lower, None)
                prest.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class MaterielClientEditView(ClientJournalMixin, CreateUpdateView):
    model = models.MaterielClient
    form_class = forms.MaterielClientForm
    template_name = "alarme/client_materiel.html"
    journal_add_message = "Installation d’un nouveau matériel ({obj})"
    journal_edit_message = "Modification du matériel installé ({fields})"
    json_response = True

    def get_client(self):
        if self.object:
            client = self.object.client
        else:
            client = get_object_or_404(Client, pk=self.kwargs['client_pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_initial(self):
        initial = super().get_initial()
        if 'client' in self.request.GET:
            initial['client'] = Client.objects.get(pk=self.request.GET['client'])
        if 'materiel' in self.request.GET:
            initial['materiel'] = models.Materiel.objects.get(pk=self.request.GET['materiel'])
        return initial

    def form_valid(self, form):
        if not form.instance.client_id:
            form.instance.client = self.get_client()
        with transaction.atomic():
            is_new = not form.instance.pk
            super().form_valid(form)
            matclient = self.object
            if matclient.materiel and matclient.materiel.chez_personne:
                # Sortir le matériel du stock personnel
                matclient.materiel.chez_personne = None
                matclient.materiel.save()
            type_mat = matclient.type_mat or matclient.materiel.type_mat
            if is_new and type_mat.article_achat:
                article = type_mat.article_achat
                models.Facture.objects.create(
                    client=form.instance.client,
                    mois_facture=matclient.date_debut, date_facture=date.today(),
                    article=article,
                    install=None, materiel=matclient,
                    libelle=f"{article.designation} (installé le {matclient.date_debut.strftime('%d.%m.%Y')})",
                    montant=article.prix, exporte=None
                )
                messages.info(self.request, "Une facture a été créée pour l’achat de ce matériel par le client.")
            if not is_new and form.cleaned_data.get('date_fin_abo') and type_mat.article_achat:
                # Archiver
                matclient.materiel.date_archive = form.cleaned_data['date_fin_abo']
                matclient.materiel.save()
                messages.success(self.request, "Le matériel a été archivé.")
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class MissionEditView(ClientJournalMixin, CreateUpdateView):
    model = models.Mission
    template_name = 'alarme/mission_edit.html'
    par_benevole = False
    journal_add_message = "Nouvelle intervention: {obj}"
    journal_edit_message = "Modification d’une intervention ({fields})"
    json_response = True

    @cached_property
    def _client(self):
        client = self.object.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if self.object and not self.object.can_edit(self.request.user):
            raise PermissionDenied(
                "Vous n’avez pas les permissions nécessaires pour modifier cette intervention."
            )
        return client

    def get_client(self):
        return self._client

    def get_form_class(self):
        return forms.BenevoleMissionForm if self.par_benevole else forms.MissionForm

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'client': self.get_client()}

    def get_initial(self):
        initial = super().get_initial()
        install = self._client.alarme_actuelle() if self._client else None
        if self.is_create and install and install.abonnement:
            initial['abonnement'] = install.abonnement
        if not self.is_create and self.object.alarme:
            initial['alarme_select'] = str(self.object.alarme)
        return initial

    def journalize(self, form, **kwargs):
        obj = form.instance
        if obj.type_mission.code == 'CHANGE' and obj.effectuee and 'effectuee' in form.changed_data:
            old_inst = obj.client.alarme_actuelle()
            if old_inst:
                message = (
                    f"Changement d’appareil, ancien: {old_inst.alarme}, "
                    f"nouveau: {form.cleaned_data['alarme']}"
                )
            else:
                message = f"Nouvel appareil: {form.cleaned_data['alarme']}"
            if form.cleaned_data.get('abonnement') and form.cleaned_data['abonnement'] != old_inst.abonnement:
                message += (
                    f"\nChangement d’abonnement, ancien: {old_inst.abonnement}, "
                    f"nouveau: {form.cleaned_data['abonnement']}"
                )
            dt = obj.effectuee
            Journal.objects.create(
                persona=self.get_client().persona, description=message,
                quand=timezone.make_aware(datetime(dt.year, dt.month, dt.day, 12, 0)),
                qui=self.request.user
            )
        else:
            return super().journalize(form, **kwargs)

    def form_valid(self, form):
        if self.is_create:
            form.instance.client = form.client
        try:
            with transaction.atomic():
                self.save_form(form)
        except IntegrityError:
            return JsonResponse({
                'result': 'Error',
                'message': "Il existe déjà une installation pour cette personne.",
            })
        return JsonResponse({'result': 'OK', 'reload': 'page'})

    def save_form(self, form):
        client = form.instance.client
        if (
            form.cleaned_data.get('type_mission') and form.cleaned_data.get('type_mission').code == 'NEW'
            and settings.DUREE_SANS_CLIENT_NEW_DEFAULT
        ):
            form.instance.duree_seul = settings.DUREE_SANS_CLIENT_NEW_DEFAULT
        super().form_valid(form)
        new_or_change = self.object.type_mission.code in {'NEW', 'CHANGE'}
        # Temporairement désactivé
        if False and new_or_change and self.object.planifiee and 'planifiee' in form.changed_data and self.par_benevole:
            # Envoi à la centrale des informations d'installation planifiée
            msg_body = default_centrale_message(
                client, alarme=self.object.alarme, mission=self.object,
            )['body']
            msg_subject = f"[Croix-Rouge {canton_abrev()}] Réservation d'installation"
            transaction.on_commit(partial(
                envoi_centrale, self.request, msg_subject, msg_body
            ))
            Journal.objects.create(
                persona=client.persona, description="Message d'intervention planifiée envoyé à la centrale",
                details=(
                    f"<b>Destinataire:</b> {settings.CENTRALE_EMAIL}\n"
                    f"<b>Sujet:</b> {msg_subject}\n"
                    f"<b>Message:</b>\n{msg_body}"
                ),
                quand=timezone.now(),
                qui=self.request.user
            )
            messages.success(
                self.request,
                "Un message avec les informations d’installation (client/appareil) "
                "a été envoyé à la centrale d’alarme."
            )

        if client and self.object.effectuee and 'effectuee' in form.changed_data and self.par_benevole:
            # Ajout d'une alerte pour que les gestionnaires de l'alarme soient informés de la fin d'intervention.
            text = f"Intervention effectuée: {str(self.object)}"
            Alerte.objects.create(
                persona=client.persona, cible=Services.ALARME, alerte=text,
                modele_lie=self.object, recu_le=timezone.now(),
            )

        nouvel_app = form.cleaned_data.get('alarme')

        def retour_appareil(appareil):
            if appareil.modele.article_achat is not None:
                # Archiver l'appareil à l'achat
                appareil.date_archive = self.object.effectuee
                appareil.save()
            elif self.object.intervenant:
                # Ajouter dans le stock de l'intervenant
                appareil.chez_personne = self.object.intervenant
                appareil.save()

        if nouvel_app and self.object.effectuee:
            if self.object.type_mission.code == 'NEW':
                # Nouvelle installation effectuée
                abo = form.cleaned_data.get('abonnement') or self.object.abonnement
                assert abo is not None
                # Cette création peut générer une exception IntegrityError si le client
                # a déjà une installation en cours (ce qui ne devrait pas arriver).
                models.Installation.objects.create(
                    client=self.object.client, alarme=nouvel_app,
                    abonnement=abo, nouvelle=True, date_debut=self.object.effectuee
                )
                if self.object.client.samaritains:
                    self.object.client.samaritains_des = (self.object.effectuee, None)
                    self.object.client.save(update_fields=["samaritains_des"])
                Journal.objects.create(
                    persona=client.persona,
                    description=f"Installation d’un nouvel appareil ({nouvel_app}, {abo})",
                    quand=timezone.now(), qui=self.request.user
                )
                if nouvel_app.chez_personne:
                    nouvel_app.chez_personne = None
                    nouvel_app.save()
            elif self.object.type_mission.code == 'CHANGE':
                # Changement d'installation effectue
                current_install = self.object.client.alarme_actuelle()
                if not current_install.date_fin_abo:
                    current_install.date_fin_abo = self.object.effectuee - timedelta(days=1)
                    current_install.retour_mat = self.object.effectuee
                    current_install.save(is_change=True)
                retour_appareil(current_install.alarme)
                # Create new install
                new_install = models.Installation.objects.create(
                    client=self.object.client, date_debut=self.object.effectuee,
                    alarme=nouvel_app, nouvelle=False,
                    abonnement=form.cleaned_data.get('abonnement') or self.object.abonnement,
                    remarques=current_install.remarques,
                )
                if nouvel_app.modele.article_achat:
                    # Facturer article à l'achat
                    article = nouvel_app.modele.article_achat
                    models.Facture.objects.create(
                        client=self.object.client,
                        mois_facture=date.today().replace(day=1),
                        article=article,
                        install=new_install, materiel=None,
                        libelle=(
                            f"{article.designation} (installé le {new_install.date_debut.strftime('%d.%m.%Y')})"
                        ),
                        montant=article.prix, exporte=None
                    )
                if nouvel_app.chez_personne:
                    nouvel_app.chez_personne = None
                    nouvel_app.save()
        elif self.object.effectuee and self.object.type_mission.code == 'UNINSTALL':
            # Désinstallation effectuée par le bénévole, le matériel revient dans son stock
            current_install = self.object.client.derniere_alarme()
            current_install.retour_mat = self.object.effectuee
            current_install.save()
            retour_appareil(current_install.alarme)
            check_archive_client(self.object.client)


class IntervenantsListView(ExportableMixin, FilterFormMixin, PermissionRequiredMixin, ListView):
    permission_required = 'benevole.view_benevole'
    filter_formclass = forms.IntervenantFilterForm
    template_name = 'alarme/intervenant_list.html'
    paginate_by = 30

    def get_queryset(self):
        base_qs = models.utilisateurs_alarme(temp_inactifs=True).order_by('last_name', 'first_name')
        base_qs = base_qs.annotate(
            adresse_active=PersonaBaseQuerySet.adresse_active_subquery(
                date.today(), outer_field_name="benevole__persona__id"
            )
        )
        return super().get_queryset(base_qs=base_qs)

    def render_to_response(self, *args, **kwargs):
        if self.export_flag:
            self.activites = TypeActivite.par_domaine('alarme')
            self.col_widths = [18, 18, 25, 25, 30, 16] + [15] * len(self.activites) + [60]
        return super().render_to_response(*args, **kwargs)

    def export_lines(self, context):
        export_cols = ['Nom', 'Prénom', 'Rue', 'Localité', 'Courriel', 'Téls', 'Date de naissance']
        yield ExpLine(
            export_cols + [act.nom for act in self.activites] + ['Remarques'],
            bold=True
        )
        for interv in self.get_queryset():
            if interv.is_benevole:
                benev = interv.benevole
                adr = benev.adresse()
                activites = {act.type_act: act for act in benev.activites_en_cours()}
                yield [
                    benev.nom, benev.prenom, adr.rue if adr else "",
                    f"{adr.npa} {adr.localite}" if adr else "", benev.courriel
                ] + [
                    "\n".join([tel.tel for tel in benev.telephones()]),
                    benev.date_naissance,
                ] + [
                    (f"x ({activites[type_act].duree.lower.strftime('%d.%m.%Y')}-)"
                     if type_act in activites else '')
                    for type_act in self.activites
                ] + [benev.remarques]
            else:
                yield [interv.last_name, interv.first_name, '', 'Interne', interv.email, interv.tel, '']


class IntervenantDetailView(UtilisateurUpdateView):
    template_name = 'alarme/intervenant_edit.html'
    permission_required = 'benevole.view_benevole'
    context_object_name = "intervenant"
    success_url = reverse_lazy('intervenants')

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'readonly': not self.request.user.has_perm('common.change_utilisateur'),
        }


class IntervenantStockView(TemplateView):
    """Utilisée à la fois pour la page de stock bénévole et le stock utilisateur."""
    template_name = 'alarme/intervenant_stock.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        if user.is_benevole:
            context.update({
                'tabs_template': 'benevole/benevole_tabs.html',
                'benevole': user.benevole,
            })
        else:
            context['tabs_template'] = 'alarme/intervenant_tabs.html'
        return {
            **context,
            'intervenant': user,
            'appareils': user.alarme_set.all().order_by('modele', 'no_serie') if user else [],
            'materiels': user.materiel_set.all() if user else [],
            'planned_installs': {
                miss.alarme_id: miss for miss in models.Mission.objects.filter(
                    effectuee__isnull=True, alarme__isnull=False
                )
            },
        }


class BenevoleVisiteNew(CreateView):
    """Création d'une nouvelle visite planifiée par un bénévole."""
    model = models.Mission
    form_class = forms.VisiteCreateForm
    template_name = 'alarme/mission_edit.html'

    def form_valid(self, form):
        form.instance.type_mission = models.TypeMission.objects.filter(code='VISITE').first()
        form.instance.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        form.instance.intervenant = self.request.user
        form.instance.delai = form.cleaned_data.get('planifiee')
        visite = form.save()
        if visite.effectuee:
            text = f"Intervention effectuée: {visite}"
            Alerte.objects.create(
                persona=visite.client.persona, cible=Services.ALARME, alerte=text,
                modele_lie=visite, recu_le=timezone.now(),
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class IntervenantVisitesView(TemplateView):
    template_name = 'alarme/intervenant_visites.html'
    for_benev = False

    def get_context_data(self, **kwargs):
        interv = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'intervenant': interv,
            'benevole': interv.benevole if interv.is_benevole else None,
            'tabs_template': (
                'benevole/benevole_tabs.html' if interv.is_benevole else 'alarme/intervenant_tabs.html'
            ),
            'a_visiter': clients_a_visiter(interv).order_by('persona__nom', 'persona__prenom'),
        }


class MissionDeleteView(DeleteView):
    model = models.Mission

    def form_valid(self, form):
        if not self.request.user.has_perm('alarme.delete_mission') or self.object.effectuee:
            raise PermissionDenied("Vous n’avez pas la permission d’annuler cette intervention")
        avec_client = self.object.client is not None
        self.object.delete()
        if avec_client:
            Journal.objects.create(
                persona=self.object.client.persona,
                description=f"Annulation de l’intervention «{self.object}»",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class IntervenantMissionsView(ListView):
    """Liste des missions attribuées à un bénévole."""
    model = models.Mission
    template_name = 'alarme/intervenant_missions.html'
    paginate_by = 25

    def dispatch(self, request, *args, **kwargs):
        self.intervenant = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(
            intervenant=self.intervenant
        ).prefetch_related('frais').order_by('-effectuee', '-delai')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'intervenant': self.intervenant,
            'benevole': self.intervenant.benevole if self.intervenant.is_benevole else None,
            'tabs_template': (
                'benevole/benevole_tabs.html' if self.intervenant.is_benevole else 'alarme/intervenant_tabs.html'
            ),
        }


class IntervenantMissionEditView(MissionEditView):
    """Création/Édition d'une mission non liée à un client."""
    def get_client(self):
        return None

    def get_form_class(self):
        return forms.MissionSansClientForm

    def get_initial(self):
        return super(MissionEditView, self).get_initial()

    def form_valid(self, form):
        if self.is_create:
            if self.par_benevole:
                form.instance.intervenant = self.request.user
            else:
                form.instance.intervenant = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        form.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleAttributionView(View):
    """Le bénévole s'attribue une intervention non encore attribuée."""
    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            intervention = models.Mission.objects.select_for_update().get(pk=kwargs['interv_pk'])
            if intervention.intervenant is not None:
                if intervention.intervenant == request.user:
                    messages.error(request, "Cette intervention vous a déjà été attribuée.")
                else:
                    messages.error(
                        request,
                        "Désolé, cette intervention vient d’être attribuée à une autre personne."
                    )
            else:
                intervention.intervenant = request.user
                intervention.save()
                messages.success(
                    request,
                    f"L’intervention chez {intervention.client.nom_prenom} vous a bien été attribuée, merci !"
                )
        return HttpResponseRedirect(reverse('home'))


class BenevoleArchivesView(AlarmeAccessMixin, TemplateView):
    """Liste des mois d'archives"""
    template_name = 'benevoles/archives-mois.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='alarme/app/base.html')
        context['months'] = self.request.user.mission_set.filter(effectuee__isnull=False).annotate(
            month=TruncMonth('effectuee')
        ).values_list('month', flat=True).distinct().order_by('-month')
        return context


class BenevoleArchivesMonthView(AlarmeAccessMixin, TemplateView):
    """Détail des archives pour un mois donné."""
    template_name = 'alarme/app/archives-mois-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = date(self.kwargs['year'], self.kwargs['month'], 1)
        end = (start + timedelta(days=32)).replace(day=1) - timedelta(days=1)
        missions = self.request.user.mission_set.filter(
            effectuee__range=[start, end],
        ).annotate(
            adresse_client=PersonaBaseQuerySet.adresse_active_subquery(
                date.today(), outer_field_name='client__persona_id'
            )
        ).order_by('-effectuee', 'pk')
        context['days'] = {
            dt: list(miss) for dt, miss in groupby(missions, key=attrgetter('effectuee'))
        }
        context['sums'] = missions.aggregate(
            total_missions=Count('pk'), total_km=Sum('km'), total_frais=Sum('frais__cout'),
            total_duree_client=Sum('duree_client'), total_duree_seul=Sum('duree_seul'),
        )
        return context


class MaterielIndexView(AlarmeAccessMixin, TemplateView):
    template_name = 'alarme/materiel_index.html'

    def get_context_data(self, **kwargs):
        alarmes_installees = models.Alarme.objects.installees()
        alarmes_non_installees = models.Alarme.objects.non_installees()
        alarmes_a_reviser = models.Alarme.objects.a_reviser()
        alarmes_en_reparation = models.Alarme.objects.non_installees().filter(en_rep_depuis__isnull=False)
        materiels_installes = models.Materiel.objects.installes()
        materiels_non_installes = models.Materiel.objects.non_installes()
        context = {
            **super().get_context_data(**kwargs),
            'types_app': models.ModeleAlarme.objects.all().annotate(
                stock=Count('alarme', filter=
                    Q(alarme__id__in=alarmes_non_installees.values('id')) &
                    ~Q(alarme__id__in=alarmes_en_reparation.values('id'))
                ),
                installes=Count('alarme', filter=Q(alarme__id__in=alarmes_installees.values('id'))),
                revision=Count('alarme', filter=Q(alarme__id__in=alarmes_a_reviser.values('id'))),
                reparation=Count('alarme', filter=Q(alarme__id__in=alarmes_en_reparation.values('id'))),
            ).order_by('nom'),
            'types_mat': models.TypeMateriel.objects.all().annotate(
                stock=Count('materiel', filter=Q(materiel__id__in=materiels_non_installes.values('id'))),
                installes=Count('materiel', filter=Q(materiel__id__in=materiels_installes.values('id')))
            ).order_by('nom'),
        }
        context['totaux_app'] = {
            'stock': sum([l.stock for l in context['types_app']]),
            'reparation': sum([l.reparation for l in context['types_app']]),
            'revision': sum([l.revision for l in context['types_app']]),
            'installes': sum([l.installes for l in context['types_app']]),
            'total': sum([l.stock + l.installes for l in context['types_app']]),
        }
        return context


class AlarmeListView(AlarmeAccessMixin, FilterFormMixin, ExportableMixin, ListView):
    model = models.Alarme
    filter_formclass = forms.AlarmeFilterForm
    paginate_by = 25
    col_widths = [20] * 8 + [40] * 2

    def get_queryset(self):
        self.is_archived = (
            self.filter_form.is_bound and self.filter_form.is_valid() and self.filter_form.cleaned_data['archived']
        )
        if self.is_archived:
            alarmes = super().get_queryset().filter(date_archive__isnull=False)
            order_by = ['-date_archive']
        else:
            alarmes = super().get_queryset().filter(date_archive=None)
            order_by = ['modele', 'no_serie']
        return alarmes.prefetch_related(
            Prefetch(
                'installation_set', to_attr='installations',
                queryset=models.Installation.objects.all().select_related(
                    'client', 'alarme', 'alarme__modele', 'abonnement'
                )
            )
        ).select_related('chez_personne').order_by(*order_by)

    def export_lines(self, context):
        fields = [
            self.model._meta.get_field(fname) for fname in [
                "modele", "no_appareil", "no_serie", "carte_sim", "date_achat",
                "date_batterie", "date_revision", "en_rep_depuis", "remarques",
            ]
        ]
        yield ExpLine([f.verbose_name for f in fields] + ['Installation/stock'], bold=True)
        for appareil in self.get_queryset():
            install = appareil.install_actuelle()
            if install:
                stock_str = str(install.client)
            elif appareil.date_archive:
                stock_str = "Archivé"
            elif appareil.en_rep_depuis:
                stock_str = f"En réparation depuis le {django_format(appareil.en_rep_depuis, 'd.m.Y')}"
            else:
                stock_str = 'En stock'
                if appareil.chez_personne_id:
                    stock_str += f", chez {appareil.chez_personne}"
                if appareil.a_reviser:
                    stock_str += ", à réviser"
            yield [getattr(appareil, f.name) for f in fields] + [stock_str]

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'planned_installs': {
                miss.alarme_id: miss for miss in models.Mission.objects.filter(
                    effectuee__isnull=True, alarme__isnull=False
                )
            },
        }


class AlarmeEditView(PermissionRequiredMixin, RedirectURLMixin, JournalMixin, CreateUpdateView):
    model = models.Alarme
    form_class = forms.AlarmeForm
    template_name = 'alarme/alarme.html'
    permission_required = 'alarme.change_alarme'
    next_page = reverse_lazy('alarmes')
    journal_add_message = "Création de l’appareil"
    journal_edit_message = "Modification de l’appareil: {fields}"

    def get_object(self):
        obj = super().get_object()
        self.cur_install = obj.install_actuelle() if obj else None
        return obj

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'cur_install': self.cur_install}

    def _create_instance(self, **kwargs):
        models.JournalAlarme.objects.create(
            alarme=self.object, **kwargs
        )

    def get_success_message(self, obj):
        if self.is_create:
            return f"La création du nouvel appareil {obj} a réussi."
        else:
            return f"L’appareil «{obj}» a bien été modifié"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'install': self.cur_install,
            'retour_manquant': self.object.installation_set.filter(
                date_fin_abo__isnull=False, retour_mat__isnull=True
            ) if self.object else [],
            'next': self.get_redirect_url(),
        }


class AlarmeArchiveView(PermissionRequiredMixin, View):
    model = models.Alarme
    permission_required = 'alarme.change_alarme'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(self.model, pk=kwargs['pk'])
        if obj.date_archive:
            obj.date_archive = None
            with transaction.atomic():
                obj.save()
                models.JournalAlarme.objects.create(
                    alarme=obj, description="Désarchivage de l’appareil",
                    quand=timezone.now(), qui=request.user
                )
            messages.success(request, "L’appareil a bien été désarchivé.")
        else:
            obj.date_archive = timezone.now()
            obj.chez_personne = None
            with transaction.atomic():
                obj.save()
                models.JournalAlarme.objects.create(
                    alarme=obj, description="Archivage de l’appareil",
                    quand=timezone.now(), qui=request.user
                )
            messages.success(request, "L’appareil a bien été archivé.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class AlarmeJournalView(ListView):
    model = models.JournalAlarme
    paginate_by = 20
    template_name = 'alarme/alarme_journal.html'

    def get(self, *args, **kwargs):
        self.alarme = get_object_or_404(models.Alarme, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        journaux = list(
            (j.quand.date(), j.qui, j.description) for j in self.alarme.journaux.order_by('-quand')
        )
        for install in self.alarme.installation_set.order_by('-date_debut').select_related('client'):
            journaux.append((install.date_debut, '', f'Installation chez {install.client}'))
            if install.retour_mat:
                journaux.append((install.retour_mat, '', f'Retour du matériel de chez {install.client}'))
        return list(reversed(sorted(journaux, key=itemgetter(0))))

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'alarme': self.alarme,
        }


class ModeleAlarmeEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.ModeleAlarme
    permission_required = 'alarme.change_modelealarme'
    form_class = forms.ModeleAlarmeForm
    json_response = True

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'OK', 'reload': '#id_modele'})


class MaterielListView(AlarmeAccessMixin, FilterFormMixin, ExportableMixin, ListView):
    model = models.Materiel
    filter_formclass = forms.MaterielFilterForm
    paginate_by = 25
    col_widths = [35, 25, 15, 10, 15, 15, 40]

    def get_queryset(self):
        self.is_archived = (
            self.filter_form.is_bound and self.filter_form.is_valid() and self.filter_form.cleaned_data['archived']
        )
        if self.is_archived:
            materiel = super().get_queryset().filter(date_archive__isnull=False)
            order_by = '-date_archive'
        else:
            materiel = super().get_queryset().filter(date_archive=None)
            order_by = 'type_mat'
        return materiel.prefetch_related(
            Prefetch('materielclient_set', to_attr='installations',
                     queryset=models.MaterielClient.objects.filter(
                        Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
                     ).select_related('client'))
        ).order_by(order_by)

    def export_lines(self, context):
        fields = [
            self.model._meta.get_field(fname) for fname in [
                'type_mat', 'no_ref', 'date_achat', 'prix_achat', 'fournisseur',
                'date_archive'
            ]
        ]
        yield ExpLine([f.verbose_name for f in fields] + ['Installation'], bold=True)
        for materiel in self.get_queryset():
            install_str = ''
            client = materiel.installations[0].client if len(materiel.installations) else None
            if client:
                install_str = str(client)
            elif materiel.date_archive:
                install_str = 'Archivé'
            else:
                install_str = 'En stock'
                if materiel.chez_personne:
                    install_str += f", chez {materiel.chez_personne}"
            yield [getattr(materiel, f.name) for f in fields] + [install_str]


class MaterielEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.Materiel
    form_class = forms.MaterielForm
    template_name = 'alarme/materiel.html'
    permission_required = 'alarme.change_materiel'
    success_url = reverse_lazy('materiel')

    def get_success_message(self, obj):
        if self.is_create:
            return f"La création du nouveau materiel {obj} a réussi."
        else:
            return f"Le matériel «{obj}» a bien été modifié."

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'install': self.object.materielclient_set.filter(
                Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
            ).first() if self.object else None,
        }


class MaterielArchiveView(PermissionRequiredMixin, View):
    model = models.Materiel
    permission_required = 'alarme.change_materiel'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(self.model, pk=kwargs['pk'])
        if obj.date_archive:
            obj.date_archive = None
            obj.save()
            messages.success(request, "Le matériel a bien été désarchivé.")
        else:
            obj.date_archive = timezone.now()
            obj.save()
            messages.success(request, "Le matériel a bien été archivé.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', ''))


class TypeMaterielEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.TypeMateriel
    permission_required = 'alarme.change_typemateriel'
    form_class = forms.TypeMaterielForm
    json_response = True

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'OK', 'reload': '#id_type_mat'})


class AlarmeSearchView(View):
    """
    Endpoint for autocomplete search for InstallationEditView.
    Therefore only search unlinked alarmes.
    """
    MAX_RESULTS = 15
    from_benev_stock = False

    def get_queryset(self, term):
        if self.from_benev_stock:
            base_qs = self.request.user.alarme_set.non_reservees()
        else:
            base_qs = models.Alarme.objects.non_installees().non_reservees().filter(
                en_rep_depuis__isnull=True
            ).exclude(
                Q(id__in=models.Alarme.objects.a_reviser().values('id'))
            )
        return base_qs.filter(
            Q(modele__nom__icontains=term) | Q(no_serie__icontains=term) | Q(carte_sim__icontains=term)
        )

    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        if term is None:
            return HttpResponseBadRequest("Un terme de recherche est obligatoire")
        results = [
            {'label': str(mat), 'value': mat.pk}
            for mat in self.get_queryset(term)[:self.MAX_RESULTS]
        ]
        return JsonResponse(results, safe=False)


class AlarmeJSONView(View):
    """
    Quand une alarme est sélectionnée dans un menu (par ex. MissionForm), cette
    vue permet de renvoyer en format JSON certains détails au sujet de l'appareil.
    """

    def get(self, *args, **kwargs):
        alarme = get_object_or_404(models.Alarme, pk=self.request.GET.get('pk'))
        content = serialize("json", [alarme])
        return HttpResponse(content, content_type="application/json")


class MaterielSearchView(View):
    """
    Endpoint for autocomplete search for MaterielClientEditView.
    Therefore only search unlinked materiel.
    """

    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        query = models.Materiel.objects.non_installes().filter(
            Q(type_mat__nom__icontains=term) | Q(no_ref__icontains=term)
        )
        results = [
            {'label': str(mat), 'value': mat.pk}
            for mat in query[:10]
        ]
        return JsonResponse(results, safe=False)


def clients_a_visiter(user):
    """Renvoie une liste des clients alarme à visiter par cette personne."""
    return user.visites.filter(
        archive_le__isnull=True, persona__date_deces__isnull=True
    ).avec_adresse(date.today()).annotate(
        derniere=Max('mission__effectuee'),
        num_inst=Count('installation', filter=(
            Q(installation__date_fin_abo__isnull=True) |
            Q(installation__date_fin_abo__gt=date.today())
        )),
    ).exclude(num_inst=0).prefetch_related(
        Prefetch('mission_set', to_attr='interv_en_cours', queryset=models.Mission.objects.filter(
            effectuee__isnull=True
        ).select_related('type_mission')),
    )


def get_client_tabs(view, client, tabs):
    if view.request.user.has_perm('alarme.change_mission'):
        tabs.insert(-1, {
            'id': 'interventions', 'title': "Interventions",
            'url': reverse('client-interventions', args=[client.pk])
        })
    if view.request.user.has_perm('alarme.view_facture') and client.factures.exists():
        tabs.insert(-1, {
            'id': 'factures', 'title': "Factures",
            'url': reverse('client-factures', args=[client.pk])
        })
    return tabs


ClientDetailContextMixin.app_tabs_methods['alarme'] = get_client_tabs


def default_centrale_message(client, alarme, mission, extra_context=None):
    template = loader.get_template('alarme/envoi_centrale.txt')
    canton = canton_abrev()
    medecins = client.profclient_set.exclude(professionnel__type_pro='Sama').select_related('professionnel')
    body_context = {
        'canton': canton,
        'client': client,
        'persona': client.persona,
        'adresse': client.adresse(),
        'alarme': alarme,
        'mission': mission,
        'medecins': [", ".join([item for item in [
            med.professionnel.nom, med.professionnel.prenom, med.professionnel.rue,
            " ".join([med.professionnel.npa, med.professionnel.localite]), med.professionnel.tel_1, med.professionnel.tel_2,
            med.professionnel.remarque, f"({med.remarque})" if med.remarque else '',
        ] if item]) for med in medecins],
        'contacts': client.contacts_as_context(),
        'liste_samas': client.profclient_set.filter(
                professionnel__type_pro='Sama'
            ).select_related('professionnel').order_by('priorite') if client.samaritains else [],
        'historique': client.journaux.filter(
            quand__gte=timezone.now() - timedelta(days=30)
        ).order_by('-quand'),
    }
    body_context.update(extra_context or {})
    return {
        'subject': f"[Croix-Rouge {canton}] Mise à jour client {client.nom_prenom}",
        'body': template.render(body_context),
    }


def envoi_centrale(request, subject, body):
    from_email = request.user.email
    if not same_domain(from_email, settings.DEFAULT_FROM_EMAIL):
        # The server cannot send mail on behalf of any mail domain.
        from_email = settings.DEFAULT_FROM_EMAIL
        body = (
            f"[Message envoyé par {request.user.last_name} {request.user.first_name} <{request.user.email}>, "
            f"depuis https://{settings.ALLOWED_HOSTS[0]}]\n\n"
        ) + body
    if not settings.TEST_INSTANCE:
        send_mail(
            subject, body, from_email, [settings.CENTRALE_EMAIL]
        )


def same_domain(email1, email2):
    return email1.rsplit('@', 1)[-1] == email2.rsplit('@', 1)[-1]


def check_archive_client(client):
    """
    Vérifier si le client doit être archivé pour l'alarme.
    Appelé normalement depuis le form. d'édition d'install (édition de date de
    retour de matériel) ou depuis le formulaire de résiliation (si matériel acheté).
    """
    active_installs = client.installation_set.filter(date_fin_abo__isnull=True)
    dates_retour_mat = list(client.installation_set.values_list('retour_mat', flat=True))
    if active_installs.exists() or None in dates_retour_mat:
        return
    try:
        prest_al = client.prestations_actuelles(limit_to=Services.ALARME)[0]
    except IndexError:
        pass
    else:
        client.archiver(None, Services.ALARME, check=False)
