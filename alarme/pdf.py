from datetime import date
from functools import partial

from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateformat import format as django_format
from django.utils.text import slugify

from reportlab.lib.colors import black
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    KeepTogether, Image, ListFlowable, ListItem, PageBreak, Paragraph,
    Spacer, Table, TableStyle
)

from common.pdf import BaseCroixrougePDF, PageNumCanvas, VerticalText
from common.utils import canton_abrev

canton_strings = {
    'JU': {
        'nom': "Croix-Rouge suisse Canton du Jura",
        'lieu': "Porrentruy",
        'sig': "Votre Croix-Rouge jurassienne",
        'tel': "032 465 84 01",
        'tel_contact': "",
        'courriel': "alarme@croix-rouge-jura.ch",
        'horaire': "du lundi au jeudi de 8h00 à 11h30",
        'filename_template': '{nom}_{prenom}_{npa}_P_N.pdf'
    },
    'NE': {
        'nom': "Croix-Rouge neuchâteloise",
        'lieu': "Neuchâtel",
        'sig': "Votre Croix-Rouge neuchâteloise",
        'tel': "032 886 88 64",
        'tel_contact': " ou par téléphone au <b>058 105 04 18</b>",
        'courriel': "alarme@croix-rouge-ne.ch",
        'horaire': "du lundi au vendredi de 8h30 à 11h30 et de 14h à 16h30",
        'filename_template': 'questionnaire_alarme_{nom}_{prenom}_{today}.pdf'
    },
}


class PrintContactMixin:
    def print_contact_table(self, contact, idx, left_label=None, relation=True):
        P = partial(Paragraph, style=self.style_normal)
        col1 = [f"{idx})", VerticalText(left_label), "", ""] if left_label else ['', '', '', '']
        tels = [tel.tel + (f"({tel.remarque})" if tel.remarque else "") for tel in contact.referenttel_set.all()]
        table_data = [[
            col1[0], "Nom", contact.nom, "Prénom", contact.prenom,
        ], [
            col1[1], "Adresse", P(contact.rue), "Localité", f"{contact.npa} {contact.localite}"
        ], [
            col1[2], "Téléphones", P("<br/>".join(tels)), "Courriel", P(contact.courriel)
        ]]
        if relation:
            table_data.append([
                col1[3], f"Relation / lien de parenté : {contact.relation}"
            ])
        if left_label:
            col_widths = [1 * cm, 2.5 * cm, 5.7 * cm, 2.5 * cm, 5.7 * cm]
        else:
            col_widths = [0 * cm, 2.5 * cm, 6.2 * cm, 2.5 * cm, 6.2 * cm]
        brd_width = 0.1
        table = Table(
            data=table_data,
            style=TableStyle([
                ('VALIGN', (1, 0), (4, 3), 'TOP'),
                ('SPAN', (0, 1), (0, len(table_data) - 1)),
                ('BOX', (0, 0), (0, 0), brd_width, black),
                ('BOX', (1, 0), (2, 0), brd_width, black),
                ('BOX', (3, 0), (4, 0), brd_width, black),
                ('BOX', (0, 1), (0, 3), brd_width, black),
                ('BOX', (1, 1), (2, 1), brd_width, black),
                ('BOX', (3, 1), (4, 1), brd_width, black),
                ('BOX', (1, 2), (2, 2), brd_width, black),
                ('BOX', (3, 2), (4, 2), brd_width, black),
                ('BOX', (1, 3), (4, 3), brd_width, black),
            ]),
            colWidths=col_widths,
        )
        self.story.append(table)


class QuestionnairePDF(PrintContactMixin, BaseCroixrougePDF):
    title = 'Questionnaire Alarme Croix-Rouge'
    version = '2.11'
    top_margin = 3 * cm
    FONTSIZE = 10

    def define_styles(self):
        super().define_styles()
        self.bold11= ParagraphStyle(
            name='Bold 11', fontName='Helvetica-Bold', fontSize=11, leading=13
        )
        # Pour la case à cocher
        registerFont(TTFont('DejaVuSans', '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'))

    def draw_header_footer(self, canvas, doc):
        super().draw_header_footer(canvas, doc)
        canvas.saveState()
        canvas.setFont("Helvetica-Bold", 12)
        if self.is_ape:
            canvas.drawString(doc.leftMargin, A4[1] - 2.3 * cm, "Questionnaire Alarme Croix-Rouge")
            canvas.drawString(doc.leftMargin, A4[1] - 2.8 * cm, "Appartement avec encadrement")
        else:
            canvas.drawString(doc.leftMargin, A4[1] - 2.6 * cm, "Questionnaire Alarme Croix-Rouge")
        canvas.setFont("Helvetica", 9)
        canvas.drawString(
            doc.leftMargin, 2.8 * cm,
            "Questionnaire ApE V2" if self.is_ape else f"Questionnaire v{self.version}"
        )
        canvas.restoreState()

    def get_filename(self, client):
        return canton_strings.get(canton_abrev())['filename_template'].format(
            nom=slugify(client.nom), prenom=slugify(client.prenom), npa=client.adresse().npa,
            today=date.today().strftime('%Y_%m_%d'),
        )

    def produce(
        self, client, install_mission=None, appareil=None, abo=None,
        pour_signature=False, signature_file=None, signature_lieu=None
    ):
        """
        Si pour_signature: la dernière page contenant la signature est omise, car elle sera affichée
            dans le HTML avec le composant de signature.
        Si signature_file/signature_lieu: les champs de signature sont complétés avec ce contenu.
        """
        install = client.alarme_actuelle()
        P = partial(Paragraph, style=self.style_normal)
        strings = canton_strings.get(canton_abrev())
        self.is_ape = client.type_logement == 'ape'
        partenaire = client.partenaire

        self.story.append(Spacer(1, 3 * cm))
        self.story.append(P("Locataire ApE Alarme Croix-Rouge") if self.is_ape else P("Client·e Alarme Croix-Rouge"))
        date_mission = (install_mission.effectuee or install_mission.planifiee) if install_mission else None
        adresse = client.adresse()
        persona = client.persona
        intit_client = "locataire" if self.is_ape else "client·e"
        style_data = [
            # whole table:
            ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ('LEFTPADDING', (0, 0), (-1, -1), 10),
            ('RIGHTPADDING', (0, 0), (-1, -1), 10),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ('TOPPADDING', (0, 0), (-1, -1), 8),
            ('BACKGROUND', (0, 0), (-1, -1), self.light_gray),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
            ('SPAN', (0, 0),(-1, 0)),
            # 3 lignes adr.
            ('BOX', (0, 1), (1, 1), 0.25, black),
            ('BOX', (2, 1), (3, 1), 0.25, black),
            ('BOX', (0, 2), (1, 2), 0.25, black),
            ('BOX', (2, 2), (3, 2), 0.25, black),
            ('BOX', (0, 3), (1, 3), 0.25, black),
            ('BOX', (2, 3), (3, 3), 0.25, black),
        ]
        tels = persona.telephones()
        table_data = [
            [Paragraph(f"AC CR {strings['nom']}", style=self.bold11), ''],
            [f'Nom {intit_client}', persona.nom, 'Prénom', persona.prenom],
            ['Adresse', adresse.rue, 'NPA, localité', f'{adresse.npa} {adresse.localite}'],
            ['Tél. princ.', tels[0].tel if tels else "", 'Tél. sec.', tels[1].tel if len(tels) > 1 else ""],
        ]
        if persona.courriel:
            table_data.append(['Courriel', persona.courriel])
        tbl_len = len(table_data)
        table_data.append(['Date de naissance',
            persona.date_naissance.strftime('%d.%m.%Y') if persona.date_naissance else '?',
            'Langue', persona.get_langues_display()
        ])
        style_data.extend([
            ('BOX', (0, tbl_len), (1, tbl_len), 0.25, black),
            ('BOX', (2, tbl_len), (3, tbl_len), 0.25, black),
        ])
        tbl_len = len(table_data)
        if partenaire:
            part_persona = partenaire.persona
            table_data.extend([
                [f"Nom {intit_client} 2", part_persona.nom, "Prénom", part_persona.prenom],
                ["Date de naissance",
                 part_persona.date_naissance.strftime('%d.%m.%Y') if part_persona.date_naissance else '?',
                 "Langue", part_persona.get_langues_display(),
                ],
            ])
            style_data.extend([
                ('BOX', (0, tbl_len), (1, tbl_len), 0.25, black),
                ('BOX', (2, tbl_len), (3, tbl_len), 0.25, black),
                ('BOX', (0, tbl_len + 1), (1, tbl_len + 1), 0.25, black),
                ('BOX', (2, tbl_len + 1), (3, tbl_len + 1), 0.25, black),
            ])
        tbl_len = len(table_data)
        table_data.extend([
            ['Responsable (installateur/trice)', '',
             install_mission.intervenant.nom_prenom if install_mission and install_mission.intervenant else '?'
            ],
            ['Date d’installation', '', date_mission.strftime('%d.%m.%Y') if date_mission else '?'],
        ])
        style_data.extend([
            ('BOX', (0, tbl_len), (1, tbl_len), 0.25, black),
            ('BOX', (2, tbl_len), (3, tbl_len), 0.25, black),
            ('BOX', (0, tbl_len + 1), (1, tbl_len + 1), 0.25, black),
            ('BOX', (2, tbl_len + 1), (3, tbl_len + 1), 0.25, black),
            ('BOX', (0, tbl_len + 2), (1, tbl_len + 2), 0.25, black),
            ('BOX', (2, tbl_len + 2), (3, tbl_len + 2), 0.25, black),
        ])
        self.story.append(Table(
            data=table_data, colWidths=[3* cm, 6 * cm, 3 * cm, 6 * cm],
            style=TableStyle(style_data),
        ))
        nom_abo = abo.nom.replace("Abo ", "") if abo else "?"
        table_data = [
            ['Prestation', 'Type d’appareil', 'ID appareil (carte SIM)', 'Numéro de série (SN)'],
            [P(f'{self.checked_box} {nom_abo}'),
             appareil.modele if appareil else '?',
             appareil.carte_sim if appareil else '?',
             appareil.no_serie if appareil else '?',
            ]
        ]
        style_data = [
            ('LEFTPADDING', (0, 0), (-1, -1), 10),
            ('RIGHTPADDING', (0, 0), (-1, -1), 10),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ('TOPPADDING', (0, 0), (-1, -1), 8),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
            ('BACKGROUND', (0, 0),(-1, -1), self.light_gray),
        ]
        no_lig = 2
        if partenaire:
            table_data.extend([
                [P(f'Accessoires<br/><br/>{self.checked_box} Bracelet émetteur supplémentaire'), '']
            ])
            style_data.append(
                ('SPAN', (0, no_lig), (-1, no_lig)),
            )
            no_lig += 1
        if client.boitier_cle:
            txt = {
                'c-r': "* Boîtier à clés Croix-Rouge",
                'prive': "Boîtier à clés privé",
            }[client.boitier_cle]
            table_data.extend([
                [P(f'{self.checked_box} {txt}'), '', '']
            ])
            style_data.extend([
                ('SPAN', (0, no_lig), (1, no_lig)),
                ('SPAN', (2, no_lig), (3, no_lig)),
            ])
        self.story.append(Table(
            data=table_data,
            colWidths=[4.5 * cm, 4.5 * cm, 4.5 * cm, 4.5 * cm],
            style=TableStyle(style_data),
        ))
        if client.boitier_cle == 'c-r':
            self.story.append(Spacer(1, 0.2 * cm))
            self.story.append(P(
                "*Avant l’installation du boîtier clé, le bénéficiaire ou son référent aura demandé "
                "à sa gérance l’installation du boîtier clé, pour autant que ce dernier soit locataire. "
                "Le bénéficiaire de l’alarme est propriétaire et ainsi responsable du boîtier clé. "
                "Le bénéficiaire autorise l’installateur-bénévole à le poser à l’endroit adapté à cet "
                f"effet. La {strings['nom']} décline toutes responsabilités lors du retrait dudit "
                "boîtier en cas d’endommagements éventuels liés à cette action par le bénéficiaire ou "
                "ses répondants."
            ))
        self.story.append(PageBreak())
        # Page 2
        self.story.append(Paragraph("Données pour la prestation d’aide par la centrale d’alarme", style=self.bold11))
        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P("Situation de vie et conditions de logement"))
        situation_vie = {
            client.SITUATION_VIE_SEUL: 's',
            client.SITUATION_VIE_COUPLE: 'c'
        }.get(client.situation_vie, 'a')
        p_situation_vie = (
            f"{self.checked_box if situation_vie == 's' else self.unchecked_box} seul<br/><br/>"
            f" {self.checked_box if situation_vie == 'c' else self.unchecked_box} "
            "avec mon époux/épouse<br/>  ou compagnon/compagne<br/><br/>"
            f" {self.checked_box if situation_vie == 'a' else self.unchecked_box} "
            f"autre : {client.situation_vie if situation_vie == 'a' else ''}"
        )
        p_type_logement = (
            f"{self.checked_box if client.type_logement == 'villa' else self.unchecked_box} dans une villa<br/>"
            f"{self.checked_box if client.type_logement == 'appartement' else self.unchecked_box} "
            "dans un appartement<br/>"
            f"{self.checked_box if client.type_logement in ['resid', 'ape'] else self.unchecked_box} "
            "appartement avec encadrement<br/>"
            f"{self.checked_box if client.type_logement == 'home' else self.unchecked_box} "
            "dans un home ou institution de soins<br/><br/>"
        )
        if client.type_logement_info:
            p_type_logement += f'Remarque : {client.type_logement_info}<br/>'
        if client.etage:
            p_type_logement += f'Étage/niveau : {client.etage}'

        self.story.append(Table(
            data=[
                [
                    P("Je vis…"), P(p_situation_vie),
                    P("J’habite…"), P(p_type_logement),
                ],
                [
                    P(f"Animal de compagnie : {self.checked_box if client.animal_comp else self.unchecked_box} Oui   "
                      f"{self.checked_box if not client.animal_comp else self.unchecked_box} Non"
                    ), "",
                    P("Ascenseur"),
                    P(f"{self.checked_box if client.ascenseur else self.unchecked_box} Oui   "
                      f"{self.checked_box if not client.ascenseur else self.unchecked_box} Non"
                    ),
                ],
                [
                    "", P(client.animal_comp),
                    P(f"Code entrée immeuble : {client.code_entree or '-'}"),
                ],
                [
                    "", "", P(f"Digicode (boîte à clé) : {client.cles or '-'}"),
                ],
                [
                    "", "", P(f"Nombre de pièces : {client.nb_pieces or '-'}"),
                ],
            ], colWidths=[2.5 * cm, 6.5 * cm, 2.1 * cm, 6.9 * cm],
            style=TableStyle([
                ('BOX', (0, 0), (1, -1), 0.25, black),
                ('BOX', (2, 0), (3, -1), 0.25, black),
                ('VALIGN', (0, 0), (-1, -1), "TOP"),
                ('SPAN', (0, 1), (1, 1)),
                ('SPAN', (2, 2), (3, 2)),
                ('SPAN', (2, 3), (3, 3)),
                ('SPAN', (2, 4), (3, 4)),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))

        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P(
            "<b>Informations importantes à l’intention du personnel soignant / des ambulanciers</b>"
        ))
        if partenaire:
            self.story.append(P(f"<b>Pour {client.nom_prenom}:</b>"))
        self.append_donnees_medic(client)
        if partenaire:
            self.story.append(Spacer(1, 0.6 * cm))
            self.story.append(P(f"<b>Pour {partenaire.nom_prenom}:</b>"))
            self.append_donnees_medic(partenaire)

        if partenaire:
            self.story.append(Spacer(1, 1.5 * cm))
        else:
            self.story.append(PageBreak())

        # Page 3
        self.story.append(Paragraph("Personnes disposant de votre clé à contacter en cas d’urgence", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        if self.is_ape:
            compl = "connaissant l’emplacement de la boîte à clé."
        else:
            compl = "disposant d’une clé de votre appartement/maison ou du moins connaissant l’emplacement de celle-ci."
        self.story.append(P(
            "Veuillez indiquer, <u>par ordre de priorité</u>, au minimum <u>trois</u> répondants vivant "
            f"dans votre voisinage immédiat et {compl}<br/>"
            "Si les personnes mentionnées ne sont pas joignables en cas d’urgence, d’autres services "
            "d’intervention peuvent être mobilisés en fonction de la situation (sauveteurs, pompiers "
            "ou policiers), les coûts éventuels étant à votre charge."
        ))
        self.story.append(Spacer(1, 0.5 * cm))

        has_samas = False
        for idx, rep in enumerate(client.repondants(), start=1):
            if rep.sama:
                # Impression des samaritains
                samas = client.profclient_set.filter(
                    professionnel__type_pro='Sama'
                ).select_related('professionnel').order_by('priorite')
                samas_strs = []
                for idx2, sama in enumerate(samas, start=1):
                    sama_str = f"{str(idx2)}. {sama.professionnel.nom} {sama.professionnel.prenom}"
                    if sama.professionnel.inactif:
                        sama_str = f"<i>{sama_str} (temporairement en inactivité)</i>"
                    samas_strs.append(sama_str)
                self.story.append(Table(
                    data=[
                        [f"{idx})", "Les Samaritains"],
                        [VerticalText("Répondant"), P("<br/>".join(samas_strs))],
                    ],
                    style=TableStyle([
                        ('VALIGN', (1, 0), (2, 2), 'TOP'),
                        ('BOX', (0, 0), (-1, -1), 0.25, black),
                        ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
                    ]),
                    colWidths=[1 * cm, 16.4 * cm],
                ))
                has_samas = True
                continue

            self.print_contact_table(
                rep, idx, left_label="Répondant", relation=True
            )
            self.story.append(Spacer(1, 0.5 * cm))

        self.story.append(PageBreak())

        # Page 4
        self.story.append(Paragraph("Personnes à prévenir en cas d’urgence", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        self.story.append(P(
            "En cas d’urgence, les personnes de référence ne sont pas mobilisées pour la "
            "prestation d’aide, mais sont informées par la centrale d’alarme. Ce sont elles "
            "qui prennent des décisions à votre place si vous n’êtes plus apte à le faire."
        ))
        self.story.append(Spacer(1, 0.5 * cm))
        referents = [ref for ref in client.referents if ref.referent]
        for idx, ref in enumerate(referents, start=1):
            self.print_contact_table(ref, idx, left_label="Pers. de référence")
            self.story.append(Spacer(1, 0.5 * cm))

        self.story.append(Paragraph("Données administratives", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        self.story.append(P(
            "Personne à contacter pour toute question d’ordre administratif ou "
            f"technique (si différent du / de la {intit_client})"
        ))
        admin = [ref for ref in client.referents if ref.admin]
        if admin:
            self.story.append(Spacer(1, 0.3 * cm))
            for pers in admin:
                self.print_contact_table(pers, None)
        else:
            self.story.append(P("<i>Aucune</i>"))
        self.story.append(Spacer(1, 0.5 * cm))

        if self.is_ape:
            # Ne pas prendre en compte la gérance (al-abo)
            ref_factures = [
                ref for ref in client.referents
                if any(val == 'al-tout' for val in ref.facturation_pour)
            ]
            fact_para = P("Facture à adresser à (options supplémentaires uniquement)")
        else:
            ref_factures = [
                ref for ref in client.referents
                if any(val.startswith('al') for val in ref.facturation_pour)
            ]
            fact_para = P("Facture à adresser à (si différent du / de la client·e)")
        if ref_factures:
            self.story.append(fact_para)
            self.story.append(Spacer(1, 0.3 * cm))
            self.print_contact_table(ref_factures[0], None)

        if not self.is_ape or ref_factures:
            self.story.append(Spacer(1, 0.8 * cm))
            self.story.append(KeepTogether([
                P("Mode de paiement (mensuel)"),
                P(f"{self.checked_box} Facture"),
            ]))

        self.story.append(Spacer(1, 0.8 * cm))

        if self.is_ape:
            self.story.append(Paragraph("Participation du locataire", style=self.bold11))
            self.story.append(Spacer(1, 0.3 * cm))
            self.story.append(P(
                "Le locataire garantit que toutes les personnes mentionnées dans le questionnaire "
                "ont consenti à intervenir en cas d’incident et qu’elles ont connaissance de "
                "l’existence de la boîte à clé.<br/>"
                "Toute modification relative au questionnaire doit être immédiatement "
                f"communiquée à la {strings['nom']}."
            ))
            self.story.append(P(
                "Le locataire prévient la <b>centrale d’alarme Croix-Rouge au numéro 058 105 04 18</b> "
                "(ou en pressant sur le bouton du bracelet émetteur) lorsqu’il s’absente un minimum "
                "de 3 jours et laisse le bracelet émetteur à domicile."
            ))
            self.story.append(P(
                "Le locataire manipule les appareils mis à sa disposition avec tout le soin "
                "nécessaire et signale immédiatement les éventuels dysfonctionnements."
            ))
            self.story.append(Spacer(1, 0.5 * cm))

        if not pour_signature:
            tstyle = [
                ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
                ('BOX', (0, 0), (-1, -1), 0.25, black),
                ('VALIGN', (0, 0), (-1, -1), "CENTER"),
            ]
            sigdata = [[P("Lieu et date"), P("Signature locataire") if self.is_ape else P("Signature client·e")]]
            if signature_file:
                im = Image(signature_file, 2 * cm, 2 * cm)
                sigdata.append([P(f"{signature_lieu}, le {date.today().strftime('%d.%m.%Y')}"), im])
            else:
                tstyle.append(('BOTTOMPADDING', (0, 0), (-1, -1), 40))
            if self.is_ape:
                compl = f"de location entre votre gérance et la {strings['nom']}."
            else:
                compl = "Alarme Croix-Rouge et les conventions de prestations correspondantes."
            self.story.append(KeepTogether([
                Paragraph("Dispositions finales", style=self.bold11),
                Spacer(1, 0.3 * cm),
                P(
                    "Veuillez noter que tous les détails des prestations auxquelles vous recourez "
                    f"sont régies par le contrat {compl}"
                ),
                Spacer(1, 0.5 * cm),
                P(
                    "<b>Accord</b><br/>"
                    "Je confirme avoir pris connaissance des informations techniques ci-dessus et "
                    "accepte que mes données personnelles soient enregistrées auprès de la "
                    f"centrale d’alarme et de la {strings['nom']}."
                ),
                Spacer(1, 0.5 * cm),
                Table(
                    data=sigdata,
                    colWidths=[8 * cm, 8 * cm],
                    style=TableStyle(tstyle)
                ),
                Spacer(1, 0.5 * cm),
                P(
                    f"Veuillez remplir ce formulaire de manière complète et précise. La {strings['nom']} "
                    "et la centrale d’alarme s’engagent à traiter de façon strictement "
                    "confidentielle toutes les données personnelles qui y figurent."
                ),
            ]))

        self.doc.build(
            self.story, onFirstPage=self.draw_header_footer, onLaterPages=self.draw_header_footer,
            canvasmaker=PageNumCanvas
        )

    def append_donnees_medic(self, client):
        donnees_medic = client.donnees_medic or {}
        P = partial(Paragraph, style=self.style_normal)

        def medic_cb(key):
            box = self.checked_box if donnees_medic.get(key) else self.unchecked_box
            return P(f'{box} {client.DONNEES_MEDIC_MAP[key]}')

        def medic_cb_plus_text(key):
            return P(
                f'{self.checked_box} {client.DONNEES_MEDIC_MAP[key]} : {donnees_medic[key]}'
                if donnees_medic.get(key)
                else f'{self.unchecked_box} {client.DONNEES_MEDIC_MAP[key]}'
            )

        self.story.append(Table(
            data=[[
                medic_cb_plus_text('troubles_eloc'), medic_cb_plus_text('troubles_aud'),
            ], [
                medic_cb_plus_text('allergies')
            ], [
                medic_cb_plus_text('empl_medic')
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOX', (0, 2), (-1, 2), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
                ('SPAN', (0, 1),(-1, 1)),
                ('SPAN', (0, 2),(-1, 2)),
            ])
        ))
        self.story.append(Table(
            data=[[
                medic_cb('diabete'), medic_cb('troubles_cardio'), medic_cb('epilepsie'),
            ], [
                medic_cb('oxygene'), medic_cb('anticoag'),
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))
        self.story.append(Table(
            data=[[
                medic_cb_plus_text('directives'),
            ], [
                P(f"{client.DONNEES_MEDIC_MAP['autres']} : {donnees_medic.get('autres') or ''}")
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))
        self.story.append(Spacer(1, 0.4 * cm))

        self.story.append(P("Médecin traitant"))
        medecins = client.profclient_set.exclude(professionnel__type_pro='Sama').select_related('professionnel')
        if medecins:
            tdata = []
            for med in medecins:
                tdata.append([
                    P(f"{med.professionnel.nom_prenom}"),
                    P(f"Tél : {med.professionnel.tel_1}"),
                    P(f"Remarque : {med.remarque}") if med.remarque else "",
                ])
            self.story.append(Table(
                data=tdata,
                style=TableStyle([
                    ('INNERGRID', (0, 0), (2, -1), 0.25, black),
                    ('BOX', (0, 0), (-1, -1), 0.25, black),
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
                ]),
                colWidths=[6.7 * cm, 4 * cm, 6.7 * cm],
            ))
        else:
            self.story.append(P("<i>Non renseigné</i>"))


class ConventionLocPDF(PrintContactMixin, BaseCroixrougePDF):
    title = 'Convention de localisation - Alarme Croix-Rouge'
    FONTSIZE = 11

    def get_filename(self, client):
        return f"convention_{slugify(client.nom_prenom)}_{date.today().strftime('%Y_%m_%d')}.pdf"

    def produce(self, client, pour_signature=False, signature_file=None, signature_lieu=None):
        P = partial(Paragraph, style=self.style_normal)
        style_title = ParagraphStyle(
            name='title', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 4, leading=self.FONTSIZE + 5,
        )
        self.story.extend([
            Spacer(1, 0.8 * cm),
            Paragraph("Convention relative à la localisation", style_title),
            Spacer(1, 0.8 * cm),
            P(
                "<b>La présente convention additionnelle fait partie intégrante du contrat client "
                "Alarme Croix-Rouge.</b>"
            ),
            Spacer(1, 0.4 * cm),
        ])
        adr = client.adresse()
        style_data = [
            ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ('TOPPADDING', (0, 0), (-1, -1), 8),
            ('BACKGROUND', (0, 0),(0, -1), self.light_gray),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
        ]
        tels = client.telephones()
        self.story.append(Table(
            data=[
                ["Nom et Prénom du client", client.nom_prenom],
                ["Rue", adr.rue],
                ["CP et localité", f"{adr.npa} {adr.localite}"],
                ["Téléphone", tels[0].tel if tels else ""],
            ], colWidths=[5 * cm, 12.6 * cm],
            style=TableStyle(style_data),
        ))
        self.story.extend([
            Spacer(1, 0.4 * cm),
            P("Déclare accepter d’être localisé·e par la centrale d’alarme de la Croix-Rouge."),
            Spacer(1, 0.3 * cm),
            P(
                "Les répondants ou personnes de référence suivant·e·s sont autorisé·e·s à "
                "déposer une demande de localisation :"
            ),
            Spacer(1, 0.5 * cm),
        ])
        repondants = client.repondants()
        referents = [ref for ref in client.referents if ref.referent and ref not in repondants]
        for idx, contact in enumerate(repondants + referents, start=1):
            if contact.sama:
                continue
            self.print_contact_table(contact, idx, left_label=" ", relation=False)
            self.story.append(Spacer(1, 0.4 * cm))

        if not pour_signature:
            # Inclure lieu/date/signature dans le PDF
            tstyle = [
                ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
                ('BOX', (0, 0), (-1, -1), 0.25, black),
                ('VALIGN', (0, 0), (-1, -1), "CENTER"),
            ]
            sigdata = [[P("Lieu et date"), P("Signature client·e")]]
            if signature_file:
                im = Image(signature_file, 2 * cm, 2 * cm)
                sigdata.append([P(f"{signature_lieu}, le {date.today().strftime('%d.%m.%Y')}"), im])
            else:
                tstyle.append(('BOTTOMPADDING', (0, 0), (-1, -1), 40))
            self.story.append(Table(
                data=sigdata,
                colWidths=[8 * cm, 8 * cm],
                style=TableStyle(tstyle)
            ))
        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class CourrierReferentPDF(BaseCroixrougePDF):
    title = 'Courrier Alarme Croix-Rouge'
    FONTSIZE = 11

    def get_filename(self, referent):
        return f'courrier_repondant_{referent.nom}.pdf'

    def _print_adr_referent(self, referent):
        P = partial(Paragraph, style=self.style_normal)
        self.story.append(Spacer(1, 2 * cm))
        self.story.append(P(referent.get_salutation_display()))
        self.story.append(P(f"{referent.nom_prenom}"))
        if referent.complement:
            self.story.append(P(referent.complement))
        if referent.rue:
            self.story.append(P(referent.rue))
        self.story.append(P(f"{referent.npa} {referent.localite}"))

    def _print_adr_client(self, client):
        self.story.append(Spacer(1, 1.8 * cm))

    def _print_lieu_date(self, lieu):
        self.story.append(Paragraph(
            f"{lieu}, le {django_format(date.today(), 'j F Y')}",
            style=self.style_normal
        ))
        self.story.append(Spacer(1, 1.1 * cm))

    def _print_concerne(self, texte):
        self.story.append(Paragraph(f"<b>{texte}</b>", self.style_normal))
        self.story.append(Spacer(1, 0.6 * cm))

    def _print_salutation(self, referent, titre):
        self.story.append(Paragraph(f"{titre},", self.style_normal))

    def _print_contenu(self, referent, titre, tel_contact):
        Pjust = partial(Paragraph, style=self.style_just)
        client_nom = f"{referent.client.titre} {referent.client.prenoms} {referent.client.noms}"
        verbe = 'peuvent' if referent.client.partenaire_id else 'peut'
        abo = get_abo(referent.client)
        has_relax = abo and "relax" in abo.nom.lower()

        paras = [
            "Par la présente, nous vous informons qu’un appareil d’alarme Croix-Rouge a été "
            f"installé récemment chez {client_nom}, qui {verbe} désormais envoyer un "
            "appel à l’aide 24 heures sur 24 à une centrale d’alarme, par simple pression "
            "sur une touche placée sur un bracelet.",
            "Vous avez accepté de lui porter secours en cas de problème et votre nom est de "
            "ce fait enregistré auprès de la centrale d’alarme en qualité de répondant-e. "
            "Nous vous remercions de votre disponibilité et attirons votre attention sur les "
            "points suivants :",
        ]
        for para in paras:
            self.story.append(Pjust(para))
        strings = canton_strings.get(canton_abrev())
        if has_relax:
            middle_bull_paras = [
                "Avec l’abonnement choisi, à savoir <b>« Relax »</b>, si une alarme est déclenchée, "
                "tous les répondants sont appelés en même temps <b>avec le numéro 044 505 69 96</b>. "
                "La marche à suivre vous est expliquée, à savoir <b>presser sur la touche 5</b> "
                "du téléphone afin d’entrer en communication avec la personne via l’appareil alarme.",
                "Si aucun des répondant-es ne répond à l’appel d’urgence, la centrale d’alarme "
                "réceptionne et traite l’appel d’urgence. <b>La centrale peut vous rappeler avec le "
                "numéro 043 255 34 40</b> et vous demande de vous rendre au domicile de la personne. "
                "Dès que vous vous trouvez sur place, veuillez vous mettre en contact avec le "
                "collaborateur de la centrale par l’intermédiaire du haut-parleur installé chez "
                f"{client_nom} ou par téléphone au <b>058 105 04 18</b>. Vous pouvez ainsi indiquer "
                "si vous êtes en mesure de fournir l’aide nécessaire ou s’il convient de faire appel "
                "à un médecin ou à une ambulance.",
            ]
        else:
            middle_bull_paras = [
                "<b>La centrale d’alarme vous appelle avec ce numéro : 043 255 34 00.</b>",
                "Si une alarme est déclenchée, il se peut que la centrale vous demande de vous "
                "rendre à son domicile. Dès que vous vous trouvez sur place, veuillez vous mettre "
                "en contact avec le collaborateur de la centrale par l’intermédiaire du haut-parleur "
                f"installé chez {client_nom}{tel_contact}. Vous pouvez ainsi "
                "indiquer si vous êtes en mesure de fournir l’aide nécessaire ou s’il convient de "
                "faire appel à un médecin ou à une ambulance. Si la liaison avec la centrale est "
                "interrompue, vous pouvez la rétablir en appuyant une nouvelle fois sur la touche "
                "d’alarme.",
            ]
        if canton_abrev() == "NE":
            final_para = (
                f"Vous déménagez ou êtes absent-e une semaine ou plus, merci d’avertir {client_nom} "
                f"ainsi que la {strings['nom']} par téléphone au <b>{strings['tel']}</b> ou de préférence "
                f"par courriel à : {strings['courriel']}, avec un délai de 48h minimum à l’avance "
                "sur jours ouvrables."
            )
        else:
            final_para = (
                f"Vous déménagez ou êtes absent-e trois jours ou plus, merci d’avertir {client_nom} "
                f"ainsi que la centrale d'alarme."
            )
        bull_paras = [
            "Veuillez vérifier que vous avez bien reçu les clés vous permettant "
            f"d’accéder au logement de {client_nom}.",
        ] + middle_bull_paras + [final_para]
        self.story.append(ListFlowable(
            [ListItem(Pjust(para), leftIndent=35) for para in bull_paras],
            bulletType='bullet'
        ))
        self.story.append(Pjust(
            "Nous restons à disposition pour tout complément d’information et vous "
            f"adressons, {titre}, nos cordiales salutations."
        ))
        self.story.append(Spacer(1, 0.8 * cm))

    def _print_signature(self, texte):
        self.story.append(Paragraph(texte, self.style_normal))

    def produce(self, referent):
        strings = canton_strings.get(canton_abrev())
        titre = referent.get_salutation_display() if (
            referent.salutation and referent.salutation != 'E'
        ) else 'Madame, Monsieur'

        self._print_adr_referent(referent)
        self._print_adr_client(referent.client)
        self._print_lieu_date(strings['lieu'])
        self._print_concerne("Alarme Croix-Rouge")
        self._print_salutation(referent, titre)
        self._print_contenu(referent, titre, strings['tel_contact'])
        self._print_signature(strings['sig'])

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


def get_abo(client):
    install = client.alarme_actuelle()
    if install:
        return install.abonnement
    try:
        mission_en_cours = client.mission_set.filter(effectuee__isnull=True, abonnement__isnull=False).latest('delai')
    except ObjectDoesNotExist:
        return None
    return mission_en_cours.abonnement
