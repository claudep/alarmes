from django.apps import apps
from django.contrib import admin
from django.http import HttpResponse
from django.utils.timezone import now

from common.admin import NullableAsBooleanListFilter, ExportAction
from . import models


@admin.register(models.ArticleFacture)
class ArticleFactureAdmin(admin.ModelAdmin):
    list_display = ['code', 'designation', 'prix', 'compte']
    ordering = ['code']


@admin.register(models.Facture)
class FactureAdmin(admin.ModelAdmin):
    list_display = ['client', 'article', 'date_facture', 'mois_facture', 'montant', 'exporte']
    search_fields = ['client__persona__nom', 'article__designation']
    raw_id_fields = ['install']
    autocomplete_fields = ["client"]
    date_hierarchy = 'date_facture'
    list_filter = ['article']
    actions = ['export_csv_ju']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not apps.is_installed('cr_ju'):
            del actions['export_csv_ju']
        return actions

    @admin.action(description='Export CSV pour Jura')
    def export_csv_ju(self, request, queryset):
        from cr_ju.views import FacturesExportView

        view = FacturesExportView()
        response = HttpResponse(content_type='text/csv', charset='cp1252')
        response['Content-Disposition'] = 'attachment; filename="factures_%s.csv"' % now().strftime('%Y%m%d_%H%M')
        view.exporter_factures(response, queryset.select_related('client').order_by('client', 'mois_facture'))
        return response


@admin.register(models.RabaisAuto)
class RabaisAutoAdmin(admin.ModelAdmin):
    list_display = ['client', 'duree', 'article']
    autocomplete_fields = ["client"]


@admin.register(models.ModeleAlarme)
class ModeleAlarmeAdmin(admin.ModelAdmin):
    list_display = ['nom', 'selected_abos', 'article_achat', 'article_install', 'archive']

    def selected_abos(self, obj):
        return ", ".join(str(abo) for abo in obj.abos.all())


@admin.register(models.Alarme)
class AlarmeAdmin(admin.ModelAdmin):
    list_display = ['modele', 'no_serie', 'no_appareil', 'carte_sim', 'date_achat', 'actif']
    list_filter = ['modele', ('date_archive', NullableAsBooleanListFilter)]
    search_fields = ['no_appareil', 'no_serie', 'carte_sim']
    actions = [ExportAction("Liste appareils alarme")]

    @admin.display(boolean=True)
    def actif(self, obj):
        return obj.date_archive is None


@admin.register(models.JournalAlarme)
class JournalAlarmeAdmin(admin.ModelAdmin):
    list_display = ['alarme', 'description', 'quand', 'qui']


@admin.register(models.TypeAbo)
class TypeAboAdmin(admin.ModelAdmin):
    list_display = ['nom', 'article']


@admin.register(models.Installation)
class InstallationAdmin(admin.ModelAdmin):
    list_display = [
        "alarme", "client_name", "abonnement", "nouvelle", "date_debut", "date_fin_abo", "retour_mat"
    ]
    raw_id_fields = ['alarme']
    autocomplete_fields = ["client"]
    search_fields = ['client__persona__nom']
    list_filter = ['abonnement', 'alarme__modele']

    @admin.display(ordering='client__persona__nom')
    def client_name(self, obj):
        return str(obj.client) if obj.client else "<sans client>"


@admin.register(models.MaterielClient)
class MaterielClientAdmin(admin.ModelAdmin):
    list_display = ['nom', 'client', 'abonnement', 'date_debut', 'date_fin_abo']


@admin.register(models.TypeMateriel)
class TypeMaterielAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Materiel)
class MaterielAdmin(admin.ModelAdmin):
    pass


@admin.register(models.TypeMission)
class TypeMissionAdmin(admin.ModelAdmin):
    list_display = ['nom', 'code', 'avec_client']


@admin.register(models.Mission)
class MissionAdmin(admin.ModelAdmin):
    list_display = ['client_name', 'type_mission', 'intervenant', 'delai', 'effectuee']
    list_filter = ['effectuee', 'type_mission']
    search_fields = ['client__persona__nom', 'description']
    raw_id_fields = ['alarme']
    autocomplete_fields = ["client"]

    @admin.display(ordering='client__persona__nom')
    def client_name(self, obj):
        return str(obj.client) if obj.client else "<sans client>"


@admin.register(models.Frais)
class FraisAdmin(admin.ModelAdmin):
    list_display = ['interv_name', 'descriptif', 'cout']

    @admin.display(ordering='intervenant__last_name')
    def interv_name(self, obj):
        return str(obj.mission.intervenant)
