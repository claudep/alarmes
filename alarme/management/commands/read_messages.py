import re
import ssl
import email
from datetime import date, datetime
from io import BytesIO

from imapclient import IMAPClient
from pypdf import PdfReader

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.mail import mail_admins
from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils.timezone import now

from client.models import Alerte, Client, Journal
from common.choices import Services
from alarme.models import ArticleFacture, Facture


class Command(BaseCommand):
    """
    Commande vérifiant les arrivées de messages AlarmPost de la centrale.
    Généralement exécutée par un job cron toutes les heures.
    """
    def handle(self, **options):
        context = ssl.create_default_context()
        self.server = IMAPClient(settings.ALARMPOST_EMAIL_HOST, ssl_context=context)
        self.server.login(settings.ALARMPOST_EMAIL, settings.ALARMPOST_EMAIL_PASSWORD)
        select_info = self.server.select_folder('INBOX')
        num_messages = select_info[b'EXISTS']

        message_ids = self.server.search()
        messages = self.server.fetch(message_ids, data=['ENVELOPE', 'RFC822'])
        successes = 0
        for mid, content in messages.items():
            success = self.handle_message(mid, content)
            if success:
                successes += 1
        self.server.close_folder()  # also expunges deleted messages
        self.server.logout()
        if options.get('verbosity') > 0:
            return f"{num_messages} handled, {successes} with success"

    def handle_message(self, mid, content):
        envelope = content[b'ENVELOPE']
        raw = email.message_from_bytes(content[b'RFC822'], policy=email.policy.default)
        if '@jotform.com>' in raw['From']:
            try:
                self.handle_jot_message(raw)
            except Exception as exc:
                mail_admins(
                    "[Alarmes] Erreur de traitement de courriel",
                    f"Erreur lors du traitement du courriel jotform.\n"
                    f"L'erreur est {exc}."
                )
            else:
                self.server.delete_messages([mid])
                return True
            return False
        try:
            get_attachments(raw)
        except Exception as exc:
            # Uncomment to exceptionnaly delete some message.
            # breakpoint()
            # self.server.delete_messages([mid])
            mail_admins(
                "[Alarmes] Courriel non traité",
                f"Impossible de lire le courriel avec le sujet «{envelope.subject.decode()}».\n"
                f"L'erreur est {exc}."
            )
        else:
            self.server.delete_messages([mid])
            return True
        return False

    def handle_jot_message(self, raw):
        """Handle message received by JotForm (samaritains)."""
        from bs4 import BeautifulSoup

        def clean_text(txt):
            return txt.strip().replace("\r\n", " ").replace("\n", " ")

        payload = None
        if raw.is_multipart():
            for part in raw.walk():
                if part.get_content_type() == 'text/html':
                    payload = part.get_payload(decode=True)
            if not payload:
                raise ValueError("No payloads detected in Jot message")
        else:
            payload = raw.get_payload(decode=True)
        doc = BeautifulSoup(payload, 'html.parser')
        day = doc.find(id="value_6").text
        day = datetime.strptime(day, '%d-%m-%Y').date()
        heure_al = clean_text(doc.find(id="value_12").text)
        nom_sama = clean_text(doc.find(id="value_4").text)
        descriptif = clean_text(doc.find(id="value_20").text)
        client_parts = clean_text(doc.find(id="value_5").text).split(",")
        parsed_nom = client_parts[0].strip().split(' ')[0]
        parsed_prenom = client_parts[0].strip().split(' ')[-1]
        parsed_npa = client_parts[-1].strip().split(' ')[0]
        client = match_client(nom=parsed_nom, prenom=parsed_prenom, npa=parsed_npa)
        article = ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA)
        libelle = f"Intervention samaritain du {day.strftime('%d.%m.%Y')} ({heure_al})"
        if parten := client.get_partenaire_de() or client.partenaire_id:
            libelle += f" - {client.nom_prenom}"
        # Plusieurs messages peuvent arriver pour la même intervention, ne pas recréer
        # la facture pour même jour/heure
        if not Facture.objects.filter(client=client, libelle=libelle).exists():
            Facture.objects.create(
                client=client, date_facture=day, mois_facture=day,
                libelle=libelle, article=article, montant=article.prix,
            )
            msg = f"Intervention samaritain facturée. Samaritain: {nom_sama}. Description: {descriptif}"
            Alerte.objects.create(
                persona=client.persona, cible=Services.ALARME, alerte=msg, recu_le=now()
            )
            Journal.objects.create(
                persona=client.persona, description=msg, quand=now(), qui=None
            )
        return True


def get_attachments(msg):
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue
        file_name = part.get_filename()  # Get the filename

        if file_name and 'pdf' in part.get('Content-Type').lower():
            payload = part.get_payload(decode=True)
            save_payload(payload)
            return True
    raise ValueError("No valid PDF attachment found in the message content.")


def save_payload(pdf_payload):
    """
    Extract client from PDF payload and create Journal/Alerte instances.
    """
    date_re = r'\d{1,2}\.\d{1,2}\.\d{4}\s+\d{2}:\d{2}:\d{2}'
    lines = [line.strip() for line in get_pdf_text(BytesIO(pdf_payload)).split('\n')]

    def get_line_content(header):
        content = next((line for line in lines if line.startswith(header)), '')
        if content == header:
            content = lines[lines.index(header) + 1]  # Next line
        else:
            content = content.replace(header, '')
        for parasite in ('Gerätetyp:', 'Tel. Wohnung:'):
            if parasite in content:
                content = content.split(parasite)[0]
        return content.strip()

    # -> '12.06.2022 10:14:22'
    date_m = re.search(date_re, get_line_content('Eingegangen am:'))
    if not date_m:
        raise ValueError("Unable to find a date for the alert")
    date_obj = datetime.strptime(date_m.group(), '%d.%m.%Y %H:%M:%S').astimezone()
    ignore_words = ["Monsieur", "Madame", "Famille", "Familie", "Signora", "Herr", "Frau"]
    # -> 'Monsieur Valentin a Marca'
    name_words = [word for word in get_line_content('Name:').split() if word not in ignore_words]
    if not name_words:
        raise ValueError("No name provided in the PDF file")
    npa = get_line_content('PLZ | Ort:').split()[0]
    client = match_client(nom=name_words[-1], prenom=name_words[0], npa=npa)
    # Save PDF to client journal
    remarque = get_line_content('Bemerkung:')
    alerte = Alerte.objects.create(
        persona=client.persona, cible=Services.ALARME, alerte=remarque, recu_le=date_obj
    )
    alerte.fichier.save(f'alerte_{alerte.pk}.pdf', ContentFile(pdf_payload))
    Journal.objects.create(
        persona=client.persona,
        description=f'Alerte Curena: {remarque} (<a href="{alerte.fichier.url}">pdf</a>)',
        quand=date_obj, qui=None,
    )


def match_client(nom, prenom, npa):
    # Find client by name/location
    orig_nom = nom
    nom = re.sub(r'ö|oe', '(ö|oe)', nom)
    prenom = prenom.replace("NA_", "")
    npa = str(npa) if npa else npa
    try:
        client = Client.objects.avec_adresse(date.today()).get(
            persona__nom__unaccent__iregex=nom, adresse_active__npa=npa
        )
    except Client.DoesNotExist:
        raise ValueError(
            f"Impossible de trouver un client dont le nom contient {orig_nom} et le NPA est {npa}"
        )
    except Client.MultipleObjectsReturned:
        try:
            client = Client.objects.par_service(Services.ALARME).avec_adresse(date.today()).get(
                Q(persona__nom__unaccent__iregex=nom) & Q(persona__prenom__unaccent__iregex=prenom) &
                Q(adresse_active__npa=npa)
            )
        except (Client.DoesNotExist, Client.MultipleObjectsReturned):
            raise ValueError(
                f"Plusieurs correspondances pour un client dont le nom contient {orig_nom} "
                f"et le NPA est {npa}"
            )
    return client


def get_pdf_text(pdf):
    read_pdf = PdfReader(pdf)
    page = read_pdf.pages[0]
    return page.extract_text()
