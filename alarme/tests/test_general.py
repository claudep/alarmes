import pickle
from contextlib import ContextDecorator
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from pathlib import Path
from unittest import mock, skipIf

from freezegun import freeze_time

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.core import mail
from django.core.files import File
from django.db.backends.postgresql.psycopg_any import DateRange
from django.test import TestCase, tag
from django.test.utils import ignore_warnings, modify_settings, override_settings
from django.urls import reverse
from django.utils.timezone import get_current_timezone, localtime, make_aware, now

from client.forms import ReferentForm
from client.models import (
    AdresseClient, AdressePresence, Alerte, Client, Prestation, ProfClient,
    Professionnel, Referent, ReferentTel,
)
from alarme.forms import FactureForm, MissionForm
from alarme.models import (
    Alarme, ArticleFacture, FacturationPolicyBase, Facture, Frais, Installation,
    JournalAlarme, Materiel, MaterielClient, Mission, ModeleAlarme, TypeAbo,
    TypeMateriel, TypeMission
)
from benevole.models import Benevole, TypeActivite
from common.choices import Services
from common.export import openxml_contenttype
from common.models import Utilisateur
from common.stat_utils import Month
from common.test_utils import (
    BaseDataMixin, TempMediaRootMixin, read_response_pdf_text, read_xlsx
)
from common.utils import canton_abrev, canton_app, format_mois_an

today = date.today()

ALARME = Services.ALARME
NOT_SET = object()


class default_facturation_policy(ContextDecorator):
    def __enter__(self):
        self._old_policy = canton_app.fact_policy
        canton_app.fact_policy = FacturationPolicyBase()
        return self

    def __exit__(self, *exc):
        canton_app.fact_policy = self._old_policy
        return False


class TestUtils(BaseDataMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_client', 'change_client', 'view_alarme', 'change_alarme', 'change_materiel',
                'change_articlefacture', 'change_mission', 'view_benevole', 'change_benevole',
                'view_facture', 'change_professionnel', 'view_questionnaire',
            ]))
        )
        cls.article_abo = ArticleFacture.objects.create(
            code='ABO1', designation='Abonnement mensuel CASA', prix=Decimal('37.45')
        )
        art_install = ArticleFacture.objects.create(
            code='install',
            designation="Taxe de raccordement : frais de dossier et d’installation",
            prix=80
        )
        ArticleFacture.objects.create(
            code=settings.CODE_ARTICLE_INTERV_SAMA or "40.305",
            designation="Intervention samaritain",
            prix=60
        )
        cls.abo = TypeAbo.objects.create(nom='standard', article=cls.article_abo)
        cls.modele = ModeleAlarme.objects.create(nom='Neat NOVO 4G', article_install=art_install)
        cls.modele.abos.add(cls.abo)
        cls.typem = TypeMission.objects.create(nom='Nouvelle installation', code='NEW')
        cls.type_emetteur = TypeMateriel.objects.create(nom='Émetteur')

    def create_intervenant(self, nom, prenom):
        grp, created = Group.objects.get_or_create(name="Intervenants alarme")
        if created:
            grp.permissions.add(Permission.objects.get(codename='view_alarme'))
        user = Utilisateur.objects.create_user(
            f'{prenom.lower()}@example.org', 'mepassword', first_name=prenom, last_name=nom,
        )
        user.groups.add(grp)
        return user

    def create_client_with_installations(self, num: int, client_data=(), debut_install=None, abo=None):
        """Avec bulk_create, les signaux ne sont pas envoyés pour la création des Installations."""
        clients = []
        bulk_clients = []
        for i in range(num):
            create_kwargs = {
                "nom": "Donzé", "prenom": f"Léa{i}",
                "rue": "rue du Stand 1", "npa": "2000", "localite": "Neuchâtel",
            }
            if len(client_data) > i and client_data[i]:
                create_kwargs |= client_data[i]
            clients.append(Client.objects.create(**create_kwargs))
        alarmes = Alarme.objects.bulk_create(
            [Alarme(modele=self.modele, centrale="medicall", no_appareil=i) for i in range(num)]
        )
        Prestation.objects.bulk_create([
             Prestation(client=cl, service=Services.ALARME, duree=(date.today() - timedelta(days=2), None))
             for cl in clients
        ])
        return Installation.objects.bulk_create([
            Installation(
                client=clients[i], alarme=alarmes[i], abonnement=abo or self.abo,
                date_debut=debut_install or (date.today() - timedelta(days=60)),
            ) for i in range(num)
        ])
        mail.outbox = []

    def nouvelle_install_par_mission(self, client=None, alarme=None):
        today = date.today()
        if client is None:
            client = Client.objects.create(
                nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont"
            )
        mission = Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10)
        )
        if alarme is None:
            alarme = Alarme.objects.create(modele=self.modele, no_serie=999)
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('mission-edit', args=[mission.pk]), data={
                'type_mission': mission.type_mission.pk,
                'client': client.pk,
                'delai': mission.delai,
                'alarme': alarme.pk,
                'abonnement': self.abo.pk,
                'effectuee': today.strftime('%d.%m.%Y'),
                'frais-TOTAL_FORMS': 0,
                'frais-INITIAL_FORMS': 0,
            })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json()['result'], 'OK')
        return client

    def assertJournalMessage(self, obj, message, user=NOT_SET):
        journal = obj.journaux.latest()
        self.assertEqual(journal.description, message)
        self.assertEqual(journal.qui, self.user if user is NOT_SET else user)

    def assertDictContainsSubset(self, d1, d2):
        # Simplified port of equiv Python 2 method.
        try:
            self.assertTrue(set(d2.items()).issubset(set(d1.items())))
        except AssertionError:
            for key, val in d1.items():
                try:
                    if d1[key] != d2[key]:
                        self.fail(f"{key}: {d1[key]} != {key}: {d2[key]}")
                except KeyError:
                    self.fail(f"{key} is absent from the second dict")


class ClientTests(TempMediaRootMixin, TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        if canton_app.label == "cr_ju":
            # Obligatoire pour l'impression du contrat
            ArticleFacture.objects.create(
                code='EMETTEUR', designation="Émetteur suppl.", prix=135.5, compte='3410/10'
            )

    def test_home_nouveaux_clients(self):
        cl_nouv_part = Client.objects.create(
            nom="Dupond", prenom="Élise", npa='2345', date_naissance=date(1947, 8, 2), service=ALARME,
        )
        cl_nouv = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3), service=ALARME,
            partenaire=cl_nouv_part,
        )
        cl2 = Client.objects.create(
            nom="Dupont", prenom="Jeanne", npa='2000', localite='Neuchâtel', service=ALARME,
        )
        Mission.objects.create(
            type_mission=self.typem,
            client=cl2,
            delai=today + timedelta(days=10)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertQuerySetEqual(response.context['nouveaux']['clients'], [cl_nouv])
        self.assertEqual(len(response.context['nouveaux']['missions']), 1)
        self.assertEqual(response.context['nouveaux']['missions'][0].adresse_client["npa"], "2000")

    def test_client_list(self):
        self.client.force_login(self.user)
        client1 = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3), service=ALARME,
            tel_1='078 888 88 44',
        )
        client2 = Client.objects.create(
            nom="Dupônt", prenom="Mélissa", npa='2345', date_naissance=date(1948, 2, 24), service=ALARME,
        )
        Client.objects.create(
            nom="Autre", prenom="Client", npa='2345', date_naissance=date(1950, 3, 21), service=Services.TRANSPORT,
        )
        parten = Client.objects.create(
            nom="Partenaire", prenom="Client", npa='2345', date_naissance=date(1950, 3, 21), service=ALARME,
        )
        client_couple = Client.objects.create(
            nom="En couple", prenom="Client", npa='2345', date_naissance=date(1950, 3, 21), service=ALARME,
            partenaire=parten,
        )
        install_old, install = self.create_client_with_installations(2, [{'npa': '2300'}, {'npa': '2301'}])
        install_old.date_fin_abo = today - timedelta(days=10)
        install_old.save()
        response = self.client.get(reverse('clients'))
        self.assertQuerySetEqual(response.context['object_list'], [])
        # Filter by client name
        response = self.client.get(reverse('clients') + '?nom_prenom=pont')
        self.assertQuerySetEqual(response.context['object_list'], [client2])
        # Filter by tel
        response = self.client.get(reverse('clients') + '?tel=8844')
        self.assertQuerySetEqual(response.context['object_list'], [client1])
        # Filter by liste - résiliation en cours
        response = self.client.get(reverse('clients') + '?listes=resil')
        self.assertQuerySetEqual(response.context['object_list'], [install_old.client])
        self.assertContains(response, f'résilié le {install_old.date_fin_abo.strftime("%d.%m.%Y")}')
        # Filter by liste - clients actifs
        response = self.client.get(reverse('clients') + '?listes=actifs')
        self.assertQuerySetEqual(response.context['object_list'], [install.client])
        # Filter by liste - clients avec partenaire
        response = self.client.get(reverse('clients') + '?listes=parten')
        self.assertQuerySetEqual(response.context['object_list'], [client_couple])
        # Export
        response = self.client.get(reverse('clients') + '?listes=actifs&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        content = read_xlsx(response.content)
        self.assertEqual(content[0][:2], ['Secteurs', 'Nom, Prénom'])
        self.assertEqual(content[0][-2:], ['Première installation', 'Modèle appareil'])
        self.assertEqual(content[1][:2], ['alarme', 'Donzé Léa1'])

    def test_client_list_archives(self):
        # Créer install passée, client est maintenant seulement 'transport'
        install = self.create_client_with_installations(1)[0]
        install.client.prestations.update(
            duree=(date.today() - timedelta(days=60), date.today() - timedelta(days=30))
        )
        Prestation.objects.create(
            client=install.client, service=Services.TRANSPORT, duree=(date.today() - timedelta(days=60), None)
        )
        install.date_fin_abo = today - timedelta(days=60)
        install.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-archives'))
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('clients-archives') + '?nom_prenom=donz')
        self.assertQuerySetEqual(response.context['object_list'], [install.client])

    def test_client_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-new'))
        self.assertContains(
            response,
            '<input type="text" name="nom" maxlength="60" class="form-control" required="" id="id_nom">',
            html=True
        )
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'genre': 'M',
            'npalocalite': '2345 Petaouchnok',
            'langues': ['fr', 'it'],
            'date_naissance': '3.12.1945',
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 1,
            "telephone_set-0-tel": "0551234567",
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        client = Client.objects.get(persona__nom='Dupond')
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual(client.prestations.first().service, ALARME)
        self.assertEqual(client.date_naissance, date(1945, 12, 3))
        self.assertEqual(str(client.adresse()), '2345 Petaouchnok')
        self.assertEqual([tel.tel for tel in client.telephones()], ["055 123 45 67"])
        self.assertJournalMessage(client, "Création du client")
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertContains(response, "Le questionnaire des besoins devrait être fait pour cette personne")

    def test_client_edit(self):
        # Client existant, mais dans d'autres secteurs.
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3),
            service=[Services.TRANSPORT, Services.VISITE], langues=['fr'],
        )
        ProfClient.objects.create(
            client=client, professionnel=Professionnel.objects.create(nom="Docteur super", tel_1="088 888 88 99")
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[client.pk]))
        self.assertNotContains(response, "Cette personne se trouve dans les archives")
        self.assertContains(response, "Cette personne n’est pas identifiée comme cliente «alarme».")
        form_data = {
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'langues': ['fr', 'it'],
            'date_naissance': '3.11.1945',
            'situation_choix': 'Seul-e',
            'courrier_envoye': 'on',
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 1,
            "telephone_set-0-tel": "0551234567",
        }
        response = self.client.post(reverse('client-edit', args=[client.pk]), data=form_data)
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        client.refresh_from_db()
        self.assertEqual(client.langues, ['fr', 'it'])
        self.assertEqual(client.situation_vie, 'Seul-e')
        # Note: le service alarme ne vient plus automatiquement, mais un message sur la
        # page indique que le client n'est pas client alarme.
        self.assertEqual(
            set(client.prestations_actuelles(as_services=True)),
            set([Services.TRANSPORT, Services.VISITE])
        )
        self.assertEqual(client.courrier_envoye, date.today())
        self.maxDiff = None
        expected_changes = (
            "date de naissance (de «1945-12-03» à «1945-11-03»), "
            "langues (de «français» à «français, italien»), téléphone: ajout de numéro («055 123 45 67»), "
            "le courrier de bienvenue a été envoyé (de «faux» à "
            f"«{date.today().strftime('%Y-%m-%d')}»), ajout de situation de vie («Seul-e»)"
        )
        journaux = list(client.journaux.all())
        self.assertEqual(journaux[-1].description, f"Modification du client: {expected_changes}")
        # Alertes pour autres secteurs
        expected_msg = f"Modifications depuis l’application «alarme»: {expected_changes}"
        self.assertQuerySetEqual(
            client.alertes.all().values_list('cible', 'alerte', 'recu_le__date').order_by('cible'),
            [
                ('transport', expected_msg, date.today()),
                ('visite', expected_msg, date.today())
            ]
        )
        # Modif seulement tél, journalisé quand même
        form_data.update({
            "situation_vie": "Seul-e",
            "telephone_set-INITIAL_FORMS": 1,
            "telephone_set-0-tel": "0551234568",
            "telephone_set-0-id": client.telephones()[0].pk,
            "telephone_set-0-persona": client.persona.pk,
        })
        response = self.client.post(reverse('client-edit', args=[client.pk]), data=form_data)
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        journaux = list(client.journaux.all())
        self.assertEqual(
            journaux[-1].description,
            "Modification du client: téléphone: numéro (de «055 123 45 67» à «055 123 45 68»)"
        )

    def test_client_edit_donnees_medic(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", date_naissance=date(1945, 12, 3),
            rue='chemin des Fleurs 3', npa='2345', localite='Quelquepart',
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-donneesmedicales', args=[client.pk]), data={
            'allergies': 'Aux noix',
            'diabete': 'False',
            'oxygene': 'True',
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            client,
            "Modification des données médicales (ajout de allergies («Aux noix»), "
            "ajout de diabète («faux»), ajout de oxygène («vrai»))"
        )

    def test_client_archive(self):
        # Client sans alarme peut être archivé si pas de mission ouverte.
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))
        mission = Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10)
        )
        self.assertFalse(client.can_be_archived(self.user, 'alarme'))
        mission.effectuee = date.today()
        mission.save()
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))

        install = self.create_client_with_installations(1)[0]
        install.client.courrier_envoye = today - timedelta(days=40)
        install.client.save()
        referent = Referent.objects.create(
            client=install.client, nom='Schmid', courrier_envoye=today - timedelta(days=40),
        )
        self.assertFalse(install.client.can_be_archived(self.user, 'alarme'))
        install.date_fin_abo = today - timedelta(days=35)
        install.save()
        client = install.client
        self.assertFalse(client.can_be_archived(self.user, 'alarme'))
        install.retour_mat = today - timedelta(days=33)
        install.save()
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))

        self.client.force_login(self.user)
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, reverse('clients'))
        client.refresh_from_db()
        self.assertIsNone(client.archive_le)
        duree = client.prestations.first().duree
        self.assertEqual((duree.lower, duree.upper), (today - timedelta(days=2), today))
        # Bouton Réactiver sur l'onglet Général
        response = self.client.get(client.get_absolute_url())
        self.assertContains(
            response, '<button type="submit" class="btn btn-danger">Réactiver</button>'
        )
        # client-archive is a toggle, will be reactivated the second time
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, client.get_absolute_url())
        client.refresh_from_db()
        referent.refresh_from_db()
        self.assertIsNone(client.archive_le)
        self.assertIsNone(client.courrier_envoye)
        self.assertIsNone(referent.date_archive)
        self.assertIsNone(referent.courrier_envoye)

    def test_client_install_passee(self):
        """
        Si la dernière installation est passée, elle apparaît quand même.
        """
        install = self.create_client_with_installations(1)[0]
        install.date_fin_abo = today - timedelta(days=5)
        install.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[install.client.pk]))
        self.assertContains(
            response,
            '<p class="mb-0 align-self-center">Appareil installé (depuis le {debut} <b>jusqu’au {fin}</b>): '
            'MEDICALL/<a href="{url_app}" class="text-success">Neat NOVO 4G (-)</a>, abo standard</p>'.format(
                debut=install.date_debut.strftime('%d.%m.%Y'),
                fin=install.date_fin_abo.strftime('%d.%m.%Y'),
                url_app=install.alarme.get_absolute_url(),
            ),
            html=True
        )

    def test_client_derniere_alarme(self):
        install = self.create_client_with_installations(1, debut_install=date.today() - timedelta(days=10))[0]
        install.date_fin_abo = today - timedelta(days=5)
        install.save()
        install2 = Installation.objects.create(
            client=install.client, alarme=Alarme.objects.create(modele=self.modele, no_appareil='9876'),
            abonnement=self.abo, nouvelle=False, date_debut=date.today() - timedelta(days=5),
        )
        self.assertEqual(install.client.derniere_alarme(), install2)
        # Now test when installation_set is prefetched on queryset
        client = Client.objects.filter(pk=install.client_id).prefetch_related('installation_set')[0]
        self.assertEqual(client.derniere_alarme(), install2)

    def test_adresse_absence_edition(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", service=ALARME)
        adr = AdresseClient.objects.create(persona=client.persona, hospitalisation=True)
        AdressePresence.objects.create(
            adresse=adr, depuis=today - timedelta(days=10), jusqua=today - timedelta(days=2)
        )
        pres = AdressePresence.objects.create(adresse=adr, depuis=today - timedelta(days=10))
        self.client.force_login(self.user)
        # test home tab
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Absences/Hospit. <span class="badge bg-secondary">1</span>')
        edit_url = reverse("client-edit", args=[client.pk])
        self.assertContains(response, f'<div class="col"><a href="{edit_url}">Dupond Ladislas</a></div>', html=True)
        response = self.client.post(reverse('client-absence-edit', args=[client.pk, pres.pk]), data={
            'jusqua': '',
            'dernier_contact': today.strftime('%Y-%m-%d'),
            'remarque': 'Toujours absent',
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page#absences'})
        self.assertJournalMessage(
            client,
            f"Suivi d’absence (dernier contact: {today.strftime('%d.%m.%Y')}, remarque: Toujours absent)")

    def test_referent_recoit_courrier(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        repondant = Referent.objects.create(client=client, nom='Schmid', repondant=1)
        referent = Referent.objects.create(client=client, nom='Dupond', referent=1)
        self.assertTrue(repondant.recoit_courrier())
        self.assertFalse(referent.recoit_courrier())

    def test_professionnel_new_from_client(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-professionnel-add', args=[client.pk]))
        response = self.client.post(reverse('client-professionnel-add', args=[client.pk]), data={
            'type_pro': 'Médecin',
            'nom': 'Dr Maboule',
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'tel_1': '076 111 11 22',
            'remarque_client': "Pour madame",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.profclient_set.count(), 1)
        self.assertEqual(client.profclient_set.first().remarque, "Pour madame")
        self.assertJournalMessage(client, "Ajout du professionnel «Dr Maboule (Petaouchnok, Médecin)» (Pour madame)")
        # Now test link to an existing prof.
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        response = self.client.post(reverse('client-professionnel-add', args=[client.pk]), data={
            'professionnel': 'Dr Jekill',
            'professionnel_pk': str(pro.pk),
            'remarque_client': "Pour madame",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.profclient_set.count(), 2)
        self.assertEqual(client.profclient_set.get(professionnel__nom='Dr Jekill').remarque, "Pour madame")
        self.assertJournalMessage(client, "Ajout du professionnel «Dr Jekill (Petaouchnok, Médecin)» (Pour madame)")

    def test_samaritains_liste(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa="2000", localite="Neuchâtel", service=ALARME
        )
        Professionnel.objects.create(
            nom="Archivé", type_pro='Sama', date_archive=date.today() - timedelta(days=10)
        )
        sama_libre = Professionnel.objects.create(nom='Sama1', type_pro='Sama')
        lien_sama = ProfClient.objects.create(
            client=client, priorite=1,
            professionnel=Professionnel.objects.create(nom="SamaActif", type_pro='Sama')
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-samaritains', args=[client.pk]))
        self.assertQuerySetEqual(response.context['current_samas'], [lien_sama])
        self.assertQuerySetEqual(response.context['other_samas'], [sama_libre])

    def test_samaritains_set_nouveau_client(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa="2000", localite="Neuchâtel", service=ALARME
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-setsamas', args=[client.pk]))
        # Pas d'install en cours, ajout simple
        self.assertContains(response, "Confirmez-vous l’ajout du service des Samaritains ?")
        response = self.client.post(reverse('client-setsamas', args=[client.pk]), data={
            "samaritains_des_0": "",
            "samaritains_des_1": "",
        })
        self.assertEqual(response.json()["result"], "OK")
        client.refresh_from_db()
        self.assertEqual(client.samaritains, 1)
        self.assertIsNone(client.samaritains_des)
        # Date début samas est fixée par date d'install.
        self.nouvelle_install_par_mission(client=client)
        client.refresh_from_db()
        self.assertEqual(client.samaritains_des, DateRange(date.today(), None))

    def test_alerte_edit(self):
        client = Client.objects.create(nom="Dupond", prenom="Julie")
        with (Path(__file__).parent / 'alarmpost.pdf').open(mode='rb') as fh:
            alerte = Alerte.objects.create(
                persona=client.persona, cible=Services.ALARME, fichier=File(fh, name='alarmpost.pdf'),
                alerte='Remarque du fichier',
                recu_le=make_aware(datetime(2022, 8, 8, 12, 0))
            )
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-alerte-edit', args=[alerte.pk]), data={
            'traite_le': 'on',
            'remarque': 'Tout est OK',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        alerte.refresh_from_db()
        self.assertEqual(alerte.traite_le.date(), today)
        self.assertEqual(alerte.remarque, 'Tout est OK')
        self.assertEqual(alerte.par, self.user)
        self.assertJournalMessage(client, "Alerte traitée: Tout est OK")

    def test_installation_new_from_client(self):
        # deprecated: new install should go through new intervention
        client1 = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        self.client.force_login(self.user)
        new_install_url = reverse('install-new') + f'?client={client1.pk}'
        response = self.client.get(new_install_url)
        self.assertNotContains(response, "Changement d’alarme")
        self.assertContains(response, "Client: <b>Dupond Ladislas</b>")
        response = self.client.post(new_install_url, data={
            'client': client1.pk,
            'alarme': alarme1.pk,
            'alarme_select': 'Novo 4G',
            'abonnement': self.abo.pk,
            'date_debut': today.strftime('%d.%m.%Y'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})

    def test_installation_new_from_alarme(self):
        # deprecated: new install should go through new intervention
        client1 = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        self.client.force_login(self.user)
        new_install_url = reverse('install-new') + f'?alarme={alarme1.pk}'
        response = self.client.get(new_install_url)
        self.assertNotContains(response, "Changement d’appareil")
        self.assertNotContains(response, "id_alarme_select")
        self.assertContains(response, "Appareil: <b>Neat NOVO 4G (-)</b>")
        response = self.client.post(new_install_url, data={
            'client': client1.pk,
            'alarme': alarme1.pk,
            'abonnement': self.abo.pk,
            'date_debut': today.strftime('%d.%m.%Y'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})

    def test_installation_edit(self):
        install = self.create_client_with_installations(1, debut_install=date(2023, 1, 10))[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('install-edit', args=[install.pk]))
        self.assertContains(response, 'Client: <b>Donzé Léa0</b>')
        response = self.client.post(reverse('install-edit', args=[install.pk]), data={
            'client': install.client.pk,
            'abonnement': install.abonnement.pk,
            'date_debut': "2023-01-03",
            'date_fin_abo': "",
            'motif_fin': "",
            'retour_mat': "",
            'remarques': "Correction début",
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertJournalMessage(
            install.client,
            "Modification de l’installation alarme (date d’installation "
            "(de «2023-01-10» à «2023-01-03»), ajout de remarques («Correction début»))"
        )

    def test_installation_edit_retour_mat(self):
        install = self.create_client_with_installations(1, debut_install=date(2023, 1, 10))[0]
        self.client.force_login(self.user)
        form_data = {
            'client': install.client.pk,
            'abonnement': install.abonnement.pk,
            'date_debut': "2023-01-10",
            'date_fin_abo': date.today() + timedelta(days=10),
            'motif_fin': "décès",
            'retour_mat': date.today() + timedelta(days=2),
            'remarques': "",
        }
        response = self.client.post(reverse('install-edit', args=[install.pk]), data=form_data)
        self.assertEqual(
            response.context['form'].errors, {'retour_mat': [
                "La date de retour de matériel ne peut pas être dans le futur.",
                "La date de retour de matériel ne peut pas être avant la date de fin d’abonnement."
            ]}
        )
        form_data.update({
            'date_fin_abo': date.today() - timedelta(days=10),
            'retour_mat': date.today(),
        })
        response = self.client.post(reverse('install-edit', args=[install.pk]), data=form_data)
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertEqual(
            install.client.prestations.get(service=Services.ALARME).duree.upper,
            date.today()
        )

    def test_installation_close_with_mission(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('install-close', args=[install.pk]))
        self.assertContains(response, 'Client: <b>Donzé Léa0</b>')
        in_eight_days = today + timedelta(days=8)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            # Form install
            'date_fin_abo': today,
            'motif_fin': 'décès',
            'remarques': 'Annoncé ce jour par sa fille',
            # Form mission
            'interv_cb': 'on',
            'type_mission': TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL').pk,
            'description': 'Texte',
            'delai': in_eight_days.strftime('%Y-%m-%d'),
            'benevole': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.date_fin_abo, today)
        self.assertEqual(install.motif_fin, 'décès')
        self.assertIsNone(install.retour_mat)
        self.assertJournalMessage(
            install.client,
            f"Fin de l’abonnement le {today.strftime('%d.%m.%Y')}, motif Décès, Annoncé ce jour par sa fille"
        )
        mission = install.client.mission_set.first()
        self.assertEqual(mission.type_mission.nom, 'Désinstallation')
        self.assertIsNone(mission.effectuee)

    def test_installation_close_without_mission(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            # Form install
            'date_fin_abo': today,
            'motif_fin': 'décès',
            'mat_achete': '',
            'remarques': '',
            # Form mission
            'interv_cb': '',
            'type_mission': TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL').pk,
            'description': '',
            'delai': '',
            'benevole': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.client.mission_set.count(), 0)

    def test_installation_close_mat_achete(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            'date_fin_abo': today + timedelta(days=3),
            'motif_fin': 'décès',
            'mat_achete': 'on',
            'remarques': '',
            'interv_cb': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.client.mission_set.count(), 0)
        self.assertEqual(install.alarme.date_archive, today + timedelta(days=3))
        self.assertEqual(
            install.client.prestations.get(service=Services.ALARME).duree.upper,
            date.today()
        )

    def test_installation_close_cancel(self):
        install = self.create_client_with_installations(1)[0]
        install.date_fin_abo = today + timedelta(days=3)
        install.motif_fin = 'autre'
        install.remarques = "Ne veut plus"
        install.save()
        install.client.prestations.update(duree=(today - timedelta(days=120), today))
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[install.client.pk]))
        self.assertContains(response, "Annuler…</button>")
        response = self.client.post(
            reverse('install-close-cancel', args=[install.pk]),
            data={'comment': "Ne voulait plus. Mais a changé d’avis."}
        )
        install.refresh_from_db()
        self.assertIsNone(install.date_fin_abo)
        self.assertEqual(install.motif_fin, "")
        self.assertJournalMessage(
            install.client,
            'La résiliation a été annulée: Ne voulait plus. Mais a changé d’avis.'
        )
        self.assertEqual(
            install.client.prestations.get(service=Services.ALARME).duree.upper,
            None
        )

    def test_emetteur_ajout(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        emetteur = Materiel.objects.create(
            type_mat=self.type_emetteur, chez_personne=self.user,
        )
        abo_autre = TypeAbo.objects.create(
            nom='Autre abo',
            article=ArticleFacture.objects.create(
                code='xxO1', designation='Autre', prix=Decimal('2')
            )
        )
        autre_type = TypeMateriel.objects.create(nom='Autre mat.')
        autre_type.abos.add(abo_autre)
        abo_emetteur = TypeAbo.objects.create(
            nom='Émetteur supp.',
            article=ArticleFacture.objects.create(
                code='EMO1', designation='Émetteur', prix=Decimal('11.30')
            )
        )
        self.type_emetteur.abos.add(abo_emetteur)
        self.client.force_login(self.user)
        post_data = {
            'materiel': emetteur.pk,
            'materiel_select': 'Émet',
            'abonnement': abo_autre.pk,
            'date_debut': '',  # Date manquante
        }
        url = reverse('materielclient-new', args=[client.pk])
        response = self.client.get(url)
        self.assertQuerySetEqual(
            response.context['form'].fields['abonnement'].queryset,
            [abo_autre, abo_emetteur]
        )
        response = self.client.post(url, data=post_data)
        self.assertEqual(
            response.context['form'].errors, {
                'date_debut': ['Ce champ est obligatoire.'],
                'abonnement': ['Ce matériel n’est pas compatible avec l’abonnement Autre abo'],
            }
        )
        post_data.update({'date_debut': today.replace(day=1), 'abonnement': abo_emetteur.pk})
        response = self.client.post(url, data=post_data)
        if 'html' in response['Content-Type']:
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        emetteur.refresh_from_db()
        self.assertIsNone(emetteur.chez_personne)

    def test_emetteur_facturable_ajout(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        article = ArticleFacture.objects.create(
            code='ACH1', designation='Émetteur achat', prix=Decimal('25.00')
        )
        type_mat = TypeMateriel.objects.create(nom='Émetteur achat', article_achat=article)
        emetteur = Materiel.objects.create(type_mat=type_mat)
        self.client.force_login(self.user)
        post_data = {
            'materiel': emetteur.pk,
            'materiel_select': 'Émet',
            'type_mat': '',
            'abonnement': '',
            'date_debut': date.today() - timedelta(days=10),
        }
        response = self.client.post(reverse('materielclient-new', args=[client.pk]), data=post_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.factures.first().montant, Decimal('25.00'))

    def test_emetteur_fin(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        self.type_emetteur.article_achat = ArticleFacture.objects.create(
            code='ACO1', designation='Montre Domo', prix=Decimal('312.90')
        )
        self.type_emetteur.save()
        emetteur = Materiel.objects.create(type_mat=self.type_emetteur)
        mc = MaterielClient.objects.create(
            client=client, materiel=emetteur, date_debut=date.today() - timedelta(days=10)
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('materielclient-edit', args=[mc.pk]), data={
            'materiel': emetteur.pk,
            'type_mat': '',
            'abonnement': '',
            'date_debut': mc.date_debut,
            'date_fin_abo':  date.today(),
        })
        self.assertEqual(response.json()['result'], 'OK')
        # Matériel archivé car à l'achat
        emetteur.refresh_from_db()
        self.assertEqual(emetteur.date_archive, date.today())

    def test_impression_questionnaire(self):
        install = self.create_client_with_installations(1)[0]
        client = install.client
        Referent.objects.create(client=client, nom='Schmid', repondant=1)
        client.samaritains = 2
        client.samaritains_des = (date.today(), None)
        client.save()
        ProfClient.objects.create(
            client=client, priorite=1,
            professionnel=Professionnel.objects.create(nom="Supersama", type_pro='Sama')
        )
        ProfClient.objects.create(
            client=client, priorite=2,
            professionnel=Professionnel.objects.create(nom="Sama2", type_pro='Sama')
        )
        Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        url = reverse('client-questionnaire', args=[client.pk])
        response = self.client.get(url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        # Aussi avec partenaire
        adr = client.adresse()
        client.partenaire = Client.objects.create(
            nom="Donzé", prenom="Julien",
            rue=adr.rue, npa=adr.npa, localite=adr.localite,
            langues=["de"],
        )
        client.save()
        response = self.client.get(url)
        self.assertEqual(response["Content-Type"], "application/pdf")

    def test_signature_questionnaire(self):
        png_data = (
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAA"
            "C0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII="
        )
        install = self.create_client_with_installations(1)[0]
        Mission.objects.create(
            client=install.client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        url = reverse('client-questionnaire-signer', args=[install.client.pk])
        img_url = reverse('client-questionnaire', args=[install.client.pk]) + '?asimg=yes&forsign=yes'
        response = self.client.get(url)
        self.assertContains(response, f'<img src="{img_url}" class="w-100">', html=True)
        self.assertContains(response, "<canvas")
        # Test sig_data absent
        response = self.client.post(url, data={'sig_data': '', 'lieu': 'Dombresson'})
        self.assertContains(response, "Une erreur a été détectée")
        response = self.client.post(url, data={'sig_data': png_data, 'lieu': 'Dombresson'})
        self.assertRedirects(response, install.client.get_absolute_url())
        quest = install.client.fichiers.first()
        self.assertEqual(quest.titre, f"Alarme - Questionnaire signé du {today.strftime('%d.%m.%Y')}")
        # Nom exact dépendant du canton
        self.assertRegex(quest.fichier.name, r"clients/.*donze.*.pdf")

    def test_client_contrat(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-contrat', args=[install.client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_signature_contrat(self):
        png_data = (
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAA"
            "C0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII="
        )
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        url = reverse('client-contrat-signer', args=[install.client.pk])
        img_url = reverse('client-contrat', args=[install.client.pk]) + '?asimg=yes&forsign=yes'
        response = self.client.get(url)
        self.assertContains(response, f'<img src="{img_url}" class="w-100">', html=True)
        self.assertContains(response, "<canvas")
        # Test sig_data absent
        response = self.client.post(url, data={'sig_data_cr': '', 'lieu': 'Dombresson'})
        self.assertContains(response, "Une erreur a été détectée")
        response = self.client.post(url, data={
            'sig_data_cr': png_data, 'sig_data_cl': png_data, 'lieu': 'Dombresson'
        })
        self.assertRedirects(response, install.client.get_absolute_url())
        quest = install.client.fichiers.first()
        self.assertEqual(quest.titre, f"Alarme - Contrat signé du {today.strftime('%d.%m.%Y')}")
        self.assertEqual(
            quest.fichier.name,
            f"clients/contrat_donze-lea0_{today.strftime('%Y_%m_%d')}.pdf"
        )

    @override_settings(SIGNATURES={})
    def test_impression_courrier_client(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), service=ALARME,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-courrier', args=[client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_impression_courrier_referent(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", genre="M", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), service=ALARME,
        )
        referent = Referent.objects.create(
            client=client, nom='Schmid', prenom='Esther', salutation='F', referent=1
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('client-referent-courrier', args=[client.pk, referent.pk, 'courrier_repondant'])
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertIn(
            " été installé récemment chez Monsieur Ladislas Dupond,",
            read_response_pdf_text(response).replace("\n", " ")
        )

    def test_impression_courrier_referent_abo_relax(self):
        install = self.create_client_with_installations(
            1, abo=TypeAbo.objects.create(nom='Abo Relax', article=self.article_abo)
        )[0]
        referent = Referent.objects.create(
            client=install.client, nom='Schmid', prenom='Esther', salutation='F', referent=1
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('client-referent-courrier', args=[install.client.pk, referent.pk, 'courrier_repondant'])
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertIn(
            "Avec l’abonnement choisi, à savoir « Relax »",
            read_response_pdf_text(response)
        )


class MissionTests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        cls.benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.INSTALLATION], utilisateur=user_benev,
        )

    def test_get_appareil_abo(self):
        from alarme.views import get_appareil_abo

        # Client "nu"
        client = Client.objects.create(nom="Test", service=ALARME)
        self.assertEqual(get_appareil_abo(client), (None, None))

        # Client avec mission nouvelle installation
        appareil = Alarme.objects.create(modele=self.modele, no_serie='123456')
        Mission.objects.create(
            client=client,
            delai=date.today() + timedelta(days=10),
            type_mission=self.typem,
            abonnement=self.abo,
            alarme=appareil,
        )
        self.assertEqual(get_appareil_abo(client), (appareil, self.abo))

        # Client avec installation + mission changement appareil
        install = self.create_client_with_installations(1, abo=self.abo)[0]
        autre_abo = TypeAbo.objects.create(nom='autre', article=self.article_abo)
        Mission.objects.create(
            client=install.client,
            delai=date.today() + timedelta(days=10),
            type_mission=TypeMission.objects.create(nom='Changement appareil', code='CHANGE'),
            abonnement=autre_abo,
            alarme=appareil,
        )
        self.assertEqual(get_appareil_abo(install.client), (appareil, autre_abo))

    def test_mission_new(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        in4days = today + timedelta(days=4)
        response = self.client.get(reverse('mission-new', args=[install.client_id]))
        self.assertEqual(response.context['form'].initial, {'abonnement': self.abo})
        form_data = {
            'description': "Installation chez Mme Trucmuche",
            'type_mission': TypeMission.objects.create(nom='Installation').pk,
            'planifiee_0': in4days.strftime('%d.%m.%Y'),
            'planifiee_1': '15:30',
            'delai': (today + timedelta(days=10)).strftime('%d.%m.%Y'),
        }
        response = self.client.post(reverse('mission-new', args=[install.client_id]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        journal = install.client.journaux.latest()
        self.assertEqual(journal.description, 'Nouvelle intervention: Installation: Installation chez Mme Trucmuche')
        self.assertEqual(journal.qui, self.user)
        mission = Mission.objects.get(client=install.client)
        self.assertEqual(
            localtime(mission.planifiee),
            datetime.combine(in4days, time(15, 30), tzinfo=get_current_timezone())
        )
        # Édition
        form_data.update({
            'intervenant': self.benev.utilisateur.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            install.client,
            f'Modification d’une intervention (ajout de effectuée le («{today.strftime("%Y-%m-%d")}»), '
            'ajout de intervenant («Duplain Irma»))'
        )
        user_autre = self.create_intervenant(nom='Miche', prenom='Cosette')
        form_data['intervenant'] = user_autre.pk
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=form_data)
        self.assertJournalMessage(
            install.client,
            'Modification d’une intervention (intervenant (de «Duplain Irma» à «Miche Cosette»))'
        )

    def test_mission_form_validation(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        alarme = Alarme.objects.create(modele=self.modele, no_serie=999)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        # n° serie/abonnement manquants:
        mission_data = {
            'type_mission': self.typem.pk,
            'delai': mission.delai,
            'abonnement': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        form = MissionForm(instance=mission, data={
            **mission_data, "effectuee": today.strftime("%d.%m.%Y"),
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                "alarme_select": ["Vous devez indiquer l’appareil installé/à installer."],
                "abonnement": ["Vous devez indiquer l’abonnement choisi."],
            }
        )
        # Alarme ne correspond pas à l'abo
        mission.refresh_from_db()
        mission_data.update({
            'alarme_select': 'Novo 4G',
            'alarme': alarme.pk,
            'abonnement': TypeAbo.objects.create(nom='autre', article=self.article_abo).pk,
        })
        form = MissionForm(instance=mission, data=mission_data)
        self.assertIn('alarme', form.fields)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'alarme_select': ['L’appareil Neat NOVO 4G (999) n’est pas compatible avec l’abonnement autre']}
        )
        # Pas de date effectuée dans le futur
        mission_data.update({'abonnement': self.abo.pk, 'effectuee': date.today() + timedelta(days=10)})
        form = MissionForm(instance=mission, data=mission_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'effectuee': ['Il n’est pas autorisé de mettre une date d’intervention effectuée dans le futur']}
        )
        # Pas de date effectuée plus de 1 mois dans le passé
        mission_data["effectuee"] = date.today() - timedelta(days=70)
        form = MissionForm(instance=mission, data=mission_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {"effectuee": ["La date d’intervention effectuée date de plus de deux mois"]}
        )

    def test_mission_nouvelle_installation(self):
        """
        Quand une mission de nouvelle installation est effectuée, une nouvelle
        instance Installation est automatiquement créée.
        """
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        alarme = Alarme.objects.create(
            modele=self.modele, no_serie=999, chez_personne=self.benev.utilisateur
        )

        self.client.force_login(self.user)
        edit_url = reverse('mission-edit', args=[mission.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, "N° de série (SN)")
        # Quand la mission est attribuée, le choix des appareils = stock intervenant
        mission.intervenant = self.benev.utilisateur
        mission.save()
        response = self.client.get(edit_url)
        self.assertNotContains(response, "N° de série (SN)")
        self.assertContains(
            response,
            f"""
            <select name="alarme" class="form-control" id="id_alarme" data-jsonurl="/alarmes/json/">
              <option value="" selected="">---------</option>
              <option value="{alarme.pk}">Neat NOVO 4G (999)</option>
            </select>
            """,
            html=True,
        )

        mission_data = {
            'type_mission': self.typem.pk,
            'delai': mission.delai,
            'effectuee': '',
            'alarme_select': 'Novo 4G',
            'alarme': alarme.pk,
            'abonnement': self.abo.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(edit_url, data=mission_data)
        self.assertEqual(response.json()['result'], 'OK')
        alarme.refresh_from_db()
        # L'appareil est toujours considéré chez l'intervenant tant que la mission
        # n'est pas déclarée comme effectuée.
        self.assertIsNotNone(alarme.chez_personne)
        mission.refresh_from_db()
        self.assertEqual(mission.alarme, alarme)
        response = self.client.get(edit_url)
        self.assertContains(
            response,
            f'<input type="hidden" name="alarme" value="{alarme.pk}" '
            'data-jsonurl="/alarmes/json/" class="form-control" id="id_alarme">'
        )
        self.assertContains(
            response,
            '<input type="text" name="alarme_select" value="Neat NOVO 4G (999)" class="form-control autocomplete"'
            ' autocomplete="off" data-searchurl="/alarmes/search/" data-pkfield="alarme" id="id_alarme_select">'
        )

        # Déclarer comme effectuée
        mission_data['effectuee'] = today.strftime('%d.%m.%Y')
        response = self.client.post(edit_url, data=mission_data)
        self.assertEqual(client.alarme_actuelle().alarme, alarme)
        alarme.refresh_from_db()
        self.assertIsNone(alarme.chez_personne)
        journal = client.journaux.latest()
        self.assertEqual(
            journal.description,
            'Installation d’un nouvel appareil (Neat NOVO 4G (999), standard)'
        )

    def test_mission_change_alarme(self):
        """
        Quand une mission de changement d’installation est effectuée, l'ancienne
        instance est close et une nouvelle instance Installation est automatiquement créée.
        """
        article = ArticleFacture.objects.create(
            code='ABO2', designation='Abo mensuel 2', prix=Decimal('25.00')
        )
        abo_autre = TypeAbo.objects.create(nom='spécial', article=article)
        self.modele.abos.add(abo_autre)
        install = self.create_client_with_installations(1)[0]
        client = install.client
        typem = TypeMission.objects.create(nom='Changement d’installation', code='CHANGE')
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=typem,
            abonnement=abo_autre,
        )
        nouvelle_alarme = Alarme.objects.create(
            modele=self.modele, no_serie=1999, chez_personne=self.benev.utilisateur
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('mission-edit', args=[mission.pk]))
        self.assertContains(response, "N° de série (SN)")
        self.assertContains(response, f'<option value="{abo_autre.pk}" selected>spécial</option>', html=True)
        post_data = {
            'type_mission': mission.type_mission.pk,
            'delai': mission.delai,
            'alarme_select': 'Novo 4G',
            'alarme': nouvelle_alarme.pk,
            'abonnement': abo_autre.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'intervenant': self.benev.utilisateur.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(client.alarme_actuelle().alarme, nouvelle_alarme)
        journal = client.journaux.latest()
        self.assertEqual(
            journal.description,
            'Changement d’appareil, ancien: Neat NOVO 4G (-), nouveau: Neat NOVO 4G (1999)\n'
            'Changement d’abonnement, ancien: standard, nouveau: spécial'
        )
        install.refresh_from_db()
        self.assertEqual(install.date_fin_abo, today - timedelta(days=1))
        self.assertEqual(install.retour_mat, today)
        # L'ancien matériel est en stock chez l'intervenant
        self.assertEqual(install.alarme.chez_personne, self.benev.utilisateur)
        self.assertIsNone(install.alarme.date_archive)
        new_install = install.client.installation_set.get(date_fin_abo__isnull=True)
        self.assertEqual(new_install.date_debut, today)
        self.assertEqual(new_install.alarme, nouvelle_alarme)
        self.assertEqual(new_install.abonnement, abo_autre)
        nouvelle_alarme.refresh_from_db()
        self.assertIsNone(nouvelle_alarme.chez_personne)
        # Réédition après effectuée
        del post_data['alarme_select']
        del post_data['alarme']
        del post_data['abonnement']
        post_data['km'] = 15
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        mission.refresh_from_db()
        self.assertEqual(mission.km, 15)

    def test_mission_change_alarme_sans_install(self):
        """Impossible de créer mission changement d'appareil si pas d'appareil installé."""
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        typem = TypeMission.objects.create(nom='Changement d’installation', code='CHANGE')
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-new', args=[client.pk]), data={
            'description': "Changement chez Mme Trucmuche",
            'type_mission': typem.pk,
            'delai': date.today(),
        })
        self.assertContains(
            response, "Vous ne pouvez pas créer d’intervention de changement d’appareil"
        )

    def test_mission_change_alarme_article_achat(self):
        install = self.create_client_with_installations(1)[0]
        client = install.client
        abo_autre = TypeAbo.objects.create(nom='spécial', article=self.article_abo)
        typem = TypeMission.objects.create(nom='Changement d’installation', code='CHANGE')
        article_achat = ArticleFacture.objects.create(
            code='ACO1', designation='Montre Domo', prix=Decimal('312.90')
        )
        modele_achat = ModeleAlarme.objects.create(
            nom='Montre Domo', article_install=ArticleFacture.objects.get(code='install'),
            article_achat=article_achat
        )
        modele_achat.abos.add(abo_autre)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=typem,
            abonnement=abo_autre,
        )
        nouvel_appareil = Alarme.objects.create(
            modele=modele_achat, no_serie=9876, chez_personne=self.benev.utilisateur
        )

        self.client.force_login(self.user)
        post_data = {
            'type_mission': typem.pk,
            'delai': mission.delai,
            'alarme_select': 'Domo',
            'alarme': nouvel_appareil.pk,
            'abonnement': abo_autre.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'intervenant': self.benev.utilisateur.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(client.factures.filter(article=article_achat).count(), 1)
        self.assertIsNone(client.factures.filter(article=article_achat).first().date_facture)

    def _desinstaller_par_mission(self, install):
        client = install.client
        install.date_fin_abo = today - timedelta(days=1)
        install.motif_fin = 'ems'
        install.save()
        typem = TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL')
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=typem,
            intervenant=self.benev.utilisateur,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data={
            'type_mission': mission.type_mission.pk,
            'delai': mission.delai,
            'effectuee': today.strftime('%d.%m.%Y'),
            'intervenant': self.benev.utilisateur.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')

    def test_mission_desinstaller_alarme(self):
        install = self.create_client_with_installations(1)[0]
        self._desinstaller_par_mission(install)
        install.refresh_from_db()
        self.assertEqual(install.retour_mat, today)
        self.assertEqual(install.alarme.chez_personne, self.benev.utilisateur)
        self.assertEqual(install.client.prestations.first().duree.upper, today)

    def test_mission_desinstaller_alarme_achetee(self):
        """Appareil à l'achat ne retourne pas en stock, mais est directement archivé."""
        install = self.create_client_with_installations(1)[0]
        install.alarme.modele.article_achat = ArticleFacture.objects.create(
            code='ACO1', designation='Montre Domo', prix=Decimal('312.90')
        )
        install.alarme.modele.save()
        self._desinstaller_par_mission(install)
        install.refresh_from_db()
        self.assertEqual(install.retour_mat, today)
        self.assertIsNone(install.alarme.chez_personne)
        self.assertEqual(install.alarme.date_archive, date.today())
        self.assertEqual(install.client.prestations.first().duree.upper, today)

    def test_mission_delete(self):
        self.user.user_permissions.add(Permission.objects.get(codename='delete_mission'))
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        mission_sans_client = Mission.objects.create(
            client=None,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-delete', args=[mission.pk]), data={})
        self.assertQuerySetEqual(Mission.objects.filter(pk=mission.pk), [])
        self.assertJournalMessage(client, "Annulation de l’intervention «Nouvelle installation»")
        response = self.client.post(reverse('mission-delete', args=[mission_sans_client.pk]), data={})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertQuerySetEqual(Mission.objects.filter(pk=mission_sans_client.pk), [])

    def test_calculateur_frais_mensuels(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        other_user = Utilisateur.objects.create_user(
            'other@example.org', 'mepassword', first_name='Jules', last_name='Dupond',
        )
        base_data = {
            'client': client, 'effectuee': '2023-10-07', 'type_mission': self.typem,
            'km': 12, 'intervenant': self.benev.utilisateur,
        }
        Mission.objects.bulk_create([
            Mission(**base_data),
            Mission(**{**base_data, 'effectuee': '2023-10-25'}),
            # Autre mois
            Mission(**{**base_data, 'effectuee': '2023-09-30'}),
            # Intervenant non bénévole
            Mission(**{**base_data, 'intervenant': other_user}),
        ])
        calculateur = canton_app.fact_policy.calculateur_frais(date(2023, 10, 1))
        frais = calculateur.frais_par_benevole()
        self.assertEqual(len(frais), 1)
        self.assertDictContainsSubset(
            {
                'benevole__id': self.benev.pk, 'benevole__nom': 'Duplain', 'benevole__prenom': 'Irma',
                'frais_repas': Decimal('0.00'), 'frais_divers': Decimal('0.00'),
                'kms': Decimal('24.0'),
                'num_visites': 2, 'visites_defrayees': 0,
                'mois': date(2023, 10, 1),
            },
            frais[self.benev],
        )


class MaterielTests(TestUtils, TestCase):
    def test_materiel_list(self):
        mat1 = Materiel.objects.create(type_mat=self.type_emetteur)
        archived = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='9988', date_archive=date(2022, 1, 1))
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-liste'))
        self.assertQuerySetEqual(response.context['object_list'], [mat1])
        response = self.client.get(reverse('materiel-liste') + "?nom_client=xyz")
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('materiel-liste') + '?archived=on')
        self.assertQuerySetEqual(response.context['object_list'], [archived])
        response = self.client.get(reverse('materiel-liste') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_alarme_new(self):
        arch_modele = ModeleAlarme.objects.create(nom='Neat OLD', archive=True)
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarme-new'))
        self.assertContains(response, '<h2>Nouvel appareil</h2>')
        self.assertNotContains(response, 'Neat OLD')

        response = self.client.post(reverse('alarme-new'), data={
            'modele': self.modele.pk,
            'no_appareil': '1234',
            'no_serie': 'xy553322',
            'date_achat': '12.4.2020',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('alarmes'))
        alarme = Alarme.objects.get(no_serie='xy553322')
        self.assertIsNotNone(alarme.modele)
        journal = alarme.journaux.latest()
        self.assertEqual(journal.description, "Création de l’appareil")
        self.assertEqual(journal.qui, self.user)

    def test_alarme_installed_edit(self):
        """Plusieurs champs sont en lecture seule si l'appareil est installé. (#211)"""
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarme-edit', args=[install.alarme.pk]))
        self.assertTrue(response.context['form'].fields['carte_sim'].disabled)

    def test_alarme_edit_benev(self):
        alarme = Alarme.objects.create(modele=self.modele, no_serie='123456')
        interv = self.create_intervenant(nom='Duplain', prenom='Irma')
        self.client.force_login(self.user)
        response = self.client.post(reverse('alarme-edit', args=[alarme.pk]), data={
            'modele': self.modele.pk,
            'no_serie': '123456',
            'chez_personne': interv.pk,
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('alarmes'))
        self.assertJournalMessage(alarme, "Modification de l’appareil: chez Duplain Irma")

    def test_alarmes_list(self):
        install = self.create_client_with_installations(1)[0]
        alarme1 = Alarme.objects.create(modele=self.modele, no_serie='123456', chez_personne=self.user)
        archived = Alarme.objects.create(modele=self.modele, no_serie='123456', date_archive=date(2022, 1, 1))
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarmes'))
        self.assertQuerySetEqual(response.context['object_list'], [install.alarme, alarme1])
        response = self.client.get(reverse('alarmes') + '?archived=on')
        self.assertQuerySetEqual(response.context['object_list'], [archived])
        response = self.client.get(reverse('alarmes') + '?en_stock=on')
        self.assertQuerySetEqual(response.context['object_list'], [alarme1])
        response = self.client.get(reverse('alarmes') + '?nom_client=valj')
        self.assertQuerySetEqual(response.context['object_list'], [alarme1])
        response = self.client.get(reverse('alarmes') + '?en_stock=off')
        self.assertQuerySetEqual(response.context['object_list'], [install.alarme])
        response = self.client.get(reverse('alarmes') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_alarme_journal(self):
        appareil = Alarme.objects.create(modele=self.modele, no_appareil='22334455')
        self.client.force_login(self.user)
        response = self.client.post(reverse('alarme-edit', args=[appareil.pk]), data={
            'modele': self.modele.pk,
            'no_appareil': '22334455',
            'no_serie': '88-123456',
            'chez_benevole': '',
        })
        self.assertRedirects(response, reverse('alarmes'))

        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        install = Installation.objects.create(
            client=client, alarme=appareil, abonnement=self.abo,
            nouvelle=True, date_debut=date.today() - timedelta(days=60),
        )
        JournalAlarme.objects.create(
            alarme=appareil, description="Test",
            quand=make_aware(datetime.combine(install.date_debut, time(11, 30))),
            qui=self.user
        )
        response = self.client.get(reverse('alarme-journal', args=[install.alarme.pk]))
        self.assertEqual(response.context['object_list'], [
            (date.today(), self.user,
             'Modification de l’appareil: ajout de n° de série (sn) («88-123456»)'),
            (install.date_debut, '', 'Installation chez Donzé Léa'),
            (install.date_debut, self.user, 'Test'),
        ])

    def test_alarme_a_reviser(self):
        yest = today - timedelta(days=1)
        install1, install2 = self.create_client_with_installations(3)[0:2]
        install1.date_fin_abo = yest
        install1.retour_mat = yest
        install1.save()
        install2.date_fin_abo = yest
        install2.retour_mat = yest
        install2.save()
        install2.alarme.date_revision = yest
        install2.alarme.save()

        with self.settings(APPAREIL_REVISION_ACTIF=True):
            self.assertEqual(Alarme.objects.a_reviser().count(), 1)
            self.assertTrue(install1.alarme.a_reviser())
            self.assertFalse(install2.alarme.a_reviser())
            # Search view should not find install1
            self.client.force_login(self.user)
            response = self.client.get(reverse('alarme-search') + '?q=neat')
            self.assertEqual(len(response.json()), 1)
            self.assertEqual(response.json()[0]['value'], install2.alarme.pk)

        with self.settings(APPAREIL_REVISION_ACTIF=False):
            self.assertEqual(Alarme.objects.a_reviser().count(), 0)
            self.assertFalse(install1.alarme.a_reviser())
            response = self.client.get(reverse('alarme-search') + '?q=neat')
            self.assertEqual(len(response.json()), 2)

    def test_alarme_reservee(self):
        """
        Si un appareil est déjà prévu pour une installation à faire, il ne
        doit pas apparaître dans la recherche.
        """
        alarme1 = Alarme.objects.create(modele=self.modele, no_serie='123456')
        alarme2 = Alarme.objects.create(modele=self.modele, no_serie='123457')
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarme-search') + '?q=neat')
        self.assertEqual(len(response.json()), 2)
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem,
            alarme=alarme1
        )
        response = self.client.get(reverse('alarme-search') + '?q=neat')
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['value'], alarme2.pk)

    def test_alarme_archive(self):
        alarme = Alarme.objects.create(
            modele=self.modele, no_serie='123456',
            chez_personne=self.user
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('alarme-archive', args=[alarme.pk]), data={})
        self.assertRedirects(response, reverse('home'), fetch_redirect_response=False)
        alarme.refresh_from_db()
        self.assertEqual(alarme.date_archive, date.today())
        self.assertIsNone(alarme.chez_personne)
        self.assertEqual(alarme.journaux.latest().description, "Archivage de l’appareil")

    def test_materiel_edit(self):
        mat = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='1234')
        Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[TypeActivite.INSTALLATION])
        Benevole.objects.create(nom="Doe", prenom="John", activites=[TypeActivite.TRANSPORT])
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-edit', args=[mat.pk]))
        self.assertContains(response, "1234")
        self.assertNotContains(response, "Doe")
        response = self.client.post(reverse('materiel-edit', args=[mat.pk]), data={
            'type_mat': self.type_emetteur.pk,
            'no_ref': '5678',
            'date_achat': '2022-01-06',
            'prix_achat': '',
            'fournisseur': '',
            'chez_personne': '',
        })
        mat.refresh_from_db()
        self.assertEqual(mat.no_ref, '5678')
        self.assertEqual(mat.date_achat, date(2022, 1, 6))

    def test_search_materiel(self):
        Materiel.objects.create(type_mat=self.type_emetteur, no_ref='1234')
        mat2 = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='5555')
        Materiel.objects.create(type_mat=self.type_emetteur, no_ref='9999')
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-search') + '?q=55')
        self.assertEqual(response.json(), [{'label': 'Émetteur (5555)', 'value': mat2.pk}])


@tag("factures")
class FactureTests(TestUtils, TestCase):
    def test_article_facture_edit(self):
        article = ArticleFacture.objects.create(
            code="40.305",
            designation="Batterie supplémentaire",
            prix=60
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse("article-edit", args=[article.pk]), data={
            "code": "40.306",
            "designation": "Batterie supplémentaire",
            "prix": "55.50",
        })
        self.assertEqual(response.json()["result"], "OK")
        article.refresh_from_db()
        self.assertEqual(article.prix, Decimal(55.5))

    def test_facture_new(self):
        """Créer manuellement une facture pour un client."""
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        self.client.force_login(self.user)
        response = self.client.get(reverse('facture-new', args=[client.pk]))
        self.assertContains(response, '<label for="id_article" class="required">Article :</label>', html=True)
        this_month = date.today().replace(day=1)
        self.assertContains(
            response,
            f"""<option value="{this_month.strftime('%Y-%m-%d')}" selected>"""
            f'{format_mois_an(this_month)}</option>',
            html=True
        )

        date_fact = this_month.replace(day=12).strftime('%d.%m.%Y')
        response = self.client.post(reverse('facture-new', args=[client.pk]), data={
            'date_facture': date_fact,
            'mois_facture': this_month.strftime('%Y-%m-%d'),
            'article': self.article_abo.pk,
            'libelle': self.article_abo.designation,
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertJournalMessage(
            client,
            f"Création d’une facture de CHF 37.45 du {date_fact} (Abonnement mensuel CASA)"
        )
        fact = client.factures.first()
        self.assertEqual(fact.date_facture, this_month.replace(day=12))
        self.assertEqual(fact.mois_facture, this_month)
        self.assertEqual(fact.libelle, self.article_abo.designation)
        self.assertEqual(fact.montant, self.article_abo.prix)
        self.assertIsNone(fact.exporte)

    def test_facture_delete(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.get(code='install')
        facture = Facture.objects.create(
            client=install.client, install=install,
            date_facture=today,
            article=article,
            libelle=article.designation, montant=article.prix,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facture-delete', args=[facture.client.pk, facture.pk]))
        self.assertEqual(response.json()['result'], 'OK')

    def test_facture_change_article(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.get(code='install')
        today = date.today().replace(day=16)
        facture = Facture.objects.create(
            client=install.client, install=install,
            date_facture=today,
            mois_facture=today.replace(day=1),
            article=article,
            libelle=article.designation, montant=article.prix,
        )
        autre_article = ArticleFacture.objects.create(code='AUTRE', designation="Blah", prix=100)
        self.client.force_login(self.user)
        edit_url = reverse('facture-edit', args=[install.client.pk, facture.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, article.designation)
        demain = today + timedelta(days=1)
        response = self.client.post(edit_url, data={
            'date_facture': demain.strftime('%Y-%m-%d'),
            'mois_facture': demain.replace(day=1),
            'article': autre_article.pk,
            'libelle': autre_article.designation,
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            install.client,
            f"Modification d’une facture (date de facturation (de «{today}» à «{demain}»), "
            "libellé (de «Taxe de raccordement : frais de dossier et d’installation» à «Blah»), "
            "Montant (de «80.00» à «100.00»))"
        )
        facture.refresh_from_db()
        self.assertEqual(facture.date_facture, demain)
        self.assertEqual(facture.libelle, "Blah")
        self.assertEqual(facture.montant, 100)

    def test_facture_exportee_readonly(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.get(code='install')
        facture = Facture.objects.create(
            client=install.client, install=install,
            date_facture=today,
            article=article,
            libelle=article.designation,
            exporte=now(),
        )
        form = FactureForm(instance=facture)
        self.assertIs(form.readonly, True)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_facture_liste(self):
        today = date.today()
        self.create_client_with_installations(10)
        Installation.generer_factures(today)
        self.client.force_login(self.user)
        response = self.client.get(reverse('factures'))
        self.assertEqual(len(response.context['facture_list']), 10)
        # Test filtrer par date
        moispasse = today - timedelta(days=32)
        fact1 = Facture.objects.first()
        fact1.date_facture = moispasse
        fact1.save()
        date_as_month = moispasse.strftime('%m.%Y')
        response = self.client.get(reverse('factures') + f'?date_facture={date_as_month}')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['facture_list']), 1)
        self.assertContains(response, f'?date_facture={date_as_month}&amp;export=1')
        # Test exportation
        response = self.client.get(reverse('factures') + f'?export=1&amp;date_facture={date_as_month}')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        # Test liste par mois
        liste_url = reverse("factures-mois", args=[today.year, today.month])
        response = self.client.get(liste_url)
        self.assertEqual(len(response.context['facture_list']), 10)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_factures_non_transmises(self):
        day_last_month = date.today().replace(day=1) - timedelta(days=12)
        installs = self.create_client_with_installations(2)
        Installation.generer_factures(day_last_month)
        self.client.force_login(self.user)
        response = self.client.get(reverse('factures-non-transmises'))
        self.assertEqual(len(response.context['facture_list']), 2)
        # Test filter
        article_inst = ArticleFacture.objects.get(code='install')
        Facture.objects.create(
            client=installs[0].client, install=installs[0],
            date_facture=day_last_month,
            mois_facture=day_last_month,
            article=article_inst,
            libelle=article_inst.designation, montant=article_inst.prix,
        )
        response = self.client.get(reverse('factures-non-transmises') + f'?article={article_inst.pk}')
        self.assertEqual(len(response.context['facture_list']), 1)
        response = self.client.get(reverse('factures-non-transmises') + f'?nom_client=Donzé')
        self.assertEqual(len(response.context['facture_list']), 3)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures(self):
        self.user.user_permissions.add(
            Permission.objects.get_by_natural_key('add_facture', 'alarme', 'facture')
        )
        article = ArticleFacture.objects.create(
            code='EMO1', designation='Émetteur', prix=Decimal('11.30')
        )
        abo_emetteur = TypeAbo.objects.create(nom='Émetteur supp.', article=article)

        today = date.today()
        installs = self.create_client_with_installations(6)
        # Install courante -> facture
        installs[0].date_fin_abo = today + timedelta(days=60)
        # Créer facture mois précédent pour installs[0]
        Facture.objects.create(
            client=installs[0].client, install=installs[0],
            date_facture=today - timedelta(days=31),
            mois_facture=(today - timedelta(days=31)).replace(day=1),
            article=self.article_abo,
            libelle='Facture mois précédent', montant=37.5,
        )
        fact_sans_date = Facture.objects.create(
            client=installs[0].client,
            mois_facture=(today - timedelta(days=31)).replace(day=1),
            article=self.article_abo,
            libelle='Facture sans date', montant=37.5,
        )
        self.assertIsNone(fact_sans_date.date_facture)
        # .update() pour éviter les signaux
        # Install passée -> pas de facture
        Installation.objects.filter(pk=installs[1].pk).update(
            date_debut=today - timedelta(days=90), date_fin_abo=today - timedelta(days=60)
        )
        # Install fin fin mois -> facture
        Installation.objects.filter(pk=installs[2].pk).update(date_fin_abo=today.replace(day=25))
        # Install début début mois -> facture + frais install
        Installation.objects.filter(pk=installs[3].pk).update(date_debut=today.replace(day=5))
        # Install future -> pas de facture
        Installation.objects.filter(pk=installs[4].pk).update(date_debut=today + timedelta(days=60))
        # Install avec déjà facture du mois -> pas de facture
        exist = Facture.objects.create(
            client=installs[5].client, install=installs[5],
            date_facture=today, mois_facture=today.replace(day=1),
            article=self.article_abo,
            libelle='Facture de ce mois', montant=self.article_abo.prix,
        )
        # Matériel -> facture abo (mais pas install.)
        installs[3].client.partenaire = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        installs[3].client.save(update_fields=["partenaire"])
        MaterielClient.objects.create(
            client=installs[3].client.partenaire,
            materiel=Materiel.objects.create(type_mat=self.type_emetteur),
            abonnement=abo_emetteur,
            date_debut=today.replace(day=5),
        )
        num_factures = Installation.generer_factures(today)
        num_factures += MaterielClient.generer_factures(today)
        # 3 abos + 1 installation + 1 émetteur
        self.assertEqual(num_factures, 3 + 1 + 1)
        factures_abo = list(Facture.objects.exclude(pk=exist.pk).filter(
            mois_facture=today.replace(day=1)
        ).order_by('client_id'))
        self.assertEqual(
            sorted([fact.montant for fact in factures_abo]),
            [Decimal('11.30'), Decimal('37.45'), Decimal('37.45'), Decimal('37.45')]
        )
        self.assertEqual(factures_abo[0].libelle, f"Abonnement mensuel CASA - {format_mois_an(today)}")
        self.assertEqual(factures_abo[3].libelle, f"Émetteur supp. - {format_mois_an(today)} - Donzé Léa")
        factures_inst = Facture.objects.filter(montant=80)
        self.assertEqual(
            sorted(factures_inst.values_list('montant', flat=True)),
            [Decimal('80.00')]
        )

        # Factures jamais à double
        num_factures = Installation.generer_factures(today)
        num_factures += MaterielClient.generer_factures(today)
        self.assertEqual(num_factures, 0)

        # Tester la vue
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('facturation-mensuelle', args=[today.year, today.month]),
            {'date_facture': today}
        )
        self.assertRedirects(response, reverse('facturation'))
        fact_sans_date.refresh_from_db()
        self.assertEqual(fact_sans_date.date_facture, today)

    def _creer_installations_successives(self, date_changement):
        install1 = self.create_client_with_installations(1)[0]
        install1.date_debut = date(2023, 12, 10)
        install1.date_fin_abo = date_changement - timedelta(days=1)
        install1.save()
        install1_pk = install1.pk
        al2 = Alarme.objects.create(modele=ModeleAlarme.objects.first(), no_appareil='99')
        article2 = ArticleFacture.objects.create(
            code='ABO2', designation='Abonnement mensuel Spécial', prix=Decimal('51.00')
        )
        abo2 = TypeAbo.objects.create(nom='autre', article=article2)
        install2 = install1
        install2.pk = None
        install2._state.adding = True
        install2.date_debut = date_changement
        install2.date_fin_abo = None
        install2.alarme = al2
        install2.abonnement = abo2
        install2.save()
        Facture.objects.all().delete()
        return Installation.objects.get(pk=install1_pk), install2

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures_installs_a_cheval(self):
        """
        Si un client possède plusieurs installations successives (différentes
        alarmes, par ex.), la facture en tient compte (une seule facture).
        """
        install1, install2 = self._creer_installations_successives(today.replace(day=10))
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)
        facture = Facture.objects.get(client=install1.client)
        self.assertEqual(facture.montant, install2.abonnement.article.prix)
        self.assertEqual(facture.article, install2.abonnement.article)
        # Changement install après 20 du mois
        install1.date_fin_abo = today.replace(day=25) - timedelta(days=1)
        install1.save()
        install2.date_debut = today.replace(day=25)
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)
        facture = Facture.objects.get(client=install1.client)
        self.assertEqual(facture.montant, install1.abonnement.article.prix)
        self.assertEqual(facture.article, install1.abonnement.article)
        # Changement install aux limites mensuelles
        # (fin install fin du mois passe, nouvelle install dès 1er du mois courant)
        install1.date_fin_abo = today.replace(day=1) - timedelta(days=1)
        install1.save()
        install2.date_debut = today.replace(day=1)
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)
        facture = Facture.objects.get(client=install1.client)
        self.assertEqual(facture.montant, install2.abonnement.article.prix)
        self.assertEqual(facture.article, install2.abonnement.article)
        # Changement install le mois passé
        # (fin et nouvelle installs le mois passé)
        install1.date_fin_abo = today.replace(day=1) - timedelta(days=17)
        install1.save()
        install2.date_debut = today.replace(day=1) - timedelta(days=16)
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)

    @default_facturation_policy()
    @freeze_time("2024-01-12")
    def test_generer_facture_montant_vide(self):
        article = ArticleFacture.objects.create(
            code='EMO1', designation='Émetteur', prix=None
        )
        abo_sans_prix = TypeAbo.objects.create(nom='Émetteur supp.', article=article)
        installs = self.create_client_with_installations(1, abo=abo_sans_prix)
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 1)
        facture = Facture.objects.first()
        self.assertEqual(facture.libelle, 'Émetteur - janvier 2024')
        self.assertIsNone(facture.montant)

    @override_settings(NOTESFRAIS_TRANSMISSION_URLNAME=None)
    def test_defraiements(self):
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont",
            service=ALARME,
        )
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.INSTALLATION], utilisateur=user_benev
        )
        m1 = Mission.objects.create(
            type_mission=self.typem, client=client, intervenant=benev.utilisateur,
            delai=date.today(), effectuee=date(2023, 5, 12), km=26
        )
        Frais.objects.create(mission=m1, typ='repas', cout='18.35', descriptif='Repas')
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')
        m2 = Mission.objects.create(
            type_mission=self.typem, client=client, intervenant=benev.utilisateur,
            delai=date.today(), effectuee=date(2023, 5, 20), km=5
        )
        Frais.objects.create(mission=m2, typ='repas', cout='12.00', descriptif='Repas')
        m3 = Mission.objects.create(
            type_mission=self.typem, client=client, intervenant=benev.utilisateur,
            delai=date.today(), effectuee=date(2023, 5, 21), km=None
        )

        self.client.force_login(self.user)
        defr_url = reverse('defraiements') + '?year=2023&month=5'
        response = self.client.get(defr_url)
        benev_line1 = response.context['object_list'][0]
        self.assertDictContainsSubset(
            {
                'benevole__id': benev.pk, 'benevole__nom': 'Duplain', 'benevole__prenom': 'Irma',
                'frais_repas': Decimal('30.35'), 'frais_divers': Decimal('4.50'), 'frais_sum': Decimal('34.85'),
                'kms': Decimal('31.0'), 'num_visites': 3,
                'note_frais': None, 'mois': date(2023, 5, 1),
            },
            benev_line1,
        )
        # Le montant exact varie selon le tarif cantonal
        self.assertGreater(benev_line1["montant_total"], Decimal("50"))
        # Exportation
        response = self.client.post(reverse('defraiements-figer', args=['2023', '5']), data={})
        self.assertRedirects(response, defr_url)
        response = self.client.post(reverse('defraiements-export', args=['2023', '5']), data={})
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)


class StatsTests(TestUtils, TestCase):

    def test_stats_installations(self):
        installs = self.create_client_with_installations(6)
        installs[0].date_debut = today
        installs[0].client.samaritains = 1
        installs[0].client.samaritains_des = (date.today(), None)
        installs[0].client.save(update_fields=['samaritains', 'samaritains_des'])
        installs[0].alarme.modele = ModeleAlarme.objects.create(nom="Autre modèle")
        installs[0].alarme.save()
        installs[0].save()
        installs[1].date_fin_abo = today
        installs[1].save()
        installs[2].date_debut = today - timedelta(days=120)
        installs[2].date_fin_abo = today - timedelta(days=40)
        installs[2].save()
        # Passé lointain, plus dans les stats
        installs[3].date_debut = today - timedelta(days=400)
        installs[3].date_fin_abo = today - timedelta(days=310)
        installs[3].save()
        if settings.CODE_ARTICLE_INTERV_SAMA:
            Facture.objects.create(
                client=installs[0].client,
                date_facture=today + timedelta(days=40), mois_facture=today,
                article=ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA),
                montant=20.5,
            )

        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-index'))
        this_month = Month.from_date(today)
        self.assertEqual(response.context['stats']['current'][this_month], 4)
        self.assertEqual(response.context['stats']['current']['total'], 5)
        self.assertEqual(response.context['stats']['new'][this_month], 1)
        self.assertEqual(response.context['stats']['end'][this_month], 1)
        self.assertEqual(response.context['duree_moyenne'].days, 70)
        if settings.CODE_ARTICLE_INTERV_SAMA:
            self.assertEqual(response.context['stats']['interv_sama'][this_month], 1)
            # Listes
            samas_url = reverse('stats-list-samas', args=[this_month.year, this_month.month])
            response = self.client.get(samas_url)
            self.assertContains(response, "Clients avec répondants samaritains")
            self.assertEqual(len(response.context['object_list']), 1)
            with ignore_warnings(category=RuntimeWarning):
                response = self.client.get(samas_url + '?export=1')
            self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
            intervs_url = reverse("stats-intervs-samas", args=[this_month.year, this_month.month])
            response = self.client.get(intervs_url)
            self.assertContains(response, "Interventions samaritains en")
            self.assertEqual(len(response.context['object_list']), 1)
        # Installs du mois, tous modèles
        curinstalls_url = reverse("stats-list-curinstalls", args=[this_month.year, this_month.month, 0])
        response = self.client.get(curinstalls_url)
        self.assertEqual(len(response.context['object_list']), 4)
        response = self.client.get(curinstalls_url + "?export=1")
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        # Installs du mois, modèle spécifique
        response = self.client.get(reverse('stats-list-curinstalls', args=[
            this_month.year, this_month.month, installs[1].alarme.modele_id
        ]))
        self.assertEqual(len(response.context['object_list']), 3)
        # Nouvelles installations du mois
        response = self.client.get(reverse('stats-list-newinstalls', args=[this_month.year, this_month.month]))
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(
            response,
            f'<td><a href="{reverse("client-edit", args=[installs[0].client_id])}">Donzé</a> Léa0</td>',
            html=True
        )
        self.assertContains(response, '<a href="/">Accueil</a>', html=True)
        # Désinstallations du mois
        response = self.client.get(reverse('stats-list-uninstalls', args=[this_month.year, this_month.month]))
        self.assertEqual(len(response.context['object_list']), 1)

    def test_stats_benevoles(self):
        typem = TypeMission.objects.create(nom='Installation', code='NEW')
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.INSTALLATION], utilisateur=user_benev
        )
        clientavs = Client.objects.create(
            nom='Donzé', prenom='Léa', service=ALARME, date_naissance=date(1950, 3, 4),
        )
        clientnonavs = Client.objects.create(
            nom='Donzé', prenom='Léa', service=ALARME,
            date_naissance=date(date.today().year - 62, 3, 4),
        )
        Mission.objects.bulk_create([
            Mission(
                type_mission=typem, client=clientavs, effectuee='2023-03-12',
                duree_client=timedelta(seconds=2800), duree_seul=timedelta(), km=5,
                intervenant=user_benev,
            ),
            Mission(
                type_mission=typem, client=clientnonavs, effectuee='2023-03-29',
                duree_client=timedelta(seconds=1500), duree_seul=timedelta(seconds=300), km=0,
                intervenant=user_benev,
            ),
            # Non pris en compte: non effectuée
            Mission(
                type_mission=typem, effectuee=None,
                duree_client=timedelta(seconds=2800), duree_seul=timedelta(), km=5,
                intervenant=user_benev,
            ),
            # Non pris en compte: intervenant non bénévole
            Mission(
                type_mission=typem, effectuee='2023-03-29',
                duree_client=timedelta(seconds=2800), duree_seul=timedelta(), km=5,
                intervenant=Utilisateur.objects.create_user('interv@example.org', 'xxx'),
            ),
        ])
        self.client.force_login(self.user)
        url = reverse('stats-benevoles') + '?start_month=2&start_year=2023&end_month=3&end_year=2023'
        response = self.client.get(url)
        month = Month(2023, 3)
        month_stats = response.context['stats']['total'][month]
        self.assertEqual(month_stats['durees_client'].seconds, 4300)
        self.assertEqual(month_stats['durees_seul'].seconds, 300)
        self.assertEqual(month_stats['kms'], 5)
        self.assertEqual(response.context['stats']['benevoles'][month], 1)
        response = self.client.get(url + '&avs_only=on')
        self.assertEqual(response.context['stats']['total'][month]['durees_client'].seconds, 2800)
        response = self.client.get(url + '&export=1')
        self.assertEqual(response.status_code, 200)


class OtherTests(TempMediaRootMixin, TestUtils, TestCase):
    def test_acces_plusieurs_secteurs(self):
        interv = self.create_intervenant(nom='Miche', prenom='Cosette')
        interv.user_permissions.add(
            Permission.objects.get(codename='view_questionnaire')
        )
        self.client.force_login(interv)
        response = self.client.get(reverse('home'), follow=True)
        self.assertTemplateUsed(response, 'alarme/index-choice.html')
        self.assertContains(response, "Analyse des besoins")
        self.assertContains(response, "Mes interventions")

    def test_home_page_alarmpost_sans_fichier(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa')
        Alerte.objects.create(persona=client.persona, cible=Services.ALARME, recu_le=now())
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_home_mat_en_stock(self):
        Utilisateur.objects.create
        user = Utilisateur.objects.create_user("user1@example.org", password="test", is_active=False)
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil="123456", chez_personne=user)
        benev = self._create_benevole(avec_utilisateur=True, activites=[])
        alarme2 = Alarme.objects.create(
            modele=self.modele, no_appareil="789023", chez_personne=benev.utilisateur
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("home"))
        self.assertEqual(
            set(response.context["mat_en_attente"]["app_stock"]),
            {user, benev.utilisateur}
        )

    def test_home_controles(self):
        self.client.force_login(self.user)
        install1, install2 = self.create_client_with_installations(2)
        Referent.objects.create(client=install1.client, nom='Schmid', repondant=1)
        install1.client.courrier_envoye = date(2023, 12, 2)
        install1.client.save(update_fields=['courrier_envoye'])
        # Un client sans installation n'apparaît pas dans les contrôles d'envoi
        client = Client.objects.create(nom='Smith', prenom='Jill', service=ALARME)
        Referent.objects.create(client=client, nom='Schmid', repondant=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('controles'))
        self.assertQuerySetEqual(
            response.context['courriers_non_envoyes'],
            [install1.client, install2.client]
        )

    def test_read_alerte_match_client(self):
        from alarme.management.commands.read_messages import match_client

        cl1 = Client.objects.create(
            nom='Dupond du Sapin', prenom='Julie', npa='2345', localite='Les Breuleux', service=ALARME,
        )
        Client.objects.create(
            nom='Dupond', prenom='Julien', npa='2345', localite='Les Breuleux', service=Services.TRANSPORT,
        )
        cl2 = Client.objects.create(
            nom='Roesti', prenom='Hans', npa='2800', localite='Delémont', service=ALARME,
        )
        with self.assertRaisesMessage(
            ValueError, "Impossible de trouver un client dont le nom contient Dupond et le NPA est 2440"
        ):
            match_client(nom='Dupond', prenom="julie", npa=2440)
        self.assertEqual(match_client(nom='Dupond', prenom="julie", npa=2345), cl1)
        self.assertEqual(match_client(nom='Sapin', prenom="julie", npa=2345), cl1)
        self.assertEqual(match_client(nom='Röst', prenom="Hans", npa=2800), cl2)

    def test_read_alerte_pdf(self):
        from alarme.management.commands.read_messages import save_payload

        cl = Client.objects.create(
            nom='Dupond', prenom='Julie', npa='2345', localite='Les Breuleux', service=ALARME,
        )
        pdf_content = (Path(__file__).parent / 'alarmpost.pdf').read_bytes()
        save_payload(pdf_content)
        self.assertEqual(cl.alertes.count(), 1)
        self.assertEqual(cl.alertes.first().alerte, 'Akku vide_NEMO_email CRS_hoo')

    @skipIf(settings.CODE_ARTICLE_INTERV_SAMA == '', "CODE_ARTICLE_INTERV_SAMA needed for this test")
    def _test_read_alerte_samas(self, mail_content):
        from alarme.management.commands.read_messages import Command

        cl = Client.objects.create(
            nom='Rafter', prenom='Julien', npa='2028', localite='Vaumarcus', service=ALARME,
        )
        command = Command()
        command.server = mock.Mock()
        success = command.handle_message(123, mail_content)
        self.assertIs(success, True)
        command.server.delete_messages.assert_called_once_with([123])
        self.assertEqual(cl.factures.count(), 1)
        fact = cl.factures.first()
        self.assertEqual(fact.libelle, "Intervention samaritain du 07.02.2024 (05:50)")
        self.assertEqual(fact.montant, Decimal(60))
        msg = (
            "Intervention samaritain facturée. Samaritain: Super Sama. Description: "
            "M. Rafter était allongé à côté de son lit. Pas de douleurs ressenties. "
            "Petite plaie ouverte sur le coude gauche et hématome sur le coude droit. "
            "Relevage, soins."
        )
        self.maxDiff = None
        self.assertEqual(cl.alertes.first().alerte, msg)
        self.assertJournalMessage(cl, msg, user=None)
        # Deuxième lecture avec mêmes infos date/heure -> pas de 2ème facture
        command.handle_message(123, mail_content)
        self.assertEqual(cl.factures.count(), 1)

    def test_read_alerte_samas(self):
        with open(Path(__file__).parent / 'jotmsg528.pickle', 'rb') as fh:
            mail_content = pickle.load(fh)
        self._test_read_alerte_samas(mail_content)

    def test_read_alerte_samas_multipart(self):
        with open(Path(__file__).parent / 'jotmultipart.pickle', 'rb') as fh:
            mail_content = pickle.load(fh)
        self._test_read_alerte_samas(mail_content)

    def test_exportation_admin(self):
        user = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        alarme2 = Alarme.objects.create(modele=self.modele, no_appareil='78910')
        change_url = reverse('admin:alarme_alarme_changelist')
        # POST data to change_url
        self.client.force_login(user)
        response = self.client.post(change_url, {
            'action': 'export', '_selected_action': [alarme1.pk, alarme2.pk]
        })
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    @override_settings(TEST_INSTANCE=False)
    @modify_settings(ALLOWED_HOSTS={'prepend': 'alarme.croix-rouge-ne.ch'})
    def test_envoi_centrale(self):
        install = self.create_client_with_installations(1, client_data=[{
            'rue': 'Rue du Stand 99',
            'npa': '2000', 'localite': 'Neuchâtel',
            'tel_1': '032 222 22 22',
            'langues': ['fr'],
            'donnees_medic': {'troubles_eloc': 'Aphasie', 'diabete': False, 'epilepsie': True, 'oxygene': None},
            'situation_vie': 'Seul-e',
            'animal_comp': '3 chiens',
            'type_logement': 'appartement',
            'type_logement_info': 'Entrée par derrière',
            'etage': '1er',
            'ascenseur': True,
            'code_entree': '12345',
            'nb_pieces': '3.5',
            'cles': 'clé sous le paillasson',
            'samaritains': True,
        }])[0]
        install.alarme.no_serie = "X5432B1"
        install.alarme.save()
        ref = Referent.objects.create(
            client=install.client, nom='Schmid', prenom='Claire', npa='2000', localite='Neuchâtel',
            referent=1, repondant=2
        )
        ReferentTel.objects.create(referent=ref, priorite=1, tel='079 999 99 99')
        ProfClient.objects.create(
            client=install.client, professionnel=Professionnel.objects.create(
                nom="Cabinet GROUPE", rue="Rue Basse 14", npa='1700', localite='Fribourg',
                tel_1="088 888 88 99", remarque="Dr Knox"
            ),
            remarque="Si urgence",
        )
        ProfClient.objects.create(
            client=install.client, priorite=2,
            professionnel=Professionnel.objects.create(nom="Supersama", type_pro='Sama')
        )
        ProfClient.objects.create(
            client=install.client, priorite=1,
            professionnel=Professionnel.objects.create(nom="SamaP1", prenom="Céline", type_pro='Sama')
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[install.client.pk]))
        subj = response.context['form'].initial['subject']
        self.assertRegex(subj, r'\[Croix-Rouge (NE|JU)\] Mise à jour client Donzé Léa0')
        body = response.context['form'].initial['body']
        self.assertEqual(body, f"""Client Alarme Croix-Rouge {canton_abrev()}
====
Nom: Donzé
Prénom: Léa0
Rue: Rue du Stand 99
NPA, localité: 2000 Neuchâtel
Tél. 1: 032 222 22 22
Tél. 2: 
Courriel: 
Date de naissance: None
Langue(s): français

Appareil: Neat NOVO 4G, SIM , SN X5432B1

Situation de vie: Seul-e
Animal de compagnie: 3 chiens
Type de logement: Appartement, Entrée par derrière
Nb de pièces: 3.5
Étage: 1er, avec ascenseur
Code d’entrée: 12345
Infos clés: clé sous le paillasson

Données médicales
========
Troubles de l’élocution: Aphasie
Diabète: Non
Epilepsie: Oui
Oxygène: ?

Médecins/soignant-e-s:
 - Cabinet GROUPE, Rue Basse 14, 1700 Fribourg, 088 888 88 99, Dr Knox, (Si urgence)

Répondants
========
1. Samaritains
    1. SamaP1 Céline
    2. Supersama 

2. Schmid Claire, , 2000 Neuchâtel
Tél 1: 079 999 99 99
Relation: 

Personnes à prévenir en cas d’urgence
========
1. Schmid Claire, , 2000 Neuchâtel
Tél 1: 079 999 99 99
Relation: 

Historique récent (30j.)
========

""")  # noqa
        response = self.client.post(reverse('client-envoicentrale', args=[install.client.pk]), data={
            'subject': subj,
            'body': body,
        })
        self.assertRedirects(response, reverse('client-edit', args=[install.client.pk]))
        self.assertEqual(mail.outbox[0].recipients(), [settings.CENTRALE_EMAIL])
        self.assertEqual(mail.outbox[0].subject, subj)
        self.assertTrue(mail.outbox[0].body.startswith(
            "[Message envoyé par Valjean Jean <me@example.org>, depuis https://alarme.croix-rouge-ne.ch]\n\n"
        ))
        self.assertJournalMessage(install.client, f"Envoi des données du jour à {settings.CENTRALE_EMAIL}")
        self.assertIn("<b>Destinataire:</b>", install.client.journaux.latest().details)

    def test_envoi_centrale_appareil_de_mission(self):
        """
        Les données d'appareil doivent apparaître dans le message même si
        l'installation n'a pas encore eu lieu.
        """
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', service=ALARME,
            rue="Fleurs 432", npa="2800", localite="Delémont",
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[client.pk]))
        self.assertIn("Appareil: pas encore défini\n", response.context['form'].initial['body'])

        Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10),
            planifiee=now(),
            alarme=Alarme.objects.create(modele=self.modele, no_serie=999)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[client.pk]))
        body = response.context['form'].initial['body']
        self.assertIn(
            f"""
Appareil: Neat NOVO 4G, SIM , SN 999
Type d’intervention: Nouvelle installation
Intervention planifiée le: {localtime(now()).strftime('%d.%m.%Y %H:%M')}
""",
            body
        )

    def test_envoi_centrale_changement_appareil(self):
        install = self.create_client_with_installations(1)[0]
        Mission.objects.create(
            type_mission=TypeMission.objects.create(nom="Changement d’installation", code='CHANGE'),
            client=install.client,
            delai=today + timedelta(days=10),
            planifiee=now(),
            alarme=Alarme.objects.create(modele=self.modele, no_serie=999)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[install.client.pk]))
        body = response.context['form'].initial['body']
        self.assertIn(
            f"""
Ancien appareil: Neat NOVO 4G, SIM , SN 
Nouvel appareil: Neat NOVO 4G, SIM , SN 999
Type d’intervention: Changement d’installation
Intervention planifiée le: {localtime(now()).strftime('%d.%m.%Y %H:%M')}
""",
            body
        )

    def test_utilisateurs_actifs(self):
        from alarme.models import utilisateurs_alarme

        inact = self.create_intervenant(nom='Inactif', prenom='User')
        inact.is_active = False
        inact.save()
        intervenant = self.create_intervenant(nom='Test', prenom='Interv')
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[TypeActivite.INSTALLATION],
            npa="2800", localite="Delémont",
            utilisateur=user_benev,
        )
        user_benev_temp_inactif = Utilisateur.objects.create_user(
            'temp@example.org', 'yyy', first_name='John', last_name='Smith',
        )
        benev_temp = Benevole.objects.create(
            nom="Smith", prenom="John", activites=[TypeActivite.VISITE_ALARME], utilisateur=user_benev_temp_inactif,
        )
        benev_temp.activite_set.update(inactif=True)
        old_user_benev = Utilisateur.objects.create_user(
            'old@example.org', 'xxx', first_name='Zoé', last_name='Schmid',
        )
        old_benev = Benevole.objects.create(
            nom="Schmid", prenom="Zoé", activites=[TypeActivite.INSTALLATION], utilisateur=old_user_benev,
        )
        old_benev.activite_set.update(duree=('2022-03-10', '2024-01-31'))

        self.assertQuerySetEqual(utilisateurs_alarme(), [user_benev, intervenant])
        self.assertQuerySetEqual(utilisateurs_alarme(include=inact), [user_benev, inact, intervenant])

        # Liste des bénévoles
        self.client.force_login(self.user)
        response = self.client.get(reverse('intervenants'))
        self.assertQuerySetEqual(
            response.context['object_list'], [user_benev, user_benev_temp_inactif, intervenant]
        )
        self.assertEqual(response.context['object_list'][0].adresse_active["localite"], "Delémont")
        response = self.client.get(reverse('intervenants') + '?activite=vis')
        self.assertQuerySetEqual(
            response.context['object_list'], [user_benev_temp_inactif]
        )
        response = self.client.get(reverse('intervenants') + '?activite=interv')
        self.assertQuerySetEqual(
            response.context['object_list'], [intervenant]
        )
        response = self.client.get(reverse('intervenants') + '?nom=dupl')
        self.assertQuerySetEqual(
            response.context['object_list'], [user_benev]
        )
        response = self.client.get(reverse('intervenants') + '?npa_localite=dele')
        self.assertQuerySetEqual(
            response.context['object_list'], [user_benev]
        )
        response = self.client.get(reverse('intervenants') + "?export=1")
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
