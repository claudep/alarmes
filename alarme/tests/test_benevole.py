from django.contrib.auth.models import Permission
from datetime import date, timedelta
from decimal import Decimal
from unittest import expectedFailure

from django.conf import settings
from django.core import mail
from django.core.files.base import ContentFile
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.timezone import now

from alarme.forms import BenevoleMissionForm
from alarme.models import Alarme, Frais, Mission, ModeleAlarme, TypeMission
from benevole.models import Benevole, Formation, TypeActivite, TypeFormation
from client.models import AdresseClient, AdressePresence, Client
from common.choices import Services
from common.models import Fichier, TypeFichier, Utilisateur
from common.utils import canton_abrev

from .test_general import TestUtils

today = date.today()
ALARME = Services.ALARME


class BenevoleTests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # Création d'une bénévole
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Jeanne', last_name='d’Arc',
        )
        cls.benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", genre="F", npa="2000", localite="Neuchâtel",
            utilisateur=user_benev,
            activites=[TypeActivite.INSTALLATION],
        )
        assert cls.benev.adresse() is not None
        # Employé Croix-Rouge non bénévole
        cls.intervenant = Utilisateur.objects.create_user(
            'employe@example.org', 'mepassword', first_name='Jane', last_name='Austin',
        )
        cls.intervenant.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_alarme',
            ]))
        )

    def test_has_intervention_on(self):
        cl1 = Client.objects.create(nom='Schmid', prenom='Léna')
        cl2 = Client.objects.create(nom='Schmid', prenom='Urs')
        benev_user = self.benev.utilisateur
        self.assertFalse(benev_user.has_intervention_on(cl1))
        Mission.objects.create(
            client=cl1, intervenant=benev_user, type_mission=self.typem, delai=today,
            effectuee=today - timedelta(days=30),
        )
        Mission.objects.create(
            client=cl2, intervenant=benev_user, type_mission=self.typem, delai=today,
            effectuee=today - timedelta(days=3),
        )
        # L'intervention sur cl1 a plus de 10 jours, ne compte plus
        self.assertFalse(benev_user.has_intervention_on(cl1))
        Mission.objects.create(
            client=cl1, intervenant=benev_user, type_mission=self.typem, delai=today,
            effectuee=today - timedelta(days=3),
        )
        self.assertTrue(benev_user.has_intervention_on(cl1))
        self.assertTrue(benev_user.has_intervention_on(cl1, types=['NEW']))
        self.assertFalse(benev_user.has_intervention_on(cl1, types=['VISITE']))

    @override_settings(EXEMPT_2FA_NETWORKS=[])
    def test_benevole_login(self):
        response = self.client.get(reverse('home-app'), follow=True)
        expected_url = '/accounts/login/?next=/app/'
        self.assertEqual(response.redirect_chain, [(expected_url, 302)])
        response = self.client.post(expected_url, data={
            'auth-username': 'benev@example.org',
            'auth-password': 'mepassword',
            'login_view-current_step': 'auth',
        }, follow=True)
        self.assertContains(response, "Activer l'authentification à deux facteurs")
        self.assertEqual(response.redirect_chain, [('/app/', 302), ('/account/two_factor/setup/', 302)])

    def test_acces_benevole_accueil(self):
        cl = Client.objects.create(
            nom='Schmid', prenom='Léna', rue="Rue Inconnue 2", npa="2000", localite="Neuchâtel",
            visiteur=self.benev.utilisateur,
        )
        Mission.objects.create(
            client=cl, intervenant=self.benev.utilisateur,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home'), follow=True)
        self.assertTemplateUsed(response, 'alarme/app/index.html')
        self.assertEqual(response.context["mes_missions"][0].adresse_client["localite"], "Neuchâtel")
        self.assertContains(response, "Schmid")
        self.assertContains(response, "Rue Inconnue 2")

    def test_acces_plusieurs_secteurs(self):
        self.benev.utilisateur.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'change_materiel', 'change_alarme', 'view_questionnaire'
            ]))
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home'), follow=True)
        self.assertTemplateUsed(response, 'alarme/index-choice.html')
        self.assertContains(response, "Analyse des besoins")
        self.assertContains(response, "Atelier")
        self.assertContains(response, "Mes interventions")

    def test_mission_client_autre_adresse(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna')
        adr = AdresseClient.objects.create(persona=cl.persona, hospitalisation=True)
        AdressePresence.objects.create(adresse=adr, depuis=date.today() - timedelta(days=10))
        Mission.objects.create(
            client=cl, intervenant=self.benev.utilisateur,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        self.assertContains(response, "Actuellement à l’hôpital")

    def _test_acces_au_client(self, utilisateur):
        cl = Client.objects.create(nom='Schmid', prenom='Léna')
        self.client.force_login(utilisateur)
        edit_url = reverse('benevole-client-edit', args=[cl.pk])
        response = self.client.get(edit_url)
        # Pas d'accès sans intervention
        self.assertEqual(response.status_code, 403)

        Mission.objects.create(
            client=cl, intervenant=utilisateur,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )
        self.assertTrue(cl.can_read(utilisateur))
        response = self.client.get(edit_url)
        self.assertTrue(response.context['form'].readonly)
        self.assertEqual(
            [tab['id'] for tab in response.context['tabs']],
            ['general', 'journal']
        )

        # L'utilisateur peut modifier le client s'il a une mission d’installation pour lui
        Mission.objects.create(
            client=cl, intervenant=utilisateur,
            type_mission=TypeMission.objects.create(nom='Install', code='NEW'),
            delai=today, effectuee=None,
        )
        self.assertTrue(cl.can_edit(utilisateur))
        response = self.client.get(edit_url)
        self.assertFalse(response.context['form'].readonly)
        self.assertEqual(
            [tab['id'] for tab in response.context['tabs']],
            ['general', 'journal']
        )
        # Accès listes liées au client
        for url_name in ['client-referent-list', 'client-fichier-list']:
            response = self.client.get(reverse(url_name, args=[cl.pk]))
            self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('persona-adresse-list', args=[cl.persona_id]))
        self.assertEqual(response.status_code, 200)
        # Accès également au questionnaire pour signature
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 200)

    def test_acces_benevole_au_client(self):
        self._test_acces_au_client(self.benev.utilisateur)

    def test_acces_intervenant_au_client(self):
        self._test_acces_au_client(self.intervenant)

    def test_acces_benevole_quest_contrat(self):
        """Accès à la section Questionnaire/Contrat de l'édition des clients (#527)."""
        cl = Client.objects.create(nom="Schmid", prenom="Léna", service=ALARME)
        Mission.objects.create(
            client=cl, intervenant=self.benev.utilisateur,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )
        edit_url = reverse("benevole-client-edit", args=[cl.pk])
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(edit_url)
        self.assertContains(response, "Questionnaire pour signature")
        # S'il y a déjà des fichiers quest/contrat, la section n'est plus dispo
        Fichier.objects.create(
            content_object=cl.persona, titre="Quest", typ=TypeFichier.QUEST_ALARME, quand=now(),
            fichier=ContentFile("abc", name="quest.text")
        )
        Fichier.objects.create(
            content_object=cl.persona, titre="Quest", typ=TypeFichier.CONTRAT_ALARME, quand=now(),
            fichier=ContentFile("def", name="contrat.text")
        )
        response = self.client.get(edit_url)
        self.assertNotContains(response, "Questionnaire pour signature")

    def test_benevole_attribution(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation'),
            delai=today + timedelta(days=10),
        )
        autre_benev = Benevole.objects.create(
            nom="Duschnock", prenom="Jules", courriel='ju@example.org',
            utilisateur=Utilisateur.objects.create_user(
                'ju@example.org', 'jupassword', first_name='Ju', last_name='Ju'
            ),
            activites=[TypeActivite.INSTALLATION],
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "L’intervention chez Schmid Léna vous a bien été attribuée, merci !"
        )
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "Cette intervention vous a déjà été attribuée."
        )
        interv.intervenant = autre_benev.utilisateur
        interv.save()
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "Désolé, cette intervention vient d’être attribuée à une autre personne."
        )

    @expectedFailure
    @override_settings(TEST_INSTANCE=False)
    def test_benevole_mission_planifiee(self):
        """
        Quand la date de planification est fixée par un bénévole, un message
        avec les infos client et alarme est envoyé à la centrale.
        """
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), benevole=self.benev,
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', no_serie='10131839', carte_sim='+467191111111111',
            chez_personne=self.benev.utilisateur,
        )
        self.client.force_login(self.benev.utilisateur)
        date_planif = today + timedelta(days=4)
        post_data = {
            'alarme': alarme1.pk,
            'planifiee_0': date_planif.strftime('%d.%m.%Y'),
            'planifiee_1': '14:30',
            'effectuee': '',
            'duree_client': '',
            'duree_seul': '',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        url = reverse('benevole-mission-edit', args=[interv.pk])
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(url, data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(mail.outbox[0].recipients(), [settings.CENTRALE_EMAIL])
        self.assertEqual(mail.outbox[0].subject, f"[Croix-Rouge {canton_abrev()}] Réservation d'installation")
        self.assertIn("Appareil: Neat NOVO 4G, SIM +467191111111111, SN 10131839", mail.outbox[0].body)
        self.assertIn(f"Installation planifiée le: {date_planif.strftime('%d.%m.%Y')} 14:30", mail.outbox[0].body)
        self.assertJournalMessage(
            interv.client, "Message d'intervention planifiée envoyé à la centrale", user=self.benev.utilisateur
        )

    def test_benevole_mission_install(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), intervenant=self.benev.utilisateur,
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', chez_personne=interv.intervenant
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-mission-edit', args=[interv.pk]))
        self.assertEqual(response.status_code, 200)
        # Si l'abonnement n'est pas encore défini, il apparaît dans le formulaire
        self.assertContains(response, 'id="id_abonnement"')
        post_data = {
            'alarme': alarme1.pk,
            'effectuee': today,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.post(url, data=post_data)
        self.assertEqual(
            response.context['form'].errors, {
                'abonnement': ['Vous devez indiquer l’abonnement choisi.'],
            }
        )
        post_data.update({'km': '25', 'abonnement': self.abo.pk})
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(interv.client.alertes.count(), 1)
        self.assertEqual(interv.client.alertes.first().alerte, "Intervention effectuée: Installation")

    def test_benevole_mission_install_abo_defini(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), intervenant=self.benev.utilisateur,
            abonnement=self.abo
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', chez_personne=interv.intervenant
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-mission-edit', args=[interv.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'id="id_abonnement"')
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.post(url, data={
            'alarme': alarme1.pk,
            'effectuee': today,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '0',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')

    def test_benevole_mission_change(self):
        install = self.create_client_with_installations(1)[0]
        client = install.client
        interv = Mission.objects.create(
            client=client,
            type_mission=TypeMission.objects.create(nom='Changement appareil', code='CHANGE'),
            delai=today + timedelta(days=10),
            intervenant=self.benev.utilisateur,
            abonnement=self.abo,
        )
        mauvaise_alarme = Alarme.objects.create(
            modele=ModeleAlarme.objects.create(nom='Modèle spécial'),
            no_serie=555, chez_personne=self.benev.utilisateur,
        )
        nouvelle_alarme = Alarme.objects.create(
            modele=self.modele, no_serie=1999, chez_personne=self.benev.utilisateur,
        )
        url = reverse('benevole-mission-edit', args=[interv.pk])
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(url)
        # Abo non éditable, car déjà défini par admin.
        self.assertContains(response, "Abonnement: standard")
        post_data = {
            'type_mission': interv.type_mission.pk,
            'delai': interv.delai,
            'alarme_select': 'Novo 4G',
            'alarme': mauvaise_alarme.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'benevole': self.benev.pk,
            'duree_seul': '00:00',
            'duree_client': '01:00',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(url, data=post_data)
        # Erreur si l'abo n'est pas compatible avec l'appareil
        self.assertFormError(
            response.context['form'], 'alarme',
            ['L’appareil Modèle spécial (555) n’est pas compatible avec l’abonnement standard']
        )

        post_data['alarme'] = nouvelle_alarme.pk,
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        client.refresh_from_db()
        new_install = client.alarme_actuelle()
        self.assertEqual(new_install.abonnement, self.abo)
        self.assertEqual(client.alertes.count(), 1)
        self.assertEqual(client.alertes.first().alerte, "Intervention effectuée: Changement appareil")

    def test_benevole_mission_duree_obligatoire(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), intervenant=self.benev.utilisateur,
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', chez_personne=interv.intervenant
        )
        form_data = {
            'type_mission': interv.type_mission.pk,
            'delai': interv.delai,
            'alarme_select': 'Novo 4G',
            'alarme': alarme1.pk,
            'abonnement': self.abo.pk,
            'effectuee': today,
            'duree_seul': '',
            'duree_client': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        form = BenevoleMissionForm(data=form_data, instance=interv)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            "duree_seul": ["Vous devez compléter ce champ"],
            "duree_client": ["Vous devez compléter ce champ"],
        })
        form_data["duree_seul"] = "00:00"
        form_data["duree_client"] = "00:00"
        form = BenevoleMissionForm(data=form_data, instance=interv)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            "duree_client": ["La durée avec le client ne peut pas être à 0"],
        })

    def test_non_benevole_mission_change(self):
        """
        Les intervenants non bénévoles ne saisissent jamais les durées, km et frais. (#455)
        """
        user_non_benev = self.intervenant
        install = self.create_client_with_installations(1)[0]
        client = install.client
        interv = Mission.objects.create(
            client=client,
            type_mission=TypeMission.objects.create(nom='Changement appareil', code='CHANGE'),
            delai=today + timedelta(days=10),
            intervenant=user_non_benev,
            abonnement=self.abo,
        )
        url = reverse('benevole-mission-edit', args=[interv.pk])
        self.client.force_login(user_non_benev)
        response = self.client.get(url)
        self.assertEqual(
            list(response.context['form'].fields.keys()),
            ['planifiee', 'effectuee', 'rapport', 'alarme']
        )
        self.assertIsNone(response.context['form'].formset)

    def test_mission_archive_edition(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today, effectuee=today.replace(day=1),
            intervenant=self.benev.utilisateur, abonnement=self.abo, km=6
        )
        self.client.force_login(self.benev.utilisateur)
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Permissions insuffisantes")
        self.assertEqual(
            list(response.context['form'].fields.keys()),
            ['planifiee', 'effectuee', 'rapport', 'duree_client', 'duree_seul', 'km']
        )
        self.assertIsNotNone(response.context['form'].formset)
        response = self.client.post(url, data={
            'effectuee': interv.effectuee,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '8',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')
        interv.refresh_from_db()
        self.assertEqual(interv.km, 8)

    def test_benevole_nouvelle_visite(self):
        TypeMission.objects.create(nom='Visite', code='VISITE')
        # Client sans installation
        Client.objects.create(
            nom='Schmid', prenom='Léna', service=ALARME, visiteur=self.benev.utilisateur
        )
        install, install_resil, install_resil_passee = self.create_client_with_installations(
            3, client_data=[{'visiteur': self.benev.utilisateur}] * 3
        )
        # Client avec résiliation en cours
        install_resil.date_fin_abo = date.today() + timedelta(days=4)
        install_resil.save()
        install_resil_passee.date_fin_abo = date.today() - timedelta(days=4)
        install_resil_passee.save()
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        self.assertEqual(response.context['visites'], [install.client, install_resil.client])
        self.assertEqual(response.context['visites_a_faire'], 1)
        self.assertContains(response, "Résiliation pour le")
        response = self.client.post(
            reverse('benevole-visite-new', args=[install_resil.client.pk]),
            {
                'planifiee_0': date.today() + timedelta(days=10),
                'planifiee_1': '13:45',
            }
        )
        self.assertEqual(response.json()['result'], 'OK')
        interv_visite = install_resil.client.mission_set.first()
        self.assertIsNotNone(interv_visite.planifiee)
        self.assertEqual(interv_visite.intervenant, self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        # L'appareil est affiché
        self.assertContains(response, '<div class="col-8">Neat NOVO 4G (-) (standard)</div>', html=True)

    def test_benevole_nouvelle_visite_effectuee(self):
        """Création d'une visite et marquer immédiatement comme effectuée."""
        TypeMission.objects.create(nom='Visite', code='VISITE')
        client = Client.objects.create(
            nom="Schmid", prenom="Léna", service=ALARME, visiteur=self.benev.utilisateur
        )
        create_url = reverse("benevole-visite-new", args=[client.pk])
        self.client.force_login(self.benev.utilisateur)
        post_data = {
            "planifiee_0": date.today() - timedelta(days=1),
            "planifiee_1": "13:45",
            "effectuee": date.today() - timedelta(days=1),
        }
        response = self.client.post(create_url, data=post_data)
        self.assertContains(response, "La durée avec le client ne peut pas être à 0")
        post_data["duree_client"] = "01:45"
        response = self.client.post(create_url, data=post_data)
        self.assertEqual(response.json()["result"], "OK")
        alerte = client.alertes.first()
        self.assertEqual(alerte.alerte, "Intervention effectuée: Visite")

    def test_benevole_note_frais(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna', service=ALARME)
        interv = Mission.objects.create(
            client=cl,
            intervenant=self.benev.utilisateur,
            delai=date.today(),
            type_mission=self.typem,
            effectuee=date.today().replace(day=1) - timedelta(days=4),
            km=Decimal(9),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-notesfrais', args=['alarme', self.benev.pk]))
        self.assertEqual(len(response.context['object_list']), 1)

    def test_benevole_missions(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna', service=ALARME)
        interv = Mission.objects.create(
            client=cl,
            intervenant=self.benev.utilisateur,
            delai=date.today(),
            type_mission=self.typem,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('intervenant-interventions', args=[self.benev.utilisateur_id]))
        self.assertQuerySetEqual(response.context['object_list'], [interv])

    def test_benevole_mission_sans_client_par_admin(self):
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-intervention-new', args=[self.benev.utilisateur.pk]), data={
            'type_mission': typem.pk,
            'description': "Réparation urgente",
            'effectuee': date.today(),
            'duree_seul': '1:30',
            'km': '6',
            'frais-TOTAL_FORMS': 1,
            'frais-INITIAL_FORMS': 0,
            'frais-0-id': '',
            'frais-0-mission': '',
            'frais-0-descriptif': 'Achat fil',
            'frais-0-cout': '12.60',
            'frais-0-justif': '',
        })
        self.assertEqual(response.json()['result'], 'OK')
        mission = Mission.objects.get(description="Réparation urgente")
        self.assertEqual(mission.intervenant, self.benev.utilisateur)

    def test_benevole_mission_sans_client_par_benevole(self):
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)
        self.client.force_login(self.benev.utilisateur)
        response = self.client.post(reverse('benevole-mission-new'), data={
            'type_mission': typem.pk,
            'description': "Réparation urgente",
            'effectuee': '',
            'duree_seul': '',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')
        mission = Mission.objects.get(description="Réparation urgente")
        self.assertEqual(mission.intervenant, self.benev.utilisateur)
        # Edition
        url = reverse('benevole-mission-edit', args=[mission.pk])
        response = self.client.get(url)
        self.assertNotIn('duree_client', response.context['form'].fields)
        response = self.client.post(url, data={
            'planifiee_0': '',
            'planifiee_1': '',
            'effectuee': date.today(),
            'duree_seul': '1:30',
            'frais': '0',
            'km': '8',
            'frais-TOTAL_FORMS': 1,
            'frais-INITIAL_FORMS': 0,
            'frais-0-id': '',
            'frais-0-mission': mission.pk,
            'frais-0-descriptif': 'Achat fil',
            'frais-0-cout': '12.80',
            'frais-0-justif': '',
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(mission.frais.first().cout, Decimal('12.80'))

    def test_benevole_stock(self):
        """Onglet 'Stock' de la fiche Bénévole."""
        Alarme.objects.create(modele=self.modele, no_serie='111111')
        alarme_stock = Alarme.objects.create(
            modele=self.modele, no_serie='111222', chez_personne=self.benev.utilisateur
        )
        alarme_stock2 = Alarme.objects.create(
            modele=self.modele, no_serie='111333', chez_personne=self.benev.utilisateur
        )
        # Un des 2 appareils est prévu dans une autre install.
        client = Client.objects.create(nom='Donzé', prenom='Léa', service=ALARME)
        Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem,
            alarme=alarme_stock2
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('intervenant-stock', args=[self.benev.utilisateur_id]))
        self.assertQuerySetEqual(response.context['appareils'], [alarme_stock, alarme_stock2])

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-alarme-search') + '?q=111')
        self.assertEqual(response.json(), [{'label': 'Neat NOVO 4G (111222)', 'value': alarme_stock.pk}])

        client2 = Client.objects.create(nom='Tell', prenom='Guillaume', service=ALARME)
        mission = Mission.objects.create(
            client=client2,
            intervenant=self.benev.utilisateur,
            delai=today + timedelta(days=10),
            type_mission=self.typem,
            alarme=None
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-mission-edit', args=[mission.pk]))
        self.assertQuerySetEqual(
            response.context['form'].fields['alarme'].queryset.all(),
            [alarme_stock]
        )
        mission.alarme = alarme_stock
        mission.save()
        response = self.client.get(reverse('benevole-mission-edit', args=[mission.pk]))
        self.assertQuerySetEqual(
            response.context['form'].fields['alarme'].queryset.all(),
            [alarme_stock]
        )

    def test_benevole_visites(self):
        """Onglet 'Visites' de la fiche Bénévole."""
        client = Client.objects.create(
            nom='Schmid', prenom='Léa', rue="Fleurs 2", npa="2800", localite="Delémont",
            service=ALARME, visiteur=self.benev.utilisateur,
        )
        dans_2_jours = now() + timedelta(days=2)
        Mission.objects.create(
            client=client, intervenant=self.benev.utilisateur, type_mission=self.typem,
            delai=dans_2_jours.date(), planifiee=dans_2_jours, effectuee=None,
        )
        self.assertTrue(client.can_edit(self.benev.utilisateur))
        self.client.force_login(self.user)
        self.nouvelle_install_par_mission(client=client)
        # Pas encore d'install, ne doit pas apparaître
        Client.objects.create(
            nom='Schmid', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont",
            service=ALARME, visiteur=self.benev.utilisateur,
        )
        # Archivé, ne doit pas apparaître
        Client.objects.create(
            nom='Marchand', prenom='Dage', rue="Grand-Rue 2",
            npa="2000", localite="Neuchâtel",
            visiteur=self.benev.utilisateur, archive_le=date.today()
        )
        response = self.client.get(reverse('intervenant-visites', args=[self.benev.utilisateur.pk]))
        self.assertQuerySetEqual(response.context['a_visiter'], [client])
        self.assertContains(
            response,
            f"Prochaine : planifiée le {dans_2_jours.strftime('%d.%m.%Y')} (Nouvelle installation)"
        )

    def test_archives_mois(self):
        time_in_last_month = now().replace(day=1, hour=10, minute=0) - timedelta(days=15)
        client = Client.objects.create(nom='Schmid', prenom='Léna')
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)

        def create_mission(effectuee=time_in_last_month, **kwargs):
            return Mission.objects.create(
                client=client, type_mission=typem, delai=None, effectuee=effectuee,
                intervenant=self.benev.utilisateur, duree_client=timedelta(hours=1), km=6
            )

        # Ne doivent pas apparaître:
        create_mission(effectuee=time_in_last_month - timedelta(days=20))  # trop tôt
        create_mission(effectuee=None)  # pas terminée
        # Doivent apparaître
        m1 = create_mission()
        m2 = create_mission()
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(
            reverse('app-archives-month', args=[time_in_last_month.year, time_in_last_month.month])
        )
        self.assertEqual(response.context['days'], {time_in_last_month.date(): [m1, m2]})
        self.assertEqual(response.context['sums'], {
            'total_missions': 2, 'total_km': 12, 'total_frais': Decimal('4.50'),
            'total_duree_client': timedelta(hours=2), 'total_duree_seul': None,
        })

    def test_benevole_fin_activite(self):
        client = Client.objects.create(
            nom='Schmid', prenom='Léa', rue="Fleurs 2", npa="2800", localite="Delémont",
            service=ALARME, visiteur=self.benev.utilisateur,
        )
        form_data = {
            'nom': self.benev.nom,
            'prenom': self.benev.prenom,
            'genre': self.benev.genre,
            'npalocalite': '',
            'langues': ['fr'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '1',
            'activite_set-0-id': self.benev.activite_set.first().pk,
            'activite_set-0-activite': TypeActivite.INSTALLATION,
            'activite_set-0-duree_0': (date.today() - timedelta(days=60)).strftime('%d.%m.%Y'),
            'activite_set-0-duree_1': date.today().strftime('%d.%m.%Y'),
            'telephone_set-TOTAL_FORMS': '0',
            'telephone_set-INITIAL_FORMS': '0',
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-edit', args=['alarme', self.benev.pk]), data=form_data)
        self.assertRedirects(response, reverse('intervenants'))
        client.refresh_from_db()
        self.assertIsNone(client.visiteur)

    def test_acces_benevole_quest_besoins(self):
        client = Client.objects.create(
            nom="Doe", prenom="Jill", date_naissance="1950-05-03", service=Services.ALARME,
        )
        Mission.objects.create(
            client=client, intervenant=self.benev.utilisateur, type_mission=self.typem, delai=date.today(),
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'), follow=True)
        self.assertNotContains(response, "Analyse des besoins")
        self.benev.formations.add(Formation.objects.create(
            titre="Formation analyse",
            categorie=TypeFormation.objects.create(nom="Formation besoins", code="besoins"),
            quand=date.today()
        ))
        response = self.client.get(reverse('home-app'), follow=True)
        self.assertContains(response, "Analyse des besoins")
        response = self.client.get(reverse("besoins:q-intro", args=[client.pk]))
        self.assertContains(response, "Canevas de dialogue introductif")
