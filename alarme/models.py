from collections import defaultdict
from datetime import date, timedelta
from decimal import Decimal

from django.apps import apps
from django.conf import settings
from django.contrib.postgres.fields import DateRangeField
from django.db import models
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import Count, F, Max, Min, Prefetch, OuterRef, Q, Subquery, Sum, Value
from django.db.models.functions import TruncMonth
from django.urls import reverse
from django.utils.dateformat import format as django_format

from benevole.models import Benevole, CalculateurFraisBase, LigneFrais, TypeActivite, TypeFrais
from client.models import Client
from common.models import Utilisateur
from common.utils import canton_app, date_range_overlap, last_day_of_month, same_month


class NonFacturableError(Exception):
    pass


class ArticleFacture(models.Model):
    code = models.CharField(max_length=25, unique=True)
    designation = models.CharField("Désignation", max_length=100)
    prix = models.DecimalField("Montant", max_digits=7, decimal_places=2, blank=True, null=True)
    compte = models.CharField("Compte de débit", max_length=25, blank=True)

    def __str__(self):
        code_prix = f"{self.code}, {self.prix}" if self.prix else self.code
        return f'{self.designation} ({code_prix})'


class ModeleAlarme(models.Model):
    nom = models.CharField(max_length=100, unique=True)
    article_achat = models.ForeignKey(
        ArticleFacture, on_delete=models.PROTECT, blank=True, null=True,
        related_name="modeles_achat", verbose_name="Article pour achat",
        help_text=(
            "Si un article d’achat est défini, les appareils de ce modèle sont "
            "automatiquement facturés au client"
        ),
    )
    article_install = models.ForeignKey(
        ArticleFacture, on_delete=models.PROTECT, blank=True, null=True,
        related_name="modeles_install", verbose_name="Article pour installation",
    )
    abos = models.ManyToManyField('TypeAbo', blank=True)
    archive = models.BooleanField("Archivé", default=False)

    def __str__(self):
        return self.nom


class AlarmeQuerySet(models.QuerySet):
    def installees(self):
        return self.annotate(
            current_installs=models.Count(
                'installation',
                filter=Q(installation__date_fin_abo__isnull=True) | Q(installation__retour_mat__isnull=True)
            )
        ).filter(current_installs__gt=0)

    def non_installees(self):
        return self.annotate(
            current_installs=models.Count(
                'installation',
                filter=Q(installation__date_fin_abo__isnull=True) | Q(installation__retour_mat__isnull=True)
            )
        ).filter(date_archive=None, current_installs=0)

    def non_reservees(self, allowed_pk=None):
        return self.annotate(
            reserve=Count('mission', filter=Q(mission__effectuee__isnull=True))
        ).exclude(Q(reserve__gt=0) & ~Q(pk=allowed_pk))

    def a_reviser(self):
        if not settings.APPAREIL_REVISION_ACTIF:
            return self.none()
        # Keep in sync with Alarme.a_reviser()
        return self.annotate(
            current_installs=models.Count(
                'installation',
                filter=Q(installation__date_fin_abo__isnull=True) | Q(installation__retour_mat__isnull=True)
            ),
            last_retour=models.Max('installation__retour_mat')
        ).filter(
            Q(date_archive=None, current_installs=0, last_retour__isnull=False, en_rep_depuis__isnull=True) &
            Q(Q(date_revision__isnull=True) | Q(last_retour__gt=F('date_revision')))
        )


class Alarme(models.Model):
    CENTRALE_CHOICES = (
        ('curena', "Curena"),
        ('medicall', "Medicall"),
    )
    modele = models.ForeignKey(ModeleAlarme, on_delete=models.PROTECT, verbose_name='Modèle')
    no_centrale = models.PositiveIntegerField("N° appareil à la centrale", null=True, blank=True)
    no_appareil = models.CharField("N° d’appareil (BU, …)", max_length=30, blank=True)
    no_serie = models.CharField("N° de série (SN)", max_length=30, blank=True)
    carte_sim = models.CharField("Carte SIM", max_length=30, blank=True)
    centrale = models.CharField("Centrale d’alarme", max_length=10, blank=True, choices=CENTRALE_CHOICES)
    date_achat = models.DateField("Date d’achat", blank=True, null=True)
    prix_achat = models.DecimalField("Prix d’achat", blank=True, null=True, max_digits=7, decimal_places=2)
    fournisseur = models.CharField("Fournisseur", max_length=80, blank=True)
    chez_personne = models.ForeignKey(
        Utilisateur, blank=True, null=True, on_delete=models.SET_NULL, verbose_name="Chez la personne"
    )
    en_rep_depuis = models.DateField("Envoyé en réparation le", null=True, blank=True)
    date_revision = models.DateField("Révision le", null=True, blank=True)
    date_batterie = models.DateField("Dernier changement de batterie", null=True, blank=True)
    date_archive = models.DateField("Archivé le", null=True, blank=True)
    remarques = models.TextField(blank=True)

    objects = AlarmeQuerySet.as_manager()

    def __str__(self):
        return f'{self.modele} ({self.no_serie or "-"})'

    def get_absolute_url(self):
        return reverse('alarme-edit', args=[self.pk])

    def abo_compatible(self, abo):
        return abo in self.modele.abos.all()

    def install_actuelle(self):
        if not hasattr(self, 'installations'):
            self.installations = self.installation_set.all()
        try:
            return [
                inst for inst in self.installations if inst.actuelle()
            ][0]
        except IndexError:
            return None

    def a_reviser(self):
        if not settings.APPAREIL_REVISION_ACTIF:
            return False
        # Keep in sync with objects.a_reviser()
        installations = getattr(self, 'installations', self.installation_set.all())  # Possible prefetch
        try:
            last_retour = sorted([inst.retour_mat for inst in installations if inst.retour_mat])[-1]
        except IndexError:
            last_retour = None
        return (
            self.date_archive is None and self.install_actuelle() is None and
            last_retour is not None and self.en_rep_depuis is None
        ) and (
            self.date_revision is None or last_retour > self.date_revision
        )


class JournalAlarme(models.Model):
    alarme = models.ForeignKey(Alarme, on_delete=models.CASCADE, verbose_name='Alarme', related_name='journaux')
    description = models.TextField()
    quand = models.DateTimeField()
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        get_latest_by = "quand"

    def __str__(self):
        return self.description


class TypeAbo(models.Model):
    nom = models.CharField(max_length=100, unique=True)
    article = models.ForeignKey(ArticleFacture, on_delete=models.PROTECT, verbose_name="Article")

    def __str__(self):
        return self.nom


class FacturationPolicyBase:
    # Pas de facturation si l'abo démarre après ce jour du mois
    JOUR_MOIS_LIMITE_FACTURATION = 20

    def calculer_montant_abo(self, mois, installs):
        debut = min(inst.date_debut for inst in installs)
        if same_month(mois, debut) and debut.day >= self.JOUR_MOIS_LIMITE_FACTURATION:
            raise NonFacturableError("Pas de facturation pour les abos commençant le 20 ou plus tard.")
        # Mois entamé est facturé
        return installs[-1].abonnement.article.prix

    def get_article_facture(self, install):
        if isinstance(install, Installation):
            return install.alarme.modele.article_install
        if isinstance(install, MaterielClient):
            return install.type_mat.article_install

    def get_client_et_suffixe(self, client):
        """
        Si client est un partenaire secondaire, adresser factures au partenaire principal et ajouter suffixe à son nom.
        Si client est un partenaire principal, rajouter un suffixe avec son nom à tous les libellés de facture.
        Sinon (cas habituel), renvoyer client tel quel sans suffixe.
        """
        if partenaire_principal := client.get_partenaire_de():
            nom_client_sfx = f" - {client.nom_prenom}"
            client = partenaire_principal
        elif client.partenaire_id:
            nom_client_sfx = f" - {client.nom_prenom}"
        else:
            nom_client_sfx = ""
        return client, nom_client_sfx

    def has_facture_installation(self, install):
        return install.factures.filter(
            article=install.alarme.modele.article_install
        ).exists()

    def creer_facture_installation(self, install, date_factures):
        """Facturer les frais d'installation"""
        factures = []
        client, nom_client_sfx = self.get_client_et_suffixe(install.client)
        # Si appareil à l'achat, facturer
        if isinstance(install, Installation) and install.alarme.modele.article_achat:
            article = install.alarme.modele.article_achat
            fact = Facture.objects.create(
                client=client,
                date_facture=date_factures, mois_facture=install.date_debut,
                article=article,
                install=install,
                libelle=article.designation + nom_client_sfx,
                montant=article.prix,
                exporte=None
            )
            factures.append(fact)
        # Facture d'installation
        art_install = self.get_article_facture(install)
        if art_install:
            fact = Facture.objects.create(
                client=client,
                date_facture=date_factures, mois_facture=install.date_debut,
                article=art_install,
                install=install if isinstance(install, Installation) else None,
                materiel=install if isinstance(install, MaterielClient) else None,
                libelle=f"{art_install.designation} ({install.date_debut.strftime('%d.%m.%Y')}){nom_client_sfx}",
                montant=art_install.prix,
                exporte=None
            )
            factures.append(fact)
        return factures

    def creer_facture_mensuelle(self, mois, installs, date_factures):
        # Exclure les installations "tardives" du mois
        installs = [
            inst for inst in installs
            if inst.date_debut <= mois.replace(day=self.JOUR_MOIS_LIMITE_FACTURATION)
        ]
        if not installs:
            return []
        try:
            montant = self.calculer_montant_abo(mois, installs)
        except NonFacturableError:
            return []
        install = installs[-1]
        client, nom_client_sfx = self.get_client_et_suffixe(install.client)
        # Si une facture existe déjà pour ce mois, ce client et cet article, ignorer
        if client.has_facture_for_month(mois, install.abonnement.article):
            return []

        pour_materiel = isinstance(install, MaterielClient)
        un_du_mois = mois.replace(day=1)
        factures = []
        if pour_materiel:
            libelle = f'{install.abonnement.nom} - %s{nom_client_sfx}'
        else:
            libelle = f'{install.abonnement.article.designation} - %s{nom_client_sfx}'
            libelle = libelle.replace('abonnement mensuel', 'abonnement')
        nom_mois = django_format(mois, "F Y")
        fact = Facture.objects.create(
            client=client, mois_facture=un_du_mois, date_facture=max(date_factures, un_du_mois),
            article=install.abonnement.article,
            install=install if not pour_materiel else None,
            materiel=install if pour_materiel else None,
            libelle=libelle % nom_mois,
            montant=montant, exporte=None
        )
        factures.append(fact)
        # Éventuel rabais automatique
        if not pour_materiel:
            rabais = client.rabaisauto_set.filter(duree__contains=un_du_mois).first()
            if rabais:
                fact_rabais = Facture.objects.create(
                    client=client, mois_facture=un_du_mois, date_facture=max(date_factures, un_du_mois),
                    article=rabais.article,
                    install=install if not pour_materiel else None,
                    materiel=install if pour_materiel else None,
                    libelle=rabais.article.designation + nom_client_sfx,
                    montant=None, exporte=None
                )
                factures.append(fact_rabais)
        # Si samaritains comme répondants, facturer service
        if (
            not pour_materiel and client.samaritains_des and
            date_range_overlap(client.samaritains_des, DateRange(un_du_mois, un_du_mois + timedelta(days=20)))
        ):
            try:
                art_samas = ArticleFacture.objects.get(code=settings.CODE_ARTICLE_ABO_SAMA)
            except ArticleFacture.DoesNotExist:
                pass
            else:
                if not client.has_facture_for_month(mois, art_samas):
                    for i in range(2 if client.partenaire_id else 1):
                        sfx = nom_client_sfx if i == 0 else f" - {client.partenaire.nom_prenom}"
                        fact = Facture.objects.create(
                            client=client, mois_facture=un_du_mois, date_facture=date_factures,
                            article=art_samas,
                            install=install,
                            materiel=None,
                            libelle=f'Répondants samaritains - {nom_mois}{sfx}',
                            montant=art_samas.prix,
                            exporte=None
                        )
                        factures.append(fact)
        return factures


class CalculateurFraisAlarme(CalculateurFraisBase):
    """
    Calculateur de frais bénévoles à partir des interventions.
    Cette classe est destinée à être surchargée dans le fichier alarme_policies.py cantonal.
    """
    service = 'alarme'
    TARIF_KM_ALARME = None
    TARIF_VISITES_DEFRAYEES = 0

    def frais_par_benevole(self, benevole=None):
        # Pour l'instant, seuls les Missions effectuées par des bénévoles sont prises en comptes
        if benevole:
            base_qs = Mission.objects.filter(intervenant__benevole=benevole)
            benevoles = {benevole.pk: benevole}
        else:
            base_qs = Mission.objects.filter(intervenant__benevole__isnull=False)
            benevoles = {benev.pk: benev for benev in Benevole.objects.filter(
                pk__in=base_qs.values_list('intervenant__benevole', flat=True)
            )}

        query = base_qs.annotate(
            month=TruncMonth('effectuee'),
            benevole__id=F('intervenant__benevole__id'),
            benevole__nom=F('intervenant__benevole__persona__nom'),
            benevole__prenom=F('intervenant__benevole__persona__prenom'),
            frais_repas=Subquery(
                Frais.objects.filter(mission_id=OuterRef('pk'), typ='repas').values(
                    total_frais=Sum('cout')
                )[:1]
            ),
            frais_divers=Subquery(
                Frais.objects.filter(mission_id=OuterRef('pk')).exclude(typ='repas').values(
                    total_frais=Sum('cout')
                )[:1]
            ),
        ).filter(
            month=self.mois
        ).values(
            'benevole__id', 'benevole__nom', 'benevole__prenom',
        ).annotate(
            mois=Value(self.mois),
            kms=Sum('km', default=Decimal('0.0')),
            num_visites=Count('id', filter=Q(type_mission__avec_client=True)),
            frais_repas=Sum('frais_repas', default=Decimal(0)),
            frais_divers=Sum('frais_divers', default=Decimal(0)),
        ).order_by('benevole__nom', 'benevole__prenom')
        return {benevoles[line['benevole__id']]: line for line in query}

    def lignes_depuis_data(self, benevole, data):
        type_map = canton_app.TYPE_FRAIS_MAP
        lignes = []
        # Frais repas et divers
        if data.get('frais_repas', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['repas']),
                quantite=Decimal('1'), montant_unit=data['frais_repas']
            ))
        if data.get('frais_divers', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['divers']),
                quantite=Decimal('1'), montant_unit=data['frais_divers']
            ))
        if data['kms'] > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['kms']),
                quantite=data['kms'], montant_unit=self.TARIF_KM_ALARME,
            ))
        return lignes


class InstallBase(models.Model):
    FIN_CHOICES = (
        ('décès', 'Décès'),
        ('ems', 'Départ en EMS'),
        ('plus_utile', 'Plus d’utilité'),
        ('autre', 'Autre motif (voir remarques)'),
    )
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    abonnement = models.ForeignKey(TypeAbo, on_delete=models.PROTECT, blank=True, null=True)
    date_debut = models.DateField("Date d’installation")
    date_fin_abo = models.DateField("Fin d’abonnement le", blank=True, null=True)
    motif_fin = models.CharField("Motif de résiliation", max_length=20, choices=FIN_CHOICES, blank=True)
    retour_mat = models.DateField("Date de retour du matériel", blank=True, null=True)
    remarques = models.TextField("Remarques", blank=True)

    class Meta:
        abstract = True

    def actuelle(self):
        today = date.today()
        return self.date_debut <= today and (self.date_fin_abo is None or self.date_fin_abo >= today)

    def duree_pour_mois(self, mois):
        start = max(mois.replace(day=1), self.date_debut)
        end = min(last_day_of_month(mois), self.date_fin_abo) if self.date_fin_abo else last_day_of_month(mois)
        return (end - start).days

    @classmethod
    def generer_factures(cls, mois: date, client: Client = None, date_factures: date = None) -> int:
        """
        Générer une facture pour chaque installation encore active le mois indiqué.
        mois: n'importe quel jour du mois à facturer.
        """
        if date_factures is None:
            date_factures = date.today()
        un_du_mois = mois.replace(day=1)
        un_du_mois_suivant = (mois.replace(day=28) + timedelta(days=10)).replace(day=1)
        query_installs = cls.objects.filter(
            abonnement__article__isnull=False, date_debut__isnull=False
        ).filter(
            # Obligé de prendre les anciennes installs pour déterminer le "vrai" début d'install.
            Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gte=(un_du_mois - timedelta(days=60)))
        ).prefetch_related(
            # Pas besoin de considérer les factures trop vieilles
            Prefetch('factures', queryset=Facture.objects.filter(date_facture__gte=un_du_mois - timedelta(days=90)))
        ).select_related('abonnement').order_by('client')
        if client is not None:
            query_installs = query_installs.filter(client=client)
        count = 0
        client_installs = defaultdict(list)
        # Grouper installations par client
        for install in query_installs:
            # Si date_de_debut > fin du mois (install future): ignorer
            if install.date_debut >= un_du_mois_suivant:
                continue
            client_installs[install.client].append(install)

        fact_policy = canton_app.fact_policy
        for client, installs in client_installs.items():
            date_install = min(inst.date_debut for inst in installs)
            # Éventuellement exclure ici les installs en trop du mois préc. (maintenant
            # que date_install est déterminée)
            installs = [inst for inst in installs if not inst.date_fin_abo or inst.date_fin_abo >= un_du_mois]
            if not installs:
                # Aucune install active pour ce mois de facturation
                continue
            install1 = installs[0]
            pour_materiel = isinstance(install1, MaterielClient)

            # Vérifier d'abord si frais d'install doivent être facturés
            if (
                not pour_materiel and
                (date_install >= un_du_mois or (date.today() - date_install).days <= 30) and
                not fact_policy.has_facture_installation(install1)
            ):
                factures = fact_policy.creer_facture_installation(install1, date_factures)
                count += len(factures)

            factures = fact_policy.creer_facture_mensuelle(mois, installs, date_factures)
            count += len(factures)
        return count


class Installation(InstallBase):
    alarme = models.ForeignKey(Alarme, on_delete=models.PROTECT)
    nouvelle = models.BooleanField(default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='client_current_unique', fields=['client'], condition=Q(date_fin_abo=None))
        ]

    def __str__(self):
        return f"Installation de {self.alarme} chez {self.client}"

    @classmethod
    def clients_en_cours(cls, base_qs, debut, fin):
        # Sous-requête pour obtenir l'installation la plus récente
        installs = Installation.objects.filter(
            client=OuterRef('pk')
        ).order_by('-date_debut')
        # Attention fin_install est signifiant que si en_cours=0
        return base_qs.annotate(
            debut_install=Max('installation__date_debut', filter=Q(installation__nouvelle=True)),
            fin_install=Max('installation__date_fin_abo'),
            en_cours=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
            modele=Subquery(installs.values('alarme__modele__nom')[:1]),
        ).filter(
            Q(debut_install__lte=fin) & (
                Q(en_cours__gt=0) | Q(fin_install__gte=debut)
            )
        )

    def save(self, is_change=False, **kwargs):
        # is_change=True: an alarme change on this install.
        self._previous_state = Installation.objects.get(pk=self.pk) if self.pk else None
        self._is_change = is_change
        super().save(**kwargs)


class TypeMateriel(models.Model):
    nom = models.CharField(max_length=120)
    article_achat = models.ForeignKey(
        ArticleFacture, on_delete=models.PROTECT, blank=True, null=True,
        related_name="mat_achat", verbose_name="Article pour achat",
        help_text="Si un article d’achat est défini, les appareils de ce modèle sont automatiquement facturés au client"
    )
    article_install = models.ForeignKey(
        ArticleFacture, on_delete=models.PROTECT, blank=True, null=True,
        related_name="mat_install", verbose_name="Article pour installation",
    )
    abos = models.ManyToManyField(
        'TypeAbo', blank=True, verbose_name="Types d’abonnements possibles pour ce type de matériel"
    )

    def __str__(self):
        return self.nom


class MaterielManager(models.Manager):
    def installes(self):
        return self.annotate(
            current_installs=models.Count(
                'materielclient',
                filter=Q(materielclient__date_fin_abo__isnull=True) | Q(materielclient__date_fin_abo__gt=date.today())
            )
        ).filter(current_installs__gt=0)

    def non_installes(self):
        return self.annotate(
            current_installs=models.Count(
                'materielclient',
                filter=Q(materielclient__date_fin_abo__isnull=True) | Q(materielclient__date_fin_abo__gt=date.today())
            )
        ).filter(date_archive=None, current_installs=0)


class Materiel(models.Model):
    type_mat = models.ForeignKey(TypeMateriel, on_delete=models.PROTECT, verbose_name="Type de matériel")
    no_ref = models.CharField("N° de référence", max_length=30, blank=True)
    no_centrale = models.PositiveIntegerField("N° matériel à la centrale", null=True, blank=True)
    date_achat = models.DateField("Date d’achat", blank=True, null=True)
    prix_achat = models.DecimalField("Prix d’achat", blank=True, null=True, max_digits=7, decimal_places=2)
    fournisseur = models.CharField("Fournisseur", max_length=100, blank=True)
    chez_personne = models.ForeignKey(
        Utilisateur, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Chez la personne"
    )
    date_archive = models.DateField("Archivé le", null=True, blank=True)

    objects = MaterielManager()

    def __str__(self):
        return f'{self.type_mat.nom} ({self.no_ref})'

    def get_absolute_url(self):
        return reverse('materiel-edit', args=[self.pk])

    def abo_compatible(self, abo):
        return abo in self.type_mat.abos.all()


class MaterielClient(InstallBase):
    materiel = models.ForeignKey(
        Materiel, on_delete=models.PROTECT, verbose_name="Matériel", null=True, blank=True
    )
    type_mat = models.ForeignKey(
        TypeMateriel, on_delete=models.PROTECT, verbose_name="Type de matériel", null=True, blank=True
    )

    def __str__(self):
        return f"{self.materiel or self.type_mat} installé chez {self.client}"

    @property
    def nom(self):
        return self.materiel.type_mat.nom if self.materiel_id else self.type_mat.nom


class Facture(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='factures')
    # Not sure we'll still use install/materiel (as facture can affect several installs)
    install = models.ForeignKey(Installation, blank=True, null=True, on_delete=models.SET_NULL, related_name='factures')
    materiel = models.ForeignKey(MaterielClient, blank=True, null=True, on_delete=models.SET_NULL, related_name='factures')
    mois_facture = models.DateField("Mois comptable", blank=True, null=True)
    date_facture = models.DateField(
        "Date de facturation", blank=True, null=True, help_text=(
            "Quand elle est laissée vide, la date de facturation est définie au moment "
            "de la prochaine facturation mensuelle"
        )
    )
    article = models.ForeignKey(ArticleFacture, on_delete=models.PROTECT, verbose_name="Article")
    # libelle et montant sont des copies statiques des mêmes champs d'article.
    libelle = models.CharField("Libellé", max_length=200)
    montant = models.DecimalField("Montant", max_digits=7, decimal_places=2, blank=True, null=True)
    rabais = models.DecimalField("Rabais", max_digits=7, decimal_places=2, blank=True, null=True)
    exporte = models.DateTimeField("Exporté vers compta", blank=True, null=True)
    export_err = models.TextField("Erreur d’exportation", blank=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        date_str = f' du {self.date_facture.strftime("%d.%m.%Y")}' if self.date_facture else ''
        val = f'Facture «{self.libelle}» {date_str} pour {self.client}'
        if self.montant is not None:
            val = val + f" (CHF {self.montant})"
        return val

    def adresse_facturation(self):
        return self.client.adresse_facturation(self)

    @property
    def autre_debiteur(self):
        adr = self.client.adresse_facturation(self)
        return None if adr is self.client else adr

    def match(self, referent):
        """Renvoie la priorité de prise en charge de la facture si le référent est débiteur."""
        if 'al-abo' in referent.facturation_pour and (
            'Abonnement' in self.article.designation or 'Installation' in self.article.designation
        ):
            return 2
        elif 'al-tout' in referent.facturation_pour:
            return 1
        return 0

    @classmethod
    def non_transmises(cls):
        non_transmises = cls.objects.filter(exporte__isnull=True)
        if apps.is_installed("cr_ne"):
            un_du_mois = date.today().replace(day=1)
            non_transmises = non_transmises.filter(mois_facture__lt=un_du_mois)
        return non_transmises


class RabaisAuto(models.Model):
    """Rabais automatique pour un client sur une certaine période (CRNE, #469)."""
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    duree = DateRangeField("Durée")
    article = models.ForeignKey(ArticleFacture, on_delete=models.PROTECT)

    def __str__(self):
        return f"Rabais pour {self.client}"


class TypeMission(models.Model):
    CODE_CHOICES = (
        ('NEW', 'Nouvelle installation'),
        ('CHANGE', 'Changement d’installation'),
        ('UNINSTALL', 'Suppression d’installation'),
        ('VISITE', 'Visite chez un client'),
    )
    nom = models.CharField(max_length=50)
    code = models.CharField(max_length=10, choices=CODE_CHOICES, blank=True)
    avec_client = models.BooleanField("Toujours liée avec un client", default=True)

    def __str__(self):
        return self.nom


class Mission(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True)
    type_mission = models.ForeignKey(TypeMission, on_delete=models.PROTECT, verbose_name="Type d’intervention")
    description = models.TextField(blank=True)
    delai = models.DateField("Délai", null=True)
    planifiee = models.DateTimeField("Planifiée le", blank=True, null=True)
    effectuee = models.DateField("Effectuée le", blank=True, null=True)

    # Seulement pour nouvelles installations (ou changement)
    alarme = models.ForeignKey(Alarme, on_delete=models.SET_NULL, blank=True, null=True)
    abonnement = models.ForeignKey(TypeAbo, on_delete=models.SET_NULL, blank=True, null=True)

    intervenant = models.ForeignKey(
        Utilisateur, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Intervenant"
    )
    rapport = models.TextField(blank=True)
    duree_client = models.DurationField("Durée (avec client)", blank=True, null=True)
    duree_seul = models.DurationField("Durée (sans client)", blank=True, null=True)
    km = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)

    def __str__(self):
        return ": ".join(txt for txt in [self.type_mission.nom, self.description] if txt)

    def can_edit(self, user):
        if user.has_perm('alarme.change_mission'):
            return True
        if user == self.intervenant:
            if date.today().day == 1:
                un_du_mois = (date.today() - timedelta(days=1)).replace(day=1)
            else:
                un_du_mois = date.today().replace(day=1)
            # Dès le 2 d'un mois, seules les missions du mois courant sont éditables.
            return not self.effectuee or self.effectuee >= un_du_mois
        return False

    def edit_url(self):
        if self.type_mission.avec_client:
            return reverse('mission-edit', args=[self.pk])
        else:
            return reverse('benevole-intervention-edit', args=[self.pk])


class Frais(models.Model):
    mission = models.ForeignKey(Mission, related_name="frais", on_delete=models.CASCADE)
    descriptif = models.TextField("Descriptif")
    cout = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Coût")
    typ = models.CharField("Type de frais", max_length=6, choices=[('repas', 'Repas'), ('autre', 'Autre')])
    justif = models.FileField(upload_to="justificatifs", blank=True, verbose_name="Justificatif")

    class Meta:
        verbose_name = "Frais"
        verbose_name_plural = "Frais"

    def __str__(self):
        return f"Frais ({self.cout}) pour l'intervention {self.mission}"

    def description(self):
        return self.descriptif or ("Repas" if self.typ == 'repas' else "")


def utilisateurs_alarme(include=None, temp_inactifs=False):
    acts = TypeActivite.par_domaine('alarme')
    act_filter = Q(activite__type_act__in=acts) & Q(activite__duree__contains=date.today())
    if not temp_inactifs:
        act_filter &= Q(activite__inactif=False)
    benevs_alarme = Benevole.objects.annotate(
        app_active=Count('activite', filter=act_filter)
    ).filter(app_active__gt=0)

    include_filter = Q(pk=include.pk if include else -1)
    users = Utilisateur.objects.exclude(is_superuser=True).filter(
        Q(is_active=True) | include_filter
    ).filter(
        Q(groups__name="Intervenants alarme") |
        Q(benevole__in=benevs_alarme) |
        include_filter
    ).distinct().order_by('last_name', 'first_name')
    return users
