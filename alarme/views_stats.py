import calendar
from datetime import date, timedelta
from decimal import Decimal

from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.backends.postgresql.psycopg_any import DateRange
from django.db.models import (
    Avg, Count, F, Max, Min, OuterRef, Prefetch, Q, Subquery, Sum
)
from django.db.models.functions import TruncMonth
from django.forms import BooleanField, CheckboxInput
from django.shortcuts import get_object_or_404
from django.utils.dates import MONTHS
from django.utils.text import slugify
from django.views.generic import TemplateView

from alarme.models import (
    ArticleFacture, Facture, Installation, Mission, ModeleAlarme, TypeMission
)
from client.models import Client, Referent
from common.choices import Services
from common.export import ExpLine
from common.functions import IsAVS
from common.stat_utils import DateLimitForm, Month, StatsMixin
from common.utils import date_range_overlap, last_day_of_month
from common.views import ListView
from .views import ClientListView


class DateLimitFormWithAVS(DateLimitForm):
    avs_only = BooleanField(
        label="Clients AVS uniquement", required=False,
        widget=CheckboxInput(attrs={'form': 'date_select_form', 'class': 'immediate_submit'}),
    )


class StatsInstallView(StatsMixin, TemplateView):
    template_name = 'stats/installations.html'
    stat_items = {
        'current': {
            'label': 'Installations en cours',
            'help': "Nombre d’installations qui ont été actives au moins un jour dans le mois concerné",
        },
        'samas': {'label': 'Abo samaritains', 'help': "Les clients avec partenaires comptent pour 2"},
        'interv_sama': {'label': 'Interventions samaritains'},
        'new': {'label': 'Nouvelles installations'},
        'end': {'label': 'Désinstallations'},
        'age': {'label': 'Âge moyen des bénéficiaires'},
    }

    def get_stats(self, months):
        self.clients = Installation.clients_en_cours(Client.objects, self.date_start, self.date_end)
        models = {
            m["nom"]: m
            for m in ModeleAlarme.objects.all().order_by('nom').values("pk", "nom", "archive")
        }
        counters = self.init_counters(self.stat_items.keys(), months)
        counter_models = self.init_counters(models.keys(), months)
        tous_clients = set()
        clients_sama = set()
        for client in self.clients:
            tous_clients.add(client.pk)
            for month in months:
                if month.is_future():
                    continue
                month_start, month_end = month.limits()
                if (
                    client.debut_install <= month_end and (
                        client.en_cours or client.fin_install >= month_start
                    )
                ):
                    counters['current'][month] += 1
                    counter_models[client.modele][month] += 1
                    if client.samaritains and date_range_overlap(
                        client.samaritains_des, DateRange(month_start, month_end)
                    ):
                        clients_sama.add(client.pk)
                        counters['samas'][month] += (2 if client.partenaire_id else 1)
                    if client.date_naissance:
                        counters['age'][month].append(date(month.year, month.month, 1) - client.date_naissance)
                if month == Month.from_date(client.debut_install):
                    counters['new'][month] += 1
                    counters['new']['total'] += 1
                if not client.en_cours and client.fin_install and month == Month.from_date(client.fin_install):
                    counters['end'][month] += 1
                    counters['end']['total'] += 1
        counters['current']['total'] = len(tous_clients)
        counters['samas']['total'] = len(clients_sama)
        for month in months:
            if month.is_future():
                continue
            num_ages = len(counters['age'][month])
            counters['age'][month] = (
                sum([c.days for c in counters['age'][month]]) / num_ages / 365.25
                if num_ages else '-'
            )
        if settings.CODE_ARTICLE_INTERV_SAMA:
            # On compte sur le fait que la date d’intervention se trouve dans mois_facture
            # (alors que date_facture change au moment d’envoyer la facture)
            facts = Facture.objects.filter(
                mois_facture__gte=self.date_start, mois_facture__lte=self.date_end,
                article__code=settings.CODE_ARTICLE_INTERV_SAMA
            )
            for fact in facts:
                counters['interv_sama'][Month(fact.mois_facture.year, fact.mois_facture.month)] += 1
                counters['interv_sama']['total'] += 1

        # Durée moyenne des installations (terminées)
        stats_duree_moyenne = Client.objects.annotate(
            num_installs=Count('installation'),
            debut_install=Min('installation__date_debut'),
            fin_install=Max('installation__date_fin_abo'),
            en_cours=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
        ).filter(
            num_installs__gt=0, en_cours=0, fin_install__range=(self.date_start, self.date_end)
        ).aggregate(
            duree_moyenne=Avg(F('fin_install') - F('debut_install')),
            nb_clients=Count('id'),
        )

        return {
            'labels': list(self.stat_items.values()),
            'stats': {key: counters[key] for key in self.stat_items.keys()},
            'stat_models': {
                key: (counter_models[key], models[key]["pk"])
                for key in counter_models if counter_models[key]["total"] > 0 or not models[key]["archive"]
            },
            'duree_moyenne': stats_duree_moyenne['duree_moyenne'],
            'show_samas': bool(settings.CODE_ARTICLE_INTERV_SAMA),
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['art_sama'] = ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA).pk
        except ArticleFacture.DoesNotExist:
            pass
        return context

    def export_data(self, export, context):
        super().export_data(export, context)
        export.add_sheet("Clients")
        export.write_line(ExpLine([
            'Nom', 'Prénom', 'Rue', 'NPA', 'Localité', 'Date de naiss.', 'Début install.', 'Fin install.'
        ], bold=True, col_widths=[20, 20, 30, 8, 20, 15, 15, 15]))
        for client in self.clients.avec_adresse(date.today()):
            adresse = client.adresse()
            export.write_line([
                client.noms, client.prenoms, adresse.rue, adresse.npa, adresse.localite,
                client.date_naissance, client.debut_install,
                client.fin_install if not client.en_cours else '',
            ])

    def export_lines(self, context):
        months = context['months']
        yield ExpLine(['Installations alarme'] + [str(month) for month in months] + ['Total'], bold=True)
        for key, data in self.stat_items.items():
            if key == 'samas':
                for subkey, subdata in context['stat_models'].items():
                    yield (
                        [f'  {subkey}'] +
                        [subdata[month] for month in months] +
                        [subdata['total']]
                    )
            yield (
                [data['label']] +
                [context['stats'][key][month] for month in months] +
                [context['stats'][key]['total']]
            )


class ClientsSamaView(ClientListView):
    """Liste des clients samaritains du mois."""
    template_name = 'alarme/client_list.html'
    base_template = 'alarme/base.html'

    def get_title(self):
        return f"Clients avec répondants samaritains en {MONTHS[self.kwargs['month']]} {self.kwargs['year']}"

    def get_queryset(self):
        month_start = date(self.kwargs['year'], self.kwargs['month'], 1)
        month_end = date(
            self.kwargs['year'], self.kwargs['month'], calendar.monthrange(self.kwargs['year'], self.kwargs['month'])[1]
        )
        return Client.objects.avec_adresse(date.today()).par_service(
            Services.ALARME, date_actif=DateRange(month_start, month_end)
        ).annotate(
            debut_first_install=Subquery(
                Installation.objects.filter(client=OuterRef('pk')).order_by('date_debut').values("date_debut")[:1]
            ),
            fin_last_install=Subquery(
                Installation.objects.filter(client=OuterRef('pk')).order_by('-date_debut').values("date_fin_abo")[:1]
            ),
        ).filter(
            Q(samaritains_des__startswith__lte=month_end) & Q(debut_first_install__lte=month_end) & (
                Q(fin_last_install__isnull=True) | Q(fin_last_install__gt=month_start)
            )
        ).order_by('persona__nom')


class ClientsIntervSamaView(PermissionRequiredMixin, ListView):
    """Liste des interventions samaritains du mois."""
    permission_required = "client.view_client"
    template_name = "alarme/factures.html"
    paginate_by = 25

    def get_queryset(self):
        month_start = date(self.kwargs["year"], self.kwargs["month"], 1)
        month_end = last_day_of_month(month_start)
        return Facture.objects.alias(
            mois=TruncMonth("mois_facture")
        ).filter(
            mois=month_start, article__code=settings.CODE_ARTICLE_INTERV_SAMA
        ).order_by("mois_facture")

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "title": f"Interventions samaritains en {MONTHS[self.kwargs['month']]} {self.kwargs['year']}",
        }


class InstallsByMonth(ClientListView):
    template_name = "alarme/client_list.html"
    filter_formclass = None
    base_template = "alarme/base.html"
    title = ""

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        self.this_month = date(self.kwargs["year"], self.kwargs["month"], 1)

    def get_title(self):
        return self.title.format(mois=MONTHS[self.kwargs["month"]], annee=self.kwargs["year"])

    @property
    def export_file_name(self):
        return slugify(self.get_title())


class CurrentInstallsView(InstallsByMonth):
    """Liste des installations actives ce mois."""
    title = "Clients avec installation en {mois} {annee}"

    def setup(self, *args, **kwargs):
        super().setup(*args, **kwargs)
        if self.kwargs["mod_pk"] > 0:
            self.modele = get_object_or_404(ModeleAlarme, pk=self.kwargs["mod_pk"])
        else:
            self.modele = None

    def get_title(self):
        return super().get_title() + (f" ({self.modele.nom})" if self.modele else "")

    def get_queryset(self):
        clients = Installation.clients_en_cours(
            Client.objects.avec_adresse(date.today()),
            self.this_month, last_day_of_month(self.this_month),
        ).order_by("persona__nom")
        if self.modele:
            clients = clients.filter(modele=self.modele.nom)
        return clients


class NewInstallsView(InstallsByMonth):
    """Liste des nouvelles installations du mois."""
    title = "Clients avec nouvelle installation en {mois} {annee}"

    def get_queryset(self):
        clients = Client.objects.avec_adresse(date.today()).annotate(
            debut_install=TruncMonth(Max("installation__date_debut", filter=Q(installation__nouvelle=True))),
            date_tout_debut=Min('installation__date_debut')
        ).filter(
            debut_install=self.this_month
        ).order_by('persona__nom')
        if self.export_flag:
            clients = clients.prefetch_related(
                Prefetch('referent_set', to_attr="debiteurs", queryset=Referent.objects.filter(
                    date_archive=None, facturation_pour__overlap=['al-abo', 'al-tout']
                ))
            )
            clients._hints['_export_autres_debiteurs'] = True
        return clients


class UninstallsView(InstallsByMonth):
    """Liste des désinstallations du mois."""
    title = "Clients avec désinstallation en {mois} {annee}"

    def get_queryset(self):
        return Client.objects.avec_adresse(date.today()).annotate(
            fin_install=TruncMonth(Max('installation__date_fin_abo')),
            en_cours=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
        ).filter(
            en_cours=0, fin_install=self.this_month
        ).order_by('persona__nom')


class StatsBenevView(StatsMixin, TemplateView):
    template_name = 'stats/benevoles.html'
    form_class = DateLimitFormWithAVS
    stat_items = {
        'benevoles': {
            'label': 'Nombre de bénévoles',
            'help': "Nombre de bénévoles ayant effectué au moins une intervention sur la période donnée",
        },
        'nb': {'label': 'Nombre d’interventions'},
        'durees_client': {'label': 'Temps passé par les bénévoles avec les clients'},
        'durees_seul': {'label': 'Temps passé par les bénévoles sans client'},
        'kms': {'label': 'Km défrayés aux bénévoles'},
        'frais': {'label': 'Frais remboursés aux bénévoles'},
    }

    def get_stats(self, months):
        annots = {'mois': TruncMonth('effectuee')}
        filters = {'effectuee__range': (self.date_start, self.date_end)}
        if self.date_form.cleaned_data['avs_only']:
            annots['is_ofas'] = IsAVS(
                F('effectuee'), F('client__tarif_avs_des'),
                F('client__persona__date_naissance'), F('client__persona__genre')
            )
            filters['is_ofas'] = True
        intervs_base = Mission.objects.exclude(
            intervenant__benevole__isnull=True
        ).annotate(**annots).filter(**filters).order_by()
        intervs = intervs_base.values('mois', 'type_mission').annotate(
            nb=Count('id'),
            durees_client=Sum('duree_client', default=timedelta()),
            durees_seul=Sum('duree_seul', default=timedelta()),
            kms=Sum('km', default=0),
            frais=Sum('frais__cout', default=0),
        )

        missions = {m.pk: m for m in TypeMission.objects.all()}
        counters = {}
        init_data = {
            'nb': 0, 'durees_client': timedelta(), 'durees_seul': timedelta(),
            'kms': 0, 'frais': Decimal(0),
        }
        for mis in tuple(missions.keys()) + ('total',):
            counters[mis] = {}
            for month in months:
                counters[mis][month] = init_data.copy()
            counters[mis]['total'] = init_data.copy()

        stat_keys = [k for k in self.stat_items if k != 'benevoles']
        for line in intervs:
            month = Month.from_date(line['mois'])
            for key in stat_keys:
                counters[line['type_mission']][month][key] += line.get(key)
                counters[line['type_mission']]['total'][key] += line.get(key)
                counters['total'][month][key] += line.get(key)
                counters['total']['total'][key] += line.get(key)

        # Nb bénévoles
        counters.update(self.init_counters(['benevoles'], months))
        benev_qs = intervs_base.values('mois').annotate(
            nb_benev=Count('intervenant', distinct=True),
        )
        for line in benev_qs:
            counters['benevoles'][Month.from_date(line['mois'])] = line['nb_benev']
        counters['benevoles']['total'] = intervs_base.aggregate(
            nb_benev=Count('intervenant', distinct=True)
        )['nb_benev']

        return {
            'labels': self.stat_items,
            'missions': missions,
            'stat_subkeys': [k for k in self.stat_items if k not in ['benevoles', 'nb']],
            'stats': counters,
        }

    def export_lines(self, context):
        months = context['months']
        title = "Bénévoles alarme"
        if self.date_form.cleaned_data['avs_only']:
            title += " (chez clients AVS)"
        yield ExpLine([title] + [str(month) for month in months] + ['Total'], bold=True)
        yield (
            ['Nombre de bénévoles'] + [context['stats']['benevoles'][month] for month in months] +
            [context['stats']['benevoles']['total']]
        )
        for pk, mission in context['missions'].items():
            yield ExpLine(
                [str(mission)] + [context['stats'][pk][month]['nb'] for month in months] +
                [context['stats'][pk]['total']['nb']],
                bold=True,
            )
            for key, data in self.stat_items.items():
                if key in ['nb', 'benevoles']:
                    continue
                yield (
                    [f"  {data['label']}"] + [context['stats'][pk][month][key] for month in months] +
                    [context['stats'][pk]['total'][key]]
                )
        yield ExpLine(['Totaux'], bold=True)
        for key, data in self.stat_items.items():
            if key == 'benevoles':
                continue
            yield (
                [data['label']] + [context['stats']['total'][month][key] for month in months] +
                [context['stats']['total']['total'][key]]
            )
