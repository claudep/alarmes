import string

import factory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyText

from common.choices import Services
from . import models


class AlarmeFactory(DjangoModelFactory):
    class Meta:
        model = models.Alarme

    modele = factory.Iterator(models.ModeleAlarme.objects.all(), cycle=True)
    no_appareil = FuzzyText(length=12)
    no_serie = FuzzyText(length=8, chars=string.digits)
    date_achat = factory.Faker('date')


class InstallationFactory(DjangoModelFactory):
    class Meta:
        model = models.Installation

    client = factory.Iterator(models.Client.objects.par_service(Services.ALARME))
    alarme = factory.Iterator(models.Alarme.objects.filter(en_rep_depuis=None))
    abonnement = factory.Iterator(models.TypeAbo.objects.all())
    date_debut = factory.Faker('date')


class TypeMissionFactory(DjangoModelFactory):
    class Meta:
        model = models.TypeMission

    nom = factory.fuzzy.FuzzyText(length=8)
    code = factory.fuzzy.FuzzyChoice(models.TypeMission.CODE_CHOICES, getter=lambda c: c[0])


class MissionFactory(DjangoModelFactory):
    class Meta:
        model = models.Mission

    client = factory.Iterator(models.Client.objects.par_service(Services.ALARME))
    type_mission = factory.SubFactory(TypeMissionFactory)
