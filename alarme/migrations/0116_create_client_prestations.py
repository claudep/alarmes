from django.db import migrations
from django.db.models import Count, Q

from common.choices import Services


def create_prestations(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    Prestation = apps.get_model('client', 'Prestation')

    # alarme
    clients = Client.objects.annotate(num_install=Count('installation')).filter(
        Q(type_client__contains=['alarme']) | Q(num_install__gt=0)
    )
    for client in clients:
        installs = sorted(client.installation_set.all(), key = lambda inst: inst.date_debut)
        if not installs:
            debut = client.journaux.order_by('quand').first().quand.date()  # création client
            fin = client.archive_le or None
        else:
            debut = installs[0].date_debut
            if client.archive_le or 'alarme' in client.type_client:
                fin = client.archive_le
            else:
                if any(inst.date_fin_abo is None for inst in installs):
                    fin = None
                else:
                    fin = sorted([inst.date_fin_abo for inst in installs])[-1]
        if fin and fin < debut:
            import pdb; pdb.set_trace()
        Prestation.objects.create(client=client, service=Services.ALARME, duree=(debut, fin))


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0018_prestation'),
        ('alarme', '0115_facture_rabais'),
    ]

    operations = [
        migrations.RunPython(create_prestations),
    ]
