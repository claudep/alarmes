from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("alarme", "0119_is_ofas_alarme"),
    ]

    # Replaced by client migration
    operations = []
