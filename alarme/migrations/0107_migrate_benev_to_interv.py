from django.conf import settings
from django.db import migrations


def migrate_mission_benev_to_interv(apps, schema_editor):
    Mission = apps.get_model('alarme', 'Mission')
    for mission in Mission.objects.filter(benevole__isnull=False):
        mission.intervenant = mission.benevole.utilisateur
        mission.save(update_fields=['intervenant'])


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alarme', '0106_mission_intervenant'),
    ]

    operations = [
        migrations.RunPython(migrate_mission_benev_to_interv)
    ]
