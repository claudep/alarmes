from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0110_migrate_chez_benev_to_utilisateur'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alarme',
            name='chez_benevole',
        ),
        migrations.RemoveField(
            model_name='materiel',
            name='chez_benevole',
        ),
    ]
