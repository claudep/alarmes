from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0123_alarme_date_batterie'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelealarme',
            name='archive',
            field=models.BooleanField(default=False, verbose_name="Archivé"),
        ),
    ]
