from django.contrib.postgres.fields.ranges import DateRangeField
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0116_create_client_prestations'),
        ('client', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='RabaisAuto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duree', DateRangeField(verbose_name='Durée')),
                ('article', models.ForeignKey(on_delete=models.deletion.PROTECT, to='alarme.articlefacture')),
                ('client', models.ForeignKey(on_delete=models.deletion.CASCADE, to='client.client')),
            ],
        ),
    ]
