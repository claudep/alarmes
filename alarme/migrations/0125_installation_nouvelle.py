from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0124_modelealarme_archive'),
    ]

    operations = [
        migrations.AddField(
            model_name='installation',
            name='nouvelle',
            field=models.BooleanField(default=True),
        ),
    ]
