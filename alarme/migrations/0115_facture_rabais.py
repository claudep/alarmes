from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0114_modeles_article_install'),
    ]

    operations = [
        migrations.AddField(
            model_name='facture',
            name='rabais',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Rabais'),
        ),
    ]
