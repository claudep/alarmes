from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0107_migrate_benev_to_interv'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mission',
            name='benevole',
        ),
    ]
