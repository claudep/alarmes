from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0122_date_facture_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='alarme',
            name='date_batterie',
            field=models.DateField(blank=True, null=True, verbose_name='Dernier changement de batterie'),
        ),
    ]
