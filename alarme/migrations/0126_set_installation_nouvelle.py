from datetime import timedelta
from operator import attrgetter

from django.db import migrations
from django.db.models import Count


def set_installation_nouvelle(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    for client in Client.objects.annotate(
        num_installs=Count("installation")
    ).filter(num_installs__gt=0).prefetch_related("installation_set"):
        installs = sorted(list(client.installation_set.all()), key=attrgetter("date_debut"))
        if len(installs) < 2:
            continue
        prev = None
        for install in installs:
            # L'installation n'et pas nouvelle si elle suit de près une précédente.
            if (
                prev is not None and prev.date_fin_abo and
                install.date_debut - prev.date_fin_abo < timedelta(days=3)
            ):
                install.nouvelle = False
                install.save(update_fields=["nouvelle"])
            prev = install


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0125_installation_nouvelle'),
        ('client', '__first__'),
    ]

    operations = [
        migrations.RunPython(set_installation_nouvelle, migrations.RunPython.noop)
    ]
