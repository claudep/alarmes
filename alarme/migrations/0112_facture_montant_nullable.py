from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0111_remove_alarme_chez_benevole'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facture',
            name='montant',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True, verbose_name='Montant'),
        ),
    ]
