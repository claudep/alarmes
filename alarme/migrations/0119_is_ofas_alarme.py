from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("alarme", "0118_remove_modelealarme_prix_location"),
    ]

    # Replaced by client migration
    operations = []
