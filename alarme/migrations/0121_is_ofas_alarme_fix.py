from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("alarme", "0120_is_ofas_alarme_exact_birthdate"),
    ]

    # Replaced by client migration
    operations = []
