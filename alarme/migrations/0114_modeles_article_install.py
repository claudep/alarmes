from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0113_is_ofas_alarme'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelealarme',
            name='article_install',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='modeles_install', to='alarme.articlefacture', verbose_name='Article pour installation'),
        ),
        migrations.AddField(
            model_name='typemateriel',
            name='article_install',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='mat_install', to='alarme.articlefacture', verbose_name='Article pour installation'),
        ),
        migrations.AlterField(
            model_name='modelealarme',
            name='article_achat',
            field=models.ForeignKey(blank=True, help_text='Si un article d’achat est défini, les appareils de ce modèle sont automatiquement facturés au client', null=True, on_delete=models.deletion.PROTECT, related_name='modeles_achat', to='alarme.articlefacture', verbose_name='Article pour achat'),
        ),
        migrations.AlterField(
            model_name='typemateriel',
            name='article_achat',
            field=models.ForeignKey(blank=True, help_text='Si un article d’achat est défini, les appareils de ce modèle sont automatiquement facturés au client', null=True, on_delete=models.deletion.PROTECT, related_name='mat_achat', to='alarme.articlefacture', verbose_name='Article pour achat'),
        ),
    ]
