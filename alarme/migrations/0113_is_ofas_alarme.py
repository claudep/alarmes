from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("alarme", "0112_facture_montant_nullable"),
    ]

    operations = [
        # Exception tarif_avs_acquis 2023, voir https://gitlab.com/croixrouge/transports/-/issues/335
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION is_ofas_simple(
    event_date date, tarif_avs_acquis boolean, date_naissance date, genre varchar(1)
)
RETURNS BOOLEAN AS $$
DECLARE is_ofas boolean;
        num_years integer := EXTRACT(YEAR FROM event_date) - EXTRACT(YEAR FROM date_naissance);
BEGIN
    SELECT (tarif_avs_acquis AND EXTRACT(YEAR FROM event_date) <> 2023) OR num_years >= 65 OR (genre = 'F' AND num_years >= 64) INTO is_ofas;
    RETURN is_ofas;
END
$$
LANGUAGE plpgsql
IMMUTABLE;""")
    ]
