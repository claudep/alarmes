from django.conf import settings
from django.db import migrations


def migrate_alarme_benev_to_interv(apps, schema_editor):
    Alarme = apps.get_model('alarme', 'Alarme')
    for alarme in Alarme.objects.filter(chez_benevole__isnull=False):
        alarme.chez_personne = alarme.chez_benevole.utilisateur
        alarme.save(update_fields=['chez_personne'])

    Materiel = apps.get_model('alarme', 'Materiel')
    for mat in Materiel.objects.filter(chez_benevole__isnull=False):
        mat.chez_personne = mat.chez_benevole.utilisateur
        mat.save(update_fields=['chez_personne'])


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alarme', '0109_alarme_chez_personne'),
    ]

    operations = [
        migrations.RunPython(migrate_alarme_benev_to_interv)
    ]
