from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alarme', '0105_typemateriel_abos'),
    ]

    operations = [
        migrations.AddField(
            model_name='mission',
            name='intervenant',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Intervenant'),
        ),
    ]
