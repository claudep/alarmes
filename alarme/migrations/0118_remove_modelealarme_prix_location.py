from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0117_rabaisauto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='modelealarme',
            name='prix_location',
        ),
    ]
