from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alarme', '0108_remove_mission_benevole'),
    ]

    operations = [
        migrations.AddField(
            model_name='alarme',
            name='chez_personne',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL,
                verbose_name='Chez la personne'
            ),
        ),
        migrations.AddField(
            model_name='materiel',
            name='chez_personne',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL,
                verbose_name='Chez la personne'
            ),
        ),
    ]
