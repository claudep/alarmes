from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0121_is_ofas_alarme_fix'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facture',
            name='date_facture',
            field=models.DateField(
                blank=True,
                help_text='Quand elle est laissée vide, la date de facturation est définie au moment de la prochaine facturation mensuelle',
                null=True, verbose_name='Date de facturation'
            ),
        ),
    ]
