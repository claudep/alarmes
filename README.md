# Alarme

## Description

TODO

## Dev environment with docker-compose

Prerequisites:

- Docker & docker-compose installed on your system
- "docker" group exists on your system (`sudo groupadd docker` will fail anyway if it exists)
- Your user in "docker" group (`sudo usermod -aG docker $USER` + restart your system)

### Quick start

To run a dev environment: `docker-compose up -d`  
Then you'll be able to reach your local environment on localhost:8000

To collect static: `docker-compose exec web /code/manage.py collectstatic`

To restart & rebuild your environment: `docker-compose down --remove-orphans && docker-compose up -d --build`

To use Python debugger (breakpoint): `docker attach $(docker-compose ps -q web)`
