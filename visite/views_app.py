from datetime import date, timedelta
from itertools import groupby
from operator import attrgetter

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.db.models import Count, Sum
from django.db.models.functions import TruncDate, TruncMonth
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView

from common.views import BenevoleMixin, CreateUpdateView
from . import forms
from .models import ACTIVITE_INSCRIPTEUR, Attribution, Visite


class VisiteEditView(BenevoleMixin, CreateUpdateView):
    model = Visite
    form_class = forms.VisiteBenevForm
    template_name = 'visite/visite_edit.html'
    json_response = True

    def dispatch(self, request, *args, **kwargs):
        self.attribution = None
        if 'attribpk' in kwargs:  # Nouvelle visite
            self.attribution = get_object_or_404(Attribution, pk=kwargs['attribpk'])
            if (
                request.user.is_benevole and not request.user.benevole.has_activite(ACTIVITE_INSCRIPTEUR) and
                not self.attribution.visiteur.utilisateur == request.user
            ):
                raise PermissionDenied("Vous n’avez pas les permissions pour ajouter des visites")
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if not form.instance.client_id and self.attribution:
            form.instance.client = self.attribution.client
        if not form.instance.visiteur_id and self.attribution:
            form.instance.visiteur = self.attribution.visiteur
        try:
            super().form_valid(form)
        except IntegrityError:
            form.add_error(None, "Cette visite existe déjà")
            return self.form_invalid(form)
        date_str = self.object.debut.date().strftime('%d.%m.%Y')
        if self.is_create:
            msg = f"La visite du {date_str} pour {self.object.client} a bien été créée."
        else:
            msg = f"La visite du {date_str} pour {self.object.client} a bien été modifiée."
        messages.success(self.request, msg)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleArchivesView(BenevoleMixin, TemplateView):
    """Liste des mois d'archives"""
    template_name = 'benevoles/archives-mois.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='visite/app/base.html')
        context['months'] = self.benev.visite_set.exclude(duree=None).annotate(
            month=TruncMonth('debut')
        ).values_list('month', flat=True).distinct().order_by('-month')
        return context


class BenevoleArchivesMonthView(BenevoleMixin, TemplateView):
    template_name = 'visite/app/archives-mois-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = date(self.kwargs['year'], self.kwargs['month'], 1)
        end = (start + timedelta(days=32)).replace(day=1) - timedelta(days=1)
        visites = self.benev.visite_set.exclude(duree=None).annotate(
            date=TruncDate('debut'),
        ).filter(date__range=[start, end]).order_by('debut')
        context.update({
            'days': {
                dt: list(vis) for dt, vis in groupby(visites, key=attrgetter('date'))
            },
            'benev': self.benev,
        })
        context['sums'] = visites.aggregate(
            total_visites=Count('id'),
            total_duree=Sum('duree'),
        )
        return context
