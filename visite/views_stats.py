from datetime import datetime, time, timedelta, timezone

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Count, Q, Sum
from django.db.models.functions import TruncMonth
from django.views.generic import TemplateView

from common.export import ExpLine
from common.stat_utils import Month, StatsMixin
from .models import Attribution, Visite


class StatsVisitesView(PermissionRequiredMixin, StatsMixin, TemplateView):
    permission_required = 'visite.view_attribution'
    template_name = 'visite/stats/index.html'
    active = 'visites'

    stat_items = {
        'clients': {
            'label': 'Nbre de bénéficiaires',
            'help': "Bénéficiaires ayant eu au moins une visite durant le mois (sans visite initiale)",
        },
        'visiteurs': {
            'label': 'Nbre de bénévoles',
            'help': "Bénévoles ayant effectué au moins une visite durant le mois (sans visite initiale)",
        },
        'binomes': {
            'label': 'Nbre de binômes',
            'help': "Binômes en activité, avec ou sans visite durant le mois"
        },
        'vad_initiales': {'label': 'Nbre de visites initiales'},
        'nb_visites': {
            'label': 'Nbre de visites',
            'help': "Jusqu’à octobre 2024, les visites ont été comptabilisées une fois par binôme et par mois"
        },
        'duree_heures1': {'label': 'Heures de visites initiales'},
        'duree_heures': {'label': 'Heures de visites «normales»'},
    }

    def get_stats(self, months):
        months = [m for m in months if not m.is_future()]
        start, _ = months[0].limits()
        _, end = months[-1].limits()
        start = datetime.combine(start, time(0), tzinfo=timezone.utc)
        end = datetime.combine(end, time(23, 59), tzinfo=timezone.utc)
        visites_qs = Visite.objects.filter(debut__range=(start, end))
        duree0 = timedelta(0)
        stats = visites_qs.annotate(
            month=TruncMonth('debut'),
        ).values('month').annotate(
            count_cl=Count('client', filter=Q(vad_initiale=False, duree__gt=duree0), distinct=True),
            count_benev=Count('visiteur', filter=Q(vad_initiale=False, duree__gt=duree0), distinct=True),
            count_visite1=Count('vad_initiale', filter=Q(vad_initiale=True)),
            count_visites=Count('id', filter=Q(vad_initiale=False, duree__gt=duree0)),
            count_heures1=Sum('duree', filter=Q(vad_initiale=True)),
            count_heures=Sum('duree', filter=Q(vad_initiale=False)),
        )
        by_month = {
            Month(line['month'].year, line['month'].month): {
                'clients': line['count_cl'],
                'visiteurs': line['count_benev'],
                'vad_initiales': line['count_visite1'],
                'nb_visites': line['count_visites'],
                'duree_heures1': line['count_heures1'],
                'duree_heures': line['count_heures'],
            } for line in stats
        }
        totaux = visites_qs.aggregate(
            total_cl=Count('client', filter=Q(vad_initiale=False, duree__gt=duree0), distinct=True),
            total_vi=Count('visiteur', filter=Q(vad_initiale=False, duree__gt=duree0), distinct=True),
        )

        counter_keys = list(self.stat_items.keys())

        counters = self.init_counters(counter_keys, months)
        for month in months:
            if month not in by_month:
                continue
            for key in [
                'clients', 'visiteurs', 'vad_initiales', 'nb_visites', 'duree_heures1', 'duree_heures'
            ]:
                if by_month[month][key] is None:
                    continue
                counters[key][month] += by_month[month][key]
                if key in ['duree_heures1', 'duree_heures', 'vad_initiales', 'nb_visites']:
                    counters[key]['total'] += by_month[month][key]
        counters['clients']['total'] = totaux['total_cl']
        counters['visiteurs']['total'] = totaux['total_vi']

        # Stats des binômes
        attrib_qs = Attribution.objects.filter(
            Q(debut__lt=end) & (Q(fin__isnull=True) | Q(fin__gt=start))
        )
        for attrib in attrib_qs:
            for month in months:
                mstart, mend = month.limits()
                if attrib.debut < mend and (attrib.fin is None or attrib.fin > mstart):
                    counters['binomes'][month] += 1
        counters['binomes']['total'] = len(attrib_qs)

        return {
            'labels': list(self.stat_items.values()),
            'stats': {key: counters[key] for key in counter_keys},
        }

    def export_lines(self, context):
        months = context['months']
        yield ExpLine(['Statistique visites'] + [str(month) for month in months] + ['Total'], bold=True)
        for key, data in self.stat_items.items():
            yield (
                [data['label']] +
                [context['stats'][key][month] for month in months] +
                [context['stats'][key]['total']]
            )
