from datetime import date, timedelta

from dateutil.relativedelta import relativedelta
from openpyxl.utils import get_column_letter

from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db.models import (
    Count, F, Max, OuterRef, Prefetch, Q, Subquery, Sum,
)
from django.db.models.functions import TruncMonth
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.views.generic import TemplateView, View

from benevole.models import Benevole
from benevole.views import (
    BenevoleEditView as BenevoleEditViewBase, BenevoleListView as BenevoleListViewBase
)
from common.choices import Services
from common.export import ExpLine
from common.utils import last_day_of_month
from common.views import CreateUpdateView, ExportableMixin, FilterFormMixin, ListView
from client.models import Client
from client.views import (
    ClientDetailContextMixin, ClientEditViewBase, ClientJournalMixin, ClientListViewBase
)

from . import forms
from .models import (
    ACTIVITE_INSCRIPTEUR, ACTIVITE_VISITE, Attribution, Inscription, Visite,
    clients_en_attente_inscription
)


def visite_profil(user):
    """
    Renvoie le profil de l'utilisateur dans le contexte des visites:
     - 'gestion' pour les gestionnaires
     - 'inscripteur' pour les bénévoles saisissant les inscriptions
     - 'visiteur' pour les bénévoles visiteurs
     - None si pas d'accès
    """
    if user.is_anonymous:
        return None
    if user.has_perm("visite.change_attribution") or user.has_perm("visite.view_attribution"):
        # Gestionnaires des visites
        return 'gestion'
    try:
        benevole = user.benevole
    except Benevole.DoesNotExist:
        return None
    if benevole.has_activite(ACTIVITE_INSCRIPTEUR):
        # Groupe inscription/1ère visite
        return 'inscripteur'
    if benevole.has_activite(ACTIVITE_VISITE):
        return 'visiteur'
    return None


def home(request):
    profil = visite_profil(request.user)
    if profil in ["gestion", "inscripteur"]:
        en_attente = clients_en_attente_inscription()
        for client in en_attente:
            try:
                client.visite_prevue = client.visites.latest('debut')
            except Visite.DoesNotExist:
                client.visite_prevue = None
        context = {"en_attente": en_attente}
        if profil == 'gestion':
            return render(request, 'visite/home.html', context=context)
        if profil == 'inscripteur':
            context["attribs"] = request.user.benevole.attribution_set.filter(fin__isnull=True).exists()
            return render(request, 'visite/home_inscripteur.html', context=context)
    # Renvoi sur l’app bénévole, qui se charge elle-même du contrôle de ses accès
    return home_visiteur(request)


def home_visiteur(request):
    profil = visite_profil(request.user)
    if profil is None or profil == 'gestion':
        raise PermissionDenied("Accès réservé aux bénévoles visiteurs")
    attribs = request.user.benevole.attribution_set.filter(fin__isnull=True).select_related(
        'client', 'client__inscription'
    ).prefetch_related('client__persona__adresseclient_set', 'client__referent_set')
    context = {'attributions': []}
    for attrib in attribs:
        attrib.prochaine = Visite.objects.filter(
            client=attrib.client, visiteur=attrib.visiteur, debut__gte=timezone.now()
        ).order_by('debut').first()
        attrib.non_rapportees = Visite.objects.filter(
            client=attrib.client, visiteur=attrib.visiteur, debut__lt=timezone.now(), duree=None,
        ).order_by('debut')
        attrib.donnees_inscr = {}
        try:
            inscription = attrib.client.inscription
        except ObjectDoesNotExist:
            pass
        else:
            # Remplir attrib.donnees_inscr avec les données de l'inscription
            for field in inscription._meta.concrete_fields:
                if field.name in {'id', 'client', 'compte_rendu'}:
                    continue
                if field.name == 'types_visites':
                    value = inscription.get_types_visites_display()
                else:
                    value = getattr(inscription, field.name)
                if not value:
                    continue
                attrib.donnees_inscr[field.verbose_name] = value
            if attrib.client.handicaps:
                attrib.donnees_inscr['Handicaps'] = attrib.client.get_handicaps_display()
        attrib.contacts = [ref for ref in attrib.client.referents.filter(contact_visites=True)]
        context['attributions'].append((attrib, attrib.client))
    return render(request, 'visite/app/index.html', context=context)


class ClientListView(ClientListViewBase):
    template_name = 'visite/client_list.html'
    return_all_if_unbound = True
    client_types = ['visite']
    client_libelles = ("bénéficiaire", "bénéficiaires")

    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch(
                'attributions',
                queryset=Attribution.objects.filter(fin__isnull=True).select_related('visiteur')
            )
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "visite/base.html",
            'home_url': reverse('home'),
            'client_url_name': 'client-edit',
        }


class ClientEditView(ClientDetailContextMixin, ClientEditViewBase):
    template_name = 'visite/client_edit.html'
    base_template = 'visite/base.html'
    form_class = forms.ClientEditForm
    client_type = 'visite'


class ClientsAttenteView(PermissionRequiredMixin, TemplateView):
    permission_required = 'client.view_client'
    template_name = 'visite/client_attente.html'
    title = "Bénéficiaires en attente de visiteurs"

    def get_queryset(self):
        return Client.objects.par_service(Services.VISITE).avec_adresse(date.today()).annotate(
            num_attr_actives=Count('attributions', filter=Q(attributions__fin__isnull=True)),
            date_demande=F('inscription__date_demande'),
            date_inscr=F('inscription__date_quest'),
            last_fin=Max('attributions__fin'),
        ).filter(
            num_attr_actives=0,
            inscription__date_quest__isnull=False,  # Exclure clients non inscrits (sur page accueil)
        ).order_by('persona__nom')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        clients = self.get_queryset()
        context.update({
            # En attente attribution binôme depuis l’inscription
            'attente_binome': clients.filter(last_fin=None),
            # En attente depuis la fin du dernier binôme
            'attente_prochain': clients.filter(last_fin__isnull=False),
        })
        return {**context, 'count': clients.count()}


class ClientAttributionsView(ClientDetailContextMixin, ListView):
    model = Attribution
    template_name = 'visite/client_attribs.html'

    def get(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.client.attributions.order_by('-fin', '-debut')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientVisitesView(ClientDetailContextMixin, ListView):
    model = Visite
    template_name = 'visite/client_visites.html'

    def get(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.client.visites.order_by('-debut')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientInscriptionView(ClientDetailContextMixin, ClientJournalMixin, CreateUpdateView):
    template_name = 'visite/client_inscription.html'
    model = Inscription
    form_class = forms.InscriptionForm
    journal_add_message = "Création de l’inscription visites"
    journal_edit_message = "Modification de l’inscription visites: {fields}"

    def get_object(self):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        try:
            return self.client.inscription
        except ObjectDoesNotExist:
            return None

    def get_client(self):
        return self.client

    def get_context_data(self, **kwargs):
        visite = self.client.visites.filter(vad_initiale=True).first()
        return super().get_context_data(
            visiteur_inscr=visite.visiteur if visite else None,
            **kwargs
        )

    def get_success_message(self, obj):
        return "Le formulaire d’inscription a bien été enregistré"

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        if not form.instance.client_id:
            form.instance.client = self.client
        return super().form_valid(form)


def get_client_tabs(view, client, tabs):
    tabs.insert(1, {
        'id': 'inscription', 'title': "Inscription",
        'url': reverse('client-inscription', args=[client.pk])
    })
    profil = visite_profil(view.request.user)
    if profil == 'gestion':
        tabs.insert(2, {
            'id': 'attribs', 'title': "Binômes",
            'url': reverse('client-attribs', args=[client.pk])
        })
        tabs.insert(3, {
            'id': 'visites', 'title': "Visites",
            'url': reverse('client-visites', args=[client.pk])
        })
    else:
        tabs.pop()
    return tabs


ClientDetailContextMixin.app_tabs_methods['visite'] = get_client_tabs


class BenevoleListView(BenevoleListViewBase):
    template_name = 'visite/benevole_list.html'
    filter_formclass = forms.BenevoleFilterForm

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Benevole.objects.annotate(
                num_visites=Count('attribution', filter=Q(attribution__fin__isnull=True), distinct=True),
            )
        )


class BenevoleEditView(BenevoleEditViewBase):
    template_name = 'visite/benevole.html'

    def get_form_class(self):
        return forms.BenevoleVisiteForm

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'visites_en_cours': self.object.attribution_set.filter(fin__isnull=True),
        }


class BenevoleBinomesView(PermissionRequiredMixin, ListView):
    permission_required = 'visite.view_attribution'
    model = Attribution
    paginate_by = 30
    template_name = 'visite/attributions.html'
    tabs_template = 'benevole/benevole_tabs.html'

    def get_queryset(self):
        return super().get_queryset().filter(
            visiteur_id=self.kwargs['pk']
        ).select_related('client').order_by('-debut')

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'active': 'benevole',
            'benevole': get_object_or_404(Benevole, pk=self.kwargs['pk']),
        }
        context['title'] = f"Binômes de visites pour {context['benevole']}"
        return context


class AttributionsView(PermissionRequiredMixin, FilterFormMixin, ExportableMixin, ListView):
    permission_required = 'visite.view_attribution'
    model = Attribution
    paginate_by = 30
    filter_formclass = forms.AttributionFilterForm
    template_name = 'visite/attributions.html'
    tabs_template = 'visite/attrib_tabs.html'

    def get_queryset(self):
        qs = super().get_queryset().annotate(
            derniere_visite=Subquery(
                Visite.objects.filter(
                    client_id=OuterRef('client_id'), visiteur_id=OuterRef('visiteur_id')
                ).values('debut').order_by('-debut')[:1]
            )
        ).select_related('client', 'visiteur')
        if self.kwargs['status'] == 'encours':
            return qs.filter(fin__isnull=True).order_by('client__persona__nom')
        if self.kwargs['status'] == 'terminees':
            return qs.filter(fin__isnull=False).order_by('-fin')
        raise Http404(f"Statut {self.kwargs['status']} inconnu")

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'active': self.kwargs['status'],
            'annees': [str(date.today().year), str(date.today().year - 1)],
            'exportable': True,
        }
        if self.kwargs['status'] == 'encours':
            try:
                selected_year = int(self.request.GET.get("annee", date.today().year))
            except ValueError:
                selected_year = date.today().year
            first_day_of_year = date(selected_year, 1, 1)
            if selected_year == date.today().year:
                last_month_day = last_day_of_month(date.today())
            else:
                last_month_day = date(selected_year, 12, 31)
            totaux_mensuels = {
                f"{line['mois'].strftime('%Y-%m-%d')}-{line['client']}-{line['visiteur']}": (
                    line['tot_durees'], line['tot_visites']
                )
                for line in Visite.objects.filter(
                    debut__date__range=(first_day_of_year, last_month_day)
                ).annotate(mois=TruncMonth('debut')).values(
                    'mois', 'client', 'visiteur'
                ).annotate(
                    tot_durees=Sum('duree'), tot_visites=Count('id')
                )
            }
            lastm = last_month_day.replace(day=1)
            context.update({
                'totaux_mensuels': totaux_mensuels,
                # Tous les mois depuis le début de l'année
                'mois': [m for m in [first_day_of_year] + [
                    first_day_of_year + relativedelta(months=i) for i in range(1, 12)
                ] if m <= lastm],
            })
        context['title'] = "Binômes de visites" + (" terminés" if self.kwargs['status'] == 'terminees' else "")
        return context

    @property
    def col_widths(self):
        if self.kwargs["status"] == "terminees":
            return [32, 32, 12, 12, 90, 12]
        return [32, 32, 12]

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = self.export_class(sheet_title="Binômes-heures", col_widths=self.col_widths)
            export.fill_data(self.export_lines(context, 0))
            export.add_sheet("Binômes-nombres")
            export.fill_data(self.export_lines(context, 1))
            return export.get_http_response(self.__class__.__name__.replace('View', '') + '.xlsx')
        return super().render_to_response(context, **response_kwargs)

    def export_lines(self, context, total_idx):
        fields = [("Bénéficiaire", "client"), ("Bénévole", "visiteur"), ("Dès le", "debut")]
        exp_term = self.kwargs["status"] == "terminees"
        if exp_term:
            fields.extend([
                ("Jusqu’au", "fin"), ("Motif fin", "motif_fin"), ("Dern. visite", "derniere_visite"),
            ])
            headers = [f[0] for f in fields]
        else:
            headers = [f[0] for f in fields] + [
                django_format(m, "F Y") for m in context.get("mois", [])
            ] + ["Total"]
        yield ExpLine(headers, bold=True)
        num_lines = 0
        last_col_ltr = get_column_letter(len(headers) - 1)
        total_col_idx = len(headers)
        total_format = "[hh]:mm" if total_idx == 0 else ""
        for attrib in self.get_queryset():
            month_vals = [
                context['totaux_mensuels'].get(
                    f"{str(m)}-{attrib.client_id}-{attrib.visiteur_id}", (None, None)
                )[total_idx]
                for m in context.get("mois", [])
            ]
            yield ExpLine(
                [getattr(attrib, f[1]) for f in fields] + month_vals + (
                    [f"=SUM(D{num_lines + 2}:{last_col_ltr}{num_lines + 2})"] if not exp_term else []
                ),
                number_formats={total_col_idx: total_format},
            )
            num_lines += 1
        if not exp_term:
            # Ligne sommes totales
            yield ExpLine(
                ["" for i in range(len(fields))] + [
                    f"=SUM({get_column_letter(idx)}2:{get_column_letter(idx)}{num_lines + 1})"
                    for idx in range(len(fields) + 1, len(fields) + 2 + len(context.get("mois", [])))
                ],
                number_formats={
                    idx: total_format
                    for idx, m in enumerate(context.get("mois", []), start=len(fields) + 2)
                },
                bold=True
            )


class AttributionEditView(PermissionRequiredMixin, CreateUpdateView):
    permission_required = 'visite.add_attribution'
    model = Attribution
    form_class = forms.AttributionForm
    template_name = 'general_edit.html'
    json_response = True

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VisitesView(PermissionRequiredMixin, FilterFormMixin, ListView):
    permission_required = 'visite.view_visite'
    model = Visite
    filter_formclass = forms.VisiteFilterForm
    template_name = 'visite/visites.html'
    paginate_by = 30

    def get_queryset(self):
        return super().get_queryset().select_related('client', 'visiteur').order_by('-debut')


class VisiteEditView(UserPassesTestMixin, CreateUpdateView):
    permission_required = 'visite.add_visite'
    model = Visite
    form_class = forms.VisiteForm
    template_name = 'visite/visite_edit.html'
    json_response = True

    def test_func(self):
        user = self.request.user
        return user.has_perm("visite.add_visite") or (
            user.is_benevole and user.benevole.has_activite(ACTIVITE_INSCRIPTEUR)
        )

    def get_initial(self):
        if self.is_create:
            client = None
            if self.request.GET.get('client'):
                client = get_object_or_404(Client, pk=self.request.GET['client'])
            return {
                **super().get_initial(),
                'client': client,
                'visiteur': self.request.GET.get('benevole'),
                'debut': (
                    (timezone.now().replace(day=1) - timedelta(days=1)).replace(day=25, hour=12, minute=0)
                    if self.request.user.has_perm("visite.add_visite") else ""
                ),
            }
        else:
            return super().get_initial()

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VisiteDeleteView(View):
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': 'page'})

    def get_object(self):
        obj = get_object_or_404(Visite, pk=self.kwargs['pk'])
        if (
            not self.request.user.has_perm('visite.delete_visite') and
            not self.request.user == obj.visiteur.utilisateur
        ):
            raise PermissionDenied("Vous n’avez pas la permission de supprimer cette visite.")
        return obj
