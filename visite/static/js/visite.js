"use strict";

window.addEventListener('DOMContentLoaded', () => {
    // Pour sélecteur dans attributions.html
    attachHandlerSelector(document, 'input[type=radio][name="type_affichage"]', 'change', (ev) => {
        if (ev.target.value == "heures") {hide(".total_nbre"); show(".total_duree");}
        else {hide(".total_duree"); show(".total_nbre");}
    });
});
