import common.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('client', '__first__'),
        ('visite', '0006_visite_duree_nullable'),
    ]

    operations = [
        migrations.CreateModel(
            name='Inscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parcours_vie', models.TextField(blank=True, verbose_name='Profession\xa0et parcours de vie')),
                ('dispos', models.TextField(blank=True, verbose_name='Disponibilités')),
                ('frequence', models.CharField(blank=True, max_length=200, verbose_name='Fréquence de visites')),
                ('types_visites', common.fields.ChoiceArrayField(base_field=models.CharField(choices=[('presence', 'Présence/Écoute'), ('prom', 'Promenades'), ('lecture', 'Lecture'), ('jeux', 'Jeux')], max_length=10), blank=True, default=list, size=None, verbose_name='Activités souhaitées durant les visites')),
                ('pref_benevole', models.CharField(blank=True, max_length=200, verbose_name='Préférence bénévole')),
                ('etat_sante', models.TextField(blank=True, verbose_name='État de santé')),
                ('interets', models.TextField(blank=True, verbose_name='Intérêts')),
                ('demande_par', models.CharField(blank=True, max_length=150, verbose_name='Demande par')),
                ('services', models.TextField(blank=True, verbose_name='Services à domicile')),
                ('famille', models.TextField(blank=True, verbose_name='Entourage familial')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('client', models.OneToOneField(on_delete=models.deletion.CASCADE, to='client.client')),
            ],
        ),
    ]
