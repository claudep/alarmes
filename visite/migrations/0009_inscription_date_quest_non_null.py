from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0008_inscription_date_quest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inscription',
            name='date_quest',
            field=models.DateField(verbose_name='Date de remplissage'),
        ),
    ]
