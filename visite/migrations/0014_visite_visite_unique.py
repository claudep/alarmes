from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '__first__'),
        ('client', '__first__'),
        ('visite', '0013_inscription_date_demande'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='visite',
            constraint=models.UniqueConstraint(
                fields=('client', 'visiteur', 'debut'), name='visite_unique',
            )
        ),
    ]
