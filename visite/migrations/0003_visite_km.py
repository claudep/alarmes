from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0002_client_visite'),
    ]

    operations = [
        migrations.AddField(
            model_name='visite',
            name='km',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=5, null=True),
        ),
    ]
