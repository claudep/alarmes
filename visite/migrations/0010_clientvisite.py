from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '__first__'),
        ('visite', '0009_inscription_date_quest_non_null'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientVisite',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('client.client',),
        ),
    ]
