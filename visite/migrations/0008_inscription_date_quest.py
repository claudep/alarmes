from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0007_inscription'),
    ]

    operations = [
        migrations.AddField(
            model_name='inscription',
            name='date_quest',
            field=models.DateField(blank=True, null=True, verbose_name='Date de remplissage'),
        ),
    ]
