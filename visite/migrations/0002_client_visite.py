from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '__first__'),
        ('benevole', '__first__'),
        ('visite', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Visite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.DateTimeField(verbose_name='Début')),
                ('duree', models.DurationField(verbose_name='Durée')),
                ('compte_rendu', models.TextField(blank=True, verbose_name='Compte-rendu')),
                ('client', models.ForeignKey(
                    on_delete=models.deletion.PROTECT, related_name='visites', to='client.client'
                )),
                ('visiteur', models.ForeignKey(on_delete=models.deletion.PROTECT, to='benevole.benevole')),
            ],
        ),
    ]
