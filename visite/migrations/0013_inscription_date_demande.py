from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0012_inscription_compte_rendu'),
    ]

    operations = [
        migrations.AddField(
            model_name='inscription',
            name='date_demande',
            field=models.DateField(blank=True, null=True, verbose_name='Date de la demande'),
        ),
        migrations.AlterField(
            model_name='inscription',
            name='date_quest',
            field=models.DateField(blank=True, null=True, verbose_name='Date de remplissage'),
        ),
    ]
