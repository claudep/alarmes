from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0003_visite_km'),
    ]

    operations = [
        migrations.AddField(
            model_name='visite',
            name='vad_initiale',
            field=models.BooleanField(default=False, verbose_name='1ère VAD-inscription'),
        ),
    ]
