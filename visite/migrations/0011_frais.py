from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0010_clientvisite'),
    ]

    operations = [
        migrations.CreateModel(
            name='Frais',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descriptif', models.TextField(verbose_name='Descriptif')),
                ('cout', models.DecimalField(decimal_places=2, max_digits=5, verbose_name='Coût')),
                ('typ', models.CharField(
                    choices=[('transp', 'Transports publics'), ('park', 'Parking')],
                    max_length=6, verbose_name='Type de frais')
                ),
                ('justif', models.FileField(blank=True, upload_to='justificatifs', verbose_name='Justificatif')),
                ('visite', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='frais', to='visite.visite')),
            ],
            options={
                'verbose_name': 'Frais',
                'verbose_name_plural': 'Frais',
            },
        ),
    ]
