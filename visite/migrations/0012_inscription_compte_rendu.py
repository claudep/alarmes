from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0011_frais'),
    ]

    operations = [
        migrations.AddField(
            model_name='inscription',
            name='compte_rendu',
            field=models.TextField(blank=True, verbose_name='Compte-rendu de visite (non visible par bénévoles)'),
        ),
    ]
