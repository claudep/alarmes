from django.db import migrations
from django.db.models import Count, Q

from common.choices import Services


def create_prestations(apps, schema_editor):
    return  # Plus besoin de la migration, et type_client n'existe plus

    Client = apps.get_model('client', 'Client')
    Prestation = apps.get_model('client', 'Prestation')

    # visites
    for client in Client.objects.annotate(num_attr=Count('attributions')).filter(
        Q(type_client__contains=['visite']) | Q(num_attr__gt=0)
    ):
        if client.num_attr == 0:
            debut = client.journaux.order_by('quand').first().quand.date()  # création client
            fin = client.archive_le
        else:
            debut = client.attributions.order_by('debut')[0].debut
            if client.archive_le:
                fin = client.archive_le
            else:
                dates_fin = [attr.fin for attr in client.attributions.all()]
                if None in dates_fin or 'visite' in client.type_client:
                    fin = None
                else:
                    fin = sorted(dates_fin)[-1]
        Prestation.objects.create(client=client, service=Services.VISITE, duree=(debut, fin))


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0018_prestation'),
        ('visite', '0004_visite_vad_initiale'),
    ]

    operations = [
        migrations.RunPython(create_prestations, migrations.RunPython.noop),
    ]
