from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visite', '0005_create_client_prestations'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visite',
            name='duree',
            field=models.DurationField(blank=True, null=True, verbose_name='Durée'),
        ),
    ]
