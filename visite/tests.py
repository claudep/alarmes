from datetime import date, datetime, timedelta, timezone

from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.timezone import make_aware, now

from freezegun import freeze_time

from benevole.models import Benevole, TypeActivite
from client.models import Client
from common.choices import Services
from common.export import openxml_contenttype
from common.models import Utilisateur
from common.stat_utils import Month
from common.test_utils import BaseDataMixin

from .forms import AttributionForm, VisiteForm
from .models import (
    ACTIVITE_INSCRIPTEUR, ACTIVITE_VISITE, Attribution, Frais, Inscription,
    Visite
)

VISITE = Services.VISITE


class VisiteTestsMixin(BaseDataMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        TypeActivite.objects.create(code=ACTIVITE_INSCRIPTEUR, nom='Visites initiales', service=VISITE)
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            Permission.objects.get(content_type__model='client', codename='view_client'),
            Permission.objects.get(content_type__model='client', codename='change_client'),
            Permission.objects.get(content_type__model='benevole', codename='view_benevole'),
            Permission.objects.get(content_type__model='benevole', codename='change_benevole'),
            Permission.objects.get(content_type__model='visite', codename='view_visite'),
            Permission.objects.get(content_type__model='visite', codename='add_visite'),
            Permission.objects.get(content_type__model='attribution', codename='view_attribution'),
            Permission.objects.get(content_type__model='attribution', codename='add_attribution'),
        )
        cls.visiteur1 = Benevole.objects.create(
            nom='Valjean', prenom='Jean', npa='2300', activites=[ACTIVITE_VISITE]
        )
        cls.visiteur2 = Benevole.objects.create(
            nom='Doe', prenom='Kyle', npa='2000', activites=[ACTIVITE_VISITE]
        )


class VisiteTests(VisiteTestsMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        Inscription.objects.create(client=cls.cl_visite, date_quest=date(2024, 1, 2))

    def test_create_client(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Trucchouette',
            'prenom': 'Nadine',
            'genre': 'F',
            'rue': 'Grand-Rue 17',
            'npalocalite': '2000 Neuchâtel',
            'remarques_int_visite': "Pour beurre",
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 0,
        })
        self.assertEqual(response.status_code, 302)
        client = Client.objects.get(persona__nom='Trucchouette')
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual(client.prestations_actuelles(as_services=True), ['visite'])

    def test_convert_client_to_visite(self):
        self.client.force_login(self.user)
        client = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=Services.ALARME,
        )
        edit_url = reverse("client-edit", args=[client.pk])
        response = self.client.post(
            reverse("client-add-service", args=[client.pk]),
            data={}, headers={"referer": edit_url}
        )
        self.assertRedirects(response, edit_url)
        client.refresh_from_db()
        self.assertEqual(sorted(client.prestations_actuelles(as_services=True)), ["alarme", "visite"])
        journal = client.journaux.latest()
        self.assertEqual(journal.description, "Ajout type de client «visite»")

    def test_client_liste(self):
        Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel", service=Services.ALARME,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients'))
        self.assertQuerySetEqual(response.context['object_list'], [self.cl_visite])
        response = self.client.get(reverse('clients') + '?nom=Doe')
        self.assertQuerySetEqual(response.context['object_list'], [self.cl_visite])
        response = self.client.get(reverse('clients') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_client_liste_attente(self):
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        cl_sans_attrib = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        cl_attrib_passee = Client.objects.create(
            nom="Smith", prenom="John", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        Inscription.objects.create(client=cl_attrib_passee, date_quest=date.today() - timedelta(days=60))
        Attribution.objects.create(
            client=cl_attrib_passee, visiteur=self.visiteur1,
            debut=date.today() - timedelta(days=60), fin=date.today() - timedelta(days=4),
        )
        cl_apres_inscr = Client.objects.create(
            nom="Smith", prenom="Gill", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        Inscription.objects.create(client=cl_apres_inscr, date_quest=date.today() - timedelta(days=30))
        Visite.objects.create(
            client=cl_apres_inscr, visiteur=self.visiteur1,
            debut=now() - timedelta(days=30), duree='01:45', vad_initiale=True,
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-attente'))
        self.assertQuerySetEqual(response.context['attente_binome'], [cl_apres_inscr])
        self.assertQuerySetEqual(response.context['attente_prochain'], [cl_attrib_passee])
        self.assertContains(response, "2000 Neuchâtel")

    def test_client_attribs(self):
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("client-attribs", args=[self.cl_visite.pk]))
        self.assertContains(response, "<td><i>En cours</i></td>")

    def test_client_archives(self):
        Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        cl_archive = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        cl_archive.prestations.update(duree=('2024-01-01', '2024-04-12'))
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-archives'))
        self.assertQuerySetEqual(response.context['object_list'], [cl_archive])
        response = self.client.get(reverse('clients-archives') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_client_inscription(self):
        visite = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=now() - timedelta(days=30), duree='01:45', vad_initiale=True,
        )
        self.client.force_login(self.user)
        inscr_url = reverse('client-inscription', args=[self.cl_visite.pk])
        response = self.client.get(inscr_url)
        self.assertContains(response, "Quels sont vos intérêts ?")
        # L'auteur de la visite d'inscription apparaît.
        self.assertContains(response, "par Valjean Jean")
        response = self.client.post(inscr_url, data={
            # sous-ensemble de champs
            'date_quest': '14.05.2024',
            'dispos': "Le jeudi matin",
            'frequence': "Si possible chaque semaine",
            'types_visites': ['prom', 'lecture'],
            'etat_sante': "Diabète",
            'famille': "Plus de famille dans la région",
            'remarques': "Voir dossier",
        })
        self.assertRedirects(response, inscr_url)
        self.cl_visite.refresh_from_db()
        self.assertEqual(self.cl_visite.inscription.dispos, "Le jeudi matin")

    def test_benevole_list(self):
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['visite']))
        self.assertEqual(response.status_code, 200)
        self.assertQuerySetEqual(response.context['object_list'], [self.visiteur2, self.visiteur1])
        response = self.client.get(reverse('benevoles', args=['visite']) + '?sans_visite=on')
        self.assertQuerySetEqual(response.context['object_list'], [self.visiteur2])

    def test_benevole_edit(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-edit', args=['visite', self.visiteur1.pk]))
        self.assertContains(response, self.visiteur1.nom)

    def test_benevole_note_frais(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-notesfrais', args=['visite', self.visiteur1.pk]))
        self.assertContains(response, "Notes de frais pour Valjean Jean")
        # Ajout visite avec kms + frais
        vis = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=now().replace(day=1) - timedelta(days=3), duree='01:45', km=12.5
        )
        Frais.objects.create(visite=vis, typ='park', cout='2.45')
        Frais.objects.create(visite=vis, typ='transp', cout='11.30')
        vis2 = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=now().replace(day=1) - timedelta(days=1), duree='01:45', km=3
        )
        Frais.objects.create(visite=vis2, typ='park', cout='0.75')
        response = self.client.get(reverse('benevole-notesfrais', args=['visite', self.visiteur1.pk]))
        self.assertContains(response, '<div>Transports publics, 11,30</div>', html=True)
        self.assertContains(response, '<div>Frais de parking, 3,20</div>', html=True)
        self.assertContains(response, '<div>Indemnités kilométriques, 15,5</div>', html=True)

    def test_visite_ajout(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('visite-new'))
        self.assertIn('vad_initiale', response.context['form'].fields)
        response = self.client.get(reverse('visite-new'))
        # Pas de formset pour les frais.
        self.assertIsNone(response.context['form'].formset)
        visite_data = {
            'client': str(self.cl_visite.pk),
            'visiteur': str(self.visiteur1.pk),
            'debut_0': '5.07.2023',
            'debut_1': '14:00',
            'duree': '2:00',
            'km': '18',
            'compte_rendu': '',
        }
        response = self.client.post(reverse('visite-new'), data=visite_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        visite = self.cl_visite.visites.first()
        self.assertEqual(visite.debut, datetime(2023, 7, 5, 12, 0, tzinfo=timezone.utc))
        self.assertEqual(visite.km, 18)
        # Pas de double visite en même temps pour le même binôme.
        response = self.client.post(reverse('visite-new'), data=visite_data)
        self.assertContains(response, "Cette visite existe déjà.")

    def test_visite_ajout_duree_0(self):
        form = VisiteForm(initial={}, data={
            'client': str(self.cl_visite.pk), 'visiteur': str(self.visiteur1.pk),
            'debut_0': '5.04.2024', 'debut_1': '14:00',
            'duree': '0:00',
        })
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(self.cl_visite.visites.first().duree, timedelta(0))

    def test_visite_ajout_attrib_existante(self):
        Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=now() - timedelta(days=30), duree='01:45'
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('visite-new') + f'?client={self.cl_visite.pk}&benevole={self.visiteur1.pk}'
        )
        self.assertNotIn('vad_initiale', response.context['form'].fields)
        self.assertEqual(response.context['form'].initial['client'], self.cl_visite)
        self.assertEqual(response.context['form'].initial['visiteur'], str(self.visiteur1.pk))

    def test_premiere_vad_avec_frais(self):
        visite = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=now() - timedelta(days=30), duree='01:45', vad_initiale=True,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('visite-edit', args=[visite.pk]), data={
            'client': str(visite.client_id),
            'visiteur': str(visite.visiteur_id),
            'debut_0': '4.07.2024',
            'debut_1': '14:00',
            'duree': '2:00',
            'km': '18',
            'vad_initiale': True,
            'compte_rendu': 'OK',
            'frais-TOTAL_FORMS': 1,
            'frais-INITIAL_FORMS': 0,
            'frais-0-id': '',
            'frais-0-visite': str(visite.pk),
            'frais-0-typ': 'transp',
            'frais-0-cout': '12.60',
            'frais-0-justif': '',
        })
        self.assertEqual(response.json()['result'], "OK")
        self.assertEqual(visite.frais.count(), 1)

    def test_visite_list(self):
        # Liste principale de toutes les visites
        v1 = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=make_aware(datetime(2023, 6, 25, 12, 0)), duree=timedelta(hours=2)
        )
        v2 = Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur2,
            debut=make_aware(datetime(2023, 7, 8, 14, 30)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('visites'))
        self.assertQuerySetEqual(response.context['object_list'], [v2, v1])
        response = self.client.get(reverse('visites') + '?nom_client=Truc')
        self.assertQuerySetEqual(response.context['object_list'], [v2, v1])
        response = self.client.get(reverse('visites') + f'?visiteur={self.visiteur1.pk}')
        self.assertQuerySetEqual(response.context['object_list'], [v1])
        with freeze_time("2024-01-05"):
            response = self.client.get(reverse('visites') + '?mois=7.2023')
            self.assertQuerySetEqual(response.context['object_list'], [v2])
        # Liste des visites par client
        response = self.client.get(reverse('client-visites', args=[self.cl_visite.pk]))
        self.assertQuerySetEqual(response.context['object_list'], [v2, v1])

    def test_attribution_list(self):
        attr = Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        attr2 = Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur2,
            debut=date.today() - timedelta(days=360), fin=date.today() - timedelta(days=160),
            motif_fin="Partie en home",
        )
        last_m = date.today().replace(day=25) - timedelta(days=30)
        Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=make_aware(datetime(last_m.year, last_m.month, 25, 12, 0)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('attributions', args=['encours']))
        self.assertQuerySetEqual(response.context['object_list'], [attr])
        self.assertContains(response, "02:00")
        response = self.client.get(reverse('attributions', args=['encours']) + "?nom_client=xy")
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('attributions', args=['encours']) + "?nom_visiteur=val")
        self.assertQuerySetEqual(response.context['object_list'], [attr])
        response = self.client.get(reverse('attributions', args=['terminees']))
        self.assertQuerySetEqual(response.context['object_list'], [attr2])
        response = self.client.get(reverse('attributions', args=['terminees']) + "?export=1")
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        # Onglet binôme du bénévole
        response = self.client.get(reverse('benevole-binomes', args=[self.visiteur1.pk]))
        self.assertContains(response, "en cours…")

    def test_attribution_form(self):
        attr = Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        attr_form = AttributionForm(instance=attr)
        self.assertNotIn('client', attr_form.fields)
        self.assertIn(
            f'<option value="{self.visiteur1.pk}" selected>Valjean Jean</option>',
            attr_form.as_div()
        )
        ancien_visiteur = Benevole.objects.create(nom='Old', prenom='Guy', npa='2000')
        attr.visiteur = ancien_visiteur
        attr.save()
        attr_form = AttributionForm(instance=attr)
        self.assertIn(
            f'<option value="{ancien_visiteur.pk}" selected>Old Guy</option>',
            attr_form.as_div()
        )

    def test_attribution_fin(self):
        attr = Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('attribution-edit', args=[attr.pk]))
        self.assertContains(response, "Client: <b>Trucmuche Oscar</b>")
        response = self.client.post(reverse('attribution-edit', args=[attr.pk]), data={
            'visiteur': self.visiteur1.pk,
            'debut': attr.debut, 'fin': date.today(), 'motif_fin': "Le bénévole arrête",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        # Le client est de retour en liste d'attente
        response = self.client.get(reverse('clients-attente'))
        self.assertQuerySetEqual(response.context['attente_prochain'], [self.cl_visite])

    def test_stats(self):
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=date(2023, 6, 1),
        )
        Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur1,
            debut=make_aware(datetime(2023, 6, 25, 12, 0)), duree=timedelta(hours=2),
            vad_initiale=True
        )
        Visite.objects.create(
            client=self.cl_visite, visiteur=self.visiteur2,
            debut=make_aware(datetime(2023, 7, 8, 14, 30)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.user)
        url = reverse('stats-visites') + '?start_month=6&start_year=2023&end_month=8&end_year=2023'
        response = self.client.get(url)
        months = Month(year=2023, month=6), Month(year=2023, month=7), Month(year=2023, month=8)
        self.maxDiff = None
        self.assertEqual(
            response.context['stats'],
            {
                'binomes': {months[0]: 1, months[1]: 1, months[2]: 1, 'total': 1},
                'clients': {months[0]: 0, months[1]: 1, months[2]: 0, 'total': 1},
                'visiteurs': {months[0]: 0, months[1]: 1, months[2]: 0, 'total': 1},
                'nb_visites': {months[0]: 0, months[1]: 1, months[2]: 0, 'total': 1},
                'vad_initiales': {months[0]: 1, months[1]: 0, months[2]: 0, 'total': 1},
                'duree_heures1': {
                    months[0]: timedelta(seconds=7200), months[1]: timedelta(0),
                    months[2]: timedelta(0), 'total': timedelta(seconds=7200)
                },
                'duree_heures': {
                    months[0]: timedelta(0), months[1]: timedelta(seconds=7200),
                    months[2]: timedelta(0), 'total': timedelta(seconds=7200)
                },
            }
        )
        response = self.client.get(url + '&export=1')
        self.assertEqual(response['Content-Type'], openxml_contenttype)


class InscripteurTests(VisiteTestsMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        cls.inscripteur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", utilisateur=user_benev,
            activites=[ACTIVITE_INSCRIPTEUR],
        )

    @override_settings(EXEMPT_2FA_NETWORKS=[])
    def test_first_login(self):
        response = self.client.get(reverse('home-app'))
        # Login with django-two-factor-auth
        expected_url = "/accounts/login/?next=/app/"
        self.assertRedirects(response, expected_url)
        response = self.client.post(expected_url, data={
            'auth-username': 'benev@example.org',
            'auth-password': 'mepassword',
            'login_view-current_step': 'auth',
        }, follow=True)
        self.assertContains(response, "Activer l'authentification à deux facteurs")
        self.assertEqual(response.redirect_chain, [('/app/', 302), ('/account/two_factor/setup/', 302)])

    def test_login(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )

        self.client.force_login(self.inscripteur.utilisateur)
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Bénéficiaires en attente')
        self.assertQuerySetEqual(response.context['en_attente'], [cl_visite])
        self.assertContains(response, '2000 Neuchâtel')

    def test_client_edit_access(self):
        cl_en_attente = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Irène", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        Attribution.objects.create(
            client=cl_visite, visiteur=self.inscripteur, debut=date.today() - timedelta(days=60)
        )

        self.client.force_login(self.inscripteur.utilisateur)
        # Accès OK au client en attente
        response = self.client.get(reverse('client-edit', args=[cl_en_attente.pk]))
        self.assertContains(response, "Trucmuche Oscar")
        # Accès refusé au client avec attribution
        response = self.client.get(reverse('client-edit', args=[cl_visite.pk]))
        self.assertEqual(response.status_code, 403)

    def test_ajout_visite(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Irène", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        self.client.force_login(self.inscripteur.utilisateur)
        visite_data = {
            'client': str(cl_visite.pk),
            'visiteur': str(self.inscripteur.pk),
            'debut_0': date.today() + timedelta(days=1),
            'debut_1': '14:00',
        }
        response = self.client.post(reverse('visite-new'), data=visite_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})


class BenevoleTests(BaseDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Irma', last_name='Duplain',
        )
        cls.benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", utilisateur=user_benev,
            activites=[ACTIVITE_VISITE],
        )
        cls.cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )

    def test_home(self):
        self.client.force_login(self.benev.utilisateur)
        home_url = reverse('home-app')
        response = self.client.get(home_url)
        self.assertContains(response, 'Portail bénévoles')
        self.assertContains(response, 'Aucune visite ne vous est attribuée.')
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.benev, debut=date.today() - timedelta(days=60)
        )
        response = self.client.get(home_url)
        self.assertContains(response, 'Trucmuche Oscar')
        self.assertContains(response, '2000 Neuchâtel')
        self.assertNotContains(response, 'Données d’inscription')
        Inscription.objects.create(
            client=self.cl_visite, date_quest=date.today(),
            dispos="Tous les jours", compte_rendu="Pas commode"
        )
        response = self.client.get(home_url)
        self.assertContains(response, 'Données d’inscription')
        self.assertContains(response, 'Tous les jours')
        self.assertNotContains(response, 'Pas commode')

    def test_creation_visite(self):
        attr = Attribution.objects.create(
            client=self.cl_visite, visiteur=self.benev, debut=date.today() - timedelta(days=60)
        )
        self.client.force_login(self.benev.utilisateur)
        form_data = {
            "debut_0": (date.today() + timedelta(days=10)).strftime("%d.%m.%Y"),
            "debut_1": "14:00",
        }
        response = self.client.post(reverse('app-visite-new', args=[attr.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(self.benev.visite_set.count(), 1)
        response = self.client.post(reverse('app-visite-new', args=[attr.pk]), data=form_data)
        self.assertContains(response, "Cette visite existe déjà")

    def test_suppression_visite(self):
        visite = Visite.objects.create(
            client=self.cl_visite, visiteur=self.benev, debut=now(),
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.post(reverse('app-visite-delete', args=[visite.pk]), data={})
        self.assertEqual(response.json()['result'], 'OK')

    def test_visite_a_rapporter(self):
        Attribution.objects.create(
            client=self.cl_visite, visiteur=self.benev, debut=date.today() - timedelta(days=60)
        )
        visite = Visite.objects.create(
            client=self.cl_visite, visiteur=self.benev, debut=now() - timedelta(days=1),
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        self.assertContains(
            response, f"Rapporter la visite du {visite.debut.astimezone().strftime('%d.%m.%Y à %H:%M')}"
        )

    def test_can_edit_visite_par_benevole(self):
        visite = Visite(client=self.cl_visite, visiteur=self.benev, debut=datetime(2024, 5, 10, 14, 30))
        with freeze_time("2024-06-05"):
            self.assertTrue(visite.can_edit(self.benev))
        with freeze_time("2024-06-10"):
            self.assertFalse(visite.can_edit(self.benev))

    def test_archives_list(self):
        Visite.objects.create(
            client=self.cl_visite, visiteur=self.benev,
            debut=make_aware(datetime(2024, 4, 8, 14, 30)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('app-archives'))
        self.assertContains(response, 'avril 2024')
        self.assertContains(response, reverse('app-archives-month', args=['2024', '4']))


class AdminTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        TypeActivite.objects.get_or_create(code=ACTIVITE_VISITE, nom='Visites', service=VISITE)
        cls.user = Utilisateur.objects.create_superuser(
            'me@example.org', 'mepassword', first_name='Super', last_name='Admin',
        )
        visiteur = Benevole.objects.create(
            nom='Valjean', prenom='Jean', npa='2300', activites=[ACTIVITE_VISITE]
        )
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", service=VISITE,
        )
        Visite.objects.create(
            client=cl_visite, visiteur=visiteur,
            debut=make_aware(datetime(2024, 4, 8, 14, 30)), duree=timedelta(hours=2)
        )

    def test_client_persona_admin(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("admin:visite_visite_changelist") + "?q=Truc")
        self.assertEqual(len(response.context["results"]), 1)
