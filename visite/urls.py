from django.apps import apps
from django.conf import settings
from django.urls import include, path
from django.views.decorators.cache import cache_page

from common.views import ManifestView
from visite import views, views_app, views_stats

handler500 = 'common.views.error_view'

urlpatterns = [
    path('benevoles/visite/', views.BenevoleListView.as_view(), {'app': 'visite'}, name='benevoles'),
    path('benevoles/visite/<int:pk>/edition/', views.BenevoleEditView.as_view(is_create=False), {'app': 'visite'},
        name='benevole-edit'),
    path('benevoles/<int:pk>/binomes/', views.BenevoleBinomesView.as_view(), name='benevole-binomes'),

    path('', include('common.urls')),
    path('', include('client.urls')),
    path('', include(f'{settings.CANTON_APP}.urls')),

    path('', views.home, name='home'),
    path('clients/', views.ClientListView.as_view(), name='clients'),
    path('clients/archives/', views.ClientListView.as_view(is_archive=True), name='clients-archives'),
    path('clients/new/', views.ClientEditView.as_view(is_create=True), name='client-new'),
    path('clients/<int:pk>/edition/', views.ClientEditView.as_view(is_create=False), name='client-edit'),
    path('clients/<int:pk>/attribs/', views.ClientAttributionsView.as_view(), name='client-attribs'),
    path('clients/<int:pk>/visites/', views.ClientVisitesView.as_view(), name='client-visites'),
    path('clients/<int:pk>/inscription/', views.ClientInscriptionView.as_view(), name='client-inscription'),
    path('clients/attente/', views.ClientsAttenteView.as_view(), name='clients-attente'),
    path('attributions/nouvelle/', views.AttributionEditView.as_view(is_create=True), name='attribution-new'),
    path('attributions/<int:pk>/edition/', views.AttributionEditView.as_view(is_create=False),
        name='attribution-edit'),
    path('attributions/<str:status>/', views.AttributionsView.as_view(), name='attributions'),
    path('visites/', views.VisitesView.as_view(), name='visites'),
    path('visites/nouvelle/', views.VisiteEditView.as_view(is_create=True), name='visite-new'),
    path('visites/<int:pk>/edition/', views.VisiteEditView.as_view(is_create=False),
        name='visite-edit'),
    path('visites/<int:pk>/supprimer/', views.VisiteDeleteView.as_view(), name='visite-delete'),

    # Statistiques
    path('stats/', views_stats.StatsVisitesView.as_view(), name='stats-visites'),

    # Application bénévoles
    path('app/', views.home_visiteur, name='home-app'),
    path('app/manifest.json',
        cache_page(3600 * 24)(ManifestView.as_view(template_name="visite/manifest.json")),
        name='manifest'),
    path('app/visite/<int:attribpk>/nouvelle/', views_app.VisiteEditView.as_view(is_create=True),
        name='app-visite-new'),
    path('app/visite/<int:pk>/edition/', views_app.VisiteEditView.as_view(is_create=False),
        name='app-visite-edit'),
    path('app/visite/<int:pk>/supprimer/', views.VisiteDeleteView.as_view(),
        name='app-visite-delete'),
    path('app/archives/', views_app.BenevoleArchivesView.as_view(), name='app-archives'),
    path('app/archives/<int:year>/<int:month>/', views_app.BenevoleArchivesMonthView.as_view(),
        name='app-archives-month'),
]

if apps.is_installed('besoins'):
    urlpatterns.append(
        path('besoins/', include('besoins.urls_app'))
    )
