from django.contrib import admin

from .models import Attribution, Frais, Inscription, Visite


@admin.register(Attribution)
class AttributionAdmin(admin.ModelAdmin):
    list_display = ['client', 'visiteur', 'debut', 'fin', 'remarques']
    search_fields = ['client__persona__nom', 'visiteur__persona__nom']
    raw_id_fields = ['client', 'visiteur']


@admin.register(Frais)
class FraisAdmin(admin.ModelAdmin):
    list_display = ['benevole', 'typ', 'cout']

    def benevole(self, obj):
        return obj.visite.visiteur  # visite__visiteur in list_display in 5.1


@admin.register(Inscription)
class InscriptionAdmin(admin.ModelAdmin):
    list_display = ['client', 'date_quest']
    raw_id_fields = ['client']


@admin.register(Visite)
class VisiteAdmin(admin.ModelAdmin):
    list_display = ['client', 'visiteur', 'debut', 'duree']
    list_filter = ['vad_initiale']
    date_hierarchy = 'debut'
    raw_id_fields = ['client', 'visiteur']
    search_fields = ['client__persona__nom', 'visiteur__persona__nom']
