from .views import visite_profil


def visite(request):
    return {
        'profil': visite_profil(request.user),
    }
