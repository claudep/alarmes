import calendar
from datetime import date

from dateutil.relativedelta import relativedelta

from django import forms
from django.db.models import Q
from django.utils.dates import MONTHS
from django.utils.timezone import now

from benevole.forms import BenevoleForm, BenevoleFilterForm as BenevoleFilterFormBase
from benevole.models import Benevole
from client.forms import ClientEditFormBase
from client.models import Client
from common.choices import Services
from common.forms import (
    BootstrapMixin, DurationInput, FormsetMixin, HiddenDeleteInlineFormSet,
    HMDurationField, ModelForm, NPALocaliteMixin, PriceInput, SplitDateTimeWidget
)
from .models import Attribution, Frais, Inscription, Visite


class ClientEditForm(NPALocaliteMixin, ClientEditFormBase):
    class Meta(ClientEditFormBase.Meta):
        fields = [
            'handicaps', 'type_logement', 'type_logement_info', 'etage', 'nb_pieces', 'ascenseur',
            'code_entree', 'remarques_int_visite', 'remarques_ext_visite',
        ]

    persona_fields = [
        'nom', 'prenom', 'genre', 'date_naissance', 'case_postale',
        'courriel', 'langues_select', 'langues', 'date_deces',
    ]
    benev_hidden_fields = {'code_entree', 'remarques_int_visite'}
    field_order = [
        'nom', 'prenom', 'genre', 'date_naissance', 'rue', 'npalocalite',
    ]


class BenevoleVisiteForm(BenevoleForm):
    class Meta(BenevoleForm.Meta):
        fields = BenevoleForm.Meta.fields + ['prefs_visite']


class BenevoleFilterForm(BenevoleFilterFormBase):
    sans_visite = forms.BooleanField(label="Sans visite", required=False)

    def filter(self, benevs):
        benevs = super().filter(benevs)
        if self.cleaned_data['sans_visite']:
            benevs = benevs.filter(num_visites=0)
        return benevs


class InscriptionForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Inscription
        fields = [
            'date_demande', 'date_quest', 'parcours_vie', 'dispos', 'frequence',
            'types_visites', 'pref_benevole', 'etat_sante', 'interets',
            'demande_par', 'services', 'famille', 'remarques', 'compte_rendu',
        ]
        labels = {
            'dispos': "À quel moment êtes-vous disponible pour recevoir de la visite ?",
            'frequence': "À quelle fréquence souhaiteriez-vous recevoir des visites ?",
            'pref_benevole': "Préférence pour la personne bénévole (homme, femme, etc.)",
            'etat_sante': "Quel est votre état de santé (mobilité, indépendance) ?",
            'interets': "Quels sont vos intérêts ?",
            'demande_par': "La demande de visite a été faite par",
            'services': "Services à domiciles (infirmières, aide familiale, repas, etc.)",
            'famille': "Votre famille (enfants, petits-enfants, …)",
            'remarques': "Autres remarques, divers",
        }

    def clean(self):
        super().clean()
        if (
            any(self.cleaned_data.get(fname) for fname in [f for f in self.fields if f != 'date_demande'])
            and not self.cleaned_data.get('date_quest')
        ):
            self.add_error('date_quest', "Veuillez remplir la date de remplissage de l’inscription")


class AttributionForm(BootstrapMixin, ModelForm):
    client = forms.ModelChoiceField(
        queryset=Client.objects.par_service(Services.VISITE).order_by('persona__nom'),
    )
    visiteur = forms.ModelChoiceField(
        queryset=Benevole.objects.par_domaine('visite'),
    )

    class Meta(BootstrapMixin.Meta):
        model = Attribution
        fields = ['client', 'visiteur', 'debut', 'fin', 'motif_fin', 'en_pause', 'remarques']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance.pk:
            del self.fields['client']
            self.fields['visiteur'].queryset = Benevole.objects.par_domaine(
                'visite', include=self.instance.visiteur
            )


class AttributionFilterForm(BootstrapMixin, forms.Form):
    nom_client = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}
        ),
        required=False,
    )
    nom_visiteur = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom visiteur', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}
        ),
        required=False,
    )

    def filter(self, visites):
        if self.cleaned_data['nom_client']:
            term = self.cleaned_data['nom_client'].split()[0]
            visites = visites.filter(
                Q(client__persona__nom__unaccent__icontains=term) |
                Q(client__persona__prenom__unaccent__icontains=term)
            )
        if self.cleaned_data['nom_visiteur']:
            term = self.cleaned_data['nom_visiteur'].split()[0]
            visites = visites.filter(
                Q(visiteur__persona__nom__unaccent__icontains=term) |
                Q(visiteur__persona__prenom__unaccent__icontains=term)
            )
        return visites


class FraisMissionForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Frais
        fields = ['cout', 'typ', 'justif']
        widgets = {
            'cout': PriceInput,
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('cout') == 0:
            self.add_error(None, "Si vous n’avez pas de frais, laissez plutôt ces champs vides.")
        return cleaned_data


class VisiteForm(BootstrapMixin, FormsetMixin, ModelForm):
    client = forms.ModelChoiceField(
        queryset=Client.objects.par_service(Services.VISITE).order_by('persona__nom')
    )
    visiteur = forms.ModelChoiceField(queryset=None)
    duree = HMDurationField(label="Durée", required=False)

    class Meta(BootstrapMixin.Meta):
        model = Visite
        fields = ['client', 'visiteur', 'debut', 'duree', 'km', 'compte_rendu', 'vad_initiale']
        widgets = {
            'debut': SplitDateTimeWidget,
        }
        field_classes = {
            'debut': forms.SplitDateTimeField,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['visiteur'].queryset = Benevole.objects.par_domaine('visite')
        if kwargs['initial'].get('client') and kwargs['initial']['client'].visites.exists():
            del self.fields['vad_initiale']
        if self.instance.vad_initiale:
            FraisFormSet = forms.inlineformset_factory(
                Visite, Frais, form=FraisMissionForm, formset=HiddenDeleteInlineFormSet,
                extra=1
            )
            self.formset = FraisFormSet(**kwargs)
        else:
            self.formset = None

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['debut'] > now() and cleaned_data.get('duree') is not None:
            self.add_error(
                'duree',
                "Il n’est pas autorisé de mettre une durée pour une visite qui n'a pas encore eu lieu"
            )
        if cleaned_data['debut'] < now() and cleaned_data.get('duree') is None:
            self.add_error(
                'duree',
                "La durée est obligatoire pour les visites déjà effectuées"
            )
        return cleaned_data


class VisiteBenevForm(BootstrapMixin, ModelForm):
    """Formulaire simplifié pour bénévoles visiteurs."""
    duree = HMDurationField(
        label="Durée",
        widget=DurationInput(attrs={'class': 'inline', 'placeholder': 'HH:MM'}),
        required=False,
    )

    class Meta(BootstrapMixin.Meta):
        model = Visite
        fields = ['debut', 'duree', 'compte_rendu']
        widgets = {
            'debut': SplitDateTimeWidget,
        }
        field_classes = {
            'debut': forms.SplitDateTimeField,
        }
        labels = {
            'debut': "Le",
            'compte_rendu': "Compte-rendu (facultatif)",
        }


class VisiteFilterForm(BootstrapMixin, forms.Form):
    nom_client = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}
        ),
        required=False,
    )
    # Pas dans l'interface, seulement par querystring direct
    client = forms.ModelChoiceField(queryset=Client.objects.all(), required=False, widget=forms.HiddenInput)
    visiteur = forms.ModelChoiceField(queryset=None, required=False)
    mois = forms.ChoiceField(required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['visiteur'].queryset = Benevole.objects.par_domaine('visite')
        self.fields['mois'].choices = [('', '--------')] + [
            (f"{dt.month}.{dt.year}", f"{MONTHS[dt.month]} {dt.year}") for dt in [
                date.today() - relativedelta(months=i) for i in range(1, 12)
            ]
        ]

    def filter(self, visites):
        if self.cleaned_data['nom_client']:
            term = self.cleaned_data['nom_client'].split()[0]
            visites = visites.filter(
                Q(client__persona__nom__unaccent__icontains=term) | Q(client__persona__prenom__unaccent__icontains=term)
            )
        if self.cleaned_data['client']:
            visites = visites.filter(client=self.cleaned_data['client'])
        if self.cleaned_data['visiteur']:
            visites = visites.filter(visiteur=self.cleaned_data['visiteur'])
        if self.cleaned_data['mois']:
            mois_num, year = [int(part) for part in self.cleaned_data['mois'].split('.')]
            last_day_of_month = calendar.monthrange(year, mois_num)[1]
            visites = visites.filter(
                debut__date__range=(date(year, mois_num, 1), date(year, mois_num, last_day_of_month))
            )
        return visites
