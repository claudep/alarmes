from datetime import date, timedelta
from decimal import Decimal

from django.db import models
from django.db.models import Count, F, OuterRef, Q, Subquery, Sum, Value
from django.db.models.functions import TruncMonth

from benevole.models import Benevole, CalculateurFraisBase, LigneFrais, TypeFrais
from client.models import Client, Services
from common.fields import ChoiceArrayField
from common.utils import canton_app

ACTIVITE_VISITE = "visites"
ACTIVITE_INSCRIPTEUR = "visites1"


class ClientVisite(Client):
    class Meta:
        proxy = True

    def date_demande(self):
        try:
            prest = self.prestations_actuelles(limit_to='visite')[0]
        except IndexError:
            return None
        return prest.duree.lower


class Inscription(models.Model):
    class TypesChoices(models.TextChoices):
        PRESENCE = 'presence', "Présence/Écoute"
        PROMENADES = 'prom', "Promenades"
        LECTURE = 'lecture', "Lecture"
        JEUX = 'jeux', "Jeux"

    client = models.OneToOneField(Client, on_delete=models.CASCADE)
    date_demande = models.DateField("Date de la demande", null=True, blank=True)
    date_quest = models.DateField("Date de remplissage", null=True, blank=True)
    parcours_vie = models.TextField("Profession et parcours de vie", blank=True)
    dispos = models.TextField("Disponibilités", blank=True)
    frequence = models.CharField("Fréquence de visites", max_length=200, blank=True)
    types_visites = ChoiceArrayField(
        models.CharField(max_length=10, choices=TypesChoices),
        verbose_name="Activités souhaitées durant les visites", blank=True, default=list,
    )
    pref_benevole = models.CharField("Préférence bénévole", max_length=200, blank=True)
    etat_sante = models.TextField("État de santé", blank=True)
    interets = models.TextField("Intérêts", blank=True)
    demande_par = models.CharField("Demande par", max_length=150, blank=True)
    services = models.TextField("Services à domicile", blank=True)
    famille = models.TextField("Entourage familial", blank=True)
    remarques = models.TextField("Remarques", blank=True)
    compte_rendu = models.TextField("Compte-rendu de visite (non visible par bénévoles)", blank=True)

    def __str__(self):
        return f"Inscription visite pour {self.client}"

    @property
    def types_visites_verbose(self):
        return [dict(self.TypesChoices.choices)[v] for v in (self.types_visites or [])]

    def get_types_visites_display(self):
        return ', '.join(self.types_visites_verbose)


class Attribution(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='attributions')
    visiteur = models.ForeignKey(Benevole, on_delete=models.PROTECT)
    debut = models.DateField("Date début des visites")
    fin = models.DateField(null=True, blank=True)
    en_pause = models.BooleanField("En pause", default=False)
    motif_fin = models.TextField("Motif d’arrêt", blank=True)
    remarques = models.TextField("Remarques", blank=True)

    def __str__(self):
        return f"{self.visiteur} est visiteur/euse pour {self.client}"


class Visite(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='visites')
    visiteur = models.ForeignKey(Benevole, on_delete=models.PROTECT)
    debut = models.DateTimeField("Début")
    duree = models.DurationField("Durée", blank=True, null=True)
    km = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)
    compte_rendu = models.TextField("Compte-rendu", blank=True)
    vad_initiale = models.BooleanField("1ère VAD-inscription", default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["client", "visiteur", "debut"], name="visite_unique",
            ),
        ]

    def __str__(self):
        return f"Visite de {self.visiteur} à {self.client}, le {self.debut.strftime('%d.%m.%Y')}"

    def unique_error_message(self, *args, **kwargs):
        return "Cette visite existe déjà."

    def can_edit(self, user_or_benev):
        """
        Édition possible d’un rapport de visite par un bénévole.
        Dès le 6 d'un mois, seuls les transports du mois courant sont éditables.
        """
        if isinstance(user_or_benev, Benevole):
            if date.today().day < 6:
                un_du_mois = (date.today() - timedelta(days=6)).replace(day=1)
            else:
                un_du_mois = date.today().replace(day=1)
            return self.debut.date() >= un_du_mois and self.visiteur == user_or_benev
        else:
            return user_or_benev.has_perm("visite.change_visite")


class Frais(models.Model):
    visite = models.ForeignKey(Visite, related_name="frais", on_delete=models.CASCADE)
    descriptif = models.TextField("Descriptif")
    cout = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Coût")
    typ = models.CharField(
        "Type de frais", max_length=6, choices=[('transp', 'Transports publics'), ('park', 'Parking')]
    )
    justif = models.FileField(upload_to="justificatifs", blank=True, verbose_name="Justificatif")

    class Meta:
        verbose_name = "Frais"
        verbose_name_plural = "Frais"

    def __str__(self):
        return f"Frais ({self.cout}) pour la viste {self.visite}"

    def description(self):
        return self.descriptif or ("Repas" if self.typ == 'repas' else "")


class CalculateurFraisVisite(CalculateurFraisBase):
    service = 'visite'

    def frais_par_benevole(self, benevole=None):
        if benevole:
            base_qs = Visite.objects.filter(visiteur=benevole, duree__isnull=False)
            benevoles = {benevole.pk: benevole}
        else:
            base_qs = Visite.objects.filter(duree__isnull=False)
            benevoles = {benev.pk: benev for benev in Benevole.objects.filter(
                pk__in=base_qs.values_list('intervenant__benevole', flat=True)
            )}

        query = base_qs.annotate(
            month=TruncMonth('debut', output_field=models.DateField()),
            benevole__id=F('visiteur__id'),
            benevole__nom=F('visiteur__persona__nom'),
            benevole__prenom=F('visiteur__persona__prenom'),
            frais_transp_pub=Subquery(
                Frais.objects.filter(visite_id=OuterRef('pk'), typ='transp').values(
                    total_frais=Sum('cout')
                )[:1]
            ),
            frais_parking=Subquery(
                Frais.objects.filter(visite_id=OuterRef('pk'), typ='park').values(
                    total_frais=Sum('cout')
                )[:1]
            ),
        ).filter(
            month=self.mois
        ).values(
            'benevole__id', 'benevole__nom', 'benevole__prenom',
        ).annotate(
            kms=Sum('km', default=Decimal('0.0')),
            mois=Value(self.mois),
            transports_pub=Sum('frais_transp_pub', default=Decimal(0)),
            parking=Sum('frais_parking', default=Decimal(0)),
        ).distinct().order_by('benevole__nom', 'benevole__prenom')
        return {benevoles[line['benevole__id']]: line for line in query}

    def lignes_depuis_data(self, benevole, data):
        type_map = canton_app.TYPE_FRAIS_MAP
        lignes = []
        if data.get('parking', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['divers']),
                quantite=data['parking'],
            ))
        if data.get('transports_pub', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['transports_pub']),
                quantite=data['transports_pub'],
            ))
        if data['kms'] > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no=type_map['kms']),
                quantite=data['kms'],
            ))
        return lignes


class FacturationPolicy:
    def calculateur_frais(self, mois):
        return CalculateurFraisVisite(mois)


def clients_en_attente_inscription():
    """Ces clients sont visibles/éditables par le groupe des "inscripteurs"."""
    # Clients visite sans visite effectuée
    return ClientVisite.objects.par_service(Services.VISITE).avec_adresse(date.today()).annotate(
        num_attr_actives=Count('attributions', filter=Q(attributions__fin__isnull=True)),
        num_visites_effect=Count('visites', filter=Q(visites__duree__isnull=False)),
    ).filter(
        num_attr_actives=0, num_visites_effect=0
    )
