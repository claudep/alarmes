from django.template import Library

from common.utils import last_day_of_month

register = Library()


@register.simple_tag(takes_context=True)
def total_par_mois(context, mois, attrib):
    if attrib.debut > last_day_of_month(mois):
        return "-"
    # total_durees: voir AttributionsView.get_context_data
    return context['totaux_mensuels'].get(f"{str(mois)}-{attrib.client_id}-{attrib.visiteur_id}", (None, None))
