import math
from datetime import timedelta
from decimal import Decimal as D

from django.utils.dateformat import format as django_format

from benevole.models import LigneFrais, TypeFrais
from transport.models import (
    CalculateurFraisTransport, FacturationPolicyBase, Trajet, Transport, VehiculeOccup,
)
from .export import export_frais_benevoles


class CalculateurFrais(CalculateurFraisTransport):
    TARIF_CHAUFFEUR = D('0.70')
    TARIF_CHAUFFEUR_VEHICULE = D('0.30')
    FORFAIT_ATTENTE = D('15.00')

    def lignes_depuis_data(self, benevole, data):
        lignes = super().lignes_depuis_data(benevole, data)
        if data.get('kms_vhc', 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no='B02'),
                quantite=data['kms_vhc'], montant_unit=self.TARIF_CHAUFFEUR_VEHICULE,
            ))
        return lignes

    def get_note_frais_columns(self):
        return [
            ("Frais de repas", 'frais_repas'),
            ("Frais de parking", 'frais_divers'),
            ("Unités d’attente", 'nb_attente'),
            ("Kms", 'kms'),
            ("Kms avec véhicule C-R", 'kms_vhc'),
            ("Montant total", 'total'),
        ]

    def get_note_frais_values(self, lignes):
        valeurs = {
            'frais_repas': self.sum_by_codes(lignes, [self.type_map['repas']]),
            'frais_divers': self.sum_by_codes(lignes, [self.type_map['divers']]),
            'nb_attente': self.get_by_code(lignes, self.type_map['transp_attente']),
            'kms': self.get_by_code(lignes, self.type_map['kms']),
            'kms_vhc': self.get_by_code(lignes, self.type_map['kms_vhc']),
        }
        valeurs['total'] = sum([lig.montant for lig in lignes])
        return valeurs


class FacturationPolicy(FacturationPolicyBase):
    MIN_KM_FACTURES = D(0)
    CALCUL_KM_AUTO = True
    CALCUL_DUREE_AUTO = True
    TRANSPORT_RAPPORTE_AUTO_HEURES = 48
    TEMPS_ATTENTE_ALLER = timedelta(minutes=15)
    TEMPS_ATTENTE_OBLIGATOIRE = True
    TARIF_AVS = D('0.90')
    TARIF_NON_AVS = D('1.20')  # Aussi pour homes
    TARIF_LAA = D('1.40')
    TARIF_LOCATION = D("1.10")
    FORFAIT_ALLER = D('4.00')
    FORFAIT_ALLER_RETOUR = D('8.00')
    FORFAIT_ATTENTE = D('15.00')
    FORFAIT_REPAS = D('20.00')
    TRANSP_FACTURATION_STATUTS = {
        Transport.FacturationChoices.FACTURER,
        Transport.FacturationChoices.FACTURER_KM,
        Transport.FacturationChoices.PAS_FACTURER,
    }
    TRANSP_ANNULATION_STATUTS = {
        Transport.FacturationChoices.FACTURER_KM,
        Transport.FacturationChoices.PAS_FACTURER
    }
    ATTENTE_HELP_TEXT = (
        "Attente depuis l’heure de rendez-vous client. À partir de 3h, "
        f"un forfait de {FORFAIT_ATTENTE} vous sera défrayé."
    )

    def tarif_km(self, transport):
        if isinstance(transport, VehiculeOccup):
            return self.TARIF_LOCATION
        if transport.trajets_tries[0].typ == Trajet.Types.LAA:
            return self.TARIF_LAA
        if hasattr(transport, 'is_ofas'):
            is_ofas = transport.is_ofas
        else:
            is_ofas = not transport.client.type_logement == 'home' and transport.client.is_avs(transport.date)
        if is_ofas:
            return self.TARIF_AVS
        return self.TARIF_NON_AVS

    def kms_a_facturer(self, transport):
        if transport.km is None:
            raise RuntimeError("Les kms n’ont pas encore été calculés pour ce transport")
        if transport.est_annule():
            return D(math.ceil(super().kms_a_facturer(transport)))

        kms = transport.km
        trajets = [
            {"trajet": tr, "dist": tr.dist_calc, "nb_clients": tr.commun.nombre_clients() if tr.commun_id else 1}
            for tr in transport.trajets_tries
        ]
        # Si des trajets ont été fait en commun, diviser les kms facturés.
        if len(set(tr["nb_clients"] for tr in trajets)) == 1:
            # Cas simple, nb clients identiques pour tous trajets (division du total par nbre de clients)
            kms = kms / trajets[0]["nb_clients"]
        else:
            # Cas complexe avec trajets différents, on calcule les km à enlever pour chaque trajet.
            for tr in trajets:
                dist_km = round(tr["dist_calc"] / 1000, 1)
                kms_a_retirer = dist_km - (dist_km / tr["nb_clients"])
                kms = kms - kms_a_retirer
        # Arrondi vers le haut
        return D(math.ceil(kms))

    def cout_attente(self, transport):
        """Facturer un forfait de CHF «self.FORFAIT_ATTENTE» à partir de 3 heures."""
        if transport.temps_attente and transport.temps_attente.total_seconds() / 60 >= 180:
            return 1, self.FORFAIT_ATTENTE
        return 0, D('0')

    def cout_forfait(self, transport):
        if transport.est_annule():
            return self.FORFAIT_ALLER
        return self.FORFAIT_ALLER_RETOUR if transport.retour else self.FORFAIT_ALLER

    def cout_annulation(self, transport):
        return 0

    def calculateur_frais(self, mois):
        return CalculateurFrais(mois)

    def montant_total(self, fact_data):
        return sum(data["total"] for data in fact_data)

    def export_frais_chauffeurs(self, liste_notes, un_du_mois):
        return export_frais_benevoles(
            f"Indemnités chauffeurs {django_format(un_du_mois, 'F Y')}",
            liste_notes,
            un_du_mois
        )
