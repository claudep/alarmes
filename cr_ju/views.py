import csv
from datetime import date, datetime, timedelta
from io import StringIO

from openpyxl import load_workbook

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.dateformat import format as django_format
from django.utils.text import slugify
from django.utils.timezone import now
from django.views.generic import FormView, View

from client.models import AdresseClient, Client, Journal, Persona, Prestation, Telephone
from common.choices import Services
from common.utils import WrongDateFormat, fact_policy, read_date
from .forms import ClientImportForm

CSV_KWARGS = {'delimiter': ';', 'quotechar': '"', 'quoting': csv.QUOTE_MINIMAL}


class FactureMixin:
    """
    Exportation des factures dans un fichier CSV pour être importé dans AZ ERP.
    """
    headers = [
        'ID_FACTURE',  # chiffre unique de notre côté.
        'DATE_FACTURE',
        'ID_CLIENT',  # récupérer ID AZ
        'FACTURATION_ID',  # n° adresse de facturation (pour transport uniquement, pour l'instant)
        'CODE_ARTICLE',  # attente envoi AZ
        'DEBITEURS',  # Ex: "3410/10"
        'TEXTE_1',  # "Date: <date>
        'TEXTE_2',  # libellé article (ligne 1)
        'TEXTE_3',  # libellé article (ligne 2)
        'QUANTITE',  # 1
        'TYPE_TAUX',  # "F"
        'TAUX_HORAIRE',  # Prix unitaire
        'MONTANT',  # Total
        'CODE_TVA',  # "B"
        'VALEUR_TVA',  # "0"
        'SECTEUR',  # 'ALAR'
    ]

    def post(self, request, *args, **kwargs):
        query_factures = self.get_queryset()
        if len(query_factures) < 1:
            messages.error(request, "Aucune facture à exporter.")
            return HttpResponseRedirect(self.success_url)

        buf = StringIO()
        try:
            self.exporter_factures(buf, query_factures)
        except ValueError as err:
            messages.error(request, f"Les factures n’ont pas pu être exportées: {err}")
            return HttpResponseRedirect(reverse('factures-non-transmises'))
        else:
            query_factures.filter(exporte__isnull=True).update(exporte=now())

        app = {'ALAR': 'alarme', 'TRAN': 'transport'}.get(self.SECTEUR)
        msg = EmailMessage(
            f"Factures {app} (exportation {date.today().strftime('%d.%m.%Y')})",
            "Bonjour,\n\nVous trouverez ci-joint le ou les fichiers CSV contenant les "
            f"factures {app} à transmettre à la comptabilité.\n\n"
            "Ceci est un message automatique, merci de ne pas y répondre.",
            settings.DEFAULT_FROM_EMAIL,
            [request.user.email]
        )
        date_str = date.today().strftime("%Y_%m_%d")
        if len(query_factures) == 1:
            csv_name = f"factures_{app}_{date_str}_{slugify(query_factures[0].client.nom_prenom)}.csv"
        else:
            csv_name = f"factures_{app}_{date_str}.csv"
        msg.attach(csv_name, buf.getvalue(), "application/csv")
        msg.send()
        messages.success(
            request, f"Le fichier CSV des factures a été envoyé à votre adresse {request.user.email}"
        )
        return HttpResponseRedirect(self.get_success_url())

    def exporter_factures(self, out, factures):
        writer = csv.writer(out, **CSV_KWARGS)
        writer.writerow(self.headers)
        for fact in factures:
            if not fact.date_facture:
                raise ValueError(f"La date est manquante pour la facture «{fact}»")
            lines = self.exporter_facture(fact)
            for line in lines:
                writer.writerow(line)


class FacturesAlarmeExportView(PermissionRequiredMixin, FactureMixin, View):
    permission_required = 'alarme.view_facture'
    ID_FACTURE_START = 90000
    # Valeurs constantes
    QUANTITE = '1'
    TYPE_TAUX = 'F'
    CODE_TVA = '5'
    VALEUR_TVA = '0'
    SECTEUR = 'ALAR'

    def get_queryset(self):
        from alarme.models import Facture
        return Facture.objects.exclude(client__no_debiteur='').filter(
            exporte__isnull=True
        ).select_related('client', 'article').order_by('client', 'mois_facture')

    def exporter_facture(self, facture):
        date_fact = facture.date_facture.strftime('%d.%m.%Y')
        no_fact = self.ID_FACTURE_START + facture.pk
        no_compta = facture.article.compte
        libelle = ''
        if facture.mois_facture and 'service' in facture.article.designation.lower():
            libelle = django_format(facture.mois_facture, "F Y")
        return [[
            no_fact, date_fact, facture.client.no_debiteur, '',
            facture.article.code, no_compta, libelle, '', '', self.QUANTITE,
            self.TYPE_TAUX, facture.montant, facture.montant, self.CODE_TVA, self.VALEUR_TVA, self.SECTEUR,
        ]]

    def get_success_url(self):
        return reverse("facturation")


class FacturesTransportExportView(PermissionRequiredMixin, FactureMixin, View):
    permission_required = 'transport.gestion_finances'

    ID_FACTURE_START = 300000
    TYPE_TAUX = 'F'
    SECTEUR = 'TRAN'

    def get_queryset(self):
        from transport.models import Facture
        fact_pk = self.request.POST.get("fact_id")
        if fact_pk:
            return Facture.objects.filter(pk=fact_pk).order_by("pk")  # Order by to silence warning

        return Facture.objects.exclude(
            Q(client__no_debiteur='') & Q(client__persona__id_externe2=None)
        ).filter(
            exporte__isnull=True
        ).select_related("client__persona").order_by("client", "mois_facture")

    def exporter_facture(self, facture):
        from transport.models import Trajet

        fpolicy = fact_policy()
        no_fact = self.ID_FACTURE_START + facture.pk
        no_client = facture.client.persona.id_externe2 or facture.client.no_debiteur
        adr_fact = facture.autre_debiteur
        no_debiteur = adr_fact.id_externe if adr_fact else ''
        sinistre_laa = adr_fact and adr_fact.no_sinistre
        pers_contact = adr_fact and adr_fact.pers_contact
        lines = []
        for transp in facture.get_items():
            donnees_fact = transp.donnees_facturation()
            if donnees_fact["type_fact"] == "reservation":
                date_fact = facture.date_facture.strftime('%d.%m.%Y')
                code_tva = "1"
                valeur_tva = "0"
                libelle1 = f"Date : {transp.duree.lower.strftime('%d/%m/%Y')}"
                libelle2 = "Location privée d’un véhicule adapté"
                # Forfait de 15.-
                lines.append([
                    no_fact, date_fact, no_client, no_debiteur,
                    "TLOCATION", "3400/40", libelle1, f"{libelle2} - forfait", "", "1",
                    self.TYPE_TAUX, "15", "15", code_tva, valeur_tva, self.SECTEUR,
                ])
                taux_horaire = donnees_fact["tarif_km"]
                lines.append([
                    no_fact, date_fact, no_client, no_debiteur,
                    "TKG", "3400/10", libelle1, f"{libelle2} - kms", "", donnees_fact["km"],
                    self.TYPE_TAUX, taux_horaire, round(donnees_fact["km"] * taux_horaire, 2),
                    code_tva, valeur_tva, self.SECTEUR,
                ])
                continue

            libelle3 = ""
            if transp.trajets_tries[0].typ == Trajet.Types.LAA and sinistre_laa:
                code_article = "TVPSP"  # Transport OFAS et non OFAS, facturation Caisse accident
                libelle3 = f"{facture.client.nom_prenom}, sinistre no {sinistre_laa}"
            elif transp.vehicule_id:
                if transp.is_ofas:
                    code_article = "TKG"  # Transport OFAS, avec véhicule adapté
                else:
                    code_article = "TKGSPE"  # Transport non OFAS, avec véhicule adapté
            else:
                if transp.is_ofas:
                    code_article = "TVP"  # Transport OFAS
                else:
                    code_article = "TVPSPE"  # Transport non OFAS
            match code_article:
                case 'TVP':
                    no_compta = "3400/00"
                case 'TVPSP' | 'TVPSPE':
                    no_compta = "3400/20"
                case 'TKG':
                    no_compta = "3400/10"
                case 'TKGSPE':
                    no_compta = "3400/30"
                case 'TPARKING' | 'TREPAS' | 'TATTENTE':
                    no_compta = "3400/40"
                case _:
                    raise ValueError(f"code_article {code_article} inconnu")

            libelle1 = f"Date : {transp.date.strftime('%d/%m/%Y')}"
            dep, arr = transp.adresse_depart(), transp.destination
            str_depart = 'Domicile' if dep.est_adresse_client else f"{dep.nom} / {dep.localite}"
            str_arrivee = 'Domicile' if arr.est_adresse_client else f"{arr.nom} / {arr.localite}"
            libelle2 = f"{str_depart} -> {str_arrivee}".replace("–", "-")
            if pers_contact:
                if libelle3:
                    libelle3 += " / "
                libelle3 += f"Contact: {pers_contact}"
                # Afficher seulement une fois par facture
                pers_contact = ""
            if transp.est_annule():
                if libelle3:
                    libelle3 += " / "
                libelle3 += "Annulation transport, déplacement chauffeur"
            date_fact = facture.date_facture.strftime('%d.%m.%Y')
            quantite = donnees_fact["km"]
            taux_horaire = donnees_fact["tarif_km"]
            code_tva = "1" if code_article.startswith("TKG") or code_article == "TLOCATION" else "2"
            valeur_tva = "8" if code_tva == "2" else "0"
            lines.append([
                no_fact, date_fact, no_client, no_debiteur,
                code_article, no_compta, libelle1, libelle2, libelle3, quantite,
                self.TYPE_TAUX, taux_horaire, round(quantite * taux_horaire, 2),
                code_tva, valeur_tva, self.SECTEUR,
            ])
            if forfait := donnees_fact.get("cout_forfait", 0):
                lines.append([
                    no_fact, date_fact, no_client, no_debiteur,
                    "TFORFAIT", no_compta, libelle1,
                    f"Forfait {'aller simple' if forfait == fpolicy.FORFAIT_ALLER else 'aller-retour'}",
                    '', '1', self.TYPE_TAUX, forfait, forfait,
                    code_tva, valeur_tva, self.SECTEUR,
                ])
            for frais in donnees_fact.get("frais", []):
                # TPARKING / TREPAS
                code_article = f'T{frais["typ"].upper()}'
                taux_horaire = fpolicy.FORFAIT_REPAS if frais["typ"] == 'repas' else frais["cout"]
                lines.append([
                    no_fact, date_fact, no_client, no_debiteur,
                    code_article, "3400/40", libelle1, libelle2, '', '1',
                    self.TYPE_TAUX, taux_horaire, taux_horaire, code_tva, valeur_tva, self.SECTEUR,
                ])
            if cout_att := donnees_fact.get("cout_attente"):
                lines.append([
                    no_fact, date_fact, no_client, no_debiteur,
                    'TATTENTE', "3400/40", libelle1, libelle2, '', '1',
                    self.TYPE_TAUX, cout_att, cout_att, code_tva, valeur_tva, self.SECTEUR,
                ])
        return lines

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER', reverse("finances"))


class ImportTransportClients(FormView):
    form_class = ClientImportForm
    template_name = 'besoins/client_import.html'
    success_url = reverse_lazy('besoins:home')

    home_terms = [
        "résidence", "home", "foyer", "fondation", "hospice", "tertianum", "protégé",
        "bergeronnettes", "communauté", "bennelats", "genévrier", "espace scheulte",
        "centre de jour", "maison chappuis", "maison des soeurs",
    ]

    def form_valid(self, form):
        wb = load_workbook(form.cleaned_data['fichier'])
        ws = wb.active
        transport_only_pks = list(
            Client.objects.par_service(Services.TRANSPORT, strict=True).values_list('pk', flat=True)
        )
        headers = None
        created = updated = 0
        errors = []
        client_qs = Client.objects.avec_services().avec_adresse(date.today())
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if headers is None:
                    values = [cell.value for cell in row if cell.value is not None]
                    if len(values) < 4:
                        continue
                    headers = values
                    if not {
                        'Date de naissance', 'Rue et No', 'No postal et Localité', 'No client transport'
                    }.issubset(set(headers)):
                        messages.error(
                            self.request, "Les en-têtes détectés ne contiennent pas les en-têtes espérés."
                        )
                        return super().form_valid(form)

                values = dict(zip(headers, [cell.value for cell in row]))
                if "Client Transport" not in values['Répertoire']:
                    continue
                nom = clean_value(values.get('Nom', values.get('Nom ')))
                if 'Hôpital' in nom or 'kangoo' in nom.lower():
                    continue
                prenom = clean_value(values.get('Prénom'))
                if prenom == '' or prenom.endswith(" KG"):
                    continue
                no_client_transp = clean_value(values['No client transport'], None)
                if no_client_transp and not isinstance(no_client_transp, int):
                    errors.append(
                        f"Impossible d’importer {nom} {prenom}: valeur non numérique dans "
                        f"la colonne «No client transport» ({no_client_transp})"
                    )
                    continue
                try:
                    date_naiss = read_date(values['Date de naissance'])
                except WrongDateFormat:
                    errors.append(
                        f"Impossible d’importer {nom} {prenom}: la date de naissance "
                        f"{values['Date de naissance']} n’est pas correcte."
                    )
                    continue
                npa, localite = clean_value(values['No postal et Localité']).strip().split(' ', maxsplit=1)
                npa = npa.removeprefix('F-')
                tel_1 = clean_value(values['Téléphone'])
                tel_2 = clean_value(values['Mobile'])
                en_home = any(
                    term in clean_value(values['Adresse complémentaire']).lower()
                    for term in self.home_terms
                ) or values['Rue et No'].replace(" ", "") == "Ruedel'Hôpital11"
                mauv_payeur = clean_value(values['Mauvais payeur / client transport']) == "Oui"
                date_cree = read_date(values['Date de création']) or date.today()

                def create_client():
                    # Création d'un nouveau client transport
                    persona = Persona(
                        cree_le=now(),
                        nom=nom,
                        prenom=prenom,
                        id_externe2=no_client_transp,
                        genre={"Monsieur": 'M', "Madame": 'F'}.get(clean_value(values['Titre']), ''),
                        date_naissance=date_naiss,
                        courriel=clean_value(values['Email']),
                    )
                    client = Client(
                        type_logement='home' if en_home else '',
                        remarques_ext_transport=clean_value(values['Remarque']),
                        fonds_transport=mauv_payeur,
                    )
                    priorite = 1
                    tels = []
                    if tel_1:
                        tels.append(Telephone(persona=persona, tel=tel_1, priorite=priorite))
                        priorite += 1
                    if tel_2:
                        tels.append(Telephone(persona=persona, tel=tel_2, priorite=priorite))
                    try:
                        persona.full_clean()
                    except ValidationError as err:
                        errors.append(f"Impossible d’importer {client}: {err}")
                        raise
                    with transaction.atomic():
                        persona.save()
                        client.persona = persona
                        try:
                            client.full_clean()
                            for t in tels:
                                t.persona = persona
                                t.full_clean()
                        except ValidationError as err:
                            errors.append(f"Impossible d’importer {client}: {err}")
                            raise
                    persona.save()
                    client.save()
                    [t.save() for t in tels]
                    AdresseClient.objects.create(
                        persona=persona,
                        principale=(date.today(), None),
                        rue=clean_rue_value(values['Rue et No']),
                        c_o=clean_value(values['Adresse complémentaire']),
                        npa=npa,
                        localite=localite,
                    )
                    Prestation.objects.create(
                        client=client, service=Services.TRANSPORT, duree=(date_cree, None)
                    )
                    Journal.objects.create(
                        persona=client.persona, description="Création par importation des clients transport",
                        quand=now(), qui=self.request.user
                    )
                    return client

                def update_client(client):
                    # Mise à jour d'un client existant
                    updated = False
                    persona = client.persona
                    if client.pk in transport_only_pks:
                        transport_only_pks.remove(client.pk)
                    if Services.TRANSPORT not in client.services:
                        Prestation.objects.create(
                            client=client, service=Services.TRANSPORT, duree=(date_cree, None)
                        )
                        Journal.objects.create(
                            persona=persona,
                            description="Ajout comme client transport lors de l’importation des clients transport",
                            quand=now(), qui=self.request.user
                        )
                        updated = True
                    changed = []
                    if not persona.id_externe2:
                        persona.id_externe2 = no_client_transp
                        persona.save(update_fields=['id_externe2'])
                    if Services.ALARME not in client.services:
                        # Mise à jour de données seulement si pas client alarme
                        for field, header in [
                            ('prenom', 'Prénom'), ('courriel', 'Email'), ('remarques_ext_transport', 'Remarque'),
                        ]:
                            new_value = clean_value(values[header])
                            if getattr(client, field) != new_value:
                                changed.append(f'{header} (de «{getattr(client, field)}» à «{new_value}»)')
                                if field == 'remarques_ext_transport':
                                    setattr(client, field, new_value)
                                else:
                                    setattr(persona, field, new_value)
                        # Tels
                        new_tels = [tel for tel in [tel_1, tel_2] if tel]
                        old_tels = persona.telephones()[:2]
                        if set(new_tels) != set([t.tel for t in old_tels]):
                            changed.append(f"téléphones (de {new_tels} à {[t.tel for t in old_tels]})")
                            persona.telephones().delete()
                            priorite = 1
                            if tel_1:
                                Telephone.objects.create(persona=persona, tel=tel_1, priorite=priorite)
                                priorite += 1
                            if tel_2:
                                Telephone.objects.create(persona=persona, tel=tel_2, priorite=priorite)
                        # Adresse
                        cur_adr = client.adresse()
                        new_adr = AdresseClient(
                            persona=client.persona,
                            rue=clean_rue_value(values['Rue et No']),
                            c_o=clean_value(values['Adresse complémentaire']),
                            npa=npa,
                            localite=localite,
                        )
                        if any(
                            getattr(cur_adr, fname) != getattr(new_adr, fname)
                            for fname in ['rue', 'c_o', 'npa', 'localite']
                        ):
                            cur_adr.principale = (
                                min(cur_adr.principale.lower, date.today() - timedelta(days=2)),
                                date.today() - timedelta(days=1)
                            )
                            cur_adr.save()
                            new_adr.principale = (date.today(), None)
                            new_adr.check_geolocalized(save=False)
                            new_adr.save()
                            changed.append(
                                f"Changement d’adresse (de «{cur_adr.rue}, {cur_adr.npa} {cur_adr.localite}» "
                                f"à «{new_adr.rue}, {new_adr.npa} {new_adr.localite}»)"
                            )
                    if en_home != (client.type_logement == 'home'):
                        old_type_logement = client.type_logement
                        client.type_logement = 'home' if en_home else ''
                        changed.append(f"Type de logement (de «{old_type_logement}» à «{client.type_logement}»)")
                    if mauv_payeur != client.fonds_transport:
                        changed.append(
                            f"Mauvais payeur (de «{'Oui' if client.fonds_transport else 'Non'}» à "
                            f"«{'Oui' if mauv_payeur else 'Non'}»)"
                        )
                        client.fonds_transport = mauv_payeur
                    if changed:
                        client.save()
                        client.persona.save()
                        Journal.objects.create(
                            persona=client.persona,
                            description=f"Mise à jour lors de l’importation des clients transport ({', '.join(changed)})",
                            quand=now(), qui=self.request.user
                        )
                        updated = True
                    return updated

                if no_client_transp:
                    try:
                        client = client_qs.get(persona__id_externe2=no_client_transp)
                    except Client.DoesNotExist:
                        pass
                    else:
                        updated += int(update_client(client))
                        continue
                try:
                    if date_naiss:
                        client = client_qs.get(
                            persona__nom__iexact=nom, persona__date_naissance=date_naiss, adresse_active__npa=npa
                        )
                    else:
                        client = client_qs.get(
                            persona__nom__iexact=nom, persona__prenom__iexact=prenom, adresse_active__npa=npa
                        )
                except Client.DoesNotExist:
                    try:
                        client = create_client()
                    except ValidationError:
                        continue
                    created += 1
                except Client.MultipleObjectsReturned:
                    errors.append(
                       f"Plus d’un client trouvé pour nom={nom}, npa={npa}, date de naissance={date_naiss}"
                    )
                    continue
                else:
                    updated += int(update_client(client))

        messages.success(
            self.request,
            f"{created} nouveaux clients créés, {updated} clients mis à jour"
        )

        for pk in transport_only_pks:
            # Clients Transports qui ne sont plus dans le fichier d'importation
            client = Client.objects.get(pk=pk)
            client.archiver(
                self.request.user, for_app='transport', check=False,
                message="script importation clients transport"
            )
            messages.info(self.request, f"Le client {client} a été archivé.")

        if errors:
            messages.error(
                self.request,
                "Les erreurs suivantes sont survenues: \n" + "\n".join(errors)
            )
        return super().form_valid(form)


def clean_value(val, default=''):
    if isinstance(val, str):
        return val.strip()
    elif isinstance(val, datetime):
        return val.date()
    return default if val is None else val


def clean_rue_value(val):
    val = clean_value(val)
    return val.replace(" l' ", " l'")
