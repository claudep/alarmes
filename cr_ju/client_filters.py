from django.db.models import Max, OuterRef, Q, Subquery

from client.forms import ClientFilter


class BooleanFieldClientFilter(ClientFilter):
    def __init__(self, field_name):
        self.field_name = field_name

    @property
    def label(self):
        target = {"fasd": "la FASD"}.get(self.field_name, self.field_name.capitalize())
        return f"Clients en lien avec {target}"

    def filter(self, clients):
        Installation = clients.model._meta.get_field("installation").related_model
        return clients.filter(**{f"clientju__{self.field_name}": True}).annotate(
            date_install=Subquery(
                Installation.objects.filter(client=OuterRef('pk')).order_by('date_debut').values("date_debut")[:1]
            ),
            date_resil=Subquery(
                Installation.objects.filter(client=OuterRef('pk')).order_by('-date_debut').values("date_fin_abo")[:1]
            ),
        )

    def extra_headers(self):
        return ['Date d’installation', 'Date de résiliation']

    def extra_values(self, client):
        return [client.date_install, client.date_resil]


class ClientsSansNoDebFilter(ClientFilter):
    label = "Clients sans n° débiteur"

    def filter(self, clients):
        return clients.par_service("transport").annotate(
            referent_no_debiteur=Max(
                "referent__id_externe", filter=Q(
                    referent__facturation_pour__contains=["transp"], referent__date_archive=None
                )
            )
        ).filter(
            # id AZ transport actuellement dans id_externe2
            Q(referent_no_debiteur=None) & Q(no_debiteur="") & Q(persona__id_externe2__isnull=True)
        )

    def extra_headers(self):
        return ["Autre débit."]

    def extra_values(self, client):
        return [client.referent_no_debiteur is not None and '*' or '']


class ClientsAvecVisiteFilter(ClientFilter):
    label = "Clients avec visiteurs"

    def filter(self, clients):
        return clients.filter(visiteur__isnull=False)

    def extra_headers(self):
        return ["Visiteur"]

    def extra_values(self, client):
        return [str(client.visiteur)]
