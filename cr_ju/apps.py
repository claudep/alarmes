from importlib import import_module

from django.apps import AppConfig, apps
from django.db.models.signals import post_save


class CRJUAppConfig(AppConfig):
    name = 'cr_ju'
    verbose_name = "Croix-Rouge Jura"
    abrev = "JU"

    client_profil_form_class = None

    EXTRA_LOGEMENT_CHOICES = ()
    TYPE_FRAIS_MAP = {
        'repas': 'B07',
        'divers': 'B06',
        'kms': 'B01',
        'kms_vhc': 'B02',
        'kms_chauffeurs': 'B01',
        'transp_attente': 'B08',
    }
    BESOINS_TYPE_LOGEMENT_EXCLUDE = ['home', 'resid']
    # Tri des clients besoins à questionner
    BESOINS_CLIENT_ORDERING = ["persona__nom"]
    pdf_doc_registry = {
        'courrier_repondant': {
            'title': "Courrier pour le répondant",
            'class': "alarme.pdf.CourrierReferentPDF",
            'input': "contact-est_repondant",
        },
    }

    def ready(self):
        from client.models import Client
        from . import signals
        from .forms import ClientJUForm

        self.client_profil_form_class = ClientJUForm
        post_save.connect(signals.client_created, sender=Client)

        if apps.is_installed('alarme'):
            from alarme.forms import ClientFilterForm
            from alarme.models import Installation
            from . import alarme_policies
            from .client_filters import BooleanFieldClientFilter, ClientsAvecVisiteFilter

            ClientFilterForm.add_filter("fasd", BooleanFieldClientFilter("fasd"))
            ClientFilterForm.add_filter("adapart", BooleanFieldClientFilter("adapart"))
            ClientFilterForm.add_filter("megapart", BooleanFieldClientFilter("megapart"))
            ClientFilterForm.add_filter("visite", ClientsAvecVisiteFilter())
            self.fact_policy = alarme_policies.FacturationPolicy()
            post_save.connect(signals.install_saved, sender=Installation)

        if apps.is_installed('transport'):
            from transport.forms import ClientFilterForm
            from . import transport_policies
            from .client_filters import ClientsSansNoDebFilter

            ClientFilterForm.add_filter('no_debit', ClientsSansNoDebFilter())
            self.fact_policy = transport_policies.FacturationPolicy()

    def pdf_headfoot_class(self):
        try:
            return self.module.pdf.PDFHeaderFooter
        except AttributeError:
            import_module(f'{self.name}.pdf')
            return self.module.pdf.PDFHeaderFooter
