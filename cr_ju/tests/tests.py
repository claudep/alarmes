from datetime import date

from django.test import TestCase

from client.models import Client, Services


class JUTests(TestCase):
    def test_client_profil(self):
        client = Client.objects.create(
            service=Services.ALARME, nom='Müller', prenom='Hans',
            rue='Rue du Stand 24', npa='2800', localite='Delémont',
            date_naissance=date(1950, 5, 24),
        )
        self.assertFalse(client.clientju.fasd)
        self.assertFalse(client.clientju.adapart)
        self.assertFalse(client.clientju.megapart)
