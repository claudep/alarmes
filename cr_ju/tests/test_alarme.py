from datetime import date, timedelta
from decimal import Decimal

from freezegun import freeze_time

from django.core import mail
from django.db.models import Count
from django.test import TestCase, tag
from django.test.utils import override_settings
from django.urls import reverse
from django.utils import timezone

from alarme.models import (
    Alarme, ArticleFacture, Facture, Installation, Mission, ModeleAlarme,
    TypeMission
)
from alarme.tests.test_general import TestUtils
from benevole.models import TypeActivite
from client.models import Client, Referent
from common.test_utils import read_response_pdf_text
from cr_ju.alarme_policies import CalculateurFrais


class JUTestMixin:
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ArticleFacture.objects.bulk_create([
            ArticleFacture(
                code='TAXE2', designation="Frais d'installation et boîtier à clés", prix=150, compte='3410/00'
            ),
            ArticleFacture(
                code='REFACT MONTRE LIMMEX', designation="Appareil Limmex 2.1", prix=390
            ),
            ArticleFacture(
                code='ALRM', designation="Service d’alarme mensuel", prix=25.5, compte='3410/10'
            ),
            ArticleFacture(
                code='EMETTEUR', designation="Émetteur suppl.", prix=135.5, compte='3410/10'
            ),
        ])
        ArticleFacture.objects.filter(code='install').update(code='TAXE', compte='3410/00')


class FactureTests(JUTestMixin, TestUtils, TestCase):
    @tag("factures")
    @freeze_time("2022-03-10")
    def test_generer_factures_en_avance(self):
        installs = self.create_client_with_installations(2, debut_install=date(2022, 2, 25))
        for install in installs:
            # Cela va créer les factures manquantes, y compris la taxe d'installation.
            install.save()
        # Fin le mois suivant, la 3ème facture ne doit pas être générée.
        Installation.objects.filter(pk=installs[1].pk).update(
            date_fin_abo = date(2022, 5, 25)
        )
        num_factures = Installation.generer_factures(date(2022, 4, 1))
        self.assertEqual(num_factures, 5)
        self.assertEqual(
            sorted(Facture.objects.values_list('date_facture').annotate(nbr=Count('date_facture')).distinct()),
            [
                (date(2022, 3, 10), 4),  # 2 taxe install + 2 factures mens.
                (date(2022, 4, 1), 2),
                (date(2022, 5, 1), 2),
                (date(2022, 6, 1), 1)
            ]
        )

    @tag("factures")
    @freeze_time("2022-03-25")
    def test_generer_factures_en_avance_apres_20_du_mois(self):
        installs = self.create_client_with_installations(2, debut_install=date(2022, 3, 22))
        autre_cl = Client.objects.create(nom='Donzé', prenom='Nouveau')
        # Il faut qu'il existe au moins une facture pour chaque mois du trimestre prochain pour que des
        # factures individuelles soient créées pour ces mois.
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-04-01', date_facture='2022-03-04', montant=2, article=self.article_abo
        )
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-05-01', date_facture='2022-03-04', montant=2, article=self.article_abo
        )
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-06-01', date_facture='2022-03-04', montant=2, article=self.article_abo
        )
        # Fin le mois suivant, la 3ème facture ne doit pas être générée.
        Installation.objects.filter(pk=installs[1].pk).update(
            date_fin_abo = date(2022, 5, 25)
        )
        num_factures = Installation.generer_factures(date(2022, 3, 25))
        self.assertEqual(num_factures, 7)
        self.assertEqual(
            sorted(Facture.objects.exclude(
                client=autre_cl
            ).values_list('date_facture').annotate(nbr=Count('date_facture')).distinct()),
            [
                (date(2022, 3, 25), 2),  # 2 taxe install (pas de facture pour mars)
                (date(2022, 4, 1), 2),
                (date(2022, 5, 1), 2),
                (date(2022, 6, 1), 1)
            ]
        )

    @tag("factures")
    def test_export_factures_csv(self):
        article = ArticleFacture.objects.get(code='ALRM')
        installs = self.create_client_with_installations(3, client_data=[
            {'no_debiteur': '123'}, {'no_debiteur': ''}, {'no_debiteur': '456'}
        ])
        factures = []
        # facture déjà exportée
        factures.append(Facture(
            client=installs[0].client, article=article,
            date_facture=date.today(), libelle="Déjà exportée", montant=1, exporte=timezone.now()
        ))
        # facture avec client sans no debiteur
        factures.append(Facture(
            client=installs[1].client, article=article,
            date_facture=date.today(), libelle="Sans no débiteur", montant=1,
        ))
        factures.extend([
            # 2 factures avec même client/mois_facture.
            # Dans une version précédente, ce cas donnait lieu à une facture unique à plusieurs articles.
            # La CR-JU a ensuite préféré revenir à un article = une facture.
            Facture(
                client=installs[2].client, article=article,
                date_facture=date(2022, 4, 3), mois_facture=date(2022, 4, 1),
                libelle="Article 1", montant=12.5,
            ),
            Facture(
                client=installs[2].client, article=article,
                date_facture=date(2022, 4, 3), mois_facture=date(2022, 4, 1),
                libelle="Abo’22 : Mme Charpié", montant=15.5,
            ),
            # sans date de facture
            Facture(
                client=installs[2].client,
                article=ArticleFacture.objects.get(code='TAXE'),
                mois_facture=date(2022, 4, 1),
                libelle="Frais d'installation", montant=80,
            ),
        ])
        factures = Facture.objects.bulk_create(factures)

        self.client.force_login(self.user)
        response = self.client.post(reverse('factures-alarme-export'), follow=True)
        self.assertContains(
            response,
            "Les factures n’ont pas pu être exportées: La date est manquante pour la facture"
        )
        factures[-1].date_facture = date(2022, 4, 8)
        factures[-1].save()
        response = self.client.post(reverse('factures-alarme-export'))
        self.assertRedirects(response, reverse('facturation'))
        self.assertEqual(len(mail.outbox), 1)
        csv_content = mail.outbox[0].attachments[0][1]
        lines = {line.split(';')[0]: line for line in csv_content.splitlines()}
        self.assertTrue(lines['ID_FACTURE'].startswith('ID_FACTURE;DATE_FACTURE;ID_CLIENT;FACTURATION_ID;CODE_ARTICLE;'))
        self.assertEqual(len(lines), 4)
        fact3_no = str(90000 + factures[2].pk)
        self.assertEqual(
            lines[fact3_no],
            f'{fact3_no};03.04.2022;456;;ALRM;3410/10;avril 2022;;;1;F;12.50;12.50;5;0;ALAR'
        )
        fact5_no = str(90000 + factures[4].pk)
        self.assertEqual(
            lines[fact5_no],
            f'{fact5_no};08.04.2022;456;;TAXE;3410/00;;;;1;F;80.00;80.00;5;0;ALAR'
        )
        # Facture.exporte est rempli
        self.assertFalse(Facture.objects.exclude(client__no_debiteur='').filter(exporte__isnull=True).exists())

    @tag("factures")
    def test_facture_apres_nouvelle_install(self):
        """
        Quand une nouvelle installation est créée:
         - les factures mensuelles du prochain trimestre sont créées
        """
        self.client.force_login(self.user)
        with freeze_time("2022-02-16"):
            client = self.nouvelle_install_par_mission()
            # Should have created 3 bills (install + 02 + 03)
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-02-01', '2022-02-16', '37.45'),  # 1er mois
                 ('2022-02-16', '2022-02-16', '80.00'),  # Frais install
                 ('2022-03-01', '2022-03-01', '37.45')  # Mois suivant complet
                ],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture), str(fac.montant))
            )
        with freeze_time("2022-03-26"):
            # Facturation du trimestre suivant
            Installation.generer_factures(date(2022, 4, 1), date_factures=date(2022, 3, 26))
            self.assertEqual(
                [str(fact.montant) for fact in client.factures.filter(date_facture__gte=date(2022, 3, 26))],
                ['37.45', '37.45', '37.45']
            )

    @tag("factures")
    def test_facture_apres_nouvelle_install_meme_mois(self):
        self.client.force_login(self.user)
        with freeze_time("2022-03-04"):
            client = self.nouvelle_install_par_mission()
            # Should have created only bill for 03 (next quarter not yet billed) + installation
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-03-01', '2022-03-04'), ('2022-03-04', '2022-03-04')],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture))
            )

    @tag("factures")
    def test_facture_apres_nouvelle_install_facture_existante(self):
        """Une facture mensuelle existe déjà pour ce client (install précédente)."""
        client = Client.objects.create(
            nom="Donzé", prenom="Nouveau", npa="2800", localite="Delémont"
        )
        Facture.objects.create(
            client=client, article=self.article_abo,
            date_facture=date(2023, 4, 1), mois_facture=date(2023, 4, 1),
            libelle="Abo 1", montant=12.5,
        )

        self.client.force_login(self.user)
        with freeze_time("2023-04-03"):
            client = self.nouvelle_install_par_mission(client=client)
            # Should have created bills for May/June
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2023-04-01', '2023-04-01'), ('2023-04-03', '2023-04-03'),
                 ('2023-05-01', '2023-05-01'), ('2023-06-01', '2023-06-01'),
                ],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture))
            )

    @tag("factures")
    def test_factures_trimestre_suivant_si_deja_commence(self):
        # Facture pour prochain trimestre pour un autre client (qui va signaler
        # au système que la facturation du prochain trimestre est déjà entamée).
        Facture.objects.create(
            client=Client.objects.create(nom='Test'),
            date_facture=date(2022, 4, 1), mois_facture=date(2022, 4, 1),
            article=self.article_abo, montant=55,
        )
        with freeze_time("2022-03-04"):
            self.client.force_login(self.user)
            client = self.nouvelle_install_par_mission()
            # Should have created bills for 03/04/05/06 (next quarter already billed)
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-03-01', '2022-03-04'),
                 ('2022-03-04', '2022-03-04'),  # Install
                 ('2022-04-01', '2022-04-01'),
                 ('2022-05-01', '2022-05-01'),
                 ('2022-06-01', '2022-06-01')],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture))
            )

    @tag("factures")
    @freeze_time("2022-12-03")
    def test_facture_install_limmex(self):
        modele_limmex = ModeleAlarme.objects.create(
            nom='Limmex 2.1',
            article_achat=ArticleFacture.objects.get(code='REFACT MONTRE LIMMEX'),
            article_install=ArticleFacture.objects.get(code='TAXE')
        )
        modele_limmex.abos.add(self.abo)
        alarme = Alarme.objects.create(modele=modele_limmex, no_serie=999)
        self.client.force_login(self.user)
        client = self.nouvelle_install_par_mission(alarme=alarme)
        self.assertQuerySetEqual(
            client.factures.order_by('montant'),
            [Decimal('37.45'), Decimal(80), Decimal(390)],
            transform=lambda f: f.montant
        )

    @tag("factures")
    @freeze_time("2022-12-03")
    def test_facture_install_boitier_cle(self):
        """
        Si le client possède un boîtier clés Croix-Rouge, les frais d'installation
        sont majorés du coût du boîtier, par la sélection d'un article de
        facturation approprié.
        """
        client = self.create_client_with_installations(
            1, client_data=[{'boitier_cle': 'c-r'}], debut_install=date.today()
        )[0]
        Installation.generer_factures(date.today())
        self.assertQuerySetEqual(
            client.factures.all().order_by('-montant'),
            [('TAXE2', Decimal(150)), ('ABO1', Decimal('37.45'))],
            transform=lambda f: (f.article.code, f.montant)
        )
        # Ne pas générer factures à double
        self.assertEqual(Installation.generer_factures(date.today()), 0)


@override_settings(COMPTA_EMAIL='compta@example.org')
class OtherTests(JUTestMixin, TestUtils, TestCase):
    def test_client_new(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'genre': 'M',
            'npalocalite': '2345 Petaouchnok',
            'langues': ['fr', 'it'],
            'date_naissance': '3.12.1945',
            'fasd': 'on',
            "telephone_set-INITIAL_FORMS": 0,
            "telephone_set-TOTAL_FORMS": 1,
            "telephone_set-0-tel": "0551234567",
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        client = Client.objects.get(persona__nom='Dupond')
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual([t.tel for t in client.telephones()], ["055 123 45 67"])
        self.assertTrue(client.clientju.fasd)

    def test_client_contrat(self):
        install = self.create_client_with_installations(1)[0]
        install.client.clientju.fasd = True
        install.client.clientju.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-contrat', args=[install.client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')
        content = read_response_pdf_text(response)
        self.assertIn("convention entre la FASD et la CRJU", content.replace('\n', ' '))

    def test_client_list_filter(self):
        installs = self.create_client_with_installations(3)
        self.client.force_login(self.user)
        for idx, fname in enumerate(["fasd", "adapart", "megapart"]):
            setattr(installs[idx].client.clientju, fname, True)
            installs[idx].client.clientju.save()
        for idx, fname in enumerate(["fasd", "adapart", "megapart"]):
            response = self.client.get(reverse("clients") + f"?listes={fname}")
            self.assertQuerySetEqual(response.context["object_list"], [installs[idx].client])

    def test_mission_install_duree_seul(self):
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', no_debiteur='123987', rue="Fleurs 432", npa="2800", localite="Delémont"
        )
        in4days = date.today() + timedelta(days=4)
        form_data = {
            'description': "Installation chez Mme Trucmuche",
            'type_mission': TypeMission.objects.create(nom='Installation', code='NEW').pk,
            'planifiee_0': in4days.strftime('%d.%m.%Y'),
            'planifiee_1': '15:30',
            'delai': (date.today() + timedelta(days=10)).strftime('%d.%m.%Y'),
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-new', args=[client.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        mission = client.mission_set.first()
        self.assertEqual(mission.duree_seul.total_seconds(), 30 * 60)

    def test_defraiement_visites(self):
        client1 = Client.objects.create(
            nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont"
        )
        client2 = Client.objects.create(
            nom='Pittet', prenom='Lise', rue="Gare 12", npa="2900", localite="Porrentruy"
        )
        benev = self._create_benevole(
            npa="2800", localite="Delémont", activites=[TypeActivite.VISITE_ALARME],
            avec_utilisateur=True,
        )
        Mission.objects.create(
            client=client1, type_mission=TypeMission.objects.get_or_create(code="VISITE")[0],
            effectuee="2024-10-14", intervenant=benev.utilisateur
        )
        Mission.objects.create(
            client=client2, type_mission=TypeMission.objects.get(code="VISITE"),
            effectuee="2024-10-15", intervenant=benev.utilisateur
        )
        calculateur = CalculateurFrais(date(2024, 10, 1))
        calculateur.TARIF_VISITES_DEFRAYEES = Decimal("10.00")
        calculateur.calculer_frais(enregistrer=True)
        note = benev.notefrais_set.first()
        # Seule la visite dans une autre localité est défrayée (#526)
        self.assertQuerySetEqual(
            note.lignes.values_list("libelle__no", "montant_unit"),
            [("B10", Decimal("10"))]
        )

    def test_message_apres_nouvelle_install(self):
        """
        Quand une nouvelle installation est créée:
         - un message est envoyé à la comptabilité pour créer le client
        """
        self.client.force_login(self.user)
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', no_debiteur='123987', rue="Fleurs 432", npa="2800", localite="Delémont"
        )
        Referent.objects.create(client=client, nom='Schmid', referent=1, facturation_pour=[])
        client.refresh_from_db()
        client = self.nouvelle_install_par_mission(client=client)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['compta@example.org'])
        message = (
            "Un nouvel abonnement alarme a été créé pour Donzé Léa. Merci de "
            "bien vouloir créer un nouveau client dans votre système comptable "
            "et de communiquer le n° de débiteur à la personne responsable des alarmes.\n\n"
            "Client:\n"
            "Donzé Léa\n"
            "Fleurs 432\n"
            "2800 Delémont\n"
            "N° débiteur : 123987\n"
            "\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        )
        self.assertEqual(mail.outbox[0].body, message)

    def test_fin_abo_envoi_message(self):
        install = self.create_client_with_installations(1)[0]

        # Un changement d'alarme ne doit pas envoyer de message
        nouvelle_alarme = Alarme.objects.create(modele=ModeleAlarme.objects.first(), no_serie=999)
        self.client.force_login(self.user)
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('install-edit', args=[install.pk]), data={
                'client': install.client_id,
                'abonnement': install.abonnement_id,
                'date_debut': install.date_debut.strftime('%d.%m.%Y'),
                'nouvelle_alarme': nouvelle_alarme.pk,
                'date_changement': date.today().strftime('%d.%m.%Y'),
            })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertEqual(len(mail.outbox), 0)

        install = install.client.alarme_actuelle()
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('install-close', args=[install.pk]), data={
                'date_fin_abo': date.today(),
                'motif_fin': 'décès',
            })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['compta@example.org'])
        message = (
            "L’abonnement alarme de Donzé Léa0 a été résilié pour le {today}. "
            "Veuillez vérifier si des factures en cours doivent être annulées."
            "\n\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        ).format(today=date.today().strftime('%d.%m.%Y'))
        self.assertEqual(mail.outbox[0].body, message)

    def test_stats_installs(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-index'))
        self.assertContains(response, 'Nouvelles installations')
        self.assertNotContains(response, 'Abo samaritains')

    def test_signature_convention_loc(self):
        png_data = (
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAA"
            "C0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII="
        )
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        url = reverse('client-localisation-signer', args=[install.client.pk])
        img_url = reverse('client-localisation', args=[install.client.pk]) + '?asimg=yes&forsign=yes'
        response = self.client.get(url)
        self.assertContains(response, "<canvas")
        response = self.client.post(url, data={'sig_data': png_data, 'lieu': 'Porrentruy'})
        self.assertRedirects(response, install.client.get_absolute_url())
        quest = install.client.fichiers.first()
        self.assertEqual(
            quest.titre,
            f"Alarme - Convention de localisation signée du {date.today().strftime('%d.%m.%Y')}"
        )
        # Nom exact dépendant du canton
        self.assertRegex(quest.fichier.name, r"clients/.*donze.*.pdf")
