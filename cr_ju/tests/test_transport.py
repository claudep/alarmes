import math
from datetime import date, datetime, time, timedelta, timezone
from decimal import Decimal as D

from dateutil import relativedelta

from django.core import mail
from django.test import TestCase, tag
from django.urls import reverse
from django.utils.timezone import get_current_timezone

from benevole.models import Benevole
from client.models import Client, Referent
from common.choices import Services
from common.export import openxml_contenttype
from common.test_utils import read_xlsx
from transport.models import (
    Adresse, Facture, Frais, Trajet, TrajetCommun, Transport, Vehicule, VehiculeOccup,
)
from transport.tests.tests import DataMixin
from transport.views import LotAFacturer

TRANSPORT = Services.TRANSPORT


class JUTransportTests(DataMixin, TestCase):
    def test_tarifs_transports(self):
        """
        Selon: https://www.croix-rouge-jura.ch/nos-prestations/maintien-domicile/la-cle-de-lautonomie
        Clients AVS : CHF 0.90
        Clients non AVS/EMS : CHF 1.20
        Clients LAA/AI : CHF 1.40
        Forfait par course: CHF 4.00

        Temps d'attente, à partir de 3 heures d'attente: CHF 15.00 (forfait)

        Frais de repas (si transport durant les heures de midi): CHF 20.00

        Frais de parking:  selon quittance
        """
        this_year = date.today().year
        cl_avs = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Léa',
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.815, 47.094],
            date_naissance=date(this_year - 70, 5, 23),
        )
        cl_non_avs = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Julien',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.815, 47.094],
            date_naissance=date(this_year - 60, 5, 23),
        )
        cl_home = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Georges',
            rue='Rue des Crêtets 96', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.815, 47.094],
            date_naissance=date(this_year - 70, 5, 21), type_logement='home',
        )
        cl_laa = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Julie',
            rue='Rue des Crêtets 98', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.815, 47.094],
            date_naissance=date(this_year - 50, 5, 21),
        )
        Referent.objects.create(
            client=cl_laa, nom="Assurance", no_sinistre="123789", facturation_pour=["transp"]
        )
        heure_rdv = datetime.combine(
            date.today(), time(7, 30), tzinfo=get_current_timezone()
        )
        transport_aller = self.create_transport(
            client=cl_avs, heure_rdv=heure_rdv,
            destination=self.hne_ne, retour=False, km=D('30'),
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        # km calculés à partir des trajets: 29.2, arrondis à 30
        transport_aller.client = cl_avs
        cout_avs = transport_aller.donnees_facturation()
        self.assertEqual(cout_avs['cout_km'], D('30') * D('0.9'))
        self.assertNotIn("cout_attente", cout_avs)
        self.assertEqual(cout_avs['cout_forfait'], D('4.0'))
        transport_aller.client = cl_non_avs
        self.assertEqual(transport_aller.donnees_facturation()['cout_km'], D('30') * D('1.2'))
        transport_aller.client = cl_home
        self.assertEqual(transport_aller.donnees_facturation()['cout_km'], D('30') * D('1.2'))
        transport_aller.client = cl_laa
        transport_aller.trajets.update(typ=Trajet.Types.LAA)
        transport_aller = Transport.objects.get(pk=transport_aller.pk)
        self.assertEqual(transport_aller.donnees_facturation()['cout_km'], D('30') * D('1.4'))

    def _creer_transport_a_rapporter(self, **kwargs):
        chauffeur = kwargs.get("chauffeur") or self.create_chauffeur(empl_geo=[7.34, 47.35])  # Delémont
        adr, _ = Adresse.objects.get_or_create(
            nom='Hôpital du Jura', rue='Fbg des Capucins 30', npa='2800', localite='Delémont',
            empl_geo=[7.334, 47.368], validite=(date(2020, 1, 1), None),
        )
        heure_rdv = kwargs.get("heure_rdv") or datetime.combine(
            date.today(), time(7, 30), tzinfo=get_current_timezone()
        )
        create_kwargs = dict(
            client=self.trclient, heure_rdv=heure_rdv, duree_rdv=timedelta(minutes=70),
            destination=adr, retour=True,
            statut=Transport.StatutChoices.EFFECTUE, chauffeur=chauffeur,
        )
        transport = self.create_transport(**(create_kwargs | kwargs))
        transport.calc_chauffeur_dists(save=True)
        return transport

    def test_clients_sans_no_debiteur(self):
        cl = Client.objects.create(
            nom="Toto", rue="Rue Neuve 1", npa="2800", localite="Delémont",
            service=TRANSPORT,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse("clients") + "?listes=no_debit")
        self.assertQuerySetEqual(
            response.context["object_list"], [Client.objects.get(persona__nom="Donzé"), cl]
        )

    def test_calcul_auto_km_duree(self):
        """
        Les km et durée du transport sont calculés automatiquement et non disponibles
        dans le rapport de transport.
        """
        transport_ar = self._creer_transport_a_rapporter()
        self.client.force_login(transport_ar.chauffeur.utilisateur)
        rapport_url = reverse("benevole-rapport", args=[transport_ar.pk])
        response = self.client.get(rapport_url)
        self.assertNotContains(response, 'id="id_km"')
        self.assertNotContains(response, 'id="id_duree_eff"')
        response = self.client.post(rapport_url, data={
            'temps_attente': '1:30',
            'rapport_chauffeur': "Rien à signaler",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        })
        self.assertRedirects(response, reverse("home-app"))
        transport_ar.refresh_from_db()
        self.assertEqual(transport_ar.km, D("197.2"))
        self.assertEqual(
            transport_ar.km * 1000,
            round(transport_ar.chauff_aller_dist + transport_ar.dist_calc + transport_ar.chauff_retour_dist, 1)
        )
        # Prend en compte le temps d'attente saisi
        self.assertEqual(transport_ar.duree_eff, timedelta(seconds=9920))

    def test_recalcul_km_apres_modif_transport(self):
        """
        Un transport modifié alors que les kms sont déjà (auto)calculés, va
        recalculer les kms.
        """
        transport = self._creer_transport_a_rapporter(
            statut=Transport.StatutChoices.RAPPORTE, retour=False, duree_rdv=None, km=120
        )
        autre_dest = Adresse.objects.create(
            nom='HJU', rue='Rue du Stand 125', npa='2800', localite='Delémont',
            empl_geo=[7.33, 47.36], validite=(date(2020, 1, 1), None),
        )

        self.client.force_login(self.user)
        form_data = {
            "date": transport.date,
            "heure_depart": "08:00",
            "heure_rdv": transport.heure_rdv.strftime('%H:%M'),
            "retour": False,
            "typ": Trajet.Types.MEDIC,
            "trajets-INITIAL_FORMS": "1",
            "trajets-TOTAL_FORMS": "1",
            "trajets-0-id": transport.trajets_tries[0].pk,
            "trajets-0-transport": transport.pk,
            "trajets-0-destination_adr": autre_dest.pk,
            "trajets-0-destination_princ": "True",
        }
        response = self.client.post(reverse("transport-edit", args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport.refresh_from_db()
        self.assertNotEqual(transport.km, 120)

    def test_rapport_transport_annule(self):
        """
        En cas de transport annulé, les km et durée effective du transport sont
        automatiquement calculés, mais uniquement sur le trajet chauffeur vers
        client et retour.
        """
        transport_ar = self._creer_transport_a_rapporter()
        self.client.force_login(transport_ar.chauffeur.utilisateur)
        rapport_url = reverse("benevole-rapport", args=[transport_ar.pk])
        response = self.client.post(rapport_url, data={
            'annule': 'on',
            'rapport_chauffeur': "Client en voyage.",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        })
        self.assertRedirects(response, reverse("home-app"))
        # Uniquement les km/durée du chauffeur au client
        transport_ar.refresh_from_db()
        self.assertEqual(transport_ar.km, D("97.8"))
        self.assertEqual(transport_ar.duree_eff, timedelta(seconds=1960))

    def test_rapport_transport_aller_attente_15_min(self):
        """
        Un temps d'attente de 15 minutes est automatiquement ajouté pour les
        allers simples (#428).
        """
        transport = self._creer_transport_a_rapporter(retour=False, duree_rdv=None)
        rapport_url = reverse("benevole-rapport", args=[transport.pk])
        self.client.force_login(transport.chauffeur.utilisateur)
        response = self.client.post(rapport_url, data={
            'annule': '',
            'rapport_chauffeur': "OK",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        })
        self.assertRedirects(response, reverse("home-app"))
        transport.refresh_from_db()
        self.assertEqual(transport.temps_attente, timedelta(minutes=15))

    def test_passage_auto_effectue_en_controle(self):
        transport_recent = self._creer_transport_a_rapporter(
            retour=False, duree_rdv=None, heure_rdv=datetime.combine(
                date.today(), time(7, 30), tzinfo=get_current_timezone()
            )
        )
        heure_il_y_a_3_jours = datetime.combine(
            date.today() - timedelta(days=3), time(7, 30), tzinfo=get_current_timezone()
        )
        transport_passe_ar = self._creer_transport_a_rapporter(
            retour=True, duree_rdv=timedelta(minutes=120), chauffeur=transport_recent.chauffeur,
            destination=self.hne_ne, heure_rdv=heure_il_y_a_3_jours,
        )
        transport_passe_aller = self._creer_transport_a_rapporter(
            retour=False, duree_rdv=None, chauffeur=transport_recent.chauffeur,
            destination=self.hne_ne, heure_rdv=heure_il_y_a_3_jours,
        )
        Transport.check_effectues()
        transport_recent.refresh_from_db()
        self.assertEqual(transport_recent.statut, Transport.StatutChoices.EFFECTUE)
        transport_passe_ar.refresh_from_db()
        transport_passe_aller.refresh_from_db()
        self.assertEqual(transport_passe_ar.statut, Transport.StatutChoices.CONTROLE)
        self.assertEqual(transport_passe_ar.km, D("127.0"))
        self.assertEqual(transport_passe_ar.duree_eff, timedelta(seconds=11720))
        self.assertEqual(transport_passe_ar.temps_attente, transport_passe_ar.duree_rdv)
        self.assertEqual(transport_passe_aller.statut, Transport.StatutChoices.CONTROLE)
        self.assertEqual(transport_passe_aller.km, D("113.0"))
        self.assertEqual(transport_passe_aller.duree_eff, timedelta(seconds=4520))
        self.assertEqual(transport_passe_aller.temps_attente, timedelta(minutes=15))

    def test_defrayer_chauffeurs(self):
        chauffeur1 = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        chauffeur2 = self._create_benevole(
            nom="Plot", prenom="John", activites=['transport'], empl_geo=[6.73, 46.94]
        )
        # Pas contrôlé => non pris en compte
        tr0 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=datetime(2022, 3, 12, 14, 30, tzinfo=get_current_timezone()),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.RAPPORTE
        )
        a16h = datetime(2022, 3, 12, 16, 0, tzinfo=get_current_timezone())
        tr1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=a16h,
            km=D('23.4'), temps_attente=timedelta(minutes=45),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        # 2e chauffeur, avec temps d’attente
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=D('19.2'),
            duree_rdv=timedelta(minutes=120), temps_attente=timedelta(minutes=200),
            destination=self.hne_ne, retour=True, statut=Transport.StatutChoices.CONTROLE
        )
        # 2e chauffeur, avec véhicule CR
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h + timedelta(days=1), km=D('19.2'),
            destination=self.hne_ne, retour=False, vehicule=Vehicule.objects.create(modele='Auto'),
            statut=Transport.StatutChoices.CONTROLE
        )
        Frais.objects.bulk_create([
            Frais(transport=tr0, typ=Frais.TypeFrais.PARKING, cout=D('7.35')),  # Non contrôlé
            Frais(transport=tr1, typ=Frais.TypeFrais.REPAS, cout=D('16.55')),
            Frais(transport=tr1, typ=Frais.TypeFrais.PARKING, cout=D('2.4')),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('defraiements') + '?year=2022&month=3')
        self.assertEqual(
            response.context['object_list'][0], {
                'kms': D('23.4'), 'kms_vhc': 0,
                'nb_attente': 0, 'frais_repas': D('16.55'), 'frais_divers': D('2.4'),
                'total': D('35.35'),  # 23.4×0.7 + 16.55 + 2.4, (35.33 arrondi à 35.35)
                'benevole': chauffeur1,
                'date_export': None,
            }
        )
        self.assertEqual(
            response.context['object_list'][1], {
                'kms': D('19.2'), 'kms_vhc': D('19.2'),
                'nb_attente': D('1'), 'frais_repas': 0, 'frais_divers': 0,
                'total': D('34.20'),  # 19.2×0.7 + 19.2×0.3 + 1 * 15.-
                'benevole': chauffeur2,
                'date_export': None,
            }
        )
        # Exportation OpenXML
        response = self.client.post(reverse('defrayer-chauffeurs', args=[2022, 3]), data={})
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        content = read_xlsx(response.content)
        un_du_mois = datetime(2022, 3, 1, 0, 0)
        self.assertEqual(content[0][0], 'Indemnités chauffeurs mars 2022')
        self.assertEqual(
            content[1], [
                '31.03.2022', chauffeur1.pk, 'Duplain Irma', 'Total', 1, 1, 35.35, None, 35.35
            ]
        )
        # montant: # 19.2 * 0.7 + 19.2 * 0.3 + 15 (attente)
        self.assertEqual(
            content[2], [
                '31.03.2022', chauffeur2.pk, 'Plot John', 'Total', 1, 1, 34.20, None, 34.20
            ]
        )


class FacturesTests(DataMixin, TestCase):
    @tag("factures")
    def test_facturer_transport_avec_attente(self):
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        tr.calculer_km_auto()
        test_data = [
            (160, 0),
            (190, D("15.00")),  # forfait
        ]
        for minutes, cost in test_data:
            tr.temps_attente = timedelta(minutes=minutes)
            if cost == 0:
                self.assertNotIn('cout_attente', tr.donnees_facturation())
            else:
                self.assertEqual(tr.donnees_facturation()['cout_attente'], cost)
        tr.save()
        self.assertEqual(tr.donnees_facturation()['km'], 30)
        self.assertEqual(tr.donnees_facturation()['total'], 50)
        self.client.force_login(self.user)
        self.client.post(
            reverse('facturer-transports', args=[tr.date.year, tr.date.month]),
            data={'date_facture': date.today()}
        )
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        self.assertEqual(self.trclient.factures_transports.first().nb_transp, 1)

    @tag("factures")
    def test_facturer_transport_groupe(self):
        """Transport en commun: kms divisés par le nbre de personnes transportées."""
        chauffeur = self._create_benevole(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        client2 = Client.objects.create(
            nom='Toto', rue='Rue Neuve 1', npa='2800', localite='Delémont', empl_geo=[6.704, 47.03],
            valide_des='2022-01-01', date_naissance=date(date.today().year - 60, 5, 23),  # Non AVS
        )
        a16h = datetime(2024, 3, 12, 16, 0, tzinfo=get_current_timezone())
        tr1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=a16h, km=D('19.2'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        tr2 = self.create_transport(
            client=client2, chauffeur=chauffeur,
            heure_rdv=a16h, km=D('21'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        commun = TrajetCommun.objects.create(chauffeur=chauffeur, date=a16h.date())
        tr1.trajets_tries[0].commun = commun
        tr1.trajets_tries[0].save()
        tr2.trajets_tries[0].commun = commun
        tr2.trajets_tries[0].save()
        donnees_fact1 = tr1.donnees_facturation()
        self.assertEqual(donnees_fact1["km"], math.ceil(tr1.km / 2))
        self.assertEqual(donnees_fact1["cout_forfait"], D("4"))
        self.assertEqual(donnees_fact1["total"], D("13"))
        donnees_fact2 = tr2.donnees_facturation()
        self.assertEqual(donnees_fact2["km"], math.ceil(tr2.km / 2))
        self.assertEqual(donnees_fact2["cout_forfait"], D("4"))
        self.assertEqual(donnees_fact2["total"], D("17.20"))

    @tag("factures")
    def test_facturer_transports_debiteurs_differents(self):
        Referent.objects.create(
            client=self.trclient, nom="Assurance", no_sinistre="123789", facturation_pour=["transp"]
        )
        # 1 transport LAA + 1 transport "normal"
        a16h = datetime(2024, 3, 12, 16, 0, tzinfo=get_current_timezone())
        tr_laa = self.create_transport(
            client=self.trclient, heure_rdv=a16h, km=D('19.2'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE,
            typ=Trajet.Types.LAA
        )
        tr_med = self.create_transport(
            client=self.trclient, heure_rdv=a16h + timedelta(days=2), km=D('19.2'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE,
            typ=Trajet.Types.MEDIC
        )
        LotAFacturer(date(2024, 3, 1), client=self.trclient).facturer(date(2024, 4, 5))
        self.assertEqual(self.trclient.factures_transports.count(), 2)
        f1, f2 = self.trclient.factures_transports.all()
        self.assertQuerySetEqual(f1.get_transports(), [tr_laa])
        self.assertQuerySetEqual(f2.get_transports(), [tr_med])

    @tag("factures")
    def test_facturer_reservations_vehicule(self):
        vhc = Vehicule.objects.create(modele="Auto", no_plaque="JU 12345")
        client = Client.objects.create(
            service=TRANSPORT, nom='Donzé', prenom='Léa', id_externe2=345,
            rue='Rue du Stand 24', npa='2000', localite='Delémont', valide_des='2023-07-01',
            date_naissance=date(1955, 5, 23),
        )
        Referent.objects.create(client=client, nom="Tartempion")
        occup = VehiculeOccup.objects.create(
            vehicule=vhc,
            duree=(
                datetime(2024, 10, 4, 12, 0, tzinfo=timezone.utc),
                datetime(2024, 10, 5, 12, 0, tzinfo=timezone.utc)
            ),
            description="Réservation",
            kms=None,
            client=client,
        )
        occup.refresh_from_db()
        fact_url = reverse("facturer-transports", args=[occup.duree.lower.year, occup.duree.lower.month])
        self.client.force_login(self.user)
        response = self.client.get(fact_url)
        self.assertContains(response, "il reste des réservations de véhicules")
        occup.kms = 36
        occup.save()
        response = self.client.post(fact_url, data={'date_facture': date(2024, 11, 5)})
        occup.refresh_from_db()
        self.assertIsNotNone(occup.facture)
        self.assertEqual(occup.facture.client, client)
        # Exportation CSV
        response = self.client.post(reverse('factures-transport-export'), follow=True)
        self.assertRedirects(response, reverse('finances'))
        self.assertEqual(len(mail.outbox), 1)
        csv_content = mail.outbox[0].attachments[0][1]
        lines = [line for line in csv_content.splitlines()]
        self.assertEqual(lines[1].split(";")[1:], [
            '05.11.2024', '345', '', 'TLOCATION', '3400/40', 'Date : 04/10/2024',
            'Location privée d’un véhicule adapté - forfait', '',
            '1', 'F', '15', '15', '1', '0', 'TRAN',
        ])
        self.assertEqual(lines[2].split(";")[1:], [
            '05.11.2024', '345', '', 'TKG', '3400/10', 'Date : 04/10/2024',
            'Location privée d’un véhicule adapté - kms', '',
            '36', 'F', '1.10', '39.60', '1', '0', 'TRAN',
        ])


class FacturesExportTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.cl_avs = Client.objects.create(
            service=TRANSPORT, nom="Donzé", prenom="Léa", id_externe2=345,
            rue="Rue du Stand 24", npa="2000", localite="Delémont",
            empl_geo=[6.815, 47.094], valide_des="2023-07-01",
            date_naissance=date(date.today().year - 70, 5, 23),
        )
        cls.chauffeur = cls._create_benevole(
            nom="Duplain", prenom="Irma", activites=["transport"], empl_geo=[6.83, 47.01]
        )

    def _export_factures(self, one_fact=False):
        self.client.force_login(self.user)
        # Facturer les transports pendants
        response = self.client.post(
            reverse("facturer-transports", args=["2023", "7"]), data={"date_facture": date.today()}
        )
        self.assertRedirects(response, reverse("finances"))

        post_data = {"fact_id": Facture.objects.first().pk} if one_fact else {}
        response = self.client.post(
            reverse("factures-transport-export"), data=post_data, follow=True
        )
        self.assertRedirects(response, reverse("finances"))
        self.assertEqual(len(mail.outbox), 1)
        csv_content = mail.outbox[0].attachments[0][1]
        lines = [line for line in csv_content.splitlines()]
        self.assertTrue(lines[0].startswith(
            "ID_FACTURE;DATE_FACTURE;ID_CLIENT;FACTURATION_ID;CODE_ARTICLE;"
        ))
        return [line.split(";") for line in lines]

    @tag("factures")
    def test_export_transport_ar_attente_et_frais(self):
        # Premier transport aller-retour, avec temps d'attente et frais repas + parking
        transport_ar = self.create_transport(
            client=self.cl_avs,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=True,
            duree_rdv=timedelta(minutes=30), temps_attente=timedelta(minutes=195),
            statut=Transport.StatutChoices.CONTROLE, chauffeur=self.chauffeur,
        )
        transport_ar.km = round(transport_ar.dist_calc_chauffeur / 1000, 1)
        transport_ar.save(update_fields=["km"])
        Frais.objects.create(transport=transport_ar, typ='repas', cout='17.5')
        Frais.objects.create(transport=transport_ar, typ='parking', cout='2.55')
        lines = self._export_factures()
        date_fact = date.today().strftime("%d.%m.%Y")

        self.assertEqual(len(lines), 6)
        self.assertEqual(Facture.objects.first().montant_total, D("83.55"))
        line_transp = lines[1]
        self.assertEqual(line_transp[1:], [
            date_fact, '345', '', 'TVP', '3400/00', 'Date : 12/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel', '', '45', 'F', '0.90', '40.50', '2', '8', 'TRAN',
        ])
        line_forfait = lines[2]
        line_repas = lines[3]
        self.assertEqual(line_transp[0], line_repas[0])
        self.assertEqual(line_forfait[1:], [
            date_fact, '345', '', 'TFORFAIT', '3400/00', 'Date : 12/07/2023',
            'Forfait aller-retour', '', '1', 'F', '8.00', '8.00', '2', '8', 'TRAN',
        ])
        # Repas facturé à 20.-, quel que soit le montant réel
        self.assertEqual(line_repas[1:], [
            date_fact, '345', '', 'TREPAS', '3400/40', 'Date : 12/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel', '', '1', 'F', '20.00', '20.00', '2', '8', 'TRAN',
        ])
        line_parking = lines[4]
        self.assertEqual(line_parking[1:], [
            date_fact, '345', '', 'TPARKING', '3400/40', 'Date : 12/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel', '', '1', 'F', '2.55', '2.55', '2', '8', 'TRAN',
        ])
        line_attente = lines[5]
        self.assertEqual(line_attente[1:], [
            date_fact, '345', '', 'TATTENTE', '3400/40', 'Date : 12/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel', '', '1', 'F', '15.00', '15.00', '2', '8', 'TRAN',
        ])

    @tag("factures")
    def test_export_transport_adr_fact(self):
        cl_avs_adr_fact = Client.objects.create(
            service=TRANSPORT, nom='Aubry', prenom='Xavier', id_externe2=346,
            rue='Avenue de la Libération 1', npa='2000', localite='Delémont',
            empl_geo=[6.82, 47.11], valide_des='2023-07-01',
            date_naissance=date(date.today().year - 75, 2, 13),
        )
        Referent.objects.create(
            client=cl_avs_adr_fact, nom="AJAM", facturation_pour=["transp"],
            no_sinistre="123.789", id_externe=721, pers_contact="Mme Jobin",
        )
        # Transport pour client avec autre adresse fact. (1x normal, 1x LAA)
        transp_kwargs = dict(
            client=cl_avs_adr_fact,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=15,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        self.create_transport(**transp_kwargs)
        self.create_transport(**{
            **transp_kwargs,
            "heure_rdv": transp_kwargs["heure_rdv"] + timedelta(days=1),
            "typ": Trajet.Types.LAA,
        })
        lines = self._export_factures()

        self.assertEqual(len(lines), 5)
        line_transp1 = lines[1]
        self.assertGreater(int(line_transp1[0]), 90000)
        # Client avec autre adr. de facturation
        date_fact = date.today().strftime("%d.%m.%Y")
        self.assertEqual(line_transp1[1:], [
            date_fact, '346', '', 'TVP', '3400/00', 'Date : 12/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel', '',
            '15', 'F', '0.90', '13.50', '2', '8', 'TRAN',
        ])
        line_transp2 = lines[3]
        self.assertEqual(line_transp2[1:], [
            date_fact, '346', '721', 'TVPSP', '3400/20', 'Date : 13/07/2023',
            'Domicile -> Hôpital neuchâtelois / Neuchâtel',
            'Aubry Xavier, sinistre no 123.789 / Contact: Mme Jobin',
            '15', 'F', '1.40', '21.00', '2', '8', 'TRAN',
        ])

    @tag("factures")
    def test_export_transport_annule(self):
        transp_kwargs = dict(
            client=self.cl_avs,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=True, km=15, duree_rdv=timedelta(hours=1),
            statut=Transport.StatutChoices.ANNULE, chauffeur=None,
            statut_fact=Transport.FacturationChoices.PAS_FACTURER,
        )
        # Annulation sans facturation
        self.create_transport(**transp_kwargs)
        # Annulation avec facturation km.
        self.create_transport(**{
            **transp_kwargs,
            "heure_rdv": transp_kwargs["heure_rdv"] + timedelta(days=1),
            "statut_fact": Transport.FacturationChoices.FACTURER_KM,
            "km": D("8.5"),
        })
        lines = self._export_factures()
        date_fact = date.today().strftime("%d.%m.%Y")
        self.assertEqual(len(lines), 3)
        self.assertEqual(lines[1][1:], [
            date_fact, "345", "", "TVP", "3400/00", "Date : 13/07/2023",
            "Domicile -> Hôpital neuchâtelois / Neuchâtel", "Annulation transport, déplacement chauffeur",
            "9", "F", "0.90", "8.10", "2", "8", "TRAN"
        ])
        self.assertEqual(lines[2][1:], [
            date_fact, "345", "", "TFORFAIT", "3400/00", "Date : 13/07/2023",
            "Forfait aller simple", "", "1", "F", "4.00", "4.00", "2", "8", "TRAN"
        ])
        facture = self.cl_avs.factures_transports.first()
        self.assertEqual(facture.montant_total, D("8.10") + D("4.00"))

    @tag("factures")
    def test_export_transport_une_facture(self):
        transport_ar = self.create_transport(
            client=self.cl_avs,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=20,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=self.chauffeur,
        )
        lines = self._export_factures(one_fact=True)
