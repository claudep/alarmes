from decimal import Decimal

from dateutil import relativedelta

from django.db.models import Q
from django.db.models.functions import TruncMonth
from django.utils.dateformat import format as django_format

from alarme.models import (
    ArticleFacture, Facture, CalculateurFraisAlarme, FacturationPolicyBase,
    MaterielClient,
)
from benevole.models import LigneFrais, TypeFrais
from .export import export_frais_benevoles


class CalculateurFrais(CalculateurFraisAlarme):
    TARIF_KM_ALARME = Decimal('0.70')
    TARIF_VISITES_DEFRAYEES = 0  # Decimal("10.00")

    def frais_par_benevole(self, benevole=None):
        frais_calcules = super().frais_par_benevole(benevole=benevole)
        # Calcul des visites, si dans un autre village que le bénévole (#526).
        if self.TARIF_VISITES_DEFRAYEES > 0:
            for benev, data in frais_calcules.items():
                visites_defr = 0
                if data["num_visites"] > 0:
                    benev_adr = benev.adresse(quand=self.mois.replace(day=28))
                    benev_npa = benev_adr.npa if benev_adr else None
                    for visite in benev.utilisateur.mission_set.annotate(
                        month=TruncMonth('effectuee')
                    ).filter(
                        month=self.mois,
                        type_mission__code="VISITE",
                    ):
                        if visite.client.persona.adresse(quand=visite.effectuee).npa != benev_npa:
                            visites_defr += 1
                data["visites_defrayees"] = visites_defr
        return frais_calcules

    def lignes_depuis_data(self, benevole, data):
        lignes = super().lignes_depuis_data(benevole, data)
        if num_visites := data.get("visites_defrayees", 0) > 0:
            lignes.append(LigneFrais(
                libelle=TypeFrais.objects.get(no="B10"),
                quantite=num_visites, montant_unit=self.TARIF_VISITES_DEFRAYEES,
            ))
        return lignes


class FacturationPolicy(FacturationPolicyBase):
    def get_article_facture(self, install):
        if install.client.boitier_cle == 'c-r':
            article, _ = ArticleFacture.objects.get_or_create(code='TAXE2', defaults={
                'designation': "Frais d'installation et boîtier à clés", 'prix': 170, 'compte': '3410/00'
            })
            return article
        else:
            return super().get_article_facture(install)

    def has_facture_installation(self, install):
        return install.factures.filter(
            Q(article=install.alarme.modele.article_install) | Q(article__code='TAXE2')
        ).exists()

    def creer_facture_mensuelle(self, mois, installs, date_factures):
        factures = super().creer_facture_mensuelle(mois, installs, date_factures)
        un_du_mois = mois.replace(day=1)
        install1 = installs[0]
        client = install1.client
        pour_materiel = isinstance(install1, MaterielClient)
        client, nom_client_sfx = self.get_client_et_suffixe(client)
        if pour_materiel:
            libelle = f'{install1.abonnement.nom} - %s{nom_client_sfx}'
        else:
            libelle = f'Service d’alarme à domicile - %s{nom_client_sfx}'
        # Create factures for next months, if needed
        for i in range(4):
            nextmonth = un_du_mois + relativedelta.relativedelta(months=i + 1)
            if (
                client is not None and nextmonth.month in (1, 4, 7, 10) and
                not Facture.objects.filter(mois_facture=nextmonth).exists()
            ):
                # Only bill for next quarter if there are already bills for the month
                break
            if client.has_facture_for_month(nextmonth, install1.abonnement.article):
                continue
            # Check if abo ends
            duree_installations = sum(install.duree_pour_mois(nextmonth) for install in installs)
            if duree_installations < 15:
                break
            montant = install1.abonnement.article.prix
            fact = Facture.objects.create(
                client=client, date_facture=nextmonth, mois_facture=nextmonth,
                install=install1 if not pour_materiel else None,
                article=install1.abonnement.article,
                materiel=install1 if pour_materiel else None,
                libelle=libelle % django_format(nextmonth, "F Y"),
                montant=montant, exporte=None
            )
            factures.append(fact)
        return factures

    def calculateur_frais(self, mois):
        return CalculateurFrais(mois)

    def export_frais_benevoles(self, frais_par_benevole, un_du_mois):
        return export_frais_benevoles("Frais bénévoles alarme", frais_par_benevole, un_du_mois)
