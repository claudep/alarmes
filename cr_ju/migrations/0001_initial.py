from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientJU',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('accord_police', models.BooleanField(default=False, verbose_name='Accord avant d’envoyer police ou ambulance')),
                ('fasd', models.BooleanField(default=False, verbose_name='Client «Fondation d’aides et soins à domicile»')),
                ('client', models.OneToOneField(on_delete=models.deletion.CASCADE, to='client.client')),
            ],
        ),
    ]
