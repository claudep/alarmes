from django.db import migrations


def create_profiles(apps, schema_editor):
    Client = apps.get_model('client', 'Client')
    ClientJU = apps.get_model('cr_ju', 'ClientJU')
    for client in Client.objects.all():
        ClientJU.objects.create(client=client, accord_police=False)


class Migration(migrations.Migration):

    dependencies = [
        ('cr_ju', '0001_initial'),
        ('client', '__first__'),
    ]

    operations = [
        migrations.RunPython(create_profiles)
    ]
