from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cr_ju', '0002_creation_clientju'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clientju',
            name='accord_police',
        ),
    ]
