from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cr_ju', '0003_remove_clientju_accord_police'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientju',
            name='adapart',
            field=models.BooleanField(default=False, verbose_name='Client «Adapart»'),
        ),
        migrations.AddField(
            model_name='clientju',
            name='megapart',
            field=models.BooleanField(default=False, verbose_name='Client «Megapart»'),
        ),
    ]
