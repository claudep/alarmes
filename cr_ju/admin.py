from django.contrib import admin

from .models import ClientJU


@admin.register(ClientJU)
class ClientJUAdmin(admin.ModelAdmin):
    list_display = ["client", "fasd", "adapart", "megapart"]
    list_filter = ["fasd", "adapart", "megapart"]
    search_fields = ["client__persona__nom"]
    raw_id_fields = ["client"]
