from django.db import models

from client.models import Client


class ClientJU(models.Model):
    """
    Extension du modèle Client pour des spécificités du canton du Jura.
    Instance créée par signal (cf. signals.py).
    """
    client = models.OneToOneField(Client, on_delete=models.CASCADE)
    fasd = models.BooleanField("Client «Fondation d’aides et soins à domicile»", default=False)
    adapart = models.BooleanField("Client «Adapart»", default=False)
    megapart = models.BooleanField("Client «Megapart»", default=False)

    def __str__(self):
        return f"Données JU pour client {self.client}"
