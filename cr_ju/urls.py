from django.urls import path

from . import views

urlpatterns = [
    path('factures/alarme/export/', views.FacturesAlarmeExportView.as_view(),
        name='factures-alarme-export'),
    path('factures/transport/export/', views.FacturesTransportExportView.as_view(),
        name='factures-transport-export'),
]
