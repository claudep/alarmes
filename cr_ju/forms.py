from django import forms

from common.forms import BootstrapMixin, ReadOnlyableMixin
from .models import ClientJU


class ClientImportForm(forms.Form):
    fichier = forms.FileField(label="Clients transport à importer")

    def clean_fichier(self):
        fichier = self.cleaned_data['fichier']
        if not fichier.name.endswith('.xlsx'):
            raise forms.ValidationError("Ce fichier ne semble pas être un fichier Excel")
        return fichier


class ClientJUForm(ReadOnlyableMixin, BootstrapMixin, forms.ModelForm):
    class Meta:
        model = ClientJU
        fields = ["fasd", "adapart", "megapart"]

    def __init__(self, client=None, user_is_benev=False, **kwargs):
        self.client = client
        if client:
            kwargs['instance'] = client.clientju
        super().__init__(**kwargs)

    def save(self, client, **kwargs):
        if self.instance.client_id is None:
            client.clientju.delete()  # Supprimer l'instance par défaut
            self.instance.client = client
        return super().save()
