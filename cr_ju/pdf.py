from datetime import date
from functools import partial

from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.utils.dateformat import format as django_format
from django.utils.text import slugify

from reportlab.lib.colors import black
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    BaseDocTemplate, Frame, HRFlowable, Image, KeepTogether, PageBreak,
    PageTemplate, Paragraph, Spacer, Table, TableStyle
)

from common.pdf import BaseCroixrougePDF, PageNumCanvas


class PDFHeaderFooter:
    LOGO = find('img/logo-cr-ju.png')
    ZEWO = find('img/logo-zewo.png')

    def draw_header(self, canvas, doc):
        canvas.saveState()
        canvas.drawImage(
            self.LOGO, doc.leftMargin + 320, doc.height + 80,
            width=7 * cm, height=1.6 * cm, preserveAspectRatio=True, mask="auto"
        )
        canvas.restoreState()

    def draw_footer(self, canvas, doc):
        canvas.saveState()
        page_height = A4[1]
        canvas.drawImage(
            self.ZEWO, doc.leftMargin, page_height - 810, 1.2 * cm, 1.2 * cm, preserveAspectRatio=True, mask='auto'
        )
        tab = [220, 340, 430]
        line = [785, 795, 805, 815]

        canvas.setFont("Helvetica-Bold", 8)
        canvas.drawRightString(doc.leftMargin + tab[0], page_height - line[0], "Croix-Rouge suisse")
        canvas.drawRightString(doc.leftMargin + tab[0], page_height - line[1], "Canton du Jura")
        canvas.setLineWidth(0.5)
        line_x = doc.leftMargin + tab[0] + 12
        canvas.line(line_x, 2.2 * cm, line_x, 1.0 * cm)

        canvas.setFont("Helvetica", 8)
        canvas.drawRightString(doc.leftMargin + tab[1], page_height - line[0], "Rue de l’Eglise 13")
        canvas.drawRightString(doc.leftMargin + tab[1], page_height - line[1], "2900 Porrentruy")
        line_x = doc.leftMargin + tab[1] + 12
        canvas.line(line_x, 2.2 * cm, line_x, 1.0 * cm)

        line_x = doc.leftMargin + doc.width - 0.5 * cm
        canvas.drawRightString(line_x, page_height - line[0], "Tél. 032 465 84 00")
        canvas.drawRightString(line_x, page_height - line[1], "Fax 032 466 16 83")
        canvas.drawRightString(line_x, page_height - line[2], "info@croix-rouge-jura.ch")
        canvas.drawRightString(line_x, page_height - line[3], "www.croix-rouge-jura.ch")
        line_x = doc.leftMargin + doc.width
        canvas.line(line_x, 2.2 * cm, line_x, 1.0 * cm)

        canvas.restoreState()


class CourrierAlarmePDF(BaseCroixrougePDF):
    title = 'Courrier Alarme Croix-Rouge'
    FONTSIZE = 11

    def get_filename(self, client):
        return f'courrier_bienvenue_{client.nom}.pdf'

    def produce(self, client):
        para_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE,
            alignment=TA_JUSTIFY, spaceAfter=0.3 * cm
        )
        small_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE - 2,
            alignment=TA_JUSTIFY, spaceAfter=0.3 * cm
        )
        style_r = ParagraphStyle(name='Right', alignment=TA_RIGHT)
        P = partial(Paragraph, style=self.style_normal)

        self.story.append(Spacer(1, 1.6 * cm))
        adresse = client.adresse()
        lignes_adresse = [
            client.titre,
            f"{client.noms} {client.prenoms}",
            adresse.rue,
            f"{adresse.npa} {adresse.localite}",
        ]
        if adresse.c_o:
            lignes_adresse.insert(2, adresse.c_o)
        table_data = [[
            Paragraph("<b>Poste CH SA</b><br/><b>P.P. 2900 Porrentruy</b> - CRJU - Rue de l’Église 13", style_r),
            '',
        ], [
            HRFlowable(width="100%", thickness=0.5, color=black), '',
        ], [
            P("<br/>".join(lignes_adresse)),
            P(f"Porrentruy, le {django_format(date.today(), 'j F Y')}"),
        ]]
        self.story.append(Table(
            data=table_data, colWidths=[9 * cm, 9 * cm], rowHeights=[None, 0.2 * cm, None],
            style=TableStyle([
                ('BOTTOMPADDING', (0, 0), (-1, 0), 0),
                ('VALIGN', (0, 0), (-1, -1), "BOTTOM"),
            ]),
            hAlign=TA_LEFT,
        ))

        self.story.append(Spacer(1, 1 * cm))
        self.story.append(P("<b>Alarme Croix-Rouge</b>"))
        self.story.append(Spacer(1, 0.7 * cm))

        Pjust = partial(Paragraph, style=para_style)
        self.story.append(P(f'{client.titre},'))
        paras = [
            "Par la présente, nous vous souhaitons la bienvenue parmi nos clients Alarme "
            "Croix-Rouge. Nous espérons que l’appareil et le service choisi vous offrira "
            " entière satisfaction.",
            "Pensez bien à nous avertir du moindre changement - adresse, numéro de téléphone, "
            "n° digicode, changement de répondant, etc. –  au <b>032 465 84 01 "
            "du lundi au jeudi de 8h00 à 11h30</b> pour que nos prestations restent optimales.",
            "Si vous désirez effectuer un ordre permanent pour le règlement de vos factures "
            "d’abonnement, nous vous communiquons nos coordonnées bancaires :",
        ]
        for para in paras:
            self.story.append(Pjust(para))
        table_data = [[
            P("Banque :"), P("Banque Raiffeisen Ajoie, 2900 Porrentruy")
        ], [
            P("Bénéficiaire :"), P("Croix-Rouge du Canton du Jura, 2900 Porrentruy")
        ], [
            P("IBAN :"), P("CH82 8080 8005 5751 4017 0")
        ]]
        self.story.append(Table(
            data=table_data, colWidths=[3 * cm, 15 * cm],
            style=TableStyle([('VALIGN', (0, 0), (-1, -1), "BOTTOM")]),
            hAlign=TA_LEFT,
        ))
        self.story.append(Spacer(1, 0.2 * cm))
        paras = [
            "Nous vous informons que vous pouvez ajouter les factures d’alarme aux frais de maladie dans votre déclaration d’impôts (formule 9).",
            "Nous vous invitons à prendre contact avec votre assurance complémentaire, car il se peut qu’elle paie certains frais dû à l’installation de votre alarme.",
            "Nous vous remercions également d’avoir pensé à faire appel à notre association cantonale et de cette façon, de nous soutenir dans les divers services d’aide que nous proposons à la population jurassienne.",
            "En restant à votre disposition pour toute demande d’informations complémentaires, "
            f"nous vous adressons, {client.titre}, nos cordiales salutations."
        ]
        for para in paras:
            self.story.append(Pjust(para))
        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P("Croix-Rouge Suisse du canton du Jura"))
        self.story.append(Spacer(1, 0.4 * cm))
        if settings.SIGNATURES.get('MNobel'):
            sig1_content = Image(settings.MEDIA_ROOT / settings.SIGNATURES['MNobel'], 3 * cm, 1.2 * cm)
        else:
            sig1_content = Spacer(1, 0.7 * cm)
        if settings.SIGNATURES.get('SBerret'):
            sig2_content = Image(settings.MEDIA_ROOT / settings.SIGNATURES['SBerret'], 3 * cm, 1.2 * cm)
        else:
            sig2_content = Spacer(1, 0.7 * cm)

        table_data = [[
            P("Responsable Alarme"), P("Responsable comptabilité"),
        ], [
            sig1_content, sig2_content
        ], [
            P("Manuela Nobel"), P("Sophie Berret"),
        ], [
            Paragraph("032 465 84 01<br/>alarme@croix-rouge-jura.ch", style=small_style),
            Paragraph("032 465 84 02<br/>comptabilite@croix-rouge-jura.ch", style=small_style),
        ]]
        self.story.append(Table(
            data=table_data, colWidths=[9 * cm, 9 * cm],
            style=TableStyle([('VALIGN', (0, 0), (-1, -1), "BOTTOM")]),
            hAlign=TA_LEFT,
        ))

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class ContratPDF(BaseCroixrougePDF):
    title = 'Contrat alarme Croix-Rouge'
    FONTSIZE = 10

    def init_doc(self, tampon):
        width = self.page_size[0] - self.left_margin - self.right_margin
        height = self.page_size[1] - self.top_margin - self.bottom_margin
        frameT = Frame(self.left_margin, self.bottom_margin, width, height - 1.5 * cm, id='normal')
        return BaseDocTemplate(
            tampon,
            pageTemplates=[
                PageTemplate(id='Main', frames=frameT, onPage=self.draw_header_footer, pagesize=self.page_size),
            ],
            title=self.title,
            leftMargin=self.left_margin, rightMargin=self.right_margin,
            topMargin=self.top_margin, bottomMargin=self.bottom_margin
        )

    def define_styles(self):
        super().define_styles()
        self.bold14 = ParagraphStyle(
            name='Bold 14', fontName='Helvetica-Bold', fontSize=14, leading=16
        )
        self.para_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE,
            spaceAfter=0.2 * cm
        )
        self.title_style = ParagraphStyle(
            name='Bold 11', fontName='Helvetica-Bold', fontSize=11, leading=12,
            spaceBefore=0.4 * cm, spaceAfter=0.2 * cm
        )
        registerFont(TTFont('DejaVuSans', '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'))

    def get_filename(self, client):
        return f"contrat_{slugify(client.nom_prenom)}_{date.today().strftime('%Y_%m_%d')}.pdf"

    def draw_header_footer(self, canvas, doc):
        super().draw_header_footer(canvas, doc)
        canvas.saveState()
        canvas.setFont("Helvetica-Bold", 12)
        canvas.drawString(doc.leftMargin, doc.height + 2 * cm, "Contrat client alarme Croix-Rouge")
        canvas.restoreState()

    def produce(
        self, client, appareil=None, abo=None, pour_signature=False, signature_client=None,
        signature_cr=None, signature_cr_nom=None, signature_lieu=None
    ):
        from alarme.models import ArticleFacture

        contractant = client.contractant()
        if client is contractant:
            adr = client.adresse()
            adresse = (
                f'{client.titre}<br/>{contractant.nom_prenom}<br/>{adr.rue}<br/>'
                f'{adr.npa} {adr.localite}'
            )
        else:
            adresse = (
                f'Madame/Monsieur<br/>{contractant.prenom} {contractant.nom}<br/>{contractant.rue}<br/>'
                f'{contractant.npa} {contractant.localite}'
            )
        # Page 1
        self.story.append(Spacer(1, 0.5 * cm))
        table_data = [
            [
                'entre la',
                Paragraph(
                    '<b>Croix-Rouge jurassienne<br/>Rue de l’Eglise 13<br/>2900 Porrentruy</b>',
                    self.style_normal
                )
            ],
            ['(ci-après «la CRJU»)', ''],
            [Spacer(1, 1 * cm), ''],
            ['et', Paragraph(f'<b>{adresse}</b>', self.style_normal)],
            ['(ci-après «le client»)', ''],
        ]
        self.story.append(Table(
            data=table_data, colWidths=[9 * cm, 9 * cm],
            style=TableStyle([('VALIGN', (0, 0), (-1, -1), "TOP")]),
            hAlign=TA_LEFT, spaceBefore=2 * cm,
        ))
        self.story.append(Spacer(1, 3 * cm))
        self.story.append(Paragraph(
            "* Dans le présent contrat, le masculin désigne les personnes des deux sexes.",
            self.style_normal
        ))
        self.story.append(PageBreak())
        # Page 2
        self.story.append(Paragraph("1. Objet du contrat", self.title_style))
        paras = [
            "La CRJU gère la prestation Alarme Croix-Rouge à l’intention des clients du canton du Jura. "
            "Elle est habilitée à sous-traiter tout ou partie des prestations couvertes par le présent contrat.",
            "Les prestations sont proposées exclusivement en Suisse.",
            "Le client loue ou achète un dispositif à la CRJU. Grâce à cet appareil, il peut établir en "
            "tout temps une liaison téléphonique avec la centrale d’alarme Croix-Rouge ou donner une alerte "
            "à un répondant. Il peut ainsi transmettre des informations détaillées à la centrale, qui "
            "organise les secours adéquats selon ses indications et ses souhaits.",
            "La centrale d’alarme Croix-Rouge est opérationnelle 7/7 et 24h/24. Elle dispose des "
            "installations techniques nécessaires pour répondre aux appels d’urgence et aux autres appels "
            "des clients. Les appels sont traités par du personnel formé à cet effet et capable d’organiser "
            "les secours adéquats. Toutes les conversations avec la centrale d’alarme Croix-Rouge sont "
            "enregistrées.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(Paragraph("2. Éléments du contrat", self.title_style))
        self.story.append(Paragraph(
            "Le questionnaire ainsi que le choix de prestations ci-dessous et les descriptifs y relatifs "
            "font partie intégrante du présent contrat.", self.para_style)
        )
        self.story.append(Paragraph("2.1 Alarme", self.title_style))
        if appareil:
            self.story.append(Paragraph(
                f'<font name="DejaVuSans">☑</font>     Alarme {appareil.modele}', self.para_style
            ))
        else:
            self.story.append(Paragraph(
                '<font color="#ff0000"><b>Le type d’alarme n’est pas encore défini</b></font>',
                self.para_style
            ))

        # Page 3
        self.story.append(Paragraph("3 Modalités des prestations", self.title_style))
        paras = [
            "Les prestations d’aide sont organisées en priorité selon les indications figurant sur le "
            "questionnaire. Toutes les mesures d’intervention prises après clarification de la situation "
            "par le personnel de la centrale d’alarme Croix-Rouge et considérées comme nécessaires sont "
            "mises en œuvre pour le compte du client et en son nom.",
            "Toute modification apportée au questionnaire, en particulier un changement de répondant, "
            "doit immédiatement être signalée par écrit à la CRJU ou auprès de notre centrale d’alarme.",
            "Les éventuels dégâts occasionnés par l’intervention de tiers (p. ex. police, ambulanciers, "
            "etc.) pour entrer dans le bâtiment sont à la charge du client. Les prestataires mandatés "
            "facturent leurs services directement au client.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(Paragraph("4. Coûts", self.title_style))
        self.story.append(Paragraph(
            "Les coûts occasionnés par les prestations couvertes par le présent contrat se fondent "
            "sur la liste des prix actuelle disponible sur www.croix-rouge-jura.ch.", self.para_style)
        )
        self.story.append(Paragraph(
            "Ils font l’objet d’une facture payable sous 30 jours. Les mois entamés sont intégralement facturés.",
            self.para_style)
        )
        self.story.append(Paragraph("5. Participation du client", self.title_style))
        paras = [
            "Le client garantit que toutes les personnes répondantes mentionnées dans le questionnaire "
            "ont consenti à intervenir en cas d’incident et possèdent toutes une clé de son domicile "
            "ou ont un accès à l’habitation grâce à (une boîte à clé, une cachette etc…). Le client "
            "est responsable des agissements, des risques provoqués ou subit par ses répondants.",
            "Toute modification relative au questionnaire doit être immédiatement communiquée à la CRJU.",
            "En cas de modification apportée à ses répondants, le client en informe immédiatement la CRJU par écrit.",
            "Il lui signale à l’avance toute absence prolongée.",
            "Il utilise les dispositifs qui lui sont confiés avec précaution et en "
            "signale immédiatement tout dysfonctionnement.",
            "À l’échéance du contrat, la CRJU organise le retrait des appareils loués par un bénévole CRJU. "
            "Avec notre accord, il est aussi possible de restituer les appareils directement à notre bureau.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(Paragraph("6. Responsabilité", self.title_style))
        prix_emetteur = ArticleFacture.objects.get(code="EMETTEUR").prix
        paras = [
            "La CRJU veille au respect rigoureux des obligations stipulées dans le présent contrat "
            "et au bon fonctionnement des appareils loués. En revanche, elle décline expressément "
            "toute responsabilité, dans les limites autorisées par la loi, pour tout dommage "
            "consécutif ou indirect, ainsi que pour les personnes chargées des secours. Cela vaut "
            "également pour la responsabilité extracontractuelle.",
            "La CRJU n’endosse notamment aucune responsabilité concernant le fonctionnement des "
            "moyens de communication mis en œuvre pour transmettre l’alarme (p. ex. ligne "
            "téléphonique, réception du téléphone mobile (GSM), réception des sms, transmission "
            "radio, etc.).",
            "Le client est responsable des dommages découlant du non-respect de ses obligations "
            "contractuelles, tel qu’un maniement inadéquat de l’appareil ou l’indication de données "
            "inexactes ou non actuelles dans le questionnaire. Un forfait pour le déplacement d’un "
            "collaborateur de la CRJU sera alors facturé à CHF 50.-.",
            "En cas de perte ou dommage des accessoires ou des appareils loués, le client "
            "reconnaît devoir un montant de :<br/>"
            f"   -  Pour un bracelet émetteur : CHF {prix_emetteur} y.c. TVA<br/>"
            "   -  Pour un câble : CHF 30.00 y.c. TVA<br/>"
            "   -  Pour un appareil Casa : CHF 400.00 y.c. TVA<br/>"
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(Paragraph("7. Durée du contrat et résiliation", self.title_style))
        paras = [
            "Le présent contrat prend effet pour une durée indéterminée à compter de la mise en service "
            "de l’Alarme Croix-Rouge. Il peut être résilié en tout temps pour la fin du mois par chacune "
            "des parties.",
            "La résiliation sans délai reste réservée, notamment en cas de violation grave du contrat "
            "ou au non-paiement des honoraires dans les délais. L’utilisation abusive de l’alarme "
            "Croix-Rouge est un motif de dissolution de contrat sans délai de résiliation.",
            "La durée minimale de validité du présent contrat est de 3 mois. (en cas de décès, "
            "cette close est caduque)",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        para_num = 8
        self.story.append(KeepTogether([
            Paragraph(f"{para_num}. Protection des données", self.title_style),
            Paragraph(
                "Le traitement des données par la CRJU se fait conformément à la législation en "
                "vigueur. Seules sont collectées, enregistrées et traitées les données nécessaires "
                "à la fourniture des prestations, à l’administration et à la gestion de la relation "
                "client ainsi qu’à la facturation. Le client donne son accord explicite à cet égard.",
                self.para_style
            ),
        ]))
        if client.clientju.fasd:
            self.story.append(Paragraph(
                "Conformément aux bases légales relatives à la protection des données et dans un but "
                "statistique et de reporting comptable, le client donne son accord pour que les "
                "données nécessaires aux éléments précités soient échangées entre la Fondation pour "
                "l’aide et les soins à domicile Jura (abrégé FASD) et la CRJU.",
                self.para_style
            ))
            para_num += 1
            self.story.append(Paragraph(f"{para_num}. Indemnisation", self.title_style))
            self.story.append(Paragraph(
                "Pour le travail de formation du personnel, d’information et de conseil, la FASD "
                "perçoit une indemnisation de la part de la CRJU sous forme d’un versement unique "
                "et forfaitaire mentionné dans la convention entre la FASD et la CRJU. Ce montant "
                "est communiqué en tout temps sur demande du client.",
                self.para_style
            ))

        para_num += 1
        self.story.append(Paragraph(f"{para_num}. Dispositions finales", self.title_style))
        paras = [
            "Toute clause accessoire au présent contrat est réputée non contraignante "
            "si elle est formulée par oral. Les modifications et compléments apportés "
            "requièrent la forme écrite.",
            "La cession de droits afférents au présent contrat est exclue.",
            "Avant d’en référer à la justice, les parties s’engagent à régler par voie de "
            "négociation toute divergence de vues ou tout différend quant au présent contrat. "
            "Au cas où une disposition contractuelle s’avère lacunaire, il convient de la "
            "remplacer par une disposition correspondant le plus fidèlement possible à la "
            "volonté initiale des parties.",
            "La nullité d’une disposition particulière du présent contrat n’affecte aucunement "
            "la validité des autres dispositions. Les deux parties s’accordent pour considérer "
            "qu’au cas où une disposition contractuelle s’avère lacunaire, il convient de la "
            "remplacer par une disposition correspondant le plus fidèlement possible à leur volonté initiale.",
            "Le for juridique est Porrentruy pour tout litige découlant du présent contrat.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))

        if not pour_signature:
            hr = HRFlowable(width="100%", thickness=0.5, color=black)
            para_num += 1
            self.story.append(Paragraph(f"{para_num}. Signature du client", self.title_style))
            self.story.append(Spacer(1, 0.3 * cm))
            if signature_lieu:
                lieu_para = Paragraph(
                    f"Lieu, date: {signature_lieu}, le {date.today().strftime('%d.%m.%Y')}", self.para_style
                )
            else:
                lieu_para = Paragraph("Lieu, date:", self.para_style)
            self.story.append(lieu_para)
            if signature_client:
                im = Image(signature_client, 2 * cm, 2 * cm)
                self.story.append(im)
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Signature", self.para_style))

            para_num += 1
            self.story.append(Paragraph(f"{para_num}. Signatures  de la Croix-Rouge jurassienne", self.title_style))
            self.story.append(Spacer(1, 0.3 * cm))
            self.story.append(lieu_para)
            if signature_cr:
                im = Image(signature_cr, 2 * cm, 2 * cm)
                self.story.append(im)
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Signature", self.para_style))

        self.doc.build(self.story, canvasmaker=PageNumCanvas)
