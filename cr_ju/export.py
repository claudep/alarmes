from common.export import ExpLine, OpenXMLExport
from common.utils import last_day_of_month


def export_frais_benevoles(titre, liste_notes, un_du_mois):
    export = OpenXMLExport(col_widths=[12, 10, 22, 10, 10, 10, 10, 10, 10])
    export.write_line(ExpLine([titre], bold=True))
    for idx, note in enumerate(liste_notes, start=2):
        total = note.somme_totale()
        if total == 0:
            continue
        export.write_line([
            last_day_of_month(un_du_mois).strftime('%d.%m.%Y'),
            note.benevole.pk, f"{note.benevole.nom} {note.benevole.prenom}",
            "Total", 1, 1, total, "", total,
        ])
    return export
