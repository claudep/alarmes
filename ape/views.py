from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView

from alarme.views import FactureListView as AlarmeFactureListView
from common.export import ExpLine
from common.views import CreateUpdateView, ListView

from ape.models import Appart, Facture

from .forms import AppartFinForm, AppartForm


class AppartListView(ListView):
    model = Appart
    template_name = 'ape/appart_list.html'

    def get_queryset(self):
        return super().get_queryset().order_by('npa', 'rue', 'no_app')


class AppartEditView(PermissionRequiredMixin, CreateUpdateView):
    model = Appart
    permission_required = 'ape.change_appart'
    template_name = 'ape/appart_edit.html'
    form_class = AppartForm
    success_url = reverse_lazy('apparts')

    def get_queryset(self):
        return super().get_queryset().filter(date_archive__isnull=True).order_by('gerant', 'no_app')

    def get_success_message(self, obj):
        if self.is_create:
            return "Un nouvel appartment a été créé."
        else:
            return f"L’appartement «{obj} a bien été modifié."


class AppartClientFinView(FormView):
    form_class = AppartFinForm
    template_name = 'general_edit.html'

    def form_valid(self, form):
        appart = get_object_or_404(Appart, pk=self.kwargs['pk'])
        liaison = appart.clientappart_set.get(duree__endswith=None)
        liaison.duree = (liaison.duree.lower, form.cleaned_data['date_fin'])
        liaison.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class FactureListView(AlarmeFactureListView):
    model = Facture
    template_name = 'alarme/factures_ape.html'
    title = "Factures APE"

    def get_queryset(self, **kwargs):
        return super(AlarmeFactureListView, self).get_queryset(**kwargs).select_related('appart', 'abo'
            ).order_by('-date_facture', 'appart__gerant__nom')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        need_send = any(fact.exporte is None for fact in context['object_list'])
        context['trans_url'] = reverse('factures-transmission', args=['ape']) if need_send else ''
        return context

    def export_lines(self, context):
        yield ExpLine(['Date de facture', 'Gérance', 'Appartement', 'Abonnement'], bold=True)
        for facture in self.get_queryset():
            yield [
                facture.date_facture, str(facture.appart.gerant),
                str(facture.appart), str(facture.abo),
            ]
