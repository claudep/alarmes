from datetime import date, timedelta
from decimal import Decimal

from freezegun import freeze_time

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from alarme.models import Alarme, ArticleFacture, Installation, ModeleAlarme, TypeAbo
from client.models import Client, Referent
from common.choices import Services
from common.export import openxml_contenttype
from common.models import Utilisateur
from .models import Appart, ClientAppart


class APETests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(*list(Permission.objects.filter(codename__in=[
            'view_appart', 'change_appart', 'change_client', 'view_facture',
        ])))
        article = ArticleFacture.objects.create(
            code='ABO1', designation='Abonnement mensuel CASA', prix=Decimal('37.45')
        )
        TypeAbo.objects.create(nom='standard', article=article)
        cls.referent = Referent.objects.create(nom='Gérance')
        cls.alarme = Alarme.objects.create(
            modele=ModeleAlarme.objects.create(nom='Neat NOVO 4G'), no_appareil=1
        )

    def _create_ape(self, no, avec_client=False):
        appart = Appart.objects.create(
            gerant=self.referent, no_app=f'N°{no}', rue='Place de la Gare 4',
            npa='2300', localite='La Chaux-de-Fonds',
            abo=TypeAbo.objects.get(nom='standard')
        )
        if avec_client:
            client = Client.objects.create(
                nom="Dupond", prenom="Ladislas",
                rue="Place de la Gare 4", npa="2300", localite="La Chaux-de-Fonds",
                date_naissance=date(1945, 12, 3), service=Services.ALARME,
            )
            debut = date(2023, 12, 3)
            Installation.objects.create(
                client=client, alarme=self.alarme, abonnement=appart.abo,
                nouvelle=True, date_debut=debut,
            )
            cl_app = ClientAppart.objects.create(
                appart=appart, client=client, duree=(debut, None)
            )
        return appart

    def test_ape_creation(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('appart-new'))
        self.assertContains(response, "Appartement")
        response = self.client.post(reverse('appart-new'), data={
            'gerant': self.referent.pk,
            'gerant_select': 'Gérance',
            'no_app': 'N°1',
            'rue': 'Place de la Gare 4',
            'npalocalite': '2300 La Chaux-de-Fonds',
            'abo': TypeAbo.objects.get(nom='standard').pk,
        })
        self.assertRedirects(response, reverse('apparts'))
        appart = Appart.objects.get(no_app='N°1')
        self.assertEqual(appart.gerant, self.referent)

    def test_ape_choix_client(self):
        appart = self._create_ape('1')
        client_potentiel = Client.objects.create(
            nom="Dupond", prenom="Ladislas",
            rue="Place de la Gare 4", npa="2300", localite="La Chaux-de-Fonds",
            date_naissance=date(1945, 12, 3), service=Services.ALARME,
        )
        autre_client = Client.objects.create(
            nom="Dupont", prenom="Georges", service=Services.ALARME
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('appart-edit', args=[appart.pk]))
        self.assertQuerySetEqual(
            response.context['form'].fields['client_select'].queryset,
            [client_potentiel]
        )
        response = self.client.post(reverse('appart-edit', args=[appart.pk]), data={
            'gerant': self.referent.pk,
            'gerant_select': 'Gérance',
            'client_select': client_potentiel.pk,
            'no_app': 'N°1',
            'rue': 'Place de la Gare 4',
            'npalocalite': '2300 La Chaux-de-Fonds',            
        })
        self.assertRedirects(response, reverse('apparts'))
        appart.refresh_from_db()
        cl_app = appart.clientappart_set.first()
        self.assertEqual(cl_app.client, client_potentiel)
        self.assertEqual(str(cl_app.duree), f"[{date.today().strftime('%Y-%m-%d')}, None)")

    def test_ape_fin_client(self):
        appart = self._create_ape('1', avec_client=True)
        self.client.force_login(self.user)
        response = self.client.post(reverse('clientappart-fin', args=[appart.pk]), data={
            'date_fin': date.today(),
        })
        self.assertEqual(response.json(), {'reload': 'page', 'result': 'OK'})
        cl_app = appart.clientappart_set.first()
        self.assertEqual(cl_app.duree.upper, date.today())

    def test_fin_auto_si_resiliation(self):
        appart = self._create_ape('1', avec_client=True)
        clientappart = appart.clientappart_set.first()
        install = clientappart.client.installation_set.first()
        self.client.force_login(self.user)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            # Form install
            'date_fin_abo': date.today(),
            'motif_fin': 'décès',
            'remarques': 'Annoncé ce jour par sa fille',
            'interv_cb': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        clientappart.refresh_from_db()
        self.assertEqual(clientappart.duree.upper, date.today())

    @freeze_time("2024-04-04")
    def test_facturation_mois_vide(self):
        # ape nouveau
        self._create_ape('1')
        # ape avec client "actif"
        self._create_ape('2', avec_client=True)
        # ape sans client depuis < 3 mois 
        ape = self._create_ape('3', avec_client=True)
        ape.clientappart_set.update(duree=(date(2023, 10, 1), date(2024, 2, 25)))
        # ape sans client depuis > 3 mois 
        ape = self._create_ape('4', avec_client=True)
        ape.clientappart_set.update(duree=(date(2023, 1, 1), date(2023, 11, 25)))

        factures = Appart.generer_factures(date(2024, 3, 1), date(2024, 4, 4))
        self.assertEqual(len(factures), 1)
        self.assertEqual(factures[0].appart.gerant.nom, 'Gérance')
        self.assertEqual(str(factures[0]), "Facture APE du 04.04.2024 pour Gérance")
        factures = Appart.generer_factures(date(2024, 3, 1), date(2024, 4, 4))
        self.assertEqual(factures, [])

    def test_factures_list(self):
        ape = self._create_ape('3', avec_client=True)
        ape.clientappart_set.update(duree=(date(2023, 10, 1), date.today() - timedelta(days=60)))
        factures = Appart.generer_factures(date.today().replace(day=1), date.today())
        self.client.force_login(self.user)
        response = self.client.get(reverse('appart-factures'))
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(reverse('appart-factures') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
