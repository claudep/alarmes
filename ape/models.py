"""
Appartements avec encadrement.
"""
from datetime import date

from django.contrib.postgres.fields import DateRangeField
from django.db import models
from django.db.backends.postgresql.psycopg_any import DateRange

from alarme.models import TypeAbo
from client.models import Client, Referent
from common.stat_utils import Month


class Appart(models.Model):
    gerant = models.ForeignKey(Referent, on_delete=models.PROTECT)
    no_app = models.CharField("N° app.", max_length=10)
    rue = models.CharField("Rue", max_length=120)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    abo = models.ForeignKey(TypeAbo, on_delete=models.SET_NULL, null=True, blank=True)
    remarques = models.TextField(blank=True)
    date_archive = models.DateField("Archivé le", blank=True, null=True)

    def __str__(self):
        return f"Appartement {self.no_app}, {self.rue}, {self.npa} {self.localite}"

    @classmethod
    def generer_factures(cls, mois: date, date_factures: date = None):
        factures = []
        month = Month.from_date(mois)
        appart_vides = cls.objects.annotate(
            num_cl=models.Count(
                'clientappart',
                filter=models.Q(clientappart__duree__overlap=DateRange(*month.limits()))
            )
        ).filter(num_cl=0)
        limite_vide_3_mois = month.previous(3).limits()[0]
        for vide in appart_vides:
            # Ne facturer que les 3 premiers mois de vide.
            last_occup = vide.clientappart_set.filter(duree__endswith__lt=mois).order_by('-duree__endswith').first()
            if last_occup and last_occup.duree.upper > limite_vide_3_mois and not vide.has_facture_for_month(mois):
                factures.append(
                    Facture.objects.create(appart=vide, mois_facture=mois, date_facture=date_factures, abo=vide.abo)
                )
        return factures

    def client_actuel(self):
        if not self.pk:
            return None
        clientapp = self.clientappart_set.filter(duree__endswith=None).select_related('client').first()
        return clientapp.client if clientapp else None

    def has_facture_for_month(self, debut_mois):
        return self.factures_ape.filter(mois_facture=debut_mois).exists()


class ClientAppart(models.Model):
    appart = models.ForeignKey(Appart, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    duree = DateRangeField()


class Facture(models.Model):
    appart = models.ForeignKey(Appart, on_delete=models.CASCADE, related_name='factures_ape')
    abo = models.ForeignKey(TypeAbo, on_delete=models.PROTECT, related_name='+')
    mois_facture = models.DateField("Mois comptable", blank=True, null=True)
    date_facture = models.DateField("Date de facturation")
    exporte = models.DateTimeField("Exporté vers compta", blank=True, null=True)
    export_err = models.TextField("Erreur d’exportation", blank=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return f'Facture APE du {self.date_facture.strftime("%d.%m.%Y")} pour {self.appart.gerant}'

    @classmethod
    def non_transmises(cls):
        return cls.objects.filter(exporte__isnull=True).order_by('date_facture')

    def adresse_facturation(self):
        return self.appart.gerant

    @property
    def autre_debiteur(self):
        return self.adresse_facturation()
