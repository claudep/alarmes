from django.contrib import admin

from .models import Appart, ClientAppart


@admin.register(Appart)
class AppartAdmin(admin.ModelAdmin):
    list_display = ["no_app", "rue", "gerant"]
    raw_id_fields = ["gerant"]


@admin.register(ClientAppart)
class ClientAppartAdmin(admin.ModelAdmin):
    list_display = ["appart", "client", "duree"]
    raw_id_fields = ["client"]
