from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0117_rabaisauto'),
        ('ape', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Facture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mois_facture', models.DateField(blank=True, null=True, verbose_name='Mois comptable')),
                ('date_facture', models.DateField(verbose_name='Date de facturation')),
                ('exporte', models.DateTimeField(blank=True, null=True, verbose_name='Exporté vers compta')),
                ('export_err', models.TextField(blank=True, verbose_name='Erreur d’exportation')),
                ('id_externe', models.BigIntegerField(blank=True, null=True)),
                ('abo', models.ForeignKey(on_delete=models.deletion.PROTECT, related_name='+', to='alarme.typeabo')),
                ('appart', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='factures_ape', to='ape.appart')),
            ],
        ),
    ]
