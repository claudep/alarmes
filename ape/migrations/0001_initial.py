import django.contrib.postgres.fields.ranges
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('alarme', '__first__'),
        ('client', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no_app', models.CharField(max_length=10, verbose_name='N° app.')),
                ('rue', models.CharField(max_length=120, verbose_name='Rue')),
                ('npa', models.CharField(max_length=5, verbose_name='NPA')),
                ('localite', models.CharField(max_length=30, verbose_name='Localité')),
                ('remarques', models.TextField(blank=True)),
                ('date_archive', models.DateField(blank=True, null=True, verbose_name='Archivé le')),
                ('abo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='alarme.typeabo')),
                ('gerant', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='client.referent')),
            ],
        ),
        migrations.CreateModel(
            name='ClientAppart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duree', django.contrib.postgres.fields.ranges.DateRangeField()),
                ('appart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ape.appart')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='client.client')),
            ],
        ),
    ]
