from django.urls import path

from . import views

urlpatterns = [
    path('', views.AppartListView.as_view(), name='apparts'),
    path('nouveau/', views.AppartEditView.as_view(is_create=True), name='appart-new'),
    path('<int:pk>/edition/', views.AppartEditView.as_view(is_create=False),
        name='appart-edit'),
    path('<int:pk>/fin/', views.AppartClientFinView.as_view(), name='clientappart-fin'),
    path('factures/', views.FactureListView.as_view(), name='appart-factures'),
]
