from datetime import date

from django import forms
from django.db.models import Count
from django.urls import reverse_lazy

from client.models import Client
from common.choices import Services
from common.forms import BootstrapMixin, DateInput, NPALocaliteMixin
from .models import Appart, ClientAppart


class AppartForm(BootstrapMixin, NPALocaliteMixin, forms.ModelForm):
    gerant_select = forms.CharField(label="Gérant", widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'placeholder': "Recherche d’adresse…",
        'data-searchurl': reverse_lazy('referent-fact-search'),
        'data-pkfield': 'gerant',
    }))
    client_select = forms.ModelChoiceField(label="Client", queryset=None)

    class Meta:
        model = Appart
        fields = [
            'gerant', 'gerant_select', 'client_select', 'no_app', 'rue', 'npa', 'localite', 'npalocalite',
            'abo', 'remarques',
        ]
        widgets = {
            'gerant': forms.HiddenInput,
        }

    def __init__(self, instance=None, **kwargs):
        if instance and instance.gerant:
            kwargs['initial'] = {'gerant_select': instance.gerant.get_full_name()}
        super().__init__(instance=instance, **kwargs)
        if instance and instance.client_actuel() is None:
            self.fields['client_select'].queryset = Client.objects.par_service(
                Services.ALARME
            ).avec_adresse(date.today()).annotate(
                numapp=Count('clientappart'),
            ).filter(
                numapp=0, adresse_active__rue=instance.rue, adresse_active__npa=instance.npa,
                adresse_active__localite=instance.localite
            ).order_by('persona__nom')
        else:
            del self.fields['client_select']

    def save(self, **kwargs):
        appart = super().save(**kwargs)
        if 'client_select' in self.changed_data:
            client = self.cleaned_data['client_select']
            install = client.alarme_actuelle()
            debut = install.date_debut if install else date.today()
            ClientAppart.objects.create(client=client, appart=appart, duree=(debut, None))
        return appart


class AppartFinForm(forms.Form):
    date_fin = forms.DateField(widget=DateInput)
