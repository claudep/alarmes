from datetime import datetime
import json
import logging

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import mail_admins
from django.http import Http404, JsonResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from client.forms import notify_other_teams
from client.models import Persona, Referent
from client.views import PersonaJournalMixin
from common.choices import Services
from common.views import GeoAddressMixin
from benevole.models import Activite, Benevole, TypeActivite
from .forms import PersonaForm, ReferentForm
from .signals import TITLE_MAP_REVERSE, disconnect_sync_signals

logger = logging.getLogger('api')


class APIAccessCheckMixin:
    """Mixin checking the origin IP and the Bearer auth token to access the API."""
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        remote_ip = request.META['REMOTE_ADDR']
        auth_header = request.headers.get('Authorization')
        token = auth_header.split()[-1] if auth_header else None
        if remote_ip not in settings.API_ALLOWED_IPS:
            logger.error(f"IP {remote_ip} not in allowed IPs")
            raise PermissionDenied("Invalid request origin or token")
        if token not in settings.API_ALLOWED_TOKENS:
            logger.error(f"Token {token} not in allowed tokens")
            raise PermissionDenied("Invalid request origin or token")
        return super().dispatch(request, *args, **kwargs)


class PersonaUpdateView(APIAccessCheckMixin, GeoAddressMixin, PersonaJournalMixin, View):
    def map_tel_fields(self, data, persona, prefix="telephone_set"):
        tels = [tel for tel in [data["phone"], data["mobile"]] if tel]
        tel_data = {
            f"{prefix}-INITIAL_FORMS": 0,
            f"{prefix}-TOTAL_FORMS": 0,
        }
        # Synchro des no de tél (2 premiers (limite CID))
        exist_tels = persona.telephones()[:2]
        equal_tels = set(tels) & set([t.tel for t in exist_tels])
        for idx, etel in enumerate(exist_tels):
            tel_data[f"{prefix}-INITIAL_FORMS"] += 1
            tel_data[f"{prefix}-TOTAL_FORMS"] += 1
            tel_data.update({
                f"{prefix}-{idx}-persona": persona.pk,
                f"{prefix}-{idx}-id": etel.pk,
                f"{prefix}-{idx}-tel": etel.tel,
                f"{prefix}-{idx}-ORDER": idx + 1,
                f"{prefix}-{idx}-remarque": etel.remarque,
            })
            if etel.tel not in equal_tels:
                tel_data[f"{prefix}-{idx}-DELETE"] = "on"
        for ntel in tels:
            if ntel not in equal_tels:
                tel_data[f"{prefix}-TOTAL_FORMS"] += 1
                idx = tel_data[f"{prefix}-TOTAL_FORMS"] - 1
                tel_data.update({
                    f"{prefix}-{idx}-persona": persona.pk,
                    f"{prefix}-{idx}-id": None,
                    f"{prefix}-{idx}-tel": ntel,
                    f"{prefix}-{idx}-ORDER": idx + 1,
                })
        return tel_data

    def map_persona_fields(self, data, persona=None):
        if persona:
            tel_data = self.map_tel_fields(data, persona)
        else:
            tels = [tel for tel in [data["phone"], data["mobile"]] if tel]
            tel_data = {
                "telephone_set-INITIAL_FORMS": 0,
                "telephone_set-TOTAL_FORMS": len(tels),
            }
            for idx, tel in enumerate(tels):
                tel_data.update({
                    f"telephone_set-{idx}-tel": tel,
                })
        if data["street"] and data.get("street2"):
            c_o = data["street"]
            rue = data["street2"]
        else:
            c_o = ""
            rue = data["street"]
        return {
            'nom': data["name"],
            'prenom': data["firstname"],
            'genre': data.get("gender", 'M' if data.get("title") == "Sir" else 'F'),
            'case_postale': data.get("street3", ""),
            'c_o': c_o,
            'rue': rue,
            'npa': data["zip"],
            'localite': data["city"],
            'courriel': data["email"],
            'date_naissance': None if data["birthdate"] is False else data["birthdate"],
            'date_deces': None if data.get("deathdate", False) is False else data["deathdate"],
        } | tel_data

    def map_referent_fields(self, data, referent=None):
        if referent:
            tel_data = self.map_tel_fields(data, referent, prefix="referenttel_set")
        else:
            tels = [tel for tel in [data["phone"], data["mobile"]] if tel]
            tel_data = {
                "referenttel_set-INITIAL_FORMS": 0,
                "referenttel_set-TOTAL_FORMS": len(tels),
            }
            for idx, tel in enumerate(tels):
                tel_data.update({
                    f"referenttel_set-{idx}-tel": tel,
                })
        mapping = {
            'salutation': TITLE_MAP_REVERSE.get(data['title'], ''),
            'nom': data["name"],
            'prenom': data["firstname"],
            'case_postale': data["street3"],
            'npa': data["zip"],
            'localite': data["city"],
            'courriel': data["email"],
        } | tel_data
        if data["street"] and data["street2"]:
            mapping['complement'] = data["street"]
            mapping['rue'] = data["street2"]
        else:
            mapping['rue'] = data["street"] or data["street2"]
            mapping['complement'] = ""
        return mapping

    def post(self, request, *args, **kwargs):
        # Création persona (ou référent)
        self.is_create = True
        data = json.loads(request.body)
        logger.info(f"POST data received from CID on {request.path}: {data}")
        if not data["firstname"]:
            # Creation société côté CID, créer un référent sans client.
            form_class = ReferentForm
            form_data=self.map_referent_fields(data)
        else:
            form_class = PersonaForm
            form_data=self.map_persona_fields(data)
        try:
            form = form_class(instance=None, data=form_data)
            is_valid = form.is_valid()
        except Exception as err:
            err_msg = f"Unkwown error {err}. Send data: {data}"
            mail_admins(f"CID Error on {request.path}", err_msg)
            return JsonResponse({'error': {'code': 400, 'message': err_msg}})

        if is_valid:
            self.object, local_id = self.create_instance(form, data)
        elif form.doublons:
            logger.error(f"Persona looks like duplicate of id {form.doublons[0].pk}")
            return JsonResponse({'error': {
                'code': 409, 'message': "Cette personne semble être un doublon.",
                'doublon_id': form.doublons[0].pk,
            }})
        else:
            logger.error(f"Persona form errors: {form.errors.as_text()}")
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.",
                'errors': form.errors.get_json_data(),
            }})
        return JsonResponse({'id': local_id})

    def put(self, request, *args, **kwargs):
        """Mise à jour d'un persona ou référent depuis CID."""
        self.is_create = False
        data = json.loads(request.body)
        logger.info(f"PUT data received from CID on {request.path}: {data}")
        referents = []
        try:
            self.persona = Persona.objects.get(id_externe=data["id"])
        except Persona.DoesNotExist:
            self.persona = None
            referents = Referent.objects.filter(id_externe=data["id"])
        if not self.persona and not referents:
            raise Http404("Aucun partenaire avec cet identifiant")
        with disconnect_sync_signals():
            if self.persona:
                return self.update_persona(data)
            else:
                for referent in referents:
                    errors = self.update_referent(referent, data)
                    if errors:
                        return JsonResponse({'error': {
                            'code': 400, 'message': "Sorry, the data are not valid.",
                            'errors': errors.get_json_data(),
                        }})
                return JsonResponse({'id': f'r{referents[0].pk}'})

    def create_instance(self, form, data):
        id_externe = data["id"]
        prefix = "p"
        if isinstance(form, ReferentForm):
            form.instance.id_externe = id_externe
            prefix = "r"
        with disconnect_sync_signals():
            instance = form.save()
            if isinstance(form, PersonaForm):
                persona = instance
                persona.id_externe = id_externe
                persona.save()
                self.object = persona
                self.journalize(form, add_message="Création de la personne par CID")
        local_id = f"{prefix}{instance.pk}"
        return instance, local_id

    def update_persona(self, data):
        self.journal_edit_message = "Modification du client par CID: {fields}"
        form = PersonaForm(instance=self.persona, data=self.map_persona_fields(data, persona=self.persona))
        if form.is_valid():
            self.object = form.save()
            self.journalize(form)
            if client := self.object.get_client():
                notify_other_teams(form, client)
            return JsonResponse({'id': f'p{self.persona.pk}'})
        else:
            logger.error(f"Persona update form errors: {form.errors.as_text()}")
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})

    def update_referent(self, referent, data):
        self.journal_edit_message = "Modification du contact «{obj}» par CID: {fields}"
        form = ReferentForm(instance=referent, data=self.map_referent_fields(data, referent=referent))
        # On renonce à synchroniser en entrée les no de tél. des référents, car
        # ils sont plus précis côté alarme.
        if form.is_valid():
            referent = form.save()
            self.object = referent.client.persona if referent.client else None
            self.journalize(form)
        else:
            return form.errors


class BenevoleUpdateView(PersonaUpdateView):
    def create_instance(self, form, data):
        # ID reçu pour benevole, pas persona !
        persona, _ = super().create_instance(form, {**data, "id": None})
        benevole = Benevole.objects.create(id_externe=data["id"], persona=persona)
        for act in data.get('services', []):
            try:
                # Chercher d'abord un code correspondant aux services MAD
                type_activite = TypeActivite.service_map()[act['name']][0]
            except KeyError:
                # Chercher ensuite un code d'activité reconnu
                type_activite = TypeActivite.par_label(act['name'])
                if not type_activite:
                    mail_admins(
                        "CID synchro: unknown activity",
                        f"Activité {act['name']} inconnue pour le bénévole {benevole.pk}."
                    )
                    continue
            Activite.objects.create(
                benevole=benevole,
                type_act=type_activite,
                duree=(datetime.strptime(act['date_start'], '%Y-%m-%d').date(), None),
            )
        return persona, benevole.pk

    def map_benev_fields(self, data):
        return {
            'nom': data["name"],
            'prenom': data["firstname"],
            'genre': data["gender"],
            'rue': data["street"],
            'npa': data["zip"],
            'localite': data["city"],
            'courriel': data.get("email", ""),
            'date_naissance': data.get("birthdate"),
        }

    def put(self, request, *args, **kwargs):
        """Mise à jour bénévole"""
        self.is_create = False
        data = json.loads(request.body)
        logger.info(f"PUT data received from CID on {request.path}: {data}")
        try:
            self.benev = Benevole.objects.get(id_externe=data["id"])
        except Benevole.DoesNotExist:
            # Création du bénévole si non existant dans la base
            logger.warning(f"No benevole with external id {data['id']}, creating it")
            return self.post(request, *args, **kwargs)

        form = PersonaForm(
            instance=self.benev.persona,
            data=self.map_benev_fields(data) | self.map_tel_fields(data, self.benev.persona)
        )
        if form.is_valid():
            self.journal_edit_message = "Modification du bénévole par CID: {fields}"
            with disconnect_sync_signals():
                form.save()
            self.object = form.instance
            self.journalize(form)
            # Mettre à jour activités
            existing_acts = self.benev.activite_set.all().select_related('type_act')
            existing_servs = set(
                act.type_act.get_service_display() if act.type_act.service else act.type_act.nom
                for act in existing_acts
            )
            for act in data.get('services', []):
                if act['name'] not in existing_servs:
                    # Nouvelle activité de bénévole
                    if act['name'] in TypeActivite.service_map():
                        activite = TypeActivite.service_map()[act['name']][0]
                    else:
                        try:
                            activite = TypeActivite.objects.get(nom=act['name'])
                        except TypeActivite.DoesNotExist:
                            msg = f"Activité «{act['name']}» inconnue (pour bénévole {self.benev})"
                            mail_admins("CID synchro error", msg)
                            activite = TypeActivite.objects.create(
                                code=slugify(act['name']), nom=act['name']
                            )
                    fin = None
                    if act['date_end']:
                        fin = datetime.strptime(act['date_end'], '%Y-%m-%d').date()
                    Activite.objects.create(
                        benevole=self.benev,
                        type_act=activite,
                        duree=(datetime.strptime(act['date_start'], '%Y-%m-%d').date(), fin),
                    )
                elif act['name'] not in [
                    Services.ALARME.label, Services.TRANSPORT.label, Services.VISITE.label
                ]:
                    # Synchroniser activités autre que alarme/transport/visite
                    try:
                        activite = self.benev.activite_set.get(type_act__nom=act['name'])
                    except Activite.DoesNotExist:
                        continue
                    debut = datetime.strptime(act['date_start'], '%Y-%m-%d').date()
                    fin = datetime.strptime(act['date_end'], '%Y-%m-%d').date() if act['date_end'] else None
                    if activite.duree != (debut, fin):
                        activite.duree = (debut, fin)
                        activite.save()
            return JsonResponse({'id': self.benev.pk})
        else:
            logger.error(f"Benevole form errors: {form.errors.as_text()}")
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})
