import json
from datetime import date

import httpx

from django.conf import settings
from django.core.mail import mail_admins, send_mail
from django.utils.dateformat import format as django_format

from common.utils import canton_abrev

CID_JOURNAL_CODES = {
    'alarme': 'F520',  # Facturation MAD - Alarme (CHF)
    'ape': 'F520',  # Facturation MAD - Alarme (CHF)
    'transport': 'F510',  # Facturation MAD - Transports (CHF)
    'osad': 'F500',  # Facturation MAD - OSAD (MedLink) (CHF)
    'deb': 'DEB',  # Factures débiteurs (CHF)
}
# F200: Facturation FORMATION (CHF)
# F400: Facturation SAP_GED/ASE (MedLink) (CHF)
# F210: Facturation BABY-SITTING (CHF)
CID_COMPTES_TRANSPORT = {
    'AVS': '51.100',
    'NON_AVS': '51.110',
    'FORFAIT_ALLER': '51.120',
    'FORFAIT_AR': '51.130',
    'ATTENTE': '51.140',
    'FRAIS_PARKING': '51.300',
    'FRAIS_REPAS': '51.310',
    'ANNULATION': '51.200',
}


class ApiError(Exception):
    pass


def is_success(response):
    try:
        json_resp = response.json()
    except ValueError:
        return False
    return response.is_success and 'error' not in json_resp


def report_error(response, operation, json_sent=None, raise_=False):
    try:
        resp_data = response.json()
    except Exception:
        error = "Unknown error"
    else:
        error = str(resp_data.get('error', ''))
    msg = (
        f"{operation}, got response with status code {response.status_code} from CID server. "
        f"Error details: {error}"
    )
    if json_sent:
        msg += f"\nJSON payload: {json.dumps(json_sent, indent=2)}"
    mail_admins("CID Error", msg)
    if raise_:
        raise ApiError(error)


class BearerAuth(httpx.Auth):
    def __init__(self):
        self.token = settings.CID_API_TOKEN

    def auth_flow(self, request):
        request.headers["authorization"] = f"Bearer {self.token}"
        yield request


class CIDApi:
    def __init__(self, output=None):
        # Not defined on the class to allow for settings override (mainly tests)
        self.partner_url = f"{settings.CID_API_URL}partners"
        self.employees_url = f"{settings.CID_API_URL}employees"
        self.invoices_url = f"{settings.CID_API_URL}invoices"
        self.cancel_invoice_url = f"{settings.CID_API_URL}invoices/{{pk}}/status"
        self.expenses_url = f"{settings.CID_API_URL}expenses"
        # output is an alternative callable to receive the send call.
        self.output = output

    def _send_to_server(self, method, url, data):
        method = self.output or method
        try:
            response = method(url, json=data, auth=BearerAuth(), timeout=60)
        except httpx.TimeoutException:
            # Store on a stack and retry later?
            mail_admins(
                "CID timeout",
                f"Got timeout when trying to send data to {url} to CID server."
            )
            raise
        return response

    def post(self, url, data):
        return self._send_to_server(httpx.post, url, data)

    def put(self, url, data):
        return self._send_to_server(httpx.put, url, data)

    def create_invoice(self, type_facture, factures):
        if type_facture not in CID_JOURNAL_CODES:
            raise ValueError("Type de facture non pris en charge")
        facture0 = factures[0]
        journal = CID_JOURNAL_CODES[type_facture]
        debiteur = facture0.autre_debiteur or facture0.client
        deb_id_externe = debiteur.id_externe
        if not deb_id_externe:
            raise ValueError(
                "Impossible d’envoyer une facture si le débiteur n’est pas lié à un partenaire CID"
            )
        fact_data = {
            "debitorid": deb_id_externe,
            "beneficiaryid": facture0.client.id_externe if hasattr(facture0, 'client') else deb_id_externe,
            "invoicedate": facture0.date_facture.strftime("%Y-%m-%d"),
            "period": facture0.mois_facture.strftime("%m/%Y") if facture0.mois_facture else "",
            "journal": journal,  # Journal comptable, dépendant du type de prestation
            "lines": [],
        }
        if contact_name := getattr(debiteur, "pers_contact", ""):
            fact_data["contact_name"] = contact_name
        if type_facture == 'alarme':
            def sort_lines(fact):
                lower_descr = fact.article.designation.lower()
                if 'abonnement' in lower_descr:
                    return 0
                if 'installation' in lower_descr:
                    return 1
                return 2

            for fact in sorted(factures, key=sort_lines):
                fact_data["lines"].append({
                    #"productid": 26, -> ID CID du produit
                    "productref": fact.article.code,
                    "description": fact.libelle,
                    "quantity": 1.0,
                })
                if fact.montant:
                    fact_data["lines"][-1]["unitamount"] = float(fact.montant)
                if fact.rabais:
                    if 'abonnement' in fact.article.designation.lower():
                        code = '52.920'
                    elif (
                        'installation' in fact.article.designation.lower() or
                        'intervention' in fact.article.designation.lower()
                    ):
                        code = '52.940'
                    else:
                        code = '52.910'
                    fact_data["lines"].append({
                        "productref": code,
                        "description": "Escomptes, rabais",
                        "quantity": 1.0,
                        "unitamount": -float(fact.rabais),
                    })

        elif type_facture == 'ape':
            fact_data["lines"].append({
                "productref": facture0.abo.article.code,
                "description": (
                    f"{facture0.abo.nom} - {django_format(facture0.mois_facture, 'F Y')} "
                    f"- Appartement {facture0.appart.no_app}"
                ),
                "quantity": 1.0,
            })

        elif type_facture == 'transport':
            if getattr(debiteur, 'no_sinistre', ''):
                fact_data["claimnumber"] = debiteur.no_sinistre
            for transp in facture0.get_transports():
                donnees = transp.donnees_facturation()
                aller_ret = transp.aller_ret_map.get(transp.retour)
                date_str = django_format(donnees['rendez-vous'], 'd.m.Y H:i')
                date_str_iso = django_format(donnees['rendez-vous'], 'Y-m-d H:i:00')
                if donnees['km'] > 0:
                    fact_data["lines"].append({
                        "productref": (
                            CID_COMPTES_TRANSPORT['AVS' if donnees['avs'] else 'NON_AVS']
                        ),
                        "description": f"Transport du {date_str}",
                        "code_transport": transp.pk,
                        "date_transport": date_str_iso,
                        "type_transport": transp.trajets_tries[0].get_typ_display(),
                        "sens_transport": aller_ret['label'],
                        "start_transport": donnees['depart'],
                        "end_transport": donnees['destination'],
                        "quantity": float(donnees['km']),
                    })
                if donnees.get('cout_forfait'):
                    fact_data["lines"].append({
                        "code_transport": transp.pk,
                        "productref": (
                            CID_COMPTES_TRANSPORT['FORFAIT_AR' if transp.retour else 'FORFAIT_ALLER']
                        ),
                        "description": f"Prise en charge {aller_ret['label'].lower()}",
                        "quantity": 1.0,
                    })
                if donnees.get('nb_attente'):
                    fact_data["lines"].append({
                        "code_transport": transp.pk,
                        "productref": CID_COMPTES_TRANSPORT['ATTENTE'],
                        "description": "Temps d’attente",
                        "quantity": float(donnees['nb_attente']),
                    })
                if donnees.get('annulation'):
                    fact_data["lines"].append({
                        "code_transport": transp.pk,
                        "date_transport": date_str_iso,
                        "type_transport": transp.trajets_tries[0].get_typ_display(),
                        "sens_transport": aller_ret['label'],
                        "start_transport": donnees['depart'],
                        "end_transport": donnees['destination'],
                        "productref": CID_COMPTES_TRANSPORT['ANNULATION'],
                        "description": "Annulation de dernière minute",
                        "quantity": 1.0,
                    })
                for frais in donnees.get('frais', []):
                    compte = (
                        CID_COMPTES_TRANSPORT['FRAIS_REPAS'] if frais['typ'] == 'repas'
                        else CID_COMPTES_TRANSPORT['FRAIS_PARKING']
                    )
                    fact_data["lines"].append({
                        "code_transport": transp.pk,
                        "productref": compte,
                        "description": frais['descr'],
                        "quantity": 1.0,
                        "unitamount": float(frais['cout']),
                    })
        response = self.post(self.invoices_url, fact_data)
        return response.json()

    def cancel_invoice(self, type_facture, facture):
        put_data = {'type': 'refund'}
        response = self.put(self.cancel_invoice_url.format(pk=facture.id_externe), put_data)
        if not is_success(response):
            err_msg = response.json().get("error", {}).get("message", "")
            if "is in 'paid' state" in err_msg:
                raise ApiError("La facture est déjà dans l’état «Payée» dans CID. Impossible de l’annuler.")
            report_error(response, "Trying to cancel facture", json_sent=put_data, raise_=True)
        if settings.COMPTA_EMAIL:
            send_mail(
                f"[Croix-Rouge {canton_abrev()}] Note de crédit / {facture.client}",
                f"Une correction de facture a été effectuée pour {facture.client} ({facture.no}).\n"
                f"Motif: {facture.annulee_msg}\n\n"
                "Message automatique de la plateforme MAD, ne pas répondre.",
                None,
                [settings.COMPTA_EMAIL],
            )
        return response.json()

    def send_expense(self, note):
        if not note.benevole.id_externe:
            raise ValueError(f"Le bénévole {note.benevole} n'est pas synchronisé sur CID")
        note_data = {
            "id": note.pk,
            "date": date.today().strftime('%Y-%m-%d'),  # Date du mois ?
            "description": f"NF {note.service} - {django_format(note.mois, 'F Y')}",
            "employeeid": note.benevole.id_externe,
            "lines": [],
        }
        for line in note.lignes.all():
            note_data['lines'].append({
                "productref": line.libelle.no,
                "quantity": float(line.quantite),
                "servicetype": note.service,
            })
            if not line.libelle.montant_fixe and line.montant_unit:
                note_data['lines'][-1]["unitamount"] = float(line.montant_unit)
        if note.id_externe:
            # Mise à jour
            response = self.put(self.expenses_url, note_data)
        else:
            response = self.post(self.expenses_url, note_data)
        return response.json()
