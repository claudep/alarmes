from contextlib import contextmanager

import httpx

from django.conf import settings
from django.core.mail import mail_admins, send_mail
from django.db.models import Q
from django.db.models.signals import pre_save

from client.models import Client, Referent, client_saved, referent_saved
from benevole.models import Benevole, benevole_saved
from .core import BearerAuth, CIDApi, is_success, report_error

TITLE_MAP = {
    "M": "Sir",
    "F": "Madam",
    "A": "Maître",
    "E": "",
    "P": "Madame et Monsieur",
}
TITLE_MAP_REVERSE = {v: k for k, v in TITLE_MAP.items()}


class CIDSyncHandlerBase:
    """after save signal listener to sync Client/Referent/Benevole to CID."""
    api_timeout = 15
    model = None
    external_id_field = 'id_externe'
    # Correspondance des champs à synchroniser vers CID
    synced_fields = {}
    required_fields = []

    def __init__(self):
        self.api = CIDApi()

    def __call__(self, sender, instance, created, **kwargs):
        if self._missing_values(instance):
            return
        if not getattr(instance, self.external_id_field):
            return self.post_object(instance)
        if created:
            return self.post_object(instance)
        else:
            changed_fields = self.put_object(instance, force=kwargs.get('force', False))
            return bool(changed_fields)

    def _missing_values(self, instance):
        return [fname for fname in self.required_fields if getattr(instance, fname) in (None, '')]

    @classmethod
    def _add_adr_fields(cls, pers):
        adr = pers.adresse()
        for field_name in ['c_o', 'rue', 'npa', 'localite']:
            setattr(pers, field_name, getattr(adr, field_name))

    @classmethod
    def cache_previous_instance(cls, sender, instance, *args, **kwargs):
        """pre_save signal listener to keep previous instance in cache."""
        if hasattr(instance, '_original_obj_data'):
            return
        if instance.id:
            original_obj = instance.__class__.objects.get(pk=instance.id)
            instance._original_obj_data = cls.build_json_data(original_obj)
        else:
            instance._original_obj_data = {}

    @classmethod
    def serialize(cls, instance, field_name):
        value = getattr(instance, field_name)
        if field_name in ['date_naissance', 'date_deces']:
            value = value.strftime('%Y-%m-%d') if value else False
        elif field_name in ['archive_le', 'date_archive']:
            return not bool(value)
        return value

    @classmethod
    def build_json_data(cls, instance):
        json_data = {'id': instance.pk, 'services': []}
        if ext_id := getattr(instance, cls.external_id_field):
            json_data['external_id'] = ext_id
        for fname, json_name in cls.synced_fields.items():
            json_data[json_name] = cls.serialize(instance, fname)
        if json_data.get('title') in TITLE_MAP.keys():
            json_data['title'] = TITLE_MAP.get(json_data['title'], '')
        if cls.model in [Benevole, Client]:
            tels = instance.telephones()
            if len(tels) > 1 and (tels[0].est_mobile, tels[1].est_mobile) in ((False, False), (True, True)):
                json_data['phone'] = tels[0].tel
                json_data['mobile'] = tels[1].tel
            else:
                json_data['phone'] = next((tel.tel for tel in tels if not tel.est_mobile), '')
                json_data['mobile'] = next((tel.tel for tel in tels if tel.est_mobile), '')
        if cls.model == Referent:
            if hasattr(instance, 'phone'):  # en cache
                json_data['phone'] = instance.phone
                json_data['mobile'] = instance.mobile
            else:
                tels = instance.telephones() if instance.pk else []
                json_data['phone'] = next((tel.tel for tel in tels if not tel.est_mobile), '')
                json_data['mobile'] = next((tel.tel for tel in tels if tel.est_mobile), '')
        if getattr(instance, 'pays', None) and instance.pays != "CH":
            json_data['country'] = instance.pays
        else:
            json_data['country'] = "NE" if ("2000" <= instance.npa <= "2525") else "CH"
        return json_data

    def post_object(self, instance):
        # Push new object to CID
        json_sent = self.build_json_data(instance)
        try:
            response = httpx.post(self.api_url, json=json_sent, auth=BearerAuth(), timeout=self.api_timeout)
        except httpx.TimeoutException:
            # Store on a stack and retry later?
            mail_admins(
                "CID timeout",
                f"Got timeout when trying to push new {instance.__class__.__name__} {instance.pk} to CID server."
            )
        else:
            if is_success(response):
                if isinstance(instance, Client):
                    instance = instance.persona
                setattr(instance, self.external_id_field, response.json()['id'])
                instance.save(update_fields=[self.external_id_field])
            else:
                msg = f"Trying to push new {instance.__class__.__name__} {instance.pk}"
                report_error(
                    response, msg, json_sent=json_sent
                )

    def put_object(self, instance, force=False):
        """Return a dict of changed fields names if instance was successfully sent to remote server."""
        # Check if any synced fields changed
        new_data = self.build_json_data(instance)
        old_data = getattr(instance, "_original_obj_data", {})
        changed_fields = {
            key: {'old': old_data.get(key), 'new': val}
            for key, val in new_data.items() if val != old_data.get(key)
        }
        if force or changed_fields:
            # Update object to CID
            try:
                response = httpx.put(self.api_url, json=new_data, auth=BearerAuth(), timeout=self.api_timeout)
            except httpx.TimeoutException:
                # Store on a stack and retry later?
                mail_admins(
                    "CID timeout",
                    f"Got timeout when trying to update (put) {instance.__class__.__name__} {instance.pk} to CID server."
                )
                return {}
            if is_success(response):
                return changed_fields
            report_error(
                response, f"Trying to update (put) {instance.__class__.__name__} {instance.pk}",
                json_sent=new_data
            )
        return {}


class CIDClientSyncHandler(CIDSyncHandlerBase):
    model = Client
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        'genre': 'title',
        #'rue'/'c_o' dans 'street'/'street2' selon contenus
        'case_postale': 'street3',
        'npa': 'zip',
        'localite': 'city',
        'courriel': 'email',
        'date_naissance': 'birthdate',
        'date_deces': 'deathdate',
        'archive_le': 'active',
        # tel_1 and tel_2 are manually mapped to either 'phone' or 'mobile'.
    }
    required_fields = ['nom', 'prenom', 'genre', 'npa', 'localite']

    def __call__(self, sender, instance, created, **kwargs):
        self._add_adr_fields(instance.persona)
        return super().__call__(sender, instance, created, **kwargs)

    def _missing_values(self, instance):
        return super()._missing_values(instance.persona)

    @property
    def api_url(self):
        return self.api.partner_url

    @classmethod
    def build_json_data(cls, instance):
        persona = instance.persona
        cls._add_adr_fields(persona)
        persona.archive_le = instance.archive_le
        result = super().build_json_data(persona)
        result['id'] = f"c{instance.pk}"  # Keep id of client to keep compatibility with CID
        if persona.rue and persona.c_o:
            result['street'] = persona.c_o
            result['street2'] = persona.rue
        else:
            result['street'] = persona.rue or persona.c_o
            result['street2'] = ""
        result['services'] = [{
            'name': prest.get_service_display(),
            'date_start': prest.duree.lower.strftime('%Y-%m-%d') if prest.duree.lower else False,
            'date_end': prest.duree.upper.strftime('%Y-%m-%d') if prest.duree.upper else False,
        } for prest in instance.prestations.all().order_by('-duree')]
        return result


class CIDReferentSyncHandler(CIDSyncHandlerBase):
    model = Referent
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        'salutation': 'title',
        #'rue'/'complement' dans 'street'/'street2' selon contenus
        'case_postale': 'street3',
        'npa': 'zip',
        'localite': 'city',
        'courriel': 'email',
        'date_archive': 'active',
        # tel_1 and tel_2 are manually mapped to ReferentTel instances.
    }

    @property
    def api_url(self):
        return self.api.partner_url

    @classmethod
    def build_json_data(cls, instance):
        data = {
            **super().build_json_data(instance),
            'birthdate': False, 'deathdate': False, 'services': [],
        }
        data['id'] = f"r{data['id']}"
        if instance.rue and instance.complement:
            data['street'] = instance.complement
            data['street2'] = instance.rue
        else:
            data['street'] = instance.rue or instance.complement
            data['street2'] = ""
        return data

    def __call__(self, sender, instance, created, **kwargs):
        if not instance.facturation_pour:
            # Pas besoin de synchro pour les référents sans lien avec la facturation.
            return
        if instance.id_externe and Client.objects.filter(
            persona__id_externe=instance.id_externe, archive_le=None
        ).exists():
            # Si le référent est aussi Client, on laisse la priorité à la MàJ par le client.
            return
        if (
            instance.date_archive is not None and
            Referent.objects.filter(id_externe=instance.id_externe, date_archive__isnull=True).exists()
        ):
            # Si le référent est archivé, mais que d'autres instances non archivées de ce référent
            # existent, ne pas l'archiver dans CID.
            return
        super().__call__(sender, instance, created, **kwargs)

    def post_object(self, instance):
        autres_refs_meme_deb = Referent.objects.exclude(pk=instance.pk).filter(
            id_externe=instance.id_externe, id_externe__isnull=False
        )
        if instance.id_externe and autres_refs_meme_deb.exists():
            # Si ce référent existe déjà pour un autre client, pas besoin de le re-créer sur CID.
            return
        super().post_object(instance)

    def put_object(self, instance, **kwargs):
        changed = super().put_object(instance, **kwargs)
        # Év. synchroniser autres référents ayant le même no débiteur.
        other_same_refs = Referent.objects.exclude(pk=instance.pk).filter(
            id_externe=instance.id_externe, id_externe__isnull=False
        )
        if changed and other_same_refs:
            with disconnect_sync_signals():
                for ref in other_same_refs:
                    changed = False
                    for fname in self.synced_fields:
                        if fname == 'date_archive':
                            continue
                        if getattr(instance, fname) != getattr(ref, fname):
                            setattr(ref, fname, getattr(instance, fname))
                            changed = True
                    if changed:
                        ref.save()


class CIDBenevoleSyncHandler(CIDSyncHandlerBase):
    model = Benevole
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        'genre': 'gender',
        'rue': 'street',
        'npa': 'zip',
        'localite': 'city',
        'courriel': 'email',
        'date_naissance': 'birthdate',
        'archive_le': 'active',
    }
    required_fields = ['nom', 'prenom', 'npa', 'localite', 'date_naissance']

    def __call__(self, sender, instance, created, **kwargs):
        self._add_adr_fields(instance.persona)
        return super().__call__(sender, instance, created, **kwargs)

    def _missing_values(self, instance):
        return super()._missing_values(instance.persona)

    @property
    def api_url(self):
        return self.api.employees_url

    @classmethod
    def build_json_data(cls, instance):
        persona = instance.persona
        cls._add_adr_fields(persona)
        persona.archive_le = instance.archive_le
        result = super().build_json_data(persona)
        result.update({
            "id": instance.pk,
            "services": [],
        })
        if instance.id_externe:
            result["external_id"] = instance.id_externe
        for act in instance.activite_set.all():
            result["services"].append({
                'name': act.service,
                'date_start': act.duree.lower.strftime('%Y-%m-%d'),
                'date_end': act.duree.upper.strftime('%Y-%m-%d') if act.duree.upper else False,
            })
        return result

    def post_object(self, instance):
        super().post_object(instance)
        if settings.MUTATIONS_BENEVOLES_EMAIL:
            adr = instance.adresse()
            send_mail(
                "[Croix-Rouge NE] Nouveau bénévole",
                "Un nouveau bénévole a été créé dans le logiciel MAD:\n"
                f"Nom: {instance.nom}\n"
                f"Prénom: {instance.prenom}\n"
                f"Adresse: {adr.rue}, {adr.npa} {adr.localite}\n"
                f"Date de naissance: {instance.date_naissance.strftime('%d.%m.%Y')}\n"
                f"Service(s): {', '.join(act.service for act in instance.activite_set.all())}\n"
                "\n--\n"
                "Ceci est un message automatique, ne pas répondre",
                None, settings.MUTATIONS_BENEVOLES_EMAIL,
            )

    def put_object(self, instance, **kwargs):
        changed_fields = super().put_object(instance, **kwargs)
        monitored_fields = ['name', 'firstname', 'street', 'zip', 'city', 'services']
        if target_fields := set(changed_fields.keys()).intersection(monitored_fields):
            msg = (
                f"Les modifications suivantes ont été apportées au bénévole {instance.nom_prenom} "
                "dans le logiciel MAD:\n"
            )
            synced_rev = {v: k for k, v in self.synced_fields.items()}
            for key in sorted(target_fields, key=lambda f: monitored_fields.index(f)):
                if key == "services":
                    old_val = ", ".join(
                        f"{serv['name']} ({serv['date_start']} - {serv['date_end'] or ''})"
                        for serv in changed_fields[key]["old"]
                    )
                    new_val = ", ".join(
                        f"{serv['name']} ({serv['date_start']} - {serv['date_end'] or ''})"
                        for serv in changed_fields[key]["new"]
                    )
                    key_str = "Activités"
                else:
                    fld = synced_rev[key]
                    old_val = changed_fields[key]["old"]
                    new_val = getattr(instance, fld)
                    key_str = fld.capitalize()
                msg += f" * {key_str}: «{old_val}» => «{new_val}»\n"
            msg += "\n--\nCeci est un message automatique, ne pas répondre"
            send_mail(
                "[Croix-Rouge NE] Modification de bénévole",
                msg, None, settings.MUTATIONS_BENEVOLES_EMAIL,
            )
        return changed_fields


client_sync_receiver = CIDClientSyncHandler()
referent_sync_receiver = CIDReferentSyncHandler()
benevole_sync_receiver = CIDBenevoleSyncHandler()


@contextmanager
def connect_sync_signals(*args, **kwargs):
    """Connexion temporaire des signaux de synchonisation."""
    pre_save.connect(client_sync_receiver.cache_previous_instance, sender=Client)
    client_saved.connect(client_sync_receiver, sender=Client)
    pre_save.connect(referent_sync_receiver.cache_previous_instance, sender=Referent)
    referent_saved.connect(referent_sync_receiver, sender=Referent)
    pre_save.connect(benevole_sync_receiver.cache_previous_instance, sender=Benevole)
    benevole_saved.connect(benevole_sync_receiver, sender=Benevole)
    try:
        yield
    finally:
        pre_save.disconnect(client_sync_receiver.cache_previous_instance, sender=Client)
        client_saved.disconnect(client_sync_receiver, sender=Client)
        pre_save.disconnect(referent_sync_receiver.cache_previous_instance, sender=Referent)
        referent_saved.disconnect(referent_sync_receiver, sender=Referent)
        pre_save.disconnect(benevole_sync_receiver.cache_previous_instance, sender=Benevole)
        benevole_saved.disconnect(benevole_sync_receiver, sender=Benevole)


@contextmanager
def disconnect_sync_signals(*args, **kwargs):
    """Déconnexion temporaire des signaux de synchonisation."""
    pre_save_dcc = pre_save.disconnect(client_sync_receiver.cache_previous_instance, sender=Client)
    client_saved_dcc = client_saved.disconnect(client_sync_receiver, sender=Client)
    pre_save_dcr = pre_save.disconnect(referent_sync_receiver.cache_previous_instance, sender=Referent)
    referent_saved_dcr = referent_saved.disconnect(referent_sync_receiver, sender=Referent)
    pre_save_dcb = pre_save.disconnect(benevole_sync_receiver.cache_previous_instance, sender=Benevole)
    benevole_saved_dcb = benevole_saved.disconnect(benevole_sync_receiver, sender=Benevole)
    try:
        yield
    finally:
        if pre_save_dcc:
            pre_save.connect(client_sync_receiver.cache_previous_instance, sender=Client)
        if client_saved_dcc:
            client_saved.connect(client_sync_receiver, sender=Client)
        if pre_save_dcr:
            pre_save.connect(referent_sync_receiver.cache_previous_instance, sender=Referent)
        if referent_saved_dcr:
            referent_saved.connect(referent_sync_receiver, sender=Referent)
        if pre_save_dcb:
            pre_save.connect(benevole_sync_receiver.cache_previous_instance, sender=Benevole)
        if benevole_saved_dcb:
            benevole_saved.connect(benevole_sync_receiver, sender=Benevole)
