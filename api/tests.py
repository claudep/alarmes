from datetime import date, timedelta
from unittest.mock import ANY, patch

from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import Permission
from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse

from client.forms import ClientEditFormBase, ReferentForm
from client.models import Client, Persona, Referent, ReferentTel, Telephone
from common.choices import Services
from common.models import Utilisateur
from common.test_utils import BaseDataMixin, mocked_httpx
from common.utils import current_app
from benevole.forms import BenevoleForm
from benevole.models import Benevole, TypeActivite
from api import signals

api_timeout = signals.CIDSyncHandlerBase.api_timeout


class ClientForm(ClientEditFormBase):
    persona_fields = [
        'nom', 'prenom', 'genre', 'date_naissance', 'case_postale', 'courriel',
        'langues', 'date_deces',
    ]


@override_settings(
    API_ALLOWED_IPS=["127.0.0.1"], API_ALLOWED_TOKENS=["abcdefghij1234"],
    ADMINS=[('admin', 'test@example.org')]
)
class APIServerTests(BaseDataMixin, TestCase):
    """
    Tests des API de synchronisation en provenance de l'app. externe.
    """
    persona_data = {
        "id": 22, # ID logiciel tiers
        "title": "Sir",  # Valeurs possibles : Sir, Madam, Madame et Monsieur
        "name": "Dupont",
        "firstname": "Jean",
        "street": "Rue de la Poste 1",
        "street2": "",
        "street3": "",
        "zip": "1000",
        "city": "Lausanne",
        "country": "",
        "phone": "021 211 21 21",
        "mobile": "079 211 21 21",
        "email": "jean@dupont.com",
        "birthdate": "1942-12-31",
        "deathdate": "",
    }

    def test_persona_create(self):
        post_data = self.persona_data
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            self.assertEqual(response.status_code, 200)
            json_resp = response.json()
            self.assertIn('id', json_resp)
            mocked.assert_called_once()  # geolocation

        persona = Persona.objects.get(nom="Dupont")
        self.assertEqual(json_resp['id'], f'p{persona.pk}')
        self.assertEqual(persona.id_externe, 22)
        self.assertEqual(persona.date_naissance, date(1942, 12, 31))
        self.assertIsNone(persona.date_deces)
        self.assertEqual(persona.adresse().empl_geo, [1.0, 2.0])
        self.assertEqual([t.tel for t in persona.telephones()], ["021 211 21 21", "079 211 21 21"])
        journal = persona.journaux.latest()
        self.assertEqual(journal.description, "Création de la personne par CID")
        self.assertEqual(journal.qui, None)
        # Pas de création client auto depuis CID.
        self.assertIsNone(persona.get_client())
        # Même requête une seconde fois, doublon détecté (non sensible à la casse)
        post_data = {**post_data, 'id': 24, 'name': post_data['name'].upper()}
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {'error':
                {'code': 409, 'doublon_id': persona.pk, 'message': 'Cette personne semble être un doublon.'}
            }
        )
        # Pas de doublon détecté si prénom différent
        post_data = {**post_data, 'id': 25, 'firstname': "Eric"}
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn('id', response.json())
            mocked.assert_called_once()

    def test_persona_create_no_birthdate(self):
        post_data = {**self.persona_data, "birthdate": False}
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.post(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(response.status_code, 200)
        persona = Persona.objects.get(nom="Dupont")
        self.assertIsNone(persona.date_naissance)

    def test_persona_create_street_street2(self):
        """
        Si street et street2 sont remplis, on suppose que c_o est dans street
        et rue dans street2.
        """
        post_data = {**self.persona_data, "street": "Résidence Helvétie", "street2": "Rue des Clés 3"}
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            self.assertEqual(response.status_code, 200)
            json_resp = response.json()
            self.assertIn('id', json_resp)
            mocked.assert_called_once()  # geolocation

        persona = Persona.objects.get(nom="Dupont")
        adr = persona.adresse()
        self.assertEqual(adr.c_o, "Résidence Helvétie")
        self.assertEqual(adr.rue, "Rue des Clés 3")

    def test_persona_create_error(self):
        post_data = {**self.persona_data, "city": ''}
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(), {
                'error': {
                    'code': 400,
                    'message': 'Sorry, the data are not valid.',
                    'errors': {'localite': [{'message': 'Ce champ est obligatoire.', 'code': 'required'}]}
                }
            }
        )

    def test_persona_update(self):
        persona = Persona.objects.create(
            id_externe=22, nom="Dupond", prenom="Jean",
            rue="Rue du Stand 3", npa="1000", localite="Lausanne", valide_des="2024-01-01",
            langues=["fr", "de"],
        )
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.put(
                '/api/partners', self.persona_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json(), {'id': f'p{persona.pk}'})
            mocked.assert_called_once()  # Rue modifiée va appeler géoloc.
        persona.refresh_from_db()
        self.assertEqual(persona.courriel, "jean@dupont.com")

    def test_client_update(self):
        client = Client.objects.create(
            id_externe=22, service=[Services.ALARME, Services.TRANSPORT],
            nom="Dupond", prenom="Jean",
            rue="Rue du Stand 3", npa="1000", localite="Lausanne", valide_des="2024-01-01",
            remarques_int_alarme="Remarque interne",
            langues=["fr", "de"],
            tel_1="079 211 21 21",
            tel_2="021 211 21 21",
        )
        post_data = {
            **self.persona_data,
            "id": client.id_externe, "name": "DUPOND", "firstname": "Jeannot",
            "street3": "Case postale 16", "phone": "021 111 00 21",
        }
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.put(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json(), {'id': f'p{client.persona_id}'})
            mocked.assert_called_once()  # Rue modifiée va appeler géoloc.
        client.refresh_from_db()
        persona = client.persona
        self.assertEqual(persona.nom, "Dupond")
        self.assertEqual(persona.date_naissance, date(1942, 12, 31))
        self.assertEqual(persona.case_postale, "Case postale 16")
        self.assertEqual(persona.langues, ["fr", "de"])
        self.assertEqual([t.tel for t in persona.telephones()], ["079 211 21 21", "021 111 00 21"])
        adr_old = client.adresse(date.today() - timedelta(days=1))
        adr_new = client.adresse(date.today())
        self.assertEqual(adr_old.rue, "Rue du Stand 3")
        self.assertEqual(adr_new.rue, "Rue de la Poste 1")
        # Champ existant et non synchronisé intact
        self.assertEqual(client.remarques_int_alarme, "Remarque interne")
        journal = client.journaux.latest()
        self.maxDiff = None
        self.assertEqual(
            journal.description,
            "Modification du client par CID: prénom (de «Jean» à «Jeannot»), ajout de genre («M»), "
            "ajout de case postale («Case postale 16»), ajout de courriel («jean@dupont.com»), "
            "ajout de date de naissance («1942-12-31»), rue (de «Rue du Stand 3» à «Rue de la Poste 1»), "
            "suppression de téléphone (N° 021 211 21 21), "
            "téléphone: ajout de numéro («021 111 00 21»)"
        )
        self.assertEqual(journal.qui, None)
        self.assertEqual(client.alertes.count(), 2)
        self.assertTrue(client.alertes.all()[0].alerte.startswith('Modifications depuis l’ERP: prénom'))
        self.assertEqual(set([al.cible for al in client.alertes.all()]), {'alarme', 'transport'})

        post_data.update({
            "deathdate": "2023-05-20",  # Date de décès
            "street": "rue de la poste 1",  # Différence de casse pas prise en compte
            "mobile": "",  # Suppr. no de tél.
        })
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.put(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(response.status_code, 200)
        client.refresh_from_db()
        journal = client.journaux.latest()
        self.assertEqual(
            journal.description,
            "Modification du client par CID: ajout de décès le («2023-05-20»), "
            "suppression de téléphone (N° 079 211 21 21)"
        )
        self.assertEqual(client.adresse().rue, "Rue de la Poste 1")
        self.assertEqual([t.tel for t in client.telephones()], ["021 111 00 21"])
        self.assertEqual(client.date_deces, date(2023, 5, 20))
        self.assertEqual(
            client.alertes.latest().alerte,
            "Cette personne a été marquée comme décédée depuis l’ERP"
        )

    def test_client_update_doutes(self):
        """
        Il ne devrait pas arriver que nom, prenom et numéro postal changent en même temps.
        """
        cl = Client.objects.create(
            id_externe=22, nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
        )
        post_data = {
            "id": 22,
            "title": "",
            "name": "Smith",
            "firstname": "Joe",
            "street": "Rue de la Poste 2",
            "street2": "1",
            "street3": "",
            "zip": "2300",
            "city": "La Chaux-de-Fonds",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "",
            "email": "",
            "birthdate": "",
            "deathdate": "",
        }
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.put(
                '/api/partners', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(
            response.json()['error']['errors']['__all__'][0]['message'],
            "La modification simultanée du nom, prénom et numéro postal est douteuse"
        )

    def test_referent_create(self):
        # Seulement si pas de prénom
        post_data = {
            "id": 44, # ID logiciel tiers
            "title": "",
            "name": "Cinq Sàrl",
            "firstname": "",
            "street": "Bureau 321",
            "street2": "Rue de la Poste 2",
            "street3": "",
            "zip": "1000",
            "city": "Lausanne",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "",
            "email": "suva@example.org",
            "birthdate": "",
            "deathdate": "",
        }
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        json_resp = response.json()
        self.assertIn('id', json_resp)
        referent = Referent.objects.get(nom="Cinq Sàrl")
        self.assertEqual(json_resp['id'], f'r{referent.pk}')
        self.assertEqual(referent.salutation, "E")
        # Quand street et street2 sont remplis, on suppose que complement est dans street
        # et rue dans street2.
        self.assertEqual(referent.complement, "Bureau 321")
        self.assertEqual(referent.rue, "Rue de la Poste 2")
        self.assertEqual(referent.id_externe, 44)
        self.assertEqual([t.tel for t in referent.telephones()], ["021 211 21 21"])

    def test_referent_update(self):
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        referent0 = Referent.objects.create(
            client=cl0, nom='Dupont', prenom='Jeannot', rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            id_externe=22, facturation_pour=['al-abo', 'al-tout', 'transp'],
        )
        ReferentTel.objects.create(referent=referent0, tel="031 111 00 00", priorite=1)
        referent1 = Referent.objects.create(
            client=cl1, nom='Dupont', prenom='Jeannot', rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            id_externe=22, facturation_pour=['al-abo', 'al-tout', 'transp'],
        )
        ReferentTel.objects.create(referent=referent1, tel="031 111 00 00", priorite=1)
        post_data = {
            **self.persona_data,
            "id": 22, "name": "DUPONT", "title": "Madame et Monsieur", "street3": "Case postale 16",
        }
        response = self.client.put(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        referent0.refresh_from_db()
        self.assertEqual(referent0.nom, "Dupont")
        self.assertEqual(referent0.salutation, "P")
        referent1.refresh_from_db()
        self.assertEqual(referent1.nom, "Dupont")
        self.assertEqual([t.tel for t in referent1.telephones()], ["021 211 21 21", "079 211 21 21"])
        journal = cl0.journaux.latest()
        self.maxDiff = None
        self.assertEqual(
            journal.description,
            "Modification du contact «Jean Dupont, adresse de fact. pour Doe John» par CID: "
            "ajout de salutation («P»), prénom (de «Jeannot» à «Jean»), "
            "rue (de «Rue du Stand 3» à «Rue de la Poste 1»), ajout de case postale («Case postale 16»), "
            "npa (de «2000» à «1000»), localité (de «Neuchâtel» à «Lausanne»), "
            "ajout de courriel («jean@dupont.com»), suppression de téléphone de référent "
            "(N° 031 111 00 00), téléphone de référent: ajout de numéro («021 211 21 21»), "
            "téléphone de référent: ajout de numéro («079 211 21 21»)"
        )

    def test_referent_sans_client_update(self):
        referent = Referent.objects.create(
            client=None, nom='Dupont', prenom='Jeannot',
            rue="Rue de la Poste 1", npa="1000", localite="Lausanne",
            id_externe=22,
        )
        response = self.client.put(
            '/api/partners', self.persona_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        referent.refresh_from_db()
        self.assertEqual(referent.courriel, "jean@dupont.com")

    def test_benevole_create(self):
        TypeActivite.objects.create(code='boutiques', nom='Boutiques', service=''),
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "NE",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1972-12-31",
            #"tag" => Liste valeurs possibles?
            "account": "CH41...",
            "services": [
                {'name': 'Transports', 'date_start': '2022-10-23', 'date_end': False},
                {'name': 'Boutiques', 'date_start': '2023-10-01', 'date_end': False},
                {'name': 'Visites', 'date_start': '2023-10-01', 'date_end': False},
                {'name': 'AMF', 'date_start': '2023-10-01', 'date_end': '2024-02-25'},
                {'name': 'INCONNU', 'date_start': '2023-10-01', 'date_end': False},
            ],
        }
        # Avec erreur de formulaire
        with self.assertLogs('api', level='ERROR') as cm:
            response = self.client.post(
                '/api/employees', {**post_data, "phone": "1234"}, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(
            cm.output,
            ['ERROR:api:Persona form errors: * tel\n  * Le numéro de téléphone n’est pas valide']
        )
        self.maxDiff = None
        self.assertEqual(
            response.json(),
            {'error': {
                'code': 400, 'message': "Sorry, the data are not valid.",
                'errors': {'tel': [{'message': 'Le numéro de téléphone n’est pas valide', 'code': ''}]},
            }}
        )
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.post(
                '/api/employees', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            mocked.assert_called_once()
        self.assertEqual(response.status_code, 200)
        benev = Benevole.objects.get(persona__nom="Dupont")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].body, f"Activité INCONNU inconnue pour le bénévole {benev.pk}.")
        log = benev.journaux.latest()
        self.assertEqual(log.description, "Création de la personne par CID")
        self.assertEqual(response.json()['id'], benev.pk)
        self.assertEqual(benev.id_externe, 22)
        self.assertIsNone(benev.persona.id_externe)
        self.assertEqual(benev.genre, 'M')
        self.assertEqual(benev.date_naissance, date(1972, 12, 31))
        self.assertEqual(benev.adresse().empl_geo, [1.0, 2.0])
        self.assertEqual(
            set([act.type_act.nom for act in benev.activites_en_cours()]),
            set(['Transports', 'Boutiques', 'Visites', 'AMF'])
        )
        # Même requête une seconde fois, doublon détecté
        response = self.client.post(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {'error':
                {'code': 409, 'doublon_id': benev.persona.pk, 'message': 'Cette personne semble être un doublon.'}
            }
        )

    def test_benevole_update(self):
        benev = Benevole.objects.create(
            id_externe=22,
            persona=Persona.objects.create(
                nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel"
            ),
            activites=[TypeActivite.TRANSPORT]
        )
        benev.activite_set.update(duree=("2023-01-10", None))
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1942-12-31",
            "services": [
                {"name": "Transports", "date_start": "2023-01-10", "date_end": False},
                {"name": "Alarme", "date_start": "2023-10-31", "date_end": False},
                {"name": "AMF", "date_start": "2023-10-31", "date_end": False},
                {"name": "NOUVEAU", "date_start": "2023-10-31", "date_end": False},
            ],
        }
        with patch('httpx.get', side_effect=mocked_httpx) as mocked:
            response = self.client.put(
                '/api/employees', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
            mocked.assert_called_once()  # geolocation
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"id": benev.pk})
        benev.refresh_from_db()
        log = benev.journaux.latest()
        self.assertTrue(log.description.startswith("Modification du bénévole par CID:"))
        self.assertIn("ajout de courriel («jean@dupont.com»)", log.description)
        self.assertIsNone(log.qui)
        self.assertEqual(benev.nom, "Dupont")
        self.assertEqual(benev.courriel, "jean@dupont.com")
        self.assertEqual(benev.date_naissance, date(1942, 12, 31))
        self.assertEqual([t.tel for t in benev.telephones()], ["021 211 21 21", "079 211 21 21"])
        self.assertEqual(benev.adresse().empl_geo, [1.0, 2.0])
        self.assertEqual(benev.activites_en_cours().count(), 4)
        # Mise à jour date fin service existant
        post_data["services"][2]["date_end"] = "2023-11-30"
        response = self.client.put(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        benev.refresh_from_db()
        self.assertEqual(benev.activites_en_cours().count(), 3)

    def test_benevole_update_non_existant(self):
        """
        Si une requête de modification survient pour un bénévole non existant,
        ce bénévole est créé.
        """
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1942-12-31",
        }
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.put(
                '/api/employees', post_data, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(response.status_code, 200)
        benev = Benevole.objects.get(persona__nom="Dupont")
        self.assertEqual(benev.id_externe, 22)


class APIClientTests(BaseDataMixin, TestCase):
    """
    Test des API de synchronisation vers CID-ERP.
    """
    def setUp(self):
        """Patching httpx.put and httpx.post for all tests."""
        super().setUp()
        patcher_post = patch('api.signals.httpx.post')
        patcher_put = patch('api.signals.httpx.put')
        self.mock_post = patcher_post.start()
        self.mock_put = patcher_put.start()
        self.addCleanup(patcher_post.stop)
        self.addCleanup(patcher_put.stop)

    @override_settings(ADMINS=[('admin', 'test@example.org')])
    def test_client_creation_error(self):
        self.mock_post.return_value.status_code = 400
        self.mock_post.return_value.json.return_value = {
            "error": {"code": 400, "message": "Field \"street\" is required in JSON dict and must have a value"}
        }
        self.mock_post.return_value.ok = False
        client_data = dict(
            nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3),
        ) | {"telephone_set-TOTAL_FORMS": 0, "telephone_set-INITIAL_FORMS": 0}
        form = ClientForm(data=client_data)
        self.assertTrue(form.is_valid(), form.errors)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            cl = form.save()
        self.assertEqual(len(mail.outbox), 1)
        self.maxDiff = None
        self.assertEqual(
            mail.outbox[0].body,
            f"Trying to push new Client {cl.pk}, got response with status code 400 from CID server. "
            "Error details: "
            "{'code': 400, 'message': 'Field \"street\" is required in JSON dict and must have a value'}\n"
            f'JSON payload: {{\n  "id": "c{cl.pk}",\n  "services": [],\n  '
            '"name": "Doe",\n  "firstname": "John",\n  "title": "Sir",\n  '
            '"street3": "",\n  "zip": "2000",\n  "city": "Neuch\\u00e2tel",\n  "email": "",\n  '
            '"birthdate": "1950-04-03",\n  "deathdate": false,\n  "active": true,\n  '
            '"phone": "",\n  "mobile": "",\n  "country": "NE",\n  "street": "",\n  "street2": ""\n}'
        )

    def _test_client_creation(self, client_data, expected_json):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        client_data.update({"telephone_set-TOTAL_FORMS": 0, "telephone_set-INITIAL_FORMS": 0})
        if "tel_1" in client_data:
            client_data.update({
                "telephone_set-TOTAL_FORMS": 1,
                "telephone_set-0-tel": client_data.pop("tel_1"),
            })
        form = ClientForm(data=client_data)
        self.assertTrue(form.is_valid(), form.errors)
        with (
            signals.connect_sync_signals(),
            self.captureOnCommitCallbacks(execute=True),
            patch('httpx.get', side_effect=mocked_httpx)
        ):
            client = form.save()
        expected_json['id'] = f"c{client.pk}"
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json=expected_json,
            auth=ANY,
            timeout=api_timeout,
        )
        # refresh_from_db() does not clear non-field values
        self.assertEqual(Client.objects.get(pk=client.pk).id_externe, 6523)

    def test_client_creation(self):
        client_data = dict(
            nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            case_postale="C.P. 1001", date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
        )
        self._test_client_creation(client_data, expected_json={
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'street2': '', 'street3': 'C.P. 1001',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44', 'active': True,
                'services': [],
            },
        )

    def test_client_creation_avec_co(self):
        client_data = dict(
            nom="Doe", prenom="John", genre='M',
            c_o="Résidence Helvétie", rue="Rue des Clés 4", npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3),
        )
        self._test_client_creation(client_data, expected_json={
            'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
            'street': 'Résidence Helvétie', 'street2': 'Rue des Clés 4', 'street3': '',
            'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
            'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
            'phone': '', 'mobile': '', 'active': True,
            'services': [],
        })

    def test_client_transport_creation(self):
        if not apps.is_installed('transport'):
            self.skipTest("Only testable with transport app")
        superuser = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        self.client.force_login(superuser)
        with signals.connect_sync_signals(), patch('httpx.get', side_effect=mocked_httpx):
            self.mock_post.return_value.json.return_value = {'id': 6523}
            with self.captureOnCommitCallbacks(execute=True):
                response = self.client.post(reverse('client-new'), data={
                    'nom': 'Donzé', 'prenom': 'Léa', 'genre': 'F',
                    'rue': 'Rue des Crêtets 92', 'npalocalite': '2300 La Chaux-de-Fonds',
                    'date_naissance': '1952-10-23',
                    "telephone_set-TOTAL_FORMS": 0, "telephone_set-INITIAL_FORMS": 0,
                })
                if response.status_code == 200:
                    self.fail(response.context['form'].errors.as_text())
        cl = Client.objects.get(persona__nom='Donzé')
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f'c{cl.pk}', 'name': 'Donzé', 'firstname': 'Léa', 'title': 'Madam',
                'street': 'Rue des Crêtets 92', 'street2': '', 'street3': '', 'zip': '2300',
                'city': 'La Chaux-de-Fonds', 'email': '', 'birthdate': '1952-10-23',
                'deathdate': False, 'active': True, 'phone': '',
                'mobile': '', 'country': 'NE',
                'services': [{
                    'name': 'Transports', 'date_start': date.today().strftime('%Y-%m-%d'), 'date_end': False
                }],
            },
            auth=ANY,
            timeout=api_timeout,
        )

    def test_client_creation_missing_values(self):
        client_data = dict(
            nom="Doe", prenom="John", genre='', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3)
        ) | {"telephone_set-TOTAL_FORMS": 0, "telephone_set-INITIAL_FORMS": 0}
        form = ClientForm(data=client_data)
        self.assertTrue(form.is_valid(), form.errors)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            client = form.save()
        self.mock_post.assert_not_called()

    def test_client_update(self):
        client_data = dict(
            id_externe=6523, nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
        )
        cl = Client.objects.create(**client_data)
        client_data.update({
            "telephone_set-TOTAL_FORMS": 2, "telephone_set-INITIAL_FORMS": 1,
            "telephone_set-0-id": cl.telephones()[0].pk,
            "telephone_set-0-persona": cl.persona.pk,
            "telephone_set-0-tel": client_data["tel_1"],
            "telephone_set-0-ORDER": 1,
            "telephone_set-1-persona": cl.persona.pk,
            "telephone_set-1-tel": "031 310 31 31",
            "telephone_set-1-ORDER": 2,
        })

        form = ClientForm(data=client_data, instance=cl)
        self.assertTrue(form.is_valid(), form.errors)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form.save()
        expected_json = {
            'id': f'c{cl.pk}', 'external_id': 6523,
            'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
            'street': '', 'street2': '', 'street3': '',
            'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
            'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
            'phone': '031 310 31 31', 'mobile': '078 444 44 44',
            'active': True, 'services': [],
        }
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json=expected_json,
            auth=ANY,
            timeout=api_timeout,
        )

        def test_with_tels(no1, no2):
            client_data.update({
                "telephone_set-INITIAL_FORMS": 2,
                "telephone_set-0-id": cl.telephones()[0].pk,
                "telephone_set-0-tel": no1,
                "telephone_set-1-id": cl.telephones()[1].pk,
                "telephone_set-1-tel": no2,
            })
            expected_json.update({"phone": no1, "mobile": no2})
            form = ClientForm(data=client_data, instance=cl)
            self.assertTrue(form.is_valid(), form.errors)
            with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
                form.save()
            self.mock_put.assert_called_with(
                f'{settings.CID_API_URL}partners',
                json=expected_json,
                auth=ANY,
                timeout=api_timeout,
            )
        # Deux mobiles
        test_with_tels("078 444 44 44", "076 111 00 11")
        # Deux fixes
        test_with_tels("032 444 44 44", "058 111 00 11")

    def test_client_update_rue(self):
        """La géolocalisation ne doit pas interférer avecla synchro."""
        client_data = dict(
            id_externe=6523, nom="Doe", prenom="John", genre='M', npa="2034", localite="Peseux",
            date_naissance=date(1950, 4, 3), valide_des='2023-01-01',
        )
        cl = Client.objects.create(**client_data)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            superuser = Utilisateur.objects.create_superuser('admin@example.org', password='test')
            self.client.force_login(superuser)
            with patch('httpx.get', side_effect=mocked_httpx) as mocked:
                response = self.client.post(
                    reverse('persona-adresse-princ-edit', args=[cl.adresse().pk]),
                    data={'des_le': date.today(), 'rue': 'Rue du Stand 24', 'npalocalite': "2000 Neuchâtel"}
                )
                self.assertEqual(response.json()['result'], 'OK')
        expected_json = {
            'id': f'c{cl.pk}', 'external_id': 6523,
            'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
            'street': 'Rue du Stand 24', 'street2': '', 'street3': '',
            'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
            'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
            'phone': '031 310 31 31', 'mobile': '',
            'active': True, 'services': ANY,#[],
        }
        self.mock_put.assert_called_once()
        self.assertEqual(self.mock_put.call_args.kwargs['json']['street'], 'Rue du Stand 24')

    def test_client_update_admin(self):
        client_data = dict(
            id_externe=6523, nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_1="078 444 44 44", service=[Services.ALARME],
        )
        cl = Client.objects.create(**client_data)
        superuser = Utilisateur.objects.create_superuser("admin@example.org", password="test")
        self.client.force_login(superuser)
        with signals.connect_sync_signals():
            response = self.client.post(
                reverse("admin:client_client_change", args=[cl.pk]),
                data={
                    **client_data,
                    'persona': cl.persona.pk,
                    'prestations-TOTAL_FORMS': '1',
                    'prestations-INITIAL_FORMS': '1',
                    'prestations-0-id': cl.prestations.first().pk,
                    'prestations-0-client': cl.pk,
                    'prestations-0-service': "alarme",
                    'prestations-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
                    'prestations-0-duree_0': '31.10.2023',
                    'prestations-0-duree_1': '',
                },
            )
            self.assertEqual(response.status_code, 302)
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"c{cl.pk}", 'external_id': 6523,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'street2': '', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44', 'active': True,
                'services': [{'name': 'Alarme', 'date_start': '2023-10-31', 'date_end': False}]
            },
            auth=ANY,
            timeout=api_timeout,
        )

    def test_client_archiver_desarchiver(self):
        client_data = dict(
            id_externe=6523, nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_1='078 444 44 44',
            service=[Services.ALARME, Services.TRANSPORT],
        )
        client = Client.objects.create(**client_data)
        client.prestations.filter(service=Services.ALARME).update(
            duree=(date.today() - timedelta(days=60), None)
        )

        # Commencer par archiver une prestation et pas l'autre
        with signals.connect_sync_signals():
            client.archiver(None, 'transport', check=False)
        prest_al, prest_trans = client.prestations.all().order_by('pk')
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"c{client.pk}", 'external_id': 6523,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'street2': '', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44',
                'active': True,
                'services': [
                    {'name': 'Transports',
                     'date_start': prest_trans.duree.lower.strftime('%Y-%m-%d'),
                     'date_end': prest_trans.duree.upper.strftime('%Y-%m-%d'),
                    },
                    {'name': 'Alarme',
                     'date_start': prest_al.duree.lower.strftime('%Y-%m-%d'),
                     'date_end': False,
                    },
                ],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        self.assertIsNone(client.archive_le)
        self.mock_put.reset_mock()

        prest_al.duree=(prest_al.duree.lower, prest_trans.duree.upper)
        prest_al.save()
        with signals.connect_sync_signals():
            client._archiver_fort(None)
        prest_al.refresh_from_db()
        prest_trans.refresh_from_db()
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"c{client.pk}", 'external_id': 6523,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'street2': '', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44',
                'active': False,
                'services': [
                    {'name': 'Transports',
                     'date_start': prest_trans.duree.lower.strftime('%Y-%m-%d'),
                     'date_end': prest_trans.duree.upper.strftime('%Y-%m-%d'),
                    },
                    {'name': 'Alarme',
                     'date_start': prest_al.duree.lower.strftime('%Y-%m-%d'),
                     'date_end': prest_al.duree.upper.strftime('%Y-%m-%d'),
                    },
                ],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        self.assertEqual(client.archive_le, date.today())

        # Désarchivage
        with signals.connect_sync_signals():
            client.desarchiver(None, 'alarme')
        self.assertEqual(client.prestations.count(), 3)
        self.mock_put.assert_called_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"c{client.pk}", 'external_id': 6523,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'street2': '', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44',
                'active': True,
                'services': [{
                    'name': 'Alarme',
                    'date_start': date.today().strftime('%Y-%m-%d'), 'date_end': False
                }, {
                    'name': 'Transports',
                    'date_start': prest_trans.duree.lower.strftime('%Y-%m-%d'),
                    'date_end': prest_trans.duree.upper.strftime('%Y-%m-%d'),
                }, {
                    'name': 'Alarme',
                    'date_start': prest_al.duree.lower.strftime('%Y-%m-%d'),
                    'date_end': prest_al.duree.upper.strftime('%Y-%m-%d'),
                }],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        self.assertIsNone(client.archive_le)

    def test_referent_creation(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        # Test sans facturation_pour
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form_data = {
                'salutation': 'M',
                'nom': 'Schmid',
                'prenom': 'Marco',
                'npalocalite': "2000 Neuchâtel",
                'referenttel_set-INITIAL_FORMS': 0,
                'referenttel_set-TOTAL_FORMS': 1,
                'referenttel_set-0-tel': '033 000 33 00',
            }
            form = ReferentForm(client=cl0, instance=None, data=form_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            form.save()
        self.mock_post.assert_not_called()

        # Test avec facturation_pour
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form_data.update({
                'facturation_pour': ['al-tout'],
                'rue': "Rue des Fleurs 4",
                'case_postale': "C.P. 124",
            })
            form = ReferentForm(client=cl0, instance=None, data=form_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            referent = form.save()
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"r{referent.pk}",
                'name': 'Schmid', 'firstname': 'Marco', 'title': 'Sir',
                'street': 'Rue des Fleurs 4', 'street2': '', 'street3': 'C.P. 124',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'phone': '033 000 33 00', 'mobile': '',
                'active': True, 'birthdate': False, 'deathdate': False, 'services': [],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        referent.refresh_from_db()
        self.assertEqual(referent.id_externe, 6523)
        # Si le même référent est ajouté pour un autre client, pas de post.
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            referent = Referent.objects.create(
                client=cl1, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
                facturation_pour=['al-tout'],
            )
        self.mock_post.assert_called_once()

    def test_referent_creation_avec_complement(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        referent_data = {
            'salutation': 'E',
            'nom': 'Assurance X',
            'prenom': '',
            'complement': 'Secrétariat principal',
            'rue': "Rue des Fleurs 4",
            'case_postale': "C.P. 124",
            'npalocalite': "2000 Neuchâtel",
            'facturation_pour': ['al-tout'],
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 1,
            'referenttel_set-0-tel': '033 000 33 00',
        }
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = ReferentForm(client=cl0, instance=None, data=referent_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            referent = form.save()
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"r{referent.pk}",
                'name': 'Assurance X', 'firstname': '', 'title': '',
                'street': 'Secrétariat principal', 'street2': 'Rue des Fleurs 4', 'street3': 'C.P. 124',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'phone': '033 000 33 00', 'mobile': '',
                'active': True, 'birthdate': False, 'deathdate': False, 'services': [],
            },
            auth=ANY,
            timeout=api_timeout,
        )

    def test_referent_update(self):
        self.mock_post.return_value.json.return_value = {'id': 6523}
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        referent = Referent.objects.create(
            client=cl0, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
        )
        # Test referent ajout facturation_pour
        form_data = {f: getattr(referent, f) for f in ['nom', 'prenom']}
        form_data.update({
            'rue': "Rue des Fleurs 4",
            'npalocalite': "2000 Neuchâtel",
            'complement': "c/o Secteur comptable",
            'facturation_pour': ['al-tout'],
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 1,
            'referenttel_set-0-tel': '078 777 77 77',
        })
        form = ReferentForm(client=cl0, instance=referent, data=form_data, initial={})
        self.assertTrue(form.is_valid(), form.errors)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form.save()
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"r{referent.pk}",
                'name': 'Schmid', 'firstname': 'Marco', 'title': '',
                'street': 'c/o Secteur comptable', 'street2': 'Rue des Fleurs 4', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'phone': '', 'mobile': '078 777 77 77',
                'active': True, 'birthdate': False, 'deathdate': False, 'services': [],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        referent.refresh_from_db()
        self.assertEqual(referent.id_externe, 6523)

        # Autre référent avec même id_externe: test modification adresse
        referent_cl1 = Referent.objects.create(
            client=cl1, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
            id_externe=6523, facturation_pour=['al-tout'],
        )
        form_data = {f: getattr(referent, f) for f in ['id_externe', 'nom', 'prenom', 'facturation_pour']}
        form_data.update({
            'npalocalite': "2000 Neuchâtel",
            'rue': "Rue des Terreaux 14",
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 0,
        })
        form = ReferentForm(client=cl1, instance=referent_cl1, data=form_data, initial={})
        self.assertTrue(form.is_valid())
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form.save()
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"r{referent_cl1.pk}", 'external_id': 6523,
                'name': 'Schmid', 'firstname': 'Marco', 'title': '',
                'street': 'Rue des Terreaux 14', 'street2': '', 'street3': '',
                'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'phone': '', 'mobile': '', 'active': True,
                'birthdate': False, 'deathdate': False, 'services': [],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        referent_cl1.refresh_from_db()
        self.assertEqual(referent_cl1.rue, "Rue des Terreaux 14")

    def test_referent_update_archive(self):
        """
        Archivage d'un référent, mais un autre similaire est encore actif,
        ne pas l'archiver dans CID.
        """
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel", service=current_app(),
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel", service=current_app(),
        )
        referent0 = Referent.objects.create(
            client=cl0, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
            id_externe='6523', facturation_pour=['al-tout'],
        )
        referent1 = Referent.objects.create(
            client=cl1, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
            id_externe='6523', facturation_pour=['al-tout'],
        )
        user = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        with signals.connect_sync_signals():
            cl0.archiver(user, current_app())
        self.mock_put.assert_not_called()

    def test_referent_client_update(self):
        """Cas particulier du référent qui est aussi client."""
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Schmid", prenom="Marco", npa="2000", localite="Neuchâtel",
            id_externe=555
        )
        referent = Referent.objects.create(
            client=cl0, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
            id_externe=555, facturation_pour=['al-tout'],
        )
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = ReferentForm(client=cl0, instance=referent, initial={}, data={
                'id_externe': '555', 'nom': 'Schmid', 'prenom': 'Marco',
                'rue': 'xy 1', 'npalocalite': "2000 Neuchâtel",
                'facturation_pour': ['al-tout'],
                'referenttel_set-INITIAL_FORMS': 0,
                'referenttel_set-TOTAL_FORMS': 0,
            })
            self.assertTrue(form.is_valid(), form.errors)
            form.save()
            self.mock_post.assert_not_called()

    def test_referent_autre_pays(self):
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        referent = Referent.objects.create(
            client=cl0, nom='Stranger', prenom='Maria', npa="03331", localite="Calla", pays="ES",
            id_externe=555, facturation_pour=['al-tout'],
        )
        signals.referent_sync_receiver(sender=Referent, instance=referent, created=False, force=True)
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': f"r{referent.pk}", 'external_id': 555,
                'name': 'Stranger', 'firstname': 'Maria', 'title': '',
                'street': '', 'street2': '', 'street3': '', 'zip': '03331', 'city': 'Calla',
                'country': 'ES', 'email': '', 'phone': '', 'mobile': '',
                'active': True,
                'birthdate': False, 'deathdate': False, 'services': [],
            },
            auth=ANY,
            timeout=api_timeout,
        )

    def _benevole_form_data(self):
        form_data = dict(
            nom="Doe", prenom="John", genre='M', npalocalite="2000 Neuchâtel",
            date_naissance=date(1950, 4, 3), courriel="john.doe@example.org",
        )
        form_data.update({
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '0',
            'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
            'activite_set-0-duree_0': '31.10.2023',
            'telephone_set-TOTAL_FORMS': '0',
            'telephone_set-INITIAL_FORMS': '0',
        })
        return form_data

    @override_settings(MUTATIONS_BENEVOLES_EMAIL=["test@example.org"])
    def test_benevole_creation(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = BenevoleForm(instance=None, data={
                **self._benevole_form_data(),
                'activite_set-TOTAL_FORMS': '2',
                'activite_set-1-type_act': TypeActivite.objects.get(code=TypeActivite.TRANSPORT).pk,
                'activite_set-1-duree_0': '10.02.2024',
                'telephone_set-TOTAL_FORMS': '1',
                'telephone_set-INITIAL_FORMS': '0',
                'telephone_set-0-tel': '078 444 44 44',
            }, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            benev = form.save()
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}employees',
            json={
                'id': benev.pk,
                'name': 'Doe', 'firstname': 'John', 'gender': 'M',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': 'john.doe@example.org', 'birthdate': '1950-04-03',
                'phone': '', 'mobile': '078 444 44 44', 'active': True,
                'services': [
                    {'name': 'Alarme', 'date_start': '2023-10-31', 'date_end': False},
                    {'name': 'Transports', 'date_start': '2024-02-10', 'date_end': False},
                ],
            },
            auth=ANY,
            timeout=api_timeout,
        )
        benev.refresh_from_db()
        self.assertEqual(benev.id_externe, 6523)
        # Message de création envoyé
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            "Un nouveau bénévole a été créé dans le logiciel MAD:\n"
            "Nom: Doe\n"
            "Prénom: John\n"
            "Adresse: , 2000 Neuchâtel\n"
            "Date de naissance: 03.04.1950\n"
            "Service(s): Alarme, Transports\n"
            "\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        )

    def test_benevole_creation_sans_date_naissance(self):
        """Pas de synchro si date de naissance manquante"""
        form = BenevoleForm(
            data={**self._benevole_form_data(), 'date_naissance': ''},
            instance=None, initial={}
        )
        self.assertTrue(form.is_valid(), form.errors)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            benev = form.save()
        self.mock_post.assert_not_called()

    def test_benevole_creation_debut_futur(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        superuser = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        self.client.force_login(superuser)
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('benevole-new', args=[current_app()]), data={
                **self._benevole_form_data(),
                "activite_set-0-duree_0": date.today() + timedelta(days=4),
            })
        self.mock_post.assert_called_once()
        self.assertTrue(Benevole.objects.filter(persona__nom="Doe").exists())

    @override_settings(MUTATIONS_BENEVOLES_EMAIL=["test@example.org"])
    def test_benevole_update(self):
        pers_data = dict(
            nom="Doe", prenom="Johnny", npa="2000", genre='M', localite="Neuchâtel",
            date_naissance=date(1950, 4, 3),
        )
        act_tische = TypeActivite.objects.get_or_create(code='tische', nom='FT-HT')[0]
        benev = Benevole.objects.create(
            id_externe=6523,
            persona=Persona.objects.create(**pers_data),
            activites=[TypeActivite.INSTALLATION, 'tische'],
        )
        tel = Telephone.objects.create(persona=benev.persona, tel="078 444 44 44", priorite=1)
        activs = benev.activite_set.values_list('pk', flat=True)
        form_data = {
            **self._benevole_form_data(),
            'telephone_set-TOTAL_FORMS': '2',
            'telephone_set-INITIAL_FORMS': '1',
            'telephone_set-0-id': tel.pk,
            'telephone_set-0-tel': '078 444 44 44',
            'telephone_set-0-persona': benev.persona_id,
            'telephone_set-1-tel': '031 310 31 31',
            'activite_set-TOTAL_FORMS': '2',
            'activite_set-INITIAL_FORMS': '2',
            'activite_set-0-id': activs[0],
            'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
            'activite_set-0-duree_0': '31.10.2023',
            'activite_set-1-id': activs[1],
            'activite_set-1-type_act': act_tische.pk,
            'activite_set-1-duree_0': '30.11.2023',
        }
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = BenevoleForm(instance=benev, data=form_data)
            self.assertTrue(form.is_valid(), form.errors)
            benev = form.save()
        expected_json = {
            'id': benev.pk, 'external_id': 6523,
            'name': 'Doe', 'firstname': 'John', 'gender': 'M',
            'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
            'email': 'john.doe@example.org', 'birthdate': '1950-04-03',
            'phone': '031 310 31 31', 'mobile': '078 444 44 44', 'active': True,
            'services': [
                {'name': 'Alarme', 'date_start': '2023-10-31', 'date_end': False},
                {'name': 'FT-HT', 'date_start': '2023-11-30', 'date_end': False},
            ],
        }
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}employees',
            json=expected_json,
            auth=ANY,
            timeout=api_timeout,
        )
        # Message de mutation envoyé
        self.assertEqual(len(mail.outbox), 1)
        today_str = date.today().strftime("%Y-%m-%d")
        self.assertEqual(
            mail.outbox[0].body,
            "Les modifications suivantes ont été apportées au bénévole Doe John "
            "dans le logiciel MAD:\n"
            " * Prenom: «Johnny» => «John»\n"
            f" * Activités: «Alarme ({today_str} - ), FT-HT ({today_str} - )» => "
            "«Alarme (2023-10-31 - ), FT-HT (2023-11-30 - )»\n"
            "\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        )
        # Une seule mise à jour de date d'activité doit aussi générer la synchro
        benev = Benevole.objects.get(pk=benev.pk)
        form_data['activite_set-1-duree_0'] = '5.12.2023'
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = BenevoleForm(instance=benev, data=form_data)
            self.assertTrue(form.is_valid(), form.errors)
            benev = form.save()
        expected_json['services'][1]['date_start'] = '2023-12-05'
        self.mock_put.assert_called_with(
            f'{settings.CID_API_URL}employees',
            json=expected_json,
            auth=ANY,
            timeout=api_timeout,
        )
        self.assertEqual(len(mail.outbox), 2)
        self.assertIn(
            "«Alarme (2023-10-31 - ), FT-HT (2023-11-30 - )» => «Alarme (2023-10-31 - ), FT-HT (2023-12-05 - )»",
            mail.outbox[1].body
        )

    def test_benevole_update_minimal(self):
        """
        Si aucune modification ne touche des champs synchronisés, l'API de sync
        n'est pas appelée.
        """
        pers_data = dict(
            nom="Doe", prenom="John", npa="2000", genre='M', localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), courriel='john.doe@example.org'
        )
        benev = Benevole.objects.create(
            id_externe=6523, persona=Persona.objects.create(**pers_data), activites=[]
        )
        form_data = {
            **self._benevole_form_data(),
            'activite_set-TOTAL_FORMS': '0',
            'activite_set-INITIAL_FORMS': '0',
        }
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            form = BenevoleForm(instance=benev, data=form_data)
            self.assertTrue(form.is_valid(), form.errors)
            benev = form.save()
        self.mock_put.assert_not_called()

    def test_benevole_update_admin(self):
        pers_data = dict(
            nom="Doe", prenom="John", npa="2000", genre='M', localite="Neuchâtel",
            pays='CH', date_naissance=date(1950, 4, 3),
        )
        benev = Benevole.objects.create(
            id_externe=6523,
            persona=Persona.objects.create(**pers_data),
            activites=[TypeActivite.INSTALLATION],
        )
        Telephone.objects.create(persona=benev.persona, tel="078 444 44 44", priorite=1)
        superuser = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        self.client.force_login(superuser)
        with signals.connect_sync_signals():
            response = self.client.post(
                reverse('admin:benevole_benevole_change', args=[benev.pk]),
                data={
                    **pers_data,
                    'id_externe': 6523,
                    'persona': benev.persona.pk,
                    'activite_set-TOTAL_FORMS': '1',
                    'activite_set-INITIAL_FORMS': '1',
                    'activite_set-0-id': benev.activite_set.first().pk,
                    'activite_set-0-type_act': TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
                    'activite_set-0-duree_0': '31.10.2023',
                },
            )
            self.assertEqual(response.status_code, 302)
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}employees',
            json={
                'id': benev.pk, 'external_id': 6523,
                'name': 'Doe', 'firstname': 'John', 'gender': 'M',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03',
                'phone': '', 'mobile': '078 444 44 44', 'active': True,
                'services': [{'name': 'Alarme', 'date_start': '2023-10-31', 'date_end': False}]
            },
            auth=ANY,
            timeout=api_timeout,
        )

    @override_settings(MUTATIONS_BENEVOLES_EMAIL=["test@example.org"])
    def test_benevole_update_archive(self):
        benev = self._create_benevole(
            id_externe=6523, npa="2000", localite="Neuchâtel", activites=[TypeActivite.INSTALLATION]
        )
        user = Utilisateur.objects.create_user(
            "me@example.org", "mepassword", first_name="Jean", last_name="Valjean",
        )
        user.user_permissions.add(
            Permission.objects.get(codename="view_benevole"),
            Permission.objects.get(codename="change_benevole")
        )
        with signals.connect_sync_signals(), self.captureOnCommitCallbacks(execute=True):
            self.client.force_login(user)
            response = self.client.post(
                reverse("benevole-edit", args=[current_app(), benev.pk]),
                data={
                    **self._benevole_form_data(),
                    "id_externe": 6523,
                    "persona": benev.persona.pk,
                    "activite_set-TOTAL_FORMS": "1",
                    "activite_set-INITIAL_FORMS": "1",
                    "activite_set-0-id": benev.activite_set.first().pk,
                    "activite_set-0-type_act": TypeActivite.objects.get(code=TypeActivite.INSTALLATION).pk,
                    "activite_set-0-duree_0": "31.10.2023",
                    "activite_set-0-duree_1": "31.10.2024",
                },
            )
            self.assertEqual(response.status_code, 302)
        # 1x pour enregistrement, 1x pour archivage
        self.assertEqual(self.mock_put.call_count, 1)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(
            f"* Activités: «Alarme ({date.today().strftime('%Y-%m-%d')} - )» => "
            "«Alarme (2023-10-31 - 2024-10-31)»",
            mail.outbox[0].body
        )
