from datetime import date

from django import forms

from common.forms import FormsetMixin, ModelForm
from client.forms import get_referent_formset, AdresseFormMixin, PersonaForm as PersonaFormBase
from client.models import Persona, Referent, ReferentTel
from cr_ne.utils import rue_changed


class PermissiveRueField(forms.CharField):
    """Ignorer certains changements mineurs dans les noms de rues."""
    def has_changed(self, initial, data):
        if not rue_changed(initial or '', data or ''):
            return False
        return super().has_changed(initial, data)


class PersonaForm(AdresseFormMixin, PersonaFormBase):
    class Meta(PersonaFormBase.Meta):
        fields = [
            'nom', 'prenom', 'genre', 'case_postale', 'courriel',
            'date_naissance', 'date_deces',
        ]
    adresse_fields = ["c_o", "rue", "npa", "localite"]
    always_add_address_fields = True

    def __init__(self, instance=None, **kwargs):
        super().__init__(instance=instance, **kwargs)
        self._add_address_fields(field_classes={'rue': PermissiveRueField})
        client = instance.get_client() if instance else None
        if instance and client and client.partenaire_id:
            # Ne pas mettre à jour le genre pour les couples (CID envoie Madame et Monsieur)
            del self.fields['genre']

    def clean(self):
        data = super().clean()
        self.doublons = None
        if not self.instance.pk and not self.errors:
            # Détection de doublons
            self.doublons = Persona.objects.avec_adresse(date.today()).filter(
                nom__iexact=self.cleaned_data['nom'],
                prenom__iexact=self.cleaned_data['prenom'],
                adresse_active__localite=self.cleaned_data['localite'],
                date_naissance=self.cleaned_data['date_naissance']
            )
            if self.doublons:
                raise forms.ValidationError("Cette personne existe déjà dans la base")
        if self.instance.pk and {'nom', 'prenom', 'npa'}.issubset(set(self.changed_data)):
            raise forms.ValidationError(
                "La modification simultanée du nom, prénom et numéro postal est douteuse"
            )
        return data

    def save(self, **kwargs):
        is_new = self.instance.pk is None
        persona = super().save(**kwargs)
        if not is_new and set(self.changed_data).intersection({'rue', 'npa', 'localite'}):
            persona.adresses.nouvelle(
                rue=self.cleaned_data['rue'], npa=self.cleaned_data['npa'], localite=self.cleaned_data['localite']
            )
        return persona


class ReferentForm(FormsetMixin, ModelForm):
    doublons = False

    class Meta:
        model = Referent
        fields = [
            'salutation', 'nom', 'prenom', 'rue', 'complement',
            'case_postale', 'npa', 'localite', 'courriel',
        ]
        field_classes = {'rue': PermissiveRueField}

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        TelFormset = get_referent_formset()
        self.formset = TelFormset(
            data=data, instance=kwargs.get('instance'),
            queryset=ReferentTel.objects.order_by('priorite'),
        )

    def clean_nom(self):
        data = self.cleaned_data['nom']
        if self.instance.pk:
            # Ne pas accepter modifs noms tout en majuscules
            initial = self.instance.nom
            if initial and data and data.isupper() and initial.lower() == data.lower():
                return initial
        return data

    def save(self, **kwargs):
        for idx, frm in enumerate(self.formset.ordered_forms):
            frm.instance.priorite = idx + 1
        return super().save(**kwargs)
